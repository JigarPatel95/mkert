# README

Describes configuration files

## mkert-0.1.4-cmmb1.ini

- HW synchronization
- Serial port on /dev/ttyAMA0 with 115200 baudrate
- 30FPS, precise synchronized
- Sync by whole frame flash

## mkert-0.1.4-rpi.ini

- SW synchronization (FPGA)
- 15FPS
- Sync by long whole frame flash


