# MkE SDK v@VERSION@

This is the SDK for the sensor Magik Eye 1.

For the description of MkE API see:

* [mkeapi/doc/html/index.html](mkeapi/doc/html/index.html)
* [mkeapi/doc/mkeapi.pdf](mkeapi/doc/mkeapi.pdf)

For a Matlab implementation of a MkE API client see:

* [client/matlab/README.html](client/matlab/README.html)

For a Python implementation of a MkE API client see:

* [client/python/README.html](client/python/README.html)


---
Copyright (c) 2016-2017, Magik-Eye Inc.
