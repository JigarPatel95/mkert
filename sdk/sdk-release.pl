#!/usr/bin/perl

use Getopt::Long;
use File::Basename;
use File::Copy;
use File::Temp qw/ tempfile tempdir /;
use Cwd 'abs_path', 'getcwd';
use File::Path qw(make_path remove_tree);



###############################################################################

sub copyto($$) {
  my $src = shift;
  my $dst = shift;
  
  if (-f $src)
    {
      $src_file = $src;
      my($filename, $dirs, $suffix) = fileparse($src);
     
      if (-d $dst)
        {
          $dst_file = "$dst/$filename$suffix";
        }
      elsif (-f $dst)
        {
          $dst_file = $dst;
        }
      else
        {
          die ("Not a file or a directory: \"$dst\"");
        }
          
      copy("$src_file", "$dst_file") 
        or die ("Copy failed \"$src_file\" to \"$dst_file\": $!");
    }
  elsif (-d $src)
    {
      if (-d $dst)
        {
          $dst_dir = $dst;
        }
      else
        {
          die("Not a directory: $dst");
        }
      
      `cp -r "$src" "$dst_dir"`;
    }
  else
    {
      die("Not a file or a directory: $src");
    }
}

sub get_version($) {
  my $root_dir = shift;
  open(my $fh, '<', "$root_dir" . "/sdk/version.in");
  
  while (my $row = <$fh>) 
    {
      ($row =~ /^\s*VERSION\s*=\s*([0-9]+).([0-9])+.([0-9]+)\s*/)
        and return "$1.$2.$3";
    }

}

###############################################################################
## main ###

GetOptions(
    "output_dir=s"     => \$output_dir,
);

# Root directory of MKERT git repo
$root_dir = dirname(abs_path($0 . "/../")) . "/";
print "MkE runtime root: $root_dir\n";

# SDK version
$sdk_version = get_version($root_dir);

# Output directory
(defined $output_dir)
  or $output_dir = getcwd();

print "Output directory: $output_dir\n";

# SDK package
(defined $sdk_version)
  or die "Unrecognized SDK version: set \"version.in\" file";

$tmp_dir = tempdir(CLEANUP => 1);
$sdk_name = "mke-sdk-$sdk_version";
$sdk_dir = "$tmp_dir/$sdk_name";
print "SDK temp directory: $sdk_dir\n";

###############################################################################

$cdir = getcwd();

# SDK directory
mkdir("$sdk_dir")
 or die("mkdir: $!");

# Main SDK readme
copyto("$root_dir/sdk/README.md", "$sdk_dir");
`sed -i 's/\@VERSION\@/$sdk_version/' "$sdk_dir/README.md"`;
`pandoc -s "$sdk_dir/README.md" -o "$sdk_dir/README.html"`;


# MkE API
mkdir("$sdk_dir/mkeapi") or die("mkdir: $!");
mkdir("$sdk_dir/mkeapi/include") or die("mkdir: $!");
mkdir("$sdk_dir/mkeapi/doc") or die("mkdir: $!");

chdir("$root_dir/mkeapi/doc") or die "chdir: $!";
`rm -rf html`;
`./compile.sh`;
chdir($cdir) or die "chdir: $!";

copyto("$root_dir/mkeapi/include/mkeapi.h", "$sdk_dir/mkeapi/include/");
copyto("$root_dir/mkeapi/doc/mkeapi.pdf", "$sdk_dir/mkeapi/doc");
copyto("$root_dir/mkeapi/doc/html", "$sdk_dir/mkeapi/doc");

# Clients
mkdir("$sdk_dir/client") or die("mkdir: $!");
copyto("$root_dir/client/matlab", "$sdk_dir/client/");
remove_tree("$sdk_dir/client/matlab/reserved_api");
`pandoc -s "$root_dir/client/matlab/README.md" -o "$sdk_dir/client/matlab/README.html"`;

copyto("$root_dir/client/python", "$sdk_dir/client/");
remove_tree("$sdk_dir/client/python/reserved_api");
`pandoc -s "$root_dir/client/python/README.md" -o "$sdk_dir/client/python/README.html"`;

# Remove stray temp files
$tmp_files = `find $sdk_dir | grep '^.*~\$' | xargs rm`;


# Create final archive
chdir($tmp_dir) or die "chdir: $!";
`zip -r "$sdk_name.zip" "$sdk_name"`;
chdir($cdir) or die "chdir: $!";
copy("$sdk_dir.zip", "$output_dir") or die "Copy failed: $!";
