/*
 * Client - wraps communication protocol with the MagikEye sensor
 *          independently to a bus type
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef _CLI_CLIENT_H_
#define _CLI_CLIENT_H_

/* -------------------------------------------------------------------------- */

#include <vector>
#include <queue>
#include <cstdint>
#include <stdexcept>
#include <cassert>

#include <boost/crc.hpp>

/* -------------------------------------------------------------------------- */

#include "mkeapi.h"
#include "basebus.h"

/* -------------------------------------------------------------------------- */

namespace mke {
namespace cli { 

/* -------------------------------------------------------------------------- */

class Client;

/* -------------------------------------------------------------------------- */

template <typename Type>
class Array
{
protected:
  const Type          *       begin_;
  const Type          *       end_;
  
public:
  
  // constructor by end
  Array(const Type * begin_ptr, Type * end_ptr) :
    begin_(begin_ptr), end_(end_ptr)
  {};
  
  // constructor by size
  Array(const Type * begin_ptr, size_t size) :
    begin_(begin_ptr), end_(begin_ + size)
  {};
  
  inline const Type * begin() const { return begin_; }
  
  inline const Type * end() const { return end_; }
  
  inline size_t size() const { return end_ - begin_; }
  
  inline const Type & operator[](int idx) { return begin() + idx; }
  
  // destructor
  virtual ~Array() {};
};

/* -------------------------------------------------------------------------- */

class Frame
{
private:
  std::vector<char>            data_collector_;
  api::MkEReply_Frame          params;

  friend class Client;
  
public:
  
  Frame() :
    data_collector_(0)
  {};
  
  const api::MkEReply_Frame getParams() const { return params; }
  
  template <typename MkEFrameItemType>
  const Array<MkEFrameItemType> getPtsAs() const { return Array<MkEFrameItemType>(reinterpret_cast<const MkEFrameItemType *>(data_collector_.data()), size()); };
  
  uint16_t size() const { return params.num_data; }
  
  bool isFrameValid()
  {
    boost::crc_32_type result;
    result.process_bytes(data_collector_.data(), data_collector_.size()-4);
    uint32_t * crc_received = reinterpret_cast<uint32_t *>(data_collector_.data() + data_collector_.size()-4);
    uint32_t crc_computed = result.checksum();
    
    return crc_computed == *crc_received;
  }
};

/* -------------------------------------------------------------------------- */

class Client

{
public:
  typedef uint32_t Handle;
  
protected:

  
  BaseBus               *       bus_;           // pointer to used comm. bus
  Handle                        seqn_;          // sequence number for next request
  
  std::vector<Frame>            frame_buffer_;  // frame buffer
  std::queue<Frame *>           frame_received_;// list of ready readed frames
  std::queue<Frame *>           frame_free_;    // list of usable buffer items
  std::unique_ptr<api::MkERequest>
                                frame_push_req_;// frame push request
  
  // should be there some buffer for frames?

  /**
   * 
   */
  void prepareRequest(api::MkERequest & req, api::MkERequestType req_type);
  
  /**
   * 
   */
  void checkReply(const api::MkEReply & rep, 
                  api::MkEReplyStatus status = api::MKE_REPLY_OK);
  
  /**
   * 
   */
  inline void waitForReply(const api::MkERequest & request, api::MkEReply & reply)
  {
    std::vector<char> no_expected_payload;
    waitForReply(request, reply, no_expected_payload);
  }
  
  /**
   * 
   */
  void waitForReply(const api::MkERequest & request, api::MkEReply & reply, 
                    std::vector<char> & payload);

  /**
   * 
   */
  virtual size_t getFrameItemSize(api::MkEFrameType frame_type);
  
public:

  /**
   * @brief Contructor of the client
   * 
   * @param bus communication bus will be used
   */
  Client(BaseBus * bus) :
    bus_(bus),
    seqn_(0)
  {};
  
  /**
   * @brief Destructor of the client
   */
  virtual ~Client() {};
  
  /**
   * @brief Get current state of the device
   * 
   * @return cuurent state of the device
   */
  api::MkEStateType getState();
  
  /**
   * @brief Set state of the device
   * 
   * @param state state should be setted
   */
  void setState(api::MkEStateType state);
  
  /**
   * @brief Get firmware info of the device
   * 
   * @return firmware info
   */
  api::MkEReply_FirmwareInfo getFirmwareInfo();
  
  /**
   * @brief Get device info of the device
   * 
   * @return device info
   */
  api::MkEReply_DeviceInfo getDeviceInfo();
  
  /**
   * @brief Terminate device
   * 
   * @param term_type type of termination
   */
  void terminate(api::MkETerminateMethodType term_type);
  
  /**
   * @brief Get current frame from the device (depth-sensor)
   * 
   * @param frame_type type of items given in frame
   * @param items reference to array which should be filled with items
   * @param out_params get frame params if you want them
   */
  void getFrame(api::MkEFrameType frame_type, Frame & output);
  
  /**
   * @brief Start pushing frames
   * 
   * @param frame_type type of items given in frame
   * @return handle for receiving data
   */
  void startFramePush(api::MkEFrameType frame_type, int buffer_size = 8);
  
  /**
   * @brief Stop pushing frames
   */
  void stopFramePush();
  
  /**
   * @brief Get pushed frame - nullptr means stream is not initialized
   */
  const Frame * getPushedFrame();
  
  /**
   * 
   */
  void releasePushedFrame(const Frame * buffer_item);
};

/* -------------------------------------------------------------------------- */

} // end of mke::cli namespace
} // end of mke namespace 

/* -------------------------------------------------------------------------- */

#endif // _CLI_CLIENT_H_

/* -------------------------------------------------------------------------- */
