/*
 * ReservedClient - wraps FULL communication protocol with the MagikEye sensor
 *                     independently to a bus type
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#include "reserved_client.h"

/* -------------------------------------------------------------------------- */

#include <cstring>
#include <cstdio>
#include <cassert>

/* -------------------------------------------------------------------------- */

using namespace mke;
using namespace mke::cli;

/* -------------------------------------------------------------------------- */



/* -------------------------------------------------------------------------- */

uint32_t ReservedClient::getShutterSpeed()

{
  api::MkERequest req;
  prepareRequest(req, api::MKE_REQUEST_GET_SHUTTER);
  bus_->sendRequest(req);
  
  api::MkEReply rep;
  waitForReply(req, rep);
  checkReply(rep);
  
  return rep.params.getshutterspeed.shutter_speed;
}

/* -------------------------------------------------------------------------- */

void ReservedClient::setShutterSpeed(uint32_t shutter_speed)

{
  api::MkERequest req;
  prepareRequest(req, api::MKE_REQUEST_SET_SHUTTER);
  req.params.setshutterspeed_params.shutter_speed = shutter_speed;
  bus_->sendRequest(req);
  
  api::MkEReply rep;
  waitForReply(req, rep);
  checkReply(rep);
}

/* -------------------------------------------------------------------------- */

void ReservedClient::getGains(uint32_t& analog_gain, uint32_t& digital_gain)

{
  api::MkERequest req;
  prepareRequest(req, api::MKE_REQUEST_GET_GAIN);
  bus_->sendRequest(req);
  
  api::MkEReply rep;
  waitForReply(req, rep);
  checkReply(rep);

  analog_gain = rep.params.getgain.analog_gain;
  digital_gain = rep.params.getgain.digital_gain;
}

/* -------------------------------------------------------------------------- */

void ReservedClient::setISO(uint32_t iso_value)

{
  api::MkERequest req;
  prepareRequest(req, api::MKE_REQUEST_SET_SHUTTER);
  req.params.setgain_params.iso_value = iso_value;
  bus_->sendRequest(req);
  
  api::MkEReply rep;
  waitForReply(req, rep);
  checkReply(rep);
}

/* -------------------------------------------------------------------------- */

void ReservedClient::getLaserPattern(uint16_t& laser_pattern, uint32_t& duration)

{
  api::MkERequest req;
  prepareRequest(req, api::MKE_REQUEST_GET_LASER);
  bus_->sendRequest(req);
  
  api::MkEReply rep;
  waitForReply(req, rep);
  checkReply(rep);
  
  laser_pattern = rep.params.getlaser.pattern;
  duration = rep.params.getlaser.duration;
}

/* -------------------------------------------------------------------------- */

void ReservedClient::setLaserPattern(uint16_t laser_pattern, uint32_t duration)

{
  api::MkERequest req;
  prepareRequest(req, api::MKE_REQUEST_SET_SHUTTER);
  req.params.setlaser_params.pattern = laser_pattern;
  req.params.setlaser_params.duration = duration;
  bus_->sendRequest(req);
  
  api::MkEReply rep;
  waitForReply(req, rep);
  checkReply(rep);
}

/* -------------------------------------------------------------------------- */

// read element

const char * readElement(const char * c, char * buff, int limit)
{
  // skip spaces
  
  while(*c == ' ' || *c == '\t' || *c == '\n' || *c == '\r')
    c++;
  
  // skip comment if presets
  
  if(*c == '#')
    {
      while(*c != '\n' && *c != '\r')
        c++;
      
      return readElement(c, buff, limit);
    }

  // read element

  char * b = buff;
  while(*c != ' ' && *c != '\t' && *c != '\n' && *c != '\r' && limit > 0)
    {
      *b++ = *c++;
      limit--;
    }
  *b = '\0';
  
  if(limit < 0)
    throw std::runtime_error("Invalid file format");
  
  return c;
}

/* -------------------------------------------------------------------------- */

void parsePGM(const char * c, size_t size, Image & out)
{
  const char * cE = c + size;
  char buff[5];
  
  c = readElement(c, buff, std::min<int>(2, cE-c));
  if(strcmp(buff, "P5") != 0)
    throw std::runtime_error("Unsupported type");
  
  c = readElement(c, buff, std::min<int>(4, cE-c));
  out.stride = out.w = std::atoi(buff);
  c = readElement(c, buff, std::min<int>(4, cE-c));
  out.h = std::atoi(buff);
  c = readElement(c, buff, std::min<int>(4, cE-c));
  // ignore max val

  out.data = c;
}

/* -------------------------------------------------------------------------- */

void ReservedClient::getImage(MemImage & out, int num_avg_images)

{
  api::MkERequest req;
  prepareRequest(req, api::MKE_REQUEST_GET_IMAGE);
  req.params.getimage_params.num_average = num_avg_images;
  bus_->sendRequest(req);
  
  api::MkEReply rep;
  waitForReply(req, rep, out.data_colletor_);
  checkReply(rep);
  
  parsePGM(out.data_colletor_.data(), out.data_colletor_.size(), out);
}

/* -------------------------------------------------------------------------- */

void DebugImages::parseReply()

{
  subheaders_.clear();
  
  const char * ptr = data_colletor_.data();
  for(int i = 0; i < size(); ++i)
  {
    const api::MkEDebugImagesSubHeader * sub = reinterpret_cast<const api::MkEDebugImagesSubHeader *>(ptr);
    subheaders_.push_back(sub);
    ptr += sizeof(api::MkEDebugImagesSubHeader) 
        + sub->image_num_bytes 
        + sub->frame_num_bytes;
  }
}

/* -------------------------------------------------------------------------- */

Image DebugImages::getImage(int image_id) const
{
  Image out;
  const api::MkEDebugImagesSubHeader * sub = subheaders_[image_id];
  const char * im_ptr = reinterpret_cast<const char *>(sub) 
                                  + sizeof(const api::MkEDebugImagesSubHeader);
  parsePGM(im_ptr, sub->image_num_bytes, out);
  return out;
}

/* -------------------------------------------------------------------------- */

void ReservedClient::getDebugImages(api::MkEFrameType frame_type, int num_images,
                                    DebugImages & out)
{
  api::MkERequest req;
  prepareRequest(req, api::MKE_REQUEST_GET_DEBUG_IMAGES);
  req.params.debugimages_params.frame_type = frame_type;
  req.params.debugimages_params.num_images = num_images;
  bus_->sendRequest(req);
  
  api::MkEReply rep;
  waitForReply(req, rep, out.data_colletor_);
  checkReply(rep);
  
  out.size_ = rep.params.debugimages_params.num_images;
  out.parseReply();
}

/* -------------------------------------------------------------------------- */

void ReservedClient::uploadPackage(const char* data, size_t size)

{
  api::MkERequest req;
  prepareRequest(req, api::MKE_REQUEST_SET_SHUTTER);
  req.params.dynamic_params.payload_size = size;
  bus_->sendRequest(req);
  bus_->sendPayload(data, size);
  
  api::MkEReply rep;
  waitForReply(req, rep);
  checkReply(rep);
}

/* -------------------------------------------------------------------------- */
