/*
 * ReservedClient - wraps FULL communication protocol with the MagikEye sensor
 *                     independently to a bus type
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef _CLI_RESERVED_CLIENT_H_
#define _CLI_RESERVED_CLIENT_H_

/* -------------------------------------------------------------------------- */

#include <vector>
#include <queue>
#include <cstdint>
#include <stdexcept>
#include <cassert>

#include <boost/crc.hpp>

/* -------------------------------------------------------------------------- */

#include "mkeapi_reserved.h"
#include "../client.h"

/* -------------------------------------------------------------------------- */

namespace mke {
namespace cli { 

/* -------------------------------------------------------------------------- */

class ReservedClient;

/* -------------------------------------------------------------------------- */

/**
 * general image
 */
struct Image 
{
public:
  Image() {};
  
  Image(const char * data, int w, int h, int stride = -1) :
    data(data), w(w), h(h), stride(stride > w ? stride : w)
  {};
    
  const char *          data;           // first pixel of image
  int                   w;              // width of image
  int                   h;              // height of image
  int                   stride;         // memory width of image in bytes
};

/**
 * memory allocated image
 */
struct MemImage : public Image
{
private:
  std::vector<char>     data_colletor_;
  friend class ReservedClient;
};


/**
  * structure for returning image with frame
  */
class DebugImages
{
private:
  std::vector<char>           data_colletor_;
  std::vector<const api::MkEDebugImagesSubHeader *>
                              subheaders_;
  int                         size_;
  
  void parseReply();
  
  friend class ReservedClient;

public:
  /**
   * 
   */
  Image getImage(int image_id) const;
  
  int size() const { return size_; }

  /**
   * 
   */
  inline api::MkEReply_Frame getParams(int image_id) const 
  { 
    const api::MkEDebugImagesSubHeader * sub = subheaders_[image_id];
    api::MkEReply_Frame params;
    params.timer = sub->timer;
    params.seqn = 0;
    params.data_type = 0;
    params.frame_type = sub->frame_type;
    params.num_data = sub->frame_num_data;
    
    return params; 
  };
  
  /**
   * 
   */
  template <typename MkEFrameItemType>
  Array<MkEFrameItemType> getPtsAs(int idx) const 
  { 
    const api::MkEDebugImagesSubHeader * sub = subheaders_[idx];
    const char * ptr = reinterpret_cast<const char *>(sub) 
      + sizeof(api::MkEDebugImagesSubHeader) + sub->image_num_bytes;
    return Array<MkEFrameItemType>(reinterpret_cast<const MkEFrameItemType *>(ptr), sub->frame_num_data);
  }
};

/* -------------------------------------------------------------------------- */

class ReservedClient : public Client

{
public:

  
  /**
   * constructor
   */
  ReservedClient(BaseBus* bus)
  : Client(bus)
  {};
  
  /**
   * 
   */
  void getDebugImages(api::MkEFrameType frame_type, int num_images,
                      DebugImages & out);
  
  /**
   * 
   */
  void getImage(MemImage & out, int num_avg_images = 1);
  
  /**
   * 
   */
  uint32_t getShutterSpeed();
  
  /**
   * 
   */
  void setShutterSpeed(uint32_t shutter_speed);
  
  /**
   * 
   */
  void getGains(uint32_t & analog_gain, uint32_t & digital_gain);
  
  /**
   * 
   */
  void setISO(uint32_t iso_value);
  
  /**
   * 
   */
  void getLaserPattern(uint16_t & laser_pattern, uint32_t & duration);

  /**
   *
   */
  void setLaserPattern(uint16_t laser_pattern, uint32_t duration = 0);
  
  /**
   * 
   */
  void uploadPackage(const char * data, size_t num);
};

/* -------------------------------------------------------------------------- */

}       // end of mke::cli
}       // end of mke

/* -------------------------------------------------------------------------- */

#endif

/* -------------------------------------------------------------------------- */
