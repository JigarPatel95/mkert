/*
 * TcpClient - class wrapping communication protocol with the MagikEye sensor
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef _CLI_TCPBUS_H_
#define _CLI_TCPBUS_H_

/* -------------------------------------------------------------------------- */

#include "mkeapi.h"
#include "basebus.h"

#include <boost/asio.hpp>

/* -------------------------------------------------------------------------- */

namespace mke {
namespace cli { 

/* -------------------------------------------------------------------------- */

class TcpBus : public BaseBus

{
protected:
  const char            *       host_;          // address of the device 
  const char            *       port_;          // communication port of the device
  
  boost::asio::io_service       io_service_;    // boost is service
  boost::asio::ip::tcp::socket  socket_;        // comm. socket

public:

  /**
   * @brief Contructor of the client
   * 
   * @param host address of the device
   * @param port communication port of the device
   * 
   */
  TcpBus(const char * host, const char * port = "8888", bool verbose = false);
  
  /**
   * @brief Abstract method for sending request with payload through the bus 
   * 
   * @param req request will be sent
   */
  virtual void send(const char * data, size_t num);
  
  /**
   * @brief Abstract method for receiving next reply from the bus
   * 
   * @return next received reply
   */
  virtual size_t recv(char * buff, size_t buff_size);
};

/* -------------------------------------------------------------------------- */

} // end of mke::cli namespace
} // end of mke namespace 

/* -------------------------------------------------------------------------- */

#endif // _CLI_TCPBUS_H_

/* -------------------------------------------------------------------------- */
