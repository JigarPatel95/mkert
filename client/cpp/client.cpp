/*
 * Client - wraps communication protocol with the MagikEye sensor
 *          independently to a bus type
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#include "client.h"

/* -------------------------------------------------------------------------- */

#include <cstring>
#include <cstdio>
#include <cassert>
#include <sstream>

/* -------------------------------------------------------------------------- */

#define MKE_REQUEST_MAGIK       "MKERQ100"
#define MKE_REPLY_MAGIK         "MKERP100"

/* -------------------------------------------------------------------------- */

#define MKECLI_MAX_NUMPTS       255

/* -------------------------------------------------------------------------- */

using namespace mke;
using namespace mke::cli;

/* -------------------------------------------------------------------------- */

int atoi(const char * str, int num_bytes)

{
  int ret = 0;
  for(; num_bytes > 0; num_bytes--, str++)
    ret = ret * 10 + (*str-'0');
  
  return ret;
}

/* -------------------------------------------------------------------------- */

void itoa(int val, char * buff, size_t len)

{
  char * b = buff + len - 1;
  for (; b >= buff; --b)
    {
      *b = '0' + (val % 10);
      val /= 10;
    }
}

/* -------------------------------------------------------------------------- */

const char * mkeReplyCodeToStr(mke::api::MkEReplyStatus st)

{
  switch(st)
    {
    case api::MKE_REPLY_CLIENT_ERROR:
      return "CLIENT_ERROR";
    case api::MKE_REPLY_CLIENT_ILLEGAL_REQUEST_TYPE:
      return "CLIENT_ILLEGAL_REQUEST_TYPE";
    case api::MKE_REPLY_CLIENT_MALFORMED_REQUEST:
      return "CLIENT_MALFORMED_REQUEST";
    case api::MKE_REPLY_CLIENT_REQUEST_DOES_NOT_APPLY:
      return "CLIENT_REQUEST_DOES_NOT_APPLY";
    case api::MKE_REPLY_DATA_STOPPED:
      return "DATA_STOPPED";
    case api::MKE_REPLY_DATA_WILL_CONTINUE:
      return "DATA_WILL_CONTINUE";
    case api::MKE_REPLY_DATA_WILL_START:
      return "DATA_WILL_START";
    case api::MKE_REPLY_OK:
      return "OK";
    case api::MKE_REPLY_SERVER_BUSY:
      return "SERVER_BUSY";
    case api::MKE_REPLY_SERVER_ERROR:
      return "SERVER_ERROR";
    case api::MKE_REPLY_SERVER_INSUFFICIENT_RESOURCES:
      return "SERVER_INSUFFICIENT_RESOURCES:";
    case api::MKE_REPLY_SERVER_REQUEST_INTERRUPTED:
      return "SERVER_REQUEST_INTERRUPTED";
    case api::MKE_REPLY_UNDEF:
      return "UNDEF";
    default:
      return "!!UNKNOWN!!";
    }
}

/* -------------------------------------------------------------------------- */

void Client::prepareRequest(api::MkERequest& req, api::MkERequestType req_type)

{
  std::memcpy(req.magik, MKE_REQUEST_MAGIK, 8);
  itoa(int(req_type), req.type, 4);
  req.reqid = seqn_++;
  std::memset(req.params.param_bytes, 0, sizeof(api::MkERequest_Params));
}

/* -------------------------------------------------------------------------- */

void Client::checkReply(const api::MkEReply & rep, api::MkEReplyStatus status)

{
  int rep_status = atoi(rep.status, 4);
  if(rep_status != status)
    {
      std::ostringstream oss;
      oss << "Unexpected reply no.: " << rep_status << " (" 
          << mkeReplyCodeToStr(mke::api::MkEReplyStatus(rep_status)) << ")";
      throw std::runtime_error(oss.str());
    }
}

/* -------------------------------------------------------------------------- */

void Client::waitForReply(const api::MkERequest & request, api::MkEReply & reply,
                          std::vector<char> & payload)

{
  bus_->recvReply(reply);
  int istatus = atoi(reply.status);
  
  if(frame_push_req_ && frame_push_req_->reqid == reply.reqid && istatus >= 100 && istatus < 200)
    {
      if(istatus == api::MKE_REPLY_DATA_WILL_CONTINUE)
        {
          if(frame_free_.empty())
            {
              // ignore frame
              
              std::vector<char> dummy(reply.num_bytes);
              bus_->recvPayload(dummy.data(), reply.num_bytes);
            }
          else
            {
              // get frame to buffer
              
              Frame * bi = frame_free_.front();
              frame_free_.pop();
              bi->params = reply.params.frame_params;
              bi->data_collector_.resize(reply.num_bytes);
              bus_->recvPayload(bi->data_collector_.data(), bi->data_collector_.size());
              frame_received_.push(bi);

            }
        }
      else if(istatus == api::MKE_REPLY_DATA_STOPPED 
              || istatus == api::MKE_REPLY_SERVER_REQUEST_INTERRUPTED)
        {
          // empty queues
          
          while(!frame_free_.empty())
            frame_free_.pop();
          while(!frame_received_.empty())
            frame_received_.pop();
          
          frame_buffer_.resize(0);
        }
      else 
        {
          std::cout << "Status " << istatus << std::endl;
        }
    }
  else if(reply.num_bytes > 0)
    {
      payload.resize(reply.num_bytes);
      bus_->recvPayload(payload.data(), reply.num_bytes);
    }
  
  if(request.reqid != reply.reqid)
    waitForReply(request, reply, payload);
}

/* -------------------------------------------------------------------------- */

api::MkEStateType Client::getState()

{
  api::MkERequest req;
  prepareRequest(req, api::MKE_REQUEST_GET_STATE);
  bus_->sendRequest(req);
  
  api::MkEReply rep;
  waitForReply(req, rep);
  checkReply(rep);
  
  return api::MkEStateType(rep.params.state_params.state);
}

/* -------------------------------------------------------------------------- */

void Client::setState(api::MkEStateType state)

{
  api::MkERequest req;
  prepareRequest(req, api::MKE_REQUEST_SET_STATE);
  req.params.setstate_params.new_state = state;
  bus_->sendRequest(req);
  
  api::MkEReply rep;
  waitForReply(req, rep);
  checkReply(rep);
}

/* -------------------------------------------------------------------------- */

api::MkEReply_FirmwareInfo Client::getFirmwareInfo()

{
  api::MkERequest req;
  prepareRequest(req, api::MKE_REQUEST_GET_FIRMWARE_INFO);
  bus_->sendRequest(req);
  
  api::MkEReply rep;
  waitForReply(req, rep);
  checkReply(rep);
  
  return rep.params.fw_params;
}

/* -------------------------------------------------------------------------- */

api::MkEReply_DeviceInfo Client::getDeviceInfo()

{
  api::MkERequest req;
  prepareRequest(req, api::MKE_REQUEST_GET_DEVICE_INFO);
  bus_->sendRequest(req);
  
  api::MkEReply rep;
  waitForReply(req, rep);
  checkReply(rep);
  
  return rep.params.device_params;
}

/* -------------------------------------------------------------------------- */

void Client::terminate(api::MkETerminateMethodType term_type)

{
  api::MkERequest req;
  prepareRequest(req, api::MKE_REQUEST_TERMINATE);
  req.params.terminate_params.method = term_type;
  bus_->sendRequest(req);
  
  api::MkEReply rep;
  waitForReply(req, rep);
  checkReply(rep);
}

/* -------------------------------------------------------------------------- */

size_t Client::getFrameItemSize(api::MkEFrameType frame_type)

{
  switch(frame_type)
    {
    case api::MKE_FRAME_TYPE_1:
      return sizeof(api::MkEFrameItem1);
    case api::MKE_FRAME_TYPE_2:
      return sizeof(api::MkEFrameItem2);
    default:
      return 48;
    }
}

/* -------------------------------------------------------------------------- */

void Client::startFramePush(api::MkEFrameType frame_type, int buffer_size)

{
  assert(!frame_push_req_);
  assert(frame_buffer_.size() == 0);    // first and unique push request

  try
    {
      frame_push_req_.reset(new api::MkERequest());
      api::MkERequest & req = *frame_push_req_;
      prepareRequest(req, api::MKE_REQUEST_START_FRAME_PUSH);
      req.params.getframe_params.frame_type = uint32_t(frame_type);
      bus_->sendRequest(req);
      
      api::MkEReply rep;
      waitForReply(req, rep);
      checkReply(rep, api::MKE_REPLY_DATA_WILL_START);

      // prepare buffer
      
      frame_buffer_.resize(buffer_size);
//      for(std::vector<MemFrame>::iterator bi = frame_buffer_.begin(); bi != frame_buffer_.end(); ++bi)
      for(auto & bi : frame_buffer_)
        {
          bi.data_collector_.resize(MKECLI_MAX_NUMPTS*getFrameItemSize(frame_type));
          frame_free_.push(&bi);
        }
    }
  catch(std::exception &)
    {
      frame_push_req_.reset();
      throw;
    }
}

/* -------------------------------------------------------------------------- */

void Client::stopFramePush()

{
  assert(frame_push_req_);
  
  api::MkERequest req;
  prepareRequest(req, api::MKE_REQUEST_STOP_FRAME_PUSH);
  bus_->sendRequest(req);
  
  api::MkEReply rep;
  waitForReply(req, rep);
  checkReply(rep);
  
  frame_push_req_.reset();
}

/* -------------------------------------------------------------------------- */

void Client::getFrame(api::MkEFrameType frame_type, Frame & output)

{
  api::MkERequest req;
  prepareRequest(req, api::MKE_REQUEST_GET_FRAME);
  req.params.getframe_params.frame_type = frame_type;
  bus_->sendRequest(req);
  
  api::MkEReply rep;
  waitForReply(req, rep, output.data_collector_);
  checkReply(rep);
  
  output.params = rep.params.frame_params;

}

/* -------------------------------------------------------------------------- */

const Frame * Client::getPushedFrame()

{
  if(!frame_push_req_)
    return nullptr;
  
  if(frame_received_.empty())
    {
      // receive next frame
      api::MkEReply rep;
      waitForReply(*frame_push_req_, rep);
      
      return getPushedFrame();
    }
  else
    {
      const Frame * ret = frame_received_.front();
      frame_received_.pop();
      
      return ret;
    }
}

/* -------------------------------------------------------------------------- */

void Client::releasePushedFrame(const Frame * buffer_item)

{
  frame_free_.push(const_cast<Frame *>(buffer_item));
}


/* -------------------------------------------------------------------------- */
