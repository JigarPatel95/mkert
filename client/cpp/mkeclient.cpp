/*
 * Client - wraps communication protocol with the MagikEye sensor
 *          independently to a bus type
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#include <iostream>
#include <fstream>
#include <string>
#include <chrono>

/* -------------------------------------------------------------------------- */

#include "reserved_api/reserved_client.h"
#include "client.h"
#include "tcpbus.h"

/* -------------------------------------------------------------------------- */

using namespace mke;
using namespace mke::cli;

using namespace std::chrono;

/* -------------------------------------------------------------------------- */

std::string             tcp_host;
std::string             tcp_port;

std::string             action;

bool                    verbose = false;
bool                    help = false;

/* -------------------------------------------------------------------------- */

void printUsage(const char * app_name)

{
  std::cerr << "Usage:" << std::endl;
  std::cerr << "\t" << app_name << " [-vh] HOST PORT ACTION [additional arguments]" << std::endl;
  std::cerr << "Supported actions:" << std::endl;
  std::cerr << "\tGET_STATE, SET_STATE, GET_FIRMWARE, GET_DEVICE" << std::endl;
}

/* -------------------------------------------------------------------------- */

int parseCmdArgs(int argc, char ** argv)

{
  if(argc < 4)
    return -1;
  
  int a = 1;
  
  std::string * params[] = { &tcp_host, &tcp_port, &action, nullptr };
  std::string ** p = params;
  
  while(*p && a < argc)
    {
      if(argv[a][0] != '-')
        *(*p++) = argv[a++];
      else
       {
         std::string sarg = argv[a++];
         
         if(sarg == "-v" || sarg == "--verbose")
           verbose = true;
         else if(sarg == "-h" || sarg == "--help")
           help = true;
         else
           throw std::runtime_error("Unknown command-line argument");
       }
    }
 
  return a;
}

/* -------------------------------------------------------------------------- */

template <typename MkEFrameItemType>
void getAndSaveFrame(Client & client, api::MkEFrameType frame_type, 
                     const char * out_file = nullptr, api::MkEReply_Frame * out_params = nullptr)

{
  Frame frame;
  client.getFrame(frame_type, frame);
  if(out_params)
     *out_params = frame.getParams();

  Array<MkEFrameItemType> pts = frame.getPtsAs<MkEFrameItemType>();
 
  if(out_file)
    {
      std::ofstream ofs(out_file);
      ofs.write(reinterpret_cast<const char *>(pts.begin()),
                sizeof(MkEFrameItemType) * frame.size());
      ofs.close();
    }
  else
    {
      for(const MkEFrameItemType * it = pts.begin(); it != pts.end(); ++it)
        std::cout << "pt." << it->uid << ": " 
            << it->x << ";" << it->y << ";" << it->z << std::endl;
    }
}

/* -------------------------------------------------------------------------- */

template <typename MkEFrameItemType>
void inlinePrint(const Array<MkEFrameItemType> & pts)
{
  for(const MkEFrameItemType * it = pts.begin(); it != pts.end(); ++it)
    std::cout << it->uid << "=" << it->x << ";" << it->y << ";" << it->z << " ";  
}

/* -------------------------------------------------------------------------- */

int main(int argc, char ** argv)

{
  try
  {
    // check input arguments
  
    int next_arg = parseCmdArgs(argc, argv);
    if(next_arg < 0 || help)
    {
      if(!help)
        std::cerr << "Invalid number of arguments." << std::endl;
      
      printUsage(argv[0]);
      return 1;
    }

    // prepare client
    
    TcpBus bus(tcp_host.c_str(), tcp_port.c_str(), verbose);
    ReservedClient client(&bus);
    
    // make an action
    
    if(action == "GET_STATE")
      {
        std::cout << "Current state: " << client.getState() << std::endl;
      }
    else if(action == "SET_STATE")
      {
        if (argc < (next_arg+1))
          throw std::runtime_error("Missing argument (new_state) for action SET_STATE");
        
        api::MkEStateType new_state = api::MkEStateType(atoi(argv[next_arg++]));
        
        client.setState(new_state);
      }
    else if(action == "GET_FIRMWARE")
      {
        api::MkEReply_FirmwareInfo fwinfo = client.getFirmwareInfo();
        
        std::cout << "Firmware version:\t" << 
          unsigned(fwinfo.fw_ver_major) << "." <<
          unsigned(fwinfo.fw_ver_minor) << "." <<
          unsigned(fwinfo.fw_ver_patch) << std::endl;
        std::cout << "System version:\t\t" << 
          unsigned(fwinfo.sys_ver_major) << "." <<
          unsigned(fwinfo.sys_ver_minor) << "." <<
          unsigned(fwinfo.sys_ver_patch) << std::endl;
        std::cout << "Build stamp:\t\t" << fwinfo.posix_time << std::endl;
        std::cout << "GIT commit:\t\t" << std::hex << fwinfo.git_commit << std::endl;
        
        if(fwinfo.git_commit != 0)
          std::cout << "!! On the device running UNOFFICIAL version of MKERT !!" << std::endl;
      }
    else if(action == "GET_DEVICE")
      {
        api::MkEReply_DeviceInfo devinfo = client.getDeviceInfo();
        
        std::cout << "Unit ID: " << devinfo.unit_id << std::endl;
      }
    else if(action == "TERMINATE")
      {
        if (argc < (next_arg+1))
          throw std::runtime_error("Missing argument (terminate_method) for action TERMINATE");
        
        api::MkETerminateMethodType term_type = 
                          api::MkETerminateMethodType(atoi(argv[next_arg++]));
                          
        client.terminate(term_type);
      }
    else if(action == "FRAME_PUSH")
      {
        if (argc < (next_arg+2))
          throw std::runtime_error("Missing arguments frame_type and timeout for action FRAME_PUSH");
        
        api::MkEFrameType frame_type = api::MkEFrameType(atoi(argv[next_arg++]));
        float timeout = atof(argv[next_arg++])*1000;
        
        switch(frame_type)
        {
          case api::MKE_FRAME_TYPE_1:
          case api::MKE_FRAME_TYPE_2:
          case api::MKE_FRAME_TYPE_RESERVED_1:
          case api::MKE_FRAME_TYPE_RESERVED_2:
            break;
          default:
            throw std::runtime_error("Unknown frame_type");
        }            

        client.setState(api::MKE_STATE_DEPTH_SENSOR);
        
        auto start = high_resolution_clock::now();
        client.startFramePush(frame_type);
        
        while(duration_cast<milliseconds>(high_resolution_clock::now() - start).count() < timeout)
          {
            const Frame * frame = client.getPushedFrame();
  
            const api::MkEReply_Frame & frame_params = frame->getParams();
            
            std::cout << "Frame type:\t" << frame_params.frame_type << std::endl;
            std::cout << "Data type:\t" << frame_params.data_type << std::endl;
            std::cout << "Sequence no.:\t" << frame_params.seqn << std::endl;
            std::cout << "Timer:\t\t" << frame_params.timer << std::endl;
            
            std::cout << "Items:\t\t";
            if(frame_type == api::MKE_FRAME_TYPE_1)
              inlinePrint<api::MkEFrameItem1>(frame->getPtsAs<api::MkEFrameItem1>());
            else if(frame_type == api::MKE_FRAME_TYPE_2)
              inlinePrint<api::MkEFrameItem2>(frame->getPtsAs<api::MkEFrameItem2>());
            else if(frame_type == api::MKE_FRAME_TYPE_RESERVED_1)
              inlinePrint<api::MkEFrameItemReserved1>(frame->getPtsAs<api::MkEFrameItemReserved1>());
            else if(frame_type == api::MKE_FRAME_TYPE_RESERVED_2)
              inlinePrint<api::MkEFrameItemReserved2>(frame->getPtsAs<api::MkEFrameItemReserved2>());
            else
              std::cout << "unknown type";
            std::cout << std::endl;
            
            client.releasePushedFrame(frame);
          }
          
        client.stopFramePush();
        
        client.setState(api::MKE_STATE_IDLE);
      }    
    else if(action == "DEBUG_IMAGES")
      {
        if (argc < (next_arg+2))
          throw std::runtime_error("Missing arguments frame_type and number of images for action DEBUG_IMAGES");
        
        api::MkEFrameType frame_type = api::MkEFrameType(atoi(argv[next_arg++]));
        int num_images = atoi(argv[next_arg++]);
        
        switch(frame_type)
        {
          case api::MKE_FRAME_TYPE_1:
          case api::MKE_FRAME_TYPE_2:
          case api::MKE_FRAME_TYPE_RESERVED_1:
          case api::MKE_FRAME_TYPE_RESERVED_2:
            break;
          default:
            throw std::runtime_error("Unknown frame_type");
        }            

        client.setState(api::MKE_STATE_DEPTH_SENSOR);
        
        DebugImages dbg;
        client.getDebugImages(frame_type, num_images, dbg);
        
        for(int i = 0; i < dbg.size(); ++i)
          {
            Image im = dbg.getImage(i);
            const api::MkEReply_Frame & frame_params = dbg.getParams(i);
            
            std::cout << "Frame type:\t" << frame_params.frame_type << std::endl;
            std::cout << "Data type:\t" << frame_params.data_type << std::endl;
            std::cout << "Sequence no.:\t" << frame_params.seqn << std::endl;
            std::cout << "Timer:\t\t" << frame_params.timer << std::endl;
            
            std::cout << "Items:\t\t";
            if(frame_type == api::MKE_FRAME_TYPE_1)
              inlinePrint<api::MkEFrameItem1>(dbg.getPtsAs<api::MkEFrameItem1>(i));
            else if(frame_type == api::MKE_FRAME_TYPE_2)
              inlinePrint<api::MkEFrameItem2>(dbg.getPtsAs<api::MkEFrameItem2>(i));
            else if(frame_type == api::MKE_FRAME_TYPE_RESERVED_1)
              inlinePrint<api::MkEFrameItemReserved1>(dbg.getPtsAs<api::MkEFrameItemReserved1>(i));
            else if(frame_type == api::MKE_FRAME_TYPE_RESERVED_2)
              inlinePrint<api::MkEFrameItemReserved2>(dbg.getPtsAs<api::MkEFrameItemReserved2>(i));
            else
              std::cout << "unknown type";
            std::cout << std::endl;
          }
        
        client.setState(api::MKE_STATE_IDLE);
      }   
    else if(action == "GET_IMAGE")
      {
        if (argc < (next_arg+1))
          throw std::runtime_error("Missing arguments number of images to average for action GET_IMAGE");
        
        int num_images = atoi(argv[next_arg++]);
        
        client.setState(api::MKE_STATE_SERVICE);
        
        MemImage im;
        client.getImage(im, num_images);

        std::cout << "Width:\t" << im.w << std::endl;
        std::cout << "Height:\t" << im.h << std::endl;
        std::cout << "Stride:\t" << im.stride << std::endl;
        
        client.setState(api::MKE_STATE_IDLE);
      }         
      else if(action == "GET_FRAME")
      {
        if (argc < (next_arg+1))
          throw std::runtime_error("Missing argument frame_type for action GET_FRAME");
        
        api::MkEFrameType frame_type = api::MkEFrameType(atoi(argv[next_arg++]));
        const char * out_name = argc < (next_arg+1) ? nullptr : argv[next_arg++];
        
        api::MkEReply_Frame frame_params;
        switch(frame_type)
        {
          case api::MKE_FRAME_TYPE_1:
            getAndSaveFrame<api::MkEFrameItem1>(client, frame_type, out_name, 
                                                &frame_params);
            break;
          case api::MKE_FRAME_TYPE_2:
            getAndSaveFrame<api::MkEFrameItem2>(client, frame_type, out_name, 
                                                &frame_params);
            break;
          case api::MKE_FRAME_TYPE_RESERVED_1:
            getAndSaveFrame<api::MkEFrameItemReserved1>(client, frame_type, 
                                                        out_name, &frame_params);
            break;
          case api::MKE_FRAME_TYPE_RESERVED_2:
            getAndSaveFrame<api::MkEFrameItemReserved2>(client, frame_type, 
                                                        out_name, &frame_params);
            break;
          default:
            throw std::runtime_error("Unknown frame_type");
        }
        
        std::cout << "Frame type:\t" << frame_params.frame_type << std::endl;
        std::cout << "Data type:\t" << frame_params.data_type << std::endl;
        std::cout << "Sequence no.:\t" << frame_params.seqn << std::endl;
        std::cout << "Timer:\t\t" << frame_params.timer << std::endl;
        std::cout << "Frame saved to file " << out_name << "." << std::endl;
      }
    else
      {
        throw std::runtime_error("Unknown action.");
      }
  }
  catch(std::exception & ex)
  {
    std::cerr << "ERROR: " << ex.what() << std::endl;
    return -1;
  }
  
  return 0;
}

/* -------------------------------------------------------------------------- */
