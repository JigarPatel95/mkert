/*
 * Client - wraps communication protocol with the MagikEye sensor
 *          independently to a bus type
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#include "tcpbus.h"
#include <iostream>

/* -------------------------------------------------------------------------- */

using namespace mke;
using namespace mke::cli;
using boost::asio::ip::tcp;

/* -------------------------------------------------------------------------- */

TcpBus::TcpBus(const char * host, const char * port, bool verbose) :
  BaseBus(verbose),
  host_(host),
  port_(port),
  socket_(io_service_)

{
  // resolve host name
  
  tcp::resolver resolver(io_service_);
  tcp::resolver::query query(host, port);
  tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
  
  // connect socket
  
  boost::asio::connect(socket_, endpoint_iterator);  
}

/* -------------------------------------------------------------------------- */

void TcpBus::send(const char * data, size_t size)

{
  socket_.write_some(boost::asio::buffer(data, size)); 
}

/* -------------------------------------------------------------------------- */

size_t TcpBus::recv(char * buff, size_t buff_size)

{
  boost::system::error_code error;
  size_t len = socket_.read_some(boost::asio::buffer(buff, buff_size), error);  
  
  if(error)
    throw std::runtime_error(error.message());
  
  return len;
}

/* -------------------------------------------------------------------------- */
