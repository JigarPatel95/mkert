/*
 * BaseBus - wraps communication bus for the Client class
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef _CLI_BASEBUS_H_
#define _CLI_BASEBUS_H_

/* -------------------------------------------------------------------------- */

#include "mkeapi.h"

#include <vector>
#include <cstring>
#include <iostream>
#include <iomanip>
#include <cstdint>
#include <cstdio>

/* -------------------------------------------------------------------------- */

namespace mke {
namespace cli { 

/* -------------------------------------------------------------------------- */

class BaseBus

{
protected:
  bool verbose_;
  
  virtual void send(const char * data, size_t num) = 0;
  
  virtual size_t recv(char * buff, size_t buff_size) = 0;

  void printDatagram(const char * data, size_t size, size_t ascii, const char * prefix)

  {
    std::printf("%s", prefix);
    
    const char * p = data;
    size_t i = 0;
    for(; i < ascii; ++i, ++p)
      std::printf("%c", *p);
    
    for(; i < size; ++i, ++p)
      std::printf(" %02X", uint8_t(*p));
    
    std::printf("\n");
  }
  
public:
  
  BaseBus(bool verbose = false) :
    verbose_(verbose)
  {};
  
  ~BaseBus() {};
  
  /**
   * @brief Send request with payload to the bus
   * 
   * @param req request will be sent
   */
  inline void sendRequest(const api::MkERequest & req)
  {
    if(verbose_)
      printDatagram(reinterpret_cast<const char *>(&req), sizeof(api::MkERequest), 12, "<< ");
    send(reinterpret_cast<const char *>(&req), sizeof(api::MkERequest));
  }
  
  /**
   * @brief Send request with payload to the bus
   * 
   * @param req request will be sent
   */
  inline void sendPayload(const char * payload, size_t size)
  {
    if(verbose_)
      std::printf("<< PAYLOAD [%lu]\n", size);
    
    send(payload, size);
  }
  
  /**
   * @brief Receive next reply from the bus
   * 
   * @return next received reply
   */
  inline void recvReply(api::MkEReply & reply)
  {
    char * data_ptr = reinterpret_cast<char *>(&reply);
    size_t to_read = sizeof(api::MkEReply);
    size_t len = 0;
    
    while(to_read > 0)
      {
        len = recv(data_ptr, to_read);
        to_read -= len;
        data_ptr += len;
      }

    if(verbose_)
      printDatagram(reinterpret_cast<const char *>(&reply), sizeof(api::MkEReply), 16, ">> ");
  }
  
  /**
   * @brief Send request with payload to the bus
   * 
   * @param req request will be sent
   */
  inline void recvPayload(char * buff, size_t payload_size)
  {
    char * data_ptr = buff;
    size_t to_read = payload_size;
    size_t len = 0;
    
    while(to_read > 0)
      {
        len = recv(data_ptr, to_read);
        if(len == 0)
          throw std::runtime_error("Closed connection");
        to_read -= len;
        data_ptr += len;
      }    
      
    if(verbose_)
      {
        std::printf(">> PAYLOAD [%lu] ..", payload_size);
        for(size_t i = 0 ; i < std::min<size_t>(32, payload_size); ++i)
          std::printf(" %02x", uint8_t(*(buff+i)));
        std::printf("\n");
      }
    
  }
};

/* -------------------------------------------------------------------------- */

}  // end of mke::cli namespace
}  // end of mke namespace

/* -------------------------------------------------------------------------- */

#endif // _CLI_BASEBUS_H_

/* -------------------------------------------------------------------------- */
