#!/bin/python3
#
# client_pushframes - start pushing frames from device and show them
# 
# Copyright (c) 2017, Magik-Eye s.r.o., Prague
# author: Ondra Fisar
#
# ------------------------------------------------------------------------------

# imports

import sys
import struct
import time

from mkeclient import MkeClient

import numpy as np

import matplotlib
#matplotlib.use("Qt5Agg")    # you can use diferrent backend for rendering
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# ------------------------------------------------------------------------------

# convert data frame into matrix with id, x, y, z (coordinates), lid, did

def convert_frame_to_matrix(frame):
  
  ftype   = frame['frame_type']
  num     = frame['num_data']

  if ftype < 1 or ftype > 2:
    raise RuntimeError("Undefined frame_type")

  # get size of given frames
  
  pt_size = { 1: 8, 2: 12 }.get(ftype)
  dim     = { 1: 4, 2: 6 }.get(ftype)
  
  data = np.zeros((dim, num))
  
  for i in range(num):
    loc = frame['payload'][i*pt_size:(i+1)*pt_size]
    
    data[0,i] = struct.unpack('<H', loc[0:2])[0]
    data[1,i] = struct.unpack('<h', loc[2:4])[0]
    data[2,i] = struct.unpack('<h', loc[4:6])[0]
    data[3,i] = struct.unpack('<h', loc[6:8])[0]
    
    if dim > 4:
      data[4,i] = struct.unpack('<H', loc[8:10])[0]
      data[5,i] = struct.unpack('<H', loc[10:12])[0]
  
  return data

# ------------------------------------------------------------------------------

# check arguments

if len(sys.argv) < 2:
  print("Bad number arguments.\n Usage %s IP port [--verbose]" % sys.argv[0])
  exit(1)

host = sys.argv[1]
port = int(sys.argv[2])

# ------------------------------------------------------------------------------

client = None

try:  
  client = MkeClient(host, port, '--verbose' in sys.argv)

  # go to state depth sensing

  state = client.get_state() 

  if state != MkeClient.STATE_DEPTH_SENSOR:

    print("Invalid state, let's change it to DEPTH_SENSOR")
    
    # firstly set state to IDLE
    
    if state != MkeClient.STATE_IDLE:
      client.set_state(MkeClient.STATE_IDLE)
      
    # then change state to DEPTH_SENSOR
    
    client.set_state(MkeClient.STATE_DEPTH_SENSOR)

  else:
    
    print("Already in correct state.")

  # main body ------------------------------------------------------------------

  # prepare figure
  
  fig = plt.figure()
  ax = fig.add_subplot(111, projection='3d')
  plt.ioff()
  fig.show()

  # prepare variables

  start_time = time.time()          # start of capturing
  timeout = 10                      # when should be stop_frame_push called
  num = 0                           # number of received images
  frame_type = 1                    # used frame type
  
  # start pushing frames
  
  start_seq_id = client.start_frame_push(frame_type)
  stop_seq_id = None

  while True:
    
    # send stop push after num frames
    
    if stop_seq_id is None and (time.time() - start_time) >= timeout:  
      stop_seq_id = client.stop_frame_push()

    # get next frame from the bus
    
    frame = client.get_pushed_frame(start_seq_id, stop_seq_id)
    
    # None denotes no more data
    
    if frame is None:
      print("Correctly finished loop")
      
      # all replies should be processed
      break
    
    # convert data and plot them
    
    data = convert_frame_to_matrix(frame)
    ax.cla()
    ax.plot(data[1,:], data[2,:], data[3,:], 'b.')
    plt.pause(0.001)
    
    num += 1
  
  
  print("Average displayed FPS: %.2f" % (num/(time.time()-start_time)))

  print("Correct termination");

except Exception as e:
  print("An error occured: ", str(e))
finally:
  if client:
    client.set_state(MkeClient.STATE_IDLE)

# ------------------------------------------------------------------------------
