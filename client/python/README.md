# Python Client for MkE API #

This directory contains several Python (version 3.x) examples on how to use MkE API.

> **Note:** To satisfy all third party python packages dependencies please run the following command in the client/python/ directory:
>
> `pip install -r requirements.txt`

## Overview ##

There are several examples that demostrate how to use MKE API for communication with the sensor.
All examples required an IP address or a hostname as the first parameter and the port number as the second parameter:

`python3 demo.py <IP address or hostname> [port number = default 8888]`

All examples could be divided into two categories:

## Demos with RAW communication ##

These examples demostrate how to communicate with the sensor on low level (RAW).
This is the best way to understand the client/sensor communication.

List of demos:

### 1. raw_getstate.py <hostname> [port = 8888] ###

  - Reads current sensor state.

>  **Note:** Script prints request and related reply datagrams in RAW format.


### 2. raw_setstate.py <hostname> [port = 8888] ###

  - Checks currect sensor state.
  - Switches to `STATE_DEPTH_SENSOR`.
  - Waits a while.
  - Switches back to `STATE_IDLE`.

>  **Note:** All request and related reply datagrams are immediately printed in RAW format.


### 3. raw_getframe.py <hostname> [port = 8888] ###

  - Checks currect sensor state.
  - Switches to `STATE_DEPTH_SENSOR`.
  - Captures one frame from the sensor.
  - Switches back to `STATE_IDLE`.

>  **Note:** All request and reply datagrams are immediately printed in RAW format.

## Demos with classes ##

These examples use `MkeClient` class that provides all the funcionality of MKE API.
MkeClient is implemented in the file `mkeclient.py`.

List of demos:

### 1. client_getframe.py <hostname> <port> [--verbose] ###

  - Checks currect sensor state.
  - Switches to `STATE_DEPTH_SENSOR`.
  - Captures one frame from the sensor.
  - Switches back to `STATE_IDLE`.

>  **Note:** Use `--verbose` switch to print all request and reply diagrams in colored RAW format.


### 2. client_pushframes.py <hostname> <port> [--verbose] ###

  - Checks currect sensor state.
  - Switches to `STATE_DEPTH_SENSOR`.
  - Capture one frame from the sensor.
  - Switches back to `STATE_IDLE`.

>  **Note:** Use `--verbose` switch to print all request and reply diagrams in colored RAW format.
