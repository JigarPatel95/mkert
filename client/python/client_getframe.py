#!/bin/python3
#
# client_getframe - get frame by mke client
# 
# Copyright (c) 2017, Magik-Eye s.r.o., Prague
# author: Ondra Fisar
#
# ------------------------------------------------------------------------------

# imports

import sys
import struct

from mkeclient import MkeClient

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# ------------------------------------------------------------------------------

# show given frame

def show_frame(frame):
  
  if frame['frame_type'] == 0:
    raise RuntimeError("Undefined frame_type")
  
  ids = []
  xs = []
  ys = []
  zs = []
  
  for i in range(frame['num_data']):
    sz = { 1: 8, 2: 12 }.get(frame['frame_type'])
    loc = frame['payload'][i*sz:(i+1)*sz]
    
    ids.append(struct.unpack('<H', loc[0:2])[0])
    xs.append(struct.unpack('<h', loc[2:4])[0])
    ys.append(struct.unpack('<h', loc[4:6])[0])
    zs.append(struct.unpack('<h', loc[6:8])[0])
    
  fig = plt.figure()
  ax = fig.add_subplot(111, projection='3d')
  
  ax.plot(xs, ys, zs, 'b.')
  plt.show()

# ------------------------------------------------------------------------------

# check arguments

if len(sys.argv) < 2:
  print("Bad number arguments.\n Usage %s IP port [--verbose]" % sys.argv[0])
  exit(1)

host = sys.argv[1]
port = int(sys.argv[2])

# ------------------------------------------------------------------------------

client = None

try:  
  client = MkeClient(host, port, '--verbose' in sys.argv)

  # go to state depth sensing

  state = client.get_state() 
  print("Current state: %d" % state);

  if state != MkeClient.STATE_DEPTH_SENSOR:

    print("Invalid state, let's change it")
    
    # firstly set state to IDLE
    
    if state != MkeClient.STATE_IDLE:
      client.set_state(MkeClient.STATE_IDLE)
      
    # then change state to DEPTH_SENSOR
    
    client.set_state(MkeClient.STATE_DEPTH_SENSOR)

    # now the device is in DEPTH_SENSOR state

    print("Current state: %d" % client.get_state())

  else:
    
    print("Already in correct state.")


  # get one frame

  frame = client.get_frame(1);
  show_frame(frame)

  print("Correct termination");

except Exception as e:
  print("An error occured: %s" % str(e))
finally:
  if client:
      client.set_state(MkeClient.STATE_IDLE)

# ------------------------------------------------------------------------------
