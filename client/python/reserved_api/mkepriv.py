#!/bin/python 
#
# mkepriv - class for TCP communication with sensor in priviledge mode
# 
# Copyright (c) 2017, Magik-Eye s.r.o., Prague
# author: Ondra Fisar
#
# ------------------------------------------------------------------------------

# imports

import sys
import os
import struct
import math
from numpy import reshape

# nasty but works
sys.path.append(os.path.dirname(__file__) + "/../")

from mkeclient import MkeClient

# ------------------------------------------------------------------------------

# read PGM

def read_pgm(data):
  
  pos = data.find(b'\n');
  parts = data[0:pos].decode().split(' ');

  assert(parts[0] == 'P5')
  w = int(parts[1])
  h = int(parts[2])
  
  return reshape(bytearray(data[pos+1:]), (h, w)) #.transpose()

# ------------------------------------------------------------------------------

def fromSci(scival):
  return scival[0] * math.pow(2, scival[1])

# ------------------------------------------------------------------------------

def toSci(value):
  exp = math.floor(math.log2(abs(value))-6) if value != 0 else 0;
  mantisa = round(value/math.pow(2,exp))
  return (mantisa, exp)

# ------------------------------------------------------------------------------

class MkePrivClient(MkeClient):
  
  # PUBLIC VARIABLES =======================================
  
  STATE_SERVICE         = 10
  
  TERMINATE_RESTART     = 3
  TERMINATE_EXIT        = 4

  # PRIVATES VARIABLES =====================================

  # constatants (API calls) --------------------------------
  
  __START_TIMER         = 1002
  __STOP_TIMER          = 1003
  __STREAM_ON           = 1004
  __STREAM_OFF          = 1005
  __GET_SHUTTER         = 1006
  __SET_SHUTTER         = 1007
  __GET_GAIN            = 1008
  __SET_GAIN            = 1009
  __GET_LASER           = 1010
  __SET_LASER           = 1011
  __GET_IMAGE           = 1012
  __GET_DEBUG_IMAGES    = 1013
  __GET_STATS           = 1014
  __GET_RESERVED_INFO   = 1015
  __GET_PROFILE         = 1016
  __SET_PROFILE         = 2002
    
  # constructor - connect to server ------------------------
  
  def __init__(self, host, port, verbose = False):
    
    super(MkePrivClient, self).__init__(host, port, verbose)
    
   
  # get frame item size ------------------------------------
  
  def _get_frame_item_size(self, frame_type):

    if frame_type <= 1000:
      return super()._get_frame_item_size(frame_type)
      
    assert(frame_type <= 1002)
    
    return { 1001: 20, 1002: 36 }.get(frame_type)
  

  # parse frame item ---------------------------------------
  
  def _parse_frame_item(self, item_data, frame_type):
    
    ret = super()._parse_frame_item(item_data, min(frame_type, 2));

    if frame_type >= 1001:
      ret['u'] = struct.unpack('<I', item_data[12:16])[0]
      ret['v'] = struct.unpack('<I', item_data[16:20])[0]
      ret['u'] = float(ret['u'])/256/256
      ret['v'] = float(ret['v'])/256/256
    
    if frame_type >= 1002:
      ret['det']    = struct.unpack('<I', item_data[20:24])[0]
      ret['bwidth'] = struct.unpack('<I', item_data[24:28])[0]
      ret['sigma']  = struct.unpack('<I', item_data[28:32])[0]
      ret['resp']   = struct.unpack('<I', item_data[32:36])[0]
      
    assert(frame_type <= 2 or (frame_type >= 1001 and frame_type <= 1002))
      
    return ret
  
  # send command with dynamic payload and check result -----
  
  def _send_dyncmd(self, cmd, dyn_params = None, payload = bytearray()):

    assert(cmd >= 2000 and cmd < 3000)
    
    if dyn_params is None:
      dyn_params = bytearray(4)
      
    params = bytearray(struct.pack('<I', len(payload))) + dyn_params
    seq_id = self._send_mke(cmd, params)

    self._send_payload(payload)
      
    ret = self._recv_mke()

    assert(cmd == ret['ret_cmd'] and seq_id == ret['seq_id'])
    
    return ret
  
  # PUBLIC CALLS ===========================================  
    
  # get debug images
  
  def get_debug_images(self, num_images, frame_type, imFormat = 'PGM'):
    
    iformat = 0
    if imFormat == 'PNG':
      iformat = 1
    params = struct.pack('<IHB', num_images, frame_type, iformat) + bytearray(1)
    ret = self._send_cmd(MkePrivClient.__GET_DEBUG_IMAGES, params)
    
    if ret['ret_code'] != MkeClient.STATUS_OK:
      raise RuntimeError("Unexpected reply with code %d" % ret['ret_code'])

    ret_num = struct.unpack('<I', ret['params'][0:4])[0]
    
    assert(ret_num == num_images)
    
    offset = 0
    out = []
    for i in range(ret_num):
      (timer, frame_id, ibytes, fbytes, frame_type, frame_num_data, pattern) = struct.unpack('<QIIIHHH', ret['payload'][offset:(offset+26)])
      offset += 32
      idata = ret['payload'][offset:(offset+ibytes)]
      offset += ibytes
      fdata = ret['payload'][offset:(offset+fbytes)]
      offset += fbytes
      
      out.append({ 'timer'      : timer, 
                   'image_id'   : frame_id,
                   'frame_type' : frame_type,
                   'frame_num_data': frame_num_data,
                   'image_data' : idata,
                   'frame_data' : fdata,
                   'pattern'    : pattern })

    return out
  

  # set laser pattern
  
  def set_laser(self, pattern, duration = 0, offset = 0):
    
    scidur = toSci(duration)
    scioff = toSci(offset)
    params = struct.pack('<Hbbbb', pattern, scidur[0], scidur[1], scioff[0], scioff[1]) + bytearray(2)
    ret = self._send_cmd(MkePrivClient.__SET_LASER, params)
    
    if ret['ret_code'] != MkeClient.STATUS_OK:
      raise RuntimeError("Unexpected reply with code %d" % ret['ret_code'])

  # get lasers
  
  def get_laser(self):
    
    ret = self._send_cmd(MkePrivClient.__GET_LASER)
    
    if ret['ret_code'] != MkeClient.STATUS_OK:
      raise RuntimeError("Unexpected reply with code %d" % ret['ret_code'])
    
    (pattern, durman, durexp, offman, offexp) = struct.unpack('<Hbbbb', ret['params'][0:6])
    return { 'pattern': pattern,
             'duration': fromSci((durman, durexp)),
             'offset': fromSci((offman, offexp))}


  # set shutter speed
  
  def set_shutter(self, exposure_time):
    
    params = struct.pack('<I', exposure_time) + bytearray(4)
    ret = self._send_cmd(MkePrivClient.__SET_SHUTTER, params)
    
    if ret['ret_code'] != MkeClient.STATUS_OK:
      raise RuntimeError("Unexpected reply with code %d" % ret['ret_code'])


  # get shutter speed
  
  def get_shutter(self):
    
    ret = self._send_cmd(MkePrivClient.__GET_SHUTTER)
    
    if ret['ret_code'] != MkeClient.STATUS_OK:
      raise RuntimeError("Unexpected reply with code %d" % ret['ret_code'])
    
    return struct.unpack('<I', ret['params'][0:4])[0]


  # get gain
  
  def get_gain(self):
    
    ret = self._send_cmd(MkePrivClient.__GET_GAIN)
    
    if ret['ret_code'] != MkeClient.STATUS_OK:
      raise RuntimeError("Unexpected reply with code %d" % ret['ret_code'])
    
    items = struct.unpack('<II', ret['params'][0:8])
    
    return { 'analog': items[0],
             'digital': items[1]}


  def set_gain(self, iso):
    
    params = struct.pack('<I', iso) + bytearray(4)
    ret = self._send_cmd(MkePrivClient.__SET_GAIN, params)
    
    if ret['ret_code'] != MkeClient.STATUS_OK:
      raise RuntimeError("Unexpected reply with code %d" % ret['ret_code'])


  # get debug image - averaged
  
  def get_image(self, num_images, turnOffLasers = False, imFormat = 'PGM'):
    
    flags = 0;
    iformat = 0
    if turnOffLasers:
      flags = flags | 1
    if imFormat == 'PNG':
      iformat = 1
    params = struct.pack('<IBB', num_images, flags, iformat) + bytearray(2)
    ret = self._send_cmd(MkePrivClient.__GET_IMAGE, params)
    
    if ret['ret_code'] != MkeClient.STATUS_OK:
      raise RuntimeError("Unexpected reply with code %d" % ret['ret_code'])
    
    (timer, image_id) = struct.unpack('<QI', ret['params'][0:12])
    
    return { 'timer'    : timer,
             'image_id' : image_id,
             'data'     : ret['payload'] }

  # get reserved info
  
  def get_reserved_info(self):
    
    ret = self._send_cmd(MkePrivClient.__GET_RESERVED_INFO)
    
    if ret['ret_code'] != MkeClient.STATUS_OK:
      raise RuntimeError("Unexpected reply with code %d" % ret['ret_code'])

    return

  # get current profile
  
  def get_profile(self):
    
    ret = self._send_cmd(MkePrivClient.__GET_PROFILE)
    
    if ret['ret_code'] != MkeClient.STATUS_OK:
      raise RuntimeError("Unexpected reply with code %d" % ret['ret_code'])
    
    return ret['payload'].decode();
  
  # get current profile
  
  def set_profile(self, profile):
    
    ret = self._send_dyncmd(MkePrivClient.__SET_PROFILE, None, bytes(profile, 'ascii'))
    
    if ret['ret_code'] != MkeClient.STATUS_OK:
      raise RuntimeError("Unexpected reply with code %d" % ret['ret_code'])
    
    return
  
# ------------------------------------------------------------------------------
