#!/bin/python 
#
# basic_getframe - get frame by mke client
# 
# Copyright (c) 2017, Magik-Eye s.r.o., Prague
# author: Ondra Fisar
#
# ------------------------------------------------------------------------------

# imports

import sys
import struct
import time
import traceback
import os
import shutil

from mkepriv import MkePrivClient, read_pgm
import matplotlib.pyplot as plt
import numpy as np

# ------------------------------------------------------------------------------

def find_closest2(im, templates):
  ds = {}
  for t in templates:
    ds[t['pattern']] = []
    
  for r in range(im.shape[0]):
    mn = float('inf')
    midx = None
    dists = []
    for t in templates:
      dist = np.sum(np.abs(im[r,:].astype(int) - t['im'][r,:].astype(int)))
      dists.append(dist)
      if dist < mn:
        mn = dist
        midx = t['pattern']
    
    if mn/np.partition(dists,2)[2] < 0.8:
      ds[midx].append(mn)

  mx = 0
  midx = None
  cnt = 0
  for t in templates:
    num = len(ds[t['pattern']])
    cnt += num
    
    if num > mx:
      mx = num
      midx = t['pattern']
      
  return (midx, mx);

def find_closest(im, templates):
  mn = float('inf')
  midx = None
  vals = []
  for t in templates:
    val = np.sum(np.abs(im.astype(float) - t['im'].astype(float)))
    vals.append(val)
    if val < mn:
      mn = val
      midx = t['pattern']
      
  return ( midx, 1-mn/np.partition(vals,2)[2] )
  

# ------------------------------------------------------------------------------

# check arguments

if len(sys.argv) < 2:
  print("Bad number arguments.\n Usage %s IP port [--verbose]" % sys.argv[0])
  exit(1)

host = sys.argv[1]
port = int(sys.argv[2])

# ------------------------------------------------------------------------------

try:  
  client = MkePrivClient(host, port, '--verbose' in sys.argv)

  # go to state depth sensing

  state = client.get_state() 
  print("Current state: %d" % state);

  if state != MkePrivClient.STATE_IDLE:

    print("Invalid state, let's change it")
    
    # firstly set state to IDLE
    
    if state != MkePrivClient.STATE_IDLE:
      client.set_state(MkePrivClient.STATE_IDLE)
      
  outdir = '/tmp/analyze'
  if os.path.exists(outdir):
    shutil.rmtree(outdir)
  os.mkdir(outdir)
  
  templates = []
  client.set_state(MkePrivClient.STATE_SERVICE)
  for pat in (0, 5, 10):
    client.set_laser(pat, 0)
    
    out = client.get_image(1)
    out['im'] = read_pgm(out['data'])
    print("mx %d: %d" % (pat, np.max(out['im'])))
    out['pattern'] = pat
    
    with open(outdir + '/pat_%d.pgm' % pat, 'wb+') as f:
      f.write(out['data']);
    
    templates.append(out)
  
  client.set_state(MkePrivClient.STATE_IDLE)
  client.set_state(MkePrivClient.STATE_DEPTH_SENSOR)
  
  while True:
    out = client.get_debug_images(8, 1)
    
    for o in out:
      im = read_pgm(o['image_data'])
      plt.clf()
      plt.imshow(im, cmap=plt.get_cmap('jet'))
      plt.pause(0.1)
      
      (fpat, rate) = find_closest(im, templates)
      
      with open(outdir + '/im_%08d_%d_%03d.pgm' % (o['image_id'], fpat, round(100*rate)), 'wb+') as f:
        f.write(o['image_data'])
        
    time.sleep(30);

  print("Correct termination");

except Exception as e:
  print("An error occured: %s" % str(e))
  traceback.print_exc(file=sys.stdout)
finally:
  client.set_state(MkePrivClient.STATE_IDLE)
  client.set_state(MkePrivClient.STATE_SERVICE)
  client.set_laser(0,0)
  client.set_state(MkePrivClient.STATE_IDLE)

# ------------------------------------------------------------------------------
