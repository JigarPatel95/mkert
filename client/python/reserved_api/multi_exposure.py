#!/bin/python 
#
# multiple exposure saver - ISO / shutter combination 
# 
# Copyright (c) 2017, Magik-Eye s.r.o., Prague
# author: Ondra Fisar
#
# ------------------------------------------------------------------------------

# imports

import sys
import struct
import time
import traceback
import os
import subprocess
import shutil

from mkepriv import MkePrivClient, read_pgm

# check arguments

if len(sys.argv) < 3:
  print("Bad number arguments.\n Usage %s IP port [--verbose]" % sys.argv[0])
  exit(1)

host = sys.argv[1]
port = int(sys.argv[2])
mkertdir = sys.argv[3]
if len(mkertdir) > 0 and not sys.argv[3].endswith('/'):
  mkertdir += '/'
  
num_imgs = 4
  
# ------------------------------------------------------------------------------

def one_run(outdir, iso, shutter):

  process = None

  try:  
    # start application

    cmd = '%smkert --config=%smkert.ini --Camera.iso=%d --Camera.shutter=%d' % (
        mkertdir, mkertdir, iso, shutter
      )
   
    print(cmd) 
    process = subprocess.Popen(cmd.split())
    time.sleep(5)
    
    client = MkePrivClient(host, port, '--verbose' in sys.argv)

    # go to state depth sensing
    
    client.set_state(MkePrivClient.STATE_DEPTH_SENSOR)
    time.sleep(1)
    
    dbg = client.get_debug_images(num_imgs, 1);

    i = 0
    for o in dbg:
      i += 1
      with open(outdir + '/im_%03d_%05d-%02d+%03d.pgm' % (iso, shutter, i, o['frame_num_data']), 'wb+') as f:
        f.write(o['image_data'])

    print("Correct termination");

  except Exception as e:
    print("An error occured: %s" % str(e))
    traceback.print_exc(file=sys.stdout)
  finally:
    client.set_state(MkePrivClient.STATE_IDLE)
    client.terminate(MkePrivClient.TERMINATE_EXIT)

  if process:
    process.communicate()
    assert(process.wait() == 0)
  
# ------------------------------------------------------------------------------

# prepare structures

dirname = input('Batch ID: ');
outdir = '/tmp/' + dirname;

if not os.path.exists(outdir):
  os.mkdir(outdir)

isos = [ 100, 200, 400, 800 ]
shutters = [ 1000, 2500, 5000, 10000, 15000, 20000, 25000, 35000, 50000 ]

# ------------------------------------------------------------------------------

for iso in isos:
  for shut in shutters:
    one_run(outdir, iso, shut)
