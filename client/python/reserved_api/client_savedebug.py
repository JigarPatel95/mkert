#!/bin/python 
#
# client_savedebug - get n debug images from sensor
# 
# Copyright (c) 2017, Magik-Eye s.r.o., Prague
# author: Ondra Fisar
#
# ------------------------------------------------------------------------------

# imports

import sys
import struct
import time
import traceback
import os

import numpy as np
import matplotlib.pyplot as plt

from mkepriv import MkePrivClient, read_pgm

# ------------------------------------------------------------------------------

# check arguments

if len(sys.argv) != 5:
  print("Bad number arguments.\n Usage %s IP port save_dir num_images" % sys.argv[0])
  exit(1)

host = sys.argv[1]
port = int(sys.argv[2])
save_dir = sys.argv[3]
num_imgs = int(sys.argv[4])

if not save_dir.endswith('/'):
  save_dir += '/'

fig_dir = save_dir + 'fig/'
if not os.path.isdir(fig_dir):
  os.mkdir(fig_dir)

fid = 0

# ------------------------------------------------------------------------------

client = None
pmap = { }
ims = {}
flick_id = {}
flick_lid = {}
fig = plt.figure(figsize=(10, 10))

try:  
  client = MkePrivClient(host, port, True)

  # go to state depth sensing

  if client.set_state(MkePrivClient.STATE_DEPTH_SENSOR) != MkePrivClient.STATUS_OK:
    raise RuntimeError("Unable to set correct state.");

  # wait a while to have stable images

  time.sleep(2)
  
  # get one frame

  with open(save_dir + 'stats.txt', 'w+') as stats:
    
    dbg = client.get_debug_images(num_imgs, 1001, imFormat="PNG");
    client.set_state(MkePrivClient.STATE_IDLE)
    
    for i in range(num_imgs):
      
      imname = 'dbgim_%02d_%02d-%03d.pgm' % (i, dbg[i]['pattern'], dbg[i]['frame_num_data'])
      with open(save_dir + imname, 'wb+') as f:
        f.write(dbg[i]['image_data'])
      pts = client.parse_frame(dbg[i]['frame_data'], 
                                      dbg[i]['frame_num_data'], dbg[i]['frame_type'])
      ims[imname] = read_pgm(dbg[i]['image_data'])
      
      stats.write(imname)
      for p in pts:
        p['imname'] = imname
        if p['uid'] in pmap:
          last = pmap[p['uid']]
          if p['x'] != last['x'] or p['y'] != last['y'] or p['z'] != last['z']:
            print('!! %s != %s' % (str(p), str(last)))

            flick_id[p['uid']] = 1 + (flick_id[p['uid']] if p['uid'] in flick_id else 0)
            flick_lid[p['lid']] = 1 + (flick_lid[p['lid']] if p['lid'] in flick_lid else 0)
            
            fig.clf()
            plt.imshow(np.abs(ims[p['imname']].astype(float) - ims[last['imname']].astype(float)), cmap=plt.get_cmap('jet'))
            plt.axis('image')
            plt.colorbar()
#            plt.plot([p['u'], last['u']], [p['v'], last['v']], 'w-')
            plt.scatter(p['u'], p['v'], s=120, facecolors='none', edgecolors='r')
            plt.title('%f, %f = %f' % (last['u'] - p['u'], last['v'] - p['v'], np.linalg.norm([last['u'] - p['u'], last['v'] - p['v']])))
#            plt.quiver(p['u'], p['v'], last['u'] - p['u'], last['v'] - p['v'], color='w', width=5, scale=1.0, units='xy')
            plt.savefig(fig_dir + '%03d.png' % fid)
            fid += 1
            #plt.waitforbuttonpress()
        
        pmap[p['uid']] = p
        
        stats.write('\t%d:[%dx%dx%d~%dx%d]' % (p['uid'], p['x'], p['y'], p['z'], p['u'], p['v']))
      stats.write('\n')
      
        
      

  print("Correct termination");
  print(flick_id)
  print(flick_lid)

except Exception as e:
  print("An error occured: %s" % str(e))
  traceback.print_exc(file=sys.stdout)
finally:
  if client:
      client.set_state(MkePrivClient.STATE_IDLE)

# ------------------------------------------------------------------------------
