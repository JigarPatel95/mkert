#!/bin/python 
#
# basic_getframe - get frame by mke client
# 
# Copyright (c) 2017, Magik-Eye s.r.o., Prague
# author: Ondra Fisar
#
# ------------------------------------------------------------------------------

# imports

import sys
import struct
import time
import traceback
import os
import shutil
import json

from mkepriv import MkePrivClient, read_pgm

# ------------------------------------------------------------------------------

# check arguments

if len(sys.argv) < 2:
  print("Bad number arguments.\n Usage %s IP port" % sys.argv[0])
  exit(1)

host = sys.argv[1]
port = int(sys.argv[2])

# ------------------------------------------------------------------------------

try:  
  client = MkePrivClient(host, port, '--verbose' in sys.argv)

  # go to state depth sensing
      
  profiles = list()
  profiles.append({"engine": { "video_mode": 4, "width": 1640, "height": 1232 }});
  profiles.append({"engine": { "video_mode": 6, "width": 1280, "height": 720 }});
  profiles.append({"engine": { "video_mode": 2, "width": 3280, "height": 2464 }});
  #profiles.append({"engine": { "width": 1640, "height": 1232 }});
  #profiles.append({"engine": { "width": 1280, "height": 720 }});

  i = 0
  for p in profiles:
    json_profile = json.dumps(p, separators=(',', ':'))
    print(p)
    client.set_profile(json_profile)
    client.set_state(MkePrivClient.STATE_SERVICE)
    
    time.sleep(1)
    out = client.get_image(1)
    
    with open("image%d.png" % i, 'wb+') as f:
      f.write(out['data']);

    client.set_state(MkePrivClient.STATE_IDLE)
    time.sleep(1)
    i += 1
  
  print("Correct termination");

except Exception as e:
  print("An error occured: %s" % str(e))
  traceback.print_exc(file=sys.stdout)
finally:
  client.set_state(MkePrivClient.STATE_IDLE)

# ------------------------------------------------------------------------------
