#!/bin/python 
#
# basic_getframe - get frame by mke client
# 
# Copyright (c) 2017, Magik-Eye s.r.o., Prague
# author: Ondra Fisar
#
# ------------------------------------------------------------------------------

# imports

import sys
import struct
import time
import traceback
import os
import shutil

from mkepriv import MkePrivClient, read_pgm
import matplotlib.pyplot as plt
import numpy as np

# ------------------------------------------------------------------------------

# check arguments

if len(sys.argv) < 3:
  print("Bad number arguments.\n Usage %s IP port output" % sys.argv[0])
  exit(1)

host = sys.argv[1]
port = int(sys.argv[2])
outfile = sys.argv[3]

# ------------------------------------------------------------------------------

try:  
  client = MkePrivClient(host, port, '--verbose' in sys.argv)

  # go to state depth sensing
      
  outdir = '/tmp/analyze'
  if os.path.exists(outdir):
    shutil.rmtree(outdir)
  os.mkdir(outdir)
  
  client.set_state(MkePrivClient.STATE_SERVICE)

  time.sleep(1)
  out = client.get_image(1, imFormat='PNG')
  
  with open(outfile, 'wb+') as f:
    f.write(out['data']);
  
  print("Correct termination");

except Exception as e:
  print("An error occured: %s" % str(e))
  traceback.print_exc(file=sys.stdout)
finally:
  client.set_state(MkePrivClient.STATE_IDLE)

# ------------------------------------------------------------------------------
