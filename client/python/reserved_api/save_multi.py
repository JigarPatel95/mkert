#!/bin/python 
#
# basic_getframe - get frame by mke client
# 
# Copyright (c) 2017, Magik-Eye s.r.o., Prague
# author: Ondra Fisar
#
# ------------------------------------------------------------------------------

# imports

import sys
import struct
import time
import traceback
import os
import shutil

from mkepriv import MkePrivClient, read_pgm
import matplotlib.pyplot as plt
import numpy as np

# ------------------------------------------------------------------------------

# check arguments

if len(sys.argv) < 3:
  print("Bad number arguments.\n Usage %s IP port output" % sys.argv[0])
  exit(1)

host = sys.argv[1]
port = int(sys.argv[2])

# ------------------------------------------------------------------------------

try:  
  client = MkePrivClient(host, port, '--verbose' in sys.argv)

  # go to state depth sensing
      
  client.set_state(MkePrivClient.STATE_SERVICE)

  client.set_laser(15, 0)
  shutters = [ 10, 100, 500, 1000, 5000, 10000, 25000, 50000 ]
  print(client.get_laser())
    
  for s in shutters:
    
    client.set_shutter(s)
    time.sleep(1)
    out = client.get_image(1)
    curr = client.get_shutter()
    
    print(curr)
    
    with open('/tmp/shut_%05d.pgm' % curr, 'wb+') as f:
      f.write(out['data']);
  
  print("Correct termination");

except Exception as e:
  print("An error occured: %s" % str(e))
  traceback.print_exc(file=sys.stdout)
finally:
  client.set_state(MkePrivClient.STATE_IDLE)

# ------------------------------------------------------------------------------
