from PyQt5 import QtCore, QtGui, QtWidgets

import pyqtgraph as pg
import pyqtgraph.opengl as gl

import numpy as np
import struct
import threading
import sys

from mkeclient import MkeClient

# -----------------------------------------------------------------------------

class SimpleViewer(QtWidgets.QMainWindow):
    
    FRAME_TYPE = 1
    MAX_DIST = 2000
    GRID_SCALE = 0.1
    DATA_SCALE = 0.01
    POINT_SIZE = 0.05
    
    model_updated = QtCore.pyqtSignal()
    critical_occured = QtCore.pyqtSignal(str)
    
    def __init__(self):
        super().__init__()
        
        self.__win = self
        self.__model = np.empty((0,4))
        self.__cloud = None
        self.__shared = threading.Lock()
        self.__init_ui()
        
        self.__running = False
        if len(sys.argv) > 1:
            self.__host = (str(sys.argv[1]), 8888)
        else:
            self.__host = self.__get_host()
        if self.__host is None:
            QtWidgets.QApplication.exit()
        
        self.__thread = threading.Thread(target=self.__thread_proc)
        self.__thread.start()
    
    def closeEvent(self, event):
        self.__running = False
        
    def warning(self, msg):
        print("WARNING: " + msg)
    
    def critical(self, msg):
        QtWidgets.QMessageBox.critical(self, "Error", msg);
        self.close();
    
    def __init_view(self):
        view = gl.GLViewWidget()
        zgrid = gl.GLGridItem()
        zgrid.scale(SimpleViewer.GRID_SCALE,
                    SimpleViewer.GRID_SCALE,
                    SimpleViewer.GRID_SCALE)

        view.addItem(zgrid)
        view.opts['fov'] = 1
        view.opts['distance'] = 2000
        view.opts['center'] = QtGui.QVector3D(0,0,5)
        return view
    
    def __init_ui(self):
        self.__view = self.__init_view()
        self.__win.setCentralWidget(self.__view)
        
        self.model_updated.connect(self.render_view)
        self.critical_occured.connect(self.critical)
        
        # self.__init_toolbar()
    
    def __get_host(self):
        host, ok = QtWidgets.QInputDialog.getText(self.__win, 'Connect to sensor',
                                                  'Host')
    
        if ok and not host is None and host != '':
            return (host, 8888)
        
        return None
    
    @staticmethod
    def convert_frame_to_matrix(frame):
        ftype   = frame['frame_type']
        dtype   = frame['data_type']
        num     = frame['num_data']

        if ftype < 1 or ftype > 2:
            raise RuntimeError("Undefined frame_type")
    
        # get size of given frames

        pt_size = { 1: 8, 2: 12 }.get(ftype)
        dim     = { 1: 4, 2: 6 }.get(ftype)

        data = np.zeros((num, dim))

        for i in range(num):
            loc = frame['payload'][i*pt_size:(i+1)*pt_size]

            data[i,0:4] = struct.unpack('<Hhhh', loc[0:8])
        
            if dim > 4:
                data[i,4:] = struct.unpack('<HH', loc[8:12])
        
        if dtype > 0:
            data[:,1:] /= pow(2,dtype)
        return data
        
    def connect_to_sensor(self, host, port):
        self.__client = MkeClient(host, port)
    
        # go to state depth sensing
    
        state = self.__client.get_state() 
    
        if state != MkeClient.STATE_DEPTH_SENSOR:
    
            self.warning("Invalid state, let's change it to DEPTH_SENSOR")
        
            # firstly set state to IDLE

            if state != MkeClient.STATE_IDLE:
                self.__client.set_state(MkeClient.STATE_IDLE)
              
    def start_pushing(self):
        self.__start_seq_id = self.__client.start_frame_push(SimpleViewer.FRAME_TYPE)
        self.__stop_seq_id = None
        
    def stop_pushing(self):
        self.__stop_seq_id = self.__client.stop_frame_push()       
    
    def __thread_proc(self):
        try:
            self.__running = True
            self.connect_to_sensor(self.__host[0], self.__host[1])

            # then change state to DEPTH_SENSOR
        
            self.__client.set_state(MkeClient.STATE_DEPTH_SENSOR)
            
            self.start_pushing()
            while self.__running:
                frame = self.__client.get_pushed_frame(self.__start_seq_id, self.__stop_seq_id)
                if frame is None:
                    break
                
                pts3d = SimpleViewer.convert_frame_to_matrix(frame)
                with self.__shared:
                    self.__model = pts3d
                self.model_updated.emit()        
                
            self.__stop_seq_id = self.stop_pushing()
            raise RuntimeError("Data stream from host has been interupted")
            
        except Exception as e:
            self.__runnning = False
            self.critical_occured.emit(str(e))
        finally:
            try:
                self.__client.set_state(MkeClient.STATE_IDLE)
            except:
                pass
        
    @QtCore.pyqtSlot()
    def render_view(self):
        with self.__shared:
            N = self.__model.shape[0]
            size = np.ones((N))*SimpleViewer.POINT_SIZE
            dists = np.linalg.norm(self.__model[:,1:], axis=1)
            norm_by = SimpleViewer.MAX_DIST #  np.max(dists) 
            dists[dists>norm_by] = norm_by
            color = np.array([pg.hsvColor(d/norm_by).getRgb() for d in dists])
            if self.__cloud:
                self.__view.removeItem(self.__cloud)
            
            if N > 0:
                self.__cloud = gl.GLScatterPlotItem(pos=self.__model[:,1:], size=size, color=color, pxMode=False)
                self.__cloud.scale(SimpleViewer.DATA_SCALE,
                                   SimpleViewer.DATA_SCALE,
                                   SimpleViewer.DATA_SCALE)
                self.__view.addItem(self.__cloud)
            else:
                self.__cloud = None
        
    
# -----------------------------------------------------------------------------

app = QtGui.QApplication([])
viewer = SimpleViewer()
viewer.show()
app.exec()

# -----------------------------------------------------------------------------
