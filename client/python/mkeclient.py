#!/bin/python3
#
# mkeclient - class for TCP communication with module
# 
# Copyright (c) 2017, Magik-Eye s.r.o., Prague
# author: Ondra Fisar
#
# ------------------------------------------------------------------------------

# imports

import sys
import socket
import time
import struct

import colorama

# ------------------------------------------------------------------------------

class MkeApiError(RuntimeError):
    
    def __init__(self, message, ret_code, seq_id):
        super(RuntimeError, self).__init__(message)
        
        self.ret_code = ret_code
        self.seq_id = seq_id

# ------------------------------------------------------------------------------

class MkeClient(object):

  # PUBLIC VARIABLES =======================================

  # constatants (API status) -------------------------------
  
  STATUS_DATA_WILL_START                = 100
  STATUS_DATA_WILL_CONTINUE             = 101
  STATUS_DATA_STOPPED                   = 102
  
  STATUS_OK                             = 200
  
  STATUS_CLIENT_ERROR                   = 400
  STATUS_CLIENT_MALFORMED_REQUEST       = 401
  STATUS_CLIENT_ILLEGAL_REQUEST_TYPE    = 402
  STATUS_CLIENT_REQUEST_DOES_NOT_APPLY  = 403
  
  STATUS_SERVER_ERROR                   = 500
  STATUS_SERVER_REQUEST_INTERRUPTED     = 501
  STATUS_SERVER_BUSY                    = 502
  STATUS_SERVER_INSUFFICIENT_RESOURCES  = 503

  STATE_IDLE                            = 1
  STATE_DEPTH_SENSOR                    = 2
  
  TERMINATE_REBOOT                      = 1
  TERMINATE_SHUTDOWN                    = 2

  # PRIVATES VARIABLES =====================================

  # constatants (API calls) --------------------------------
  
  __TERMINATE           = 10
  __GET_FIRMWARE_INFO   = 11
  __GET_DEVICE_INFO     = 12
  __GET_STATE           = 20
  __SET_STATE           = 21
  __START_FRAME_PUSH    = 24
  __STOP_FRAME_PUSH     = 25
  __GET_FRAME           = 26

  __MKE_REQUEST_HEAD    = 'MKERQ100'
  __MKE_REPLY_HEAD      = 'MKERP100'

  # varibales ----------------------------------------------
  
  __PACKET_LEN          = 48
  __REQ_PARAMS_LEN      = 24
  __sock                = None
  __seq_id              = 0
  __verbose             = False

  
  # constructor - connect to server ------------------------
  
  def __init__(self, host, port, verbose = False):
    
    self.__sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    self.__sock.connect((host, port))
    self.__verbose = verbose
    
    fw_info = self.get_fw_info();  
    dev_info = self.get_device_info();
    
    print("Connected to device with FW_VER: %d.%d.%d (SYS_VER: %d.%d.%d) on device %s:" % 
      (fw_info['fw_version']['major'], 
       fw_info['fw_version']['minor'],
       fw_info['fw_version']['patch'],
       fw_info['sys_version']['major'], 
       fw_info['sys_version']['minor'],
       fw_info['sys_version']['patch'],
       dev_info['unit_id'].decode('ascii')) )

  def __del__(self):
    self.__sock.close()

  # parse replied parameter --------------------------------

  @staticmethod
  def _parse_reply_param(data, is_signed = False):
    
    return int.from_bytes(data, byteorder='little', signed=is_signed)
  
  
  # just nice color print ----------------------------------

  @staticmethod
  def _cprint(color, text):
    print(color + text + colorama.Style.RESET_ALL)

    
  # send plain payload -------------------------------------

  def _send_payload(self, payload):
    if self.__sock.send(payload) == 0:
      raise RuntimeError("Socket broken")
    
    if self.__verbose:
      print("<<< PAYLOAD %d" % len(payload))    
      
    return
  

  # send general command -----------------------------------
  
  def _send_mke(self, cmd, params = bytearray(8)):
    
    assert(len(params) == 8)
    
    msg = MkeClient.__MKE_REQUEST_HEAD + "%04d" % cmd
    seq_id_bin = struct.pack('<I', self.__seq_id)
    msg = bytearray(msg, 'ascii') + seq_id_bin + params
    
    if self.__sock.send(msg) == 0:
      raise RuntimeError("Socket broken")
    
    if self.__verbose:
      print("<<< %s" % msg)
      
    self.__seq_id += 1
    
    return self.__seq_id - 1;
  
  
  # receive next synchronous reply -------------------------
    
  def _recv_mke(self):
    
    data = bytes()

    while len(data) < MkeClient.__PACKET_LEN:
      tmp = self.__sock.recv(MkeClient.__PACKET_LEN)
      if len(tmp) == 0:
        raise RuntimeError("Socket closed");
      data += tmp
      
    if data[0:len(MkeClient.__MKE_REPLY_HEAD)] != bytearray(MkeClient.__MKE_REPLY_HEAD, 'ascii'):
      raise RuntimeError("Received BAD magic: " + str(data[0:len(MkeClient.__MKE_REPLY_HEAD)]))

    payload_size = struct.unpack('<I', data[20:24])[0];

    ret_cmd   = int(data[8:12])
    ret_code  = int(data[12:16])
    clr       = colorama.Style.RESET_ALL

    if ret_code >= 500:
      clr = colorama.Fore.RED + colorama.Style.BRIGHT
    elif ret_code >= 400:
      clr = colorama.Fore.YELLOW + colorama.Style.BRIGHT
    elif ret_code >= 200:
      clr = colorama.Fore.GREEN + colorama.Style.BRIGHT
    elif ret_code >= 100:
      clr = colorama.Fore.CYAN + colorama.Style.BRIGHT
    
    if self.__verbose:
      MkeClient._cprint(clr, ">>> %s %s %s [%d+%d](CMD: %d, RET: %d)" % 
        (data[:20], data[20:24], data[24:], len(data), payload_size, ret_cmd, ret_code));

    to_receive = payload_size
    while to_receive > 0:
      tmp = self.__sock.recv(to_receive)
      if len(tmp) == 0:
        raise RuntimeError("Socket closed");
      data += tmp
      to_receive -= len(tmp)

    return { 'ret_cmd':     ret_cmd, 
             'ret_code':    ret_code, 
             'seq_id':      struct.unpack('<I', data[16:20])[0], 
             'params':      bytearray(data[24:48]),
             'payload':     data[48:] 
           };
  
  
  # send command and check result --------------------------
  
  def _send_cmd(self, cmd, params = bytearray(8)):

    seq_id = self._send_mke(cmd, params)
    ret = self._recv_mke()

    assert(cmd == ret['ret_cmd'] and seq_id == ret['seq_id'])
    
    return ret
    
   
  # get frame item size ------------------------------------
  
  def _get_frame_item_size(self, frame_type):
    
    assert(frame_type >= 1 and frame_type <= 2)
    
    return { 1: 8, 2: 12 }.get(frame_type)
  

  # parse frame item ---------------------------------------
  
  def _parse_frame_item(self, item_data, frame_type):
    
    ret = { 'uid': struct.unpack('<H', item_data[0:2])[0],
            'x':   struct.unpack('<h', item_data[2:4])[0],
            'y':   struct.unpack('<h', item_data[4:6])[0],
            'z':   struct.unpack('<h', item_data[6:8])[0] }
    
    if frame_type > 1:
      ret['lid'] = struct.unpack('<H', item_data[8:10])[0]
      ret['did'] = struct.unpack('<H', item_data[10:12])[0]
      
    return ret
      
  
  # PUBLIC CALLS ===========================================
  
  # terminate ----------------------------------------------
  
  def terminate(self, method):
    
    # terminate does not send reply
    
    params = struct.pack('<I', method) + bytearray(4)
    return self._send_cmd(MkeClient.__TERMINATE, params)


  # get firmware info --------------------------------------
  
  def get_fw_info(self):
        
    ret = self._send_cmd(MkeClient.__GET_FIRMWARE_INFO)
    if ret['ret_code'] != MkeClient.STATUS_OK:
      raise MkeApiError("Unexpected reply status code", ret['ret_code'], ret['seq_id'])
    
    (buildtime, commit_no) = struct.unpack('<QI', ret['params'][0:12])
    fw_ver = { 'major' : int(ret['params'][12]),
               'minor' : int(ret['params'][13]),
               'patch' : int(ret['params'][14])}
    sys_ver = { 'major' : int(ret['params'][15]),
                'minor' : int(ret['params'][16]),
                'patch' : int(ret['params'][17])}
    return { 'posixtime'  : buildtime,
             'git_commit' : commit_no,
             'fw_version' : fw_ver,
             'sys_version': sys_ver
             }
      
      
  # get device info ----------------------------------------
  
  def get_device_info(self):
        
    ret = self._send_cmd(MkeClient.__GET_DEVICE_INFO)
    if ret['ret_code'] != MkeClient.STATUS_OK:
      raise MkeApiError("Unexpected reply status code", ret['ret_code'], ret['seq_id'])
    
    return { 'unit_id'    : ret['params'][0:8] }
      

  # get state ----------------------------------------------
  
  def get_state(self):
    
    ret = self._send_cmd(MkeClient.__GET_STATE)
    
    if ret['ret_code'] != MkeClient.STATUS_OK:
      raise MkeApiError("Unexpected reply status code", ret['ret_code'], ret['seq_id'])
    
    return struct.unpack('<I', ret['params'][0:4])[0]

  
  # set state ----------------------------------------------
  
  def set_state(self, state):
    
    params = struct.pack('<I', state) + bytearray(4)
    ret = self._send_cmd(MkeClient.__SET_STATE, params)

    return ret['ret_code']
  
  
  # start pushing frames -----------------------------------
  
  def start_frame_push(self, frame_type):
    
    params = struct.pack('<H', frame_type) + bytearray(6)
    ret = self._send_cmd(MkeClient.__START_FRAME_PUSH, params)
    
    if ret['ret_code'] != MkeClient.STATUS_DATA_WILL_START:
      raise RuntimeError("Unexpected reply with code %d" % ret['ret_code'])
    
    return ret['seq_id']
  
  
  # stop pushing frames - !! returns SEQ_ID ----------------
  #  - does not check reply status, probably there are frames on the bus
  
  def stop_frame_push(self):
    
    return self._send_mke(MkeClient.__STOP_FRAME_PUSH)
  

  # get one frame ------------------------------------------
  
  def get_frame(self, frame_type):
    
    params = struct.pack('<H', frame_type) + bytearray(6)
    ret = self._send_cmd(MkeClient.__GET_FRAME, params)
    
    if ret['ret_code'] != MkeClient.STATUS_OK:
      raise RuntimeError("Unexpected reply with code %d" % ret['ret_code'])

    (timer, seqn, data_type, frame_type, num_data) = struct.unpack('<QQIHH', ret['params'])

    return { 'timer'        : timer,
             'seqn'         : seqn,
             'data_type'    : data_type,
             'frame_type'   : frame_type,
             'num_data'     : num_data,
             'payload'      : ret['payload'] }
  
  
  # get one frame ------------------------------------------
  
  def get_pushed_frame(self, start_seq_id, stop_seq_id):
    
    ret = self._recv_mke()
    
    if ret['seq_id'] == start_seq_id:       # received reply from start_frame_push
      
      if ret['ret_code'] == MkeClient.STATUS_DATA_STOPPED \
              or ret['ret_code'] == MkeClient.STATUS_SERVER_REQUEST_INTERRUPTED:  # this is last reply
        return None
      
      # reply should have STATUS_DATA_WILL_CONTINUE status
      
      if ret['ret_code'] != MkeClient.STATUS_DATA_WILL_CONTINUE:
        raise RuntimeError("Unexpected reply with code %d from start_frame_push" % ret['ret_code'])

      (timer, seqn, data_type, frame_type, num_data) = struct.unpack('<QQIHH', ret['params'])

      return { 'timer'        : timer,
              'seqn'         : seqn,
              'data_type'    : data_type,
              'frame_type'   : frame_type,
              'num_data'     : num_data,
              'payload'      : ret['payload'] }
    
    elif ret['seq_id'] == stop_seq_id:      # received reply from stop_frame_push

      # reply should be STATUS_OK
      
      if ret['ret_code'] != MkeClient.STATUS_OK:
        raise RuntimeError("Unexpected reply with code %d from stop_frame_push" % ret['ret_code'])
      
      # ignore this message and read next until STATUS_DATA_STOPPED
      
      return self.get_pushed_frame(start_seq_id, stop_seq_id)
    
    else:
      raise RuntimeError("Unexpected reply with seq_id %d" % ret['seq_id'])
    
    
  # parse frame --------------------------------------------
  
  def parse_frame(self, frame_data, num_items, frame_type):

    sz = self._get_frame_item_size(frame_type)

    # prepare lists

    pts = []

    for i in range(num_items):
      pts.append(self._parse_frame_item(frame_data[i*sz:(i+1)*sz], frame_type))
      
    return pts
  
  
# ------------------------------------------------------------------------------
