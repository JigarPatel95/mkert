#!/usr/bin/python3
#
# discover - looks for UPnP sensors by MagikEye
# 
# Copyright (c) 2017, Magik-Eye s.r.o., Prague
# author: Ondra Fisar
#
# Based on code from https://gist.github.com/provegard/1435555
#
# ------------------------------------------------------------------------------

import sys
import threading
import urllib
import xml.etree.ElementTree as etree

from twisted.internet import reactor, task
from twisted.internet.protocol import DatagramProtocol

# ------------------------------------------------------------------------------

SSDP_ADDR = '239.255.255.250'
SSDP_PORT = 1900

MS = ('M-SEARCH * HTTP/1.1\r\nHOST: %s:%d\r\nMAN: "ssdp:discover"\r\nMX: 2\r\nST: ssdp:all\r\n\r\n' % (SSDP_ADDR, SSDP_PORT)).encode('ascii')

# ------------------------------------------------------------------------------

class DiscoverServer(DatagramProtocol):

  def __init__(self, iface):

    self.iface = iface
    self.__devices = []
    #task.LoopingCall(self.send_msearch).start(6) # every 6th seconds
    self.sendMsearch();


  def sendMsearch(self):

    port = reactor.listenUDP(0, self, interface=self.iface)
    print("Sending M-SEARCH...")
    port.write(MS, (SSDP_ADDR, SSDP_PORT))
    reactor.callLater(2.5, port.stopListening) # MX + a wait margin


  def datagramReceived(self, datagram, address):
  
    message = datagram.decode('ascii')
    lines = message.rsplit('\r\n')
    
    if lines[0] != 'HTTP/1.1 200 OK':
      print("Unknown message %s" % lines[0])
      return;

    url = None
    for line in lines:
      if line[0:9] == 'LOCATION:':
        url = line[9:]
    
    if url:
      self.parseInfo(url.strip(), address)
#      except:
        # ignore errors - bad response
#        pass

  def parseInfo(self, url, address):
    response = urllib.request.urlopen(url)
    xml = response.read().decode('utf-8')
    
    root = etree.fromstring(xml)

    assert(root.tag == '{urn:schemas-upnp-org:device-1-0}root')
    
    for dev in root.findall('{urn:schemas-upnp-org:device-1-0}device'):
      manufacturer = dev.find('{urn:schemas-upnp-org:device-1-0}manufacturer').text
      modelName = dev.find('{urn:schemas-upnp-org:device-1-0}modelName').text
      modelNum  = dev.find('{urn:schemas-upnp-org:device-1-0}modelNumber').text
      sn = dev.find('{urn:schemas-upnp-org:device-1-0}serialNumber').text
      

      self.__devices.append({
        'manufacturer': manufacturer,
        'modelName': modelName,
        'modelNumber': modelNum,
        'serialNumber': sn,
        'address': address})

  def stop(self):
    # print all info

    print('List of available devices:')
    print('')
    
    if len(self.__devices) <= 0:
      print('No device available!')
      return;

    print('|%16s|%16s|%16s|%8s|%16s|' % ('IP', 
                                         'Manufacturer', 
                                         'Model Name', 
                                         'Model Number', 
                                         'S/N'))
    print('|' + '-'*16 + '+' + '-' * 16 + '+' + '-' * 16 + '+' + '-' * 8 + '+' + '-' * 16 + '|')
    
    for dev in self.__devices:
      print('|%16s|%16s|%16s|%8s|%16s|' % (dev['address'][0], 
                                           dev['manufacturer'], 
                                           dev['modelName'],
                                           dev['modelNumber'],
                                           dev['serialNumber']))

# ------------------------------------------------------------------------------

def main(iface, timeout):

  server = DiscoverServer(iface)
  reactor.addSystemEventTrigger('before', 'shutdown', server.stop)
  
  # stop after the timeout
  
  threading.Timer(timeout, reactor.callFromThread, (reactor.stop,)).start()
  

# ------------------------------------------------------------------------------

if __name__ == "__main__":
  if len(sys.argv) < 2:
    print("Usage: %s <IP of interface> [timeout]" % (sys.argv[0], ))
    sys.exit(1)

  iface = sys.argv[1]
  timeout = sys.argv[2] if len(sys.argv) > 2 else 3

  reactor.callWhenRunning(main, iface, timeout)
  reactor.run()

# ------------------------------------------------------------------------------
