%
%
% exmample of usage:
%   >> d = demo_get_image('192.168.4.74');
%   >> d.Show_data();
%   >> d.Save_image_data('showImage',true,'mode','im','getImage_mode','avgim_while_service','laserPattern',2,'laserStartDelay',1);
%
%   >> d.Save_image_data('showImage',true,'mode','data','getImage_mode','debugim_while_depth_sensing','laserStartDelay',1,'FrameType','MKE_FRAME_TYPE_RESERVED_2');
%

classdef demo_get_image
    
    %%
    properties   
    
        t
        mkeclient;
    
    end
    
    %%
    methods
        
        %------------------------------------------------------------------
        function obj = demo_get_image(mkeaddress)
            
            addpath ..
            
            obj.t = tcpclient(mkeaddress,8888,'TimeOut',1);
            obj.mkeclient = MkEReservedAPIClient;
            
        end
        %------------------------------------------------------------------
        
        
        %------------------------------------------------------------------
        function  Show_data(obj)
            
            %--- get data
            [ims,frames,subheaders] = obj.Get_image_data();
            
            %--- init fiure
            id = 1; %%% we can get more images
            figure(34534)
            
            %-- show image
            subplot(1,3,1)
            imshow(ims{id});
            try
                title(['laser pattern=',num2str(subheaders(id).laser_pattern)])
            catch
                title('you are sing version without laser pattern info');
            end
            
            %--- show 2d data
            subplot(1,3,2)
            X = frames{id}.items;
            if isfield(X,'u')
                X = [frames{id}.items.u];
                imagesc(ims{id}); hold on;
                plot(X(1,:),X(2,:),'xr');
                axis(gca,'ij');
            else
                warning('no 2D points defined');
            end
            
            %--- show 3d data
            subplot(1,3,3)
            X = double(reshape([frames{id}.items.X],3,[]));
            plot3(X(1,:),X(2,:),X(3,:),'.k'); axis equal;

        end
        %------------------------------------------------------------------
        
        
        %------------------------------------------------------------------
        function obj = Save_image_data(obj,varargin)
            iargs = inputParser;  
            iargs.addOptional('mode','data');
            iargs.addOptional('showImage',false);
            iargs.addOptional('getImage_mode','debugim_while_depth_sensing');
            iargs.addOptional('laserPattern',0);
            iargs.addOptional('laserStartDelay',0);
            iargs.addOptional('postfix','.jpg');
            iargs.addOptional('shutterSpeed',[]);
            iargs.addOptional('file2savePath',tempdir);
            iargs.addOptional('FrameType','MKE_FRAME_TYPE_2')
            iargs.parse(varargin{:}); iargs = iargs.Results;
            
            %--- get image
            [ims,frames,subheaders] = obj.Get_image_data('mode',iargs.getImage_mode,...
                'laserPattern',iargs.laserPattern, 'laserStartDelay',iargs.laserStartDelay,...
                'shutterSpeed',iargs.shutterSpeed, 'FrameType', iargs.FrameType);
            
            filepath_core = fullfile(iargs.file2savePath , datestr(datetime,'yyyymmddTHHMMSS'));
            switch iargs.mode
                case 'data'
                    tmp_path = [filepath_core , '.mat'];
                    %--- save data
                    save(tmp_path,'ims','frames','subheaders');
                case 'im'
                    tmp_path = [filepath_core , iargs.postfix];
                    imwrite(ims{1},tmp_path);
            end
            
            disp(['saved to:',tmp_path]);
            
            if iargs.showImage
                if ~isempty(ims)
                    figure(5352)
                    imshow(ims{1});
                else
                    disp('...empty image.. nothing to show.. :(');
                end
            end

        end
        %------------------------------------------------------------------
        
        
        %------------------------------------------------------------------
        function [ims,frames,subheaders] = Get_image_data(obj,varargin)
            iargs = inputParser;  
            iargs.addOptional('mode','debugim_while_depth_sensing');
            iargs.addOptional('laserPattern',[]);
            iargs.addOptional('laserStartDelay',0);
            iargs.addOptional('shutterSpeed',[]);
            iargs.addOptional('FrameType','MKE_FRAME_TYPE_2');%'MKE_FRAME_TYPE_RESERVED_2');
            iargs.parse(varargin{:}); iargs = iargs.Results;
            
            %--- always start from idle
            obj.SetState('MKE_STATE_IDLE');
            
            ims = [];
            frames = [];
            subheaders = [];
            
            switch iargs.mode
                case 'debugim_while_depth_sensing'
                    if ~isempty(iargs.shutterSpeed)
                        warning('You want to set shutter speed, but image will be taken in depth sensing mode!');
                    end
                    %--- to depth sensing
                    obj.SetState('MKE_STATE_DEPTH_SENSOR');
                    
                    %--- get image
                    RequestID = round( rand()*100 );
                    request = obj.mkeclient.sendRequestTCP(obj.t, 'MKE_REQUEST_GET_DEBUG_IMAGES' , RequestID , 'FrameType', iargs.FrameType);
                    pause(.2);
                    reply = obj.mkeclient.readReplyTCP(obj.t);
                    if isfield(reply,'num_images')
                        ims = reply.image;
                        frames = reply.frame;
                        subheaders = reply.subheaders;
                    else
                        warning('no images returned');
                    end
                case 'avgim_while_service'
                    %--- to service
                    obj.SetState('MKE_STATE_IDLE');
                    obj.SetState('MKE_STATE_SERVICE');
                    
                    %--- set laser pattern
                    if ~isempty(iargs.laserPattern)
                        disp('...setting laser pattern');
                        RequestID = round( rand()*100 );
                        request = obj.mkeclient.sendRequestTCP(obj.t, 'MKE_REQUEST_SET_LASER' , RequestID , 'LaserPattern' , iargs.laserPattern);
                        pause(.2);
                        reply = obj.mkeclient.readReplyTCP(obj.t);
                    end
                    %--- set shutter speed
                    if ~isempty(iargs.shutterSpeed)
                        disp('...setting shutter speed');
                        RequestID = round( rand()*100 );
                        request = obj.mkeclient.sendRequestTCP(obj.t, 'MKE_REQUEST_SET_SHUTTER' , RequestID , 'ShutterSpeed' , iargs.shutterSpeed);
                        pause(.4);
                        reply = obj.mkeclient.readReplyTCP(obj.t);
                    end
                    
                    %--- wait if I want to
                    if iargs.laserStartDelay>0
                        pause(iargs.laserStartDelay);
                    end
                    
                    %--- get image
                    RequestID = round( rand()*100 );
                    request = obj.mkeclient.sendRequestTCP(obj.t, 'MKE_REQUEST_GET_IMAGE' , RequestID , 'NumAvgImages' , 1);
                    pause(.2);
                    reply = obj.mkeclient.readReplyTCP(obj.t);
                    if isfield(reply,'image')
                        ims = {reply.image};
                        frames = [];
                        subheaders = [];
                    else
                        warning('no images returned');
                    end
                    
            end
            
            %--- set to idle
            obj.SetState('MKE_STATE_IDLE');
        end
        %------------------------------------------------------------------
        
    end
    
    %%
    methods (Access = private)
        function [RequestID,request,reply] = SetState(obj,StateType)
            RequestID = round( rand()*100 );
            request = obj.mkeclient.sendRequestTCP(obj.t, 'MKE_REQUEST_SET_STATE' , RequestID , 'StateType' , StateType);
            pause(.2);
            try
                reply = obj.mkeclient.readReplyTCP(obj.t);
            catch
                warning('errro while receving data for setting to state ',StateType);
            end
        end
        
        
    end
    
   
    
    
end
    
    