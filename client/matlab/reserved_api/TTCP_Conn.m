%
%
%
%
classdef TTCP_Conn < MkEReservedAPIClient
    
    
    %% ====================================================================
    properties
         
        tcp_connection         = [];     %%% tcpip object
        need_send_frame_req    = true;   %%% helper for pts
        frameIDSent            = [];
        connection_established = false;  %%% to check if tcp is on
        
        sent_ids = [];  %%% list of req=ids we are wating for
        
        Print_status_fce = @(x) disp(x);
        Print_log_fce = @(x) disp(x);
    end
    
    %% ====================================================================
    methods
        
        %------------------------------------------------------------------
        function obj = TTCP_Conn(print_status_,print_log_)
            if exist('print_status_','var') && ~isempty(print_status_)
                obj.Print_status_fce = print_status_;
            end
            if exist('print_log_','var') && ~isempty(print_log_)
                obj.Print_log_fce = print_log_;
            end
        end
        %------------------------------------------------------------------
        
      
        %------------------------------------------------------------------
        function [failed, obj] = InitTCPIP(obj,mkeaddress)
            failed = true;
            try
               if exist('mkeaddress','var') && ~isempty(mkeaddress)
               else
                  warning('mkeaddress not defined correctly');
               end
                obj.tcp_connection = tcpclient(mkeaddress,8888,'TimeOut',1);
                failed = false;
                obj.connection_established = true;
            catch
                obj.Print_status_fce(sprintf('...wating for connection %s, %i',mkeaddress,8888));
                % GUI = GUI.PrintStatus(sprintf('...wating for connection %s, %i',VPARS.mkeaddress,8888));
            end
        end
        %------------------------------------------------------------------
        
         %------------------------------------------------------------------
         function obj = SetState(obj,stateType)
             obj = obj.SendRequest('MKE_REQUEST_SET_STATE','StateType',stateType);
        end
        %------------------------------------------------------------------
        
       
        %------------------------------------------------------------------
        function reply = ReadReply(obj)
            reply   = obj.readReplyTCP(obj.tcp_connection);
            obj.Print_log_fce(sprintf('reply: \t req_id=%3i, \t status=%i, \t numbytes=%i, \t status_str=%s',reply.reqid,reply.status,reply.num_bytes,reply.status_str));
        end
        %------------------------------------------------------------------
        
        %------------------------------------------------------------------
        function [obj,replies] = ReadAllRequests(obj)
            replies = {};
            try
                while ~isempty(obj.sent_ids)
                    reply = obj.ReadReply();
                    replies{end+1} = reply;
                    obj = obj.Remove_ReqID_fromList(reply.reqid);
                end
            catch
                warning('TCPIP: ReadAllRequests doen due to timeout');
            end
            obj.sent_ids = [];
            obj.need_send_frame_req = true;
            obj.frameIDSent = [];
        end
        %------------------------------------------------------------------
        
        
        %------------------------------------------------------------------
        function obj = SendFrameRequestIfNeeded(obj,FrameType)
            if obj.need_send_frame_req
                [obj,obj.frameIDSent] = obj.SendRequest('MKE_REQUEST_GET_FRAME','FrameType',FrameType);
                obj.need_send_frame_req = false;
            end
        end
        %------------------------------------------------------------------
        
        %------------------------------------------------------------------
        % get data from sensor
        function [failed,data,obj] = GetFrameData(obj,FrameType)
            failed = true;
            data = [];
            
            %--- do we have some reply ready?
            if(obj.tcp_connection.BytesAvailable >= 48)
                %--- read reply
                reply = obj.ReadReply();
                
                %--- I was asked to get frame...
                obj.need_send_frame_req = true;
                
                if obj.frameIDSent==reply.reqid && strcmp(reply.status_str,'MKE_REPLY_OK')
                    obj = obj.Remove_ReqID_fromList(obj.frameIDSent);
                    if reply.num_data>0
                        data   = reply.frame.items;
                        failed = false;
                    end
                else
                    warning(['No matching request reply or wrong reply, sent ',num2str(obj.frameIDSent),' reciveed ',num2str(reply.reqid)]);
                end
            else
                warning(['Replay has to be at least 48 Bytes, it is',num2str(obj.tcp_connection.BytesAvailable)]);
            end
            
            %--- next request (it checks if I want to send it
            obj = obj.SendFrameRequestIfNeeded(FrameType);
            
        end
        %------------------------------------------------------------------
        
        
        %------------------------------------------------------------------
        function [obj,im] = Get_UnsyncImage(obj)
            
            %--- set sensro to service
            [obj,initial_state] = obj.SafeResetToService();
            
            %--- read image
            obj = obj.SendRequest('MKE_REQUEST_GET_IMAGE');
            [obj,replies] = obj.ReadAllRequests();
            im = replies{1}.image;
            
            %--- set state to service (if it is not at service)
            if  ~strcmp(initial_state,'MKE_STATE_SERVICE')
                obj = obj.SetState('MKE_STATE_IDLE');
                obj = obj.SetState(initial_state);
                obj = obj.ReadAllRequests();
            end
            
            obj.need_send_frame_req = true;
        end
        %------------------------------------------------------------------
        
          %------------------------------------------------------------------
        function [obj,ims,frames,subheaders,used_frame_type] = Get_DebugImage(obj)
            ims = [];
            frames = [];
            subheaders = [];
            used_frame_type = 'MKE_FRAME_TYPE_RESERVED_2';
            %--- be sure we dont have anythng to wait for
            [obj,~] = obj.ReadAllRequests();
            
            %--- read image
            obj = obj.SendRequest('MKE_REQUEST_GET_DEBUG_IMAGES','FrameType',used_frame_type);
            [obj,replies] = obj.ReadAllRequests();
            if ~isempty(replies) && isfield(replies{1},'image')
                ims    = replies{1}.image;
                frames = replies{1}.frame;
                subheaders = replies{1}.subheaders;
            else
                warning('no images from tcpip as no reply given');
            end
            
            obj.need_send_frame_req = true;
        end
        %------------------------------------------------------------------
        
    end
    
    
    %% ====================================================================
    methods (Access = 'protected')
        
        %------------------------------------------------------------------
        function [obj,RequestID] = SendRequest(obj,requestType,varargin)
            RequestID = round( rand()*100 );
            request = obj.sendRequestTCP(obj.tcp_connection, requestType , RequestID , varargin{:});
            obj.Print_log_fce(sprintf('reqst: \t req_id=%3i, \t type=%s, \t varg=%s',RequestID,requestType,[varargin{:}]));
            obj.sent_ids(end+1) = RequestID;
        end
        %------------------------------------------------------------------
        
        %------------------------------------------------------------------
        function obj = Remove_ReqID_fromList(obj,id)
            obj.sent_ids = obj.sent_ids( ~(obj.sent_ids==id) );
        end
        %------------------------------------------------------------------
        
        %------------------------------------------------------------------
        % move to service and wait for finishing all requests
        function [obj,initial_state] = SafeResetToService(obj)
             %--- be sure we are not waiting for something else
            [obj,~] = obj.ReadAllRequests();
            
            %--- get current state
            obj = obj.SendRequest('MKE_REQUEST_GET_STATE');
            [obj,replies] = obj.ReadAllRequests();
            if replies{1}.status==200
                initial_state = replies{1}.state_str;
            else
                return;
            end
            
            %--- set state to service (if it is not there already)
            if ~strcmp(initial_state,'MKE_STATE_SERVICE')
                obj = obj.SetState('MKE_STATE_IDLE');
                obj = obj.SetState('MKE_STATE_SERVICE');                
                [obj,replies] = obj.ReadAllRequests();
                if replies{1}.status~=200  %% not right set of service state
                     return
                 end
            end
        end
        %------------------------------------------------------------------
        
        %------------------------------------------------------------------
    end
    
end


