classdef MkEReservedAPIClient < MkEClient

    methods (Access = public, Static)
                
        function exportDebugImages(dir_path, dbg_images)
            if (~exist(dir_path))
                mkdir(dir_path);
            end
                        
            no_imgs = numel(dbg_images.image);
            
            for i = 1:no_imgs
                img_fname = fullfile(dir_path, sprintf('img_%02d.pgm', i));
                imwrite(dbg_images.image{i}, img_fname);
            end
            
            save(fullfile(dir_path, 'dbg_images.mat'), 'dbg_images');
        end
        
        % =================================================================
        
        function statsBreakdown(s)
            user_timer = str2double(s.reset_user_timer_s);
            system_timer = str2double(s.reset_system_timer_s);
            wc_timer = str2double(s.reset_wall_clock_timer_s);
            
            no_dets = numel(s.detector.mkedd.dds);
            dds_timer = 0;
            pxlist_timer = 0;
            acclist_timer = 0;
            conv_timer = 0;
            nms_timer = 0;
            bpeak_timer = 0;
            
            for i = 1:no_dets
                dds_timer = dds_timer + str2double(s.detector.mkedd.dds(i).user_timer_s);                
                pxlist_timer = pxlist_timer + str2double(s.detector.mkedd.dds(i).pxlist_timer_s);
                acclist_timer = acclist_timer + str2double(s.detector.mkedd.dds(i).acclist_timer_s);
                conv_timer = conv_timer + str2double(s.detector.mkedd.dds(i).conv_timer_s);
                nms_timer = nms_timer + str2double(s.detector.mkedd.dds(i).nms_timer_s);
                bpeak_timer = bpeak_timer + str2double(s.detector.mkedd.dds(i).bpeak_timer_s);
            end
            
             fprintf('MkERT runtime statistics breakdown\n\n');
            
            fprintf('MkERT CPU load (user):     %6.2f%% [CPU]\n', user_timer / wc_timer * 100);        
            fprintf('MkERT CPU load (system):   %6.2f%% [CPU]\n', system_timer / wc_timer * 100);
            fprintf('\n');
            fprintf('MkEDD CPU load:            %6.2f%% [mkert]\n', dds_timer / user_timer * 100);        
            fprintf('MkEDD PxList:              %6.2f%% [mkert], %6.2f%% [mkedd] \n', ...
                pxlist_timer / user_timer * 100, pxlist_timer / dds_timer * 100);        
            fprintf('MkEDD AccList:             %6.2f%% [mkert], %6.2f%% [mkedd] \n', ...
                acclist_timer / user_timer * 100, acclist_timer / dds_timer * 100);
            fprintf('\n');
            fprintf('MkEDD Conv:                %6.2f%% [mkert], %6.2f%% [mkedd],  %6.2f%% [acclist] \n', ...
                conv_timer / user_timer * 100, conv_timer / dds_timer * 100, conv_timer / acclist_timer * 100);
            fprintf('MkEDD Nms:                 %6.2f%% [mkert], %6.2f%% [mkedd],  %6.2f%% [acclist] \n', ...
                nms_timer / user_timer * 100, nms_timer / dds_timer * 100, nms_timer / acclist_timer * 100);                
            fprintf('MkEDD BestPeak:            %6.2f%% [mkert], %6.2f%% [mkedd],  %6.2f%% [acclist] \n', ...
                bpeak_timer / user_timer * 100, bpeak_timer / dds_timer * 100, bpeak_timer / acclist_timer * 100);              
        end
    end

    %%% MkEReservedAPIClient methods =======================================
    
    methods (Access = protected, Static)
        
        function [mantisa, exponent] = toSci(value)
          if (value ~= 0)
              exponent = floor(log2(abs(value)) - 6);
          else
              exponent = 0;
          end
          
          mantisa = round(value / (2^exponent));         
        end
        
        function [image, idx] = readImage(data, idx, num_data)
            data = data(idx:(idx + num_data - 1));
            idx = idx + num_data;
            
            if ((data(1) == 80) && (data(2) == 53))
                
                lf_pos = 3;
                for i = 1:3
                    while (data(lf_pos) == 10 || data(lf_pos) == ' ')
                        lf_pos = lf_pos+1;
                    end
                    
                    while (data(lf_pos) >= '0' && data(lf_pos) <= '9')
                        lf_pos = lf_pos+1;
                    end   
                end
                dims = sscanf(char(data(3:lf_pos)), '%d');
                image = reshape(data((lf_pos + 1):end), dims(1), dims(2))';
            else
                warning('Unrecognized image format');
                image = data;
            end
        end
        
    end
    
    methods (Access = protected)
        
        %%% Request =======================================================        

        function request = setupGetImageRequest(~, request_code, p)
            request = [MkEClient.getMagik(), ...
                       MkEClient.valToCode4(request_code), ...
                       typecast(uint32(p.Results.RequestID), 'uint8'), ...
                       typecast(uint32(p.Results.NumAvgImages), 'uint8'), ...
                       zeros(1, 4, 'uint8');];
        end
        
        function request = setupGetDebugImagesRequest(obj, request_code, p)
            pos = find(strcmpi(obj.MkEFrameTypeKeys, p.Results.FrameType), 1, 'first');
            
            request = [MkEClient.getMagik(), ...
                       MkEClient.valToCode4(request_code), ...
                       typecast(uint32(p.Results.RequestID), 'uint8'), ...                       
                       typecast(uint32(p.Results.NumDebugImages), 'uint8'), ...
                       typecast(uint16(obj.MkEFrameTypeCodes(pos)), 'uint8'), ...
                       zeros(1, 2, 'uint8');];
        end
        
        function request = setupSetShutterRequest(~, request_code, p)
            request = [MkEClient.getMagik(), ...
                       MkEClient.valToCode4(request_code), ...
                       typecast(uint32(p.Results.RequestID), 'uint8'), ...
                       typecast(uint32(p.Results.ShutterSpeed), 'uint8'), ...
                       zeros(1, 4, 'uint8');];
        end                
        
        function request = setupSetLaserRequest(~, request_code, p)
            [m_pd, e_pd] = MkEReservedAPIClient.toSci(p.Results.LaserPulseDuration);
            [m_off, e_off] = MkEReservedAPIClient.toSci(p.Results.LaserPulseOffset);
            
            request = [MkEClient.getMagik(), ...
                       MkEClient.valToCode4(request_code), ...
                       typecast(uint32(p.Results.RequestID), 'uint8'), ...
                       typecast(uint16(p.Results.LaserPattern), 'uint8'), ...
                       typecast(int8(m_pd), 'uint8'), ...
                       typecast(int8(e_pd), 'uint8'), ...
                       typecast(int8(m_off), 'uint8'), ...
                       typecast(int8(e_off), 'uint8'), ...                       
                       zeros(1, 2, 'uint8');];
        end   
        
        function request = setupSetGainRequest(~, request_code, p)
            request = [MkEClient.getMagik(), ...
                       MkEClient.valToCode4(request_code), ...
                       typecast(uint32(p.Results.RequestID), 'uint8'), ...
                       typecast(uint32(p.Results.GainISO), 'uint8'), ...
                       zeros(1, 4, 'uint8');];
        end  
        
        function request = setupSetProfileRequest(~, request_code, p)
            profile = p.Results.Profile;
            
            request = [MkEClient.getMagik(), ...
                       MkEClient.valToCode4(request_code), ...
                       typecast(uint32(p.Results.RequestID), 'uint8'), ...
                       typecast(uint32(numel(profile)), 'uint8'), ...
                       zeros(1, 4, 'uint8'), ...
                       uint8(profile);];
        end           
               
        function p = getRequestInputParser(obj)
            p = obj.getRequestInputParser@MkEClient();
            
            addOptional(p, 'NumAvgImages', 1, @isnumeric);
            
            addOptional(p, 'NumDebugImages', 2, @isnumeric);

            addOptional(p, 'ShutterSpeed', 1000, @isnumeric);
            
            addOptional(p, 'LaserPattern', 0, @isnumeric);
            addOptional(p, 'LaserPulseDuration', 0, @isnumeric);
            addOptional(p, 'LaserPulseOffset', 0, @isnumeric);
            
            addOptional(p, 'GainISO', 400, @isnumeric);
            
            addOptional(p, 'Profile', [], @ischar);
        end
        
        %%% Reply =========================================================
        
        function [frame, idx] = readFrame(~, data, idx, num_data, frame_type)
            one_fpf16 = hex2dec('10000');
                
            if (frame_type == 1) % FRAME_DATA_TYPE_1
                frame.items = struct(  'X', num2cell(zeros(3, num_data), 1), ...
                    'uid', num2cell(zeros(1, num_data), 1));
                
                for i = 1:num_data
                    [frame.items(i).uid, idx] = MkEClient.readData(data, idx, 1, 2, 'uint16');
                    [frame.items(i).X, idx] = MkEClient.readData(data, idx, 3, 2, 'int16');
                end
            elseif (frame_type == 2)
                % FRAME_DATA_TYPE_2
                frame.items = struct(  'X', num2cell(zeros(3, num_data), 1), ...
                    'uid', num2cell(zeros(1, num_data), 1), ...
                    'tid', num2cell(zeros(2, num_data), 1));
                
                for i = 1:num_data
                    [frame.items(i).uid, idx] = MkEClient.readData(data, idx, 1, 2, 'uint16');
                    [frame.items(i).X, idx] = MkEClient.readData(data, idx, 3, 2, 'int16');
                    [frame.items(i).tid, idx] = MkEClient.readData(data, idx, 2, 2, 'uint16');
                end
            elseif (frame_type == 1001)
                % FRAME_DATA_TYPE_RESERVED_1
                frame.items = struct(  'X', num2cell(zeros(3, num_data), 1), ...
                    'u', num2cell(zeros(2, num_data), 1), ...
                    'uid', num2cell(zeros(1, num_data), 1), ...
                    'tid', num2cell(zeros(2, num_data), 1));
                
                for i = 1:num_data
                    [frame.items(i).uid, idx] = MkEClient.readData(data, idx, 1, 2, 'uint16');
                    [frame.items(i).X, idx] = MkEClient.readData(data, idx, 3, 2, 'int16');
                    [frame.items(i).tid, idx] = MkEClient.readData(data, idx, 2, 2, 'uint16');
                    [frame.items(i).u, idx] = MkEClient.readData(data, idx, 2, 4, 'int32');
                    
                    % fpfloat16 to double
                    frame.items(i).u = double(frame.items(i).u) / one_fpf16;
                end
            elseif (frame_type == 1002)
                % FRAME_DATA_TYPE_RESERVED_2
                frame.items = struct(  'X', num2cell(zeros(3, num_data), 1), ...
                    'u', num2cell(zeros(2, num_data), 1), ...
                    'uid', num2cell(zeros(1, num_data), 1), ...
                    'tid', num2cell(zeros(2, num_data), 1), ...
                    'det', num2cell(zeros(1, num_data), 1), ...
                    'bwidth', num2cell(zeros(1, num_data), 1), ...
                    'sigma', num2cell(zeros(1, num_data), 1), ...
                    'resp', num2cell(zeros(1, num_data), 1));
                
                for i = 1:num_data
                    [frame.items(i).uid, idx] = MkEClient.readData(data, idx, 1, 2, 'uint16');
                    [frame.items(i).X, idx] = MkEClient.readData(data, idx, 3, 2, 'int16');
                    [frame.items(i).tid, idx] = MkEClient.readData(data, idx, 2, 2, 'uint16');
                    [frame.items(i).u, idx] = MkEClient.readData(data, idx, 2, 4, 'int32');
                    [frame.items(i).det, idx] = MkEClient.readData(data, idx, 1, 4, 'uint32');
                    [frame.items(i).bwidth, idx] = MkEClient.readData(data, idx, 1, 4, 'int32');
                    [frame.items(i).sigma, idx] = MkEClient.readData(data, idx, 1, 4, 'int32');
                    [frame.items(i).resp, idx] = MkEClient.readData(data, idx, 1, 4, 'int32');
                    
                    % fpfloat16 to double
                    frame.items(i).u = double(frame.items(i).u) / one_fpf16;
                    frame.items(i).sigma = double(frame.items(i).sigma) / one_fpf16;
                    frame.items(i).resp = double(frame.items(i).resp) / one_fpf16;
                end
            else
                warning('Unrecognized frame type: %d', frame_type);
                return;
            end
            
            [frame.crc32, idx] = MkEClient.readData(data, idx, 1, 4, 'uint32');
        end            
        
        function reply = parseDebugImages(obj, reply)
            data = obj.ReplyBuffer.data;
            idx = 25;
            
            [reply.num_images, idx] = MkEClient.readData(data, idx, 1, 4, 'uint32');
            idx = idx + 20;
            
            reply.subheaders = struct('timer', num2cell(zeros(1, reply.num_images), 1), ...
                'image_frame_id', num2cell(zeros(1, reply.num_images), 1), ...
                'image_num_bytes', num2cell(zeros(1, reply.num_images), 1), ...
                'frame_num_bytes', num2cell(zeros(1, reply.num_images), 1), ...
                'frame_type', num2cell(zeros(1, reply.num_images), 1), ...
                'frame_num_data', num2cell(zeros(1, reply.num_images), 1), ...
                'laser_pattern', num2cell(zeros(1, reply.num_images), 1));
            
            for i = 1:reply.num_images
                [reply.subheaders(i).timer, idx] = MkEClient.readData(data, idx, 1, 8, 'int64');
                [reply.subheaders(i).image_frame_id, idx] = MkEClient.readData(data, idx, 1, 4, 'int32');
                [reply.subheaders(i).image_num_bytes, idx] = MkEClient.readData(data, idx, 1, 4, 'int32');
                [reply.subheaders(i).frame_num_bytes, idx] = MkEClient.readData(data, idx, 1, 4, 'int32');
                [reply.subheaders(i).frame_type, idx] = MkEClient.readData(data, idx, 1, 2, 'int16');
                [reply.subheaders(i).frame_num_data, idx] = MkEClient.readData(data, idx, 1, 2, 'int16');
                [reply.subheaders(i).laser_pattern, idx] = MkEClient.readData(data, idx, 1, 2, 'int16');
                idx = idx + 6; % dummy padding;
                
                [reply.image{i}, idx] = MkEReservedAPIClient.readImage(data, idx, reply.subheaders(i).image_num_bytes);
                [reply.frame{i}, idx] = obj.readFrame(data, idx, ...
                     reply.subheaders(i).frame_num_data, reply.subheaders(i).frame_type);
            end
        end
        
        function reply = parseImage(obj, reply)
            data = obj.ReplyBuffer.data;
            idx = 25; 
            [reply.timer, idx] = MkEClient.readData(data, idx, 1, 8, 'uint64');
            [reply.image_id, ~] = MkEClient.readData(data, idx, 1, 4, 'uint32');
            
            [reply.image, ~] = obj.readImage(data, 49, ...
                               numel(data) - 48);
        end
        
        function reply = parseReplyBuffer(obj)
            data = obj.ReplyBuffer.data;
            reply = obj.parseReplyBuffer@MkEClient();
            
            if (reply.status ~= 200)
                return;
            end
            
            if (reply.type == 1012)
                % MKE_REQUEST_GET_IMAGE
                reply = obj.parseImage(reply);
            elseif (reply.type == 1013)
                % MKE_REQUEST_GET_DEBUG_IMAGES
                reply = obj.parseDebugImages(reply);
            elseif (reply.type == 1006)
                % MKE_REQUEST_GET_SHUTTER
                reply.shutter = typecast(data(25:28), 'uint32');
            elseif (reply.type == 1008)
                % MKE_REQUEST_GET_GAIN
                reply.analog_gain = typecast(data(25:28), 'uint32');
                reply.digital_gain = typecast(data(29:32), 'uint32');  
            elseif (reply.type == 1016)
                % MKE_REQUEST_GET_PROFILE
                if (numel(data) > 48)
                    profile = typecast(data(49:end), 'uint8');
                    if (profile(end) == 0)
                      profile = profile(1:end-1);
                    end
                    reply.profile_str = char(profile);
                    reply.profile = jsondecode(reply.profile_str);
                else
                    reply.profile_str = '';
                    reply.profile = [];
                end                
            elseif (reply.type == 1014)
                % MKE_REQUEST_GET_STATS
                if (numel(data) > 48)
                    stats = typecast(data(49:end), 'uint8');
                    if (stats(end) == 0)
                      stats = stats(1:end-1);
                    end
                    reply.stats_str = char(stats);
                    reply.stats = jsondecode(reply.stats_str);
                else
                    reply.stats_str = '';
                    reply.stats = [];
                end                
            end            
        end        
        
        %%% Types =========================================================        
        
        function updateMkERequestType(obj)
            obj.MkERequestType = [ ...
                obj.MkERequestType;{ ...
                'MKE_REQUEST_PING', 1001, @obj.setupNonParamRequest; ... %TBD
                'MKE_REQUEST_START_TIMER', 1002, @obj.setupNonParamRequest; ... %TBD
                'MKE_REQUEST_STOP_TIMER', 1003, @obj.setupNonParamRequest; ...
                'MKE_REQUEST_STREAM_ON', 1004, @obj.setupNonParamRequest; ... %TBD
                'MKE_REQUEST_STREAM_OFF', 1005, @obj.setupNonParamRequest; ... %TBD
                'MKE_REQUEST_GET_SHUTTER', 1006, @obj.setupNonParamRequest; ...
                'MKE_REQUEST_SET_SHUTTER', 1007, @obj.setupSetShutterRequest; ...
                'MKE_REQUEST_GET_GAIN', 1008, @obj.setupNonParamRequest; ...
                'MKE_REQUEST_SET_GAIN', 1009, @obj.setupSetGainRequest; ... 
                'MKE_REQUEST_GET_LASER', 1010, @obj.setupNonParamRequest; ... %TBD 
                'MKE_REQUEST_SET_LASER', 1011, @obj.setupSetLaserRequest; ...
                'MKE_REQUEST_GET_IMAGE', 1012, @obj.setupGetImageRequest; ...
                'MKE_REQUEST_GET_DEBUG_IMAGES', 1013, @obj.setupGetDebugImagesRequest; ...
                'MKE_REQUEST_GET_STATS', 1014, @obj.setupNonParamRequest; ...
                'MKE_REQUEST_GET_PROFILE', 1016, @obj.setupNonParamRequest; ...
                'MKE_REQUEST_RESET_STATS', 1017, @obj.setupNonParamRequest; ...
                'MKE_REQUEST_SET_PROFILE', 2002, @obj.setupSetProfileRequest; ...
                }];
            
            obj.MkERequestTypeKeys = obj.MkERequestType(:, 1);
            obj.MkERequestTypeCodes = cell2mat(obj.MkERequestType(:, 2));            
        end
        
        function updateMkETerminateType(obj)
            obj.MkETerminateType = [ ...
                obj.MkETerminateType;{ ...
                'MKE_TERMINATE_BY_RESTART', 3; ...
                'MKE_TERMINATE_BY_EXIT', 4; ...
            }];

            obj.MkETerminateTypeKeys = obj.MkETerminateType(:, 1);
            obj.MkETerminateTypeCodes = cell2mat(obj.MkETerminateType(:, 2));      
        end         
        
        function updateMkEFrameType(obj)
            obj.MkEFrameType = [ ...
                obj.MkEFrameType;{ ...
                'MKE_FRAME_TYPE_RESERVED_1', 1001; ...
                'MKE_FRAME_TYPE_RESERVED_2', 1002 ...
            }];

            obj.MkEFrameTypeKeys = obj.MkEFrameType(:, 1);
            obj.MkEFrameTypeCodes = cell2mat(obj.MkEFrameType(:, 2));        
        end     
        
        function updateMkEStateType(obj)
            obj.MkEStateType = [ ...
                obj.MkEStateType;{ ...
                'MKE_STATE_SERVICE', 10; ...
            }];

            obj.MkEStateTypeKeys = obj.MkEStateType(:, 1);
            obj.MkEStateTypeCodes = cell2mat(obj.MkEStateType(:, 2));  
        end             
        
    end
    
    methods (Access = public)

        function obj = MkEReservedAPIClient
            obj = obj@MkEClient();
            
            obj.updateMkERequestType();
            obj.updateMkETerminateType();
            obj.updateMkEFrameType();
            obj.updateMkEStateType();        
        end
        

    end
end
