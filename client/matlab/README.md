# Matlab Client for MkE API #


## Overview
This directory contains four demos (01-04) that introduce the sensor's communication protocol.
The demos provides examples on how to use the client API in a custom tools.
For clarity, the demonstration files are ordered from easy-to-understand to more complicated ones--- starting with a  simple state reading and ending with an example of a real-time viewer.

All demo files can be executed separately, or all at once by issuing the following command:

```
>> demo_run_all
```

## Prerequisites
* The sensor is powered on and can be reached via the network from your PC.
* The file ``demo_config.m`` contains variables ``address`` and ``port`` that correspond to your sensor IP address and TCP port.

## 01. Read sensor state
This demo reads the current sensor state and outputs it on the Matlab's command line.

Execution:
```
>> demo01_get_state
 status string = MKE_REPLY_OK
 state string  = MKE_STATE_DEPTH_SENSOR
```
 
## 02. Get single frame data
This demo switches the sensor's state to the depth-sensing state, reads current data frame, switches the sensor's state back to idle and plots the obtained 3D data.

Execution:
```
>> demo02_get_single_frame
```

The result should similar to the image bellow.

![Output example of demo02_get_single_frame.m](doc_data/demo02_prntscr.JPG)

## 03. Read sensor data in a loop
This demo again switches sensor to the depth-sensing state, reads the sensor measurements for a specific amount of time, and finally plots the total number of the obtained 3D points.

Execution:
```
>> demo03_get_frames_loop;
 state changed to depth sensing
 runnig for 5 sec. reamining:  0
 state changed to idle
 4019 points read in 29 frames 
```

## 04. Real time data viewer
This demo sets the sensor to the depth-sensing state and runs a simple viewer that plots what the sensor "sees".
Data is plotted into a standard Matlab's figure, so one can use its tools for the camera movement and a slider to
control the zoom. A real-sized 3d model is plotted in the center of the view to help the viewer to gauge the 3D data scale.

Execution:
```
>> demo04_viewer
```


The result should look similar to the image bellow, the 3D model of the sensor is shown the center, the zoom control slider on the left.

![Output example of demo04_viewer.m](doc_data/demo04_prntscr.JPG)
