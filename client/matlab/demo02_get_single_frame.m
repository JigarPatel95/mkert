%
%  1) Set state to depth sensing
%  2) Read data
%  3) Set state to idle
%  4) Plot 3d data
%
% MagikEye

%% init
demo_config;
try
    t = tcpclient(address,port,'TimeOut',1);
    mkeclient = MkEClient;
catch
    error(' connection not established, be sure demo_config.m file has correct parameters.');
end
  
%% set state to depth sensing
RequestID = round( rand()*100 );

request = mkeclient.sendRequestTCP(t, 'MKE_REQUEST_SET_STATE',RequestID,'StateType','MKE_STATE_DEPTH_SENSOR');
reply   = mkeclient.readReplyTCP(t);
pause(.1);     % wait a bit to be sure all is really running

if RequestID==reply.reqid && strcmp(reply.status_str,'MKE_REPLY_OK')
    fprintf(' state changed to depth sensing\n');
else
    warning('unable change state to depth sensing, reqidcheck= %i, status = %i, status_str = %s',RequestID==reply.reqid,reply.status,reply.status_str);
end
    
%% get 3d data
RequestID = round( rand()*100 );
request = mkeclient.sendRequestTCP(t, 'MKE_REQUEST_GET_FRAME',RequestID,'FrameType','MKE_FRAME_TYPE_2');

% we need to wait if data are not ready for us
while t.BytesAvailable<48
    disp(' waiting till data are ready');
    pause(.1)
end

% read data    
reply   = mkeclient.readReplyTCP(t);

% save them to the X varible
if RequestID==reply.reqid && strcmp(reply.status_str,'MKE_REPLY_OK')
    fprintf(' read %i 3d points from sensor\n',reply.num_data);
    if reply.num_data>0
        X = [reply.frame.items.X];
    else
        warning('no 3d points, status = %i, status_str = %s',reply.status,reply.status_str);
    end
else
    warning('request not proccedd correctly,, reqidcheck= %i, status = %i, status_str = %s',RequestID==reply.reqid,reply.status,reply.status_str);
end

%% set state back to idle
RequestID = round( rand()*100 );

request = mkeclient.sendRequestTCP(t, 'MKE_REQUEST_SET_STATE',RequestID,'StateType','MKE_STATE_IDLE');
reply   = mkeclient.readReplyTCP(t);

if RequestID==reply.reqid && strcmp(reply.status_str,'MKE_REPLY_OK')
    fprintf(' state changed to idle\n');
else
    warning('unable change state to idle sensing, reqidcheck= %i, status = %i, status_str = %s',RequestID==reply.reqid,reply.status,reply.status_str);
end

%% plot 3d points
if exist('X','var') && ~isempty(X)
    figure();
    dist = sqrt(sum(X.^2));
    scatter3(X(1,:),X(2,:),X(3,:),dist*0+30,dist,'filled');
    axis equal;
    set(gcf, 'Color', [1 1 1]);set(gca, 'Color', [1 1 1]);
    colormap('jet');
    c = colorbar();
    c.Label.String = 'Distance from cam. center [mm]';
    fprintf(' 3d points plotted to figure\n');

    hold on;
    %--- plot coor frame
    frame_size = mean(dist);
    plot3([0 frame_size],[0 0],[0 0],'-r','LineWidth',2); hold on;
    plot3([0 0],[0 frame_size],[0 0],'-g','LineWidth',2);
    plot3([0 0],[0 0],[0 frame_size/2]','-b','LineWidth',2);
end


