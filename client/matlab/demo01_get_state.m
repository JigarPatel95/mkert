%
%  Read and display state of the sensor
%
% MagikEye

%% init
demo_config;
try
    t = tcpclient(address,port,'TimeOut',1);
    mkeclient = MkEClient;
catch
    error(' connection not established, be sure demo_config.m file has correct parameters.');
end

%% request and read reply
RequestID = round( rand()*100 );

request = mkeclient.sendRequestTCP(t, 'MKE_REQUEST_GET_STATE',RequestID);
reply   = mkeclient.readReplyTCP(t);

%% display results
if RequestID==reply.reqid % if reply corresponds to request
    fprintf(' status string = %s\n state string  = %s\n', reply.status_str,reply.state_str);
else
    warning('request not proccedd correctly, reqidcheck= %i, status = %i, status_str = %s',RequestID==reply.reqid,reply.status,reply.status_str);
end

