%
%  1) Set state to depth sensing
%  2) Read data in a infinte loop and plot them
%
% MagikEye

classdef demo04_viewer < MkEClient
    
    %%
    properties (GetAccess = 'public', SetAccess = 'private')
        
        % tcp
        address;
        port;
        tcp_connection;
        tcp_need_send_frame_request;
        sent_ids = [];
        
        % print info?
        show_debug_info = false;
        
        % for all data
        data;
                
        % gui figure handelrs
        hfig = 3434;
        hdata  = [];
        hMkEMeshModel = [];
        
        % colors
        bckgColor  = [1 1 1];
        frgColor   = [1 1 1]*.4;
        
        % gui buttons
        hUIPtsColor;
        hUIPtsColor_label;
        hUIExitButton;
        hUIZoomSlider;
        hUIZoomSlider_label;

    end
    
    %%
    methods
        
        %------------------------------------------------------------------
        % constructor that runs it all
        function obj = demo04_viewer(address_,port_)
            % init connection and its paramters
            demo_config;
            if exist('address_','var') && ~isempty(address_)
                obj.address = address_;
            else
                obj.address = address;
            end
            if exist('port_','var') && ~isempty(port_)
                obj.port = port_;
            else
                obj.port = port;
            end
            
            obj = obj.InitConnection();
            obj.tcp_need_send_frame_request = true;
            
            obj.data.X = [];
            
            % set state for depth sensing
            obj     = obj.SendRequest('MKE_REQUEST_SET_STATE','StateType','MKE_STATE_DEPTH_SENSOR');
            [obj,~] = obj.ReadAllReplies();
            
            % init gui
            obj = obj.InitFig();
            obj = obj.InitUI();
            obj = obj.PlotSensorMeshObject();
            
            % read pts in loop and plot them
            while obj.DoesFigExist() % this also works for closing it
                if obj.tcp_need_send_frame_request
                    obj = obj.SendRequest('MKE_REQUEST_GET_FRAME','FrameType','MKE_FRAME_TYPE_2');
                    obj.tcp_need_send_frame_request = false;
                end
                if obj.IsFrameAvailable()
                    obj = obj.GetFrameData();
                    obj.tcp_need_send_frame_request = true;
                end
                obj = obj.PlotData();
                obj.UpdateFigParams();
                drawnow;
            end
            
            % close fig
            obj = obj.CloseFig();
                       
            % turn off depth sensing
            obj     = obj.SendRequest('MKE_REQUEST_SET_STATE','StateType','MKE_STATE_IDLE');
            [obj,~] = obj.ReadAllReplies();
            
        end
        %------------------------------------------------------------------
        
        
       
        
        %------------------------------------------------------------------
        function obj = InitFig(obj)
            figure(obj.hfig);
            set(obj.hfig, 'Color', obj.bckgColor);
            set(obj.hfig, 'Name','MkE viewer');
            colormap('jet');
            camproj perspective;
            axis equal;
            grid on;
%             camtarget([0 0 0]);
        end
        %------------------------------------------------------------------
        function obj = InitUI(obj)
            obj.hUIPtsColor_label = uicontrol('Style','text',...
                'Position',[5 40 80 20],...
                'BackgroundColor', obj.bckgColor,...
                'ForegroundColor',obj.frgColor,...
                'String','Color mode:');
            obj.hUIPtsColor = uicontrol('Style', 'popup',...
                'String', {'dist','z-axis'},...
                'Position', [100 40 50 20],...
                'BackgroundColor', obj.bckgColor,...
                'ForegroundColor',obj.frgColor);
            obj.hUIExitButton = uicontrol('Style', 'pushbutton',...
                'String', 'Exit',...
                'Position', [10 10 150 20],...
                'BackgroundColor', obj.bckgColor,...
                'ForegroundColor',obj.frgColor,...
                'Callback',@(src,event)obj.QuitLoop(obj,src,event));
            obj.hUIZoomSlider = uicontrol('Style', 'slider',...
                'Min',100,'Max',3500,'Value',1500,...
                'BackgroundColor', obj.bckgColor,...
                'ForegroundColor',obj.frgColor,...
                'Position', [20 80 12 300],...
                'String','');
            obj.hUIZoomSlider_label = uicontrol('Style', 'text',...
                'Position', [5 150 10 150],...
                'BackgroundColor', obj.bckgColor,...
                'ForegroundColor',obj.frgColor,...
                'String','Z o o m');
        end
        %------------------------------------------------------------------
        function col = GetXColor(obj)
            col = [];
            if obj.DoesFigExist()
                color_modes = get(obj.hUIPtsColor,'String');
                color_mode = color_modes{ get(obj.hUIPtsColor,'Value') };
            else
                return 
            end
            
            switch color_mode
                case 'dist'
                    col = sqrt(sum(double(obj.data.X).^2));
                case 'z-axis'
                    col = obj.data.X(3,:);
            end
        end
        %------------------------------------------------------------------
        function fig_exist = DoesFigExist(obj)
            fig_exist = ~isempty(obj.hfig) && ishandle(obj.hfig);
        end
        %------------------------------------------------------------------
        function obj = PlotData(obj)
            if ~isempty(obj.data.X)
                X_color = obj.GetXColor();
                hold on;
                try delete(obj.hdata); catch, end;
                obj.hdata = scatter3(obj.data.X(1,:),obj.data.X(2,:),obj.data.X(3,:),X_color*0+30,X_color,'filled');
            end
        end
        %------------------------------------------------------------------
        function UpdateFigParams(obj)
            zoom_value = get(obj.hUIZoomSlider,'Value');
            axis([-zoom_value zoom_value -zoom_value zoom_value -100 zoom_value]);
            caxis([0 zoom_value]);            
        end
        %------------------------------------------------------------------
        function obj = CloseFig(obj)
            try delete(obj.hdata); catch, end;
            obj.hdata = [];
            try delete(obj.hfig); catch, end;
            obj.hfig = [];
        end
        %------------------------------------------------------------------
        
        
        
        %------------------------------------------------------------------
        function QuitLoop(obj,src,event,~)
            delete(get(event,'Parent'));
            disp('bye');
        end
        %------------------------------------------------------------------
        
        %------------------------------------------------------------------
        function obj = InitConnection(obj)
            obj.tcp_connection = tcpclient(obj.address,obj.port);
            obj.DispText(' connection initilized to address = %s, port = %i\n',obj.address,obj.port);
        end
        %------------------------------------------------------------------
                %------------------------------------------------------------------
        function is_frame_avail = IsFrameAvailable(obj)
            is_frame_avail = true;
            if(obj.tcp_connection.BytesAvailable<48)
                is_frame_avail = false;
            end
        end
        %------------------------------------------------------------------
        
        %------------------------------------------------------------------
        % read results of all reaming requests from the sensor
        function [obj,replies] = ReadAllReplies(obj)
            replies = {};
            try
                while ~isempty(obj.sent_ids)
                    [obj,reply] = obj.ReadReply();
                    replies{end+1} = reply;
                end
            catch
                warning('TCPIP: ReadAllRequests failed due to timeout');
            end
            if ~isempty(obj.sent_ids)
                warning('TCPIP: sent_ids should be empty but it isn''t');
                obj.sent_ids = [];
            end
        end
        %------------------------------------------------------------------
        
        %------------------------------------------------------------------
        function [obj,RequestID] = SendRequest(obj,requestType,varargin)
            RequestID = round( rand()*100 );
            request = obj.sendRequestTCP(obj.tcp_connection, requestType , RequestID , varargin{:});
            obj.sent_ids(end+1) = RequestID;
        end
        %------------------------------------------------------------------
        
        %------------------------------------------------------------------
        % read single replay
        function [obj,reply] = ReadReply(obj)
            reply   = obj.readReplyTCP(obj.tcp_connection);
            obj     = obj.Remove_ReqID_fromList(reply.reqid);
        end
        %------------------------------------------------------------------
        
        %------------------------------------------------------------------
        function obj = Remove_ReqID_fromList(obj,id)
            obj.sent_ids = obj.sent_ids( ~(obj.sent_ids==id) );
        end
        %------------------------------------------------------------------
        
        %------------------------------------------------------------------
        % get data from sensor
        function [obj] = GetFrameData(obj)

            [obj,reply]   = obj.ReadReply();
            
            if strcmp(reply.status_str,'MKE_REPLY_OK')
                if reply.num_data>0 % this also checks if items > 0
                    obj.data.X   = [reply.frame.items.X];
                    obj.DispText(' read %i points\n',size(obj.data.X,2));
                else
                    obj.DispWarrning('no 3d points, status = %i, status_str = %s',reply.status,reply.status_str);
                end
            else
                obj.DispWarrning('request proceed incorrectly, status = %i, status_str = %s',reply.status,reply.status_str);
            end
        end
        %------------------------------------------------------------------
               
        
        %------------------------------------------------------------------
        function DispWarrning(obj,varargin)
            if obj.show_debug_info
                warning(varargin{:});
            end
        end
        %------------------------------------------------------------------
        
        %------------------------------------------------------------------
        function DispText(obj,varargin)
            if obj.show_debug_info
                fprintf(varargin{:});
            end
        end
        %------------------------------------------------------------------
        
        %------------------------------------------------------------------
        function obj = PlotSensorMeshObject(obj)
            ptsBase = [...
                3.5 -3.5 -8 ;...
                -3.5 -3.5 -8;...
                -3.5 -3.5 -5;...
                3.5 -3.5 -5;...
                3.5 3.5 -8 ;...
                -3.5 3.5 -8;...
                -3.5 3.5 -5;...
                3.5 3.5 -5]'*10;
            ptsConnectors = [...
                3.5 3.5 -8 ;...
                -3.5 3.5 -8;...
                -3.5 3.5 -6;...
                3.5 3.5 -6;...
                3.5 6 -8 ;...
                -3.5 6 -8;...
                -3.5 6 -6;...
                3.5 6 -6]'*10;
              hBase = patch('Vertices',ptsBase','Faces',[1 2 3;1 3 4;1 2 6; 6 2 3;3 6 7; 3 7 4; 4 7 8; 8 4 1; 8 1 5; 5 1 6; 5 8 6; 6 8 7],'FaceColor','green','EdgeColor','none');
              hold on;
              hConn = patch('Vertices',ptsConnectors','Faces',[1 2 3;1 3 4;1 2 6; 6 2 3;3 6 7; 3 7 4; 4 7 8; 8 4 1; 8 1 5; 5 1 6; 5 8 6; 6 8 7],'FaceColor',[.8 .8 .8],'EdgeColor','none');
              [X, Y, Z] = cylinder(25);
              Z = Z*(-30)-18;
              hCamLas = mesh(X,Y,Z,'facecolor','k','edgecolor','k');
              
              obj.hMkEMeshModel = [hBase hConn hCamLas];
        end
        %------------------------------------------------------------------
            
        
        
        
    end
        
end
    
    
    
  
