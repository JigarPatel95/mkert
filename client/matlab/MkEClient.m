classdef MkEClient < handle
    
    %%% MkEClient properties ==============================================
    
    properties (SetAccess = protected, GetAccess = protected)
        MkERequestType;     
        MkERequestTypeCallbacks;
        MkETerminateType;
        MkEFrameType;
        MkEStateType;
        MkEDepthSensorPolicyType;
        
        MkEReplyStatus;
        ReplyBuffer;
    end

    properties (SetAccess = protected, GetAccess = public)
        MkERequestTypeKeys;
        MkERequestTypeCodes;

        MkETerminateTypeKeys;
        MkETerminateTypeCodes;
        
        MkEFrameTypeKeys;
        MkEFrameTypeCodes;
        
        MkEStateTypeKeys;
        MkEStateTypeCodes;
        
        MkEReplyStatusKeys;
        MkEReplyStatusCodes;
        
        MkEDepthSensorPolicyTypeKeys;
        MkEDepthSensorPolicyTypeCodes;
    end    
    
    %%% MkEClient static methods ==========================================
    
    methods (Access = public, Static)
        
        function cstr = uint8ToCstr(data_buffer)            
            validateattributes(data_buffer, {'uint8'}, {'vector'});
            
            cstr = '';
            
            for i = 1:numel(data_buffer)
                c = data_buffer(i);
                if ((c >= 32) && (c <= 126))
                    cstr(end + 1) = char(c); %#ok<AGROW>
                else
                    cstr = [cstr sprintf('\\x%s', dec2hex(c))]; %#ok<AGROW>
                end
            end            
        end
    end

    methods (Access = protected, Static)
        
        function string = findTypeString(type, id)
            string = '';
            for i = 1:size(type, 1)
                if (type{i, 2} == id)
                    string = type{i, 1};
                    break;
                end
            end
        end
        
        function magik = getMagik()
            magik = uint8('MKERQ100');
        end
        
        function code4 = valToCode4(val)
            code4 = sprintf('%04d', val);
            code4 = uint8(code4);
        end
                
        function [ret, idx] = readData(data, idx, num_data, type_size, type_name)
            num_bytes = type_size * num_data;
            ret = typecast(data((idx):(idx + num_bytes - 1)), type_name)';
            idx = idx + num_bytes;
        end
        
    end

    %%% MkEClient protected methods =======================================
        
    methods (Access = protected)
       
        %%% Request =======================================================
        
        function request = setupUndefRequest(~, ~, ~)
            request = []; %#ok<NASGU>
            error('MkEClient: Cannot setup MKE_REQUEST_UNDEF request type');
        end
        
        function request = setupNonParamRequest(~, request_code, p)
            request = [MkEClient.getMagik(), ...
                       MkEClient.valToCode4(request_code), ...
                       typecast(uint32(p.Results.RequestID), 'uint8'), ...
                       typecast(uint32(0), 'uint8'), ...
                       zeros(1, 4, 'uint8');];
        end        
        
        function request = setupTerminateRequest(obj, request_code, p)
            pos = find(strcmpi(obj.MkETerminateTypeKeys, p.Results.TerminateMethod), 1, 'first');
            
            request = [MkEClient.getMagik(), ...
                       MkEClient.valToCode4(request_code), ...
                       typecast(uint32(p.Results.RequestID), 'uint8'), ...
                       typecast(uint32(obj.MkETerminateTypeCodes(pos)), 'uint8'), ...
                       zeros(1, 4, 'uint8');];
        end
        
        function request = setupSetStateRequest(obj, request_code, p)
            pos = find(strcmpi(obj.MkEStateTypeKeys, p.Results.StateType), 1, 'first');
            
            request = [MkEClient.getMagik(), ...
                       MkEClient.valToCode4(request_code), ...
                       typecast(uint32(p.Results.RequestID), 'uint8'), ...
                       typecast(uint32(obj.MkEStateTypeCodes(pos)), 'uint8'), ...
                       zeros(1, 4, 'uint8');];
        end
        
        function request = setupSetPolicyRequest(~, request_code, p)
            policy = uint8(p.Results.DepthSensorPolicy);
            if (numel(policy) < 8)
                policy(end + 1) = zeros(1, 1, 'uint8');
            end
            
            request = [MkEClient.getMagik(), ...
                       MkEClient.valToCode4(request_code), ...
                       typecast(uint32(p.Results.RequestID), 'uint8'), ...
                       policy];
        end        
        
        function request = setupStartFramePushRequest(obj, request_code, p)
            pos = find(strcmpi(obj.MkEFrameTypeKeys, p.Results.FrameType), 1, 'first');
            
            request = [MkEClient.getMagik(), ...
                       MkEClient.valToCode4(request_code), ...
                       typecast(uint32(p.Results.RequestID), 'uint8'), ...
                       typecast(uint16(obj.MkEFrameTypeCodes(pos)), 'uint8'), ...
                       zeros(1, 6, 'uint8');];
        end
        
        function request = setupGetFrameRequest(obj, request_code, p)
            pos = find(strcmpi(obj.MkEFrameTypeKeys, p.Results.FrameType), 1, 'first');
            
            request = [MkEClient.getMagik(), ...
                       MkEClient.valToCode4(request_code), ...
                       typecast(uint32(p.Results.RequestID), 'uint8'), ...
                       typecast(uint16(obj.MkEFrameTypeCodes(pos)), 'uint8'), ...
                       zeros(1, 6, 'uint8');];
        end 
        
        function request = setupUploadPackageRequest(~, request_code, p)
            if (~exist(p.Results.UploadPackage, 'file'))
                error('UploadPackage parameter does not name a valid file: %s', p.Results.UploadPackage);
            end
            
            fid = fopen(p.Results.UploadPackage,'r');
            package_data = fread(fid);
            fclose(fid);
            file_size = numel(package_data);
            
            if (file_size >= 2^26)
                 error('Upload package too big: %d (%s)', file_size, p.Results.UploadPackage);
            end
            
            request = [MkEClient.getMagik(), ...
                       MkEClient.valToCode4(request_code), ...
                       typecast(uint32(p.Results.RequestID), 'uint8'), ...
                       typecast(uint32(file_size), 'uint8'), ...
                       zeros(1, 4, 'uint8'), ...
                       package_data'];
        end         
        
        function p = getRequestInputParser(obj)
            p = inputParser;
            
            addRequired(p, 'RequestType', @(x) any(validatestring(x, obj.MkERequestTypeKeys)));
            addRequired(p, 'RequestID', @isnumeric);
            
            addOptional(p, 'TerminateMethod', 'MKE_TERMINATE_BY_EXIT', ...
                @(x) any(validatestring(x, obj.MkETerminateTypeKeys)));
            
            addOptional(p, 'FrameType', 'MKE_FRAME_TYPE_1', ...
                @(x) any(validatestring(x, obj.MkEFrameTypeKeys)));
            
            addOptional(p, 'StateType', 'MKE_STATE_IDLE', ...
                @(x) any(validatestring(x, obj.MkEStateTypeKeys)));
            
            addOptional(p, 'DepthSensorPolicy', 'POLICY00', ...
                @(x) any(validatestring(x, obj.MkEDepthSensorPolicyTypeKeys)));
            
            addOptional(p, 'UploadPackage', '', @ischar);
        end
        
        %%% Reply =========================================================
        
        function [frame, idx] = readFrame(~, data, idx, num_data, frame_type)
            if (frame_type == 1) % FRAME_DATA_TYPE_1
                frame.items = struct(  'X', num2cell(zeros(3, num_data), 1), ...
                    'uid', num2cell(zeros(1, num_data), 1));
                
                for i = 1:num_data
                    [frame.items(i).uid, idx] = MkEClient.readData(data, idx, 1, 2, 'uint16');
                    [frame.items(i).X, idx] = MkEClient.readData(data, idx, 3, 2, 'int16');
                end
            elseif (frame_type == 2)
                % FRAME_DATA_TYPE_2
                frame.items = struct(  'X', num2cell(zeros(3, num_data), 1), ...
                    'uid', num2cell(zeros(1, num_data), 1), ...
                    'tid', num2cell(zeros(2, num_data), 1));
                
                for i = 1:num_data
                    [frame.items(i).uid, idx] = MkEClient.readData(data, idx, 1, 2, 'uint16');
                    [frame.items(i).X, idx] = MkEClient.readData(data, idx, 3, 2, 'int16');
                    [frame.items(i).tid, idx] = MkEClient.readData(data, idx, 2, 2, 'uint16');
                end
            else
                warning('Unrecognized frame type: %d', frame_type);
                return;
            end
            
            [frame.crc32, idx] = MkEClient.readData(data, idx, 1, 4, 'uint32');
        end                        
                
        function resetReplyBuffer(obj)
            obj.ReplyBuffer = struct;
            
            obj.ReplyBuffer.state = 0;
            obj.ReplyBuffer.data = zeros(1, 0, 'uint8');
            obj.ReplyBuffer.bytes_to_read = 40; % sizeof(MkEReply) - sizeof(MkEReply::magik)
        end
        
        function [status, reply_buffer] = next_state(current_byte, ...
                valid_byte, status_ok, status_fail, new_state, reply_buffer)
            
            if (current_byte == valid_byte)
                reply_buffer.state = new_state;
                reply_buffer.data(end + 1) = current_byte;
                status = status_ok;
            else
                status = status_fail;
            end
        end
        
        function status = nextState(obj, current_byte, valid_byte, status_ok, status_fail, new_state)            
            if (current_byte == valid_byte)
                obj.ReplyBuffer.state = new_state;
                obj.ReplyBuffer.data(end + 1) = current_byte;
                status = status_ok;
            else
                status = status_fail;
            end
        end
        
        function [status, buffer_pos] = consumeBytes(obj, data_buffer, buffer_pos)
            current_byte = data_buffer(buffer_pos);
            
            switch (obj.ReplyBuffer.state)
                case 0
                    status = obj.nextState(current_byte, 'M', 0, 0, 1);
                case 1
                    status = obj.nextState(current_byte, 'K', 0, 1, 2);
                case 2
                    status = obj.nextState(current_byte, 'E', 0, 1, 3);
                case 3
                    status = obj.nextState(current_byte, 'R', 0, 1, 4);
                case 4
                    status = obj.nextState(current_byte, 'P', 0, 1, 5);
                case 5
                    status = obj.nextState(current_byte, '1', 0, 1, 6);
                case 6
                    status = obj.nextState(current_byte, '0', 0, 1, 7);
                case 7
                    status = obj.nextState(current_byte, '0', 0, 1, 8);
                case 8
                    obj.ReplyBuffer.data(end + 1) = current_byte;
                    obj.ReplyBuffer.bytes_to_read = obj.ReplyBuffer.bytes_to_read - 1;
                    
                    if (~obj.ReplyBuffer.bytes_to_read)
                        num_bytes = typecast(obj.ReplyBuffer.data(21:24), 'uint32'); % MkEReply::num_bytes
                        
                        if (num_bytes)
                            obj.ReplyBuffer.state = 9;
                            obj.ReplyBuffer.bytes_to_read = num_bytes;
                            status = 0;
                        else
                            status = 2;
                        end
                    else
                        status = 0;
                    end
                case 9
                    obj.ReplyBuffer.data(end + 1) = current_byte;
                    obj.ReplyBuffer.bytes_to_read =obj.ReplyBuffer.bytes_to_read - 1;
                    
                    % Try to read more bytes at once
                    bytes_to_skip = min(obj.ReplyBuffer.bytes_to_read, numel(data_buffer) - buffer_pos);
                    if (bytes_to_skip > 0)
                        obj.ReplyBuffer.data = [ ...
                            obj.ReplyBuffer.data, ...
                            data_buffer((buffer_pos + 1):(buffer_pos + bytes_to_skip)) ...
                            ];
                        
                        obj.ReplyBuffer.bytes_to_read = obj.ReplyBuffer.bytes_to_read - bytes_to_skip;
                        buffer_pos = buffer_pos + bytes_to_skip;
                    end
                    
                    if (~obj.ReplyBuffer.bytes_to_read)
                        status = 2;
                    else
                        status = 0;
                    end
            end
            
            if (status == 0)
                buffer_pos = buffer_pos + 1;
            end
        end
        
        
        function reply = parseFirmwareInfo(obj, reply)
            data = obj.ReplyBuffer.data;
            reply.posix_time = typecast(data(25:32), 'uint64');
            reply.git_commit = typecast(data(33:36), 'uint32');
            reply.fw_ver_major = data(37);
            reply.fw_ver_minor = data(38);
            reply.fw_ver_patch = data(39);
            reply.sys_ver_major = data(40);
            reply.sys_ver_minor = data(41);
            reply.sys_ver_patch = data(42);
            reply.posix_time_str = datetime(reply.posix_time, 'ConvertFrom', 'posixtime');
            reply.git_commit_str = dec2hex(reply.git_commit);
        end
        
        function reply = parseDeviceInfo(obj, reply)
            data = obj.ReplyBuffer.data;
            reply.device_id = data(25:26);
            reply.unit_id = data(27:34);
        end  
        
        function reply = parseDeviceXML(obj, reply)
            data = obj.ReplyBuffer.data;
            if (numel(data) >= 49)
                reply.unit_xml = char(data(49:end));
            else
                reply.unit_xml = '';
            end
        end             
        
        function reply = parseFrame(obj, reply)
            data = obj.ReplyBuffer.data;
            reply.timer = typecast(data(25:32), 'uint64');
            reply.seqn = typecast(data(33:40), 'uint64');
            reply.data_type = typecast(data(41:44), 'uint32');
            reply.frame_type = typecast(data(45:46), 'uint16');
            reply.num_data = typecast(data(47:48), 'uint16');
            [reply.frame, ~] = obj.readFrame(data, 49, reply.num_data, reply.frame_type);
        end        
        
        function reply = parseGetPolicy(obj, reply)
            data = obj.ReplyBuffer.data;

            policy = typecast(data(25:32), 'uint8');
            null_idx = find(policy == 0, 1);
            if (~isempty(null_idx))
                policy = policy(1:null_idx - 1);
            end
            
            reply.policy = MkEClient.uint8ToCstr(policy);
        end
                
        
        function reply = parseReplyBuffer(obj)
            data = obj.ReplyBuffer.data;
            
            reply.magik = char(data(1:8));
            reply.type = str2double(char(data(9:12)));
            reply.status = str2double(char(data(13:16)));
            reply.reqid = typecast(data(17:20), 'uint32');
            reply.num_bytes = typecast(data(21:24), 'uint32');
            
            reply.status_str = obj.getMkEReplyStatusString(reply.status);
                        
            if ((reply.type == 24) && (reply.status == 101))
                % MKE_REQUEST_START_FRAME_PUSH
                reply = obj.parseFrame(reply);
            elseif (reply.status == 200)
                if (reply.type == 26)
                    % MKE_REQUEST_GET_FRAME
                    reply = obj.parseFrame(reply);
                elseif (reply.type == 20)
                    % MKE_REQUEST_GET_STATE
                    reply.state = typecast(data(25:28), 'uint32');
                    reply.state_str = obj.getMkEStateTypeString(reply.state);
                elseif (reply.type == 11)
                    % MKE_REQUEST_GET_FIRMWARE_INFO
                    reply = obj.parseFirmwareInfo(reply);
                elseif (reply.type == 12)
                    % MKE_REQUEST_GET_DEVICE_INFO
                    reply = obj.parseDeviceInfo(reply);
                elseif (reply.type == 13)
                    % MKE_REQUEST_GET_DEVICE_XML
                    reply = obj.parseDeviceXML(reply);
                elseif (reply.type == 22)
                    % MKE_REQUEST_GET_POLICY
                    reply = obj.parseGetPolicy(reply);
                end
            end
        end
        
        %%% Types =========================================================

        function initMkERequestType(obj)
            obj.MkERequestType = { ...
                'MKE_REQUEST_UNDEF', 0, @obj.setupUndefRequest; ...
                'MKE_REQUEST_TERMINATE', 10, @obj.setupTerminateRequest; ...
                'MKE_REQUEST_GET_FIRMWARE_INFO', 11, @obj.setupNonParamRequest; ...
                'MKE_REQUEST_GET_DEVICE_INFO', 12, @obj.setupNonParamRequest; ...
                'MKE_REQUEST_GET_DEVICE_XML', 13, @obj.setupNonParamRequest; ...                
                'MKE_REQUEST_GET_STATE', 20, @obj.setupNonParamRequest; ...
                'MKE_REQUEST_SET_STATE', 21, @obj.setupSetStateRequest; ...                
                'MKE_REQUEST_GET_POLICY', 22, @obj.setupNonParamRequest; ...
                'MKE_REQUEST_SET_POLICY', 23, @obj.setupSetPolicyRequest; ...
                'MKE_REQUEST_LIST_POLICIES', 27, @obj.setupNonParamRequest; ...
                'MKE_REQUEST_START_FRAME_PUSH', 24, @obj.setupStartFramePushRequest; ...
                'MKE_REQUEST_STOP_FRAME_PUSH', 25, @obj.setupNonParamRequest; ...
                'MKE_REQUEST_GET_FRAME', 26, @obj.setupGetFrameRequest; ...
                'MKE_REQUEST_UPLOAD_PACKAGE', 2001, @obj.setupUploadPackageRequest; ...
            };            
            
            obj.MkERequestTypeKeys = obj.MkERequestType(:, 1);
            obj.MkERequestTypeCodes = cell2mat(obj.MkERequestType(:, 2));
        end      
        
        function initMkETerminateType(obj)
            obj.MkETerminateType = { ...
                'MKE_TERMINATE_BY_REBOOT', 1; ...
                'MKE_TERMINATE_BY_SHUTDOWN', 2; ...
            };
            
            obj.MkETerminateTypeKeys = obj.MkETerminateType(:, 1);
            obj.MkETerminateTypeCodes = cell2mat(obj.MkETerminateType(:, 2));
        end

        function initMkEFrameType(obj)
            obj.MkEFrameType = { ...
                'MKE_FRAME_TYPE_1', 1; ...
                'MKE_FRAME_TYPE_2', 2; ...
            };

            obj.MkEFrameTypeKeys = obj.MkEFrameType(:, 1);
            obj.MkEFrameTypeCodes = cell2mat(obj.MkEFrameType(:, 2));        
        end
        
        function initMkEStateType(obj)
            obj.MkEStateType = { ...
                'MKE_STATE_IDLE', 1; ...
                'MKE_STATE_DEPTH_SENSOR', 2; ...
                };
            
            obj.MkEStateTypeKeys = obj.MkEStateType(:, 1);
            obj.MkEStateTypeCodes = cell2mat(obj.MkEStateType(:, 2));        
        end
        
        function initMkEReplyStatus(obj)
            obj.MkEReplyStatus = { ...
                'MKE_REPLY_UNDEF', 0; ...
                'MKE_REPLY_DATA_WILL_START', 100; ...
                'MKE_REPLY_DATA_WILL_CONTINUE', 101; ...
                'MKE_REPLY_DATA_STOPPED', 102; ...
                'MKE_REPLY_OK', 200; ...
                'MKE_REPLY_CLIENT_ERROR', 400; ...
                'MKE_REPLY_CLIENT_MALFORMED_REQUEST', 401; ...
                'MKE_REPLY_CLIENT_ILLEGAL_REQUEST_TYPE', 402; ...
                'MKE_REPLY_CLIENT_REQUEST_DOES_NOT_APPLY', 403; ...
                'MKE_REPLY_SERVER_ERROR', 500; ...
                'MKE_REPLY_SERVER_REQUEST_INTERRUPTED', 501; ...
                'MKE_REPLY_SERVER_BUSY', 502; ...
                'MKE_REPLY_SERVER_INSUFFICIENT_RESOURCES', 503; ...
                };
            
            obj.MkEReplyStatusKeys = obj.MkEReplyStatus(:, 1);
            obj.MkEReplyStatusCodes = cell2mat(obj.MkEReplyStatus(:, 2));                            
        end
        
        function initMkEDepthSensorPolicyType(obj)
            obj.MkEDepthSensorPolicyType = { ...
                'Default', 0; ...
                'POLICY01', 1; ...
                'POLICY02', 2; ...
                'POLICY03', 3; ...                
                };
            
            obj.MkEDepthSensorPolicyTypeKeys = obj.MkEDepthSensorPolicyType(:, 1);
            obj.MkEDepthSensorPolicyTypeCodes = cell2mat(obj.MkEDepthSensorPolicyType(:, 2));        
        end                
    end
    
    
    %%% MkEClient public methods ==========================================
    
    methods (Access = public)

        function obj = MkEClient
            obj.initMkERequestType();
            obj.initMkETerminateType();
            obj.initMkEFrameType();
            obj.initMkEStateType();
            obj.initMkEReplyStatus();
            obj.initMkEDepthSensorPolicyType();
        end
        
        %%% Request =======================================================
        
        function request = setupRequest(obj, RequestType, RequestID, varargin)            
            p = obj.getRequestInputParser();
            parse(p, RequestType, RequestID, varargin{:});
            
            req_pos = find(strcmpi(obj.MkERequestTypeKeys, RequestType), 1, 'first');
            
            try
                req_code = obj.MkERequestTypeCodes(req_pos);
                requestCallback = cell2mat(obj.MkERequestType(req_pos, 3));
            catch
                error('MkEClient: Unrecognized request type');
            end
            
            request = requestCallback(req_code, p);
        end
        
        function request = sendRequestTCP(obj, t, varargin)
            if ((numel(varargin) == 1) && isnumeric(varargin{1}))
                request = varargin{1};
            else
                request = obj.setupRequest(varargin{:});
            end
            
            write(t, request);
        end
        
        %%% Reply =========================================================
        
        function reply = parseReply(obj, data_buffer)
            
            validateattributes(data_buffer, {'uint8'}, {'vector'});
                        
            buffer_pos = 1;
            buffer_size = numel(data_buffer);
            obj.resetReplyBuffer();
            
            reply = [];
            
            while (buffer_pos <= buffer_size)
                
                [status, buffer_pos] = obj.consumeBytes(data_buffer, buffer_pos);
                
                if (status == 1)
                    obj.resetReplyBuffer();
                elseif (status == 2)
                    reply = obj.parseReplyBuffer();
                    obj.resetReplyBuffer();
                    break;
                end
            end
        end
        
        function reply = readReplyTCP(obj, t)            
            data_buffer = read(t, 48);
            num_bytes = typecast(data_buffer(21:24), 'uint32');
            
            if (num_bytes)
                data_buffer = [data_buffer, read(t, num_bytes)];
            end
            
            reply = obj.parseReply(data_buffer);
        end
        
        %%% Types =========================================================
        
        function type_string = getMkERequestTypeString(obj, id)
            type_string = MkEClient.findTypeString(obj.MkERequestType, id);
        end
                
        function type_string = getMkETerminateTypeString(obj, id)
            type_string = MkEClient.findTypeString(obj.MkETerminateType, id);
        end
        
        function type_string = getMkEStateTypeString(obj, id)
            type_string = MkEClient.findTypeString(obj.MkEStateType, id);
        end
        
        function type_string = getMkEFrameTypeString(obj, id)
            type_string = MkEClient.findTypeString(obj.MkEFrameType, id);
        end
        
        function type_string = getMkEReplyStatusString(obj, id)
            type_string = MkEClient.findTypeString(obj.MkEReplyStatus, id);
        end
                
    end
end

