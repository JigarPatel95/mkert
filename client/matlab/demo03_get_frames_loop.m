%
%  1) Set state to depth sensing
%  2) Read 50 dataframes
%  3) Set state to idle
%  4) Show number of data read
%
% MagikEye

%% init
demo_config;
try
    t = tcpclient(address,port,'TimeOut',1);
    mkeclient = MkEClient;
catch
    error(' connection not established, be sure demo_config.m file has correct parameters.');
end
 
%% set state to depth sensing

% send request to chenge state
RequestID = round( rand()*100 );
request = mkeclient.sendRequestTCP(t, 'MKE_REQUEST_SET_STATE' , RequestID , 'StateType','MKE_STATE_DEPTH_SENSOR');

% was it ok?
reply   = mkeclient.readReplyTCP(t);
pause(.1);     % wait a bit to be sure all is really running

if RequestID==reply.reqid && strcmp(reply.status_str,'MKE_REPLY_OK')
    fprintf(' state changed to depth sensing\n');
else
    warning('unable change state to depth sensing, reqidcheck= %i, status = %i, status_str = %s',RequestID==reply.reqid,reply.status,reply.status_str);
end

    
%% get 3d data
time_run      = 5; % run for 5 sec
Npts_read     = 0;  % number of read 3d points
Nframes_read  = 0;  % number frames
fprintf(' runnig for %i sec. reamining:   ',time_run);
RequestID = round( rand()*100 );
mkeclient.sendRequestTCP(t, 'MKE_REQUEST_GET_FRAME' , RequestID , 'FrameType','MKE_FRAME_TYPE_2');

tic
while toc<time_run
    % if data avaliable
    if t.BytesAvailable>=48
        
        % read reply
        reply   = mkeclient.readReplyTCP(t);
        
        % directly send request for next data
        RequestID = round( rand()*100 );
        mkeclient.sendRequestTCP(t, 'MKE_REQUEST_GET_FRAME' , RequestID , 'FrameType','MKE_FRAME_TYPE_2');

        % if data in reply are ok
        if strcmp(reply.status_str,'MKE_REPLY_OK')
            Nframes_read = Nframes_read+1;
            if reply.num_data>0
                Npts_read = Npts_read + size([reply.frame.items.X],2); % remamber just number of points in frame
            end
        end
    end
    fprintf('\b\b\b%3i',round(time_run-toc));
end
fprintf('\n');

% this is last frame to read
reply = mkeclient.readReplyTCP(t);

%% set state back to idle

% send request to chenge state
RequestID = round( rand()*100 );
request = mkeclient.sendRequestTCP(t, 'MKE_REQUEST_SET_STATE' , RequestID , 'StateType','MKE_STATE_IDLE');

% was it ok?
reply   = mkeclient.readReplyTCP(t);

if RequestID==reply.reqid && strcmp(reply.status_str,'MKE_REPLY_OK')
    fprintf(' state changed to idle\n');
else
    warning('unable change state to idle, reqidcheck= %i, status = %i, status_str = %s',RequestID==reply.reqid,reply.status,reply.status_str);
end


%% display results
fprintf(' %i points read in %i frames \n',Npts_read, Nframes_read);



