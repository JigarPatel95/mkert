%
%  This file runs all demos.
%
% MagikEye

%%
fprintf('----------------------------------\n  #01: read state of the sensor\n----------------------------------\n');
demo01_get_state;
pause(1);
close all; clear all; 



%%
fprintf('----------------------------------\n  #02: get single data frame and plot result\n----------------------------------\n');
demo02_get_single_frame;
pause(2.5);
fprintf('  closing window and running next things\n');
pause(1);
close all; clear all; 



%%
fprintf('----------------------------------\n  #03: read 50 data one after each other\n----------------------------------\n');
demo03_get_frames_loop;
pause(1);
close all; clear all; 



%%
fprintf('----------------------------------\n  #04: real-time viewer\n----------------------------------\n');
demo04_viewer;
close all; clear all; 

fprintf('   finished\n----------------------------------\n');