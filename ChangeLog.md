
# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) 
and this project adheres to [Semantic Versioning](http://semver.org/).

## [UNRELEASED]

## [0.2.3]

### Fixed wrong data3dtype by intlut

## [0.2.2]

### Added

- CldbEng: Support adjusting shutter_speed,fps and gain_digital through profile.
- Added PanaCam/Engine

### Fixed

- CldbEng: Fixed returned shutter_speed
- CldbEng: Fixed convertion constant for shutter_speed
- OnSemiEngine: Fixed NO_BUFFER issue

### Known ISSUEs

- Sync laser firing is not working for V4L2Engine
- CLDBEngine return correct exposure number only when lasers are ON

## [0.2.1-rc2]

### Added

- Currently supported VC_MIPI IMX296 and OV9281 for V4L2Engine

### Known ISSUEs

- Sync laser firing is not working for V4L2Engine


## [0.2.1-rc1]

### Added

- Support for V4L2 cameras. Currently written exactly for IMX296
- Added V4L2 engine absed on RSPI engine

### Fixed

- Minimal number of buffer for RSPI engine

### Known ISSUEs

- Sync laser firing is not working for V4L2Engine

## [0.2.0] - fake version

- Created to keep some continuity - a lot of changes happend since 0.1.9

### Added

- Support for OnSemi Demo board + Engine
- Support for FpCam boards + Engine
- Support for CLDB board + Engine
- Basic support for SW managed AutoExposure
- ...

### Changed

- lot of changes
- ...


## [0.1.9]

### Added

- Support for timer how long DEPTH_SENSOR_MODE can be turned on
-- Added 3 config parameters (gov_max_on, gov_min_off, gov_off_factor)
- [mkedd] Support for runtime statistics
- [mkedd] Added NEON code path for CM3 detection speedup

### Changed 

- [mkedd] Most of the profile functionality moved to mkert 


## [0.1.5] - 2017-06-26

### Added

- Dry run support (possible to run DEPTH_SENSOR mode without detection) - option in config
- Added estimator for laser phase for fast FPS - option in config
- Logging messages to the syslog
- Introduced FAIL-SAFE mode
- For slower FPS support to shift phase of laser pattern
- Support for encrypted calibration/configuration
- TODO: - find the rest of changes

### Changed

- More precise time logging in stdout

### Known ISSUEs



## [0.1.4] - 2017-05-02

### Added

- [API] - New request MKE_REQUEST_GET_DEVICE_XML
- [API] - Added new requests: MKE_REQUEST_GET/SET_DEPTH_SENSOR_PROFILE
- [API] - Added new Reserved request MKE_REQUEST_GET_RESERVED_INFO (now empty)
- Support for serial port/s
- Support for UPnP/SSDP services
- Added C++ client (synchronized)
- Support for old/new IMX219 sensor (getting sensor name and it's maximal resolution)
- Support for new HW synchronization (CMLDC library)
- Introduced new parameter to config offset of strobe
- Automatically choose strobe type by board
- Internal request MKE_REQUEST_DOWNLOAD_FILE

### Changed

- [API] - Updated MKE_REQUEST_GET_DEVICE_INFO request
- [API] - Changed MKE_REQUEST_GET/SET_LASER request (changed format and added offset)
- Support for new version of RPSYNC library

### Fixed

- Fixed LoadEngine rotate just first image in directory
- Fixed missing stride functionality in LOAD LoadEngine

### Known ISSUEs

- Missing timeout support for C++ serial client (unable to detect disconnection)
- Missing streaming support
- Unable to restart only application (problem with permission?)x


## [0.1.3] - 2017-03-09

### Added

- [API] - Support for dynamic requests (uploading bigger payload to the device)
- [API] - Implemented request to upload package (for example to update FW)
- Implemented GET_DEVICE_INFO into python client

### Changed

- [API] - Extended GET_DEBUG_IMAGED reply by laser pattern (all clients modified)
- [API] - Changed frame types for reserved mode
- Build-in commit number just for unofficial builds. Official ones have filled 0x00000000
- In service mode possible to change ISO
- Fixed mode is really fixed. Improvement of fixed mode.
- Supported loading PGM files with comment 
- ?? Rearranged connection between laser-pattern and detector no.


## [0.1.2] - 2017-02-19

### Added 

- [API] - New request GET_DEVICE_INFO
- [API] - New request SET_GAIN (parameter ISO value)
- Python tests for testing API functionality
- Camera exposure initialization to stabilize digital gain

### Changed

- [API] Format of FIRMWARE_INFO reply

### Fixed

- Camera exposure should be more stable


## [0.1.1] - 2017-02-10

### Added 

- MATLAB client for customers
- Python client for customers

### Changed

- Procedure of terminating

### Fixed

- Memory leak during releasing StartFramePush request



## [0.1.0] - who knows

- Initial version of application 
- Most of the public functionality is implemented, and quite tested
