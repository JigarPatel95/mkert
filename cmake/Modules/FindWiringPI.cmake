# - Try to find Raspberry PI wiring libraries and includes
#
# Once done this will define
#  WiringPI_FOUND - System has Raspberry PI wiring
#  WiringPI_INCLUDE_DIRS - The Raspberry PI wiring include directories
#  WiringPI_LIBRARIES - The libraries needed to use Raspberry PI wiring
#  WiringPI_DEFINITIONS - Compile switches used by wiringPi

FIND_PATH(WiringPI_INCLUDE_DIR wiringPi.h)

# prefer static libs ? no problem
IF ( DEFINED WiringPI_USE_STATIC_LIBS AND WiringPI_USE_STATIC_LIBS )
    FIND_LIBRARY(WiringPI_LIBRARY NAMES libwiringPi.a)
ELSE ( DEFINED WiringPI_USE_STATIC_LIBS AND WiringPI_USE_STATIC_LIBS )
    FIND_LIBRARY(WiringPI_LIBRARY NAMES wiringPi)
ENDIF ( DEFINED WiringPI_USE_STATIC_LIBS AND WiringPI_USE_STATIC_LIBS )

#
#

INCLUDE(FindPackageHandleStandardArgs)

# handle the QUIETLY and REQUIRED arguments and set WiringPI_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(WiringPI  DEFAULT_MSG
                                  WiringPI_LIBRARY WiringPI_INCLUDE_DIR)
                                  
MARK_AS_ADVANCED(WiringPI_INCLUDE_DIR WiringPI_LIBRARY)

SET(WiringPI_INCLUDE_DIRS ${WiringPI_INCLUDE_DIR})
SET(WiringPI_LIBRARIES ${WiringPI_LIBRARY})

