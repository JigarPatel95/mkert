# - Try to find FTD2XX libraries and includes

#
# Input: 
#  FTD2XX_DIR - path where folder release from package .gz has been unpacked
#                   .gz downloaded from http://www.ftdichip.com/Drivers/D2XX.htm
#
# Once done this will define
#  FTD2XX_FOUND - System has FTD2XX
#  FTD2XX_INCLUDE_DIRS - Library FTD2XX include directories
#  FTD2XX_LIBRARIES - The libraries needed to use FTD2XX

FIND_PATH(FTD2XX_INCLUDE_DIR ftd2xx.h 
            HINTS ${FTD2XX_DIR} 
            PATH_SUFFIXES include)

# prefer static libs ? no problem
IF ( DEFINED FTD2XX_USE_STATIC_LIBS AND FTD2XX_USE_STATIC_LIBS )
    IF (WIN32)
        FIND_LIBRARY(FTD2XX_STATIC_LIBRARY
                     NAMES ftd2xx.lib
                     HINTS ${FTD2XX_DIR}
                     PATH_SUFFIXES "Static\\i386")
         FIND_LIBRARY(FTD2XX_LIBRARY
                      NAMES ftd2xx.lib
                      HINTS ${FTD2XX_DIR}
                      PATH_SUFFIXES "i386")
    ELSE (WIN32)
        FIND_LIBRARY(FTD2XX_LIBRARY
                     NAMES libftd2xx.a
                     HINTS ${FTD2XX_DIR}
                     PATH_SUFFIXES build)
    ENDIF (WIN32)
ELSE ( DEFINED FTD2XX_USE_STATIC_LIBS AND FTD2XX_USE_STATIC_LIBS )
    IF (WIN32)
        FIND_LIBRARY(FTD2XX_LIBRARY
                     NAMES ftd2xx.dll
                     HINTS ${FTD2XX_DIR}
                     PATH_SUFFIXES "i386")
    ELSE (WIN32)
        FIND_LIBRARY(FTD2XX_LIBRARY
                     NAMES ftd2xx
                     HINTS ${FTD2XX_DIR}
                     PATH_SUFFIXES build)
    ENDIF (WIN32)
ENDIF ( DEFINED FTD2XX_USE_STATIC_LIBS AND FTD2XX_USE_STATIC_LIBS )

#
#

INCLUDE(FindPackageHandleStandardArgs)

# handle the QUIETLY and REQUIRED arguments and set FTD2XX_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(FTD2XX  DEFAULT_MSG
                                  FTD2XX_LIBRARY FTD2XX_INCLUDE_DIR)
                                  
MARK_AS_ADVANCED(FTD2XX_INCLUDE_DIR FTD2XX_LIBRARY)

SET(FTD2XX_INCLUDE_DIRS ${FTD2XX_INCLUDE_DIR})
SET(FTD2XX_LIBRARIES ${FTD2XX_LIBRARY} ${FTD2XX_STATIC_LIBRARY})


