# - Try to find CLDB libraries and includes

#
# Input: 
#  CLDB_DIR - path where folder release from package .gz has been unpacked
#                   .gz downloaded from http://www.ftdichip.com/Drivers/D2XX.htm
#
# Once done this will define
#  CLDB_FOUND - System has CLDB
#  CLDB_INCLUDE_DIRS - Library CLDB include directories
#  CLDB_LIBRARIES - The libraries needed to use CLDB

FIND_PATH(CLDB_INCLUDE_DIR cldblib.h 
            HINTS ${CLDB_DIR} 
            PATH_SUFFIXES include h)

# prefer static libs ? no problem
IF ( DEFINED CLDB_USE_STATIC_LIBS AND CLDB_USE_STATIC_LIBS )
    IF (WIN32)
        FIND_LIBRARY(CLDB_STATIC_LIBRARY
                     NAMES libcldb.lib
                     HINTS ${CLDB_DIR}
                     PATH_SUFFIXES "lib")
         FIND_LIBRARY(CLDB_LIBRARY
                      NAMES libcldb.lib
                      HINTS ${CLDB_DIR}
                      PATH_SUFFIXES "i386")
    ELSE (WIN32)
        FIND_LIBRARY(CLDB_LIBRARY
                     NAMES libcldb.a
                     HINTS ${CLDB_DIR}
                     PATH_SUFFIXES lib)
    ENDIF (WIN32)
ELSE ( DEFINED CLDB_USE_STATIC_LIBS AND CLDB_USE_STATIC_LIBS )
    IF (WIN32)
        FIND_LIBRARY(CLDB_LIBRARY
                     NAMES libcldb.dll
                     HINTS ${CLDB_DIR}
                     PATH_SUFFIXES lib)
    ELSE (WIN32)
        FIND_LIBRARY(CLDB_LIBRARY
                     NAMES cldb
                     HINTS ${CLDB_DIR}
                     PATH_SUFFIXES lib)
    ENDIF (WIN32)
ENDIF ( DEFINED CLDB_USE_STATIC_LIBS AND CLDB_USE_STATIC_LIBS )

#
#

INCLUDE(FindPackageHandleStandardArgs)

# handle the QUIETLY and REQUIRED arguments and set CLDB_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(CLDB  DEFAULT_MSG
                                  CLDB_LIBRARY CLDB_INCLUDE_DIR)
                                  
MARK_AS_ADVANCED(CLDB_INCLUDE_DIR CLDB_LIBRARY)

SET(CLDB_INCLUDE_DIRS ${CLDB_INCLUDE_DIR})
SET(CLDB_LIBRARIES ${CLDB_LIBRARY} ${CLDB_STATIC_LIBRARY})



