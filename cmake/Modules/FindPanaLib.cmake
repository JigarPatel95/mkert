# - Try to find PanaLib libraries and includes

#
# Input: 
#  PanaLib_DIR - path to PanaLib folder is located
#
# Once done this will define
#  PanaLib_FOUND - System has PanaLib
#  PanaLib_INCLUDE_DIRS - PanaLib include directories
#  PanaLib_LIBRARIES - The libraries needed to use PanaLib

FIND_PATH(PanaLib_INCLUDE_DIR PanaLib.h 
            HINTS ${PanaLib_DIR} 
            PATH_SUFFIXES libs/PanaLib)

FIND_PATH(OpenCV_INCLUDE_DIR opencv2/opencv.hpp
            HINTS ${PanaLib_DIR} 
            PATH_SUFFIXES libs/OpenCV/include)

IF (WIN32)
    FIND_LIBRARY(PanaLib_LIBRARY
                    NAMES PanaLib.lib
                    HINTS ${PanaLib_DIR}
                    PATH_SUFFIXES libs/PanaLib)
    FIND_LIBRARY(OpenCV_LIBRARY
                    NAMES opencv_core2411.lib
                    HINTS ${PanaLib_DIR}
                    PATH_SUFFIXES libs/OpenCV/x86_vc11_lib)
ELSE (WIN32)
    FIND_LIBRARY(PanaLib_LIBRARY
                    NAMES PanaLib
                    HINTS ${PanaLib_DIR}
                    PATH_SUFFIXES libs/PanaLib)
    FIND_LIBRARY(OpenCV_LIBRARY
                    NAMES opencv_core2411
                    HINTS ${PanaLib_DIR}
                    PATH_SUFFIXES libs/OpenCV/x86_vc11_lib)
ENDIF (WIN32)

#
#

INCLUDE(FindPackageHandleStandardArgs)

# handle the QUIETLY and REQUIRED arguments and set PanaLib_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(PanaLib DEFAULT_MSG
                                  PanaLib_LIBRARY PanaLib_INCLUDE_DIR OpenCV_INCLUDE_DIR)
                                                     
SET(PanaLib_LIBRARIES ${PanaLib_LIBRARY} ${OpenCV_LIBRARY})
SET(PanaLib_INCLUDE_DIRS ${PanaLib_INCLUDE_DIR} ${OpenCV_INCLUDE_DIR})

MARK_AS_ADVANCED(PanaLib_INCLUDE_DIR)
