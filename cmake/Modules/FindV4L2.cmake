# - Try to find V4L2 headers

#
# Input: 
#  KERNEL_DIR - path where kernel headers are located
#
# Once done this will define
#  V4L2_FOUND - System has V4L2 headers
#  V4L2_INCLUDE_DIRS - V4L2 include directories

FIND_PATH(V4L2_INCLUDE_DIR
            NAMES linux/videodev2.h
            HINTS ${KERNEL_DIR} 
            PATH_SUFFIXES include)

#
#

INCLUDE(FindPackageHandleStandardArgs)

# handle the QUIETLY and REQUIRED arguments and set FTDI_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(V4L2 DEFAULT_MSG
                                  V4L2_INCLUDE_DIR)
                                  
MARK_AS_ADVANCED(V4L2_INCLUDE_DIR)

SET(V4L2_INCLUDE_DIRS ${V4L2_INCLUDE_DIR})
