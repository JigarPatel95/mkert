# - Try to find ApBase libraries and includes

#
# Input: 
#  ApBase_DIR - path to Aptina_Imaging folder is located
#
# Once done this will define
#  ApBase_FOUND - System has ApBase
#  ApBase_INCLUDE_DIRS - Library ApBase include directories
#  ApBase_LIBRARIES - The libraries needed to use ApBase

FIND_PATH(ApBase_INCLUDE_DIR apbase.h 
            HINTS ${ApBase_DIR} 
            PATH_SUFFIXES include)

IF (WIN32)
    FIND_LIBRARY(ApBase_LIBRARY
                    NAMES apbase.lib
                    HINTS ${ApBase_DIR}
                    PATH_SUFFIXES "lib")
    FIND_LIBRARY(MIDLIB2_LIBRARY
                    NAMES midlib2.lib 
                    HINTS ${ApBase_DIR}
                    PATH_SUFFIXES "lib")
ELSE (WIN32)
    FIND_LIBRARY(ApBase_LIBRARY
                    NAMES apbase
                    HINTS ${ApBase_DIR}
                    PATH_SUFFIXES lib)
    FIND_LIBRARY(MIDLIB2_LIBRARY
                    NAMES midlib2
                    HINTS ${ApBase_DIR}
                    PATH_SUFFIXES lib)
ENDIF (WIN32)

#
#

INCLUDE(FindPackageHandleStandardArgs)

# handle the QUIETLY and REQUIRED arguments and set FTD2XX_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(ApBase  DEFAULT_MSG
                                  ApBase_LIBRARY MIDLIB2_LIBRARY ApBase_INCLUDE_DIR)
                                                     
SET(ApBase_LIBRARIES ${ApBase_LIBRARY} ${MIDLIB2_LIBRARY})
SET(ApBase_INCLUDE_DIRS ${ApBase_INCLUDE_DIR})
UNSET(MIDLIB2_LIBRARY)

MARK_AS_ADVANCED(ApBase_INCLUDE_DIR ApBase_LIBRARIES)




