# - Try to find libftdi libraries and includes

#
# Input: 
#  FTDI_DIR - path where folder release from package .gz has been unpacked
#                   .gz downloaded from https://www.intra2net.com/en/developer/libftdi/download.php
#
# Once done this will define
#  FTDI_FOUND - System has libftdi
#  FTDI_INCLUDE_DIRS - Library libftdi include directories
#  FTDI_LIBRARIES - The libraries needed to use libftdi

FIND_PATH(FTDI_INCLUDE_DIR
            NAMES libftdi1/ftdi.h 
            HINTS ${FTDI_DIR} 
            PATH_SUFFIXES include)

FIND_LIBRARY(FTDI_LIBRARY
            NAMES libftdi1.a
            HINTS ${FTDI_DIR}
            PATH_SUFFIXES lib)

#
#

message(STATUS "[ ${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE} ] "
    " ${FTDI_DIR} "
    " FTDI_INCLUDE_DIR=${FTDI_INCLUDE_DIR}"
    " FTDI_LIBRARY=${FTDI_LIBRARY}")

INCLUDE(FindPackageHandleStandardArgs)

# handle the QUIETLY and REQUIRED arguments and set FTDI_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(FTDI DEFAULT_MSG
                                  FTDI_LIBRARY FTDI_INCLUDE_DIR)
                                  
MARK_AS_ADVANCED(FTDI_INCLUDE_DIR FTDI_LIBRARY)

SET(FTDI_INCLUDE_DIRS ${FTDI_INCLUDE_DIR})
SET(FTDI_LIBRARIES ${FTDI_LIBRARY})
