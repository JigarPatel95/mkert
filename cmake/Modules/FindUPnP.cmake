# - Try to find Upnp libraries and includes
#
# Once done this will define
#  UPnP_FOUND - System has Raspberry PI wiring
#  UPnP_INCLUDE_DIRS - The Raspberry PI wiring include directories
#  UPnP_LIBRARIES - The libraries needed to use Raspberry PI wiring
#  UPnP_DEFINITIONS - Compile switches used by wiringPi

FIND_PATH(UPnP_INCLUDE_DIR upnp/upnp.h)

# prefer static libs ? no problem
IF ( DEFINED UPnP_USE_STATIC_LIBS AND UPnP_USE_STATIC_LIBS )
    FIND_LIBRARY(UPnP_LIBRARY NAMES libupnp.a)
    FIND_LIBRARY(IXML_LIBRARY NAMES libixml.a)
    FIND_LIBRARY(TUTIL_LIBRARY NAMES libthreadutil.a)
ELSE ( DEFINED UPnP_USE_STATIC_LIBS AND UPnP_USE_STATIC_LIBS )
    FIND_LIBRARY(UPnP_LIBRARY NAMES libupnp)
    FIND_LIBRARY(IXML_LIBRARY NAMES libixml)
    FIND_LIBRARY(TUTIL_LIBRARY NAMES libthreadutil)
ENDIF ( DEFINED UPnP_USE_STATIC_LIBS AND UPnP_USE_STATIC_LIBS )

#
#

INCLUDE(FindPackageHandleStandardArgs)

# handle the QUIETLY and REQUIRED arguments and set UPnPs_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(UPnP  DEFAULT_MSG
                                  UPnP_LIBRARY UPnP_INCLUDE_DIR)

# handle the QUIETLY and REQUIRED arguments and set UPnPs_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(UPnP  DEFAULT_MSG
                                  IXML_LIBRARY UPnP_INCLUDE_DIR)

# handle the QUIETLY and REQUIRED arguments and set UPnPs_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(UPnP  DEFAULT_MSG
                                  TUTIL_LIBRARY UPnP_INCLUDE_DIR)
                                  
MARK_AS_ADVANCED(UPnP_INCLUDE_DIR UPnP_LIBRARY)

SET(UPnP_INCLUDE_DIRS ${UPnP_INCLUDE_DIR})
SET(UPnP_LIBRARIES ${UPnP_LIBRARY} ${IXML_LIBRARY} ${TUTIL_LIBRARY})
