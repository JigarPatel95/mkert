#!/bin/bash
#
# This script checkout current git tree to selected tag and create official release
#

# ------------------------------------------------------------------------------

# check input arguments

if [ "$#" -ne 1 ];then
    echo "Illegal number of parameters."
    echo "Should be one argument - tagged version."
    exit 1
fi

# ------------------------------------------------------------------------------

# save WIP

git stash save "WIP before official release $1"

# make a checkout

git checkout $1

if [ $? -ne 0 ];then
    echo "Unknown tag $1"
    exit 1
fi

# ------------------------------------------------------------------------------

# make a rebuild

make clean
make mkert

# ------------------------------------------------------------------------------

# prepare relase directory and copy releases into it

if [ -e 'release' ];then
  rm -Rf release
fi

mkdir -p 'release'
cp src/mkert release/
cp ../update/*.sh release/

# ------------------------------------------------------------------------------
