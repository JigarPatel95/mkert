#!/bin/python 
#
# client_pushframes - start pushing frames from device and show them
# 
# Copyright (c) 2017, Magik-Eye s.r.o., Prague
# author: Ondra Fisar
#
# ------------------------------------------------------------------------------

# imports

import sys
import struct
import time
import os

# nasty but avoid repeat code
sys.path.append(os.path.dirname(__file__) + '../client/python/')

from mkeclient import MkeClient

# ------------------------------------------------------------------------------

# check arguments

if len(sys.argv) < 2:
  print("Bad number arguments.\n Usage %s IP port [--verbose]" % sys.argv[0])
  exit(1)

host = sys.argv[1]
port = int(sys.argv[2])

# ------------------------------------------------------------------------------

client = None

try:  
  client = MkeClient(host, port, '--verbose' in sys.argv)

  # go to state depth sensing

  state = client.get_state() 

  if state != MkeClient.STATE_DEPTH_SENSOR:

    print("Invalid state, let's change it to DEPTH_SENSOR")
    
    # firstly set state to IDLE
    
    if state != MkeClient.STATE_IDLE:
      client.set_state(MkeClient.STATE_IDLE)
      
    # then change state to DEPTH_SENSOR
    
    client.set_state(MkeClient.STATE_DEPTH_SENSOR)

  else:
    
    print("Already in correct state.")

  # main body ------------------------------------------------------------------

  timeout = 1
  for i in range(20):
    # prepare variables

    start_time = time.time()          # start of capturing
    num = 0                           # number of received images
    frame_type = 1                    # used frame type
    
    # start pushing frames
    
    start_seq_id = client.start_frame_push(frame_type)
    stop_seq_id = None

    while True:
        
        # send stop push after num frames
        
        if stop_seq_id is None and (time.time() - start_time) >= timeout:  
            stop_seq_id = client.stop_frame_push()

        # get next frame from the bus
        
        frame = client.get_pushed_frame(start_seq_id, stop_seq_id)
        
        # None denotes no more data
        
        if frame is None:
            print("Correctly finished loop")
            
            # all replies should be processed
            break
    
        num += 1
        
        sys.stdout.write('\r%d : %.2f, elapsed: %.1fs, remaining: %.1fs     ' % 
                        (num,(num/(time.time()-start_time)), time.time() - start_time, time.time() - start_time - timeout))
        sys.stdout.flush()
    
    print("Average displayed FPS: %.2f" % (num/(time.time()-start_time)))

    timeout *= 2                  # when should be stop_frame_push called

  print("Correct termination");

except Exception as e:
  print("An error occured: ", str(e))
finally:
  if client:
    client.set_state(MkeClient.STATE_IDLE)

# ------------------------------------------------------------------------------
