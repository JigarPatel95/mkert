import unittest

import sys
import os
import subprocess
import time

import mketest as cfg
from mketest import PermanentConnTestCase
from mkepriv import MkePrivClient


class TestBasicCalls(PermanentConnTestCase):

    def test_states(self):
        
        # at the beggining sensor should be in STATE_IDLE
        self.assertEqual(self.client.get_state(), MkePrivClient.STATE_IDLE)

        # apply to same state - not apply
        self.assertEqual(self.client.set_state(MkePrivClient.STATE_IDLE),
                         MkePrivClient.STATUS_CLIENT_REQUEST_DOES_NOT_APPLY);
        
        # change to DEPTH SENSOR
        self.assertEqual(self.client.set_state(MkePrivClient.STATE_DEPTH_SENSOR),
                         MkePrivClient.STATUS_OK);
        self.assertEqual(self.client.get_state(), MkePrivClient.STATE_DEPTH_SENSOR)

        # apply to same state - not apply
        self.assertEqual(self.client.set_state(MkePrivClient.STATE_DEPTH_SENSOR),
                         MkePrivClient.STATUS_CLIENT_REQUEST_DOES_NOT_APPLY);

        # go back to idle
        self.assertEqual(self.client.set_state(MkePrivClient.STATE_IDLE),
                         MkePrivClient.STATUS_OK);

    @unittest.skipIf(not cfg.reserved_mode, "Checking RESERVED MODE is disabled")
    def test_states_reserved(self):
        
        # at the beggining sensor should be in STATE_IDLE
        self.assertEqual(self.client.get_state(), MkePrivClient.STATE_IDLE)

        # change to SERVICE
        self.assertEqual(self.client.set_state(MkePrivClient.STATE_SERVICE),
                         MkePrivClient.STATUS_OK);
        self.assertEqual(self.client.get_state(), MkePrivClient.STATE_SERVICE)

        # apply to same state - not apply
        self.assertEqual(self.client.set_state(MkePrivClient.STATE_SERVICE),
                         MkePrivClient.STATUS_CLIENT_REQUEST_DOES_NOT_APPLY);
        # apply to forbidden state - not apply
        self.assertEqual(self.client.set_state(MkePrivClient.STATE_DEPTH_SENSOR),
                         MkePrivClient.STATUS_CLIENT_REQUEST_DOES_NOT_APPLY);
        # go back to idle
        self.assertEqual(self.client.set_state(MkePrivClient.STATE_IDLE),
                         MkePrivClient.STATUS_OK);
        
        # change to DEPTH SENSOR
        self.assertEqual(self.client.set_state(MkePrivClient.STATE_DEPTH_SENSOR),
                         MkePrivClient.STATUS_OK);

        # apply to forbidden state - not apply
        self.assertEqual(self.client.set_state(MkePrivClient.STATE_SERVICE),
                         MkePrivClient.STATUS_CLIENT_REQUEST_DOES_NOT_APPLY);

        # go back to idle
        self.assertEqual(self.client.set_state(MkePrivClient.STATE_IDLE),
                         MkePrivClient.STATUS_OK);

    def test_firmware(self):

        fw = self.client.get_fw_info()
        self.assertEqual(fw['fw_version']['major'], 0)
        self.assertEqual(fw['fw_version']['minor'], 1)
        self.assertEqual(fw['fw_version']['patch'], 3)
        
        self.assertEqual(
            subprocess.call(
                ('git show %07x -q' % fw['git_commit']).split(), 
                stdout=cfg.callout), 0)

if __name__ == '__main__':
    unittest.main()