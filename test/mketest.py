#
#
#

import sys
import os
import configparser
import subprocess
import unittest
import time

# nasty but avoid repeat code
sys.path.append(os.path.dirname(__file__) + '/../client/python/reserved_api')

from mkepriv import MkePrivClient

#

def __load_config(path):
    config = configparser.ConfigParser()
    config.read(path)
    comm = config['Comm']
    
    if 'hostname' in comm:
        global hostname
        hostname = comm['hostname']
    
    if 'port' in comm:
        global port
        port = int(comm['port'])
    
    if 'cmd' in comm:
        global cmd
        cmd = comm['cmd']
    
    if 'verbose' in comm:
        global verbose
        verbose = int(comm['verbose']) > 0
    
    if 'online' in comm:
        global online
        online = int(comm['online']) > 0
        
    if 'reserved_mode' in comm:
        global reserved_mode
        reserved_mode = int(comm['reserved_mode']) > 0
        

    
# parse my own arguments

if not 'hostname' in globals():

    verbose = False
    online = False
    reserved_mode = False

    to_remove = []
    for a in sys.argv:
        
        if a.startswith("--config="):
            __load_config(a[9:])
        elif a.startswith("--hostname="):
            hostname = a[11:]
        elif a.startswith("--port="):
            port = int(a[7:])
        elif a.startswith("--cmd="):
            cmd = a[6:]
        elif a == "--verbose":
            verbose = True
        elif a == "--online":
            online = True
        elif a == "--reserved_mode":
            reserved_mode = True
        else:
            continue
        
        to_remove.append(a)
        
    
    for r in to_remove:    
        sys.argv.remove(r)

    if not 'port' in locals():
        port = 8888                     # default
    if not 'hostname' in locals():
        hostname = 'localhost'          # default
    if not 'cmd' in locals():
        raise RuntimeError("Missing argument --cmd=<Command>")
    
    cmd = cmd.split()
    callout = None if verbose else subprocess.PIPE 

def mke_check_output(output):
    if output is None:
        return;
    
    warns = []
    for r in output.split(b'\n'):
        if r.find(b'WARNING') >= 0:
            warns.append(str(r))
            
    if len(warns) > 0:
      raise RuntimeError(",".join(warns))
    
# class

class PermanentConnTestCase(unittest.TestCase):
    
    @classmethod
    def setUpClass(self):
        self.process = subprocess.Popen(cmd, stdout = callout)
        time.sleep(2)
        self.client = MkePrivClient(hostname, port, verbose)
    
    @classmethod
    def tearDownClass(self):
        # stop application
        
        self.client.terminate(MkePrivClient.TERMINATE_EXIT);

        (out, err) = self.process.communicate();
        ret = self.process.wait()
        
        if ret != 0:
          raise RuntimeError("Application terminates with code %d" % ret)

        mke_check_output(out)
