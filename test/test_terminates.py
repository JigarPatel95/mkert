import unittest

import sys
import os
import subprocess
import time
import mketest as cfg
from mkepriv import MkePrivClient

class TestTerminates(unittest.TestCase):

    def setUp(self):
        self.on_process = None
        self.off_process = None
        pass
    
    def tearDown(self):
        if self.on_process:
            self.on_process.kill()
        if self.off_process:
            self.off_process.kill()
        pass

    def test_online(self):
        
        out = None if cfg.verbose in sys.argv else subprocess.PIPE
        self.on_process = subprocess.Popen(cfg.cmd, stdout=out)
        time.sleep(3)
        client = MkePrivClient(cfg.hostname, cfg.port, cfg.verbose)

        client.terminate(MkePrivClient.TERMINATE_EXIT)
        (out, err) = self.on_process.communicate()
        self.assertTrue(self.on_process.wait() == 0)
        self.on_process = None

        cfg.mke_check_output(out)

    @unittest.skipIf(cfg.online, "works just in offline test")
    def test_offline(self):
        
        out = None if cfg.verbose in sys.argv else subprocess.PIPE
        self.off_process = subprocess.Popen(cfg.cmd, stdout=out)
        time.sleep(1)
        self.off_process.send_signal(2)
        (out, err) = self.off_process.communicate()
        self.assertTrue(self.off_process.wait() == 0)
        self.off_process = None
        
        cfg.mke_check_output(out)

if __name__ == '__main__':
    unittest.main()