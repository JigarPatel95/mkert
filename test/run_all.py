# global imports

import unittest

# test imports

import test_terminates
import test_basic_calls
import test_frames
import test_attack

# list of test cases

test_cases = [ test_terminates.TestTerminates,
               test_basic_calls.TestBasicCalls,
               test_frames.TestFramesCalls,
               test_attack.TestAttack]

# load tests cases
suites = []
for t in test_cases:
    suites.append(unittest.TestLoader().loadTestsFromTestCase(t))
    
# run all tests

big_suite = unittest.TestSuite(suites)
unittest.TextTestRunner().run(big_suite)