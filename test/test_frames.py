import unittest

import sys
import os
import subprocess
import time
import copy

import mketest as cfg
from mketest import PermanentConnTestCase
from mkepriv import MkePrivClient


class TestTerminateWhenPush(unittest.TestCase):

    def test_pushterminate(self):
        
        # change to DEPTH SENSOR
        self.assertEqual(self.client.set_state(MkePrivClient.STATE_DEPTH_SENSOR),
                         MkePrivClient.STATUS_OK);

        start_seq_id = self.client.start_frame_push(2)
        stop_seq_id = None
        
        num = 0
        last_frame = None
        while True:
           
            frame = self.client.get_pushed_frame(start_seq_id, stop_seq_id)
                
            if frame is None:
                break;
            
            if not (last_frame is None):
                self.assertEqual(last_frame['seqn']+1, frame['seqn'])
                self.assertTrue(last_frame['timer'] < frame['timer'])
            
            num += 1
            last_frame = frame

    def setUp(self):
        cmd = copy.deepcopy(cfg.cmd)
        cmd.extend("& sleep 10 && sudo killall mkert".split())
        self.process = subprocess.Popen(cmd, stdout = cfg.callout)
        time.sleep(2)
        self.client = MkePrivClient(cfg.hostname, cfg.port, cfg.verbose)
        

    def tearDown(self):
        # stop application

        (out, err) = self.process.communicate();
        ret = self.process.wait()
        
        if ret != 0:
          raise RuntimeError("Application terminates with code %d" % ret)

        cfg.mke_check_output(out)


class TestFramesCalls(PermanentConnTestCase):
    
    NUM_FRAMES = 20
    NUM_LOOPS = 5

    def setUp(self):
        
        self.client.set_state(MkePrivClient.STATE_IDLE);

    def test_pollframes(self):
        
        # change to DEPTH SENSOR
        self.assertEqual(self.client.set_state(MkePrivClient.STATE_DEPTH_SENSOR),
                         MkePrivClient.STATUS_OK);

            
        last_frame = None
        # poll NUM_FRAMES frames
        for i in range(self.NUM_FRAMES):
            frame = self.client.get_frame(2)
            
            if not (last_frame is None):
                with self.subTest(i=i):
                    self.assertTrue(last_frame['seqn'] <= frame['seqn'])
                    self.assertTrue(last_frame['timer'] <= frame['timer'])                
            
        # go back to IDLE
        self.assertEqual(self.client.set_state(MkePrivClient.STATE_IDLE),
                         MkePrivClient.STATUS_OK);
        
    def test_pushframes(self):
        
        # change to DEPTH SENSOR
        self.assertEqual(self.client.set_state(MkePrivClient.STATE_DEPTH_SENSOR),
                         MkePrivClient.STATUS_OK);

        for l in range(self.NUM_LOOPS):
            start_seq_id = self.client.start_frame_push(2)
            stop_seq_id = None
            
            num = 0
            last_frame = None
            while True:
                if num == self.NUM_FRAMES:
                    stop_seq_id = self.client.stop_frame_push()
                
                frame = self.client.get_pushed_frame(start_seq_id, stop_seq_id)
                    
                if frame is None:
                    break;
                
                if not (last_frame is None):
                    with self.subTest(loop=l, i=num):
                        self.assertEqual(last_frame['seqn']+1, frame['seqn'])
                        self.assertTrue(last_frame['timer'] < frame['timer'])
                
                num += 1
                last_frame = frame

        # go back to IDLE
        self.assertEqual(self.client.set_state(MkePrivClient.STATE_IDLE),
                         MkePrivClient.STATUS_OK);
        

if __name__ == '__main__':
    unittest.main()