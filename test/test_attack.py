import unittest

import sys
import os
import subprocess
import time

import mketest as cfg
from mketest import PermanentConnTestCase
from mkepriv import MkePrivClient


class TestAttack(PermanentConnTestCase):
    
    REPETITION = 100

    def test_ddos_getstate(self):

        # multiple get state

        for i in range(self.REPETITION):
            with self.subTest(i=i):
                self.assertEqual(self.client.get_state(),
                                 MkePrivClient.STATE_IDLE);
    
    def test_ddos_notapply(self):

        # multiple invalid command
        
        for i in range(self.REPETITION):
            with self.subTest(i=i):
                self.assertEqual(self.client.set_state(MkePrivClient.STATE_IDLE),
                                 MkePrivClient.STATUS_CLIENT_REQUEST_DOES_NOT_APPLY);
    
    def test_ddos_switchstate(self):
                
        # multiple change state

        for i in range(self.REPETITION):
            with self.subTest(state=MkePrivClient.STATE_IDLE, i=i):
        
                # change to DEPTH SENSOR
                self.assertEqual(self.client.set_state(MkePrivClient.STATE_DEPTH_SENSOR),
                                 MkePrivClient.STATUS_OK);
                self.assertEqual(self.client.get_state(), MkePrivClient.STATE_DEPTH_SENSOR)
        
                # change back
                self.assertEqual(self.client.set_state(MkePrivClient.STATE_IDLE),
                                 MkePrivClient.STATUS_OK);
                self.assertEqual(self.client.get_state(), MkePrivClient.STATE_IDLE)
    
    def test_ddos_stoppushframes(self):

        # change to DEPTH SENSOR
        self.assertEqual(self.client.set_state(MkePrivClient.STATE_DEPTH_SENSOR),
                            MkePrivClient.STATUS_OK);

        # multiple stop push frames
        
        for i in range(self.REPETITION):
            with self.subTest(i=i):
                seq = self.client.stop_frame_push()
                ret = self.client._recv_mke()
                self.assertEqual(seq, ret['seq_id']);
        

        # change to DEPTH SENSOR
        self.assertEqual(self.client.set_state(MkePrivClient.STATE_IDLE),
                            MkePrivClient.STATUS_OK);

    def test_delayed_push(self):
        
        # change to DEPTH SENSOR
        self.assertEqual(self.client.set_state(MkePrivClient.STATE_DEPTH_SENSOR),
                         MkePrivClient.STATUS_OK);

        for l in range(5):
            start_seq_id = self.client.start_frame_push(2)
            time.sleep(30)
            stop_seq_id = self.client.stop_frame_push()
                        
            num = 0
            last_frame = None
            while True:
                frame = self.client.get_pushed_frame(start_seq_id, stop_seq_id)
                    
                if frame is None:
                    break;
                
                if not (last_frame is None):
                    with self.subTest(loop=l, i=num):
                        self.assertEqual(last_frame['seqn']+1, frame['seqn'])
                        self.assertTrue(last_frame['timer'] < frame['timer'])
                
                num += 1
                last_frame = frame

        # go back to IDLE
        self.assertEqual(self.client.set_state(MkePrivClient.STATE_IDLE),
                         MkePrivClient.STATUS_OK);        

if __name__ == '__main__':
    unittest.main()