#!/bin/bash 
 
m4 api_states.m4.dot > api_states.dot ; dot -Tpdf api_states.dot > api_states.pdf 
m4 -DRESERVED_API api_states.m4.dot > api_states.dot ; dot -Tpdf api_states.dot > api_states_reserved.pdf 
 
m4 api_states.m4.dot > api_states.dot ; dot -Tpng api_states.dot > api_states.png 
m4 -DRESERVED_API api_states.m4.dot > api_states.dot ; dot -Tpng api_states.dot > api_states_reserved.png 
 
rm api_states.dot 


#########################################

lyx -e docbook-xml mkeapi.lyx

# PDF
xsltproc --stringparam paper.type A4 /usr/share/xml/docbook/stylesheet/docbook-xsl/fo/docbook.xsl mkeapi.xml > mkeapi.fo
fop -fo mkeapi.fo -pdf mkeapi.pdf

# Markdown
sed 's/colwidth="[^"]."//' mkeapi.xml > mkeapi-sed.xml
pandoc -f docbook -t markdown_github mkeapi-sed.xml -o README.md

rm mkeapi.fo
rm mkeapi.xml
rm mkeapi-sed.xml

doxygen Doxyfile.in
