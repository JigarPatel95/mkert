/*
 * Magik Eye API 1.0
 * 
 * Copyright (c) 2017, Magik-Eye Inc
 * author: Jan Heller, jan@magik-eye.com
 *         Ondra Fisar, fisar@magik-eye.com
 * 
 */

#ifndef _MKEAPI_RESERVED_H_
#define _MKEAPI_RESERVED_H_

#include "stdint.h"

#ifdef __cplusplus
namespace mke {
namespace api {
#endif
  
// MkE State ==================================================================

  
#define MKE_STATE_RESERVED \
  MKE_STATE_SERVICE = 10, 
  
  
// MkE Request ================================================================
  
#define MKE_REQUEST_RESERVED \
  MKE_REQUEST_PING = 1001, \
  MKE_REQUEST_START_TIMER = 1002, \
  MKE_REQUEST_STOP_TIMER = 1003, \
  MKE_REQUEST_STREAM_ON = 1004, \
  MKE_REQUEST_STREAM_OFF = 1005, \
  MKE_REQUEST_GET_SHUTTER = 1006, \
  MKE_REQUEST_SET_SHUTTER = 1007, \
  MKE_REQUEST_GET_GAIN = 1008, \
  MKE_REQUEST_SET_GAIN = 1009, \
  MKE_REQUEST_GET_LASER = 1010, \
  MKE_REQUEST_SET_LASER = 1011, \
  MKE_REQUEST_GET_IMAGE = 1012, \
  MKE_REQUEST_GET_DEBUG_IMAGES = 1013, \
  MKE_REQUEST_GET_STATS = 1014, \
  MKE_REQUEST_GET_RESERVED_INFO = 1015, \
  MKE_REQUEST_GET_PROFILE = 1016, \
  MKE_REQUEST_RESET_STATS = 1017, \
  MKE_REQUEST_SET_PROFILE = 2002, \
  MKE_REQUEST_DOWNLOAD_FILE = 2999

#define MKE_TERMINATE_RESERVED \
  MKE_TERMINATE_BY_RESTART = 3, \
  MKE_TERMINATE_PUBLIC = 2, \
  MKE_TERMINATE_ALL = 4,


#define MKE_IS_TERMINATE_METHOD_VALID(val, reserved_mode)  \
  (val > mke::api::MKE_TERMINATE_UNDEF && \
    val <= mke::api::MKE_TERMINATE_ALL && \
    (reserved_mode \
      || val <= mke::api::MKE_TERMINATE_PUBLIC))
  
struct MkERequest_Reserved {
  uint64_t  reserved;           /* Reserved */
};
  
struct MkERequest_Ping {
  uint32_t  ping;               /* Ping to be ponged */
  uint8_t   undefined[4];       /* Padding, should be zero */
};

struct MkERequest_Timer {
  uint32_t  timer_ticks;        /* Timer ticks */
  uint8_t   undefined[4];       /* Padding, should be zero */
};

struct MkERequest_SetShutterSpeed {
  uint32_t  shutter_speed;      /* Required shutter speed to be set */
  uint8_t   undefined[4];       /* Padding, should be zero */
};

struct MkERequest_SetGain {
  uint32_t  iso_value;          /* Required ISO value to be set */
  uint8_t   undefined[4];       /* Padding, should be zero */
};

struct MkERequest_SetLaser {
  uint16_t  pattern;            /* Laser pattern */
  int8_t    dur_mantisa;        /* Required duration of strobe - mantisa */
  int8_t    dur_exp;            /* Required duration of strobe - exponent */
  int8_t    off_mantisa;        /* Required offset of strobe - mantisa */
  int8_t    off_exp;            /* Required offset of strobe - exponent */
  uint8_t   undefined[2];       /* Padding, should be zero */
};

enum MkEGetImage_Flags  { MKE_FLAG_TURNOFF_LASERS_AFTER = 1      // turn off lasers after images are grabbed 
                        };

enum MkEImageFormat     { MKE_IMAGE_FORMAT_PGM = 0,
                          MKE_IMAGE_FORMAT_PNG = 1,
                          MKE_IMAGE_FORMAT_LAST = 1,
                          MKE_IMAGE_FORMAT_DEFAULT = 0 
                        };
                     
struct MkERequest_GetImage {
  uint32_t  num_average;        /* Number of images should be used for averaging */
  uint8_t   flags;              /* Flags */
  uint8_t   image_format;       /* Image format, see MkEImageFormat */
  uint8_t   undefined[2];       /* Padding, should be zero */
};

struct MkERequest_DebugImages {
  uint32_t  num_images;         /* Number of images should be sent back */
  uint16_t  frame_type;         /* Padding, should be zero */
  uint8_t   image_format;       /* Image format */
  uint8_t   undefined[1];       /* Padding, should be zero */
};

#define MKE_REQUEST_PARAMS_RESERVED \
    MkERequest_Reserved        reserved_params; \
    MkERequest_Ping            ping_params; \
    MkERequest_Timer           timer_params; \
    MkERequest_SetShutterSpeed setshutterspeed_params; \
    MkERequest_SetGain         setgain_params; \
    MkERequest_SetLaser        setlaser_params; \
    MkERequest_GetImage        getimage_params; \
    MkERequest_DebugImages     debugimages_params; \
    MkERequest_Dynamic         dynamic_params;
    
    
// MkE Reply ================================================================

    
struct MkEReply_Timer {
  uint64_t  timer;
  uint32_t  timer_ticks;
  char      undefined[12];
};

struct MkEReply_GetImage {
  uint64_t  timer;              /* timer of 1s image received from camera */
  uint32_t  image_id;           /* seq. number of 1st image received from camera */
  char      undefined[12];
};

struct MkEReply_DebugImages {
  uint32_t  num_images;         /* timer of 1s image received from camera */
  char      undefined[20];
};

struct MkEReply_GetShutterSpeed {
  uint32_t  shutter_speed;      /* current shutter speed in us */
  char      undefined[20];
};

struct MkEReply_GetGain {
  uint32_t  analog_gain;        /* current gain value */
  uint32_t  digital_gain;       /* current gain value */
  char      undefined[16];
};    

struct MkEReply_GetLaser {
  uint16_t  pattern;            /* current laser pattern */
  int8_t    dur_mantisa;        /* Required duration of strobe - mantisa */
  int8_t    dur_exp;            /* Required duration of strobe - exponent */
  int8_t    off_mantisa;        /* Required offset of strobe - mantisa */
  int8_t    off_exp;            /* Required offset of strobe - exponent */
  char      undefined[18];
};    

#define MKE_REPLY_PARAMS_RESERVED \
    MkEReply_Timer          timer_params; \
    MkEReply_GetImage       getimage_params; \
    MkEReply_DebugImages    debugimages_params; \
    MkEReply_GetShutterSpeed getshutterspeed; \
    MkEReply_GetGain        getgain; \
    MkEReply_GetLaser       getlaser;

    
// MkE Frame Data ================================================================

    
#define MKE_FRAME_TYPE_RESERVED \
  MKE_FRAME_TYPE_RESERVED_1 = 1001, \
  MKE_FRAME_TYPE_RESERVED_2 = 1002, \
  MKE_FRAME_TYPE_RESERVED_ALL = 1002, \
  MKE_FRAME_TYPE_PUBLIC_ALL = 2
  
#define MKE_IS_FRAME_TYPE_VALID(val, reserved_mode)  \
  ((val >= mke::api::MKE_FRAME_TYPE_1 && \
    val <= mke::api::MKE_FRAME_TYPE_PUBLIC_ALL) || \
    (reserved_mode && val >= mke::api::MKE_FRAME_TYPE_RESERVED_1 &&\
      val <= mke::api::MKE_FRAME_TYPE_RESERVED_ALL) \
  )
    
/* MkEReply_Frame.frame_type == MkEFrameType.MKE_FRAME_TYPE_RESERVED_1, sizeof(MkEFrameItemReserved1) == 20 */
struct MkEFrameItemReserved1 {
  uint16_t uid;        /* unique point identifier */
  int16_t  x, y, z;    /* measured 3D point [mm] */
  uint16_t lid, did;   /* topo laser id, topo dot id */
  uint32_t u, v;       /* detection coordinates [px], fpreal */
};

/* MkEReply_Frame.frame_type == MkEFrameType.MKE_FRAME_TYPE_RESERVED_2, sizeof(MkEFrameItemReserved2) == 36 */
struct MkEFrameItemReserved2 {
  uint16_t uid;        /* unique point identifier */
  int16_t  x, y, z;    /* measured 3D point [mm] */
  uint16_t lid, did;   /* topo laser id, topo dot id */
  uint32_t u, v;       /* detection coordinates [px], fpreal */
  uint32_t det;        /* detection accumulator offset */
  uint32_t bwidth;     /* response box width */
  uint32_t sigma;      /* response sigma, fpreal */
  uint32_t resp;       /* response, fpreal */
};

#define MKE_LARGEST_FRAME_TYPE mke::api::MkEFrameItemReserved2
    
// MkE Debug Images subheader ====================================================


/*

Data in payload will be following:

  `MkEDebugImagesSubHeader`1
  image bytes # `image_num_bytes`
  frame bytes # `frame_num_bytes`
  `MkEDebugImagesSubHeader`2
  image bytes # `image_num_bytes`
  frame bytes # `frame_num_bytes`
  ...
  `MkEDebugImagesSubHeader`n
  image bytes # `image_num_bytes`
  frame bytes # `frame_num_bytes`
*/

struct MkEDebugImagesSubHeader {
  uint64_t          timer;
  uint32_t          image_frame_id;

  uint32_t          image_num_bytes;
  uint32_t          frame_num_bytes;

  uint16_t          frame_type;
  uint16_t          frame_num_data;
  
  uint16_t          laser_pattern;
  uint16_t          data3d_type;
  char              undefined[4];
};
    

#ifdef __cplusplus
} // namespace api
} // namespace mke
#endif
  
#endif
