/*
 * Magik Eye API 1.0
 * 
 * Copyright (c) 2017, Magik-Eye Inc
 * author: Jan Heller, jan@magik-eye.com
 *         Ondra Fisar, fisar@magik-eye.com
 * 
 */


#ifndef _MKEAPI_H_
#define _MKEAPI_H_

#define _IN_MKEAPI_H_

#define MKEAPI_VERSION "1.0.0"

#ifdef MKE_USE_RESERVED_API
#include "mkeapi_reserved.h"
#endif

/**
 * \defgroup mkeapi MkE API
 * 
 */

#include "stdint.h"

#ifdef __cplusplus
namespace mke {
namespace api {
#endif
  

/** \addtogroup mkeapi
 *  @{
 *
 */
    
// MkE State ==================================================================

#ifndef MKE_STATE_RESERVED
#define MKE_STATE_RESERVED 
#endif
  
  
/** 
 * @brief Enum type listing MkE API states, i.e., modes of operation of the sensor. 
 *  
 */ 
enum MkEStateType { 
  MKE_STATE_UNDEF = 0, /**< For internal purposes only, does not name a valid state. */ 
  MKE_STATE_IDLE = 1,  /**< In this state, no 3D sensing is performed 
                        and the sensor consumes only limited power resources. */
  MKE_STATE_DEPTH_SENSOR = 2, /**< In this state, the sensor computes the depth 
      information and, upon request, provides this information to the client. */   
  MKE_STATE_RESERVED  /**< For internal purposes only */
}; 

// MkE Request ================================================================

#ifndef MKE_REQUEST_RESERVED
#define MKE_REQUEST_RESERVED 
#endif

#ifndef MKE_REQUEST_PARAMS_RESERVED
#define MKE_REQUEST_PARAMS_RESERVED
#endif

#ifndef MKE_TERMINATE_RESERVED
#define MKE_TERMINATE_RESERVED
#endif


/**
 * @brief Enum type corresponding to the legal values of `MkERequest::reqid`
 * 
 * Lists the names of commands a client of MkE API can use to ask the sensor to perform
 */
enum MkERequestType {
  MKE_REQUEST_UNDEF = 0, /**< For internal purposes only, request of 
                              this type will not be accepted */
  MKE_REQUEST_TERMINATE = 10, /**< `MKE_REQUEST_TERMINATE` request is used to 
                                    programatically shutdown or reboot the sensor. */
  MKE_REQUEST_GET_FIRMWARE_INFO = 11, /**< Use `MKE_REQUEST_GET_FIRMWARE_INFO` to 
                                        query the sensors firmware information */
  MKE_REQUEST_GET_DEVICE_INFO = 12, /**< Use `MKE_REQUEST_GET_DEVICE_INFO` to 
                                        query the sensors device information */
  MKE_REQUEST_GET_DEVICE_XML = 13, /**< Use `MKE_REQUEST_GET_DEVICE_XML` to 
                                        query the sensors device information as an XML file */
  MKE_REQUEST_GET_STATE = 20, /**< Use `MKE_REQUEST_GET_STATE` to query 
                                   the sensor's current state. */
  MKE_REQUEST_SET_STATE = 21, /**< Use `MKE_REQUEST_SET_STATE` to change
                                   the sensor's current state. */
  MKE_REQUEST_GET_POLICY = 22, /**< Use `MKE_REQUEST_GET_POLICY` to query 
                                    the sensor's current policy. */
  MKE_REQUEST_SET_POLICY = 23, /**< Use `MKE_REQUEST_SET_POLICY` to change
                                    the sensor's current policy. */
  MKE_REQUEST_START_FRAME_PUSH = 24, /**< `MKE_REQUEST_STOP_START_PUSH` type to 
                                           ellicit frame stream from the sensor. */
  MKE_REQUEST_STOP_FRAME_PUSH = 25, /**< Use `MKE_REQUEST_STOP_FRAME_PUSH` type 
                                     to stop the frame stream ellicited by previous 
                                    `MKE_REQUEST_START_FRAME_PUSH` request. */
  MKE_REQUEST_GET_FRAME = 26, /**< Use MKE_REQUEST_GET_FRAME type to request 
                                   a single frame from the sensor.*/ 
  MKE_REQUEST_LIST_POLICIES = 27, /**< Use `MKE_REQUEST_LIST_POLICIES` to list
                                       all available policies. */
  MKE_REQUEST_INTERNAL = 1000,      /**< Internal request MKE_REQUEST_INTERNAL for terminating sockets */
                                       
  MKE_REQUEST_DYNAMIC_OFFSET = 2000, /**< Offset of dynamic requests */
  MKE_REQUEST_UPLOAD_PACKAGE = 2001, /**< Use MKE_REQUEST_GET_FRAME type to upload 
                                          package to device */                                          
  MKE_REQUEST_DYNAMIC_LAST = 2999, /**< Last dynamic request */
  MKE_REQUEST_RESERVED /**< For internal purposes only */
  
};

/**
 * @brief Enum type listing possible runtime termination methods
 * 
 */
enum MkETerminateMethodType {
  MKE_TERMINATE_UNDEF = 0, /**< For internal purposes only */
  MKE_TERMINATE_BY_REBOOT = 1, /**< Reboot the sensor */
  MKE_TERMINATE_BY_SHUTDOWN = 2, /**< Shutdown the sensor */
  MKE_TERMINATE_BY_EXIT = 4, /**< Terminates the application only */
  MKE_TERMINATE_RESERVED  /**< For internal purposes only */
};

/**
 * @brief Parameter structure of the `MKE_REQUEST_TERMINATE` request
 * 
 */
struct MkERequest_Terminate {
  uint32_t  method;             /**< Termination method, see  `MkETerminateMethodType`. */
  uint8_t   undefined[4];       /**< Padding, should be set to zero */
};

/**
 * @brief Parameter structure of the `MKE_REQUEST_SET_STATE` request
 * 
 */
struct MkERequest_SetState {
  uint32_t  new_state;          /**< New state, see `MkEStateType`. */
  uint8_t   undefined[4];       /**< Padding, should be set to zero */
};

/**
 * @brief Parameter structure of the `MKE_REQUEST_GET_FRAME` and 
 * `MKE_REQUEST_START_FRAME_PUSH` requests
 * 
 */
struct MkERequest_GetFrame {
  uint16_t  frame_type;         /**< Requested frame item type, See `MkEFrameType` */
  uint8_t   undefined[6];       /**< Padding, should be set to zero */
};

/**
 * @brief Parameter structure of the `MKE_REQUEST_SET_DEPTH_SENSOR_POLICY` and 
 * 
 */
struct MkERequest_SetPolicy {
  char      profile_name[8];    /**< Name of profile should be used */
};

/**
 * @brief Parameter structure of the `MKE_REQUEST_UPLOAD_PACKAGE` request
 * 
 */
struct MkERequest_UploadPackage {
  uint32_t  num_bytes;          /**< number of bytes in payload. */
  uint8_t   undefined[4];       /**< Padding, should be set to zero */
};


/**
 * @brief Represents the MkE API  Dynamic request. 
 * 
 * MkE API request with dynamic part (not fixed size).
 * Parameters of the respective API request can be set via the
 * `MkERequest_Dynamic_Params` member.
 */

struct MkERequest_UploadPackage_DynamicParams {
  uint32_t  crc32;
};

union MkERequest_Dynamic_Params {
  char param_bytes[4];
  MkERequest_UploadPackage_DynamicParams param_upload_package;
};

struct MkERequest_Dynamic {
  uint32_t  payload_size;       /* size of payload after the request */
  MkERequest_Dynamic_Params
            params;             /* rest of parameters */
};

/**
 * @brief Represents the respective MkE API request parameters. 
 * 
 * Represents the respective MkE API request parameters. The structure is always 8 bytes long.
 */
struct MkERequest_Params {
  union {
    uint8_t param_bytes[8];       /**< Allows for per-byte access to parameter data   */
    
    MkERequest_Terminate  terminate_params;   /** < See `MkERequest_Terminate` */
    MkERequest_GetFrame   getframe_params;    /**< See `MkERequest_GetFrame`*/    
    MkERequest_SetState   setstate_params;    /** < See `MkERequest_SetState` */    
    MkERequest_SetPolicy  setpolicy_params;   /** < See `MkERequest_setPolicy` */
    MkERequest_UploadPackage  uploadpackage_params;   /** < See `MkERequest_UploadPackage` */
    MKE_REQUEST_PARAMS_RESERVED /**< For internal purposes only */
  };
};

/**
 * @brief Represents the MkE API request. 
 * 
 * MkE API request is always 24 bytes long, starting
 * with the string 'MKERQ100'. Parameters of the respective API request can be set via the
 * `MkERequest_Params` member.
 */
struct MkERequest {
  char magik[8];             /**< MkE API request packet identifier, must be set to : "MKERQ100". */
  char type[4];              /**< Request type as a zero padded decimal string, 
                                  e.g., "0001" for request type 1.        */ 
  uint32_t reqid;            /**< ID number of a request. A respective 
                                  response will have the same ID. */                              
  MkERequest_Params params; /**< Represents the respective MkE API request parameters. */
};


// MkE Reply ================================================================

#ifndef MKE_REPLY_PARAMS_RESERVED
#define MKE_REPLY_PARAMS_RESERVED
#endif


/**
 * @brief Enum type listing valid MkE reply statuses
 * 
 */
enum MkEReplyStatus {
  MKE_REPLY_UNDEF = 0, /**< For internal purposes only */
  
  MKE_REPLY_DATA_WILL_START = 100, /**< Signalizes the successfull initialization 
                                      of the frame streaming process. */
  MKE_REPLY_DATA_WILL_CONTINUE = 101, /**< Signalizes that the frame stream will
                                   continue with at least one more data packet.*/
  MKE_REPLY_DATA_STOPPED = 102, /**<  Signalizes that the frame stream has been 
               successfully stopped via `MKE_REQUEST_STOP_FRAME_PUSH` request.*/
  
  MKE_REPLY_OK = 200, /**< Signalizes that a request has been successfully handled.*/
  
  MKE_REPLY_CLIENT_ERROR = 400, /**< Signalizes a general client side error. */
  MKE_REPLY_CLIENT_MALFORMED_REQUEST = 401, /**< Signalizes a sensor's problem 
                                                with parsing a request.*/
  MKE_REPLY_CLIENT_ILLEGAL_REQUEST_TYPE = 402, /**< Signalizes that the client issued 
                                 a request type not available in the current state.*/
  MKE_REPLY_CLIENT_REQUEST_DOES_NOT_APPLY = 403, /**< Signalizes a situation 
  where a client requested resources that were not available in the sensor's current state.*/
  
  MKE_REPLY_SERVER_ERROR = 500, /**< Signalizes a general sensor side error. */
  MKE_REPLY_SERVER_REQUEST_INTERRUPTED = 501, /**< Signalizes that a sensor's work
                                  on a reply has been externally interrupted.*/
  MKE_REPLY_SERVER_BUSY = 502, /**< The sensor will issue the `MKE_REPLY_SERVER_BUSY` 
                                reply in situations where client requested an operation
                                that is already being processed by the sensor.*/
  MKE_REPLY_SERVER_INSUFFICIENT_RESOURCES = 503, /**< Signalizes fatal problem 
                                  with memory resources on the sensor's side. */
  MKE_REPLY_SERVER_FATAL_ERROR = 504, /**< Signalizes fatal error 
                                  that affects regular run of application, application is in FAILSAFE */
};


/**
 * @brief Parameter structure of the sensor's `Reply` to `MKE_REQUEST_GET_FRAME` and 
 * `MKE_REQUEST_START_FRAME_PUSH` requests
 * 
 * Whenever the `Reply`'s payload is to be interpreted as 3D data frame, its
 * parameters will have this structure.
 */
struct MkEReply_Frame {
  uint64_t timer;          /**< Time of the end of frame exposure in ms elapsed from the boot.
                              Not particularly useful on its own, but can be used to measure
                              time elapsed between different frame exposures */
  uint64_t seqn;           /**< Sequence number of a frame, starting from 0. This counter 
                              is incremented every time a frame is successfully processed 
                              by the sensor in the `MKE_STATE_DEPTH_SENSOR` state. */
  uint32_t data3d_type;    /**< Data type of 3D point cloud */
  uint16_t frame_type;     /**< Type of FrameData contained in the frame, see `MkEFrameType` */
  uint16_t num_data;       /**< Number of frame items contained in the frame */
};

/**
 * @brief Parameter structure of the sensor's `Reply` to `MKE_REQUEST_GET_STATE` request
 */
struct MkEReply_State {
  uint32_t  state;         /**< Current state, see `MkEStateType` */
  char      undefined[20]; /**< Padding, should be set to zero */
};


/**
 * @brief Parameter structure of the sensor's `Reply` to `MKE_REQUEST_GET_FIRMWARE_INFO` request
 */
struct MkEReply_FirmwareInfo {
  int64_t   posix_time;    /**< POSIX time in the time of firmware compilation */
  uint32_t  git_commit;    /**< Short git hash of the latest firmware commit */
  uint8_t   rt_ver_major;  /**< Runtime version - major part*/
  uint8_t   rt_ver_minor;  /**< Runtime version - minor part */
  uint8_t   rt_ver_patch;  /**< Runtime version - patch part */
  uint8_t   fw_ver_major; /**< Firmware version - major part*/
  uint8_t   fw_ver_minor; /**< Firmware version - minor part */
  uint8_t   fw_ver_patch; /**< Firmware version - patch part */
  char      undefined[6];  /**< Padding, should be set to zero */
};


/**
 * @brief Parameter structure of the sensor's `Reply` to `MKE_REQUEST_GET_DEVICE_INFO` request
 */
struct MkEReply_DeviceInfo {
  uint16_t  device_id;      /**< Identification of device */
  char      unit_id[8];     /**< Unit ID */
  char      undefined[14];  /**< Padding, should be set to zero */
};

/**
 * @brief Parameter structure of the sensor's `Reply` to `MKE_REQUEST_GET_DEPTH_SENSOR_POLICY` request
 */
struct MkEReply_GetPolicy {
  char      profile_name[8]; /**< Name of current profile */
  char      undefined[16];   /**< Padding, shoudl be set to zero */
};

/**
 * @brief Parameter structure of the sensor's `Reply` to `MKE_REQUEST_GET_DEPTH_SENSOR_POLICY` request
 */
struct MkEReply_ListPolicies {
  uint32_t  num_policies;    /**< Number of defined policies */
  char      undefined[20];   /**< Padding, shoudl be set to zero */
};

/** 
 * @brief Enum type listing fatal error types specifying the general error code `MKE_REPLY_SERVER_FATAL_ERROR`.
 *  
 */ 
enum MkEFatalErrorType {
  MKE_FATAL_UNDEF               = 0, /**< Unknown fatal error */
  MKE_FATAL_BADCONFIG           = 1, /**< The device has corrupted configuration */
  MKE_FATAL_DETECTORINIT        = 2, /**< Unable to initialize the detector */
  MKE_FATAL_BADCAMERA           = 3, /**< The device has encountered a problem with the camera connection */
  MKE_FATAL_RUNTIME             = 4, /**< Unspecified fatal error during runtime */
};

/**
 * @brief Parameter structure of the sensor's `Reply` specifying the general error code `MKE_REPLY_SERVER_FATAL_ERROR`.
 */
struct MkEReply_ServerFatal {
  uint32_t  err_code;      /**< Fatal error code, see `MkEFatalErrorType` */
  char      undefined[20]; /**< Padding, should be set to zero */
};

/**
 * @brief Represents the respective MkE API reply parameters. 
 * 
 * Represents the respective MkE API reply parameters. The structure is always 24 bytes long.
 */
struct MkEReply_params {
  union {
    uint8_t                 param_bytes[24]; /**< Allows for per-byte access to parameter data */
    MkEReply_Frame          frame_params;    /**< See `MkEFrameHeader` */
    MkEReply_State          state_params;    /**< See `MkEReply_State` */
    MkEReply_FirmwareInfo   fw_params;       /**< See `MkEReply_FirmwareInfo` */
    MkEReply_DeviceInfo     device_params;   /**< See `MkEReply_DeviceInfo` */
    MkEReply_GetPolicy      policy_params;   /**< See `MkEReply_GetPolicy` */
    MkEReply_ListPolicies   list_policies_params;   /**< See `MkEReply_ListPolicies` */
    MkEReply_ServerFatal    fatal_params;    /**< See `MkEReply_ServerFatal` */
    MKE_REPLY_PARAMS_RESERVED  /**< For internal purposes only */
  };
};

/**
 * @brief Represents the reply to the MkE API request. 
 * 
 */
struct MkEReply {
  char magik[8];      /**< MkE API reply packet identifier : "MKERP100". */
  char type[4];       /**< Request type of the original Mke API request this reply replies to.  
                                  i.e. "0001" for request type 1.    */
  char status[4];     /**< Reply status as a zero padded decimal string, 
                           e.g. "0200" for "Server OK" (`MkEReplyStatus::MKE_REPLY_OK`)  */                                
  uint32_t reqid;    /**< ID number of a request this reply replies to. */
  uint32_t num_bytes;  /**< Size of the additional data directly following this reply. 
              In the case there is no additional payload data, `num_bytes` must be set to 0.  */
  MkEReply_params params; /**< Represents the respective MkE API reply parameters. */
};

// MkE Frame Data ================================================================
   
#ifndef MKE_FRAME_TYPE_RESERVED
#define MKE_FRAME_TYPE_RESERVED
#endif

/**
MkE frame data consists a variable number of data entries (MkEFrameData1, 
MkEFrameData2, MkEFrameData3, or MkEFrameData4) of the same type and a frame footer. 
The data entries are in little endian format and the memory layout of 
the MkE frame data is as follows:

`MkEFrameType*`
...
`MkEFrameType*`
`MkEFrameFooter`
 */ 

/**
 * @brief MkE 3D data frame footer
 * 
 */
struct MkEFrameFooter {
  uint32_t crc32;      /**< CRC-32 check (ITU-T V.42) of the frame items bytes. */
};

/**
 * @brief Enum type listing valid 3D frame item types
 * 
 */
enum MkEFrameType {
  MKE_FRAME_TYPE_UNDEF = 0, /**< For internal purposes only */
  MKE_FRAME_TYPE_1 = 1,
  MKE_FRAME_TYPE_2 = 2,
  MKE_FRAME_TYPE_RESERVED /**< For internal purposes only */
};

/**
 * @brief Enum type listing valid 3D data types
 * 
 */
enum MkEData3dType {
  MKE_DATA3D_MM = 0,     /**< 1mm resolution */
  MKE_DATA3D_MM2 = 1,    /**< 1/2mm resolution */
  MKE_DATA3D_MM4 = 2,    /**< 1/4mm resolution */
  MKE_DATA3D_MM8 = 3,    /**< 1/8mm resolution */
  MKE_DATA3D_MM16 = 4    /**< 1/16mm resolution */
};

/**
 * @brief Inner structure of the frame item type `MKE_FRAME_TYPE_1`
 * 
 */
struct MkEFrameItem1 { 
  uint16_t uid; /**< Unique point identifier */
  int16_t  x;   /**< X coordinate of the measured 3D point [mm] */
  int16_t  y;   /**< Y coordinate of the measured 3D point [mm] */
  int16_t  z;   /**< Z coordinate of the measured 3D point [mm] */    
};

/**
 * @brief Inner structure of the frame item type `MKE_FRAME_TYPE_2`
 * 
 */
struct MkEFrameItem2 {
  uint16_t uid; /**< Unique point identifier */
  int16_t  x;   /**< X coordinate of the measured 3D point [mm] */
  int16_t  y;   /**< X coordinate of the measured 3D point [mm] */
  int16_t  z;   /**< Z coordinate of the measured 3D point [mm] */
  uint16_t lid; /**< Topological point identifier */
  uint16_t did; /**< Topological point identifier */
};

#ifndef MKE_LARGEST_FRAME_TYPE
    #define MKE_LARGEST_FRAME_TYPE mke::api::MkEFrameItem2
#endif

// =============================================================================

/** @}*/

#undef _IN_MKEAPI_H_

#ifdef __cplusplus
} // namespace api
} // namespace mke
#endif

#endif // _MKEAPI_H_
