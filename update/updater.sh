#!/bin/sh
#
# This script servers update of the application
#

# ------------------------------------------------------------------------------

# check input arguments

if [ $# -ne 2 ]; then
    echo "Illegal number of parameters."
    echo "Should be 2 arguments PACKAGE MKERT_DIR"
    echo "\tPACKAGE   - path to TAR package."
    echo "\tMKERT_DIR - directory where mkert is placed."
    exit 1
fi

# ------------------------------------------------------------------------------

# unpack into temporary folder

TMPDIR='/tmp/update/'

if [ -e $TMPDIR ]; then
  rm -Rf $TMPDIR
fi  

mkdir -p $TMPDIR

cd $TMPDIR
tar xf $1 

# run autorun file

/bin/sh autorun.sh $2

RET=$?

# make a clean up

rm -Rf $1 $TMPDIR

exit $RET

# ------------------------------------------------------------------------------
