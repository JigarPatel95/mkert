# MkERT: MagikEye Runtime #

MkE Runtime, `mkert`, is a software that executes on a MagikEye sensor. It is written in written in C++11 and it controls and synchronizes the sensor's hardware, implements the dot detection algorithm (via the [`mkedd`](https://bitbucket.org/mke_rnd/mkert/src/master/mkedd/) library), and implements the server side of the [MkE API](https://bitbucket.org/mke_rnd/mkert/src/master/mkeapi/doc/). `mkert` supports Linux ARMv7, Linux x86_64, and Windows 64bit platforms. Besides controlling a real hardware camera, `mkert` can be also used in a debugging mode, where images in a given directory are read in a loop and treated as if coming from a real hardware camera.

## Building MkERT

The `mkert` binary is built using the CMake build system. Assuming that the `MKERT_ROOT` environment variable points to the root directory of the `mkert` repository, to build the binary, just run the following commands in BASH:

```bash
$ mkdir ${MKERT_ROOT}/build && cd ${MKERT_ROOT}/build
$ cmake ..
$ make -j4
```

### Dependencies

The only required third-party dependency of `mkert` is `libboost`. Optionally, for `mkert` to support automatic network discovery `libupnp` is needed. For Compute Model 3 based sensor, `wiringPI` is required. Note however, that the official MkE cross-compilation toolchain for ARMv7 contains all required dependencies, see [Cross-compilation](#cross-compilation).

### Reserved API

In order to enable the reserved parts of the MkE API, use `USE_RESERVED_API` `cmake` option:

```bash
$ cmake -DUSE_RESERVED_API=ON ..
```

Note that reserved API should _not_ be enabled for production prototypes and is intended for debugging purposes only.

### Cross-compilation for ARMv7

In order to compile an ARMv7 binary, a cross-compilation toolchain can be used. The official MkE cross-compilation toolchain can be built using the `mke-firmware-cm3` repository (`git clone git@bitbucket.org:mke_rnd/mke-firmware-cm3.git`). The current build of the toolchain can be also downloaded from [Confluence](https://magik-eye.atlassian.net/wiki/spaces/RND/pages/35259519/MkERuntime+-+mkert).

Assuming the `CCENV` environment variable points to the root directory of the toolchain, `mkedd` library can be cross-compiled using the following BASH command:

```bash
$ cmake -DCMAKE_TOOLCHAIN_FILE=${CCENV}/toolchain-arm.cmake ..
```

### Compilation for Windows

Please download the FTD2XX driver (x86 (32-bit)) from the vendor and unpack it ie. to `C:/tmp/` directory:
https://www.ftdichip.com/Drivers/CDM/CDM%20v2.12.28%20WHQL%20Certified.zip

The following is an example of how to compile `mkert` for Windows using MinGW toolchain:

```
> C:\Projects\mkert\build>cmake ..  -G "MinGW Makefiles" -DFTD2XX_DIR="C:\tmp\CDM v2.12.28 WHQL Certified-32"
> mingw32-make -j4
```

## Executing MkERT

The behavior of `mkert` can be controlled either using command line parameters or using a configuration INI file. To list all available commands, use the `--help` options:

```bash
$  mkert --help
```

### Examples

To be able to conveniently test the `mkert` binary without the need for an actual sensor or a camera, `mkert` can used in a debugging mode, where images in a given directory are read in a loop and treated as if coming from a real hardware camera. The `mkert` repository contains testing data for this scenario:

```bash
${MKERT_ROOT}/build/src/mkert --Engine.type=LOAD --Engine.load_dir=${MKERT_ROOT}/test/data/test01/save/ --Engine.load_stride=1664 --Detector.mkedet_path=${MKERT_ROOT}/test/data/test01/mkedet01.bin --API.reserved_mode=1
```

## MkE API

MkE API is the application programming interface that defines the communication between Magik Eye depth sensor and the sensors' user. For detailed description of MkE API, see `${MKERT_ROOT}/mkeapi/doc/mkeapi.pdf` ([mkeapi.pdf](https://bitbucket.org/mke_rnd/mkert/src/mkedd_reserved_api/mkeapi/doc/mkeapi.pdf)).

## MkERT SDK

MkERT SDK contains a detailed description of MkE API as well as example implementations of client side of the API in Matlab and Python. The SDK archive can be build using the following perl script:

```bash
$ ${MKERT_ROOT}/sdk/sdk-release.pl --ouput_dir=/tmp
```

This will produce a zip archive `/tmp/ mke-sdk-X.Y.Z.zip` containing the current SDK version.

## Documentation

### Doxygen

MkERT used `doxygen` system to document its codebase. To generate the documentation, execute the following command in the `build` directory:

```bash
$ make doc
```

### Changelog

A Changelog for the `mkert` binary is kept in `${MKERT_ROOT}/Changelog.md`.
