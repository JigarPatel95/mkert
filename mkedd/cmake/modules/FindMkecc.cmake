# FindMkecc - cmake find module for MkECC library
# 
# Once done this will define
#  MKECC_FOUND - System has mkecc library
#  MKECC_INCLUDE_DIRS - The mkecc include directories
#  MKECC_LIBRARIES - The libraries needed to use mkecc library
#
# Following variables can be used to specify mkedd library files
#  Mkecc_DIR - Root directory of the mkecc library installation
#  Mkecc_INCLUDE_DIR - Include directory of the mkecc library
#  Mkecc_LIBRARY_DIR - Directory containing the mkecc library

find_path(MKECC_INCLUDE_DIR mkecc/dets/datasets.h
    HINTS ${CMAKE_PREFIX_PATH} ${Mkecc_DIR} ${Mkecc_INCLUDE_DIR}
    PATH_SUFFIXES include)

find_path(JSON_INCLUDE_DIR json.hpp
    HINTS ${CMAKE_PREFIX_PATH} ${Mkecc_DIR} ${Mkecc_INCLUDE_DIR} ${Json_INCLUDE_DIR}
    PATH_SUFFIXES mkecc include include/mkecc)

# Enforce static linking with bundled mkecc at this point
find_library(MKECC_LIBRARY NAMES libmkecc_bundled.a
    HINTS ${CMAKE_PREFIX_PATH} ${Mkecc_DIR} ${Mkecc_LIBRARY_DIR}
    PATH_SUFFIXES lib lib/mkecc)

mark_as_advanced(MKECC_LIBRARY MKECC_INCLUDE_DIR)

set(MKECC_LIBRARIES ${MKECC_LIBRARY} gomp)
set(MKECC_INCLUDE_DIRS ${MKECC_INCLUDE_DIR} ${JSON_INCLUDE_DIR})

# Show standard messages
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Mkecc DEFAULT_MSG
    MKECC_LIBRARY MKECC_INCLUDE_DIR JSON_INCLUDE_DIR
)
