# MkEDD: MagikEye dot detection library #

MkEDD is a dot detection library written in C++11. Given a byte array representing a 8-bit gray-valued image and a calibration file, `mkedd` outputs, among other things, a list of 3D coordinates representing scene points in the camera coordinate system.

## Building MkEDD

The `mkedd` library is built using the CMake build system. Assuming that the `MKERT_ROOT` environment variable points the to root directory of the `mkert` repository, to build the library, just run the following commands in BASH:

```bash
$ mkdir ${MKERT_ROOT}/mkedd/build && cd ${MKERT_ROOT}/mkedd/build
$ cmake ..
$ make -j4
```

### Dependencies

The library has several dependencies and most of those are a part of the library itself (`/mkedd/lib`): `nlohmann::json` and `json-schema-validator`. The only other dependency is the `boost` library. To be able to build and run library example codes, `libpng` is also needed.

### Runtime Statistics

The library provides a way to obtain simple runtime statistics in a form of a JSON object. However, since the time accounting somewhat slows the library down, it is not enabled by default and needs to be actively compiled into the library:

```bash
$ cmake -DMKEDD_RUNTIME_STATS=ON ..
```

### Reserved API

The library provides an enhanced interface called reserved API. This API is for debugging purposes only. It should _not_ be enabled in a production binary as it significantly slows down the detection algorithm. To enable the reserved API, use `MKEDD_RESERVED_API` option:

```bash
$ cmake -DMKEDD_RESERVED_API=ON ..
```

### Matlab Bindings

In order to build Matlab bindings for the `mkedd` library, use the following `cmake` parameters:

```bash
$ cmake -DMKEDD_MATLAB_BINDINGS=ON -DMatlab_ROOT_DIR=/opt/matlabR2017a/ ..
```

Note that enabling of `MKEDD_MATLAB_BINDINGS` implies reserved API, _i.e._, `MKEDD_RESERVED_API=ON`.

### Python Bindings

In order to build Python bindings for the `mkedd` library, use the following `cmake` parameters:

```bash
$ cmake -DMKEDD_PYTHON_BINDINGS=ON  ...
```

Note that enabling of `MKEDD_PYTHON_BINDINGS` implies reserved API, _i.e._, `MKEDD_RESERVED_API=ON`.

The script `${MKERT_ROOT}/mkedd/python/example.py` contains an example on how to use the `mkedd` Python bindings. You can execute the example using the following command:

```bash
$ PYTHONPATH=${MKERT_ROOT}/mkedd/build/python python ${MKERT_ROOT}/mkedd/python/example.py
```
### MkECC depencency

In order to use geometry-based detector that need `mkecc` session files as their input calibration data, `mkedd` needs to be compiled with `mkecc` support. Suppose that `${MKECC_ROOT}` is the `git` root directory of `mkecc` and that `${MKECC_ROOT}/build` is the directory where `mkecc` has been built. In order to compile `mkedd` with `mkecc` support, use the following `cmake` parameters:

```bash
$ cmake -DMKEDD_USE_MKECC=ON \
        -DJSON_INCLUDE_DIR=${MKECC_ROOT}/build/ \
        -DMkecc_INCLUDE_DIR=${MKECC_ROOT} \
        -DMkecc_LIBRARY_DIR=${MKECC_ROOT}/build/mkecc/  
```

### ARMv7 Cross-compilation

In order to compile an ARMv7 binary, a cross-compilation toolchain can be used. The official MkE cross-compilation toolchain can be built using the `mke-firmware-cm3` repository (`git clone git@bitbucket.org:mke_rnd/mke-firmware-cm3.git`). The current build of the toolchain can be also downloaded from [Confluence](https://magik-eye.atlassian.net/wiki/spaces/RND/pages/35259519/MkERuntime+-+mkert).

Assuming the `CCENV` environment variable points to the root directory of the toolchain, `mkedd` library can be cross-compiled using the following BASH command:

```bash
$ cmake -DCMAKE_TOOLCHAIN_FILE=${CCENV}/toolchain-arm.cmake ..
```

### NIOSII Cross-compilation
In order to compile a NIOSII binary, The NiosII Embedded Design Suite (EDS) needs to be installed. The EDS is included as part of our Intel Quartus Prime (15.1 and higher) or Intel Quartus development software. For a space optimized FPGA library build, unnecessary detectors and classifiers can be turned off:

```
$ cmake  -DMKEDD_USE_BOXLUT_DETECTOR=OFF -DMKEDD_USE_BOXLUT2_DETECTOR=OFF -DMKEDD_USE_CIRCLIN_CLASSIFIER=OFF ..
```

Next, a NIOSII Makefile for needs to be generated from the NIOSII shell:

```
> ./nios2build.sh
```

Finally, the NIOSII compatible FPGA library can be build. Again, from the NIOSII shell:

```
> cd nios2build
> make -j4
```

## MkEDD Test suite

The library repository also contains a simple C++ application `mkedd_tests` (`${MKERT_ROOT}/mkedd/src/mkedd_tests.cpp`) that serves both as an example of how to use the library's API and as the library testing tool. In order to build the application, set `MKEDD_TESTS=ON`:

```bash
$ cmake -DMKEDD_TESTS=ON ..
```

The application parses a JSON file describing one or more tests and output the results. The following example show a simple test that performs dot detection 100-times on an image. The JSON file describing this test resides in `${MKERT_ROOT}/mkedd/data/test00/test.json`:


```json
[
  {
    "name":"VGA DD_SECTION_COLLUT BoxLut2",
    "calib":"../test01/mkedet01-collut.bin",
    "images":[
      "../test01/img.png"
    ],
    "detidx":[
      0
    ],
    "runs":100,
    "profile":{
      "detector":{
        "type":"BoxLut2",
        "classifier":"CircLinear",
        "threshold":8,
        "peak_ratio":1.2,
        "curv_ratio":1.15,
        "no_epochs":0,
        "box_delta":2,
        "no_threads":4
      },
      "postproc":{
        "type":"PassThru"
      }
    }
  }
]
```

The output of the `mkedd_tests` should look similar to the following:

```bash
$ ${MKERT_ROOT}/mkedd/build/src/mkedd_tests -i ${MKERT_ROOT}/mkedd/data/test00/test.json

=================================================================
Performing test: VGA DD_SECTION_COLLUT BoxLut2
  Setting calibration file: /home/jheller/mkert/mkedd/data/test00/../test01/mkedet01-collut.bin
  Setting profile: {
  "detector": {
    "box_delta": 2,
    "classifier": "CircLinear",
    "curv_ratio": 1.15,
    "no_epochs": 0,
    "no_threads": 4,
    "peak_ratio": 1.2,
    "threshold": 8,
    "type": "BoxLut2"
  },
  "postproc": {
    "type": "PassThru"
  }
}
  Number of detectors: 1
  Stride[0]: 752
  Loading image data: /home/jheller/mkert/mkedd/data/test00/../test01/img.png
  Image size: [752x480x1]
  Running the detector 100 times...
  Elapsed time: 0.453136s (220.684 Hz)
  Sensor statistics: {
  "dds": [
    {
      "acclist_timer_s": 0.886032425,
      "bpeak_timer_s": 0.044613785,
      "conv_timer_s": 0.458707328,
      "nms_timer_s": 0.203960726,
      "pxlist_timer_s": 0.135081294,
      "user_timer_s": 1.119982926
    }
  ],
  "laser_patterns": [
    1
  ],
  "pps": [
    null
  ]
}
```
