/* 
 * mxutils.h: matlab utility functions
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#ifndef _MXUTILS_H_
#define _MXUTILS_H_

#include <stdexcept>
	
#include "mex.h"
#include "json.hpp"
//#include "mkecc/gil/gil.h"

namespace mke {
namespace dd {
namespace mx {

inline std::string mxarray_to_string(const mxArray *mx)
{
  char *mxchar = mxArrayToString(mx);
  std::string mxstr = mxchar;
  mxFree(mxchar);
  
  return mxstr;  
}

inline mxArray* string_to_mxarray(const std::string& str)
{
  return mxCreateString(str.c_str());  
}

template<typename T> inline void mxarray_to_data(const mxArray *mx, T &data, size_t &m, size_t &n)
{
  data = T(mxGetPr(mx));
  m = mxGetM(mx);
  n = mxGetN(mx);
}


inline void no_params_eq(const int no_params, const int required_no_params, const std::string &err_msg)
{
  if (no_params < required_no_params)
    throw std::runtime_error(err_msg);      
}

inline void no_params_geq(const int no_params, const int required_no_params, const std::string &err_msg)
{
  if (no_params < required_no_params)
    throw std::runtime_error(err_msg);      
}

/*
inline void mxarray_to_view(const mxArray *mx, mke::cc::gil::ImageView &view)
{
  mxClassID class_id = mxGetClassID(mx);  
  mwSize no_dims = mxGetNumberOfDimensions(mx);
  size_t m, n; 

  if ((class_id == mxUINT8_CLASS) && (no_dims <= 2))
    {
      uint8_t *data;  
      
      mxarray_to_data(mx, data, m, n);
      view = mke::cc::gil::ImageView(boost::gil::interleaved_view(m, n, (boost::gil::gray8_pixel_t*) data, m));      
    }
  else
    {
      throw std::runtime_error("Cannot convert mxArray to GIL image view: unsupported type");
    }
}
*/
    
} // namespace mx
} // namespace dd
} // namespace mke

#endif // _MXUTILS_H_
