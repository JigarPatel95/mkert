/* 
 * mxjson.h: nlohmann::json to/from mxArray
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#ifndef _MXJSON_H_
#define _MXJSON_H_

// TODO: Native & lossless conversion

#include "mex.h"
#include "json.hpp"

namespace mke {
namespace dd {
namespace mx {

//==============================================================================

typedef enum {
  NUMBER = 1,
  NULL_VALUE = 2,
  BOOLEAN = 4,
  STRING = 8,
  OBJECT = 16,
  ARRAY = 32
} JsonBasicType;

//==============================================================================

inline static bool have_equal_structure(const nlohmann::json &nljson1, const nlohmann::json &nljson2)
{
  if (nljson1.size() != nljson2.size())
    return false;

  auto it1 = nljson1.begin();
  auto it2 = nljson2.begin();

  for (unsigned int i = 0; i < nljson1.size(); i++)
    {
      if (it1.key() != it2.key())
        return false;

      it1++;
      it2++;
    }

  return true;
}

//==============================================================================

inline static void json2mxarray(const nlohmann::json &nljson, mxArray* &mx)
{
  if (nljson.is_number())
    {
      mx = mxCreateNumericMatrix(1, 1, mxDOUBLE_CLASS, mxREAL);
      *mxGetPr(mx) = nljson.get<double>();
    }
  else if (nljson.is_null())
    {
      mx = mxCreateNumericMatrix(0, 0, mxDOUBLE_CLASS, mxREAL);
    }
  else if (nljson.is_boolean())
    {
      mx = mxCreateLogicalMatrix(1, 1);
      *mxGetLogicals(mx) = mxLogical(nljson.get<bool>());
    }
  else if (nljson.is_string())
    {
      mx = mxCreateString(nljson.get<std::string>().c_str());
    }
  else if (nljson.is_object())
    {
      mx = mxCreateStructMatrix(1, 1, 0, nullptr);

      for (auto it = nljson.begin(); it != nljson.end(); ++it)
        {
          mxArray *submx;
          json2mxarray(it.value(), submx);

          int field_number = mxAddField(mx, it.key().c_str());
          mxSetFieldByNumber(mx, 0, field_number, submx);
        }
    }
  else if (nljson.is_array())
    {
      unsigned int items_type = 0;
      const nlohmann::json *object_ptr = nullptr;
      bool structure_equal = false;

      for (const auto &item: nljson)
        {
          if (item.is_number())
            items_type |= JsonBasicType::NUMBER;
          else if (item.is_null())
            items_type |= JsonBasicType::NULL_VALUE;
          else if (item.is_boolean())
            items_type |= JsonBasicType::BOOLEAN;
          else if (item.is_string())
            items_type |= JsonBasicType::STRING;
          else if (item.is_array())
            items_type |= JsonBasicType::ARRAY;
          else if (item.is_object())
            {
              if (!object_ptr)
                {
                  object_ptr = &item;
                  structure_equal = true;
                }
              else
                {
                  if (!have_equal_structure(*object_ptr, item))
                    structure_equal = false;
                }

              items_type |= JsonBasicType::OBJECT;
            }
          if (__builtin_popcountll(items_type) > 1)
            break;
        }

      if (items_type == JsonBasicType::NUMBER)
        {
          mx = mxCreateNumericMatrix(nljson.size(), 1, mxDOUBLE_CLASS, mxREAL);
          double *ptr = mxGetPr(mx);

          for (const auto &item: nljson)
            *ptr++ = item.get<double>();
        }
      else if (items_type == JsonBasicType::BOOLEAN)
        {
          mx = mxCreateLogicalMatrix(nljson.size(), 1);
          mxLogical *ptr = mxGetLogicals(mx);

          for (const auto &item: nljson)
            *ptr++ = item.get<bool>();
        }
      else if ((items_type == JsonBasicType::OBJECT) && structure_equal)
        {
          mx = mxCreateStructMatrix(nljson.size(), 1, 0, nullptr);
          for (auto it = object_ptr->begin(); it != object_ptr->end(); ++it)
            mxAddField(mx, it.key().c_str());

          int i = 0;
          for (const auto &item: nljson)
            {
              int j = 0;
              for (auto it = item.begin(); it != item.end(); it++, j++)
                {
                  mxArray *submx;
                  json2mxarray(*it, submx);
                  mxSetFieldByNumber(mx, i, j, submx);
                }
              i++;
            }
        }
      else
        {
         mx = mxCreateCellMatrix(nljson.size(), 1);

         for (unsigned int i = 0; i < nljson.size(); i++)
           {
             mxArray *submx;
             json2mxarray(nljson[i], submx);
             mxSetCell(mx, i, submx);
           }
        }
    }
  else
    {
      throw std::runtime_error("Cannot convert nlohmann::json to mxArray");
    }
}

//==============================================================================
//==============================================================================

inline void nljson_to_mxarray(const nlohmann::json &nljson, mxArray* &mx, bool native = false)
{
  if (native)
    {
      mxArray *rhs[1];
      mxArray *lhs[1];

      // JSON to string
      rhs[0] = mxCreateString(nljson.dump().c_str());

      // string to mxArray
      mexCallMATLAB(1, lhs, 1, rhs, "jsondecode");
      mx = lhs[0];

      // Free string memory
      mxDestroyArray(rhs[0]);
    }
  else
    {
      json2mxarray(nljson, mx);
    }
}

//==============================================================================

inline void nljson_to_mxarray_native(const nlohmann::json &nljson, mxArray* &mx)
{
  mxArray *rhs[1];
  mxArray *lhs[1];

  // JSON to string
  rhs[0] = mxCreateString(nljson.dump().c_str());

  // string to mxArray
  mexCallMATLAB(1, lhs, 1, rhs, "jsondecode");
  mx = lhs[0];

  // Free string memory
  mxDestroyArray(rhs[0]);
}

//==============================================================================

inline void mxarray_to_nljson(const mxArray *mx, nlohmann::json &nljson)
{
  char *json_str = nullptr;

  if (mxIsChar(mx))
    {
      json_str = mxArrayToString(mx);
    }
  else
    {
      mxArray *rhs[1];
      mxArray *lhs[1];

      rhs[0] = const_cast<mxArray*>(mx);

      // mxArray to char* JSON
      mexCallMATLAB(1, lhs, 1, rhs, "jsonencode");

      json_str = mxArrayToString(lhs[0]);
    }

  // Parse char* to json object
  nljson = nlohmann::json::parse(json_str);

  // Free char* memory
  mxFree(json_str);
}

//==============================================================================

inline nlohmann::json mxarray_to_nljson(const mxArray *mx)
{
  nlohmann::json nljson;
  mxarray_to_nljson(mx, nljson);
  return nljson;
}
    
} // namespace mx
} // namespace dd
} // namespace mke

#endif // _MXJSON_H_
