/* 
 * mxClass
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 * basedd on: mxClass.hpp
 * 
 
Copyright (c) 2012, Oliver Woodford
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in
      the documentation and/or other materials provided with the distribution

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

 */

#ifndef _MXCLASS_H_
#define _MXCLASS_H_

#include "mex.h"
#include <stdint.h>
#include <string>
#include <cstring>
#include <typeinfo>


namespace mke {
namespace dd {
namespace mx {

#define CLASS_HANDLE_SIGNATURE 0xFF00F0A5
template<class T> class mxClass
{
public:
    mxClass(T *ptr) : ptr_m(ptr), name_m(typeid(T).name()) 
    { 
      signature_m = CLASS_HANDLE_SIGNATURE;         
    }
    
    ~mxClass() 
    { 
      signature_m = 0; 
      delete ptr_m;         
    }
    
    bool isValid() 
    { 
      return ((signature_m == CLASS_HANDLE_SIGNATURE) && !strcmp(name_m.c_str(), typeid(T).name()));         
    }
    
    T *ptr() { 
      return ptr_m; 
    }

private:
    T *ptr_m;
    std::string name_m;
    uint32_t signature_m;
};

template<class T> inline mxArray *ptr_to_mat(T *ptr)
{
  mexLock();
  mxArray *out = mxCreateNumericMatrix(1, 1, mxUINT64_CLASS, mxREAL);
  *((uint64_t *)mxGetData(out)) = reinterpret_cast<uint64_t>(new mxClass<T>(ptr));
  return out;
}

template<class T> inline mxClass<T> *mat_to_handleptr(const mxArray *in)
{
  if (mxGetNumberOfElements(in) != 1 || mxGetClassID(in) != mxUINT64_CLASS || mxIsComplex(in))
    mexErrMsgTxt("Input must be a real uint64 scalar.");

  mxClass<T> *ptr = reinterpret_cast<mxClass<T> *>(*((uint64_t *)mxGetData(in)));
  if (!ptr->isValid())
    mexErrMsgTxt("Handle not valid.");

  return ptr;
}

template<class T> inline T *mat_to_ptr(const mxArray *in)
{
  return mat_to_handleptr<T>(in)->ptr();
}

template<class T> inline void destroy_object(const mxArray *in)
{
  delete mat_to_handleptr<T>(in);
  mexUnlock();
}

    
} // namespace mx
} // namespace dd
} // namespace mke


#endif // _MXCLASS_H_
