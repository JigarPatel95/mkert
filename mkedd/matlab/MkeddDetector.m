classdef MkeddDetector < handle
    
    properties (SetAccess = private, Hidden = true)
        objectHandle; % Handle to the underlying C++ class instance
        
        frame;
    end
    
    %======================================================================
    
    methods (Access = public)
        function obj = MkeddDetector(name)
            obj.objectHandle = mkeddmm('mke::dd::dd::Detector', 'new', name);
        end
        
        function delete(obj)
            mkeddmm('mke::dd::dd::Detector', 'delete', obj.objectHandle);
        end
                
        function params = getParams(obj)
            params = mkeddmm('mke::dd::dd::Detector', 'getParams', obj.objectHandle);
        end
        
        function setParams(obj, params)
            mkeddmm('mke::dd::dd::Detector', 'setParams', obj.objectHandle, params);
        end      
        
        %{
        function initDebugData(obj)
            mkeddmm('mke::dd::dc::Classifier', 'initDebugData', obj.objectHandle);
        end               

        function dbg_data = getDebugData(obj)
            dbg_data = mkeddmm('mke::dd::dc::Classifier', 'getDebugData', obj.objectHandle);
        end               
        
        function setFrameParams(obj, width, height, stride)
            mkeddmm('mke::dd::dc::Classifier', 'setFrameParams', obj.objectHandle, width, height, stride);
        end
        
        function classify(obj, data, u, v, uid, pt3d)
            mkeddmm('mke::dd::dc::Classifier', 'classify', obj.objectHandle, data, u, v, uid, pt3d);
        end
        %}
        % =================================================================
                
        function plotAccum(~, accum_data, res)
             fs = SubFigure(4);

             no_pels = numel(accum_data);
             peaks = res.peaks;
             no_peaks = numel(peaks);
             cumsum = res.cumsum;
             
             %%%
             fs.new();
             plot(0:no_pels - 1, accum_data);
                
             for i = 1:no_peaks
                plot(peaks(i).offset, accum_data(peaks(i).offset + 1), 'xr');  
             end
             
             %%%
             fs.new();
             plot(0:no_pels - 1, cumsum, 'xr');
             
             %%%
             fs.new();
             cconv = [res.cconv{:}]';
             imagesc(cconv);
             
             for i = 1:no_peaks
                plot(peaks(i).offset + 1, peaks(i).scale + 1, 'xr');  
             end             
        end  
     end
end