classdef MkeddTester < handle
%{
  tester = MkeddTester();
  [params, results] = tester.executeTest(1);
  tester.saveToFile('/home/jheller/tmp/nms_perlin51.json', params, results);
  [params, results, info] = tester.loadFromFile('/home/jheller/tmp/tests-rpi3.json');
%}
    
    properties (SetAccess = private, Hidden = true)
        objectHandle; % Handle to the underlying C++ class instance
        fs;
    end
    %======================================================================
    
    methods (Access = public)
        
        function obj = MkeddTester()
            obj.objectHandle = mkeddmm('mke::dd::util::Tester', 'new');
        end
        
        function delete(obj)
            mkeddmm('mke::dd::util::Tester', 'delete', obj.objectHandle);
        end
        
        function results = process(obj, params)
            results = mkeddmm('mke::dd::util::Tester', 'process', obj.objectHandle, params);           
            results = MkeddTester.fixResults(results);
        end
        
        function [params, results] = processFromFile(obj, fpath)
            tests = mkeddmm('mke::dd::util::Tester', 'processFromFile', obj.objectHandle, fpath);
            
            params = MkeddTester.fixResults(tests.params);
            results = MkeddTester.fixResults(tests.results);
        end  
        
        function [params, results, info] = loadFromFile(obj, fpath)
            tests = mkeddmm('mke::dd::util::Tester', 'loadFromFile', obj.objectHandle, fpath);
            
            params = MkeddTester.fixResults(tests.params);
            results = MkeddTester.fixResults(tests.results);
            info = tests.info;
        end
        
        function saveToFile(obj, fpath, params, results, info)
            if (nargin < 5)
                info = [];
            end
            
            tests.params = params;
            tests.results = results;
            tests.info = info;
            
            mkeddmm('mke::dd::util::Tester', 'saveToFile', obj.objectHandle, fpath, tests);
        end        
                
        %==================================================================
        
        function [params, results] = executeTest(obj, testno, varargin)
            params = obj.generateTests(testno, varargin{:});
            results = obj.process(params);
            obj.showResults(params, results, []);
        end        
    end
     
    %======================================================================
    %======================================================================

    methods (Access = private, Static)

        function results = fixResults(results)            
            if (numel(results) == 1)
                results = {results};
            elseif (isstruct(results))
                res = results;
                results = cell(1, numel(res));
                
                for i = 1:numel(res)
                    results{i} = res(i);
                end
            end
        end
        
        %==================================================================

        function no_figs = showDdResults(test, res, fs)
            no_figs = 0; %#ok<NASGU>
            
            if (strcmp(test.method, 'nonMaximaSuppression'))
                no_figs = MkeddTester.showNmsResults(test.params, res, fs);
            elseif (strcmp(test.method, 'convolution'))
                no_figs = MkeddTester.showConvolutionResults(test.params, res, fs);
            else
                error(['[showDdResults] Unknown method: ' test.method]);
            end
        end
        
        function no_figs = showNmsResults(params, res, fs)
            
            no_figs = 1;
            
            if (isempty(fs))
                return;
            end
            
            %%%%
            
            fs.new();
            
            data = [params.data{:}]';
            imagesc(data);

            status = MkeddTester.pretty(res.status);
            if (~strcmp(status, 'OK'))
                status = ['\color{red}' status '\color{black}'];
            end
            
            title(sprintf('Method: %s, Status: %s', MkeddTester.pretty(res.method), ...
                status));
            
            if (~numel(res.maximas))
                return;
            end
            
            xs = [res.maximas(:).x] + 1;
            ys = [res.maximas(:).y] + 1;
            
            plot(xs, ys, 'xr');
        end        
        
        function no_figs = showConvolutionResults(params, res, fs)
            
            no_figs = 2;
            
            if (isempty(fs))
                return;
            end
            
            maximas = [];
            xs = [];
            ys = [];
            ss = [];
            
            if (isfield(res, 'maximas'))
                maximas = res.maximas;
                if (~isempty(maximas))
                    xs = [res.maximas(:).x];
                    ys = [res.maximas(:).y];
                    ss = [res.maximas(:).s];
                end
            end            
            
            peak_idx = (ss == 0);
            curv_idx = (ss == 1);
            
            %%%%
            
            fs.new();
            
            data = params.data;
            plot(0:numel(data)-1, data, '-b');
            plot(0:numel(data)-1, data, 'xr');
            
            if (~isempty(xs))
                ps = xs(peak_idx);                
                plot(ps, data(ps + 1), 'dm');
                
                ps = xs(curv_idx);     
                plot(ps, data(ps + 1), 'dm', 'MarkerFaceColor', 'm', 'MarkerSize', 6); 
            end
            
            ylim([0, 256]);
            
            %%%%
            
            fs.new();
            
            data = [res.cconv{:}]';
            imagesc(data);

            status = MkeddTester.pretty(res.status);
            method = MkeddTester.pretty(res.method);
            
            if (~strcmp(status, 'OK'))
                status = ['\color{red}' status '\color{black}'];
            end
            
            if (isfield(res, 'nms_method'))
                nms_status = MkeddTester.pretty(res.nms_status);
                method = [method ', ' MkeddTester.pretty(res.nms_method)];
                
                if (~strcmp(nms_status, 'OK'))
                    nms_status = ['\color{red}' nms_status '\color{black}'];
                end                
                
                status = [status ', ' nms_status];
            end
                        
            if (~isempty(xs))

                plot(xs(peak_idx) + 1, ys(peak_idx) + 1, 'md');
                plot(xs(curv_idx) + 1, ys(curv_idx) + 1, 'md', 'MarkerFaceColor', 'm', 'MarkerSize', 6);
            end            
            
            title(sprintf('Method: %s, Status: %s', method, status));
        end               
        
        %==================================================================
        
    end
    
    %======================================================================
    %======================================================================

    methods (Access = public, Static)

        function str = pretty(ustr)
           str = strrep(ustr, '_', '\_');             
        end
        %==================================================================

        function s = perlin2D(m)
            s = zeros([m,m]);
            w = m;
            i = 0;
            
            while (w > 3)
                i = i + 1;
                d = interp2(randn([m,m]), i-1, 'spline');
                s = s + i * d(1:m, 1:m);
                w = w - ceil(w/2 - 1);
            end
            
            s = (s - min(min(s(:,:)))) ./ (max(max(s(:,:))) - min(min(s(:,:))));
        end

        %==================================================================
        
        function nmsparams = forEachNmsMethod(params, fpr16_only)
            
            methods = {
                'NAIVE_METHOD', ...
                'PHAM_METHOD', ...
                'NAIVE_FPR16_METHOD', ...
                'PHAM_FPR16_METHOD', ...
                'NEON_FPR16_METHOD', ...
                'AVX2_FPR16_METHOD' ...
                };
           
            no_params = numel(params);
            no_methods = numel(methods);
            
            idx = 1;
            if ((nargin > 1) && fpr16_only)
                idx = 3;
            end
            
            nmsparams = cell(0, 0);
            k = 1;
                
            for i = idx:no_params
               for j = idx:no_methods 
                   ps = params{i};                   
                   ps.params.method = methods{j};
                   nmsparams{end + 1} = ps; %#ok<AGROW>
                   
                   k = k + 1;
               end
            end                       
        end
        
        %==================================================================
        
        function nmsparams = forEachConvMethod(params, fpr16_only)
            
            methods = {
                'NAIVE_METHOD', ...
                'NAIVE_FPR16_METHOD', ...
                'NEON_FPR16_METHOD', ...
                'AVX2_FPR16_METHOD', ...
                'HALIDE_FPR16_METHOD', ...
            };
           
            no_params = numel(params);
            no_methods = numel(methods);
                        
            idx = 1;
            if ((nargin > 1) && fpr16_only)
                idx = 2;
            end            
            
            nmsparams = cell(0, 0);
            k = 1;
            
            for i = 1:no_params
               for j = idx:no_methods 
                   ps = params{i};                   
                   ps.params.method = methods{j};
                   nmsparams{end + 1} = ps; %#ok<AGROW>
                   
                   k = k + 1;
               end
            end                       
        end        
            
        %================================================================== 
        %==================================================================

        function showResults(data, results, testno)
            if ((nargin < 3) || isempty(testno))
                testno = 1:numel(data);
            end
                        
            % Get the number of windows needed
            
            fs = [];
            no_figs = 0;

            for tidx = testno
                test = data{tidx};
                res = results{tidx};
                
                if (~isfield(test, 'module'))
                    error('Only module type test can be vizualized');
                end
                
                if (strcmp(test.module, 'dd'))
                    no_figs = no_figs + MkeddTester.showDdResults(test, res, fs);
                else
                    error(['[showResults] Unknown module: ' test.module]);
                end
                
            end
               
            % Show results
            
            if (no_figs < 4)
                no_figs = 4;
            end
            
            fs = SubFigure(no_figs);
            
            for tidx = testno
                
                test = data{tidx};
                res = results{tidx};
                
                if (~isfield(test, 'module'))
                    error('Only module type test can be vizualized');
                end
                
                if (strcmp(test.module, 'dd'))
                    MkeddTester.showDdResults(test, res, fs);
                else
                    error(['[showResults] Unknown module: ' test.module]);
                end
            end
        end        
                        
        %==================================================================

        function data = generateTests(testno, varargin)
            p = inputParser;
            
            addRequired(p, 'TestId', @isnumeric);
            addOptional(p, 'AccumData', [], @isnumeric);
            
            p.parse(testno, varargin{:});            
                        
            % NMS =========================================================
            if (testno == 1)
                test.params.data = MkeddTester.perlin2D(51) * 100;
                test.params.method = 'NAIVE_METHOD';
                test.method = 'nonMaximaSuppression';
                test.module = 'dd';
                
                data = MkeddTester.forEachNmsMethod({test});
            elseif (testno == 2)
                data = zeros(20, 10);
                data(10, 5) = 1;
                
                test.params.data = data;
                test.method = 'nonMaximaSuppression';
                test.module = 'dd';
                                
                data = MkeddTester.forEachNmsMethod({test});
            % Convolution =================================================
            elseif (testno == 11)
                test.params.data = 10 * ones(1, 101);
                test.params.method = 'NAIVE_METHOD';
                test.method = 'convolution';
                test.module = 'dd';
                
                data = MkeddTester.forEachConvMethod({test});
            elseif (testno == 12)
                test.params.data = 100 * gaussmf(1:101,[3 50]);
                test.params.method = 'NAIVE_FPR16_METHOD';
                test.method = 'convolution';
                test.module = 'dd';
                
                data = MkeddTester.forEachConvMethod({test});
            % Convolution + NMS ===========================================    
            elseif (testno == 21)
                
                zval = 100;
                
                test.params.data = zval + 50 * gaussmf(1:101,[10 50]);
                test.params.method = 'NAIVE_METHOD';
                test.params.nms_method = 'NAIVE_FPR16_METHOD';
                test.method = 'convolution';
                test.module = 'dd';
                
                data = MkeddTester.forEachConvMethod({test}, true);            
            elseif (testno == 22)
                
                zval = 100;
                
                test.params.data = p.Results.AccumData;
                test.params.method = 'NAIVE_METHOD';
                test.params.nms_method = 'NAIVE_FPR16_METHOD';
                test.method = 'convolution';
                test.module = 'dd';
                
                data = MkeddTester.forEachConvMethod({test}, true);                     
            end                 
            
            data = MkeddJsonFile.translate(data);
            data = MkeddTester.fixResults(data); 
        end
    end
    
    %======================================================================
end