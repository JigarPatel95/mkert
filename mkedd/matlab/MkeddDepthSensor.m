classdef MkeddDepthSensor < handle
    
    properties (SetAccess = private, Hidden = true)
        objectHandle; % Handle to the underlying C++ class instance
    end
    
    %======================================================================
    
    methods (Access = public)
        function obj = MkeddDepthSensor()
            obj.objectHandle = mkeddmm('mke::dd::DepthSensor', 'new');
        end
        
        function delete(obj)
            mkeddmm('mke::dd::DepthSensor', 'delete', obj.objectHandle);
        end
        
        function version = getVersion(obj)
           version = mkeddmm('mke::dd::DepthSensor', 'getVersion', obj.objectHandle);
        end
        
        % Profile =========================================================
        
        function profile = getProfile(obj)
            profile = mkeddmm('mke::dd::DepthSensor', 'getProfile', obj.objectHandle);
        end
        
        function profile = getDefaultProfile(obj)
            profile = mkeddmm('mke::dd::DepthSensor', 'getDefaultProfile', obj.objectHandle);
        end        
        
        function validateProfile(obj, profile)
            mkeddmm('mke::dd::DepthSensor', 'validateProfile', obj.objectHandle, profile);
        end

        function profile = getCurrentProfileSchema(obj)
            profile = mkeddmm('mke::dd::DepthSensor', 'getCurrentProfileSchema', obj.objectHandle);
        end
        
        function setDefaultProfile(obj)
            mkeddmm('mke::dd::DepthSensor', 'setDefaultProfile', obj.objectHandle);
        end       
        
        function setProfile(obj, profile)
            mkeddmm('mke::dd::DepthSensor', 'setProfile', obj.objectHandle, profile);
        end             
        
        % Stats ===========================================================
   
        function resetStats(obj)
            mkeddmm('mke::dd::DepthSensor', 'resetStats', obj.objectHandle);
        end
               
        function stats = getStats(obj)
            stats = mkeddmm('mke::dd::DepthSensor', 'getStats', obj.objectHandle);
        end
        
        % Calibration data files ==========================================
        
        function addCalibFile(obj, fpath)
            mkeddmm('mke::dd::DepthSensor', 'addCalibFile', obj.objectHandle, fpath);
        end
        
        function clearCalibFiles(obj)
            mkeddmm('mke::dd::DepthSensor', 'clearCalibFiles', obj.objectHandle);
        end   
        
        % Processing ======================================================
        
        function no_dets = process(obj, det_idx, data)
            if (~isa(data, 'uint8'))
                error('Expecting uint8 type image data')
            end
            
            if (~ismatrix(data))
                error('Expecting gray image');
            end
            
            stride = obj.getStride(det_idx);
            
            if (stride ~= 0)
               [height, width] = size(data);
               if (width < stride)
                   data = [data, zeros(height, stride - width, class(data))]; 
               elseif (width > stride)
                   data = data(:, 1:stride);                   
               end
            end
                
            no_dets = mkeddmm('mke::dd::DepthSensor', 'process', obj.objectHandle, det_idx, data'); 
        end
               
        % Info ============================================================

        function max_no_dets = getMaxNoDetections(obj)
            max_no_dets = mkeddmm('mke::dd::DepthSensor', 'getMaxNoDetections', obj.objectHandle); 
        end
        
        function no_dets = getNoDetections(obj, det_idx)
            no_dets = mkeddmm('mke::dd::DepthSensor', 'getNoDetections', obj.objectHandle, det_idx); 
        end       
        
        function no_dds = getNoDetectors(obj)
            no_dds = mkeddmm('mke::dd::DepthSensor', 'getNoDetectors', obj.objectHandle); 
        end
        
        function lp = getLaserPattern(obj, det_idx)
            lp = mkeddmm('mke::dd::DepthSensor', 'getLaserPattern', obj.objectHandle, det_idx); 
        end            
        
        function stride = getStride(obj, det_idx)
            stride = mkeddmm('mke::dd::DepthSensor', 'getStride', obj.objectHandle, det_idx); 
        end
        
        function data3d_type = getData3dType(obj, det_idx)
            data3d_type = mkeddmm('mke::dd::DepthSensor', 'getData3dType', obj.objectHandle, det_idx); 
        end            
        
        % Reserved API ====================================================
        
        function results = getResults(obj, det_idx)
            results = mkeddmm('mke::dd::DepthSensor', 'getResults', obj.objectHandle, det_idx);
        end
        
        function dbgdata = getDebugData(obj, det_idx)
            dbgdata = mkeddmm('mke::dd::DepthSensor', 'getDebugData', obj.objectHandle, det_idx);
        end     
        
        function triangulation = getTriangulation(obj, det_idx)
            triangulation = mkeddmm('mke::dd::DepthSensor', 'getTriangulation', obj.objectHandle, det_idx);
        end         
        
        function calibdata = parseCalibFile(obj, file_name)
            calibdata = mkeddmm('mke::dd::DepthSensor', 'parseCalibFile', obj.objectHandle, file_name);
        end            
        
    end
end
