classdef MkEDetector < handle
%{
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Create detector and load calibdation file
    
  mkedd = MkEDetector;
  mkedd.setMkEDetFile('/home/jheller/MagikEye/data/mkert_offline/vbox_1d/mkedet01.bin');
  mkedd.showMkEDetInfo();
    
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Execute the detector given an image and detector index and show the results
    
  mkedd.execute('/home/jheller/MagikEye/data/mkert_offline/vbox_1d/save/dbgim_01_10-080.pgm', 1);
  mkedd.showResults();
    
  % OR
  
  mkedd.setImage('/home/jheller/MagikEye/data/mkert_offline/vbox_1d/save/dbgim_01_10-080.pgm');
  mkedd.setDetIdx(1);
  mkedd.execute();
  mkedd.showResults();
    
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Show accumulator content after detector execution
    
  mkedd.showAccum(path_id);
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Change detector parameters
    
  profile = mkedd.getProfile();
  profile.detector.peak_ratio = 1.1;
  mkedd.setProfile(profile);
  mkedd.execute();
    
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Get debug images from sensor, visualize, and run mkedd detector to validate 
    
  mke_client = MkEReservedAPIClient;
  t = tcpclient('192.168.4.174', 8888);  
  mke_client.sendRequestTCP(t, 'MKE_REQUEST_GET_DEBUG_IMAGES', 1, 'NumDebugImages', 4, 'FrameType', 'MKE_FRAME_TYPE_RESERVED_2'); 
  reply = mke_client.readReplyTCP(t);
  mkedd.setDebugImages(reply);
  mkedd.showDebugImage(2);  
  mkedd.setImageFromDebugImages(2);
  mkedd.execute();
  mkedd.showResults();
  mkedd.showDebugImageAndResults(2);
    
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Change detector parameters on a sensor
  
  p = mkedd.getProfile();
  profile.mkedd = p;
  profile.mkedd.detector.no_epochs = 0;  
  mke_client = MkEReservedAPIClient;
  t = tcpclient('192.168.4.174', 8888);    
  mke_client.sendRequestTCP(t, 'MKE_REQUEST_SET_PROFILE', 1, 'Profile', jsonencode(profile)); 
  reply = mke_client.readReplyTCP(t)
 
%}
    
    properties (SetAccess = public, GetAccess = public)
        mkedet;
        calibdata;
        
        dd_stypes;
        pp_stypes;
        
        dimgs;
        
        profile;
        mkemdd_params;
        im_data;
        im_fpath;
        det_idx;
        res;
        dbgdata;
        
        mkedd;
    end % properties (SetAccess = protected, GetAccess = public)
    
    % =========================================================================
    % =========================================================================
    % =========================================================================
    
    methods (Access = private, Static)
        
        function [ret, idx] = readData(data, idx, num_data, type_size, type_name)
            num_bytes = type_size * num_data;
            ret = typecast(data((idx):(idx + num_bytes - 1)), type_name);
            idx = idx + num_bytes;
        end
        
        % =================================================================
        
        function colors = getDistinctColors(no_cols)
            hues = linspace(0, 1, no_cols + 1);
            colors = hsv2rgb([hues(1:end-1)', ones(no_cols, 1), ones(no_cols, 1)]);
        end
        
        % =================================================================
        
        function vp = getViewportRect(w, h)
            
            if (nargin < 2)
                h = w(2);
                w = w(1);
            end
            
            vp = [0, w - 1, w - 1, 0, 0;
                0, 0, h - 1, h - 1, 0];
        end
        
        % =================================================================
        
        function im = addStrideToImage(im, stride)
            
            [height, width, dim] = size(im);
            
            if (dim ~= 1)
                error('Only single channel images can be updated');
            end
            
            if (stride == width)
                return;
            elseif (stride < width)
                error('stride < width');
            end
            
            im = [im, zeros(height, stride - width, class(im))];
        end
        
        % =================================================================
        
        function name = getSectionName(sections, type)            
            idx = find(cell2mat(sections(:, 2)) == type);
            if (isempty(idx))
                error('Unknown section type');
            end
            
            name = cell2mat(sections(idx, 1));
        end    
        
         % ================================================================
        
        function moment = central_moment(p, q, img)
            sz = size(img); %// assuming image is gray scale - 2D array
            x = (1:sz(2)) - 1;
            y = (1:sz(1)).' - 1; %'
           % x = x - mean(x);
           % y = y - mean(y);
            moment = sum( reshape( bsxfun( @times, bsxfun( @times, img, x.^p ), y.^q ), [], 1 ) );            
        end
        
         % ================================================================
        
        function idx = uid2idx(res, uid)
            idx = find(res.uid == uid);
        end          
    end
    
    % =====================================================================
    % =====================================================================
    % =====================================================================
    
    methods (Access = public) % private!
        
        function clearAll(obj)
            obj.clearMkeDet();
            obj.calibdata = {};
            obj.profile = '';%{ "detector":{ "type": "BoxLut",  "classifier":  "CircLinear", "threshold": 8,  "peak_ratio": 1.2,  "curv_ratio": 1.15, "no_epochs": 3,  "box_delta": 2.0,  "no_threads": 4}, "postproc":{ "type" :  "PassThru"}}';
            obj.im_data = [];
            obj.im_fpath = '';
            obj.det_idx = 0;
            obj.res = [];
            obj.dimgs = [];
            obj.mkemdd_params = struct;
            
            obj.mkedd = MkeddDepthSensor();
        end
        
        % =================================================================
        
        function clearMkeDet(obj)
            obj.mkedet.fpath = '';
            obj.mkedet.laser_pattern = [];
            obj.mkedet.dd_sectype = 0;
            obj.mkedet.pp_sectype = 0;
        end
        
        % =================================================================
        
        function initDdSectionTypes(obj)
            obj.dd_stypes = { ...
                'DD_SECTION_UNKNOWN', 0; ...
                'DD_SECTION_LUT3D2DID', 1; ...
                'DD_SECTION_LUTSV2', 2; ...
                'DD_SECTION_COLLUT', 3; ...
            };
        end        
        
        % =================================================================
        
        function initPpSectionTypes(obj)
            obj.pp_stypes = { ...
                'PP_SECTION_UNKOWN', 0; ...
                'PP_SECTION_EMPTY', 1; ...
            };
        end         
        
        % =================================================================
        % =================================================================

        function showDdSectionLut3D2DID(obj, i)
            cd = obj.calibdata{i};
            fprintf(1, '    Detector index: %d\n', i);
            fprintf(1, '      Image buffer dims: %d x %d\n', cd.ccalib.frame_size(1), cd.ccalib.frame_size(2));
            fprintf(1, '      Image buffer stride: %d\n', cd.ccalib.stride);
            fprintf(1, '      Number of paths: %d\n', cd.accums.no_segs);
            fprintf(1, '      Number of path elements: %d\n', cd.accums.no_pels);
            fprintf(1, '      Lengths of PixelList: %d\n', cd.accums.no_pxs);
        end   
        
        % =================================================================
        
        function showDdSectionLutsV2(obj, i)
            cd = obj.calibdata{i};
            fprintf(1, '    Detector index: %d\n', i);
            fprintf(1, '      Image buffer dims: %d x %d\n', cd.ccalib.frame_size(1), cd.ccalib.frame_size(2));
            fprintf(1, '      Image buffer stride: %d\n', cd.ccalib.stride);
            fprintf(1, '      Lut3D depth min: %d\n', cd.fbp.depth_min);
            fprintf(1, '      Lut3D depth max: %d\n', cd.fbp.depth_max);            
            fprintf(1, '      Number of paths: %d\n', cd.accums.no_segs);
            fprintf(1, '      Number of path elements: %d\n', cd.accums.no_pels);
            fprintf(1, '      Lengths of PixelList: %d\n', cd.accums.no_pxs);
        end
        
        % =================================================================
        
        function showDdSectionColLut(obj, i)
            cd = obj.calibdata{i};
            fprintf(1, '    Detector index: %d\n', i);
            fprintf(1, '      Image buffer dims: %d x %d\n', cd.ccalib.frame_size(1), cd.ccalib.frame_size(2));
            fprintf(1, '      Image buffer stride: %d\n', cd.ccalib.stride);
            fprintf(1, '      Lut3D depth min: %d\n', cd.fbp.depth_min);
            fprintf(1, '      Lut3D depth max: %d\n', cd.fbp.depth_max);            
            fprintf(1, '      BoxDet kernel widths: [');            
            fprintf(1, "%d ", cd.accums.kwidths);
            fprintf(1, '\b]\n');            
            fprintf(1, '      Number of paths: %d\n', cd.accums.no_segs);
            fprintf(1, '      Number of path elements: %d\n', cd.accums.no_pels);
            fprintf(1, '      Lengths of PixelList: %d\n', cd.accums.no_pxs);
        end                              
        
        % =================================================================
        % =================================================================
        
        function plotScene(~, pts, uids)
            figure;
            hold on;
            
            no_pts = size(pts, 2);
            ds = sqrt(sum(pts.^2))';
            
            for i = 1:no_pts
                text(pts(1, i), pts(2, i), pts(3, i), sprintf('%d', uids(i)));
            end
            
            scatter3(pts(1, :), pts(2, :), pts(3, :), 20 *ones(1, no_pts), ds, 'filled')
            colormap('jet');
            axis('equal');
        end
        
        % =================================================================
        
        function showPathsAndDetections(obj, im, pts, det_idx, uids)
            if (~isempty(im))
                figure;
                imshow(im);
                hold('on');
            end
            
            if (~isempty(pts))
                if (~iscell(pts))
                    pts = {pts};
                end
                
                no_dets = numel(pts);
                colors = MkEDetector.getDistinctColors(no_dets + 2);
                colors(:, 2:end);
                markers = {'d', 'x'};
                
                for i = 1:no_dets
                    u = pts{i} + 1;
                    plot(u(:, 1), u(:, 2), 'd', 'Marker', markers{mod(i, 2) + 1}, 'Color', colors(i, :));
                end
            end
            
            % 2d paths %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            if (det_idx == 0)
                return;
            end
            
            if (isempty(obj.calibdata))
                error('Cannot show: set mkedet calibration file first');
            end
            
            
            cd = obj.calibdata{det_idx};
            if (~isempty(uids))
                paths_idx = [];
                for j = 1:numel(uids)
                    paths_idx = [paths_idx, find(cd.bb.seg2uid == uids(j))];
                end
            else
                paths_idx = 1:numel(cd.ps.paths);
            end
            
            for j = paths_idx
                p2d = cd.ps.paths{j};

                if (isempty(p2d))
                    continue;
                end
                
                pos = find(p2d(1, :) ~= 0, 1);
                
                p2d = p2d + 1;
                
                if (~size(p2d, 2))
                    continue;
                end
                
                aflags = logical(cd.ps.aflags{j});
                
                if (sum(aflags))
                    % path elements
                    plot(p2d(1, aflags), p2d(2, aflags), '.b');
                end
                
                if (sum(~aflags))
                    % shoulder elements
                    plot(p2d(1, ~aflags), p2d(2, ~aflags), '.r');
                end
                
                text(p2d(1, pos) + 1, p2d(2, pos) + 1, sprintf('%d', cd.bb.seg2uid(j)), 'Color', [0 1 1]);
            end
        end          
        
        % =================================================================
        % =================================================================
        
        
        function idx = parsePpSectionEmpty(obj, dd_no, data, idx) %#ok<INUSL>
            [num_bytes, idx] = MkEDetector.readData(data, idx, 1, 4, 'uint32');
            if (num_bytes)
                error('Cannot parse: corrupted postprocessor section');
            end
        end
        
        % =================================================================
        
        function idx = parseDdSectionLut3D2DID(obj, dd_no, data, idx)
            
            [det_num_bytes, idx] = MkEDetector.readData(data, idx, 1, 4, 'uint32'); %#ok<ASGLU>
            
            % ccalib
            
            [obj.calibdata{dd_no}.ccalib.frame_size, idx] = MkEDetector.readData(data, idx, 2, 4, 'uint32');
            [obj.calibdata{dd_no}.ccalib.stride, idx] = MkEDetector.readData(data, idx, 1, 4, 'uint32');
            
            %%% accums
            
            [no_segs, idx] = MkEDetector.readData(data, idx, 1, 4, 'uint32');
            obj.calibdata{dd_no}.accums.no_segs = no_segs;
            [no_pels, idx] = MkEDetector.readData(data, idx, 1, 4, 'uint32');
            obj.calibdata{dd_no}.accums.no_pels = no_pels;
            [no_pxs, idx] = MkEDetector.readData(data, idx, 1, 4, 'uint32');
            obj.calibdata{dd_no}.accums.no_pxs = no_pxs;
            
            [alengths, idx] = MkEDetector.readData(data, idx, no_segs, 4, 'uint32');
            obj.calibdata{dd_no}.accums.lengths = alengths;
            obj.calibdata{dd_no}.accums.offs = [1; cumsum(alengths) + 1];
            
            % aflags
            for i = 1:no_segs
                if (alengths(i))
                    [obj.calibdata{dd_no}.ps.aflags{i}, idx] = MkEDetector.readData(data, idx, alengths(i), 1, 'uint8');
                else
                    obj.calibdata{dd_no}.ps.aflags{i} = [];
                end
            end
            
            % pxlist
            [pxlist, idx] = MkEDetector.readData(data, idx, no_pxs * 5, 2, 'uint16');
            obj.calibdata{dd_no}.accums.pxlist = reshape(pxlist,[5, no_pxs]);
            
            %%% lut
            
            [no_pels2, idx] = MkEDetector.readData(data, idx, 1, 4, 'uint32');
            [no1, idx] = MkEDetector.readData(data, idx, 1, 4, 'uint32');
            [no24, idx] = MkEDetector.readData(data, idx, 1, 4, 'uint32');
            
            if (no_pels2 ~= no_pels || no1 ~= 1 || no24 ~= 24)
                error('Cannot parse: corrupted lookup table header');
            end
            
            obj.calibdata{dd_no}.bb.seg2uid = zeros(1, no_segs);
            obj.calibdata{dd_no}.bb.seg2lid = zeros(1, no_segs);
            obj.calibdata{dd_no}.bb.seg2did = zeros(1, no_segs);
            
            for i = 1:no_segs
                if (alengths(i) == 0)
                    obj.calibdata{dd_no}.ps.paths{i} = [];
                    obj.calibdata{dd_no}.ps.paths3D{i} = [];
                    obj.calibdata{dd_no}.ps.sigma3D{i} = [];
                    continue;
                else
                    obj.calibdata{dd_no}.ps.paths{i} = zeros(2, alengths(i));
                    obj.calibdata{dd_no}.ps.paths3D{i} = zeros(3, alengths(i));
                    obj.calibdata{dd_no}.ps.sigma3D{i} = zeros(1, alengths(i));
                end
                
                [luts, idx] = MkEDetector.readData(data, idx, alengths(i) * 24, 1, 'uint8');
                luts = reshape(luts, [24, alengths(i)]);
                obj.calibdata{dd_no}.ps.paths{i}(1, :) = typecast(reshape(luts(1:4, :), 1, []), 'int32');
                obj.calibdata{dd_no}.ps.paths{i}(2, :) = typecast(reshape(luts(5:8, :), 1, []), 'int32');
                obj.calibdata{dd_no}.ps.sigma3D{i} = typecast(reshape(luts(9:12, :), 1, []), 'int32');
                obj.calibdata{dd_no}.ps.paths3D{i}(1, :) = typecast(reshape(luts(13:14, :), 1, []), 'int16');
                obj.calibdata{dd_no}.ps.paths3D{i}(2, :) = typecast(reshape(luts(15:16, :), 1, []), 'int16');
                obj.calibdata{dd_no}.ps.paths3D{i}(3, :) = typecast(reshape(luts(17:18, :), 1, []), 'int16');
                
                ids = typecast(reshape(luts(19:20, :), 1, []), 'int16');
                obj.calibdata{dd_no}.bb.seg2uid(i) = ids(1);
                ids = typecast(reshape(luts(21:22, :), 1, []), 'int16');
                obj.calibdata{dd_no}.bb.seg2lid(i) = ids(1);
                ids = typecast(reshape(luts(23:24, :), 1, []), 'int16');
                obj.calibdata{dd_no}.bb.seg2did(i) = ids(1);
                
                obj.calibdata{dd_no}.ps.paths{i} = double(obj.calibdata{dd_no}.ps.paths{i}) / 65536; % fpfloat16 -> double            
            end            

        end
        
        % =================================================================
        
        function idx = parseDdSectionLutsV2(obj, dd_no, data, idx)
            
            [section_num_bytes, idx] = MkEDetector.readData(data, idx, 1, 4, 'uint32'); %#ok<ASGLU>
            
            % 2D buffer info %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            [obj.calibdata{dd_no}.ccalib.frame_size, idx] = MkEDetector.readData(data, idx, 2, 4, 'uint32');
            [obj.calibdata{dd_no}.ccalib.stride, idx] = MkEDetector.readData(data, idx, 1, 4, 'uint32');
            
            %%% 3D info %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            [obj.calibdata{dd_no}.fbp.depth_min, idx] = MkEDetector.readData(data, idx, 1, 4, 'uint32');
            [obj.calibdata{dd_no}.fbp.depth_max, idx] = MkEDetector.readData(data, idx, 1, 4, 'uint32');
            
            %%% PixelList %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            [no_pxs, idx] = MkEDetector.readData(data, idx, 1, 4, 'uint32');
            obj.calibdata{dd_no}.accums.no_pxs = no_pxs;

            [pixellist, idx] = MkEDetector.readData(data, idx, no_pxs * 5, 2, 'uint16');
            obj.calibdata{dd_no}.accums.pixellist = pixellist;
            
            % Paths %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            [no_paths, idx] = MkEDetector.readData(data, idx, 1, 4, 'uint32');
            obj.calibdata{dd_no}.accums.no_segs = no_paths;
            [no_pels, idx] = MkEDetector.readData(data, idx, 1, 4, 'uint32');
            obj.calibdata{dd_no}.accums.no_pels = no_pels;
                   
            [path_lengths, idx] = MkEDetector.readData(data, idx, no_paths, 4, 'uint32');
            obj.calibdata{dd_no}.accums.lengths = path_lengths;
            obj.calibdata{dd_no}.accums.offs = [1; cumsum(path_lengths) + 1];
            
            % Path masks
            obj.calibdata{dd_no}.ps.aflags = cell(1, no_paths); 
            for i = 1:no_paths
                if (path_lengths(i))
                    [obj.calibdata{dd_no}.ps.aflags{i}, idx] = MkEDetector.readData(data, idx, path_lengths(i), 1, 'uint8');
                else
                    obj.calibdata{dd_no}.ps.aflags{i} = [];
                end
            end
            
            % Path mask bounds
            [obj.calibdata{dd_no}.ps.afbounds, idx] = MkEDetector.readData(data, idx, no_paths * 2, 4, 'uint32');
            obj.calibdata{dd_no}.ps.afbounds = reshape(obj.calibdata{dd_no}.ps.afbounds, [2, no_paths]);
                        
            % iluts
            [iluts, idx] = MkEDetector.readData(data, idx, no_paths * 3, 4, 'uint32');
            iluts = reshape(iluts, [3, no_paths]);
            obj.calibdata{dd_no}.bb.seg2uid = iluts(1, :);
            obj.calibdata{dd_no}.bb.seg2lid = iluts(2, :);
            obj.calibdata{dd_no}.bb.seg2did = iluts(3, :);
            
            % luts
            for i = 1:no_paths
                if (path_lengths(i) == 0)
                    obj.calibdata{dd_no}.ps.paths{i} = [];
                    obj.calibdata{dd_no}.ps.paths3D{i} = [];
                    obj.calibdata{dd_no}.ps.sigma3D{i} = [];
                    continue;
                else
                    obj.calibdata{dd_no}.ps.paths{i} = zeros(2, path_lengths(i));
                    obj.calibdata{dd_no}.ps.paths3D{i} = zeros(3, path_lengths(i));
                    obj.calibdata{dd_no}.ps.sigma3D{i} = zeros(1, path_lengths(i));
                end
                
                [luts, idx] = MkEDetector.readData(data, idx, path_lengths(i) * 18, 1, 'uint8');
                luts = reshape(luts, [18, path_lengths(i)]);
                obj.calibdata{dd_no}.ps.paths{i}(1, :) = typecast(reshape(luts(1:4, :), 1, []), 'int32');
                obj.calibdata{dd_no}.ps.paths{i}(2, :) = typecast(reshape(luts(5:8, :), 1, []), 'int32');
                obj.calibdata{dd_no}.ps.sigma3D{i} = typecast(reshape(luts(9:12, :), 1, []), 'int32');
                obj.calibdata{dd_no}.ps.paths3D{i}(1, :) = typecast(reshape(luts(13:14, :), 1, []), 'int16');
                obj.calibdata{dd_no}.ps.paths3D{i}(2, :) = typecast(reshape(luts(15:16, :), 1, []), 'int16');
                obj.calibdata{dd_no}.ps.paths3D{i}(3, :) = typecast(reshape(luts(17:18, :), 1, []), 'int16');
                
                obj.calibdata{dd_no}.ps.paths{i} = double(obj.calibdata{dd_no}.ps.paths{i}) / 65536; % fpfloat16 -> double
            end
        end        
        
        % =================================================================
                
        function idx = parseDdSectionColLut(obj, dd_no, data, idx)        
        
            [section_num_bytes, idx] = MkEDetector.readData(data, idx, 1, 4, 'uint32'); %#ok<ASGLU>
            
            % 2D buffer info %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            [obj.calibdata{dd_no}.ccalib.frame_size, idx] = MkEDetector.readData(data, idx, 2, 4, 'uint32');
            [obj.calibdata{dd_no}.ccalib.stride, idx] = MkEDetector.readData(data, idx, 1, 4, 'uint32');
            
            %%% 3D info %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            [obj.calibdata{dd_no}.fbp.depth_min, idx] = MkEDetector.readData(data, idx, 1, 4, 'uint32');
            [obj.calibdata{dd_no}.fbp.depth_max, idx] = MkEDetector.readData(data, idx, 1, 4, 'uint32');
                        
            % Kernel widths %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            [no_kwidths, idx] = MkEDetector.readData(data, idx, 1, 4, 'uint32');
            [obj.calibdata{dd_no}.accums.kwidths, idx] = MkEDetector.readData(data, idx, no_kwidths, 2, 'uint16');
            
            % Paths %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            [no_paths, idx] = MkEDetector.readData(data, idx, 1, 4, 'uint32');
            obj.calibdata{dd_no}.accums.no_segs = no_paths;
            [no_pels, idx] = MkEDetector.readData(data, idx, 1, 4, 'uint32');
            obj.calibdata{dd_no}.accums.no_pels = no_pels;      
            [max_pels, idx] = MkEDetector.readData(data, idx, 1, 4, 'uint32');
            obj.calibdata{dd_no}.accums.max_pels = max_pels;               
            
            % CollatedPath
            obj.calibdata{dd_no}.accums.no_pxs = 0;
            obj.calibdata{dd_no}.accums.pixellist = [];
            
            obj.calibdata{dd_no}.ps.afbounds = zeros(2, no_paths);
            obj.calibdata{dd_no}.ps.aflags = cell(1, no_paths); 
            obj.calibdata{dd_no}.accums.lengths = zeros(1, no_paths);
            
            obj.calibdata{dd_no}.bb.seg2uid = zeros(1, no_paths);
            obj.calibdata{dd_no}.bb.seg2lid = zeros(1, no_paths);
            obj.calibdata{dd_no}.bb.seg2did = zeros(1, no_paths);            
            
            obj.calibdata{dd_no}.ps.paths = cell(1, no_paths);
            obj.calibdata{dd_no}.ps.paths3D = cell(1, no_paths);
            obj.calibdata{dd_no}.ps.sigma3D = cell(1, no_paths);
            
            for i = 1:no_paths
                % Path info
                [no_pels, idx] = MkEDetector.readData(data, idx, 1, 2, 'uint16');
                no_pels = uint32(no_pels);
                obj.calibdata{dd_no}.accums.lengths(i) = no_pels;
                
                [ids, idx] = MkEDetector.readData(data, idx, 3, 4, 'uint32');
                
                obj.calibdata{dd_no}.bb.seg2uid(i) = ids(1);
                obj.calibdata{dd_no}.bb.seg2lid(i) = ids(2);
                obj.calibdata{dd_no}.bb.seg2did(i) = ids(3);                     
                
                % Pixel List
                %{
                [no_pxs, idx] = MkEDetector.readData(data, idx, 1, 2, 'uint16');
                no_pxs = uint32(no_pxs);
                obj.calibdata{dd_no}.accums.no_pxs = obj.calibdata{dd_no}.accums.no_pxs + no_pxs;

                [obj.calibdata{dd_no}.accums.buffer_offset{i}, idx] = MkEDetector.readData(data, idx, 1, 4, 'uint32');                
                [pixellist, idx] = MkEDetector.readData(data, idx, no_pxs * 3, 2, 'uint16');
                obj.calibdata{dd_no}.accums.pixellist = [obj.calibdata{dd_no}.accums.pixellist; pixellist];
                %}
                
                % Mask
                [afbounds, idx] = MkEDetector.readData(data, idx, 2, 2, 'uint16');
                obj.calibdata{dd_no}.ps.afbounds(:, i) = afbounds;
                
                no_bytes = bitshift(no_pels + 7, -3); %floor((double(no_pels) + 7) / 8);
                [bit_aflags, idx] = MkEDetector.readData(data, idx, no_bytes, 1, 'uint8');
                
                char_aflags = zeros(1, no_pels, 'uint8');
                
                for j = 1:no_pels
                    uj = uint32(j - 1);
                    byte = bitshift(uj, -3);
                    bit = bitand(uj, 7);
                    %disp([byte, bit]);
                    
                    char_aflags(j) = bitget(bit_aflags(byte + 1), bit + 1);
                end

                obj.calibdata{dd_no}.ps.aflags{i} = char_aflags;
                
                % ElementsLut
                [luts, idx] = MkEDetector.readData(data, idx, no_pels * 12, 1, 'uint8');
                luts = reshape(luts, [12, no_pels]);
                
                ui = typecast(reshape(luts(1:2, :), 1, []), 'uint16');
                vi = typecast(reshape(luts(3:4, :), 1, []), 'uint16');
                uvf = typecast(reshape(luts(5:6, :), 1, []), 'uint16');
                
                uf = bitand(uvf, hex2dec('FF00'));                
                vf = bitshift(bitand(uvf, hex2dec('FF')), 8);
               
                us = typecast(kron(uf, uint16([1 0])) + kron(ui, uint16([0 1])), 'int32');
                vs = typecast(kron(vf, uint16([1 0])) + kron(vi, uint16([0 1])), 'int32');
                
                obj.calibdata{dd_no}.ps.paths{i}(1, :) = us;
                obj.calibdata{dd_no}.ps.paths{i}(2, :) = vs;
                
                obj.calibdata{dd_no}.ps.paths3D{i}(1, :) = typecast(reshape(luts(7:8, :), 1, []), 'int16');
                obj.calibdata{dd_no}.ps.paths3D{i}(2, :) = typecast(reshape(luts(9:10, :), 1, []), 'int16');
                obj.calibdata{dd_no}.ps.paths3D{i}(3, :) = typecast(reshape(luts(11:12, :), 1, []), 'int16');
                
                obj.calibdata{dd_no}.ps.paths{i} = double(obj.calibdata{dd_no}.ps.paths{i}) / 65536; % fpreal16 -> double    
                obj.calibdata{dd_no}.ps.sigma3D{i} = zeros(1, size(obj.calibdata{dd_no}.ps.paths{i}, 2));
            end
            
            obj.calibdata{dd_no}.accums.offs = [1; cumsum(obj.calibdata{dd_no}.accums.lengths)' + 1];
            
        end
              
        % =================================================================        
        
        function parseMkEDetData(obj, data)
            idx = 1;
            
            %%% Header
            
            % magik
            [magik, idx] = MkEDetector.readData(data, idx, 8, 1, 'uint8');
            
            if (~strcmp(char(magik'), 'MKEDET01'))
                error('Cannot parse: "MKEDET01" magic string not found');
            end
            
            % no_dets
            [no_dets, idx] = MkEDetector.readData(data, idx, 1, 4, 'uint32');
            obj.calibdata = cell(1, no_dets);
            obj.mkedet.no_dets = no_dets;
            
            % dd_sectype
            [obj.mkedet.dd_sectype, idx] = MkEDetector.readData(data, idx, 1, 4, 'uint32');            

            % pp_sectype
            [obj.mkedet.pp_sectype, idx] = MkEDetector.readData(data, idx, 1, 4, 'uint32');
            
            %%% Info
            
            [info_num_bytes, idx] = MkEDetector.readData(data, idx, 1, 4, 'uint32');
            [obj.mkedet.info, idx] = MkEDetector.readData(data, idx, info_num_bytes, 1, 'uint8');
            obj.mkedet.info = char(obj.mkedet.info');
            
            %%% Laser pattern
            
            [obj.mkedet.laser_pattern, idx] = MkEDetector.readData(data, idx, no_dets, 4, 'uint32');
            
            %%% Detectors
            
            for i = 1:no_dets
                if (obj.mkedet.dd_sectype == 1)
                    idx = obj.parseDdSectionLut3D2DID(i, data, idx);
                elseif (obj.mkedet.dd_sectype == 2)
                    idx = obj.parseDdSectionLutsV2(i, data, idx);
                elseif (obj.mkedet.dd_sectype == 3)
                    idx = obj.parseDdSectionColLut(i, data, idx);                         
                else
                    error('Cannot parse: unknown detector section');
                end
            end
            
            %%% Postprocs
            
            for i = 1:no_dets
                if (obj.mkedet.pp_sectype == 1)
                    idx = obj.parsePpSectionEmpty(i, data, idx);
                else
                    error('Cannot parse: unknown detector section');
                end
            end
            
            idx;  %#ok<VUNUS>
        end
        
        % =================================================================
        % =================================================================
        
        
    end
    
    % =====================================================================
    % =====================================================================
    % =====================================================================
    
    methods (Access = public)
        function obj = MkEDetector()
            obj.clearAll();
            obj.initDdSectionTypes();
            obj.initPpSectionTypes();
        end
        
        % =================================================================
        
        function setMkEDetFile(obj, fname)
            if (~exist(fname, 'file'))
                error('Cannot parse %s: file does not exist', fname);
            end
            
            fid = fopen(fname);
            data = uint8(fread(fid, 'uint8'));
            fclose(fid);
            
            obj.parseMkEDetData(data);
            
            obj.mkedet.fpath = fname;
            
            obj.mkedd.addCalibFile(fname);
            if (isempty(obj.profile))
                obj.mkedd.setDefaultProfile();
                obj.profile = obj.mkedd.getProfile();
            else
                obj.mkedd.setProfile(obj.profile);                
            end
        end
        
        % =================================================================
        
        function setProfileStr(obj, profile, do_not_apply)
            obj.profile = profile;
            
            if (nargin < 3 || ~do_not_apply)
                obj.mkedd.setProfile(profile);
            end
        end
        
        % =================================================================
        
        function setMkemddParams(obj, params)
            obj.mkemdd_params = params;
        end        
        
        % =================================================================
        
        function p = getProfileStr(obj)
            p = obj.profile;
        end
        
        % =================================================================
        
        function setProfile(obj, profile, do_not_apply)
            obj.profile = jsonencode(profile);
            
            if (nargin < 3 || ~do_not_apply)
                obj.mkedd.setProfile(profile);
            end
        end
        
        % =================================================================
        
        function p = getProfile(obj)
            p = jsondecode(obj.profile);
        end
        
        % =================================================================
        
        function setDetIdx(obj, det_idx)
            obj.det_idx = det_idx;
        end
        
        % =================================================================
        
        function p = getDetIdx(obj)
            p = obj.det_idx;
        end

        % =================================================================
        
        function setDetIdxByLaserPattern(obj, laser_pattern)
            obj.det_idx = obj.getDetIdxByLaserPattern(laser_pattern);
        end
        
        % =================================================================
                
        function p = getDetIdxByLaserPattern(obj, laser_pattern)
            p = find(obj.mkedet.laser_pattern == laser_pattern);
        end
                
        % =================================================================

        function importDebugImages(obj, dimgs_dir)
            if (~exist(dimgs_dir, 'dir'))
                error('Cannot import: directory "%s" does not exist, dimgs_dir');
            end

            files = dir(dimgs_dir);
            fnames = {files(:).name};
            [~, idx] = sort(fnames);
            csv_idx = idx(cellfun(@(x) contains(x, 'csv'), fnames));
            pgm_idx = idx(cellfun(@(x) contains(x, 'pgm'), fnames));

            no_imgs = numel(pgm_idx);
            if (numel(csv_idx) ~= no_imgs)
                error('Cannot import: different number of PGM and CSV files');
            end
            
            
            dbg_images.image = cell(1, no_imgs);
            dbg_images.frame = cell(1, no_imgs);
            
            % Read PGM images
            c = 1;
            for i = pgm_idx
                dbg_images.image{c} = imread(fullfile(dimgs_dir, files(i).name));
                c = c + 1;
            end
            
            % Read CSV files       
            c = 1;            
            for i = csv_idx
                fname = fullfile(dimgs_dir, files(i).name);
                fid = fopen(fname);
                
                % subheader
                tline = fgetl(fid);
                members = strsplit(tline, ';');
                tline = fgetl(fid);
                values = str2double(strsplit(tline, ';'));
                sh = struct;
                for j = 1:numel(members)
                    sh.(members{j}) = values(j);
                end
                dbg_images.subheaders(c) = sh;
                
                % frame data members
                tline = fgetl(fid);
                members = strsplit(tline, ';');
                fclose(fid);
                
                % frame data
                data = [];
                try
                    data = dlmread(fname, ';', 3, 0);
                catch
                    warning('No data: %s', fname);
                end
                
                no_data = size(data, 1);
                
                items = struct('X', [], 'u', [], 'uid', []);
                
                if (~no_data)
                    dbg_images.frame{c}.items = items;
                    c = c + 1;
                    continue;
                end
                
                items(no_data).X = [];
                                
                % 3D points
                x_idx = find(strcmp(members, 'x'));
                y_idx = find(strcmp(members, 'x'));
                z_idx = find(strcmp(members, 'x'));
                
                u_idx = find(strcmp(members, 'u'));
                v_idx = find(strcmp(members, 'v'));
                
                uid_idx = find(strcmp(members, 'uidx'));
                
                for j = 1:no_data
                    if (~isempty(x_idx) && ~isempty(y_idx) && ~isempty(z_idx))
                        items(j).X = [data(j, x_idx), data(j, y_idx), data(j, z_idx)];                        
                    end

                    if (~isempty(u_idx) && ~isempty(v_idx))
                        items(j).u = [data(j, u_idx), data(j, v_idx)] / 65536; % fpfloat16 -> double                  
                    end
                    
                    if (~isempty(uid_idx))
                        items(j).uid = data(j, uid_idx);
                    end
                end
                
                dbg_images.frame{c}.items = items;
                c = c + 1;
            end
            
            
            
            obj.setDebugImages(dbg_images);
        end            
                
        % =================================================================
        
        function setDebugImages(obj, dimgs)
            obj.dimgs = dimgs;
        end            
        
        % =================================================================

        function im = getDebugImage(obj, im_idx)
            im = obj.dimgs.image{im_idx};
        end            
        
        % =================================================================
        
        function setImage(obj, idata)
            if (ischar(idata))
                obj.im_data = imread(idata);
                obj.im_fpath = idata;
            else
                obj.im_data = idata;
            end
        end

        % =================================================================

        function setImageFromDebugImages(obj, img_idx)
            if (isempty(obj.dimgs))
                error('Cannot set image: set debug images structure first');
            end
            
            lpattern = obj.dimgs.subheaders(img_idx).laser_pattern;
            obj.det_idx = find(obj.mkedet.laser_pattern == lpattern, 1);            
            obj.im_data = obj.dimgs.image{img_idx};                 
        end
        
        % =================================================================
        
        function im = getImage(obj)
            im = obj.im_data;
        end
        
        % =================================================================
        
        function res = getResults(obj)
            res = obj.res;
        end

        % =================================================================
    
        function items = getResultsAsItems(obj)
            % Return results in MKE_REQUEST_GET_DEBUG_IMAGES format
            
            no_dets = numel(obj.res.idx);
            
            items(no_dets).X = [];
            items(no_dets).u = [];
            items(no_dets).uid = [];
            
            for i = 1:no_dets
                items(i).X = int16(obj.res.lut3d(i, :)');
                items(i).u = obj.res.lut2d(i, :)';
                items(i).uid = obj.res.lutid(i, 1);
            end
        end            
        
        % =================================================================

        function item = getDebugItem(obj, img_idx, path_id)
            uids = [obj.dimgs.frame{img_idx}.items.uid];
            item = obj.dimgs.frame{img_idx}.items(uids == path_id);
        end
        
        
        % =================================================================
        % =================================================================
        
        function importMketsDetections(json_path)
            
            
        end
        
        % =================================================================
        % =================================================================
        
        function execute(obj, idata, det_idx)
            if (nargin > 2)
                obj.setDetIdx(det_idx);
            end
            if (nargin > 1)
                obj.setImage(idata);
            end
            
            if (obj.det_idx == 0)
                error('DetIdx not set');
            end
            
            if (obj.mkedd.getNoDetectors < obj.det_idx)
                error('Illegal value of DetIdx set. Is calibration file loaded?');                
            end            
            
            obj.mkedd.process(obj.det_idx - 1, obj.im_data);
            obj.res = obj.mkedd.getResults(obj.det_idx - 1);
            
            % different JSON to Matlab conversion
            obj.res.lut2d = [obj.res.lut2d{:}]';
            obj.res.lut3d = [obj.res.lut3d{:}]';
            
            obj.dbgdata = obj.mkedd.getDebugData(obj.det_idx - 1);
            
            %{
            im = MkEDetector.addStrideToImage(obj.im_data, obj.calibdata{obj.det_idx}.ccalib.stride);
            
            % mkemdd parameters
            params = obj.mkemdd_params;
            params.profile = obj.profile;
            params.det_idx = obj.det_idx;
            params.calib_path = obj.mkedet.fpath;
            params.image = im;
            params.log = 1;
            
            obj.res = mkemdd(params);     
            
            cd = obj.calibdata{obj.det_idx};
            no_paths = cd.accums.no_segs;
            offs = cd.accums.offs;
            obj.res.accums = cell(1, no_paths);
            for i = 1:no_paths
                if(~cd.accums.lengths(i))
                    obj.res.accums{i} = [];
                else
                    obj.res.accums{i} = double(obj.res.accdump(offs(i):(offs(i+1)-1))) / 65536; % fpfloat16 -> double
                end
            end
            %}
        end
        
        % =================================================================
        % =================================================================
                
        function showMkEDetInfo(obj)
        
            if (isempty(obj.mkedet.fpath))
                error('Cannot show MkEDet info: no file loaded');
            end
            
            dd_sname = MkEDetector.getSectionName(obj.dd_stypes, obj.mkedet.dd_sectype);
            pp_sname = MkEDetector.getSectionName(obj.pp_stypes, obj.mkedet.pp_sectype);
            
            
            fprintf(1, 'MkEDet File info\n');
            fprintf(1, '  File name: %s\n', obj.mkedet.fpath);
            fprintf(1, '  Info section: %s\n', obj.mkedet.info);
            fprintf(1, '  No detectors: %d\n', obj.mkedet.no_dets);
            fprintf(1, '  DD section type: %s\n', dd_sname);
            fprintf(1, '  PP section type: %s\n', pp_sname);
            fprintf(1, '\n');

            for i=1:obj.mkedet.no_dets
                if (obj.mkedet.dd_sectype == 1)
                    obj.showDdSectionLut3D2DID(i);
                elseif (obj.mkedet.dd_sectype == 2)
                    obj.showDdSectionLutsV2(i);
                elseif (obj.mkedet.dd_sectype == 3)
                    obj.showDdSectionColLut(i);
                end 
            end
        end
        
        % =================================================================
        
        function showImage(obj)
            obj.showResults();
        end
        
        % =================================================================
        
        function showDebugImage(obj, img_idx, uid)
            if (isempty(obj.dimgs))
                error('Cannot show: set debug images structure first');
            end
            
            lpattern = obj.dimgs.subheaders(img_idx).laser_pattern;
            didx = find(obj.mkedet.laser_pattern == lpattern, 1);            
            im = obj.dimgs.image{img_idx};            
            pts = [obj.dimgs.frame{img_idx}.items(:).u];
            pts = reshape(pts, [2, numel(pts) / 2])';

            if (nargin < 3)
                uid = [];
            end
            
            obj.showPathsAndDetections(im, pts, didx, uid);      
            title(sprintf('Debug image idx. %d', img_idx));
        end
        
        % =================================================================
        
        function showDebugImageAndResults(obj, img_idx)
            if (isempty(obj.dimgs))
                error('Cannot show: set debug images structure first');
            end
            
            if (~isfield(obj.res, 'lut2d'))
                error('Cannot show: run detector first');
            end            
            
            lpattern = obj.dimgs.subheaders(img_idx).laser_pattern;
            didx = find(obj.mkedet.laser_pattern == lpattern, 1);            
            im = obj.dimgs.image{img_idx};            
            pts = [obj.dimgs.frame{img_idx}.items(:).u];
            pts = reshape(pts, [2, numel(pts) / 2])';
            u = obj.res.lut2d;
            
            obj.showPathsAndDetections(im, {pts, u}, didx);            
        end        
        
        % =================================================================
        
        function showDebugScene(obj, img_idx)
            lpattern = obj.dimgs.subheaders(img_idx).laser_pattern;                       
            pts = [obj.dimgs.frame{img_idx}.items(:).X];
            pts = double(reshape(pts, [3, numel(pts) / 3]));
            uids = [obj.dimgs.frame{img_idx}.items(:).uid];
            
            obj.plotScene(pts, uids);
        end
        
        % =================================================================
        
        function showScene(obj)
            pts = obj.res.lut3d';
            uids = obj.res.uid;
            
            obj.plotScene(pts, uids);
        end        
        
        % =================================================================
                      
        function showResults(obj, uids)            
            if (~obj.det_idx)
                error('Cannot show: DetIdx not set');
            end
            
            if (isempty(obj.im_data))
                error('Cannot show: image not set');
            end
            
            % detections
            if (~isfield(obj.res, 'lut2d'))
                u = zeros(0, 2);
            else
                u = obj.res.lut2d;
            end
            
            % 2d paths
            if (nargin < 2)
              uids = [];
            end
    
            obj.showPathsAndDetections(obj.im_data, u, obj.det_idx, uids); 
            title('Offline detector results');
        end
        
        % =================================================================
        
        function showPaths2D(obj, det_idx)
            no_dets = numel(obj.calibdata);
            
            if (nargin == 1)
                det_idx = 1:no_dets;
                colors = MkEDetector.getDistinctColors(no_dets + 1);
                colors = colors(2:end, :);
            else
                colors = [0, 0, 1];
            end
            
            
            figure;
            hold('on');
            
            c = 1;
            for i = det_idx
                cd = obj.calibdata{i};
                for j = 1:numel(cd.ps.paths)
                    p2d = cd.ps.paths{j};
                    if (~size(p2d, 2))
                        continue;
                    end
                    
                    aflags = logical(cd.ps.aflags{j});
                    
                    if (sum(aflags))
                        % path elements
                        plot(p2d(1, aflags), p2d(2, aflags), '.', 'Color', colors(c, :));
                    end
                    
                    if (sum(~aflags))
                        % shoulder elements
                        plot(p2d(1, ~aflags), p2d(2, ~aflags), '.r');
                    end
                    
                    pos = find(p2d(1, :) ~= 0, 1);
                    text(p2d(1, pos) + 5, p2d(2, pos) + 5, sprintf('%d', cd.bb.seg2uid(j)));
                end
                c = c + 1;
            end
            
            vp = MkEDetector.getViewportRect(cd.ccalib.frame_size);
            plot(vp(1, :), vp(2, :), '-b');
            set(gca,'Ydir','reverse');
            axis('equal');
            title(obj.mkedet.fpath);
        end
       
        % =================================================================
        
        function showAccum(obj, acc_id)
            if (isempty(obj.res))
                error('Cannot show: run the detector first');
            end
            
            no_figs = 0;
            if (isfield(obj.dbgdata, 'accums'))
                no_figs = no_figs + 1;
            end
            
            if (isfield(obj.dbgdata, 'classifier'))
                 no_figs = no_figs + 1;
                 class_uids = [obj.dbgdata.classifier.patches(:).uid];                 
            end
            
            no_figs = no_figs * numel(acc_id);
            fs = SubFigure(no_figs);
            
            for i = acc_id
                
                cd = obj.calibdata{obj.det_idx};
                path_idx = find(cd.bb.seg2uid == i, 1);
                res_idx = find(obj.res.uid == i, 1);
                
                
                if (isempty(path_idx))
                    error('Cannot show: cannot find path ID %d', i);
                end
                
                % Accumulator
                if (isfield(obj.dbgdata, 'accums'))
                    fs.new();
                    title(sprintf('Accumulator id. %d', i));
                    
                    %path_idx = find(obj.res.idx == acc_id, 1);
                    accum = obj.dbgdata.accums{path_idx};
                    af = logical(cd.ps.aflags{path_idx});
                    no_els = numel(accum);
                    xs = 1:no_els;
                    
                    
                    if (~no_els)
                        warning('Nothing to show, empty accumulator');
                    end
                    
                    plot(xs, accum, '-', 'Color', [0.3 0.3 0.3]);
                    plot(xs(af), accum(af), '.b');
                    plot(xs(~af), accum(~af), '.r');
                    
                    if (isempty(res_idx))
                        return;
                    end
                    
                    %x = obj.res.accels(res_idx) - cd.accums.offs(path_idx) + 2;
                    x = obj.dbgdata.det_idx(path_idx) + 1;
                    plot(x, accum(x), 'xr');
                    text(double(x) + 3, double(accum(x)), sprintf('x: %d, a: %.3f\nr: %.3f', x, accum(x), obj.res.resp(res_idx)));
                end
                
                % 2D classifier
                if (isfield(obj.dbgdata, 'classifier'))
                    class_idx = find((class_uids == i));
                    
                    im = [];
                    
                    for j = class_idx
                        
                        patch = obj.dbgdata.classifier.patches(j);
                        pd = [patch.data{:}] / 255;
                        pz = zeros([size(pd), 3]);
                        
                        if (patch.is_dot)
                            pz(:, :, 2) = pd;
                        else
                            pz(:, :, 1) = pd;
                        end
                        im = [im, pz]; %#ok<AGROW>                        
                    end
                    
                    scale = 6;
                    
                    fs.new(imresize(im, scale, 'nearest'));
                    title(sprintf('Patch data for id: %d', i));
                    
                    [w, ~] = size(pd);
                    c = ceil(w/2);
                    for j = 1:numel(class_idx)
                        plot((c + (j - 1) * w) * scale, scale * c, 'cx');
                    end
                end
                
            end
        end
        
        % =================================================================
                           
        function rs = showPatches(obj, res, varargin)
            patches = obj.dbgdata.classifier.patches;
            res = obj.res;
            
            no_patches = numel(patches);
                        
            if (~no_patches)
                return;
            end
            
            %%%
            
            p = inputParser;
            addOptional(p, 'Sigma', 0, @isnumeric);
            addOptional(p, 'PositiveOnly', false, @islogical);
            addOptional(p, 'Patches', [], @isstruct);
            addOptional(p, 'Results', [], @isstruct);
            addOptional(p, 'PatchesIdx', [], @isnumeric);
            addOptional(p, 'ShowCentroid', true, @islogical);
            addOptional(p, 'ShowUid', false, @islogical);
            p.parse(varargin{:});
            
            %%%
            
            if (~isempty(p.Results.Patches))
                patches = p.Results.Patches;
            end
            
            if (~isempty(p.Results.Results))
                results = p.Results.Results;
            end
                        
            if (~isempty(p.Results.PatchesIdx))
                patches = patches(p.Results.PatchesIdx);
                no_patches = numel(patches);
            end
                
            if(p.Results.PositiveOnly)
                patches = patches([patches.is_dot]);
                no_patches = numel(patches);
            end
                
            h = fspecial('gaussian', 15, 5);
            wp = size(patches(1).data, 1);
            w = ceil(sqrt(no_patches));
            
            cdet = CircDetector;
            cdet = cdet.initMask([1,4,7;8, 20, 40]);
            
            
            bim = zeros(wp * w, wp * w, 3);
            [is, js] = ind2sub([w, w], 1:no_patches);
            us = (is - 1) * wp + 1;
            vs = (js - 1) * wp + 1;
            
            pdata = struct; %zeros(12, no_patches);
            
            figure;
            imshow(bim / 255);
            hold on;
            %plot(1,1,'x');
            hold on;
            
            %geqn = 'd * exp(-(a*(x-e)^2 + 2*b*(x-e)*(y-f)+c*(y-f)^2))';
            geqn = 'c * exp(-( (x-d)^2/(2*a^2) + (y-e)^2/(2*b^2)))';
            
%           for k = 1:no_patches


            for k = 1:no_patches
                fprintf('Patch %d/%d ... \n', k, no_patches);
                
                u = us(k);
                v = vs(k);
            
                patch = [patches(k).data{:}];
                patch_im = double(patch) / 255;
                %patch_im(patch_im(:) < .1) = 0;
                                
                if (p.Results.Sigma)
                    patch_im = imgaussfilt(patch_im, p.Results.Sigma);
                end
                
                patch_im = h .* patch_im;
                pmin = min(patch_im(:));
                pmax = max(patch_im(:));
                patch_im(patch_im(:) < (pmin + (pmax - pmin) * 0.1)) = 0;
                patch_im = patch_im / pmax;
                pdata(k).data = patch_im;
                 
                patch = [patches(k).data{:}];
                xpatch_im = double(patch);                
                dd = cdet.getDescriptors(xpatch_im, 'Type', 'MeanMax6D');
                %dd(2) = dd(2) * 2;
                %dd(dd==Inf) = 0;
                pdata(k).dd = dd;
                
                if (p.Results.ShowCentroid)                    
                    m00 = MkEDetector.central_moment(0, 0, patch_im);
                    m10 = MkEDetector.central_moment(1, 0, patch_im);
                    m01 = MkEDetector.central_moment(0, 1, patch_im);
                    m11 = MkEDetector.central_moment(1, 1, patch_im);
                    m20 = MkEDetector.central_moment(2, 0, patch_im);
                    m02 = MkEDetector.central_moment(0, 2, patch_im);
                                        
                    xc = m10 / m00;
                    yc = m01 / m00;
                    u20 = m20 / m00 - xc * xc;
                    u02 = m02 / m00 - yc * yc;
                    u11 = m11 / m00 - xc * yc;        
                    
                    
                    K = [u20, u11; u11, u02];
                    if (~any(isnan(K)))
                        
                        theta = 0.5 * atan((2*u11) / (u20 - u02));
                        e = eig(K);
                        e = sort(e, 'ascend');
                        ecc = sqrt(1 - e(2) / e(1));
                    else
                       theta = nan;
                       e = [0;0];
                       ecc = 0;                        
                    end                    
                    
                    pdata(k).ms = [m00, m10, m01, m11, m20, m02];
                    pdata(k).ms_centroid = [xc, yc];
                    pdata(k).ms_us = [u11, u20, u11];                    
                    pdata(k).ms_var = [theta, ecc, e(1)];
                    pdata(k).center = [v, u];

                    %Gaussian %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    
                    ls = [0:(wp-1)] / wp;
                    [xs, ys] = meshgrid(ls, ls);
                    xs = xs(:);
                    ys = ys(:);
                    
                    zs = patch_im;
                    zs = zs(:);                    
                    
                        pdata(k).gaussian  = [0, 0, 0, 0, 0];
                    
                    try
                    sf= fit([xs, ys], zs, geqn, 'Start', [.1 .1 1 xc/wp yc/wp]);                    
                    pdata(k).gaussian = [sqrt(abs(sf.a/2)) sqrt(abs(sf.b/2)) sf.c sf.d*wp sf.e*wp];

                   % figure;
                   % plot(sf,[xs,ys],zs);
                    
                    catch                        
                        pdata(k).gaussian  = [0, 0, 0, 0, 0];
                    end
                                            
                    % Parabola %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                      
                    pdata(k).parabola  = [0, 0, 0, 0, 0];
                    %{
                    try
                    lzs = log(zs); idx = (~isinf(lzs));
                    sf = fit([xs(idx), ys(idx)], zs(idx), 'poly22');

                    a = sf.p20;
                    b = sf.p02;
                    c = sf.p10;
                    d = sf.p01;
                    e = sf.p11;
                    f = sf.p00;
                    
                    pdata(k).parabola = [wp* -((2*b*c-d*e)/(4*a*b-e*e)),wp * -((2*a*d-c*e)/(4*a*b-e*e))];
                    
                   % figure;
                   % plot(sf,[xs(idx), ys(idx)], zs(idx));
                    catch                        
                        pdata(k).parabola  = [0, 0, 0, 0, 0];
                    end                    
                   %}
                    
                   %text(v, u, sprintf('[%d,%d]', v, u), 'Color', [0 1 1]);
                end
            end
            

            for k = 1:no_patches
                u = us(k);
                v = vs(k);
                
                patch_im = pdata(k).data;
                                patch = [patches(k).data{:}];
                patch_im = double(patch) / 255;
                
                if (patches(k).is_dot)
                    if (~isempty(MkEDetector.uid2idx(res, patches(k).uid)))
                        bim(u:u+wp-1, v:v+wp-1, 2) = patch_im;
                    else
                        bim(u:u+wp-1, v:v+wp-1, 3) = patch_im;
                    end
                else
                    bim(u:u+wp-1, v:v+wp-1, 1) = patch_im;
                end
            end
            
            
            imshow(bim);% / 255);
            hold on;
            
            %%%%%%
            rs = zeros(2, no_patches);
            
            if (p.Results.ShowUid)
                for i = 1:no_patches
                    pd = pdata(i);
                    cs = pd.center + wp / 2;
                    text(cs(1), cs(2), sprintf('%d,%d', patches(i).uid, i), 'Color', [1 1 1]);
                end
            end
            
            if (p.Results.ShowCentroid)              
               s = 0.2;
               
               for i = 1:no_patches
                  pd = pdata(i);
                  
                  cs = pd.center + wp / 2;                  
                  plot(cs(1), cs(2), '.c');
                  
                  
                  % Moments
                  
                  cs =  pd.center + pd.ms_centroid;                                    
                  plot(cs(1), cs(2), 'xy');
                  
                  %if (~isnan(c(3)))                  
                      %r = sqrt(abs(pd.ms_var(3) * 4));
                      r = real(pd.ms_var(3));
                    
                      if (r  == 0)
                          r = 0.01;
                      end
                      rectangle('Position', [cs(1) - r/2, cs(2) - r/2, r, r] ,'Curvature', [1 1], 'EdgeColor', [1 1 0]);
                  %end
                  
                  % Gaussian
                  
                  gs = pd.gaussian;
                  cs = pd.center + gs(4:5);
                  
                  if (gs(4))
                      plot(cs(1), cs(2), 'xc');
                  end
                  
                  %if (c(7) ~= 0 && ~isnan(c(1)))
                      r =  ((gs(1) + gs(2))/2) * gs(3);% * s;
                     
                      uid = patches(i).uid;
                      idx = MkEDetector.uid2idx(res, patches(i).uid);
                      
                      if (~isempty(idx))
                        rs(1, i) = abs(gs(1)); 
                        rs(2, i) = norm(res.lut3d(idx));
                        rs(3, i) = sqrt(abs(pd.ms_var(3)));
                      end
                                           
                      rectangle('Position', [cs(1) - r/2, cs(2) - r/2, r, r] ,'Curvature', [1 1], 'EdgeColor', [0 1 1]);                      
                  %end
                  
                  
                  % Parabola
                  
                  ps = pd.parabola;
                  cs = pd.center + ps(1:2);
                  if (gs(4))
                      plot(cs(1), cs(2), 'xm');
                  end                  
                  
               end               
            end
            
            figure; hold on;
            x= [] ; for i =1:numel(pdata) x= [x, real(pdata(i).ms_var(3))]; end
             hist(x,50)
             
            figure; hold on;
            x= [] ; for i =1:numel(pdata) x= [x, real(pdata(i).ms_us(3))]; end
             hist(x,50)      
             
            figure; hold on;
            x= [] ; for i =1:numel(pdata) gs = pdata(i).gaussian; r =  ((gs(1) + gs(2))/2); x= [x, r]; end
             hist(x,50)      
             
            figure; hold on;
            dd = [pdata(:).dd];
           plot(dd(4, :) ./ dd(5, :), 'd');
          
             
             
        end                
        
        % =================================================================
    end
    
end