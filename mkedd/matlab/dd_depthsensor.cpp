/* 
 * ddDepthSensor : Matlab interface to DepthSensor object
 * 
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#include <stdexcept>
#include <cstring>

#include <mex.h>

#include "json.hpp"

#include "mx/utils.h"
#include "mx/class.h"
#include "mx/json.h"

#include "mkedd.h"

using nlohmann::json;
using namespace mke::dd;

// ============================================================================

void ddDepthSensor(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  mx::no_params_geq(nrhs, 1, "function takes at least one parameter");
  
  std::string cmd = mx::mxarray_to_string(prhs[0]);
      
  // ==========================================================================
  if (cmd == "new")
    {
      DepthSensor *sensor = new DepthSensor();
      plhs[0] = mx::ptr_to_mat<DepthSensor>(sensor);
    }
  // ==========================================================================
  else if (cmd == "delete")
    {
      mx::no_params_eq(nrhs, 2, "mke::dd::DepthSensor('delete', objHandle): function takes two parameter");
      mx::destroy_object<DepthSensor>(prhs[1]);
    }
  // ==========================================================================
  else if (cmd == "getVersion")
    {
      mx::no_params_eq(nrhs, 2, "mke::dd::DepthSensor('getVersion', objHandle): function takes two parameter");
      
      DepthSensor *sensor = mx::mat_to_ptr<DepthSensor>(prhs[1]);
      
      plhs[0] = mxCreateNumericMatrix(1, 3, mxUINT32_CLASS, mxREAL);
      uint32_t *ver = (uint32_t *) mxGetPr(plhs[0]);
      sensor->getVersion(ver[0], ver[1], ver[2]);
    }
  // ==========================================================================
  // Profile ==================================================================
  else if (cmd == "getProfile")
    {
      mx::no_params_eq(nrhs, 2, "mke::dd::DepthSensor('getProfile', objHandle): function takes two parameter");
      DepthSensor *sensor = mx::mat_to_ptr<DepthSensor>(prhs[1]);
      std::string profile = sensor->getProfile();
      plhs[0] = mx::string_to_mxarray(profile);
    }
  // ==========================================================================
  else if (cmd == "getDefaultProfile")
    {
      mx::no_params_eq(nrhs, 2, "mke::dd::DepthSensor('getDefaultProfile', objHandle): function takes two parameter");
      DepthSensor *sensor = mx::mat_to_ptr<DepthSensor>(prhs[1]);
      std::string profile = sensor->getDefaultProfile();
      plhs[0] = mx::string_to_mxarray(profile);
    }
  // ==========================================================================
  else if (cmd == "validateProfile")
    {
      mx::no_params_eq(nrhs, 3, "mke::dd::DepthSensor('validateProfile', objHandle, profile): function takes three parameter");
      DepthSensor *sensor = mx::mat_to_ptr<DepthSensor>(prhs[1]);
      json profile = mx::mxarray_to_nljson(prhs[2]);
      sensor->validateProfile(profile.dump().c_str());
    }
  // ==========================================================================
  else if (cmd == "getCurrentProfileSchema")
    {
      mx::no_params_eq(nrhs, 2, "mke::dd::DepthSensor('getCurrentProfileSchema', objHandle): function takes two parameter");
      DepthSensor *sensor = mx::mat_to_ptr<DepthSensor>(prhs[1]);
      std::string profile = sensor->getCurrentProfileSchema();
      plhs[0] = mx::string_to_mxarray(profile);
    }
  // ==========================================================================
  else if (cmd == "setDefaultProfile")
    {
      mx::no_params_eq(nrhs, 2, "mke::dd::DepthSensor('setDefaultProfile', objHandle): function takes two parameter");
      DepthSensor *sensor = mx::mat_to_ptr<DepthSensor>(prhs[1]);
      sensor->setDefaultProfile();
    }
  // ==========================================================================
  else if (cmd == "setProfile")
    {
      mx::no_params_eq(nrhs, 3, "mke::dd::DepthSensor('getProfile', objHandle, profile): function takes three parameter");
      DepthSensor *sensor = mx::mat_to_ptr<DepthSensor>(prhs[1]);
      json profile = mx::mxarray_to_nljson(prhs[2]);
      sensor->setProfile(profile.dump().c_str());
    }
  // ==========================================================================
  // Stats ====================================================================
  else if (cmd == "resetStats")
    {
      mx::no_params_eq(nrhs, 2, "mke::dd::DepthSensor('resetStats', objHandle): function takes two parameter");
      DepthSensor *sensor = mx::mat_to_ptr<DepthSensor>(prhs[1]);
      sensor->resetStats();
    }
  // ==========================================================================
  else if (cmd == "getStats")
    {
      mx::no_params_eq(nrhs, 2, "mke::dd::DepthSensor('getStats', objHandle): function takes two parameter");
      DepthSensor *sensor = mx::mat_to_ptr<DepthSensor>(prhs[1]);
      plhs[0] = mx::string_to_mxarray(sensor->getStats());
    }
  // ==========================================================================
  // Calibration data files ===================================================
  else if (cmd == "addCalibFile")
    {
      mx::no_params_eq(nrhs, 3, "mke::dd::DepthSensor('addCalibFile', objHandle, fpath): function takes three parameter");
      DepthSensor *sensor = mx::mat_to_ptr<DepthSensor>(prhs[1]);
      std::string fpath = mx::mxarray_to_string(prhs[2]);
      sensor->addCalibFile(fpath.c_str());
    }
  // ==========================================================================
  else if (cmd == "clearCalibFiles")
    {
      mx::no_params_eq(nrhs, 2, "mke::dd::DepthSensor('clearCalibFiles', objHandle): function takes two parameter");
      DepthSensor *sensor = mx::mat_to_ptr<DepthSensor>(prhs[1]);
      sensor->clearCalibFiles();
    }
  // ==========================================================================
  // Processing ===============================================================
  else if (cmd == "process")
    {
      mx::no_params_eq(nrhs, 4, "mke::dd::DepthSensor('getDetections', objHandle, det_idx, data): function takes four parameter");
      DepthSensor *sensor = mx::mat_to_ptr<DepthSensor>(prhs[1]);
      uint32_t det_idx = uint32_t(*mxGetPr(prhs[2]));
      size_t m, n;
      uint8_t *data;
      mx::mxarray_to_data(prhs[3], data, m, n);          
      uint32_t no_dets = sensor->process(det_idx, data);
      plhs[0] = mxCreateNumericMatrix(1, 1, mxDOUBLE_CLASS, mxREAL);
      *mxGetPr(plhs[0]) = double(no_dets);          
    }
  // ==========================================================================
  // Reserved API =============================================================
  else if (cmd == "getResults")
    {
      mx::no_params_eq(nrhs, 3, "mke::dd::DepthSensor('getResults', objHandle, det_idx): function takes three parameter");
      DepthSensor *sensor = mx::mat_to_ptr<DepthSensor>(prhs[1]);
      uint32_t det_idx = uint32_t(*mxGetPr(prhs[2]));
      json results;
      sensor->getResults(det_idx, results);
      mx::nljson_to_mxarray(results, plhs[0]);
    }
  // ==========================================================================
  else if (cmd == "getDebugData")
    {
      mx::no_params_eq(nrhs, 3, "mke::dd::DepthSensor('getDebugData', objHandle, det_idx): function takes three parameter");
      DepthSensor *sensor = mx::mat_to_ptr<DepthSensor>(prhs[1]);
      uint32_t det_idx = uint32_t(*mxGetPr(prhs[2]));
      json dbgdata;
      sensor->getDebugData(det_idx, dbgdata);
      mx::nljson_to_mxarray(dbgdata, plhs[0]);
    }
  // ==========================================================================
  else if (cmd == "getTriangulation")
    {
      mx::no_params_eq(nrhs, 3, "mke::dd::DepthSensor('getTriangulation', objHandle, det_idx): function takes three parameter");
      DepthSensor *sensor = mx::mat_to_ptr<DepthSensor>(prhs[1]);
      uint32_t det_idx = uint32_t(*mxGetPr(prhs[2]));
      json dbgdata;
      sensor->getTriangulation(det_idx, dbgdata);
      mx::nljson_to_mxarray(dbgdata, plhs[0]);
    }
  // ==========================================================================
  else if (cmd == "parseCalibFile")
    {
      mx::no_params_eq(nrhs, 3, "mke::dd::DepthSensor('parseCalibFile', objHandle, file_name): function takes three parameter");
      DepthSensor *sensor = mx::mat_to_ptr<DepthSensor>(prhs[1]);
      std::string file_name = mx::mxarray_to_string(prhs[2]);
      json calibdata;
      sensor->parseCalibFile(file_name.c_str(), calibdata);
      mx::nljson_to_mxarray(calibdata, plhs[0]);
    }         
  // ==========================================================================
  // Info =====================================================================
  else if (cmd == "getMaxNoDetections")
    {
      mx::no_params_eq(nrhs, 2, "mke::dd::DepthSensor('getStats', objHandle): function takes two parameter");
      DepthSensor *sensor = mx::mat_to_ptr<DepthSensor>(prhs[1]);
      uint32_t max_no_dets = sensor->getMaxNoDetections();
      plhs[0] = mxCreateNumericMatrix(1, 1, mxDOUBLE_CLASS, mxREAL);
      *mxGetPr(plhs[0]) = double(max_no_dets);
    }
  // ==========================================================================
  else if (cmd == "getNoDetections")
    {
      mx::no_params_eq(nrhs, 3, "mke::dd::DepthSensor('getNoDetections', objHandle, det_idx): function takes three parameter");
      DepthSensor *sensor = mx::mat_to_ptr<DepthSensor>(prhs[1]);
      uint32_t det_idx = uint32_t(*mxGetPr(prhs[2]));
      uint32_t no_dets = sensor->getNoDetections(det_idx);
      plhs[0] = mxCreateNumericMatrix(1, 1, mxDOUBLE_CLASS, mxREAL);
      *mxGetPr(plhs[0]) = double(no_dets);          
    }
  // ==========================================================================
  else if (cmd == "getNoDetectors")
    {
      mx::no_params_eq(nrhs, 2, "mke::dd::DepthSensor('getNoDetectors', objHandle): function takes two parameter");
      DepthSensor *sensor = mx::mat_to_ptr<DepthSensor>(prhs[1]);
      uint32_t no_dds = sensor->getNoDetectors();
      plhs[0] = mxCreateNumericMatrix(1, 1, mxDOUBLE_CLASS, mxREAL);
      *mxGetPr(plhs[0]) = double(no_dds);
    }
  // ==========================================================================
  else if (cmd == "getLaserPattern")
    {
      mx::no_params_eq(nrhs, 3, "mke::dd::DepthSensor('getLaserPattern', objHandle, det_idx): function takes three parameter");
      DepthSensor *sensor = mx::mat_to_ptr<DepthSensor>(prhs[1]);
      uint32_t det_idx = uint32_t(*mxGetPr(prhs[2]));
      uint32_t lp = sensor->getLaserPattern(det_idx);
      plhs[0] = mxCreateNumericMatrix(1, 1, mxDOUBLE_CLASS, mxREAL);
      *mxGetPr(plhs[0]) = double(lp);          
    }
  // ==========================================================================
  else if (cmd == "getStride")
    {
      mx::no_params_eq(nrhs, 3, "mke::dd::DepthSensor('getStride', objHandle, det_idx): function takes three parameter");
      DepthSensor *sensor = mx::mat_to_ptr<DepthSensor>(prhs[1]);
      uint32_t det_idx = uint32_t(*mxGetPr(prhs[2]));
      uint32_t stride = sensor->getStride(det_idx);
      plhs[0] = mxCreateNumericMatrix(1, 1, mxDOUBLE_CLASS, mxREAL);
      *mxGetPr(plhs[0]) = double(stride);
    }
  // ==========================================================================
  else if (cmd == "getData3dType")
    {
      mx::no_params_eq(nrhs, 3, "mke::dd::DepthSensor('getData3dType', objHandle, det_idx): function takes three parameter");
      DepthSensor *sensor = mx::mat_to_ptr<DepthSensor>(prhs[1]);
      uint32_t det_idx = uint32_t(*mxGetPr(prhs[2]));
      uint32_t data3d_type = sensor->getData3dType(det_idx);
      plhs[0] = mxCreateNumericMatrix(1, 1, mxDOUBLE_CLASS, mxREAL);
      *mxGetPr(plhs[0]) = double(data3d_type);
    }  
  // ==========================================================================
  else
    {
     throw std::runtime_error(std::string("mke::dd::DepthSensor('cmd', ...) : unknown command '") + cmd + "'");
    }
}    
