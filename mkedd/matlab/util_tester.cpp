/* 
 * utilTester : Matlab interface to the Tester object
 * 
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#include <stdexcept>
#include <cstring>
#include <iostream>

#include <mex.h>

#include "json.hpp"
#include "util/tester.h"

#include "mx/utils.h"
#include "mx/class.h"
#include "mx/json.h"

using nlohmann::json;
using namespace mke::dd::util;
using namespace mke::dd;

// ============================================================================

void utilTester(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  mx::no_params_geq(nrhs, 1, "function takes at least one parameter");
  
  std::string cmd = mx::mxarray_to_string(prhs[0]);
  
  // ==========================================================================
  if (cmd == "new")
    {
      Tester *tester = new Tester();
      plhs[0] = mx::ptr_to_mat<Tester>(tester);
    }
  // ============================================================================
  else if (cmd == "delete")
    {
      mx::no_params_eq(nrhs, 2, "mke::dd::util::Tester: function 'delete' takes two parameter");
      mx::destroy_object<Tester>(prhs[1]);
    }
  // ============================================================================
  // ============================================================================
  else if (cmd == "process")
    {
      mx::no_params_eq(nrhs, 3, "mke::dd::util::Tester: function 'process' takes three parameter");

      json params, results;

      Tester *tester = mx::mat_to_ptr<Tester>(prhs[1]);
      mx::mxarray_to_nljson(prhs[2], params);
      tester->process(params, results);
      mx::nljson_to_mxarray(results, plhs[0]);
    }
  // ============================================================================
  else if (cmd == "processFromFile")
    {
      mx::no_params_eq(nrhs, 3, "mke::dd::util::Tester: function 'processFromFile' takes three parameter");

      json tests_json;
      auto &params = tests_json["params"];
      auto &results = tests_json["results"];

      Tester *tester = mx::mat_to_ptr<Tester>(prhs[1]);
      std::string fpath = mke::dd::mx::mxarray_to_string(prhs[2]);
      tester->processFromFile(fpath, params, results);
      mx::nljson_to_mxarray(tests_json, plhs[0]);
    }
  // ============================================================================
  else if (cmd == "loadFromFile")
    {
      mx::no_params_eq(nrhs, 3, "mke::dd::util::Tester: function 'loadFromFile' takes three parameter");

      json tests_json;
      auto &params = tests_json["params"];
      auto &results = tests_json["results"];
      auto &info = tests_json["info"];

      Tester *tester = mx::mat_to_ptr<Tester>(prhs[1]);
      std::string fpath = mke::dd::mx::mxarray_to_string(prhs[2]);
      tester->loadFromFile(fpath, params, results, info);
      mx::nljson_to_mxarray(tests_json, plhs[0]);
    }
  // ============================================================================
  else if (cmd == "saveToFile")
    {
      mx::no_params_eq(nrhs, 4, "mke::dd::util::Tester: function 'saveToFile' takes four parameter");

      json tests_json;

      Tester *tester = mx::mat_to_ptr<Tester>(prhs[1]);
      std::string fpath = mke::dd::mx::mxarray_to_string(prhs[2]);
      mx::mxarray_to_nljson(prhs[3], tests_json);

      auto &params = tests_json["params"];
      auto &results = tests_json["results"];
      auto &info = tests_json["info"];

      tester->saveToFile(fpath, params, results, info);
    }
  // ============================================================================
  else
    {
     throw std::runtime_error(
       std::string("mkeddmm('mke::dd::util::Tester', 'cmd', ...) : unknown command '")
       + cmd + "'");
    }
}    
