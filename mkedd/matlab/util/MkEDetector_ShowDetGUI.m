%
% show detections computed by mkedd
%
% code is based on J.Heller's show-detection
% this is second version that inherits from MkEDetector
%
% -------------------------------------------------------------------------
%  Usage:
%
%    - left click to show detection
%    - right click to show path details
%
%
%  Example:
%  
%         mkedd = MkEDetector_ShowDetGUI;
%         mkedd.setMkEDetFile(mkedet);
%         mkedd.execute(im, 1);
%         mkedd.ShowGUI();
% 
%
%  Examples of matlab's addpaths:
%         addpath mkert/mkedd/matlab/
%         addpath mkert/mkedd/matlab/util
%         addpath mkert/mkedd/build/matlab/
%
%
%
%
%
% Jenda 2019
%
%
%
classdef MkEDetector_ShowDetGUI < MkEDetector
    
    properties
   
        
        hfig_main;                   %%% handler to main fig for clicking
        hfig_second;                 %%% fig with results, it is fig-id as it is created in static method 
        
        h_class_boxes;
        h_selected_dot;
        
    end
    
    
    
    %%
    methods
        
        %------------------------------------------------------------------
        function obj = MkEDetector_ShowDetGUI(varargin)
            iargs = inputParser;
            iargs.addOptional('hfig',35202);
            iargs.parse(varargin{:}); iargs = iargs.Results;
            
            obj@MkEDetector(); 
            
            obj.hfig_main   = iargs.hfig;
            obj.hfig_second = iargs.hfig+1;
        end
        
               
        
        %------------------------------------------------------------------
        function ShowGUI(obj)
        
            %-- show figure
            figure(obj.hfig_main);           
            
            %--- set figures postion and color and callback for clickinbg
            screenRes       = get(groot,'screensize');
            windowPosition  = [0 screenRes(4)/2-70 screenRes(3)/2 screenRes(4)/2];
            
            set(obj.hfig_main,...
                'WindowButtonDownFcn',  @(hObject, event)MkEDetector_ShowDetGUI.CallbacMouseClick(hObject, event, obj),...
                'KeyPressFcn', @(hObject, event)MkEDetector_ShowDetGUI.CallbacKeyPressed(hObject, event, obj),...
                'Color', [1 1 1]*.3,...
                'Position', windowPosition);
            
            %-- show image and set axes
            ax = gca();
            imshow(obj.im_data,'Parent',ax);
            set(gca,'position',[0 0 1 1],'units','normalized');
            axis off; grid off;
            
            %--- show detection lines
            obj.Show_detections_and_lines();
            
            fprintf(['  click on detections for details\n',...
                     '  press [q] yto quit\n']);
            
        end
        %------------------------------------------------------------------
        
        
      
        
        
        %------------------------------------------------------------------
        function Show_detections_and_lines(obj)
            
            hold on;
            det_idx = 1;
            cd = obj.calibdata{det_idx}';
            
            pths = [];
            cols = [];
            
            % paths
            for i = 1:length(cd.ps.paths)
                if isempty(cd.ps.paths{i})
                    continue;
                end
                paths = cd.ps.paths{i}(1:2, :) + 1;
                
                if (isempty(paths))
                    continue;
                end
                
                afs = cd.ps.aflags{i};
                
                pths = [pths , paths(1:2,:)]; % store and later scatter
                cols = [cols , afs];
               
                text(paths(1, 1) + 2, paths(2, 1) + 2, sprintf('%d', i), 'Color', [0, 1, 1]);
            end
            
            cmap = [0 1 1;0 0 1;1 0 1];
            scatter(pths(1,:),pths(2,:),pths(1,:)*0+2,cmap(:,cols+1)','filled'); hold on;
            
            % detections
            if (isfield(obj.res, 'lut2d') && ~isempty(obj.res.lut2d))
                plot(obj.res.lut2d(:, 1) + 1, obj.res.lut2d(:, 2) + 1, 'dr','LineWidth',2);
                plot(obj.res.lut2d(:, 1) + 1, obj.res.lut2d(:, 2) + 1, '.r');
            end
            
        end
        %------------------------------------------------------------------
        
        %------------------------------------------------------------------
        function I = GetRGBImage(obj)
            I(:,:,1) = obj.im_data;
            I(:,:,2) = obj.im_data;
            I(:,:,3) = obj.im_data;
        end
        %------------------------------------------------------------------
                

        %------------------------------------------------------------------
        function I = GetImagePatch(obj,xy,scale,im) %% TODO!! change to x y to use for path clicking!!!
            if ~exist('im','var') || isempty(im)
                im = obj.im_data;
            end
            I = im( xy(2)-scale:xy(2)+scale ,...
                    xy(1)-scale:xy(1)+scale  , : );
        end
        %------------------------------------------------------------------
        
        %------------------------------------------------------------------
        function paths = Get_paths_moved2center(obj,lut_id,patch_center,data_move2path,scale)
            det_idx = 1;
            paths_raw  = obj.calibdata{det_idx}.ps.paths{lut_id};
            paths      = bsxfun(@plus , paths_raw , -patch_center+data_move2path); %%% enter it to the imge path
            paths      = paths([2 1],:);
            paths      = paths + scale; %%% we will plot in detection center coor
        end
        %------------------------------------------------------------------
        
        
        %         %------------------------------------------------------------------
        %         % if even odd pathid starts from i.e. 90.. -> convert it back to pathid start  from 0
        %         function pathid = ConvertLutId2Path(obj,lutid)
        %             pathid = lutid;
        %             if lutid>obj.other_calibpar_nopaths %%% threre is need for conversion
        %                 pathid = lutid - obj.other_calibpar_nopaths;
        %             end
        %         end
        %         %------------------------------------------------------------------
        
        
        %------------------------------------------------------------------
        function Show_detections_graphs(obj,uid)
            
            cd = obj.calibdata{obj.det_idx};
            path_idx = find(cd.bb.seg2uid == uid, 1);
            res_idx = find(obj.res.uid == uid, 1);
            
            
            if (isempty(path_idx))
                error('Cannot show: cannot find path ID %d', i);
            end
            
            % Accumulator
            if (isfield(obj.dbgdata, 'accums'))
                title(sprintf('Accumulator id. %d', i));
                
                %path_idx = find(obj.res.idx == acc_id, 1);
                accum = obj.dbgdata.accums{path_idx};
                af = logical(cd.ps.aflags{path_idx});
                no_els = numel(accum);
                xs = 1:no_els;
                
                
                if (~no_els)
                    warning('Nothing to show, empty accumulator');
                end
                
                plot(xs, accum, '-', 'Color', [0.3 0.3 0.3]); hold on;
                plot(xs(af), accum(af), '.b');
                plot(xs(~af), accum(~af), '.r');
                
                if (isempty(res_idx))
                    return;
                end
                
                %x = obj.res.accels(res_idx) - cd.accums.offs(path_idx) + 2;
                x = obj.dbgdata.det_idx(path_idx) + 1;
                plot(x, accum(x), 'xr');
                text(double(x) + 3, double(accum(x)), sprintf('x: %d, a: %.3f\nr: %.3f', x, accum(x), obj.res.resp(res_idx)));
            end            
        end
        %------------------------------------------------------------------
        
        
        %------------------------------------------------------------------
        function Plot_DDBox(obj,accum)
            h = subplot(3,2,[5 6]);
            try delete(get(h,'Child')); catch, end; %%% if some graph was plotted, delete it
            try
                [~,~,cdata] = ddbox(accum);
                mesh(cdata);
                axis off tight;
                colormap jet;
            catch
                warning('ddbox call failed');
                sphere(10);
            end
        end
        %------------------------------------------------------------------
        
    end
    
    %% ====================================================================
    methods (Static)
        
        
        %------------------------------------------------------------------
        function CallbacMouseClick(hObject, event, obj)
            
            det_idx = 1;
            
            %--- get coordinates of the click
            cursorPoint = get(hObject.Children,'CurrentPoint');
            curX = cursorPoint(1,1);
            curY = cursorPoint(1,2);
            
            hf = figure(obj.hfig_second);
            set(hf,...
                'KeyPressFcn', @(hObject, event)SShow_detectionsGUI.CallbacKeyPressed(hObject, event, obj),...
                'Color', [1 1 1]);
            
            scale = 50;
            
            
            %--- knn-close points
            click_pos = round([curX curY])';
            path_centers = cellfun(@(x) mean(x')', obj.calibdata{det_idx}.ps.paths,'UniformOutput',0);
            path_centers = [path_centers{:}];
            path_knns = [];
            if exist('knnsearch')==2
                path_knns = knnsearch(path_centers',click_pos','K',10);
            else
                warning('knnsearch does not exists, no circ plotting');
            end
            
            try delete(obj.h_selected_dot); catch, end
            try delete(obj.h_class_boxes); catch, end
            
            %--- now show detection point or line without detection
            %    based on button clicked
            switch get(gcbf, 'SelectionType')
                %-------------------------------------------
                case 'normal' %%% left mosue click button (select point)
                %-------------------------------------------
                    
                    %--- get indexes of click
                    lut2d_for_search = [ obj.res.lut2d(:,1)' ; obj.res.lut2d(:,2)' ];
                    [~,idx_in_res] = min(sum(bsxfun( @plus , lut2d_for_search , -[curX;curY] ).^2));
                    uid          = obj.res.uid(idx_in_res); %uid = obj.ConvertLutId2Path(lut_id);
                    path_id      = find(obj.calibdata{det_idx}.bb.seg2uid==uid)
                    patch_center = obj.res.lut2d(idx_in_res,:)';
                    
                    %-- plot in main image
                    figure(obj.hfig_main);
                    obj.h_selected_dot = plot(patch_center(1),patch_center(2),'og','LineWidth',3);
                    figure(obj.hfig_second);

                    
                    if 0  %%% debug search
                        plot(lut2d_for_search(1,:),lut2d_for_search(2,:),'.k'); hold on;
                        plot(lut2d_for_search(1,idx_in_res),lut2d_for_search(2,idx_in_res),'oy','LineWidth',2);
                        plot(curX,curY,'xg','LineWidth',2);
                        axis equal
                    end
                    
                    %--- plot image part/patch
                    h = subplot(3,2,1);  try delete(get(h,'Child')); catch, end;  %%% delete previous results
                    
                    data_move2path = + (patch_center - round(patch_center)) +2; %%% this is T to the image patch (1 for c++ fporamring and 1 for getimage -> 1+1=2)
                    patch_center = obj.res.lut2d(idx_in_res,:)';
                    imshow( obj.GetImagePatch( round(patch_center) , scale ) ); hold on
                    plot(scale+data_move2path(1),scale+data_move2path(2),'xr','LineWidth',2); hold on;
                    
                    if 0 % debug plot lutid and path
                        fprintf('uid=%i\n',uid);
                        figure(obj.hfig_main);
                        plot(patch_center(1),patch_center(2),'xg','LineWidth',5);
                        paths_raw  = obj.calibdata{det_idx}.ps.paths{path_id};
                        plot(paths_raw(1,:),paths_raw(2,:),'oy','LineWidth',5);
                        return;
                    end
                    
                    %--- plot paths in the image patch
                    for id=[path_id path_knns]
                        paths = obj.Get_paths_moved2center(id,patch_center,data_move2path,scale);
                        if ~(isempty(paths))
                            afs = obj.calibdata{det_idx}.ps.aflags{id};
                            ai0 = afs == 0;
                            ai1 = afs == 1;
                            ai2 = afs == 2;
                            plot(paths(2, ai1), paths(1, ai1), '.b'); hold on;
                            plot(paths(2, ai0), paths(1, ai0), '.c');
                            plot(paths(2, ai2), paths(1, ai2), '.m');
                        end
                    end
                    
                    
                    %--- plot image part and graph with color to the detection rate
                    h = subplot(3,2,2);
                    try delete(get(h,'Child')); catch, end;  %%% delete previous results
                    imshow( obj.GetImagePatch( round(patch_center) , scale , obj.GetRGBImage() ) ); hold on;
                    paths = obj.Get_paths_moved2center(path_id,patch_center,data_move2path,scale);
                    if ~(isempty(paths))
                        pts_size = 8;
                        accum = obj.dbgdata.accums{path_id};
                        scatter(paths(2,:),paths(1,:),accum*0+pts_size,accum,'filled');
                        colormap(h,'jet');
                        caxis([0 256]);
                    end

                    
                    %--- get 3d points to see distances
                    [~,idx_in_paths] = min(sum(bsxfun( @plus , obj.calibdata{det_idx}.ps.paths{path_id} , -obj.res.lut2d(idx_in_res,:)' ).^2));
                    paths3d_raw  = obj.calibdata{det_idx}.ps.paths3D{path_id};
                    if idx_in_paths>1 && idx_in_paths<length(paths3d_raw)-1
                        p_center = paths3d_raw(:,idx_in_paths);
                        p_prev = paths3d_raw(:,idx_in_paths-1);
                        p_next = paths3d_raw(:,idx_in_paths+1);
                        dist2prev = sqrt(sum((p_center-p_prev).^2));
                        dist2next = sqrt(sum((p_center-p_prev).^2));
                        text(105,0,sprintf('d=%.3f [m]',sqrt(sum(p_center.^2))/1000 ));
                        text(105,15,'  dist to');
                        text(105,25,'  next/prev');
                        text(105,35,sprintf('%.3f [m]' ,dist2prev/1000));
                        text(105,45,sprintf('%.3f [m]' ,dist2next/1000));
                    end
                    
                    %--- plot graph with detection score as colors
                    h = subplot(3,2,[3 4]);
                    try delete(get(h,'Child')); catch, end; %%% if some graph was plotted, delete it
                    obj.Show_detections_graphs(uid);
                    %obj.showAccum(uid)
                    
                    %--- plot ddbox
                    % obj.Plot_DDBox(accum);
                    
                    
                    
                %-------------------------------------------
                case 'alt' %%%% right button show curve (where can be no detection)
                %-------------------------------------------
                
                    %-- find min path id
                    min_path_id = [];
                    min_path_dist = inf;
                    for path_id = 1:length(obj.calibdata{det_idx}.ps.paths)
                        if isempty(obj.calibdata{det_idx}.ps.paths{path_id})
                            continue
                        end
                        [path_dist,~] = min(sum(bsxfun( @plus , obj.calibdata{det_idx}.ps.paths{path_id}(1:2,:) , -click_pos ).^2));
                        if path_dist < min_path_dist
                            min_path_dist = path_dist;
                            min_path_id   = path_id;
                        end
                    end
                        
                    %--- clear figs
                    h = subplot(3,2,[3 4]);  try delete(get(h,'Child')); catch, end; %%% if some graph was plotted, delete it

                    title(sprintf('Accumulator dump no. %d', min_path_id));
                    
                    %-- plot graph
                    accum = obj.dbgdata.accums{min_path_id};
                    af = logical(obj.calibdata{det_idx}.ps.aflags{min_path_id});
                    no_els = numel(accum);
                    xs = 1:no_els;                    
                    
                    if (~no_els)
                        warning('Nothing to show, empty accumulator');
                    end
                    
                    plot(xs, accum, '-', 'Color', [0.3 0.3 0.3]); hold on;
                    plot(xs(af), accum(af), '.b');
                    plot(xs(~af), accum(~af), '.r');
                    
                    %-- preapre for patch plotting
                    data_move2path = +2; %%% this is T to the image patch (1 for c++ fporamring and 1 for getimage -> 1+1=2)
                    impatch = obj.GetImagePatch( click_pos , scale , obj.GetRGBImage() );
                    
                    %-- plot im-patches and close points!
                    h = subplot(3,2,1); try delete(get(h,'Child')); catch, end  %%% delete previous results
                    imshow( impatch ); hold on;
                    for id=path_knns
                        paths = obj.Get_paths_moved2center(id,click_pos,data_move2path,scale);
                        if ~(isempty(paths))
                            afs = obj.calibdata{det_idx}.ps.aflags{id};
                            ai0 = afs == 0;
                            ai1 = afs == 1;
                            ai2 = afs == 2;
                            plot(paths(2, ai1), paths(1, ai1), '.b'); hold on;
                            plot(paths(2, ai0), paths(1, ai0), '.c');
                            plot(paths(2, ai2), paths(1, ai2), '.m');
                        end
                    end
                    
                    
                    %-- plot selected path in the main figure
                    figure(obj.hfig_main);
                    path = obj.Get_paths_moved2center(min_path_id,[0,0]',0,0);
                    obj.h_selected_dot = plot(path(2,1:3:end)+1,path(1,1:3:end)+1,'oy');
                    
                    %-- plot classifier patches and results
                    
                    min_path_uid  = obj.calibdata{det_idx}.bb.seg2uid(min_path_id);           % get uid of the path
                    idx_class_use = find(min_path_uid==[obj.dbgdata.classifier.patches.uid]); % find these uids in the classifier data
                    
                    
                    
                    %path_idx = find(obj.calibdata{det_idx}.bb.seg2uid == uid, 1);

                    for id_clsptch = idx_class_use%1:length(obj.dbgdata.classifier.patches)
                        cp = obj.dbgdata.classifier.patches(id_clsptch); % classifier patch
                        col = 'r';
                        if cp.is_dot
                            col = 'g';
                        end
                        hf = rectangle('Position',[cp.cx-cp.width/2+1 cp.cy-cp.width/2+1 cp.width cp.width],...
                            'EdgeColor',col,'LineWidth',2);
                        obj.h_class_boxes = [obj.h_class_boxes hf];
                    end
                    figure(obj.hfig_second);
                    
                    
                    
                    %--- plot paths in the image patch
                    h = subplot(3,2,2); try delete(get(h,'Child')); end
                    imshow( impatch ); hold on;
                    paths = obj.Get_paths_moved2center(min_path_id,click_pos,data_move2path,scale);
                    if ~(isempty(paths))
                        pts_size = 3;
                        scatter(paths(2,:),paths(1,:),accum*0+pts_size,accum,'filled');
                        colormap(h,'jet');
                        caxis([0 256]);
                    end
                    
                    %--- plot ddbox
%                     obj.Plot_DDBox(accum
            end
            
        end
        %------------------------------------------------------------------
        
        %------------------------------------------------------------------
        function CallbacKeyPressed(hObject, event, obj)
            key = get(hObject, 'CurrentKey');
            if strcmp(key,'q')
                try
                    delete(obj.hfig_second);
                catch, end;
                try
                    delete(obj.hfig_main);
                catch, end;
                disp(' Show detections: figs deleted, bye');
            end
        end
%         %------------------------------------------------------------------
%         function [cols, rows] = get_windows_positions(no_figs)
%             screen = get(0, 'ScreenSize');
%             rows = 1;
%             while(1 == 1)
%                 h = floor(screen(4) / rows);
%                 cols = floor(screen(3) / h);
%                 if (rows * cols >= no_figs)
%                     break;
%                 end
%                 rows = rows + 1;
%             end
%         end
%         %------------------------------------------------------------------
        
%         %------------------------------------------------------------------
%         function subfig(rows, cols, ord, f)
%             twidth = 20;
%             screen = get(0, 'ScreenSize');
%             w = screen(3);
%             h = screen(4);
%             fw = w / cols;
%             fh = h / rows - twidth;
%             i = ceil(ord / cols);
%             j = rem(ord - 1, cols);
%             left = j * fw + screen(1);
%             bottom = h - i * fh + screen(2);
%             
%             if (nargin < 4)
%                 f = figure('Visible','off');
%             end
%             
%             set(f, 'OuterPosition', [left bottom fw fh]);
%             set(f, 'Visible', 'on');
%         end
%         %------------------------------------------------------------------

        
        %------------------------------------------------------------------
        function accum = get_accum(aidx, cidx, res, csidx)
            idx = (csidx(aidx)+1):csidx(aidx + 1);
            accum = res.accdump(idx, cidx);
        end
        %------------------------------------------------------------------
        
        %------------------------------------------------------------------
        function dets = get_detections(aidx, res, csidx)
            idx = (res.dets(:, 1) >= (csidx(aidx)+1)) & (res.dets(:, 1) <= csidx(aidx + 1));
            dets = res.dets(idx, :);
        end
        %------------------------------------------------------------------
        
    end
end

