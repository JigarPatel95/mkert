classdef SubFigure < handle
        
    properties (SetAccess = private, Hidden = true)
        cols;
        rows;
        figs;
        no_figs;
        idx;
    end
    
    % =====================================================================        
    % =====================================================================      
    
    methods (Access = public)
        function obj = SubFigure(no_figs)
            if (nargin < 1)
                no_figs = 6;
            end
            
            screen = get(0, 'ScreenSize');
            obj.rows = 1;
            obj.idx = 1;
            obj.no_figs = no_figs;
            
            while(1 == 1)
                h = floor(screen(4) / obj.rows);
                obj.cols = floor(screen(3) / h);
                
                if (obj.rows * obj.cols >= obj.no_figs)
                    break;
                end
                
                obj.rows = obj.rows + 1;
            end
            
        end
        
        function new(obj, f)
            twidth = 20;
            screen = get(0, 'ScreenSize');
            w = screen(3);
            h = screen(4);
            fw = w / obj.cols;
            fh = h / obj.rows - twidth;
            i = ceil(obj.idx / obj.cols);
            j = rem(obj.idx - 1, obj.cols);
            left = j * fw + screen(1);
            bottom = h - i * fh + screen(2);
            
            if (nargin < 2)
                f = figure('Visible','off');
            end
            
            if (numel(f) ~= 1)
                im = f;
                f = figure('Visible','off');
                w = warning('off', 'all');
                imshow(im);
                warning(w);
            end
            
            set(f, 'OuterPosition', [left bottom fw fh]);
            set(f, 'Visible', 'on');
            hold on;
    
            obj.idx = obj.idx + 1;    
        end
    end
    
end