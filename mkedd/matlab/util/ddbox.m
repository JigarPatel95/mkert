function [peaks, tpeaks, cdata] = ddbox(sig, opts)

% Process inputs
defaultoptions=struct('thresh',0.0002, 'verbose',false, 'noscales', 10);
if(~exist('opts','var')), 
    opts = defaultoptions; 
else
    tags = fieldnames(defaultoptions);
    for i = 1:length(tags)
         if(~isfield(opts,tags{i})),  opts.(tags{i})=defaultoptions.(tags{i}); end
    end
    if(length(tags) ~= length(fieldnames(opts))), 
        warning('register_volumes:unknownoption','unknown options found');
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


sig = double(sig);
%sig = sig / max(sig);

no_octaves = 1;
if (opts.noscales <= 10)
    no_scales = opts.noscales;
else
    no_scales = 11;
end

%filter_sizes = [9, 15, 21, 27; 15, 27, 39, 51; 27, 51, 75, 99; 51, 99, 147, 195];
filter_sizes = [9, 15, 21, 27, 39, 51, 75, 99, 147, 195];
sigma_sizes = filter_sizes * 4/30;

for octave = 1:no_octaves
    for scale = 1:no_scales
        flt = ones(1, filter_sizes(octave, scale) / 3);
        filters{octave, scale} = [-1 * flt, 2 * flt, -1 * flt];
    end
end

resp = cell(4);
tpeaks = [];
peaks = [];

for octave = 1:no_octaves
    resp{octave} = zeros(numel(sig), no_scales);
    for scale = 1:no_scales
        resp{octave}(:, scale) = conv(sig, filters{octave, scale}, 'same') / filter_sizes(octave, scale);
    end    
 %   resp{octave}(:, scale + 1) = resp{octave}(:, scale - 1);
    lmax = nms3x3_mex(resp{octave});
   % lmax2 = nonmaxsupp3x3(resp{octave});
    
    [i, j] = ind2sub(size(resp{octave}), lmax);
    tpeaks = [tpeaks, [i; j; filter_sizes(j); resp{octave}(lmax)]];
    
    for i = 1:size(tpeaks, 2)
        if (tpeaks(4, i) < opts.thresh)
            continue;
        end
        
        x = tpeaks(1, i);
        s = tpeaks(2, i);
        r = resp{octave}(x, s);
        sd1 = 1;% (sigma_sizes(s) - sigma_sizes(s - 1));
        sd2 = 1;%(sigma_sizes(s + 1) - sigma_sizes(s));
        x1 = resp{octave}(x - 1, s);
        x2 = resp{octave}(x + 1, s);
        s1 = resp{octave}(x, s - 1) / sd1;
        s2 = resp{octave}(x, s + 1) / sd2;
        f22 = resp{octave}(x + 1, s + 1) / sd2;
        f12 = resp{octave}(x - 1, s + 1) / sd2;
        f11 = resp{octave}(x - 1, s - 1) / sd1;
        f21 = resp{octave}(x + 1, s - 1) / sd1;
        
        
        dx = (x2 - x1) / 2;
        ds = (s2 - s1) / 2;
        dxx = x2 + x1 - 2 * r;
        dss = s2 + s1 - 2 * r;
        dxs = (f22 - f12 - f21 + f11) / 4;
                
        %um = - [dxx, dxs; dxs, dss] \ [dx; ds];
        
        c = 1/(dxx*dss-dxs*dxs);
        um2(1) =  (ds*dxs-dx*dss) * c;
        um2(2) =  (dxs*dx-dxx*ds) * c;
               
        %um(1) = -(dx*dss-ds*dxs)/(dxx*dss-dxs*dxs);
        %um(2) = -(dxs*dx-dxx*ds)/(dxs*dxs-dxx*dss);
        peaks = [peaks, [[tpeaks(1, i); filter_sizes(s)] + um2'; tpeaks(4, i)]];
    end    
end

cdata = resp{1};

if (opts.verbose)
    figure('Position',[1920 1200 1700 1000]);
    imagesc(resp{1}');
    figure('Position',[1920 1200 1700 1000]);
    w = size(resp{1}, 1);
    h = size(resp{1}, 2);
    [X,Y] = meshgrid(1:w, 1:h);
    mesh(X,Y,resp{1}');
end




