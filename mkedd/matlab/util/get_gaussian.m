function gs = get_gaussian(x0, sig, w, h)

c = (w - 1) / 2 + 1;

xs = [1:w] - c;

ys = 1/(sig * sqrt(2*pi)) * exp(-1/2 * (xs / sig).^2);

xs = xs + x0;

if (nargin > 3)
 ys = ys / max(ys) * h;
end

gs = [xs;ys];
