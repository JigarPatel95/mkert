%
% show detections computed by mkedd
%
% code is based on J.Heller's show-detection
%
% example:
%  >> SShow_detectionsGUI(res,calibpar.calibdata,data.im); 
%  
% Jenda 2017
%
%
%
classdef SShow_detectionsGUI
    
    properties
        
        res;
        im;
        calibdata;
        other_calibpar_nopaths;
        
        no_paths;
        csnaccels;
        
        hfig_main;                   %%% handler to main fig for clicking
        hfig_second;   %%% fig with results, it is fig-id as it is created in static method 
        
    end
    
    
    
    %%
    methods
        
        %------------------------------------------------------------------
        function obj = SShow_detectionsGUI(res_,calib_data_,im_,varargin)
            iargs = inputParser;
            iargs.addOptional('hfig',35202);
            iargs.addOptional('other_calibpar_nopaths',-1);
            iargs.parse(varargin{:}); iargs = iargs.Results;
            
            
            %--- set properties
            obj.res       = res_;
            obj.calibdata = calib_data_;
            obj.im        = im_;
            
            obj.hfig_main   = iargs.hfig;
            obj.hfig_second = iargs.hfig+1;
            
            % this is because some calib data indexe's started from the
            %    length of otehr (odd/even) calib
            obj.other_calibpar_nopaths = iargs.other_calibpar_nopaths;

            %--- generate some
            obj.no_paths  = numel(obj.calibdata.ps.paths3D);
            obj.csnaccels = [0, cumsum(obj.calibdata.ps.naccels)];
            
            %-- show figure
            figure(obj.hfig_main);           
            
            %--- set figures postion and color and callback for clickinbg
            screenRes           = get(groot,'screensize');
            windowPosition  = [0 screenRes(4)/2-70 screenRes(3)/2 screenRes(4)/2];
            
            set(obj.hfig_main,...
                'WindowButtonDownFcn',  @(hObject, event)SShow_detectionsGUI.CallbacMouseClick(hObject, event, obj),...
                'KeyPressFcn', @(hObject, event)SShow_detectionsGUI.CallbacKeyPressed(hObject, event, obj),...
                'Color', [1 1 1]*.3,...
                'Position', windowPosition);
            
            %-- show image and set axes
            imshow(obj.im);
            set(gca,'position',[0 0 1 1],'units','normalized');
            axis off; grid off;
            
            %--- show detection lines
            obj.Show_detections_and_lines();
            
            fprintf(['  click on detections for details\n',...
                     '  press [q] yto quit\n']);
            
        end
        %------------------------------------------------------------------
        
        
        %------------------------------------------------------------------
        function Show_detections_graphs(obj,pidx)
            
%             no_paths = numel(obj.calibdata.ps.paths3D);
%             csnaccels = [0, cumsum(obj.calibdata.ps.naccels)];
            
            if exist('pidx','var') && (~isempty(pidx))
                % Show path detections
               % [cols, rows] = obj.get_windows_positions(numel(pidx));
                fgs = 1;
                
                for i = pidx
                    if (i < 1 || i > obj.no_paths)
                        error('Path index [%d] out of bounds [1,%d]', i, obj.no_paths);
                    end
                    
                    %obj.subfig(rows, cols, fgs);
                     hold on;
                     fgs = fgs + 1;
                    
                    % Accumulator
                    accum = obj.get_accum(i, 2, obj.res, obj.csnaccels);
                    xs = 1:numel(accum);
                    plot(xs, accum, '-b');
                    
                    % Active flags
                    afs = obj.calibdata.ps.aflags{i};
                    zs = zeros(size(xs));
                    
                    plot(xs(afs == 0), zs(afs == 0), 'xr');
                    plot(xs(afs == 2), zs(afs == 2), 'xm');
                    
                    % Threshold
                    plot([xs(1), xs(end)], [obj.res.profile.detector.threshold, obj.res.profile.detector.threshold], ':k');
                    
                    % Detection as Gaussians
                    % Detection as Gaussians
                    idx = find(obj.res.accums == (i - 1));
                    %dets = get_detections(i, res, csnaccels);
                    for j = idx
                        x = obj.res.accels(j) - obj.csnaccels(i) + 1;
                        y = accum(x);
                        s = obj.res.sigma2d(j);
                        r = obj.res.resp(j); %dets(j, 4);
                        gs = get_gaussian(x, s, 100, y);
                        gsr = get_gaussian(x, s, 100, r);
                        
                        plot(gs(1, :), gs(2, :), '-.r');
                        text(x + 2, y + 10, sprintf('x=%.2f, y=%.2f\n s=%.2f, w=%.2f', x, y, s, 0));
                        
                        plot(gsr(1, :), gsr(2, :), '-.c');
                        text(x + 2, r + 10, sprintf('x=%.2f, r=%.2f', x, r));
                    end
                    
                    title(sprintf('Accumulator dump no. %d', i));
                end
            end
        end
        %------------------------------------------------------------------
        
        
        %------------------------------------------------------------------
        function Show_detections_and_lines(obj)
            
            %[cols, rows] = obj.get_windows_positions(2);
            
            %             f = figure('Visible','off');
            %             w = warning('off', 'all');
            %             if (isfield(obj.res, 'image'))
            %                 im = obj.res.image;
            %             else
            %                 im = imread(obj.res.image_path);
            %             end
            %             imshow(im);
            %             warning(w);
            figure(obj.hfig_main);
            hold on;
            
            for i = 1:obj.no_paths
                if isempty(obj.calibdata.ps.paths{i})
                    continue;
                end
                paths = obj.calibdata.ps.paths{i}(1:2, :) + 1;
                
                if (isempty(paths))
                    continue;
                end
                
                afs = obj.calibdata.ps.aflags{i};
                
                ai0 = afs == 0;
                ai1 = afs == 1;
                ai2 = afs == 2;
                
                plot(paths(1, ai1), paths(2, ai1), '.b');
                plot(paths(1, ai0), paths(2, ai0), '.c');
                plot(paths(1, ai2), paths(2, ai2), '.m');
                
                text(paths(1, 1) + 2, paths(2, 1) + 2, sprintf('%d', i), 'Color', [0, 1, 1]);
            end
            
            if (isfield(obj.res, 'lut2d') && ~isempty(obj.res.lut2d))
                plot(obj.res.lut2d(:, 1) + 1, obj.res.lut2d(:, 2) + 1, 'dr','LineWidth',2);
                plot(obj.res.lut2d(:, 1) + 1, obj.res.lut2d(:, 2) + 1, '.r');
            end
        end
        %------------------------------------------------------------------
        
        %------------------------------------------------------------------
        function I = GetRGBImage(obj)
            I(:,:,1) = obj.im;
            I(:,:,2) = obj.im;
            I(:,:,3) = obj.im;
        end
        %------------------------------------------------------------------
                

        %------------------------------------------------------------------
        function I = GetImagePatch(obj,xy,scale,im) %% TODO!! change to x y to use for path clicking!!!
            if ~exist('im','var') || isempty(im)
                im = obj.im;
            end
            I = im( xy(2)-scale:xy(2)+scale ,...
                    xy(1)-scale:xy(1)+scale  , : );
        end
        %------------------------------------------------------------------
        
        %------------------------------------------------------------------
        function paths = Get_paths_moved2center(obj,lut_id,patch_center,data_move2path,scale)
            paths_raw  = obj.calibdata.ps.paths{lut_id}(1:2, :);
            paths      = bsxfun(@plus , paths_raw , -patch_center+data_move2path); %%% enter it to the imge path
            paths      = paths([2 1],:);
            paths      = paths + scale; %%% we will plot in detection center coor
        end
        %------------------------------------------------------------------
        
        %------------------------------------------------------------------
        % if even odd pathid starts from i.e. 90.. -> convert it back to pathid start  from 0  
        function pathid = ConvertLutId2Path(obj,lutid)
            pathid = lutid;
            if lutid>obj.other_calibpar_nopaths %%% threre is need for conversion
                pathid = lutid - obj.other_calibpar_nopaths;
            end
        end
        %------------------------------------------------------------------
        
        
        %------------------------------------------------------------------
        function Plot_DDBox(obj,accum)
            h = subplot(3,2,[5 6]);
            try delete(get(h,'Child')); catch, end; %%% if some graph was plotted, delete it
            try
                [~,~,cdata] = ddbox(accum);
                mesh(cdata);
                axis off tight;
                colormap jet;
            catch
                warning('ddbox call failed');
                sphere(10);
            end
        end
        %------------------------------------------------------------------
        
    end
    
    %% ====================================================================
    methods (Static)
        
        
        %------------------------------------------------------------------
        function CallbacMouseClick(hObject, event, obj)
            
            %--- get coordinates of the click
            cursorPoint = get(hObject.Children,'CurrentPoint');
            curX = cursorPoint(1,1);
            curY = cursorPoint(1,2);
            
            hf = figure(obj.hfig_second);
            set(hf,...
                'KeyPressFcn', @(hObject, event)SShow_detectionsGUI.CallbacKeyPressed(hObject, event, obj),...
                'Color', [1 1 1]);
            
            scale = 50;
            
            %--- now show detection point or line without detection
            %    based on button clicked
            switch get(gcbf, 'SelectionType')
                %-------------------------------------------
                case 'normal' %%% left mosue click button (select point)
                    
                    %--- get indexes of click
                    lut2d_for_search = [ obj.res.lut2d(:,1)' ; obj.res.lut2d(:,2)' ];
                    [~,idx_in_res] = min(sum(bsxfun( @plus , lut2d_for_search , -[curX;curY] ).^2));
                    lut_id      = obj.res.lutid(idx_in_res,1)+1;
                    lut_id_cvrt = obj.ConvertLutId2Path(lut_id);
                    
                    if 0  %%% debug
                        plot(lut2d_for_search(1,:),lut2d_for_search(2,:),'.k'); hold on;
                        plot(lut2d_for_search(1,idx_in_res),lut2d_for_search(2,idx_in_res),'or','LineWidth',2);
                        plot(curX,curY,'xg','LineWidth',2);
                        axis equal
                    end
                    
                    %--- plot image part/patch
                    h = subplot(3,2,1);
                    try delete(get(h,'Child')); catch, end;  %%% delete previous results
                    
                    patch_center = obj.res.lut2d(idx_in_res,:)';
                    data_move2path = + (patch_center - round(patch_center)) +2; %%% this is T to the image patch (1 for c++ fporamring and 1 for getimage -> 1+1=2)
                    patch_center = obj.res.lut2d(idx_in_res,:)';
                    imshow( obj.GetImagePatch( round(patch_center) , scale ) ); hold on
                    plot(scale+data_move2path(1),scale+data_move2path(2),'xr','LineWidth',2); hold on;
                    
                    %--- plot paths in the image patch
                    paths = obj.Get_paths_moved2center(lut_id_cvrt,patch_center,data_move2path,scale);
%                     paths_raw  = obj.calibdata.ps.paths{lut_id}(1:2, :);
%                     paths      = bsxfun(@plus , paths_raw , -patch_center+data_move2path); %%% enter it to the imge path
%                     paths      = paths([2 1],:);
%                     paths      = paths + scale; %%% we will plot in detection center coor
                    if ~(isempty(paths))
                        afs = obj.calibdata.ps.aflags{lut_id_cvrt};
                        ai0 = afs == 0;
                        ai1 = afs == 1;
                        ai2 = afs == 2;
                        plot(paths(2, ai1), paths(1, ai1), '.b'); hold on;
                        plot(paths(2, ai0), paths(1, ai0), '.c');
                        plot(paths(2, ai2), paths(1, ai2), '.m');
                    else
                        disp('empty path');
                    end
                    
                    %--- plot image part and graph with color to the detection rate
                    h = subplot(3,2,2);
                    try delete(get(h,'Child')); catch, end;  %%% delete previous results
                    imshow( obj.GetImagePatch( round(patch_center) , scale , obj.GetRGBImage() ) ); hold on;
                    if ~(isempty(paths))
                        accum = obj.get_accum(lut_id_cvrt, 2, obj.res, obj.csnaccels);
                        pts_size = 8;
                        scatter(paths(2,:),paths(1,:),accum*0+pts_size,accum,'filled');
                        colormap(h,'jet');
                        caxis([0 256]);
                    end
                    
                    %--- get 3d points to see distances
                    [~,idx_in_paths] = min(sum(bsxfun( @plus , obj.calibdata.ps.paths{lut_id_cvrt}(1:2,:) , -obj.res.lut2d(idx_in_res,:)' ).^2));
                    paths3d_raw  = obj.calibdata.ps.paths3D{lut_id_cvrt};
                    if idx_in_paths>1 && idx_in_paths<length(paths3d_raw)-1
                        p_center = paths3d_raw(:,idx_in_paths);
                        p_prev = paths3d_raw(:,idx_in_paths-1);
                        p_next = paths3d_raw(:,idx_in_paths+1);
                        dist2prev = sqrt(sum((p_center-p_prev).^2));
                        dist2next = sqrt(sum((p_center-p_prev).^2));
                        text(105,0,sprintf('d=%.2f [m]',sqrt(sum(p_center.^2))/1000 ));
                        text(105,15,'  dist to');
                        text(105,25,'  next/prev');
                        text(105,35,sprintf('%.2f [m]' ,dist2prev/1000));
                        text(105,45,sprintf('%.2f [m]' ,dist2next/1000));
                    end
                    
                    %--- plot graph with detection score as colors
                    h = subplot(3,2,[3 4]);
                    try delete(get(h,'Child')); catch, end; %%% if some graph was plotted, delete it
                    obj.Show_detections_graphs(lut_id_cvrt);
                    
                    %--- plot ddbox
                    obj.Plot_DDBox(accum);
                %-------------------------------------------
                case 'alt' %%%% right button show curve
                    %-- find min path id
                    min_path_id = [];
                    min_path_dist = inf;
                    patch_center = round([curX curY])';
                    for path_id = 1:length(obj.calibdata.ps.paths)
                        if isempty(obj.calibdata.ps.paths{path_id})
                            continue
                        end
                        [path_dist,~] = min(sum(bsxfun( @plus , obj.calibdata.ps.paths{path_id}(1:2,:) , -patch_center ).^2));
                        if path_dist < min_path_dist
                            min_path_dist = path_dist;
                            min_path_id   = path_id;
                        end
                    end

                    h = subplot(3,2,1);
                    try delete(get(h,'Child')); catch, end;  %%% delete previous results
                    
                    h = subplot(3,2,[3 4]);
                    try delete(get(h,'Child')); catch, end; %%% if some graph was plotted, delete it

                    title(sprintf('Accumulator dump no. %d', min_path_id));
                    accum = obj.get_accum(min_path_id, 2, obj.res, obj.csnaccels);
                    xs = 1:numel(accum);
                    plot(xs, accum, '-b');
                    axis([0,256,min(xs),max(xs)]);
                    
                    h = subplot(3,2,2);
                    try delete(get(h,'Child')); catch, end;  %%% delete previous results
                    imshow( obj.GetImagePatch( patch_center , scale , obj.GetRGBImage() ) ); hold on;
                    
                    data_move2path = +2; %%% this is T to the image patch (1 for c++ fporamring and 1 for getimage -> 1+1=2)
                    
                    %--- plot paths in the image patch
                    paths = obj.Get_paths_moved2center(min_path_id,patch_center,data_move2path,scale);
                    if ~(isempty(paths))
                        pts_size = 3;
                        scatter(paths(2,:),paths(1,:),accum*0+pts_size,accum,'filled');
                        colormap(h,'jet');
                        caxis([0 256]);
                    end
                    
                    %--- plot ddbox
                    obj.Plot_DDBox(accum);
            end
            
        end
        %------------------------------------------------------------------
        
        %------------------------------------------------------------------
        function CallbacKeyPressed(hObject, event, obj)
            key = get(hObject, 'CurrentKey');
            if strcmp(key,'q')
                try
                    delete(obj.hfig_second);
                catch, end;
                try
                    delete(obj.hfig_main);
                catch, end;
                disp(' Show detections: figs deleted, bye');
            end
        end
%         %------------------------------------------------------------------
%         function [cols, rows] = get_windows_positions(no_figs)
%             screen = get(0, 'ScreenSize');
%             rows = 1;
%             while(1 == 1)
%                 h = floor(screen(4) / rows);
%                 cols = floor(screen(3) / h);
%                 if (rows * cols >= no_figs)
%                     break;
%                 end
%                 rows = rows + 1;
%             end
%         end
%         %------------------------------------------------------------------
        
%         %------------------------------------------------------------------
%         function subfig(rows, cols, ord, f)
%             twidth = 20;
%             screen = get(0, 'ScreenSize');
%             w = screen(3);
%             h = screen(4);
%             fw = w / cols;
%             fh = h / rows - twidth;
%             i = ceil(ord / cols);
%             j = rem(ord - 1, cols);
%             left = j * fw + screen(1);
%             bottom = h - i * fh + screen(2);
%             
%             if (nargin < 4)
%                 f = figure('Visible','off');
%             end
%             
%             set(f, 'OuterPosition', [left bottom fw fh]);
%             set(f, 'Visible', 'on');
%         end
%         %------------------------------------------------------------------

        
        %------------------------------------------------------------------
        function accum = get_accum(aidx, cidx, res, csidx)
            idx = (csidx(aidx)+1):csidx(aidx + 1);
            accum = res.accdump(idx, cidx);
        end
        %------------------------------------------------------------------
        
        %------------------------------------------------------------------
        function dets = get_detections(aidx, res, csidx)
            idx = (res.dets(:, 1) >= (csidx(aidx)+1)) & (res.dets(:, 1) <= csidx(aidx + 1));
            dets = res.dets(idx, :);
        end
        %------------------------------------------------------------------
        
    end
end

