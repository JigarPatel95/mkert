
%{


addpath mkert/mkedd/matlab/
addpath mkert/mkedd/matlab/util
addpath mkert/mkedd/build/matlab/

  % with default profile: 
ShowGUIScript('/mnt/RnD/testdata/190318_dataFromP015R03S000/hand01.pgm','/mnt/RnD/calibdata/P015R03S000/190318_01/mkedet01_P015R03S000.bin');

  % with set profile: 
profile = '{ "detector": {"box_delta": 2.0, "classifier":"CircLinear","classifier_params": { "store_dbg_data": true }, "curv_ratio":1.15, "fit_peaks": true, "kernel_widths": [3,5,7,9,13,17,21,29,37,53], "no_epochs": 3, "no_threads": 8, "threshold": 8,  "type": "BoxLut3" }, "postproc": { "type": "PassThru" } }';
ShowGUIScript('/mnt/RnD/testdata/190318_dataFromP015R03S000/hand01.pgm','/mnt/RnD/calibdata/P015R03S000/190318_01/mkedet01_P015R03S000.bin','profile',profile);

example to run mkedd viewer gui

Jenda, 2019
%}

function ShowGUIScript(im,mkedet,varargin)
iargs = inputParser;
iargs.addOptional('profile','');
iargs.parse(varargin{:}); iargs = iargs.Results;

mkedd = MkEDetector_ShowDetGUI;
mkedd.setMkEDetFile(mkedet);

if ~isempty(iargs.profile)
    disp('using user defined profile');
    mkedd.setProfile(iargs.profile);
end

mkedd.showMkEDetInfo();

mkedd.execute(im, 1);
%mkedd.showResults();
mkedd.ShowGUI();

end