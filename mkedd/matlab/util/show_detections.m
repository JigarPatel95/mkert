function show_detections(res, calibdata, pidx)

if (nargin < 3)
    pidx = [];
end

no_paths = numel(calibdata.ps.paths3D);
csnaccels = [0, cumsum(calibdata.ps.naccels)];

if (~isempty(pidx))
    % Show path detections
    [cols, rows] = get_windows_positions(numel(pidx));
    fgs = 1;

    for i = pidx
        if (i < 1 || i > no_paths)
            error('Path index [%d] out of bounds [1,%d]', i, no_paths);
        end
        
        subfig(rows, cols, fgs);
        hold on;
        fgs = fgs + 1;
        
        % Accumulator
        accum = get_accum(i, 2, res, csnaccels);
        xs = 1:numel(accum);
        plot(xs, accum, '-b');

        % Active flags    
        afs = calibdata.ps.aflags{i};        
        zs = zeros(size(xs));
        
        plot(xs(afs == 0), zs(afs == 0), 'xr');
        plot(xs(afs == 2), zs(afs == 2), 'xm');
        
        % Threshold
        plot([xs(1), xs(end)], [res.profile.detector.threshold, res.profile.detector.threshold], ':k');
        
        % Detection as Gaussians
        idx = find(res.accums == (i - 1));
        if (isempty(idx))
            continue;
        end
        
        %dets = get_detections(i, res, csnaccels); 
        for j = idx
            x = res.accels(j) - csnaccels(i) + 1;
            y = accum(x);
            s = res.sigma2d(j);
            r = res.resp(j); %dets(j, 4);
            gs = get_gaussian(x, s, 100, y);
            gsr = get_gaussian(x, s, 100, r);
            
            plot(gs(1, :), gs(2, :), '-.r');
            text(x + 2, y + 10, sprintf('x=%.2f, y=%.2f\n s=%.2f, w=%.2f', x, y, s, 0)); 
            
            plot(gsr(1, :), gsr(2, :), '-.c');
            text(x + 2, r + 10, sprintf('x=%.2f, r=%.2f', x, r)); 
        end
        
        title(sprintf('Accumulator dump no. %d', i));
    end
else
    % Show image detections
    [cols, rows] = get_windows_positions(2);
    
    f = figure('Visible','off');
    w = warning('off', 'all');  
    if (isfield(res, 'image'))
        im = res.image;
    else
        im = imread(res.image_path);
    end
    imshow(im);
    warning(w);
    hold on;
    fgs = 1;
    subfig(rows, cols, fgs, f);    
    fgs = fgs + 1;
    
    for i = 1:no_paths
       
        if (isempty(calibdata.ps.paths{i}))
            continue;
        end        
        
        paths = calibdata.ps.paths{i}(1:2, :) + 1;

        if (isempty(paths))
            continue;
        end
        
        afs = calibdata.ps.aflags{i};        
        
        ai0 = afs == 0;
        ai1 = afs == 1;
        ai2 = afs == 2;
        
        plot(paths(1, ai1), paths(2, ai1), '.b');        
        plot(paths(1, ai0), paths(2, ai0), '.c');
        plot(paths(1, ai2), paths(2, ai2), '.m');
        
        text(paths(1, 1) + 2, paths(2, 1) + 2, sprintf('%d', i), 'Color', [0, 1, 1]);
    end
    
    if (isfield(res, 'lut2d'))
        plot(res.lut2d(:, 1) + 1, res.lut2d(:, 2) + 1, 'dr');
        plot(res.lut2d(:, 1) + 1, res.lut2d(:, 2) + 1, '.r');
    end
    
    % Show 3D reconstruction
    subfig(rows, cols, fgs);
    hold on;

    if (isfield(res, 'lut3d'))
        plot3(res.lut3d(:, 1), res.lut3d(:, 2), res.lut3d(:, 3), '.r');
    end
    
    axis equal;
end
 
end
% -------------------------------------------------------------------------
% Functions ---------------------------------------------------------------

function [cols, rows] = get_windows_positions(no_figs)
screen = get(0, 'ScreenSize');
rows = 1;
while(1 == 1)
    h = floor(screen(4) / rows);
    cols = floor(screen(3) / h);
    if (rows * cols >= no_figs)
        break;
    end
    rows = rows + 1;
end
end

function subfig(rows, cols, ord, f)
    twidth = 20;
    screen = get(0, 'ScreenSize');
    w = screen(3);
    h = screen(4);
    fw = w / cols;
    fh = h / rows - twidth;
    i = ceil(ord / cols);
    j = rem(ord - 1, cols);
    left = j * fw + screen(1);
    bottom = h - i * fh + screen(2);
    
    if (nargin < 4)
        f = figure('Visible','off');
    end
    
    set(f, 'OuterPosition', [left bottom fw fh]);
    set(f, 'Visible', 'on');
end

function accum = get_accum(aidx, cidx, res, csidx)
    idx = (csidx(aidx)+1):csidx(aidx + 1);
    accum = res.accdump(idx, cidx);
end

function dets = get_detections(aidx, res, csidx)
    idx = (res.dets(:, 1) >= (csidx(aidx)+1)) & (res.dets(:, 1) <= csidx(aidx + 1));
    dets = res.dets(idx, :);
end