/* 
 * dcClassifier : Matlab interface to the Classifier object
 * 
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#include <stdexcept>
#include <cstring>

#include <mex.h>

#include "json.hpp"

#include "mx/utils.h"
#include "mx/class.h"
#include "mx/json.h"

#include "dc/factory.h"

using nlohmann::json;
using namespace mke::dd;
using namespace mke::dd::dc;


// ============================================================================

void dcClassifier(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  mx::no_params_geq(nrhs, 1, "function takes at least one parameter");
  
  std::string cmd = mx::mxarray_to_string(prhs[0]);
  
  // ==========================================================================
  if (cmd == "new")
    {
      mx::no_params_eq(nrhs, 2, "mke::dd::dc::Classifier: function 'new' takes two parameter");
      std::string dcname = mke::dd::mx::mxarray_to_string(prhs[1]);

      Classifier *classifier = dc::Factory::create(dcname);
      plhs[0] = mx::ptr_to_mat<Classifier>(classifier);
    }
  // ============================================================================
  else if (cmd == "delete")
    {
      mx::no_params_eq(nrhs, 2, "mke::dd::dc::Classifier: function 'delete' takes two parameter");
      mx::destroy_object<Classifier>(prhs[1]);
    }
  // ============================================================================
  // Parameters =================================================================
  else if (cmd == "getParams")
    {
      mx::no_params_eq(nrhs, 2, "mke::dd::dc::Classifier: function 'getParams' takes two parameter");
      Classifier *classifier = mx::mat_to_ptr<Classifier>(prhs[1]);
      json params;
      classifier->getParams(params);
      mx::nljson_to_mxarray(params, plhs[0]);
    }
  // ============================================================================
  else if (cmd == "setParams")
    {
      mx::no_params_eq(nrhs, 3, "mke::dd::dc::Classifier: function 'setParams' takes three parameter");
      Classifier *classifier = mx::mat_to_ptr<Classifier>(prhs[1]);
      json params;
      mx::mxarray_to_nljson(prhs[2], params);
      classifier->setParams(params);
    }
  // ============================================================================
  // Frame ======================================================================
  else if (cmd == "setFrameParams")
    {
      mx::no_params_eq(nrhs, 5, "mke::dd::dc::Classifier: function 'setFrameParams' takes five parameter");
      Classifier *classifier = mx::mat_to_ptr<Classifier>(prhs[1]);
      int width = int(*mxGetPr(prhs[2]));
      int height = int(*mxGetPr(prhs[3]));
      int stride = int(*mxGetPr(prhs[4]));
      classifier->setFrameParams(width, height, stride);
    }
  // ============================================================================
  // Classification =============================================================
  else if (cmd == "classify")
    {
      mx::no_params_eq(nrhs, 7, "mke::dd::dc::Classifier: function 'classify' takes seven parameter");
      Classifier *classifier = mx::mat_to_ptr<Classifier>(prhs[1]);
      const uint8_t *data = (uint8_t *) mxGetPr(prhs[2]);
      int u = int(*mxGetPr(prhs[3]));
      int v = int(*mxGetPr(prhs[4]));
      uint32_t uid = uint32_t(*mxGetPr(prhs[5]));
      double *pt3d = mxGetPr(prhs[6]);
      int16_t ipt3d[] = {int16_t(pt3d[0]), int16_t(pt3d[1]), int16_t(pt3d[2])};

      classifier->classify(data, u, v, uid, ipt3d);
    }
  // ============================================================================
  // DebugData ==================================================================
  else if (cmd == "initDebugData")
    {
      mx::no_params_eq(nrhs, 2, "mke::dd::dc::Classifier: function 'initDebugData' takes two parameter");
      Classifier *classifier = mx::mat_to_ptr<Classifier>(prhs[1]);
      classifier->initDebugData();
    }
  // ============================================================================
  else if (cmd == "getDebugData")
    {
      mx::no_params_eq(nrhs, 2, "mke::dd::dc::Classifier: function 'getDebugData' takes two parameter");
      Classifier *classifier = mx::mat_to_ptr<Classifier>(prhs[1]);
      json dbg_data;
      classifier->getDebugData(dbg_data);
      mx::nljson_to_mxarray(dbg_data, plhs[0]);
    }
  // ============================================================================
  else
    {
     throw std::runtime_error(
       std::string("mkeddmm('mke::dd::dc::Classifier', 'cmd', ...) : unknown command '")
       + cmd + "'");
    }
}    
