/*
 * Utility JSON file functions
 *
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#include "mex.h"


#include "mx/utils.h"
#include "mx/class.h"
#include "mx/json.h"

#include <iostream>
#include <fstream>

using nlohmann::json;
using namespace mke::dd;

void utilJsonFile(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{

  std::string cmd = mx::mxarray_to_string(prhs[0]);

  // ======================================================================
  // SaveToFile
  if (cmd == "SaveToFile")
    {
      mx::no_params_eq(nrhs, 3,
        "mkeddmm('mke::dd::util::jsonfile', 'SaveToFile', fpath, data): function 'mkeddmm' takes four parameters");

      std::string fpath = mx::mxarray_to_string(prhs[1]);
      json data_json = mx::mxarray_to_nljson(prhs[2]);

      std::ofstream ofs(fpath.c_str());
      ofs << data_json.dump(2);
    }
  // ======================================================================
  // LoadFromFile
  else if (cmd == "LoadFromFile")
    {
      mx::no_params_eq(nrhs, 2,
        "mkeddmm('mke::dd::util::jsonfile, 'LoadFromFile', fpath): function 'mkeddmm' takes three parameters");

      std::string fpath = mx::mxarray_to_string(prhs[1]);

      std::ifstream ifs(fpath.c_str());
      json data_json = json::parse(ifs);
      mx::nljson_to_mxarray(data_json, plhs[0]);
    }
  // ======================================================================
  // Translate
  else if (cmd == "Translate")
    {
      mx::no_params_eq(nrhs, 2,
        "mkeddmm('mke::dd::util::jsonfile', 'Translate', data): function 'mkeddmm' takes three parameters");

      json data_json = mx::mxarray_to_nljson(prhs[1]);
      mx::nljson_to_mxarray(data_json, plhs[0]);
  }
  // ======================================================================
  // Dump
  else if (cmd == "Dump")
    {
      mx::no_params_eq(nrhs, 2,
        "mkeddmm('mke::dd::util::jsonfile', 'Dump', data): function 'mkeddmm' takes three parameters");

      json data_json = mx::mxarray_to_nljson(prhs[1]);
      std::cout << data_json.dump(2) << std::endl;
    }
  // ======================================================================
  else
    {
      throw std::runtime_error(std::string("mkeddmm('mke::dd::util::jsonfile', 'cmd', ...) : unknown command '") + cmd + "'");
    }
}
