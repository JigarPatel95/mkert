classdef MkeddClassifier < handle
    
    properties (SetAccess = private, Hidden = true)
        objectHandle; % Handle to the underlying C++ class instance
        
        frame;
    end
    
    %======================================================================
    
    methods (Access = public)
        function obj = MkeddClassifier(name)
            obj.objectHandle = mkeddmm('mke::dd::dc::Classifier', 'new', name);
        end
        
        function delete(obj)
            mkeddmm('mke::dd::dc::Classifier', 'delete', obj.objectHandle);
        end
        
        function params = getParams(obj)
            params = mkeddmm('mke::dd::dc::Classifier', 'getParams', obj.objectHandle);
        end
        
        function setParams(obj, params)
            mkeddmm('mke::dd::dc::Classifier', 'setParams', obj.objectHandle, params);
        end      
        
        function initDebugData(obj)
            mkeddmm('mke::dd::dc::Classifier', 'initDebugData', obj.objectHandle);
        end               

        function dbg_data = getDebugData(obj)
            dbg_data = mkeddmm('mke::dd::dc::Classifier', 'getDebugData', obj.objectHandle);
        end               
        
        function setFrameParams(obj, width, height, stride)
            mkeddmm('mke::dd::dc::Classifier', 'setFrameParams', obj.objectHandle, width, height, stride);
        end
        
        function classify(obj, data, u, v, uid, pt3d)
            mkeddmm('mke::dd::dc::Classifier', 'classify', obj.objectHandle, data, u, v, uid, pt3d);
        end
        
        % =================================================================
        
        function setFrame(obj, data)
            obj.frame = data';
            [width, height] = size(obj.frame);            
            obj.setFrameParams(width, height, width);
        end            
        
        function res = process(obj, data, u, v, uid, pt3d)
            if (nargin < 5)
                uid = 0;
            end
            
            if (nargin < 6)
                pt3d = [0, 0, 0];
            end
            
            obj.setFrame(data);
            obj.initDebugData();
            obj.classify(obj.frame, u, v, uid, pt3d);
            res = obj.getDebugData();
        end            
     end
end