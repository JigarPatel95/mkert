/* 
 * mkeccmm : Matlab interface to the mkecc library
 * 
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#include <stdexcept>
#include <map>
#include <functional>
#include <cstring>

#include <mex.h>

#include "mx/utils.h"
#include "json.hpp"


typedef std::function<void(int, mxArray**, int, const mxArray**)> mkeFunction_t;

// ============================================================================
// External interfaces

void ddDepthSensor(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);
void dcClassifier(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);
void ddDetector(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);
void utilTester(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);
void utilJsonFile(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);

// ============================================================================
// Interface map

std::map<std::string, mkeFunction_t> cmap = {
    {"mke::dd::DepthSensor", &ddDepthSensor},
    {"mke::dd::dc::Classifier", &dcClassifier},
    {"mke::dd::dd::Detector", &ddDetector},
    {"mke::dd::util::Tester", &utilTester},
    {"mke::dd::util::jsonfile", &utilJsonFile}
};

// ============================================================================

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  try
    {
      mke::dd::mx::no_params_geq(nrhs, 2, "function takes at least one parameter");
      std::string cmd = mke::dd::mx::mxarray_to_string(prhs[0]);
      const auto func = cmap.find(cmd);

      if (func == cmap.end())
        throw std::runtime_error(std::string("unknown command '") + cmd + "'");
      else
        func->second(nlhs, plhs, nrhs - 1, prhs + 1);
    }
  catch (const std::system_error& e)
    {
      std::string err = std::string("mkeddmm: ") + e.code().message() + ", " + std::strerror(errno);
      mexErrMsgTxt(err.c_str());
    }        
  catch (const std::exception& e)
    {
      std::string err = std::string("mkeddmm: ") + e.what();
      mexErrMsgTxt(err.c_str());
    }        
}    
