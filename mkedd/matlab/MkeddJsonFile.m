classdef MkeddJsonFile < handle
    
    methods (Access = public, Static)        
        
        function saveToFile(fpath, data)
            mkeddmm('mke::dd::util::jsonfile', 'SaveToFile', fpath, data);
        end   
        
        function data = loadFromFile(fpath)
            data = mkeddmm('mke::dd::util::jsonfile', 'LoadFromFile', fpath);
        end
        
        function tdata = translate(data)
            tdata = mkeddmm('mke::dd::util::jsonfile', 'Translate', data);
        end   
        
        
        function dump(data)
            mkeddmm('mke::dd::util::jsonfile', 'Dump', data);
        end           

    end
end
