/* 
 * ddDetector : Matlab interface to the Detector object
 * 
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#include <stdexcept>
#include <cstring>

#include <mex.h>

#include "json.hpp"

#include "mx/utils.h"
#include "mx/class.h"
#include "mx/json.h"

#include "dd/factory.h"

using nlohmann::json;
using namespace mke::dd;
using namespace mke::dd::dd;


// ============================================================================

void ddDetector(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  mx::no_params_geq(nrhs, 1, "function takes at least one parameter");
  
  std::string cmd = mx::mxarray_to_string(prhs[0]);
  
  // ==========================================================================
  if (cmd == "new")
    {
      mx::no_params_eq(nrhs, 2, "mke::dd::dd::Detector: function 'new' takes two parameter");
      std::string dcname = mke::dd::mx::mxarray_to_string(prhs[1]);

      Detector *detector = dd::Factory::create(dcname);
      plhs[0] = mx::ptr_to_mat<Detector>(detector);
    }
  // ============================================================================
  else if (cmd == "delete")
    {
      mx::no_params_eq(nrhs, 2, "mke::dd::dd::Detector: function 'delete' takes two parameter");
      mx::destroy_object<Detector>(prhs[1]);
    }
  // ============================================================================
  // Parameters =================================================================
  else if (cmd == "getParams")
    {
      mx::no_params_eq(nrhs, 2, "mke::dd::dd::Detector: function 'getParams' takes two parameter");
      Detector *detector = mx::mat_to_ptr<Detector>(prhs[1]);
      json params;
      detector->getParams(params);
      mx::nljson_to_mxarray(params, plhs[0]);
    }
  // ============================================================================
  else if (cmd == "setParams")
    {
      mx::no_params_eq(nrhs, 3, "mke::dd::dd::Detector: function 'setParams' takes three parameter");
      Detector *detector = mx::mat_to_ptr<Detector>(prhs[1]);
      json params;
      mx::mxarray_to_nljson(prhs[2], params);
      detector->setParams(params);
    }
  // ============================================================================
/*
  else if (cmd == "getTriangulation")
    {
      mx::no_params_eq(nrhs, 2, "mke::dd::dd::Detector: function 'processAccum' takes two parameter");

      json data;
      double *accum;
      size_t m, n;

      Detector *detector = mx::mat_to_ptr<Detector>(prhs[1]);
      mx::mxarray_to_data(prhs[2], accum, m, n);
      detector->processAccum(accum, std::max(m, n), data);
      mx::nljson_to_mxarray(data, plhs[0]);
    }
 */
  // ============================================================================
  else
    {
     throw std::runtime_error(
       std::string("mkeddmm('mke::dd::dd::Detector', 'cmd', ...) : unknown command '")
       + cmd + "'");
    }
}    
