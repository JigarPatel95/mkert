# MkEDDVis - TODO

List of BUGs to fix and features to implements. Please write the issues in the order of the priority.

## BUGs to fix

- [ ] Fix trajectory detail showing when no trajectory available after image/detector change
- [ ] Fix result vs pattern indexing
 
## Features and improvements

- [ ] Add default behaviour for DockBasedPlugins (for example hide when new)
- [ ] Add some util to easy cut out path for given pat_idx/idx, result for given ridx/idx, debug_data for given ridx/idx
- [ ] Show waiting message box when computes (or rander) charts (maybe progress?)
- [ ] Add convolution matrixes.
> bindings is not ready now
- [ ] Color of the detection accordingly to the z-coord/distance
- [ ] When plugin is not enabled do not render just store input data
- [ ] Open detail view just by pat_id + iid without mouse interaction
- [ ] Some snap functionality to be able visualy compare different setups.
- [ ] Syntax highlighting for profiles (JSON).
- [ ] Some cleanup in trajectory classes
- [ ] Some more clever stats (on plane)
- [ ] Stats on plane - show error in main view?
- [ ] Profiling to speed-up views
- [ ] Verbose mode
- [ ] Add log plugin which provide user possibility to see/store exception in GUI
- [ ] catch signals Control^C

## Done

- [x] Add mkedd version which is currently used by python.
- [x] Support for live checkbox
- [x] Now supporting only one detector per bin file (does not work for FishEye)
- [x] Missing change log
- [x] Missing Readme
- [x] Multiple pickers for one click. -> Make special artist which draw all trajectories in one or because KD tree we could take just x,y click. 
> prepare KDtree for detector at preparation ( decimed )
- [x] Add some cached data for mkedd Detector objects - to avoid preparing path repeatedly.
- [x] Make some decimation for points of trajectory to speed up the rendering.
- [x] Introduce layers for plots (at least for main plugins)
- [x] Some easy plane fitting plugin for easier understanding the scene.
- [x] Some mathematical functionality to compare results/mkeddbins.
- [x] Plugins interface and implementation
- [x] Add property plugin (whole API throght messages).
