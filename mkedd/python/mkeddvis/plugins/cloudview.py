
__author__ = "Ondra Fisar"
__copyright__ = "Copyright 2019 MagikEye Inc."

"""Plugin for 3D view
"""

# -----------------------------------------------------------------------------
from core.plugin import DockBasedPlugin

from PyQt5 import QtWidgets, QtCore
import numpy as np

from matplotlib.backends.backend_qt5agg import (
    FigureCanvas, NavigationToolbar2QT as NavigationToolbar)
from matplotlib.figure import Figure
from matplotlib.cm import hsv
from mpl_toolkits.mplot3d import Axes3D

# -----------------------------------------------------------------------------

class CloudViewPlugin(DockBasedPlugin):
    
    def __init__(self):
        super(CloudViewPlugin, self).__init__()
        self.__parent = None
        
    def init_ui(self, main_window):
        super(CloudViewPlugin, self).init_ui(main_window, QtCore.Qt.RightDockWidgetArea)
        
        self.__canvas = FigureCanvas(Figure(tight_layout=True, figsize=(5,5)))
        self.__ax = self.__canvas.figure.add_subplot(111, projection='3d')
        
        self._widget.setLayout(QtWidgets.QVBoxLayout())
        self._widget.layout().addWidget(self.__canvas)
        toolbar = NavigationToolbar(self.__canvas, self._widget)
        self._widget.layout().addWidget(toolbar)
        
    def set_model(self, model):
        self.__ax.cla()
        if model and model.results:
            for res in model.results:
                if len(res['lut3d']) > 0:
                    lut3d = np.array(res['lut3d'])
                    cs = lut3d[:,2] - np.min(lut3d[:,2])
                    cs = cs / np.max(cs)
                    cs = hsv(cs)
                    self.__ax.scatter(lut3d[:,0], lut3d[:,1], lut3d[:,2],  c = cs)
        
        self.__canvas.draw()
    
    @staticmethod
    def name():
        return "3D view"
    
