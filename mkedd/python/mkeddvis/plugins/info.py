
__author__ = "Ondra Fisar"
__copyright__ = "Copyright 2019 MagikEye Inc."

"""Plugin for extracting detailed info from the detector
"""

# -----------------------------------------------------------------------------

from core.plugin import DockBasedPlugin
from PyQt5 import QtWidgets, QtCore
import re

# -----------------------------------------------------------------------------

class InfoPlugin(DockBasedPlugin):
    
    KEYS = ['/file_type',
            '/MkEDetFile/detectors/.*/[^p].*',
            '/MkEDetFile/info/info_json',
            '/MkEDetFile/[hl].*']

    def __init__(self):
        super(InfoPlugin, self).__init__(InfoPlugin.name())
        self.__tree = None
        self.__map = {}
        
    def init_ui(self, main_window):
        super(InfoPlugin, self).init_ui(main_window,    
                                        QtCore.Qt.LeftDockWidgetArea)
                
        self.__tree = QtWidgets.QTreeWidget()
        self.__tree.setColumnCount(2)
        self.__tree.setHeaderLabels(['Key', 'Value'])
        self._widget.setLayout(QtWidgets.QVBoxLayout())
        self._widget.layout().addWidget(self.__tree)
        
    def set_model(self, model):
        self.__tree.clear()
        self.__map = {}
        
        if model and model.detector and model.detector.calibdata:
            cal = model.detector.calibdata
            root = self.__tree.invisibleRootItem()
            self.__map['/'] = root
            for k in self.KEYS:
                self.__path(cal, k, root)
            self.__tree.addTopLevelItem(root)
    
    @staticmethod
    def name():
        return "Detailed info"
    
    def __path(self, data, path, parent, prefix=""):
        pos = path.find('/', 1)     
        key = path[1:pos] if pos > 0 else path[1:]
        if type(data) == dict:
            ls = data.keys()
        elif type(data) == list:
            ls = [i for i in range(len(data))]
        else:
            parent.setData(1, QtCore.Qt.DisplayRole, str(data))
            return
        
        for i in ls:
            if key == '' or re.match(key,str(i)):
                kpath = prefix + '/' + str(i)
                if not kpath in self.__map:
                    item = QtWidgets.QTreeWidgetItem()
                    item.setData(0, QtCore.Qt.DisplayRole, str(i))
                    parent.addChild(item)
                    self.__map[kpath] = item
                else:
                    item = self.__map[kpath]
                
                self.__path(data[i], path[pos:], item, kpath)
                                    
    def __add_to_tree(self, path, value):
        if type(value) == str or type(value) == int or type(value) == float:
            item = QtWidgets.QTreeWidgetItem()
            item.setData(0, QtCore.Qt.DisplayRole, path)
            item.setData(1, QtCore.Qt.DisplayRole, str(value))
            return item
