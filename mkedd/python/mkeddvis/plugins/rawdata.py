
__author__ = "Ondra Fisar"
__copyright__ = "Copyright 2019 MagikEye Inc."

"""Plugin for extracting RAW JSON data from the detector, mostly just for selection
"""

# -----------------------------------------------------------------------------

from core.plugin import DockBasedPlugin
from plugins.trajview import TrajectoryPlugin

from PyQt5 import QtWidgets, QtCore

# -----------------------------------------------------------------------------

class RawPlugin(DockBasedPlugin):
    
    def __init__(self):
        super(RawPlugin, self).__init__(RawPlugin.name())
        self.__tree = None
        self.__model = None
        self.__sel_node = QtWidgets.QTreeWidgetItem()
        self.__sel_node.setData(0, QtCore.Qt.DisplayRole, "selection")
        
        self.__selection = None

        
    def init_ui(self, main_window):
        
        widget = QtWidgets.QWidget(self)
        widget.setLayout(QtWidgets.QVBoxLayout())
        self.__box_all = QtWidgets.QCheckBox("Show All Data")
        self.__box_all.toggled.connect(self.__update_tree)
        self.__tree = QtWidgets.QTreeWidget()
        self.__tree.setColumnCount(2)
        self.__tree.setHeaderLabels(['Key', 'Value'])
        self.__tree.header().setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)
        self.__root = self.__tree.invisibleRootItem()
        self.__root.addChild(self.__sel_node)
        self.__tree.addTopLevelItem(self.__root)

        widget.layout().addWidget(self.__tree)
        widget.layout().addWidget(self.__box_all)
        
        super(RawPlugin, self).init_ui(main_window,    
                                       QtCore.Qt.RightDockWidgetArea,
                                       widget=widget)
    def process_message(self, msg, *args, **kwargs):
        if msg == TrajectoryPlugin.MSG_SHOW_SELECTED:
            RawPlugin.check_kwargs(msg, ['pat_idx', 'iid'], **kwargs)
            self.__selection = kwargs
            self.__update_selection()
        
    def set_model(self, model):
        self.__model = model
        
        self.__update_tree()
        
    def __update_tree(self):
        model = self.__model
        
        root = self.__root
        toremove = []
        for i in range(root.childCount()):
            if root.child(i) != self.__sel_node:
                toremove.append(root.child(i))
        for r in toremove:
            root.removeChild(r)
                
        self.__update_selection()

        if not self.is_enabled() or not self.__box_all.isChecked():
            return
    
        if not model or not model.detector:
            return
        
        
        if model.detector.calibdata:
            root.addChild(self.__add_to_tree('calib_data', model.detector.calibdata))
        
        if model.results:
            root.addChild(self.__add_to_tree('results', model.results))

        if model.debug_data:            
            root.addChild(self.__add_to_tree('debug_data', model.debug_data))

        if model.stats:            
            root.addChild(self.__add_to_tree('stats', model.stats))
        
         
    def __update_selection(self):
        if not self.__model or not self.__model.detector or not self.__model.detector.calibdata:
            return
        
        if not self.__selection:
            return
        
        patidx = self.__selection['pat_idx']
        iid = self.__selection['iid']
        model = self.__model
        
        # get UID
        
        paths = self.__model.detector.calibdata['MkEDetFile']['detectors'][patidx]['paths']
        mypath = paths[iid]
        uid = mypath['uid']
        
        # clear node
        
        node = self.__sel_node
        node.takeChildren()
        
        # add path data
        node.addChild(RawPlugin.__add_to_tree('path', mypath))

        # add results data
        
        if model.results:

            for ri in range(len(model.results)):
                res = model.results[ri]
                if not uid in res['uid']:
                    continue
                uididx = res['uid'].index(uid)
                
                resnode = QtWidgets.QTreeWidgetItem()
                resnode.setData(0, QtCore.Qt.DisplayRole, 'result')
                resnode.setData(1, QtCore.Qt.DisplayRole, "%d-%d" % (ri, uididx))
                
                for k in res:
                    knode = QtWidgets.QTreeWidgetItem()
                    knode.setData(0, QtCore.Qt.DisplayRole, k)
                    knode.setData(1, QtCore.Qt.DisplayRole, str(res[k][uididx]))
                    resnode.addChild(knode)
                
                node.addChild(resnode)
    
        
        if model.debug_data:
            # add classifiers
            
            for ri in range(len(model.debug_data)):
                dbg = model.debug_data[ri]['classifier']['patches']
                for pi in range(len(dbg)):
                    if dbg[pi]['uid'] != uid:
                        continue
                
                    item = RawPlugin.__add_to_tree('classifier', dbg[pi])
                    item.setData(1,QtCore.Qt.DisplayRole, "%d-%d" % (ri, pi))
                    
                    node.addChild(item)
            
            
    @staticmethod
    def name():
        return "RAW JSON data"
             
    @staticmethod                       
    def __add_to_tree(key, value):
        item = QtWidgets.QTreeWidgetItem()
        item.setData(0, QtCore.Qt.DisplayRole, key)
        if type(value) == dict:
            for k in value:
                item.addChild(RawPlugin.__add_to_tree(k, value[k]))
        elif type(value) == list:
            item.setData(1, QtCore.Qt.DisplayRole, len(value))
            for i in range(len(value)):
                item.addChild(RawPlugin.__add_to_tree("[%d]"%i, value[i]))
        else:
            item.setData(1, QtCore.Qt.DisplayRole, str(value))
        return item
                