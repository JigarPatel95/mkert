
__author__ = "Ondra Fisar"
__copyright__ = "Copyright 2019 MagikEye Inc."

"""Plugin for showing all trajectories in one view with image
"""

# -----------------------------------------------------------------------------

from matplotlib.backends.qt_compat import QtCore, QtWidgets, QtGui
from matplotlib.backends.backend_qt5agg import FigureCanvas
from matplotlib.figure import Figure
from matplotlib.cm import hsv

import numpy as np
from scipy.spatial import KDTree
import time

from core.plugin import CentralPlugin
from plugins.props import PropertiesPlugin
from util.gui import Container
from util.matplotlib import ShortToolbar, MatplotLayer

        
# -----------------------------------------------------------------------------

# TrajectoryPlugin interface 

class TrajectoryPlugin(QtCore.QObject, CentralPlugin):
    
    MSG_SELECT_TRAJECTORY = "SELECT_TRAJECTORY" # params: pat_idx, iid, uid
    MSG_SHOW_SELECTED = "SHOW_SELECTED" # params: pat_idx, iid
    MSG_SHOW_SAMPLE = "SHOW_SAMPLE" # params: xy
    
    KD_SAMPLING = 10
    MIN_CLICK_DIST = 5
    CLICK_TIMEOUT = 0.2
    
    def __init__(self):
        super(TrajectoryPlugin, self).__init__()
        self.__click_time = 0
        self.__last_im = None

    def init_ui(self, ddwin):
        self.__ddwin = ddwin
        main_widget = ddwin.viewWidgetContents

#-----------       
        layout = QtWidgets.QVBoxLayout(main_widget)

        self.__main_canvas = FigureCanvas(Figure(tight_layout=True, 
                                                 dpi=90))
        self.__main_ax = self.__main_canvas.figure.subplots()
#        self.__main_canvas.figure.set_tight_layout(True)
        self.__main_canvas.mpl_connect('button_press_event', self.__onclick)
        self.__main_canvas.mpl_connect('button_release_event', self.__onrelease)
        layout.addWidget(self.__main_canvas)
        toolbar = ShortToolbar(self.__main_canvas, main_widget)
        layout.addWidget(toolbar)

#-----------
        self.__im_layer = MatplotLayer(self.__main_ax, 'Image', 
                                       icon=QtGui.QIcon.fromTheme('image-x-generic'),
                                       tooltip="Show/hide image layer")
        self.__traj_layer = MatplotLayer(self.__main_ax, 'Trajectories',
                                       icon=QtGui.QIcon.fromTheme('format-justify-fill'),
                                       tooltip="Show/hide trajectory layer")
        self.__det_layer = MatplotLayer(self.__main_ax, 'Detection',
                                       icon=QtGui.QIcon.fromTheme('media-record'),
                                       tooltip="Show/hide detection layer")
        self.__sel_layer = MatplotLayer(self.__main_ax, 'Selection',
                                       icon=QtGui.QIcon.fromTheme('edit-find'),
                                       tooltip="Show/hide selection layer")
        
        for l in [self.__im_layer, self.__traj_layer, self.__det_layer, self.__sel_layer]:        
            btn = QtWidgets.QToolButton(toolbar)
            btn.setDefaultAction(l.toggleAction())
            toolbar.addWidget(btn)
           
        self.__sample_art = None
        
#-----------       

        self.__cmap_limits = [0,255]
        self.__sampling_step = 2

#-----------       
        self.set_model(None)  

#-----------       
        ddwin.dispatch_message(PropertiesPlugin.MSG_REGISTER_PROPERTY,
                               name='dpi', label='DPI', type='slider',
                               default=90, min=30, max=120)
        ddwin.dispatch_message(PropertiesPlugin.MSG_REGISTER_PROPERTY,
                               name='sampling_step', label='Sampling step', type='slider',
                               default=5, min=1, max=20)
        ddwin.dispatch_message(PropertiesPlugin.MSG_REGISTER_PROPERTY,
                               name='cmap_limit', label='Colormap shift', type='slider',
                               default=255, min=0, max=255)
        
    def process_message(self, msg, *args, **kwargs):
        if msg == TrajectoryPlugin.MSG_SHOW_SELECTED:
            TrajectoryPlugin.check_kwargs(msg, ['pat_idx', 'iid'], **kwargs)            
            self.__show_selection(kwargs['pat_idx'], kwargs['iid'], draw_bbox=True)
        elif msg == PropertiesPlugin.MSG_PROPERTY_CHANGED:
            TrajectoryPlugin.check_kwargs(msg, ['name', 'value'], **kwargs)
            if kwargs['name'] == 'cmap_limit':
                self.__change_cmap_limits(int(kwargs['value']))
            elif kwargs['name'] == 'dpi':
                self.__change_dpi(int(kwargs['value']))
            elif kwargs['name'] == 'sampling_step':
                self.__change_sampling(int(kwargs['value']))
        elif msg == TrajectoryPlugin.MSG_SHOW_SAMPLE:
            TrajectoryPlugin.check_kwargs(msg, ['xy'], **kwargs)            
            self.__show_sample(kwargs['xy'])

    def is_enabled(self):
        return True
        
    @staticmethod
    def name():
        return "Trajectory View"

    def __onclick(self, event):
        self.__click_time = time.time()
    
    def __onrelease(self, event):
        if (time.time() - self.__click_time) < TrajectoryPlugin.CLICK_TIMEOUT:
            self.__select_trajectory(event)
        
    def __select_trajectory(self, event):
        
        if not self.__calib:
           return True

        dmin = TrajectoryPlugin.MIN_CLICK_DIST
        cimin = None
        imin = None
        
        for ci in range(len(self.__calib)):
            kd = self.__calib[ci]['kd']
            d, i = kd.query([event.xdata, event.ydata])
            if not dmin or d < dmin:
                cimin = ci
                dmin = d
                imin = i

        if imin is None:
            return
        
        cal = self.__calib[cimin]
        trajs = cal['trajs']
        iid = int(trajs[cal['kdidx'][imin],0])
        uid = cal['idx_map'][iid]
        
        self.__ddwin.dispatch_message(TrajectoryPlugin.MSG_SHOW_SELECTED,
                                      pat_idx=ci, iid=iid, uid=uid)
        self.__ddwin.dispatch_message(TrajectoryPlugin.MSG_SELECT_TRAJECTORY,
                                      pat_idx=ci, iid=iid, uid=uid)
        
        return True

    def __prepare_cache(self, detid):
        cal = {}

        if not self.__model or not self.__model.detector:
            return cal
        det = self.__model.detector
                    
        if det.calibdata:
            desc = det.calibdata['MkEDetFile']
            paths = desc['detectors'][detid]['paths']
            N = len(paths)
            total = 0
            for p in paths:
                total = total + len(p['pels_lut'])
            trajs = np.zeros((total, 1+len(p['pels_lut'][0])))
            mask  = np.zeros((total), dtype=np.bool)
            
            off = 0
            idxmap = []
            for i in range(N):
                p = paths[i]
                n = len(p['pels_lut'])
                trajs[off:off+n,1:] = np.array(p['pels_lut'])
                trajs[off:off+n,0] = i
                mask [off:off+n] = np.array(p['mask'])
                off += n
                idxmap.append(p['uid'])
                self.__uid_map[p['uid']] = (detid, i)
                
            cal['paths'] = paths
            cal['trajs'] = trajs
            cal['mask'] = mask
            
            sampling = [i for i in range(0,trajs.shape[0], TrajectoryPlugin.KD_SAMPLING)]
            cal['kd'] = KDTree(trajs[sampling,1:3])
            cal['kdidx'] = sampling
            cal['idx_map'] = idxmap
            
        return cal
            
    def __prepare_desc(self, detid, residx, calib):
        if not self.__model or not self.__model.detector:
            None
            
        det = self.__model.detector
        
        if self.__model.debug_data and self.__model.debug_data[residx]['classifier']:
            class_uids = np.array([ p['uid'] for p in self.__model.debug_data[residx]['classifier']['patches']])
        else:
            class_uids = np.array([])
            
        if det.calibdata:
            desc = det.calibdata['MkEDetFile']
            paths = desc['detectors'][detid]['paths']
            trajs = calib['trajs']
            total = trajs.shape[0]
            desc  = np.zeros((total), dtype=np.uint8)
            
            for idx in np.unique(trajs[:,0]):
                p = paths[int(idx)]

                if self.__model.results is None or len(self.__model.results)==0:
                    d = 3
                elif p['uid'] in self.__model.results[residx]['uid']:
                    d = 1
                elif np.sum(class_uids==p['uid']) > 1:
                    d = 2
                else:
                    d = 0
                desc[trajs[:,0]==idx] = d
                
        return desc        
    
    def __render(self):
#        self.__main_canvas.figure.clf()
        if not self.__model:
            return
            
        self.__render_im()
        self.__render_paths()
        self.__render_results()
 
        if self.__model.image is None:       
            self.__main_ax.axis('image')
            lims = self.__main_ax.get_ylim()
            if lims[1] > lims[0]:
                self.__main_ax.invert_yaxis()        

        self.__main_canvas.draw()
        
        
    def __render_im(self):
        if self.__model and self.__model.image:
            print("IMAGE " + str(self.__model.image.im.shape))
            self.__im_layer.clear()
            autoscale = self.__last_im is None or self.__last_im.im.shape  != self.__model.image.im.shape
            if autoscale:
                self.__main_ax.autoscale(enable=True,tight=True)
            self.__im_layer.imshow(self.__model.image.im, cmap='gray', 
                                   vmin=self.__cmap_limits[0],
                                   vmax=self.__cmap_limits[1])
            
            if autoscale:
                self.__main_ax.autoscale(enable=False)
            self.__last_im = self.__model.image
        

    def __show_selection(self, pat_idx, idx, change_roi=False, draw_bbox=False):

        self.__sel_layer.clear()
            
        trajs = self.__calib[pat_idx]['trajs']
        mask = trajs[:,0] == idx

        self.__sel_layer.plot(trajs[mask,1], trajs[mask,2], 'mo', alpha=0.5)

        if self.__model.debug_data and self.__model.debug_data[pat_idx]['classifier']:
            patches = self.__model.debug_data[pat_idx]['classifier']['patches']
            uid = self.__calib[pat_idx]['idx_map'][idx]
            my_patches = []
            for i in range(len(patches)):
                if patches[i]['uid'] == uid:
                    my_patches.append(patches[i])

            for p in my_patches:
                border = np.array([[0,0], [1,0], [1,1], [0,1], [0,0]], dtype=np.float64)
                border[:,0] = border[:,0]*float(len(p['data'][0])-1) - len(p['data'][0])/2.
                border[:,1] = border[:,1]*float(len(p['data'])-1) - len(p['data'])/2.
                
                if p['is_dot']:
                    self.__sel_layer.plot(p['cx']+border[:,0], p['cy']+border[:,1], '-', c=[0,1,0])
                else:
                    self.__sel_layer.plot(p['cx']+border[:,0], p['cy']+border[:,1], 'r-')

        if change_roi:
            ax = self.__main_ax
            ax.set_xbound(np.min(trajs[mask,1])-10, np.max(trajs[mask,1])+10)
            ax.set_ybound(np.min(trajs[mask,2])-10, np.max(trajs[mask,2])+10)
        if draw_bbox:
            bbox = np.array([[0,0,1,1,0],[0,1,1,0,0]])
            bbox[0,bbox[0,:] == 1] = np.max(trajs[mask,1])+10
            bbox[0,bbox[0,:] == 0] = np.min(trajs[mask,1])-10
            bbox[1,bbox[1,:] == 1] = np.max(trajs[mask,2])+10
            bbox[1,bbox[1,:] == 0] = np.min(trajs[mask,2])-10
            self.__sel_layer.plot(bbox[0,:], bbox[1,:], 'm-', linewidth=5, alpha=0.5)

        self.__main_canvas.draw()

    def __show_sample(self, xy):
        if self.__sample_art in self.__sel_layer:
            self.__sel_layer.remove(self.__sample_art)

        if xy:
            self.__sample_art = self.__sel_layer.plot(xy[0], xy[1], 'o', c=[0,1,0], linewidth=5)
        else:
            self.__sample_art = None
            
        self.__main_canvas.draw()

    def __render_paths(self):
        if self.__calib:
            self.__traj_layer.clear()
            print("CALIB " + str(len(self.__calib)))
            for cal in self.__calib:
                self.__render_detector_paths(cal)
        else:
            self.__traj_layer.clear()
    
    def __render_detector_paths(self, cal):
        N = len(cal['mask'])
        sampling = np.mod(np.array([i for i in range(N)]), self.__sampling_step) == 0
        mask = np.logical_and(cal['mask'], sampling)
        imask = np.logical_and(np.logical_not(mask), sampling)
        detected = np.logical_and(mask, cal['desc'] == 1)
        nodetected = np.logical_and(mask, cal['desc'] == 0)
        unsure = np.logical_and(mask, cal['desc'] == 2)
        nores = np.logical_and(mask, cal['desc'] == 3)
        trajs = cal['trajs']
        
        self.__traj_layer.plot(trajs[detected,1], trajs[detected,2], 'b.', picker=10)
        self.__traj_layer.plot(trajs[nodetected,1], trajs[nodetected,2], 'c.', picker=10)
        self.__traj_layer.plot(trajs[nores,1], trajs[nores,2], 'b.', picker=10)
        self.__traj_layer.plot(trajs[unsure,1], trajs[unsure,2], 'y.', picker=10)
        self.__traj_layer.plot(trajs[imask,1], trajs[imask,2], 'r.', picker=10)
        
    def __render_results(self):
        if self.__model.results:
            self.__det_layer.clear()
            print("RES " + str(len(self.__model.results)))
            for res in self.__model.results:
                self.__render_detector_results(res)
    
    def __render_detector_results(self, res):
        # skip when no data available
        if len(res['lut2d']) == 0:
            return
            
        lut2d = np.array(res['lut2d'])
        lut3d = np.array(res['lut3d'])
        cs = lut3d[:,2] - np.min(lut3d[:,2])
        cs = cs / np.max(cs)
        cs = hsv(cs)
        
#        self.__det_layer.scatter(lut2d[:,0], lut2d[:,1], 'x', c=cs, markersize=10, markeredgewidth=2)
        self.__det_layer.scatter(lut2d[:,0], lut2d[:,1], marker='x', s=40, c=cs)
                    
    def __change_cmap_limits(self, topval):
        self.__cmap_limits[1] = topval
        self.__render_im()
        self.__main_canvas.draw()

    def __change_dpi(self, val):
        fig = self.__main_canvas.figure
        prev = fig.get_dpi()
        sz = fig.get_size_inches()
        k = prev/val
        
        fig.set_dpi(val)
        fig.set_size_inches(sz[0]*k, sz[1]*k)
        self.__main_canvas.draw()
        
    def __change_sampling(self, val):
        self.__sampling_step = val

        if self.__calib:
            self.__traj_layer.clear()
            print("CALIB " + str(len(self.__calib)))
            for cal in self.__calib:
                self.__render_paths()
        self.__traj_layer.draw()
        
    def set_model(self, model):
        self.__model = model
        
        # do preparation
        self.__uid_map = {}
        if model and model.detector:
            if not self.name() in model.detector.cache:
                model.detector.cache[self.name()] = {'calib': {}}
                calib = {}
            else:
                calib = model.detector.cache[self.name()]['calib']
            
            try:
                self.__calib = []
                update = False
                for rix in range(len(self.__model.det_idxs)):
                    dix = self.__model.det_idxs[rix]
                    if not dix in calib:
                        calib[dix] = self.__prepare_cache(dix)
                        update = True
                    self.__calib.append(calib[dix])
                    self.__calib[-1]['desc'] = self.__prepare_desc(dix, rix, calib[dix])
    
                if update:
                    model.detector.cache[self.name()]['calib'] = calib
            except Exception:
                print("No calib data available")
                self.__calib = None
        else:
            self.__calib = None
            
        self.__render()
