
__author__ = "Ondra Fisar"
__copyright__ = "Copyright 2019 MagikEye Inc."

"""Plugin for some simple statistics
"""

# -----------------------------------------------------------------------------

from core.plugin import DockBasedPlugin
from plugins.props import PropertiesPlugin
import util.math
from PyQt5 import QtWidgets, QtCore
import numpy as np

# -----------------------------------------------------------------------------

class StatsPlugin(DockBasedPlugin):

    PROP_FIT_PLANE = 'fit_plane'
   
    def __init__(self):
        super(StatsPlugin, self).__init__(StatsPlugin.name())
        self.__tree = None
        self.__map = {}
        
    def init_ui(self, ddwin):
        widget = QtWidgets.QTableWidget(ddwin)
        super(StatsPlugin, self).init_ui(ddwin,    
                                        QtCore.Qt.LeftDockWidgetArea,
                                        widget=widget)
        
        widget.setColumnCount(6) # name, num, mean, std, min, max? median
        widget.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)
        widget.verticalHeader().hide();
        widget.setHorizontalHeaderLabels(('Name', 'Count', 'Mean', 'Std', 'Min', 'Max'))
                
    def set_model(self, model):
        self.__model = model
        self.__recompute_stats()

    def enable(self):
        super(StatsPlugin, self).enable()
        self._ddwin.dispatch_message(PropertiesPlugin.MSG_REGISTER_PROPERTY, 
                                     name=StatsPlugin.PROP_FIT_PLANE,
                                     label='Fit plane',
                                     type='check',
                                     default=False)

    def disable(self):
        super(StatsPlugin, self).disable()
        self._ddwin.dispatch_message(PropertiesPlugin.MSG_UNREGISTER_PROPERTY, 
                                     name=StatsPlugin.PROP_FIT_PLANE)
    
    def process_message(self, msg, **kwargs):
        if msg == PropertiesPlugin.MSG_PROPERTY_CHANGED:
            StatsPlugin.check_kwargs(msg, ['name', 'value'], **kwargs)
            if kwargs['name'] == StatsPlugin.PROP_FIT_PLANE:
                self.__fit_plane = kwargs['value']
                self.__recompute_stats()
    
    @staticmethod
    def name():
        return "Stats"
    
    def __recompute_stats(self):
        self._widget.setRowCount(0)
        
        model = self.__model
        if model and model.results:
            for r in range(len(model.results)):
                res = model.results[r]
                lut3d = np.array(res['lut3d'])
                self.__add_to_table("z[%d]" % r, lut3d[:,2])
                
                if self.__fit_plane:
                    (plane, err) = util.math.Plane.fit(lut3d)
                    self.__add_to_table("err_to_fit[%d]" % r, np.abs(err))
                
    def __add_to_table(self, label, values):
        self._widget.insertRow(self._widget.rowCount())
        self._widget.setItem(self._widget.rowCount()-1, 0, 
                             QtWidgets.QTableWidgetItem(label))
        self._widget.setItem(self._widget.rowCount()-1, 1, 
                             QtWidgets.QTableWidgetItem("%d" % len(values)))
        self._widget.setItem(self._widget.rowCount()-1, 2, 
                             QtWidgets.QTableWidgetItem("%.2f" % np.mean(values)))
        self._widget.setItem(self._widget.rowCount()-1, 3, 
                             QtWidgets.QTableWidgetItem("%.2f" % np.std(values)))
        self._widget.setItem(self._widget.rowCount()-1, 4, 
                             QtWidgets.QTableWidgetItem("%.2f" % np.min(values)))
        self._widget.setItem(self._widget.rowCount()-1, 5, 
                             QtWidgets.QTableWidgetItem("%.2f" % np.max(values)))

