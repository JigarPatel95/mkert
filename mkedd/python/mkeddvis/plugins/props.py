
__author__ = "Ondra Fisar"
__copyright__ = "Copyright 2019 MagikEye Inc."

"""Plugin for general properties and broadcasting them
"""

# -----------------------------------------------------------------------------

from core.plugin import DockBasedPlugin
from PyQt5 import QtWidgets, QtCore
from util.gui import QFloatSlider
from functools import partial
import copy

# -----------------------------------------------------------------------------

# there must be better way - solution without internet

class Bind:
    def __init__(self, method, *args, **kwargs):
        self.__method = method
        self.__args = args
        self.__kwargs = kwargs
        
    def __call__(self, *args):
        kwargs = copy.copy(self.__kwargs)
        for k in kwargs:
            if type(kwargs[k]) == str and kwargs[k][0] == '_':
                kwargs[k] = args[int(kwargs[k][1:])-1]
                
        return self.__method(*self.__args, **kwargs)
    
# -----------------------------------------------------------------------------

# the plugin
        
class PropertiesPlugin(DockBasedPlugin):
    
    MSG_REGISTER_PROPERTY = "REGISTER_PROPERTY" # args: name, default, group
    MSG_UNREGISTER_PROPERTY = "UNREGISTER_PROPERTY" # args: name
    MSG_PROPERTY_CHANGED = "PROPERTY_CHANGED" # args: name, value
    
    def __init__(self):
        super(PropertiesPlugin, self).__init__(PropertiesPlugin.name())
        self.__props = {}
        self.__order = []
        self.__values = {}
        
    def init_ui(self, ddwin):        
        super(PropertiesPlugin, self).init_ui(ddwin, QtCore.Qt.LeftDockWidgetArea,
             scrolled=True)
        self._widget.setLayout(QtWidgets.QFormLayout())
        
    def __create_widget(self, p):
        if p['type'] == 'slider':
            widget = QtWidgets.QWidget(self);
            widget.setLayout(QtWidgets.QHBoxLayout())
            widget.layout().setContentsMargins(0,0,0,0)
    
            slider = QFloatSlider(QtCore.Qt.Horizontal, self)
            slider.setTracking(False)
            slider.setFloatRange(p['min'], p['max'], p.get('step', 1.))
            
            label = QtWidgets.QLabel()
            slider.floatSliderMoved.connect(label.setNum)
            slider.floatValueChanged.connect(label.setNum)
            slider.floatValueChanged.connect(Bind(self.__broadcast, name=p['name'],
                                                  value='_1'))
    #        slider.floatValueChanged.connect(partial(self.__broadcast, name=p['name']))
    
            widget.layout().addWidget(slider)
            widget.layout().addWidget(label)
            
            return (slider, widget)        
        if p['type'] == 'check':
            widget = QtWidgets.QCheckBox(self)
            widget.stateChanged.connect(Bind(self.__broadcast_bool, name=p['name'],
                                                  value='_1'))
            return (widget, widget)
        
    def process_message(self, msg, *arg, **kwargs):
        if msg == PropertiesPlugin.MSG_REGISTER_PROPERTY:
            PropertiesPlugin.check_kwargs(msg, ['name', 'default', 'label', 'type'], **kwargs)
            self.register_property(**kwargs)
        elif msg == PropertiesPlugin.MSG_UNREGISTER_PROPERTY:
            PropertiesPlugin.check_kwargs(msg, ['name'], **kwargs)
            self.unregister_property(kwargs['name'])
        elif msg == PropertiesPlugin.MSG_PROPERTY_CHANGED:
            PropertiesPlugin.check_kwargs(msg, ['name', 'value'], **kwargs)
            self.__update_property(kwargs['name'], kwargs['value'])
    
    @staticmethod
    def name():
        return "Properties"
    
    def register_property(self, name, **kwargs):
        if name in self.__props:
            raise RuntimeError("Property %s already in proplist" % name)
        
        self.__props[name] = kwargs
        self.__props[name]['name'] = name

        # may be validate params
        # ...
        
        (control, widget) = self.__create_widget(self.__props[name])
        self.__props[name]['control'] = control
        self.__props[name]['widget'] = widget
        
        self._widget.layout().addRow(kwargs['label'], widget)
        self.__order.append(name)
        
        self._ddwin.dispatch_message(PropertiesPlugin.MSG_PROPERTY_CHANGED,
                                     name=name, value=self.__props[name]['default'])
        
    def unregister_property(self, name):
        if not name in self.__props:
#            raise RuntimeError("Property %s missing in proplist" % name)
            return # can happend during initialization
            
        row = self.__order.index(name)
        self.__order.remove(name)
        self.__props.pop(name)
        
        # reomve property from layout

        self._widget.layout().removeRow(row)
                
    def __update_property(self, name, value):
        prop = self.__props.get(name)
        if prop['type'] == 'slider':
            prop['control'].setFloatValue(value)
        elif prop['type'] == 'check':
            prop['control'].setCheckState(QtCore.Qt.Checked if value else QtCore.Qt.Unchecked)
    
    def __broadcast(self, name, value):        
        self._ddwin.dispatch_message(PropertiesPlugin.MSG_PROPERTY_CHANGED,
                                     name=name, value=value)
    def __broadcast_bool(self, name, value):
        self._ddwin.dispatch_message(PropertiesPlugin.MSG_PROPERTY_CHANGED,
                                     name=name, value=value == QtCore.Qt.Checked)
        