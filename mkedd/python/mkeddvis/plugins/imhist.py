    
__author__ = "Ondra Fisar"
__copyright__ = "Copyright 2019 MagikEye Inc."

"""Plugin for image histogram view
"""

# -----------------------------------------------------------------------------
from core.plugin import DockBasedPlugin

from PyQt5 import QtWidgets, QtCore
import numpy as np

from matplotlib.backends.backend_qt5agg import FigureCanvas
from matplotlib.figure import Figure
from util.matplotlib import ShortToolbar

# -----------------------------------------------------------------------------

class ImHistPlugin(DockBasedPlugin):
    
    def __init__(self):
        super(ImHistPlugin, self).__init__()
        self.__parent = None
        
        
    def init_ui(self, main_window):
        super(ImHistPlugin, self).init_ui(main_window, QtCore.Qt.RightDockWidgetArea)
        
        self.__canvas = FigureCanvas(Figure(tight_layout=True, figsize=(5,5)))
        self.__ax = self.__canvas.figure.subplots()
        
        toolbar = ShortToolbar(self.__canvas, self._widget)
        
        self.__box_cumulative = QtWidgets.QCheckBox("Cumulative")
        self.__box_cumulative.toggled.connect(self.__render)

        self._widget.setLayout(QtWidgets.QVBoxLayout())
        self._widget.layout().addWidget(self.__canvas)
        self._widget.layout().addWidget(toolbar)
        self._widget.layout().addWidget(self.__box_cumulative)
        
        
    def set_model(self, model):
        self.__model = model
        self.__render()
        
        
    def __render(self):
        self.__ax.cla()
        self.__ax.grid(b=True, axis='both')
        if not self.is_enabled():
            return 
        
        model = self.__model
        if model and model.image:
            im = model.image.im
            cnts, bins = np.histogram(im.flatten(), bins=np.max(im)-np.min(im))
            self.__ax.hist(bins[:-1], bins, weights=cnts, density=True,
                           cumulative=self.__box_cumulative.isChecked())
        
        self.__canvas.draw()
        
    
    @staticmethod
    def name():
        return "Image Histogram"
    
