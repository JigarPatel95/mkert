from plugins import info
from plugins import trajview
from plugins import trajdetail
from plugins import cloudview
from plugins import props
from plugins import stats
from plugins import rawdata
from plugins import imhist

PLUGINS = [props.PropertiesPlugin,
           info.InfoPlugin,
           trajview.TrajectoryPlugin,
           trajdetail.TrajectoryDetailPlugin,
           cloudview.CloudViewPlugin,
           stats.StatsPlugin,
           rawdata.RawPlugin,
           imhist.ImHistPlugin]
