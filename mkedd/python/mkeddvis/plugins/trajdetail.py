
__author__ = "Ondra Fisar"
__copyright__ = "Copyright 2019 MagikEye Inc."

"""Plugin for showing detail for one trajectory
"""

# -----------------------------------------------------------------------------

from core.plugin import DockBasedPlugin
from util.gui import Container, ContainerView
from util.matplotlib import ShortToolbar
from plugins.trajview import TrajectoryPlugin

from matplotlib.backends.qt_compat import QtWidgets, QtCore
from matplotlib.backends.backend_qt5agg import FigureCanvas
from matplotlib.figure import Figure
import matplotlib.pyplot as plt

import json

import numpy as np

# -----------------------------------------------------------------------------

# Plugin wrapper

class TrajectoryDetailPlugin(DockBasedPlugin, ContainerView):
    
    def __init__(self):
        super(TrajectoryDetailPlugin, self).__init__(TrajectoryDetailPlugin.name())
        
        self.__traj_views = []
        self.__model = None
        self.__ctrl_pressed = False
        
    def init_ui(self, ddwin):
        super(TrajectoryDetailPlugin, self).init_ui(ddwin, QtCore.Qt.RightDockWidgetArea,
             scrolled=True)

        self._widget.setLayout(QtWidgets.QVBoxLayout())
        self._widget.layout().setContentsMargins(0,0,0,0)
        self._widget.layout().addStretch()
    
    @staticmethod
    def name():
        return "Trajectory Details"
    
    def set_model(self, model):
        self.__model = model
        
        for tv in self.__traj_views:
            tv.set_model(model)        
        
    def process_message(self, msg, *args, **kwargs):
        if msg == TrajectoryPlugin.MSG_SELECT_TRAJECTORY:
            TrajectoryDetailPlugin.check_kwargs(msg, ['pat_idx', 'iid', 'uid'], **kwargs)
            self.__show_trajectory(**kwargs)
        elif msg == "CTRL_PRESS":
            self.__ctrl_pressed = True
        elif msg == "CTRL_RELEASE":
            self.__ctrl_pressed = False
#        elif msg == TrajectoryPlugin.MSG_SHOW_SELECTED:
#            if not 'pat_idx' in kwargs or not 'iid' in kwargs or not 'uid' in kwargs:
#                raise ValueError("Bad arguments for message %s" % msg)
#            self.__focus_container(kwargs['container'])
            
    def __show_trajectory(self, pat_idx, iid, uid):
        # check already in view .. yes - make border?
        # or color codding
    
        append = self.__ctrl_pressed
          
        try:
            self.__traj_views.index((pat_idx, iid))
#            self.__traj_views[ti].highlight()
        except ValueError:
            # add to view
            traj_widget = TrajectoryDetailView(self._ddwin, pat_idx, iid, uid, 
                                               model=self.__model)
            traj_container = Container("Trajectory #%d (%d-%d)" % (uid, pat_idx, iid), 
                                               self, 
                                               parent=self._widget,
                                               context_menu=traj_widget.contextMenu(),
                                               uuid=(pat_idx, iid))
            traj_container.layout().addWidget(traj_widget)
            traj_container.set_data(traj_widget)
            if not append and len(self.__traj_views) > 0:
#                self.__sub_layout.itemAt(0).widget().setVisible(False)
                prev = self._widget.layout().itemAt(0).widget()
                self._widget.layout().replaceWidget(prev, 
                                                traj_container)
                prev.setVisible(False)
                self.__traj_views.remove(self.__traj_views[0])
            else:
                self._widget.layout().insertWidget(0,traj_container)
            self.__traj_views.insert(0,traj_widget)
            
    def close_container(self, container):
        try:
            ti = self.__traj_views.index(container.uuid)
            self._widget.layout().removeWidget(container)
            container.setVisible(False)
            self.__traj_views.remove(self.__traj_views[ti])
            
            del container
        except ValueError:
            pass
    
    def focus_container(self, container):
#        self.show_selection(container.uuid[0], container.uuid[1], draw_bbox=True)
        uuid = container.uuid
        self._ddwin.dispatch_message(TrajectoryPlugin.MSG_SHOW_SELECTED, 
                                      pat_idx=uuid[0], iid=uuid[1])
        
    def show_context_menu(self, container):
        container.data().show_context_menu()
            
        
# -----------------------------------------------------------------------------

# Single trajectory view
        
class TrajectoryDetailView(QtWidgets.QWidget):
    
#-----------       
    def __init__(self, ddwin, pat_idx, idx, uid, model = None, parent = None):
        super(TrajectoryDetailView, self).__init__(parent)
        
        self.__ddwin = ddwin
        self.setFixedHeight(400)
        
        self.pat_idx = pat_idx
        self.idx = idx
        self.uid = uid
        layout = QtWidgets.QVBoxLayout(self)
        self.__layout = layout

        self.__canvas = FigureCanvas(Figure(figsize=(5, 3)))
        self.__canvas.mpl_connect('axes_enter_event', self.__mouse_enter)
        self.__canvas.mpl_connect('axes_leave_event', self.__mouse_leave)
        self.__canvas.mpl_connect('motion_notify_event', self.__mouse_move)        
#        self.__sub_ax = self.__sub_canvas.figure.subplots()
        
        layout.addWidget(self.__canvas)
        toolbar = ShortToolbar(self.__canvas, self)
        self.lab3d = QtWidgets.QLabel()
        toolbar.addWidget(self.lab3d)
        layout.addWidget(toolbar)
        
        self.set_model(model)
        self.__inaxes = False
    
    def __hash__(self):
        return hash((self.pat_idx, self.idx))
        
    def __eq__(self, other):
        if self.__class__ == other.__class__:
            return (self.pat_idx, self.idx) == (other.pat_idx, other.idx)
        elif other.__class__ == tuple:
            return (self.pat_idx, self.idx) == other
        
    def __mouse_enter(self, event):
        self.__inaxes = True
    
    def __mouse_leave(self, event):
        self.__inaxes = False
        self.__print3D(None)
    
    def __mouse_move(self, event):
        if self.__inaxes:
            self.__print3D(event.xdata)
        
    def __print3D(self, xdata):
        if not self.__model or not self.__model.detector:
            return
        
        if xdata and xdata >= 0 and xdata < len(self.__mypath['pels_lut'])-1:
            xdata = int(round(xdata))
            lut = self.__mypath['pels_lut'][xdata]
            self.lab3d.setText("[%d,%d,%d]" % tuple(lut[2:]))
            self.__ddwin.dispatch_message(msg=TrajectoryPlugin.MSG_SHOW_SAMPLE, xy=lut[:2])
        else:
            self.lab3d.setText("")
            self.__ddwin.dispatch_message(msg=TrajectoryPlugin.MSG_SHOW_SAMPLE, xy=None)
        
    def __render(self):
        fig = self.__canvas.figure
        fig.clf()
        fig.set_tight_layout(True)
        
        if self.idx is None or self.pat_idx is None or self.__model is None or self.__model.debug_data is None:
            ax = fig.subplots()
            ax.plot([0,1],[0,1],'r-')
            ax.plot([1,0],[0,1],'r-')
            return;
                    
        # get number of classifiers
        my_patches = []
        if self.__model.debug_data and self.__model.debug_data[self.pat_idx]['classifier']:
            patches = self.__model.debug_data[self.pat_idx]['classifier']['patches']
            for i in range(len(patches)):
                if patches[i]['uid'] == self.uid:
                    my_patches.append(patches[i])
        
        grid = plt.GridSpec(1+min(1,len(my_patches)), max(1,len(my_patches)), 
                            wspace=0, hspace=0.3)
        ax = fig.add_subplot(grid[0,:])
        acc = self.__model.debug_data[self.pat_idx]['accums'][self.idx]
        ax.plot([i for i in range(len(acc))], acc, 'b-')
        
        # take the order from the paths
        
        mypath = self.__mypath
        assert(mypath['uid'] == self.uid)

        lut = np.array(mypath['pels_lut'])
        N = lut.shape[0]
        lutpos = []
        for p in my_patches:
            ds = np.linalg.norm(lut[:,:2]-np.tile([p['cx'], p['cy']], (N,1)),
                                axis=1)
            xpos = np.argmin(ds)
            lutpos.append(xpos)
            ax.axvline(x=xpos)
            
        porder = np.argsort(lutpos)

        for pi in range(len(porder)):
            ax = fig.add_subplot(grid[1,pi])
            p = my_patches[porder[pi]]
            ax.imshow(p['data'], vmin=0, vmax=255)
            
            border = np.array([[0,0], [1,0], [1,1], [0,1], [0,0]], dtype=np.float64)
            border[:,0] *= len(p['data'][0])-1
            border[:,1] *= len(p['data'])-1
            
            if p['is_dot']:
                ax.plot(border[:,0], border[:,1], '-', c=[0,1,0])
            else:
                ax.plot(border[:,0], border[:,1], 'r-')
            ax.axis('off')
            ax.annotate("[%d,%d]" % (p['cx'], p['cy']), xy=(p['width']/2.,len(p['data'])+2),
                        horizontalalignment='center')
            lut3d = lut[lutpos[pi],2:]
            ax.annotate("[%d,%d,%d]" % tuple(lut3d), xy=(p['width']/2.,len(p['data'])+5),
                        horizontalalignment='center')
# skip desc, write 3D pos- guess            
#            ax.annotate("[%.2f,%.2f]" % (p['desc'][0], p['desc'][1]), xy=(p['width']/2.,len(p['data'])+5),
#                        horizontalalignment='center')
            bottom,top = ax.get_ylim()
            ax.set_ylim(bottom+6,top)
        
        self.__canvas.draw()
        
    def __find_idxs(self, uid):
        paths = self.__model.detector.calibdata['MkEDetFile']['detectors']
        for p in range(len(paths)):
            for i in range(len(paths[p]['paths'])):
                if paths[p]['paths'][i]['uid'] == self.uid:
                    return (p,i)
        
        return (None,None)
        
    def set_model(self, model):
        self.__model = model
        if model:
            paths = self.__model.detector.calibdata['MkEDetFile']['detectors'][self.pat_idx]['paths']
            self.__mypath = paths[self.idx]
            if self.__mypath['uid'] != self.uid:
                self.pat_idx, self.idx = self.__find_idxs(self.uid)
                paths = self.__model.detector.calibdata['MkEDetFile']['detectors'][self.pat_idx]['paths']
                self.__mypath = paths[self.idx]                
        else:
            self.__mypath = None
            self.pat_idx = None
            self.idx = None
        self.__render()
        self.__print3D(None)

    def export(self):        
        filepath,_ = QtWidgets.QFileDialog.getSaveFileName(
                        caption="Export trajectory detail", 
                        filter="JSON file (*.json)")
        if filepath and filepath != '':
            name, ok = QtWidgets.QInputDialog.getText(self, 'Export with name', 
                                                            'Experiment Name:')
            if ok:
                name = str(name)
            else:
                name = ""
            
            data = self.__model.export_data(name, uid=self.uid)
            with open(filepath, 'w') as f:
                json.dump(data, f, indent=2)
        
    
    def contextMenu(self):
        menu = QtWidgets.QMenu()
        menu.addAction("Export").triggered.connect(self.export)
        
        return menu

