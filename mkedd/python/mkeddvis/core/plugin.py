
__author__ = "Ondra Fisar"
__copyright__ = "Copyright 2019 MagikEye Inc."

"""Plugin interface and some base classes for plugins
"""

# -----------------------------------------------------------------------------

from PyQt5 import QtWidgets, QtCore

# -----------------------------------------------------------------------------

# Interface for Plugins

class IPlugin(object):
    
    def __init__(self):
        self._ddwin = None
        
    def init_ui(self, ddwin):
        self._ddwin = ddwin
    
    def enable(self):
        raise NotImplementedError("Abstract method of plugin " + str(self.name()))

    def disable(self):
        raise NotImplementedError("Abstract method of plugin " + str(self.name()))
    
    # wrapper for QAction::triggered signal
    def set_enabled(self, enabled):
        if enabled == self.is_enabled():
            return
        
        if enabled:
            self.enable()
        else:
            self.disable()

    # abstract method    
    def is_enabled(self):
        raise NotImplementedError("Abstract method of plugin " + str(self.name()))
    
    # abstract method    
    def set_model(self, model):
        pass

    def process_message(self, msg, *args, **kwargs):
        pass
        
    @staticmethod
    def name():
        return "<UNKNOWN PLUGIN>"
    
    @staticmethod
    def check_kwargs(msg, keys, **kwargs):
        for k in keys:
            if not k in kwargs:
                raise ValueError("Missing argument '%s' for message '%s'" % (k, msg))

# -----------------------------------------------------------------------------

# Base class for plugins which main component is DockedWidget

class DockBasedPlugin(QtWidgets.QDockWidget, IPlugin):
    
    def __init__(self, label=None):
        super(DockBasedPlugin, self).__init__(label if label else self.name())
        self._widget = None
        self._scroll = None
        self.setObjectName(self.name().replace(' ', '_'))
        self.toggleViewAction().toggled.connect(self.set_enabled)
    
    def init_ui(self, ddwin, default_pos=QtCore.Qt.LeftDockWidgetArea, widget=None, scrolled=False):
        super(DockBasedPlugin, self).init_ui(ddwin)

        if widget:
            self._widget = widget
        else:
            self._widget = QtWidgets.QWidget(self)

        if scrolled:
            self._scroll = QtWidgets.QScrollArea()
            self._scroll.setWidgetResizable(True)
            self._scroll.setWidget(self._widget)
            self.setWidget(self._scroll)
        else:    
            self.setWidget(self._widget)
        
        ddwin.addDockWidget(default_pos,self)
            
    def enable(self):
        pass
#        self.show()
        
    def disable(self):
        pass
#        self.hide()
        
    def is_enabled(self):
        return not self.isHidden()

# -----------------------------------------------------------------------------

# Base class for plugins which suppose to be in central region

class CentralPlugin(IPlugin):
    
    def __init__(self):
        super(CentralPlugin, self).__init__()
        
    def enable(self):
        pass
        
    def disable(self):
        pass
        
    def is_enabled(self):
        return True
    
# -----------------------------------------------------------------------------

