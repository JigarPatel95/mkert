
__author__ = "Ondra Fisar"
__copyright__ = "Copyright 2019 MagikEye Inc."

"""Classes which wraps input objects (image, detector, profiles ...)
"""

# -----------------------------------------------------------------------------

import PIL
import numpy as np
import json

import pymkedd
from core.version import Info

# -----------------------------------------------------------------------------

# object keeping image with its path
class Image(object):
    
    def __init__(self, impath):
        self.im = np.asarray(PIL.Image.open(impath))
        self.impath = impath
        
    def label(self):
        return self.impath

    def __str__(self):
        return "Image: %s" % self.label()

# -----------------------------------------------------------------------------

# object keeping detector structure
class Detector(object):

    def __init__(self, ddpath):
        self.__load(ddpath)
        
    def __load(self, ddpath):
        self.dd = pymkedd.DepthSensor()
        self.dd.addCalibFile(ddpath)
        self.dd.setDefaultProfile()
        self.default_profile = json.loads(self.dd.getProfile())
        self.ddpath = ddpath
        self.calibdata = self.dd.parseCalibFile(self.ddpath)        
        self.laser_patterns = [self.dd.getLaserPattern(i) for i in range(self.dd.getNoDetectors())]

        # cached data for detector - data keeps the plugins
        self.cache = {} 

    def reload(self):
        self.__load(self.ddpath)
    
    def label(self):
        return self.ddpath
    
    def __str__(self):
        return "MkEDD: %s" % self.label()

# -----------------------------------------------------------------------------

class Profile(object):
    
    def __init__(self, profile = None, default=False):
        self.profile = profile
        self.default = default
    
    def label(self):
        if self.default:
            return '<DEFAULT>'
        return self.profile.get("name", "<UNDEFINED_NAME>")

    def __str__(self):
        return "Profile: %s" % self.label()
        
# -----------------------------------------------------------------------------
        
class Sources(object):
    
    def __init__(self):
        self.__images = []       # list of images
        self.__profiles = []     # list of profiles
        self.__ddbins = []         # list of bins
        
        self.clear()

    # global methods

    def clear(self):
        self.__images = []
        self.__profiles = [Profile(None, True)]
        self.__ddbins = []
        
    def load(self, filename):
        self.clear()
        with open(filename, 'r') as f:
            data = json.load(f)
            for im in data['images']:
                self.add_image(im)
            
            for p in data['profiles']:
                self.add_profile(p)
                
            for b in data['ddbins']:
                self.add_ddbin(b)

    def save(self, filename):

        ims = []
        for i in self.__images:
            ims.append(i.impath)
            
        dds = []
        for d in self.__ddbins:
            dds.append(d.ddpath)
        
        profs = []
        for p in self.__profiles:
            if p.default == False:
                profs.append(p.profile)
        
        output = {'info': Info.info(appname=Info.APPNAME, version=Info.VERSION),
                  'images': ims,
                  'ddbins': dds,
                  'profiles': profs}
        
        with open(filename, 'w') as f:
            json.dump(output, f, indent=4)

    # image methods
    
    def add_image(self, filename):
        self.__images.append(Image(filename))
        
    def remove_image(self, idx):
        self.__images.remove(self.__images[idx])
        
    def images(self):
        return self.__images
            
    # profile methods
    
    def add_profile(self, profile):
        self.__profiles.append(Profile(profile))
        
    def remove_profile(self, idx):
        self.__profiles.remove(self._profiles[idx])
        
    def update_profile(self, idx, profile):
        assert(self.__profiles[idx]['name'] == profile['name'])

        self.__profiles[idx] = profile
    

    def profiles(self):
        return self.__profiles

    # bin methods

    def add_ddbin(self, filename):
        self.__ddbins.append(Detector(filename))
    
    def remove_ddbin(self, idx):
        self.__ddbins.remove(self.__ddbins[idx])

    def reload_ddbins(self):
        for dd in self.__ddbins:
            dd.reload()

    def ddbins(self):
        return self.__ddbins
