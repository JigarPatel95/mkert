
__author__ = "Ondra Fisar"
__copyright__ = "Copyright 2019 MagikEye Inc."

"""Covers running the detector and model for views
"""

# -----------------------------------------------------------------------------

import json
import copy

from core.version import Info

# -----------------------------------------------------------------------------

class Model(object):
    
    def __init__(self, image, det, profile, det_idxs):
        self.image = image
        self.detector = det
        self.profile = profile
        self.det_idxs = det_idxs
        
        self.results = []
        self.debug_data = []
        self.stats = []        

    def export_data(self, name="", uid=None):
        if self.detector is None or self.image is None:
            return {}
        
        ver = '.'.join([str(i) for i in self.detector.dd.getVersion()])
        info = Info.info(appname='libmkedd'+ver, version=ver)
        params = {'calib': self.detector.ddpath,
                  'images': [self.image.impath],
                  'name': name, 
                  'runs': 1,
                  'profile': self.profile
                }

        ret = {'params': [],
               'info': info,
               'results': []}
        
        if uid is None:
            for i in range(len(self.det_idxs)):
                res = {'results': self.results[i],
                       'dbgdata': self.debug_data[i],
                       'stats': self.stats[i]
                       }
                lparams = copy.copy(params)
                lparams['detidx'] = [ self.det_idxs[i] ]
                ret['params'].append(lparams)
                ret['results'].append(res)
        else:
            for i in range(len(self.det_idxs)):
                res = {'stats': self.stats[i],
                       'dbgdata': {'accums': [], 
                                   'classifier': {'patches': []},
                                   'det_idx': [],
                                   'triangulation':
                                       {'coords': [],
                                        'tri': [],
                                        'uid': []}
                                  }
                     }
                       
                if uid not in self.results[i]['uid']:
                    res['results'] = dict([(n,[]) for n in self.results[i].keys()])
                    res['dbgdata'] = {'accums': [], 
                                      'classifier': {'patches': []},
                                      'det_idx': [],
                                      'triangulation':
                                          {'coords': [],
                                           'tri': [],
                                           'uid': []}
                                      }
                else:
                    idx = self.results[i]['uid'].index(uid)
                    res['results'] = dict([(n,[self.results[i][n][idx]]) for n in self.results[i].keys()])
                    res['dbgdata']['accums'] = [self.debug_data[i]['accums'][idx]]
                    res['dbgdata']['det_idx'] = [self.debug_data[i]['det_idx'][idx]]
                    res['dbgdata']['classifier']['patches'] = [p for p in self.debug_data[i]['classifier']['patches'] if p['uid'] == uid]

                lparams = copy.copy(params)
                lparams['detidx'] = [ self.det_idxs[i] ]
                ret['params'].append(lparams)
                ret['results'].append(res)
            
            
        return ret

# -----------------------------------------------------------------------------        

def process(image=None, det=None, profile=None, det_idxs=[]):
    if det and len(det_idxs) == 0:
        det_idxs = [i for i in range(det.dd.getNoDetectors())]
    
    if det and image:
        for idx in det_idxs:
            if det.dd.getStride(idx) < image.im.shape[1]:
                raise RuntimeError("Bad image dimensions %s. Detector stride %d" % (str(image.im.shape), det.dd.getStride(idx)))
    res = Model(image, det, profile, det_idxs)
    
    if det and image:
        dd = det.dd
        
        if profile:
            dd.setProfile(json.dumps(profile))
        
        for i in det_idxs:
            dd.clearDetections(i)
            dd.process(i, image.im)
            res.results.append(dd.getResults(i))
            res.debug_data.append(dd.getDebugData(i))
            res.stats.append(dd.getStats())
    
    return res
            
# -----------------------------------------------------------------------------        
