
__author__ = "Ondra Fisar"
__copyright__ = "Copyright 2019 MagikEye Inc."

"""Main Window for the utility
"""

# -----------------------------------------------------------------------------

from PyQt5 import QtWidgets, QtCore, uic
import os
import re
import json
import traceback

from core.sources import Sources, Profile
from core import mkedd
from core.version import Info
from core.plugin import DockBasedPlugin

# -----------------------------------------------------------------------------

class QListModel(QtCore.QAbstractTableModel):
    
    def __init__(self, listref):
        super(QListModel, self).__init__()
        
        self.__listref = listref
        self.__curr = None
#        self.selectionChanged = QtCore.pyqtBoundSignal()
        
    def columnCount(self, parent=QtCore.QModelIndex()):
        return 1
    
    def rowCount(self, parent=QtCore.QModelIndex()):    
        return len(self.__listref())
    
    def data(self, index, role=QtCore.Qt.DisplayRole):
        if role == QtCore.Qt.DisplayRole:
            return self.__listref()[index.row()].label()
        elif role == QtCore.Qt.CheckStateRole:
            return self.__curr == self.__listref()[index.row()]
        return None
        
    def setData(self, index, value, role):
        if role == QtCore.Qt.CheckStateRole:
            if self.__curr == self.__listref()[index.row()]:
                return False
            self.__curr = self.__listref()[index.row()]
            self.dataChanged.emit(self.createIndex(0,0),
                                  self.createIndex(self.rowCount(),0), 
                                  [QtCore.Qt.CheckStateRole])
#            self.selectionChanged.emit()
            return True
        else:
            return False
        
    def flags(self, index):
        return super(QListModel, self).flags(index) | QtCore.Qt.ItemIsUserCheckable
        
    def update(self):
        i1 = self.createIndex(0,0);
        iE = self.createIndex(self.rowCount(), self.columnCount())
        self.dataChanged.emit(i1, iE, [QtCore.Qt.DisplayRole])
        
    def current(self):
        return self.__curr if self.is_selected else None
      
    def is_selected(self):
        return self.__curr in self.__listref()

# -----------------------------------------------------------------------------

class QDDWindow(QtWidgets.QMainWindow):

    def __init__(self, app):
        super(QDDWindow, self).__init__()
        self.__app = app
        self.__init_ui()
        self.set_sources(Sources())
        self.__last_dir = os.path.curdir
        self.__model = None
#        self.__view = trajview.TrajectoriesView(self)
        self.__re_profile = re.compile('^profile(\d+)$')
        self.__plugins = []
        self.__laser_patterns = []

    def __cover_exception(function):
        def wrapper(self):
            try:
                return function(self)
            except Exception as e:
                self.__show_error(e)
    
        return wrapper
                
    @__cover_exception    
    def __load_image(self):
        filepaths,_ = QtWidgets.QFileDialog.getOpenFileNames(
            caption="Add Image", 
            filter="Image files (*.png *.pgm *.bmp)",
            directory=self.__last_dir)
        for filepath in filepaths:
            self.__sources.add_image(filepath)
        if len(filepaths) > 0:
            self.__last_dir = os.path.dirname(filepaths[0])
            self.__model_images.update()

            
    @__cover_exception    
    def __remove_image(self):
        idx = self.listImages.currentIndex().row()
        self.__sources.remove_image(idx)
        self.__model_images.update()

    
    @__cover_exception    
    def __load_ddbin(self):
        filepaths,_= QtWidgets.QFileDialog.getOpenFileNames(
            caption="Add Detector binary file", 
            filter="All supported files (*.bin *.cbor.bz2 *.cbor *.msf);;Dot Detector binary (*.bin );;Session files (*.cbor.bz2 *.cbor *.msf)",
            directory=self.__last_dir)
        for filepath in filepaths:
            self.__sources.add_ddbin(filepath)
        if len(filepaths) > 0:
            self.__last_dir = os.path.dirname(filepaths[0])
            self.__model_ddbins.update()

            
    @__cover_exception    
    def __remove_ddbin(self):
        idx = self.listImages.currentIndex().row()
        self.__sources.remove_ddbin(idx)
        self.__model_ddbins.update()

    
    @__cover_exception    
    def __load_profile(self):
        filepaths,_ = QtWidgets.QFileDialog.getOpenFileNames(
            caption="Add Profile", 
            filter="JSON file (*.json)",
            directory=self.__last_dir)
        for filepath in filepaths:
            with open(filepath, 'r') as f:
                data = json.load(f)
                if type(data) == list:
                    for p in data:
                        if type(p) != dict:
                            raise ValueError("Bad format of profile(s) in file %s" % filepath)
                        self.__sources.add_profile(p)
                elif type(data) == dict:
                    self.__sources.add_profile(data)
                else:
                    raise ValueError("Invalid format of file %s" % filepath)
                    
        if len(filepaths) > 0:
            self.__last_dir = os.path.dirname(filepaths[0])
            self.__model_profiles.update()

            
    @__cover_exception    
    def __remove_profile(self):
        idx = self.listProfiles.currentIndex().row()
        self.__sources.remove_profile(idx)
        self.__model_profiles.update()


    @__cover_exception    
    def __update_default_profile(self):
        curr_ddbin = self.__model_ddbins.current()

        if curr_ddbin:
            self.__sources.profiles()[0].profile = curr_ddbin.default_profile
        else:
            self.__sources.profiles()[0].profile = None

        self.__update_profile()

    
    @__cover_exception    
    def __update_profile(self):
        curr_ddbin = self.__model_ddbins.current()
        curr_profile = self.__model_profiles.current()

        profile = None
        if curr_ddbin:
            if curr_profile:
                if curr_profile.default:
                    profile = curr_ddbin.default_profile
                else:
                    profile = curr_profile.profile
            
            
        if profile:
            self.editProfile.setPlainText(json.dumps(profile, indent=4))
        else:
            self.editProfile.setPlainText("")
            
        self.btnSaveCurrProfile.setEnabled(not curr_profile is None 
                                           and not curr_profile.default)

    def __update_laser_patterns(self):
        # remove old ones
        
        last_checked = []
        for lw in self.__laser_patterns:
            widget = lw['widget']
            if widget.isChecked():
                last_checked.append(lw['pattern'])
            self.patternsLayout.removeWidget(widget)
            widget.setVisible(False)
            widget.setParent(None)
        
        self.__laser_patterns = []
        
        # add new ones
        
        det = self.__model_ddbins.current()
        if det is None:
            return
        
        for patidx in range(len(det.laser_patterns)):
            pat = det.laser_patterns[patidx]
            box = QtWidgets.QCheckBox("%d/%d" % (patidx, pat))
            box.setChecked(pat in last_checked)
            box.toggled.connect(self.__live_apply)
            self.patternsLayout.addWidget(box)
            self.__laser_patterns.append({'widget': box,
                                          'pattern': pat})
        
        self.__live_apply
        
    def __check_profile(self, profile):
        if type(profile) != dict or not 'name' in profile:
            raise RuntimeError("Bad format of profile")


    def __check_profile_names(self, profilename):
        for p in self.__sources.profiles():
            if p is None or p.default:
                continue
        
            if 'name' in p.profile and p.profile['name'] == profilename:
                raise RuntimeError("Profile with name %s is already exists. " % profilename)
        

    @__cover_exception    
    def __save_profile(self):
        profile = json.loads(self.editProfile.toPlainText())
        self.__check_profile(profile)
        
        curr_profile = self.__model_profiles.current()
        if profile['name'] != curr_profile.profile.get('name', ''):
            self.__check_profile_names(profile['name'])
        
        curr_profile.profile = profile

    
    def __add_profile(self, name):
        self.__check_profile_names(name)
                
        profile = json.loads(self.editProfile.toPlainText())
        profile['name'] = name
        
        print(profile)
        
        self.__sources.add_profile(profile)
        self.__model_profiles.update()
    
    @__cover_exception    
    def __add_prompt_profile(self):
        text, ok = QtWidgets.QInputDialog.getText(self, 'Save As New Profile', 
                                                        'New Profile Name:')
        if ok:
            self.__add_profile(str(text))

    
    @__cover_exception    
    def __add_auto_profile(self):
        no = 0
        
        for p in self.__sources.profiles():
            if p is None or p.default:
                continue
            res = self.__re_profile.match(p.profile['name'])
            if res:
                no = max(no, int(res.group(1)))
            
        self.__add_profile("profile%d" % (no+1))

    
    @__cover_exception    
    def __live_apply(self):
        if not self.checkLive.isChecked():
            return
        
        self.__apply()
    
    @__cover_exception    
    def __apply(self):
        im = self.__model_images.current()
        det = self.__model_ddbins.current()
        curr_profile = self.editProfile.toPlainText()
        if curr_profile == '':
            curr_profile = '{}'
        det_idxs = []
        for li in range(len(self.__laser_patterns)):
            if self.__laser_patterns[li]['widget'].isChecked():
                det_idxs.append(li)

        try:
            profile = json.loads(curr_profile)
            model = mkedd.process(im, det, profile, det_idxs)
            exportable = not (self.__model is None or im is None or det is None)
            self.actionExport.setEnabled(exportable)
            self.__set_model(model)
        except json.decoder.JSONDecodeError:
            self.__show_error("Bad format of profile!")
    
    def __set_model(self, model):
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        self.__model = model
#        self.__view.set_model(self.__model)
        for p in self.__plugins:
            try:
                p.set_model(model)
            except Exception:
                traceback.print_exc()

        QtWidgets.QApplication.restoreOverrideCursor()

    def reloadState(self):
        self.restoreState(self.settings.value("state",bytes()))
        for p in self.__plugins:
            if p.is_enabled():
                p.enable()
            else:
                p.disable()
      
#    def __update_plugins_menu(self):
#        menu = self.menuPlugins
#        for a in menu.actions():
#            a.setChecked(a.data().is_enabled())
        
    def __init_ui(self):
        self.settings = QtCore.QSettings("MagikEye", "MkEDDVis")
        self.restoreGeometry(self.settings.value("geometry",bytes()))
        
        # load UI
        script_dir = os.path.dirname(os.path.realpath(__file__))
        with open(os.path.join(script_dir, '../res/main.ui')) as f:
            uic.loadUi(f, self)
            
        # make connections
            
#        self.editProfile.textChanged.connect(self.__update_controls)

        ## actions

        self.actionExit.triggered.connect(self.__app.exit)

        ## buttons

        self.btnAddImage.clicked.connect(self.__load_image)
        self.btnAddDDbin.clicked.connect(self.__load_ddbin)
        self.btnAddProfile.clicked.connect(self.__load_profile)
        
        self.btnRemoveImage.clicked.connect(self.__remove_image)
        self.btnRemoveDDbin.clicked.connect(self.__remove_ddbin)
        self.btnRemoveProfile.clicked.connect(self.__remove_profile)
        
        self.btnSaveCurrProfile.clicked.connect(self.__save_profile)
        self.btnAddAsNewProfile.clicked.connect(self.__add_prompt_profile)
        self.btnAutoAddProfile.clicked.connect(self.__add_auto_profile)
        
        self.btnReloadDDbins.clicked.connect(self.__reload_ddbins)
        self.btnApply.clicked.connect(self.__apply)

        self.actionLoad.triggered.connect(self.__load_sources)
        self.actionSave.triggered.connect(self.__save_sources)
        
        self.actionExport.triggered.connect(self.__export_current)
        self.actionExport.setEnabled(False)
        
        self.actionAbout_Qt.triggered.connect(QtWidgets.QApplication.aboutQt)
        self.actionAbout_App.triggered.connect(self.__show_about)
        
#        self.menuPlugins.aboutToShow.connect(self.__update_plugins_menu)
        
    def dispatch_message(self, msg, *args, **kwargs):
        for p in self.__plugins:
            try:
                p.process_message(msg, *args, **kwargs)
            except Exception:
                traceback.print_exc()
            
    def set_sources(self, srcs):
        self.__sources = srcs
        self.__model_images = QListModel(self.__sources.images)
        self.__model_ddbins = QListModel(self.__sources.ddbins)
        self.__model_profiles = QListModel(self.__sources.profiles)
        self.__curr_profile = Profile("", True)
        self.listImages.setModel(self.__model_images)
        self.listDDBins.setModel(self.__model_ddbins)
        self.listProfiles.setModel(self.__model_profiles)
        
        self.__model_ddbins.dataChanged.connect(self.__update_default_profile)
        self.__model_ddbins.dataChanged.connect(self.__update_laser_patterns)
        self.__model_profiles.dataChanged.connect(self.__update_profile)

        self.__model_images.dataChanged.connect(self.__live_apply)
        # self.__model_ddbins.dataChanged.connect(self.__live_apply)
        # not necessary, will by called throu update_laser_profiles
        self.__model_profiles.dataChanged.connect(self.__live_apply)

    def add_plugin(self, plugin):
        self.__plugins.append(plugin)
        if isinstance(plugin, DockBasedPlugin):
            act = plugin.toggleViewAction()
            act.setText(plugin.name())
            self.menuPlugins.addAction(act)
#        else:
#            act = self.menuPlugins.addAction(plugin.name())
#            act.setCheckable(True)
#            act.setChecked(True)
#            act.triggered.connect(plugin.set_enabled)
#            act.setData(plugin)
        plugin.init_ui(self)
        plugin.set_model(self.__model)
    
    def closeEvent(self, event):
        self.settings.setValue("geometry", self.saveGeometry())
        self.settings.setValue("state", self.saveState())
        super(QDDWindow, self).closeEvent(event)

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Control:
            self.dispatch_message("CTRL_PRESS")

    def keyReleaseEvent(self, event):
        if event.key() == QtCore.Qt.Key_Control:
            self.dispatch_message("CTRL_RELEASE")

    @__cover_exception    
    def __load_sources(self):
        filepath,_ = QtWidgets.QFileDialog.getOpenFileName(
                        caption="Load sources", 
                        filter="JSON file (*.json)",
                        directory=self.__last_dir)
        if filepath and filepath != '':
            self.__last_dir = os.path.dirname(filepath)
            self.__sources.load(filepath)
            self.__model_images.update()            
            self.__model_ddbins.update()            
            self.__model_profiles.update()            
                        
    @__cover_exception    
    def __save_sources(self):
        filepath,_ = QtWidgets.QFileDialog.getSaveFileName(
                        caption="Save sources", 
                        filter="JSON file (*.json)",
                        directory=self.__last_dir)
        if filepath and filepath != '':
            self.__last_dir = os.path.dirname(filepath)
            self.__sources.save(filepath)        

    @__cover_exception    
    def __export_current(self):
        if self.__model is None:
            self.__show_error("No model opened")
        
        filename, _ = QtWidgets.QFileDialog.getSaveFileName(self, "Export current data","","JSON file (*.json)")
        if filename and len(filename) > 0:
            if filename.find('.') < 0:
                filename = filename + '.json'
            name, ok = QtWidgets.QInputDialog.getText(self, 'Export with name', 
                                                            'Experiment Name:')
            if ok:
                name = str(name)
            else:
                name = ""

            exported = self.__model.export_data(name)                
            
            with open(filename, 'w') as f:
                json.dump(exported, f, indent=4)

    @__cover_exception    
    def __reload_ddbins(self):
        if self.__sources:
            self.__sources.reload_ddbins()
        
    def __show_error(self, err):
        if type(err) == str:
            print(err)
        else:
            traceback.print_exc()
            
        QtWidgets.QMessageBox.critical(self, 'Error occured', str(err))
                
    def __show_about(self):
        QtWidgets.QMessageBox.about(self, "About " + Info.APPNAME, Info.APPDESC)
