
__author__ = "Ondra Fisar"
__copyright__ = "Copyright 2019 MagikEye Inc."

"""Version info
"""

# -----------------------------------------------------------------------------

import datetime
import socket
import os

import pymkedd

# -----------------------------------------------------------------------------

class Info(object):

    VERSION = '0.6.0'
    MKEDD_VERSION = '.'.join(str(i) for i in pymkedd.DepthSensor().getVersion())
    APPNAME = "MkEDDVis"    
    APPDESC = """<b>%s %s</b><br />
                 Utility for visualization results and internals of MkEDD detector<hr />
                 Copyright © MagikEye Inc. 2019<hr />
                 Using libmkedd %s""" % (APPNAME, VERSION, MKEDD_VERSION)
    
    @staticmethod
    def info(appname = None, version = None):
        info = {'timestamp': datetime.datetime.now().isoformat(),
                'hostname': socket.gethostname(),
                'system': ' '.join([str(i) for i in os.uname()]),
                'username': os.getlogin()
                }
        
        if appname:
            info['info'] = appname
    
        if version:
            info['version'] = version
            
        return info

