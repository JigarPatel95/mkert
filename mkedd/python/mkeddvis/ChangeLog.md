
# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) 
and this project adheres to [Semantic Versioning](http://semver.org/).


## [UNRELEASED]

### Added

- Live button
- RAW plugin to show RAW JSON data accordingly to choosed trajectory
- Color of the detection correspond to the z-coordinate
- Image histogram plugin
- Version of libmkedd is in about dialog now
- Tracking 2D/3D position by hovering mouse over accumulator

### Changed

- Classifier patches are sorted now accordingly to accumulator
- Classifier patches contain 2D position and guess of 3D position

### Fixed issue

- Detect mouse click from zoom/move click 

### Known issues

- Trajectory Details have a problem with multiple detectors (res vs pat indexing issue)

## [0.6.0] - 2019-06-20

### Fixed issues

- Not supported multiple detectors (patterns) in one bin file -> not supported FishEye yet

### Known issues

- Trajectory Details have a problem with multiple detectors (res vs pat indexing issue)

## [0.5.0] - 2019-06-19

### Added

- Plugins introduced
- 3D view
- Simple stats
- Properties view

### Known issues

- Not supported multiple detectors (patterns) in one bin file -> not supported FishEye yet

## [0.0.1] - ??

### Added

- Initial version
- Showing trajectories
- ...

### Known issues

- Not supported multiple detectors (patterns) in one bin file -> not supported FishEye yet
