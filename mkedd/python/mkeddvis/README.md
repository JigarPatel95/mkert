# MkEDDVis

Utility for visualization results and internals of MkEDD detector

## OS Requirements

- Application is well tested on *Linux* (especially *Ubuntu 18.04 LTS*).
- Requires *Python 3.5* and newer.
- Requires some external packages - see `requirements.txt`.
- Requires compiled Python bindings for `mkedd` library

## How to install/run

- You can store the utility wherever you want on your system
- Application requires extra python packages. Please install them by running `pip3 install -r requirements.txt` in application folder.
- Before you run the application you need to setup environment variable `PYTHONPATH` to the path where you *mkedd* python bindings (files: `_pymkedd.so` and `pymkedd.py`) is located.

## Recommendation: Make it simple to run

Application can run from different any directory. Since *Ubuntu* adds to system paths *bin* folder from you home you can place following script into your home directory (`/home/user/bin/mkeddvis`):

~~~~
#!/bin/bash

MKEDD_DIR=/path/to/mkert/mkedd
PYTHONPATH=${MKEDD_DIR}/build/python ${MKEDD_DIR}/python/mkeddvis/mkeddvis.py $@
~~~~
> Note: The script assume you run the application from *git* clone and you have your own build of *mkedd*.

## Changelog

A Changelog for the *mkeddvis* is located in project folder.
