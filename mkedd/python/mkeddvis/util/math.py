
__author__ = "Ondra Fisar"
__copyright__ = "Copyright 2019 MagikEye Inc."

"""Some math classes
"""

# -----------------------------------------------------------------------------

import numpy as np

# -----------------------------------------------------------------------------

class Plane:

    def __init__(self, normal=None, offset=None):
        self.normal = normal
        self.offset = offset
    
    # SVD fit
    # --------
    # > data[n,3] - 3d points 
    # < (Plane, distances)
    @staticmethod 
    def fit(data):
        centroid = np.mean(data, axis=0)
        N = data.shape[0]
        xyzR = data - np.tile(centroid, (N, 1))
        
        u, sigma, v       = np.linalg.svd(xyzR)
        normal            = v[2]
        normal            = normal / np.linalg.norm(normal)        
        normal            = normal * np.sign(normal[2])
        d = np.dot(-normal,centroid)
        
        err = np.matmul(data - np.tile(centroid, (N, 1)), normal)

        return (Plane(normal, d), err)
    
    # distance from the point(s) to the plane
    # --------
    # > pts[n,3] - 3d points
    # < distances
    def dist(self, pts):
        return np.abs((np.sum(pts*np.tile(self.normal, (pts.shape[0], 1)), axis=1)+self.offset[2])/np.linalg.norm(self.normal))
