
__author__ = "Ondra Fisar"
__copyright__ = "Copyright 2019 MagikEye Inc."

"""GUI utilities
"""

# -----------------------------------------------------------------------------

from matplotlib.backends.qt_compat import QtCore, QtGui, QtWidgets
import math

# --------------------------------------------------------------------------- #

class ContainerView(object):
    
#    def add_container(self, container):
#        pass
        
#    def remove_container(self, container):
#        pass
    
    def focus_container(self, container):
        pass
    
    def close_container(self, container):
        pass
    
    def show_context_menu(self, container):
        pass

# --------------------------------------------------------------------------- #

class Container(QtWidgets.QWidget):
#
#   class making title for traj charts
#    
    def __init__(self, label, container_view, parent=None, uuid=None, 
                 context_menu=None, data=None):
        super(Container, self).__init__(parent)
        self.__context_menu = context_menu
        self.__data = data
        
        # initialize signals

#        self.closeRequested = QtCore.pyqtBoundSignal(self)
#        self.focusRequested = QtCore.pyqtBoundSignal(self)
        
        # initialize ui
        
        self.setLayout(QtWidgets.QVBoxLayout())
        self.layout().setContentsMargins(0,0,0,0)
        self.layout().addWidget(self.__generate_title(label))
        
        self.container_view = container_view
        self.uuid = uuid
        
    def set_data(self, data):
        self.__data = data
    
    def data(self):
        return self.__data
        
    def __generate_title(self, label):
        title = QtWidgets.QWidget(self)
        title.setLayout(QtWidgets.QHBoxLayout())
        title.layout().addWidget(QtWidgets.QLabel(label))
        title.layout().addStretch()

        btnFocus = QtWidgets.QPushButton()
        btnFocus.setIcon(QtGui.QIcon.fromTheme("edit-find"))
        btnFocus.clicked.connect(self.__focus_me)
        title.layout().addWidget(btnFocus)
        
        if self.__context_menu:
            btnContext = QtWidgets.QPushButton()
            btnContext.setIcon(QtGui.QIcon.fromTheme("user-bookmarks"))
            btnContext.clicked.connect(self.__context_me)
            title.layout().addWidget(btnContext)
            self.__btnContext = btnContext

        btnClose = QtWidgets.QPushButton()
        btnClose.setIcon(QtGui.QIcon.fromTheme("window-close"))
        btnClose.clicked.connect(self.__close_me)
        title.layout().addWidget(btnClose)
        return title

    def __close_me(self):
        if self.container_view:
            self.container_view.close_container(self)
    
    def __focus_me(self):
        if self.container_view:
            self.container_view.focus_container(self)
            
    def __context_me(self):
        if self.__context_menu:
            self.__context_menu.popup(self.__btnContext.mapToGlobal(QtCore.QPoint(0,0)))
        
    def highlight(self):
        pass

# --------------------------------------------------------------------------- #

class QFloatSlider(QtWidgets.QSlider):

    floatValueChanged = QtCore.pyqtSignal(float, name='floatValueChanged')
    floatSliderMoved = QtCore.pyqtSignal(float, name='floatSliderMoved')
    
    def __init__(self, orientation, parent=None):
        super(QFloatSlider, self).__init__(orientation, parent)
        self.minv = super(QFloatSlider, self).minimum()
        self.maxv = super(QFloatSlider, self).maximum()
        self.step = 1
        
        self.valueChanged.connect(self.__updateValue)
        self.sliderMoved.connect(self.__updateSliderMoved)
        
    def setFloatRange(self, minv, maxv, step):
        self.minv = minv
        self.maxv = maxv
        self.step = step
        
        self.setMinimum(0)
        self.setMaximum(math.ceil((maxv-minv)/step))
        
    def setFloatValue(self, value):
        self.setValue(round((value-self.minv)/self.step))
        
    def floatValue(self):
        return self.__convValue(self.value())

    def __convValue(self, intval):
        return intval*self.step+self.minv
        
    @QtCore.pyqtSlot(int)
    def __updateValue(self, intval):
        value = self.__convValue(intval)
        self.floatValueChanged.emit(value)

    @QtCore.pyqtSlot(int)
    def __updateSliderMoved(self, intval):
        value = self.__convValue(intval)
        self.floatSliderMoved.emit(value)
        
