
__author__ = "Ondra Fisar"
__copyright__ = "Copyright 2019 MagikEye Inc."

"""Matplotlib extension for our purpose
"""

# -----------------------------------------------------------------------------

from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from PyQt5 import QtWidgets, QtCore

# -----------------------------------------------------------------------------
    
# Shorter toolbar for matplotlib

class ShortToolbar(NavigationToolbar):
    # given from https://stackoverflow.com/questions/12695678/how-to-modify-the-navigation-toolbar-easily-in-a-matplotlib-figure-window
    # only display the buttons we need

    toolitems = [t for t in NavigationToolbar.toolitems if
                 t[0] in ("Home", "Pan", "Zoom", "Save")]
    
        
    def __init__(self, *args, **kwargs):
        super(ShortToolbar, self).__init__(*args, **kwargs)

    def set_history_buttons(self):
        pass
    
# -----------------------------------------------------------------------------

# wrapper for layering
        
class MatplotLayer(object):
    
    def __init__(self, axis, name, icon=None, tooltip=None, visible=True):
        self.__axis = axis
        self.__artists = []
        self.__action = QtWidgets.QAction()
        self.__action.setCheckable(True)
        self.__action.setChecked(visible)
        if icon:
            self.__action.setIcon(icon)
        if tooltip:
            self.__action.setToolTip(tooltip)
        self.set_visible(visible)
        self.__action.toggled.connect(self.set_visible)
        
    def add(self, artist):
        self.__artists.append(artist)
        artist.set_visible(self.__visible)
        
    def remove(self, artist):
        artist.remove()
        self.__artists.remove(artist)
    
    def __contains__(self, item):
        return item in self.__artists
    
    def clear(self):
        while len(self.__artists)>0:
            self.remove(self.__artists[0])

    def set_visible(self, visibility):
        self.__visible = visibility

        for a in self.__artists:
            a.set_visible(visibility)
        
        self.draw()
    
    def draw(self):
        self.__axis.figure.canvas.draw()
#        for a in self.__artists:
#            a.draw()
            
    def toggleAction(self):
        return self.__action
    
    # propagation all not-defined calls to axis object
    def plot(self,*args,**kwargs):
        art, = self.__axis.plot(*args, **kwargs)
        self.add(art)
        return art

    def scatter(self,*args,**kwargs):
        art = self.__axis.scatter(*args, **kwargs)
        self.add(art)
        return art
    
    def imshow(self,*args,**kwargs):
        art = self.__axis.imshow(*args, **kwargs)
        self.add(art)
        return art
        