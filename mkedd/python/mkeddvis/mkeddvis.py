#!/usr/bin/env python3

__author__ = "Ondra Fisar"
__copyright__ = "Copyright 2019 MagikEye Inc."

"""Utility for analyzing behaviour of mkedd
"""

# -----------------------------------------------------------------------------

from PyQt5 import QtWidgets, QtCore, QtGui, uic
import sys
import os
import argparse
import json

from core.sources import Sources
from core.qddwin import QDDWindow
from core.version import Info

import plugins

# -----------------------------------------------------------------------------
                
class QApp(QtWidgets.QApplication):
            
    def __init__(self):
        super(QApp, self).__init__(sys.argv)

        QtCore.QCoreApplication.setOrganizationDomain("magik-eye.com")
        QtCore.QCoreApplication.setOrganizationName("MagikEye")
        QtCore.QCoreApplication.setApplicationName(Info.APPNAME)
        QtCore.QCoreApplication.setApplicationVersion(Info.VERSION)

        # set icon
        script_dir = os.path.dirname(os.path.realpath(__file__))
        self.setWindowIcon(QtGui.QIcon(os.path.join(script_dir, 'res/logo.svg')));

        self.sources = Sources()

        # parse command line
        self.parse()

        # start GUI        
        self.main = QDDWindow(self)
        self.main.set_sources(self.sources)
        for p in plugins.PLUGINS:    
            self.main.add_plugin(p())
        self.main.reloadState()
        self.main.show()
            
    def parse(self):
        arger = argparse.ArgumentParser(prog='mkeddvis')

        arger.add_argument('-v', '--verbose', action='count', default=0, help="Verbose mode")
        arger.add_argument('-i', '--images', metavar='N', type=str, nargs='+', default=[],
                           help="List of images")
        arger.add_argument('-b', '--ddbins', metavar='N', type=str, nargs='+', default=[],
                           help="List of mkedet files")
        arger.add_argument('-p', '--profile', type=str, default=None,
                           help="Profile or path to file with profile/s")
#        arger.add_argument('-e', '--execute', action='store_true', default=False, 
#                           help="Auto execute selection (should be loaded one possible choise)")
        arger.add_argument('-s', '--sources', type=str, default=None, 
                           help="Load sources form file")
                           
        opts = arger.parse_args()

        if opts.sources:
            self.sources.load(opts.sources)
        
        for im in opts.images:
            self.sources.add_image(im)

        for b in opts.ddbins:
            self.sources.add_ddbin(b)
            
        if opts.profile:
            profile = opts.profile
            if os.path.isfile(profile):
                with open(opts.profile, 'r') as f:
                    profile = json.load(f)
            
            if type(profile) == list:
                for p in profile:
                    self.source.add_profile(p)
            elif type(profile) == object:
                self.source.add_profile(p)
            else:
                raise RuntimeError("Invalid profile: " + str(profile))

# -----------------------------------------------------------------------------

if __name__ == "__main__":
    app = QApp()
    app.exec()
    

