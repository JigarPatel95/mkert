%module pymkedd

%include "typemaps.i"
%include "exception.i"

// ----------------------------------------------------------------------------

%pythonbegin %{
# This module provides wrappers for MkEDD (detection library)
# Copyright (c) 2018 MagikEye

import json
import numpy as np
%}

%{
#define SWIG_FILE_WITH_INIT
#include "../lib/nlohmann/json.hpp"
%}
%include "numpy.i"
%init %{
import_array();
%}

%include std_string.i
%inline %{
using namespace std;
%}

%numpy_typemaps(unsigned char, NPY_UBYTE, int)
%numpy_typemaps(double, NPY_DOUBLE, int)

// ----------------------------------------------------------------------------

%exception {
  try {
      $action
    } catch(const std::exception& ex) {
      SWIG_exception(SWIG_RuntimeError, ex.what());
  }
}

// ----------------------------------------------------------------------------

%typemap(out) nlohmann::json & {
  return PyString_FromString($1->dump().c_str());
}

%typemap(in) (const nlohmann::json &) (nlohmann::json jsonobj, PyObject * uniobj),  (nlohmann::json &) (nlohmann::json jsonobj, PyObject * uniobj) {

  uniobj = nullptr;
  if(PyString_Check($input))
  {
    jsonobj = nlohmann::json::parse(PyString_AsString($input));
    $1 = &jsonobj;
  }
  else if(PyUnicode_Check($input))
  {
    uniobj = PyUnicode_AsUTF8String($input);
    if(uniobj)
    {
      jsonobj = nlohmann::json::parse(PyString_AsString(uniobj));
      $1 = &jsonobj;
    }
    else
    {
      PyErr_SetString(PyExc_TypeError, "not a Unicode");
      SWIG_fail;
    }
  }
  else
  {
    PyErr_SetString(PyExc_TypeError, "not a String");
    SWIG_fail;
  }
}

%typemap(typecheck) const nlochmann::json & = char *;
%typemap(typecheck) nlochmann::json & = char *;

// ----------------------------------------------------------------------------

// mke::dd::DepthSensor

#define MKEDD_RESERVED_API 1

%{
#include "../src/mkedd.h"
%}

%include "../src/data_types.h"


%apply int {DepthSensor}

%inline %{
 typedef unsigned int uint32_t;
 typedef unsigned char uint8_t;
%}


%typemap(in, numinputs=0) (nlohmann::json &results) (nlohmann::json jsonobj) {
  $1 = &jsonobj;
}

%typemap(argout) (nlohmann::json & results) {
  $result = PyString_FromString($1->dump().c_str());
}


%typemap(in,
         fragment="NumPy_Fragments")
  (const uint8_t *data)
  (PyArrayObject* array=NULL, int is_new_object=0)
{
  npy_intp size[2] = { -1, -1 };
  array = obj_to_array_contiguous_allow_conversion($input,
                                                   NPY_UBYTE,
                                                   &is_new_object);
  if (!array || !require_dimensions(array, 2) ||
      !require_size(array, size, 2)) SWIG_fail;
 
  $1 = (uint8_t *) array_data(array);
}

%extend mke::dd::DepthSensor {

  const char * __str__()
  {
    static char tostr[256];
    uint32_t major, minor, patch;
    $self->getVersion(major, minor, patch);
    sprintf(tostr, "mke::dd::DepthSensor, v%d.%d.%d", major, minor, patch);
    return tostr;
  }

%rename(_getResults) getResults;
%rename(_parseCalibFile) parseCalibFile;
%rename(_getDebugData) getDebugData;
%rename(_getTriangulation) getTriangulation;
%rename(_process) process;

%pythoncode %{
  def getResults(self, det_idx):
     return json.loads(self._getResults(det_idx))

  def getTriangulation(self, det_idx):
     return json.loads(self._getTriangulation(det_idx))

  def getDebugData(self, det_idx):
     return json.loads(self._getDebugData(det_idx))

  def parseCalibFile(self, file_name):
     return json.loads(self._parseCalibFile(file_name))

  def process(self, det_idx, data):
     stride = self.getStride(det_idx)

     if ((stride != 0) and (stride > data.shape[1])):
         pad = np.zeros((data.shape[0], (stride - data.shape[1])), dtype=np.uint8)             
         data = np.hstack((data, pad))

     return self._process(det_idx, data)
%}
}


%apply uint32_t& OUTPUT {uint32_t& major, uint32_t& minor, uint32_t& patch};

%ignore mke::dd::DepthSensor::getDetections(const uint32_t det_idx, const PointData* &detections, uint32_t &detections_size);
%include "../src/mkedd.h"

// ----------------------------------------------------------------------------

// mke::dd::dd::Detector

%{
#include "../src/dd/detector.h"
#include "../src/dd/factory.h"
%}


%apply (double* IN_ARRAY1, int DIM1) {(const double* accum, int length)};

%typemap(in, numinputs=0) (nlohmann::json &data) (nlohmann::json jsonobj) {
  $1 = &jsonobj;
}

%typemap(argout) (nlohmann::json &data) {
  $result = PyString_FromString($1->dump().c_str());
}

%extend mke::dd::dd::Detector {

  const char * __str__()
  {
    static char tostr[256];
    sprintf(tostr, "mke::dd::dd::Detector %s", $self->getTypeString().c_str());
    return tostr;
  }

%newobject create;
  
  static Detector * create(const std::string &name)
  {
    return mke::dd::dd::Factory::create(name);
  }

%rename(_processAccum) processAccum;

%pythoncode %{
  def processAccum(self, arr):
     return json.loads(self._processAccum(arr))
%}
}

%include "../src/dd/detector.h"

// ----------------------------------------------------------------------------

// mke::dd::dd::Tester

%{
#include "../src/dd/tester.h"
%}

%extend mke::dd::dd::Tester {
  
%rename(_test) test;

%pythoncode %{
  @staticmethod
  def test(input):
     return json.loads(Tester._test(json.dumps(input)))
%}
}


%include "../src/dd/tester.h"
