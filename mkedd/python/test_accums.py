import pymkedd
import json

bl3 = pymkedd.Detector.create("BoxLut3")
arr = [1,2,3,4]
data = bl3.processAccum(arr)

print(data)

with open('accums.json', 'w') as f:
    json.dump(data, f)
