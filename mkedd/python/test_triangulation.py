import pymkedd
import imageio

im = imageio.imread('/tmp/im.png')
dd = pymkedd.DepthSensor()
dd.addCalibFile("/tmp/mkedet01_P016R07S000.bin")
dd.setDefaultProfile()
dd.process(0,im)

trig = dd.getTriangulation(0)
print(trig)
_trig = dd._getTriangulation(0)
print(_trig)
