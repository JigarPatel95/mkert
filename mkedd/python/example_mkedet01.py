import sys, os
from scipy import misc
import matplotlib.pyplot as plt
import json
import pymkedd

# =============================================================================

img_file = "../data/P010R04S001.png"
mkedet_file = "../data/mkedet01_P010R04S001.bin"

# =============================================================================

# Create detector object
mkedd = pymkedd.DepthSensor()
print("Detector: %s" % mkedd)

pathname = os.path.abspath(os.path.dirname(sys.argv[0]))
mkedet_file = pathname + '/' + mkedet_file

# Parse calib file
mkedet = mkedd.parseCalibFile(mkedet_file)

# Load image file
img_file = pathname + '/' + img_file
im = misc.imread(img_file)

# Plot paths
plt.imshow(im, cmap='gray')


paths = mkedet["MkEDetFile"]["detectors"][0]["paths"];
no_paths = len(paths)

for i in xrange(10):
    pels = paths[i]["pels_lut"]; 
    no_pels = len(pels)
    
    for j in xrange(no_pels):
       u = pels[j][0] 
       v = pels[j][1]
       plt.plot(u, v, marker=',', lw=0, linestyle="", color="r")

plt.show()

# =============================================================================
