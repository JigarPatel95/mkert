import sys, os
from scipy import misc
import matplotlib.pyplot as plt
import json
import pymkedd

# =============================================================================

img_file = '../data/P008R00S001.png'
mkedet_file = "../data/mkedet01_P008R00S001.bin"
det_idx = 0

profile = """{ "detector":{ "type": "BoxLut",  "classifier":  "CircLinear", "threshold": 8,  
               "peak_ratio": 1.2,  "curv_ratio": 1.15, "no_epochs": 3,  "box_delta": 2.0,  
               "no_threads": 4}, "postproc":{ "type" :  "PassThru"}}"""


# =============================================================================

# Create detector object
mkedd = pymkedd.DepthSensor()
print("Detector: %s" % mkedd)

pathname = os.path.abspath(os.path.dirname(sys.argv[0]))
mkedet_file = pathname + '/' + mkedet_file
img_file = pathname + '/' + img_file
print("Calibration file: %s" % mkedet_file)
print("Image file: %s" % mkedet_file)

# Add calibration file
mkedd.addCalibFile(mkedet_file)
# Set detection profile
mkedd.setProfile(profile)

# Process image
im = misc.imread(img_file)
mkedd.process(det_idx, im)

# Get results
res = mkedd.getResults(det_idx)
dbg = mkedd.getDebugData(det_idx)

mkedd.clearDetections(det_idx)

# Plot results
plt.imshow(im, cmap='gray')

no_dets = len(res["lut2d"])

for i in xrange(no_dets): 
   x = res["lut2d"][i][0] 
   y = res["lut2d"][i][1]
   plt.plot(x, y, marker='x')

plt.show()

# =============================================================================
