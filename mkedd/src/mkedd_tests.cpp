//#include <execinfo.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <iostream>
#include <fstream>
#include <stdexcept>
#include <string>

#include "util/timers.h"
#include "util/tester.h"

#include "json.hpp"
using nlohmann::json;

/*
 * https://chkno.net/memory-profiler-c++.html
 *
// Who called me?
void* caller()
{

  void *array[10];
   size_t size;

   // get void*'s for all entries on the stack
   size = backtrace(array, 10);

   // print out all the frames to stderr
   fprintf(stderr, "\n\nCaller: ");
   backtrace_symbols_fd(array, size, STDERR_FILENO);

    // The target is the caller's caller, so three functions back
    const int target = 3;
    void* returnaddresses[target];
    if (backtrace(returnaddresses, target) < target) {
        return NULL;
    }
    return returnaddresses[target-1];
}


void* operator new(size_t size) throw(std::bad_alloc) {
    void* ret = malloc(size);
    if (!ret) throw std::bad_alloc();
    fprintf(stderr, "allocate: %p %d bytes from %p\n", ret, size, caller());
    return ret;
}

void* operator new[] (size_t size) throw(std::bad_alloc) {
    void* ret = malloc(size);
    if (!ret) throw std::bad_alloc();
    fprintf(stderr, "allocate: %p %d bytes from %p\n", ret, size, caller());
    return ret;
}

void operator delete (void* data)
{
    free(data);
    fprintf(stderr, "free: %p\n", data);
}

void operator delete [] (void* data)
{
    free(data);
    fprintf(stderr, "free: %p\n", data);
}
*/

int main(int argc, char *argv[]) 
{
  char *fname = NULL;
  char *ofname = NULL;
  int c;

  try
  {
    while ((c = getopt(argc, argv, "i:o:")) != -1)
      switch (c)
        {
        case 'i':
          fname = optarg;
          break;
        case 'o':
          ofname = optarg;
          break;
        case '?':
          std::cerr << "Unknown option character" << c;
          return 1;

        default:
          abort();
        }
    
    if (!fname)
      throw std::runtime_error("Test file not set (-i)");

    json params, results;
    mke::dd::util::Tester tester;
    tester.processFromFile(fname, params, results);

    if (ofname)
      tester.saveToFile(ofname, params, results);
  }
  catch(std::exception& e)
  {
    std::cerr << "FATAL ERROR: " << e.what() << std::endl;
    return 1;
  }
}
