/*
 * mkedd - MkE depth sensor library
 *
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifndef _MKEDD_H_
#define _MKEDD_H_

#include "data_types.h"
#include <iostream>
#include <functional>

#ifdef MKEDD_RESERVED_API
#include "json.hpp"
#include "mkedd_reserved.h"
#endif


/**
 * \defgroup mkedd MkE Detector
 *
 */

namespace mke {
namespace dd {

/** \addtogroup mkedd
 *  @{
 */

class DepthSensor {
public:
  DepthSensor();
  ~DepthSensor();

  // ==========================================================================
  // Version
  void getVersion(uint32_t &major, uint32_t &minor, uint32_t &patch);

  // ==========================================================================
  // Parameter profiles
  void setProfile(const char * profile);
  std::string getProfile() const;
  void validateProfile(const char * profile) const;
  void setDefaultProfile(void);
  std::string getDefaultProfile(void) const;
  std::string getCurrentProfileSchema(void) const;

  // ==========================================================================
  // Runtime statistics
  void resetStats(void);
  std::string getStats(void) const;

  // ==========================================================================
  // Calibration data files
  void addCalibFile(const char *file_name);
  void clearCalibFiles(void);

  // ==========================================================================
  // File I/O
  typedef std::function<void(const char *, std::istream **is)> FileOpenCallback;
  typedef std::function<void(std::istream **is)> FileCloseCallback;

  void setCallback(const FileOpenCallback &fnc);
  void setCallback(const FileCloseCallback &fnc);
  
  // ==========================================================================
  // Processing
  uint32_t process(const uint32_t det_idx, const uint8_t *data);
  void getDetections(const uint32_t det_idx, const PointData* &detections, uint32_t &detections_size);
  void clearDetections(const uint32_t det_idx);

  // ==========================================================================
  // Info
  uint32_t getMaxNoDetections(void) const;
  uint32_t getNoDetections(const uint32_t det_idx) const;
  uint32_t getNoDetectors(void) const;
  uint32_t getLaserPattern(const uint32_t det_idx) const;
  uint32_t getStride(const uint32_t det_idx) const;
  Data3dType getData3dType(const uint32_t det_idx) const;

  // ==========================================================================
  // Reserved API
  #ifdef MKEDD_RESERVED_API
    // MKEDD_RESERVED_API_DECLARATIONS does not work with SWIG
    // MKEDD_RESERVED_API_DECLARATIONS

    void getResults(const uint32_t det_idx, nlohmann::json &results);
    void getDebugData(const uint32_t det_idx, nlohmann::json &results);
    void getTriangulation(const uint32_t det_idx, nlohmann::json &results);
    void parseCalibFile(const char *file_name, nlohmann::json &results);    
  #endif

private:
  
  DepthSensor(const DepthSensor&);              // noncopyable
  DepthSensor& operator=(const DepthSensor&);   //

  class Impl;  // Forward declaration of the implementation class
  Impl *impl_; // PIMPL
};


/** @}*/

} /* namespace dd */
} /* namespace mke */


#endif // _MKEDD_H_
