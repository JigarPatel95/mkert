/*
 * AVX2 bits - simple layer over AVX2 intrinsics
 *
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#pragma once

#include <iostream>
#include <immintrin.h>
#include <cstdint>

namespace mke {
namespace dd {
namespace sse {

class bit256_t {
protected:
  __m256i val;

public:
  bit256_t()
  {}

  // ========================================================================

  bit256_t& operator= (__m256i const &x)
  {
      val = x;
      return *this;
  }

  // ========================================================================

  operator __m256i() const
  {
    return val;
  }

  // ========================================================================

  bit256_t(__m256i const & x)
  {
    val = x;
  }

  // ========================================================================
  // ========================================================================

  bit256_t& load(void const * ptr)
  {
      val = _mm256_load_si256((__m256i const*) ptr);
      return *this;
  }

  // ========================================================================

  bit256_t& loadu(void const * ptr)
  {
      val = _mm256_loadu_si256((__m256i const*) ptr);
      return *this;
  }

  // ========================================================================

  void store(void * ptr) const
  {
      _mm256_store_si256((__m256i*) ptr, val);
  }

  // ========================================================================

};

static inline bit256_t operator& (bit256_t const & a, bit256_t const & b)
{
  return _mm256_and_si256(a, b);
}

// ========================================================================
// ========================================================================

class int32x8_t : public bit256_t {
public:
    int32x8_t()
    {}

    // ========================================================================

    int32x8_t(void const *ptr, bool unaligned = true)
    {
      if (unaligned)
        loadu(ptr);
      else
        load(ptr);
    }

    // ========================================================================

    int32x8_t(int32_t i) {
        val = _mm256_set1_epi32(i);
    }

    // ========================================================================

    int32x8_t(int32_t i0, int32_t i1, int32_t i2, int32_t i3,
              int32_t i4, int32_t i5, int32_t i6, int32_t i7)
    {
        val = _mm256_setr_epi32(i0, i1, i2, i3, i4, i5, i6, i7);
    }

    // ========================================================================
    // ========================================================================

    int32x8_t& operator= (__m256i const &x)
    {
        val = x;
        return *this;
    }

    // ========================================================================

    operator __m256i() const
    {
      return val;
    }

    // ========================================================================

    int32x8_t(__m256i const & x)
    {
      val = x;
    }

    // ========================================================================

    int32x8_t shuffle(const uint8_t s0, const uint8_t s1, const uint8_t s2, const uint8_t s3)
    {
      return _mm256_shuffle_epi32(val, _MM_SHUFFLE(s0, s1, s2, s3));
    }

    // ========================================================================

    int32_t operator [] (uint32_t index) const {
        int32_t x[8] __attribute__((aligned(32)));
        store(x);
        return x[index & 7];
    }

    // ========================================================================
};

// ============================================================================
// ============================================================================

inline std::ostream& operator<<(std::ostream& stream, const int32x8_t& a)
{
  int v[8] __attribute__((aligned(32)));
  a.store(v);

  return stream << "int32x8_t[" << v[0] << ", " << v[1] << ", " << v[2] << ", " << v[3] << ", "
                                << v[4] << ", " << v[5] << ", " << v[6] << ", " << v[7] << "]";
}

// ============================================================================

static inline int32x8_t operator+ (int32x8_t const &a, int32x8_t const &b)
{
  return _mm256_add_epi32(a, b);
}

// ============================================================================

static inline int32x8_t operator- (int32x8_t const &a, int32x8_t const &b)
{
  return _mm256_sub_epi32(a, b);
}

// ============================================================================

static inline int32x8_t operator* (int32x8_t const &a, int32x8_t const &b)
{
  return _mm256_mullo_epi32(a, b);
}

// ============================================================================

static inline bit256_t operator> (int32x8_t const &a, int32x8_t const &b)
{
  return _mm256_cmpgt_epi32(a, b);
}

// ============================================================================

static inline int32x8_t& operator += (int32x8_t &a, int32x8_t const &b) {
  a = a + b;
  return a;
}

// ============================================================================
// ============================================================================

static inline int32x8_t max(int32x8_t const &a, int32x8_t const &b) {
    return _mm256_max_epi32(a, b);
}

// ============================================================================

static inline int32x8_t min(int32x8_t const &a, int32x8_t const &b) {
    return _mm256_min_epi32(a,b);
}

// ============================================================================

static inline int32x8_t clip_value(int32x8_t const &val, int32x8_t const &lb, int32x8_t const &ub)
{
  return min(max(val, lb), ub);
}

// ============================================================================

static inline int32x8_t keep_if_gt(int32x8_t const &val, int32x8_t const &lb, int32x8_t const &ub)
{
  bit256_t mask = (lb > ub);
  return _mm256_and_si256(val, mask);
}

// ============================================================================

}
}
}
