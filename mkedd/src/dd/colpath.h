/*
 * CollatedPath
 *
 * Copyright (c) 2018, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#ifndef _DD_COLLATEDPATH_H_
#define _DD_COLLATEDPATH_H_

#include <cstdint>
#include <vector>

#include <boost/align/aligned_allocator.hpp>
#include <boost/dynamic_bitset.hpp>
#include "util/bitset.h"

#include "data_types.h"
#include "dd/pathels.h"

#include "dd/pxlist.h"

namespace mke {
namespace dd {
namespace dd {

/** \addtogroup mkedd
 *  @{
 */

// ==============================================================================
// ==============================================================================

class CollatedLitePath {
public:
  typedef std::vector<PathElementLiteLut, boost::alignment::aligned_allocator<PathElementLiteLut, 32>> PelsVector;

private:
  uint32_t uid_, id1_, id2_;
  uint16_t lmin_, lmax_;
  mke::dd::util::BitSet mask_;
  PelsVector pels_;
  std::vector<uint32_t> alist_;

// ============================================================================

public:

  inline void setIds(const uint32_t uid, const uint32_t id1, const uint32_t id2)
  {
    uid_ = uid;
    id1_ = id1;
    id2_ = id2;
  }

  // ==========================================================================

  inline uint32_t getUid(void) const
  {
    return uid_;
  }

  // ==========================================================================

  inline void setBounds(const uint16_t lmin, const uint16_t lmax)
  {
    lmin_ = lmin;
    lmax_ = lmax;
  }

  // ==========================================================================

  inline uint16_t getLowerBound(void) const
  {
    return lmin_;
  }

  // ==========================================================================

  inline uint16_t getUpperBound(void) const
  {
    return lmax_;
  }

  // ==========================================================================

  inline void setMask(std::vector<uint8_t> &&mask, uint32_t no_pels = 0)
  {
    mask_.set(std::move(mask), no_pels);
  }

  // ==========================================================================

  inline void setPathElementsLut(const PelsVector &pels)
  {
    pels_ = pels;
  }

  // ==========================================================================

  inline void setPathElementsLut(PelsVector &&pels)
  {
    pels_.clear();
    pels_.swap(pels);
  }

  // ==========================================================================

  inline uint32_t getNumPathElements(void) const
  {
    return pels_.size();
  }

  // ==========================================================================

  inline void setPathElement(PointDataLut &pel, uint32_t idx) const
  {
    const auto &ipel = pels_[idx];

    pel.uid = uid_;
    pel.id1 = id1_;
    pel.id2 = id2_;

    pel.x = ipel.x;
    pel.y = ipel.y;
    pel.z = ipel.z;

    pel.u = (uint32_t(ipel.ui) << 16) | (uint32_t(ipel.uvf) & 0xFF00);
    pel.v = (uint32_t(ipel.vi) << 16) | ((uint32_t(ipel.uvf) & 0xFF) << 8);
  }

  // ==========================================================================

  inline const PathElementLiteLut& getPathElement(uint32_t idx) const
  {
    return pels_[idx];
  }

  // ==========================================================================

  inline const mke::dd::util::BitSet& getMask(void) const
  {
    return mask_;
  }

  // ==========================================================================

  inline void setNeighbors(std::vector<uint32_t> &&alist)
  {
    alist_ = alist;
  }

  // ==========================================================================

  inline const std::vector<uint32_t> getNeighbors(void) const
  {
    return alist_;
  }

};

// ==============================================================================
// ==============================================================================

class CollatedPath {
private:

  uint32_t uid_, lid_, did_;

  PathPixelList pixellist_;

  uint16_t lmin_, lmax_;
  std::vector<uint8_t> mask_;

  std::vector<PointDataLut> pels_;

// ============================================================================

public:

  inline void setIds(const uint32_t uid, const uint32_t lid, const uint32_t did)
  {
    uid_ = uid;
    lid_ = lid;
    did_ = did;
  }

  // ==========================================================================

  inline void setBounds(const uint16_t lmin, const uint16_t lmax)
  {
    lmin_ = lmin;
    lmax_ = lmax;
  }

  // ==========================================================================

  inline uint16_t getLowerBound(void) const
  {
    return lmin_;
  }

  // ==========================================================================

  inline uint16_t getUpperBound(void) const
  {
    return lmax_;
  }

  // ==========================================================================

  inline void setMask(const std::vector<uint8_t> &mask, uint32_t no_pels = 0)
  {
    if (no_pels)
      {
        // This is a bit mask
        boost::dynamic_bitset<uint8_t> bitmask;
        bitmask.resize(mask.size() * 8);
        boost::from_block_range(mask.begin(), mask.end(), bitmask);

        mask_.resize(no_pels);
        for (size_t i = 0; i < no_pels; i++)
          mask_[i] = bitmask[i];
      }
    else
      {
        mask_ = mask;
      }
  }

  // ==========================================================================

  inline void setMask(std::vector<uint8_t> &&mask)
  {
    mask_.clear();
    mask_.swap(mask);
  }

  // ==========================================================================

  inline void setPixelList(const std::vector<PathPixelInfo> &pixellist, const uint32_t stride)
  {
    pixellist_.set(pixellist, stride);
  }

  // ==========================================================================

  inline void setPathElementsLut(const std::vector<PointDataLut> &pels)
  {
    pels_ = pels;
  }

  // ==========================================================================

  inline void setPathElementsLut(std::vector<PointDataLut> &&pels)
  {
    pels_.clear();
    pels_.swap(pels);
  }

  // ==========================================================================

  inline uint32_t getNumPathElements(void) const
  {
    return pels_.size();
  }

  // ==========================================================================

  inline const PointDataLut& getPathElement(uint32_t idx) const
  {
    return pels_[idx];
  }

  // ==========================================================================

  inline const PathPixelList& getPixelList(void) const
  {
    return pixellist_;
  }

  // ==========================================================================

  inline const std::vector<uint8_t>& getMask(void) const
  {
    return mask_;
  }

};


/** @}*/

} // namespace dd
} // namespace dd
} // namespace mke

#endif // _DD_COLLATEDPATH_H_
