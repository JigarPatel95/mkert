/*
 * InterpolatedRay
 *
 * Copyright (c) 2018, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#ifndef _DD_INTERPOLATED_RAY_H_
#define _DD_INTERPOLATED_RAY_H_

#include <array>

class InterpolatedRay {
public:
    typedef std::array<uint32_t, 3> tri_t;

    // ==========================================================================

private:
    uint32_t uid_;
    tri_t tri_;

    // ==========================================================================

public:

    void setId(const uint32_t uid)
    {
      uid_ = uid;
    }

    // ==========================================================================

    uint32_t& getId(void)
    {
      return uid_;
    }

    // ==========================================================================

    tri_t& getTriangulation(void)
    {
      return tri_;
    }

    // ==========================================================================


};

#endif // _DD_INTERPOLATED_RAY_H_
