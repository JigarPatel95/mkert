/*
 * Tester
 *
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */


#ifdef MKEDD_RESERVED_API

#ifndef _DD_TESTER_H_
#define _DD_TESTER_H_

#include "json.hpp"

namespace mke {
namespace dd {
namespace dd {

/** \addtogroup mkedd
 *  @{
 */

class Tester {

public:

  static void process(const nlohmann::json &params, nlohmann::json &results);

  static void nonMaximaSuppression(const nlohmann::json &params, nlohmann::json &results);
  static void convolution(const nlohmann::json &params, nlohmann::json &results);
  static void peaks1D(const nlohmann::json &params, nlohmann::json &results);
};

/** @}*/

} // namespace dd
} // namespace cc
} // namespace mke

#endif // _DD_TESTER_H_

#endif // MKEDD_RESERVED_API
