/*
 * SessionWrapper
 * 
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifndef _DD_SESSION_WRAPPER_H_
#define _DD_SESSION_WRAPPER_H_

#ifdef MKEDD_USE_MKECC

#include <stdexcept>
#include <vector>
#include <array>

#include "json.hpp"
#include "mkecc/session/session.h"
#include "mkecc/camera/camera.h"

namespace mke {
namespace dd {
namespace dd {
  
/** \addtogroup mkedd
 *  @{
 */

class SessionWrapper {
public:
  std::shared_ptr<mke::cc::session::Session> session_;

  // Laser pattern
  std::vector<uint32_t> laser_pattern_;

  // Rays
  nlohmann::json ray_groups_;
  std::vector<std::array<double, 3>> ray_orgs_;
  std::vector<std::array<double, 3>> ray_dirs_;
  std::vector<uint32_t> ids_;

  uint32_t no_doe_groups_;

  // Camera & 2D info
  nlohmann::json camera_model_;
  mke::cc::camera::Camera camera_;
  uint32_t stride_;
  uint32_t iwidth_;
  uint32_t iheight_;

  // 3D info
  uint32_t depth_min_;
  uint32_t depth_max_;

  // Results
  uint32_t det_idx_;
  uint32_t max_detections_;

  void setSession(std::shared_ptr<mke::cc::session::Session> session);
  void setDetIdx(const int det_idx);
  int getNumDoeGroups(void) const;
  const nlohmann::json& getInfoData(void) const;
  uint32_t getLaserPattern(const int det_idx) const;
};


/** @}*/

} /* namespace dd */
} /* namespace dd */
} /* namespace mke */

#endif // MKEDD_USE_MKECC
#endif // _DD_SESSION_WRAPPER_H_
