/*
 * PixelList
 *
 * Copyright (c) 2018, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifndef _DD_PIXELLIST_H_
#define _DD_PIXELLIST_H_

#include <cstdint>
#include <vector>

#include <boost/iterator/iterator_facade.hpp>

namespace mke {
namespace dd {
namespace dd {

/** \addtogroup mkedd
 *  @{
 */

// ============================================================================

struct PathPixelInfo {
  uint16_t u, v;
  uint16_t j;
  uint16_t w;
};

// ============================================================================

struct PathPixelStepInfo {
  uint16_t step;
  uint16_t j;
  uint16_t w;
};

// ============================================================================

class PathPixelList {
private:
 std::vector<PathPixelStepInfo> pixellist_;
 uint32_t buffer_offset_;

public:

  inline void set(const std::vector<PathPixelInfo> &pixellist, const uint32_t stride)
  {
    pixellist_.clear();

    int no_pxs = pixellist.size();

    if (!no_pxs)
      {
        buffer_offset_ = 0;
        return;
      }

    pixellist_.reserve(no_pxs);

    const auto &pi = pixellist[0];
    buffer_offset_ =  uint32_t(pi.v) * stride + uint32_t(pi.u);

    uint32_t pre_offset = buffer_offset_;
    uint32_t cur_offset;

    for (int i = 0; i < no_pxs; i++)
      {
        const auto &pi = pixellist[i];

        cur_offset = uint32_t(pi.v) * stride + uint32_t(pi.u);

        pixellist_[i].step = uint16_t(cur_offset - pre_offset);
        pre_offset = cur_offset;

        pixellist_[i].j = pi.j;
        pixellist_[i].w = pi.w;
      }
  }

  // ==========================================================================

  inline void set(std::vector<PathPixelStepInfo> &&pixellist, const uint32_t buffer_offset)
  {
    buffer_offset_ = buffer_offset;
    pixellist_.clear();
    pixellist_.swap(pixellist);
  }

  // ==========================================================================

  inline size_t size(void) const
  {
    return pixellist_.size();
  }

  // ==========================================================================

  inline const std::vector<PathPixelStepInfo>& getStepInfoList(void) const
  {
    return pixellist_;
  }

  // ==========================================================================

  inline uint32_t getBufferOffset(void) const
  {
    return buffer_offset_;
  }
};

// ============================================================================


/** @}*/

} // namespace dd
} // namespace cc
} // namespace mke

#endif // _DD_PIXELLIST_H_
