/*
 * SiftGeom: SIFT based Geometrical detector
 *
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifdef MKEDD_USE_MKECC

#include <exception>
#include <iostream>
#include <cmath>
#include <math.h>
#include <limits>

#include "types/convert.h"
#include "types/fpreal.h"
#include "dd/siftgeom.h"
#include "dc/factory.h"

#include "mkecc/dets/factory.h"

using namespace mke::dd::dd;
using json = nlohmann::json;
//using fpreal16 = mke::dd::types::fpreal16;
//using fhreal4r = mke::dd::types::fhreal4r;
using namespace mke::dd::types;

// ============================================================================
// ============================================================================
// Parameters

const json SiftGeomDetector::param_schema_ = R"(
    {
      "type": "object",
      "properties" : {
        "type" : {
          "description" : "Detector type",
          "type" : "string",
          "enum" : ["SiftGeom"],
          "default" : "SiftGeom"
        },
        "max_angular_error" : {
          "description" : "Maximum rect/dot angle for a reconstructed point to be valid [deg]",
          "type" : "number",
          "maximum" : 10,
          "exclusiveMinimum": 0,
          "default" : 2
        },
        "dot_detector" : {
          "description" : "Mkecc dot detector type",
          "type" : "string",
          "enum" : ["OcvSiftDotDetector"],
          "default" : "OcvSiftDotDetector"
        },
        "dot_detector_params" : {
          "description" : "Mkecc dot detector parameters",
          "type" : "object",
          "default" : {}
        },
        "classifier" : {
          "description" : "2D dot classifier type",
          "type" : "string",
          "enum" : ["PassThru", "CircLinear", "ExCircLinear", "NgCircLinear"],
          "default" : "PassThru"
        },
        "classifier_params" : {
          "description" : "2D dot classifier parameters",
          "type" : "object",
          "default" : {}
        },
        "data3d_type" : {
          "description" : "Format of output 3D data",
          "type" : "string",
          "enum" : ["DD_DATA3D_MM", "DD_DATA3D_MM2", "DD_DATA3D_MM4", "DD_DATA3D_MM8", "DD_DATA3D_MM16"],
          "default" : "DD_DATA3D_MM16"
        }
      }
    }
  )"_json;

#ifdef MKEDD_RESERVED_API
const json SiftGeomDetector::reserved_param_schema_ = R"SCHEMA(
        {
          "type": "object",
            "properties" : {
              "store_dbg_data" : {
                "type" : "boolean",
                "default" : true
              }
            }
          }
       )SCHEMA"_json;
#endif

// ============================================================================

void SiftGeomDetector::updateParams(const nlohmann::json &params, bool value_only)
{
  if (params.contains("max_angular_error"))
    max_angular_error_ = std::cos(pmap_.getParam("max_angular_error").get<double>() * M_PI / 180.0);

  // Mkecc dot detector
  if (params.contains("dot_detector"))
    detector_.reset(mke::cc::dets::Factory::create(pmap_.getParam("dot_detector").get<std::string>()));

  if (detector_.get() && params.contains("dot_detector_params"))
    detector_->setParams(params.at("dot_detector_params"));

  // 3D data type
  if (params.contains("data3d_type"))
    data3d_type_param_ = ConvertToData3dType(pmap_.getParam("data3d_type").get<std::string>());

  // 2D dot classifier
  if (params.contains("classifier"))
    {
      classifier_.reset(dc::Factory::create(pmap_.getParam("classifier").get<std::string>()));
      if (!value_only)
        classifier_->setFrameParams(iwidth_, iheight_, stride_);
    }

  if (classifier_.get() && params.contains("classifier_params"))
    classifier_->setParams(params.at("classifier_params"));

#ifdef MKEDD_RESERVED_API
  if (params.contains("store_dbg_data"))
    store_dbg_data_ = pmap_.getParam("store_dbg_data").get<bool>();
#endif

/*
#ifdef MKEDD_MULTITHREADING
  if (params.contains("no_threads"))
    {
      no_threads_param_ = uint32_t(pmap_.getParam("no_threads").get<double>());
      threadpool_.setNoThreads(no_threads_param_);
      if (!value_only)
        setupPathsSlices(no_threads_param_);
    }
#else
  no_threads_param_ = 1;
#endif
*/
}

// ============================================================================

void SiftGeomDetector::validateParams(const nlohmann::json &params)
{
  pmap_.validateParams(params);

  if (detector_.get() && (params.find("dot_detector_params") != params.end()))
    detector_->validateParams(params);

  if (classifier_.get() && (params.find("classifier_params") != params.end()))
    classifier_->validateParams(params);  
}

// ============================================================================

nlohmann::json SiftGeomDetector::getParamsSchema(void) const
{
  return pmap_.getSchema();
}

// ============================================================================

void SiftGeomDetector::setParams(const nlohmann::json &params)
{
  pmap_.validateParams(params);
  pmap_.setParams(params);
  updateParams(params);
}

// ============================================================================

void SiftGeomDetector::getParams(nlohmann::json &params) const
{
  params = pmap_.getParams();
  json classifier_params;
  json detector_params;

  // Dot classifier parameters
  if (classifier_.get())
    {
      classifier_->getParams(classifier_params);

      if (classifier_params.is_null())
        classifier_params = json::object();

      params["classifier_params"].swap(classifier_params);
    }

  // Dot detector parameters
  if (detector_.get())
    {
      detector_params = detector_->getParams();

      if (detector_params.is_null())
        detector_params = json::object();

      params["dot_detector_params"].swap(detector_params);
    }
}

// ============================================================================

const std::string SiftGeomDetector::getTypeString(void)
{
  return staticGetTypeString();
}

// ============================================================================
// ==========================================================================
// Runtime statistics

void SiftGeomDetector::resetStats(void)
{
  user_timer_.reset();
  ddet_timer_.reset();
  fproj_timer_.reset();
  rect3d_timer_.reset();
}

// ============================================================================

void SiftGeomDetector::getStats(nlohmann::json &stats) const
{
#ifndef __nios2_arch__
  stats["user_timer_s"] = user_timer_.elapsed<double, std::ratio<1>>();
  stats["ddet_timer_s"] = ddet_timer_.elapsed<double, std::ratio<1>>();
  stats["fproj_timer_s"] = fproj_timer_.elapsed<double, std::ratio<1>>();
  stats["rect3d_timer_s"] = rect3d_timer_.elapsed<double, std::ratio<1>>();
#endif
}

// ============================================================================
// ============================================================================
// Data

void SiftGeomDetector::setInitValues(void)
{
  // N/A
}

// ============================================================================
// ============================================================================
// Data loading

void SiftGeomDetector::load(std::istream *is, const SectionType section)
{
  if (section != DD_SECTION_MSF)
    throw std::runtime_error("[MkEDD] SiftGeom: Unable to parse detector section: " + std::to_string(section));

  try
  {
    if (section == DD_SECTION_MSF)
      {
        // N/A, data set via SessionWrapper interface
      }
  }
  catch (std::exception &e)
  {
    setInitValues();
    throw;
  }
}

// ============================================================================

bool SiftGeomDetector::canParse(const SectionType section)
{
  if (section == DD_SECTION_MSF)
    return true;
  else
    return false;
}

// ============================================================================
// ============================================================================
// Processing

template <typename T>
T dot(const std::array<T, 3> &a, const std::array<T, 3> &b)
{
  return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
}

// ============================================================================

template <typename T>
T norm(const std::array<T, 3> &a)
{
  return std::sqrt(dot(a, a));
}

// ============================================================================

template <typename T>
std::array<T, 3> operator+(const std::array<T, 3> &a, const std::array<T, 3> &b)
{
  std::array<T, 3> sum;
  sum[0] = a[0] + b[0];
  sum[1] = a[1] + b[1];
  sum[2] = a[2] + b[2];

  return sum;
}

// ============================================================================

template <typename T>
std::array<T, 3> operator-(const std::array<T, 3> &a, const std::array<T, 3> &b)
{
  std::array<T, 3> diff;
  diff[0] = a[0] - b[0];
  diff[1] = a[1] - b[1];
  diff[2] = a[2] - b[2];

  return diff;
}

// ============================================================================

template <typename T>
std::array<T, 3> operator*(const T &c, const std::array<T, 3> &a)
{
  std::array<T, 3> ca;

  ca[0] = c * a[0];
  ca[1] = c * a[1];
  ca[2] = c * a[2];

  return ca;
}

// ============================================================================

template <typename T>
bool isazero(const T val, const T delta = 100)
{
  return (+val < (delta * std::numeric_limits<T>::epsilon())) &&
         (-val < (delta * std::numeric_limits<T>::epsilon()));
}

// ============================================================================

template <typename T>
void print(const std::string &name, const std::array<T, 3> &a)
{
  std::cout << name << " = [" << a[0] << ", " << a[1] << ", " << a[2] << "]" << std::endl;
}

// ============================================================================

template <typename T>
bool reconstruct3D(const std::array<T, 3> &a, const std::array<T, 3> &b,
              const std::array<T, 3> &c, const std::array<T, 3> &d,
              std::array<T, 3> &m, T &t, T &s)
{
  T bd = dot(b, d);
  T denom = 1 - bd * bd;

  if (isazero(denom))
    return false;

  std::array<T, 3> amc = a - c;
  std::array<T, 3> cma = c - a;
  T cmab = dot(cma, b);
  T amcd = dot(amc, d);

  t = (cmab + amcd * bd) / denom;
  s = (amcd + cmab * bd) / denom;

  m = c + s * d;

  return true;
}

// ============================================================================

uint32_t SiftGeomDetector::process(const uint8_t *data, std::vector<PointData>& detections)
{
  user_timer_.start();

#ifdef MKEDD_RESERVED_API
  classifier_->initDebugData();

  if (store_dbg_data_)
    dbgdata_.clear();
#endif

  int no_rays = ray_orgs_.size();
  no_detections_ = 0;

  // Dot detector
  ddet_timer_.start();

  mke::cc::gil::ImageView data_view;
  data_view = mke::cc::gil::ImageView(boost::gil::interleaved_view(stride_, iheight_,
                                         (boost::gil::gray8_pixel_t*) data, stride_));

  json dots_json_;
  detector_->process(data_view, dots_json_);

  ddet_timer_.stop();

  // Dots -> directions
  fproj_timer_.start();


  int no_dots = dots_json_.size();
  dots_.resize(no_dots);
  dot_dirs_.resize(no_dots);

  for (int i = 0; i < no_dots; i++)
    {
      const auto &dot = dots_json_[i];
      dots_[i][0] = dot[0].get<double>();
      dots_[i][1] = dot[1].get<double>();
    }

  camera_.forwardProject((double *) dots_.data(), (double *) dot_dirs_.data(),
                         no_dots, mke::cc::camera::FPROJECTION_NORMALIZED_TO_ONE);

#ifdef MKEDD_RESERVED_API
  if (store_dbg_data_)
    dbgdata_["dots"].swap(dots_json_);
#endif

  fproj_timer_.stop();

  // Detections initialization
  rect3d_timer_.start();

  for (int i = 0; i < no_rays; i++)
    {
      detections[i].confidence = 0;
      detections[i].id1 = 0;
    }

  // 3D reconstruction
  std::array<double, 3> orig = {0.0, 0.0, 0.0};
  std::array<double, 3> pt3d = {0.0, 0.0, 0.0};
  std::array<double, 3> ray_pt3d = {0.0, 0.0, 0.0};
  double t, s;

  for (int i = 0; i < no_dots; i++)
    {
      double max_ddot = -1.0;
      int ray_idx = -1;
      auto &dot_dir = dot_dirs_[i];

      // Find the ray that explains this dot projection the best
      for (int j = 0; j < no_rays; j++)
        {
          bool has_point = reconstruct3D(orig, dot_dir, ray_orgs_[j], ray_dirs_[j], pt3d, t, s);

          // Do we have a candidate 3D reconstruction?
          if (!has_point)
            continue;

          // Are we in range?
          t = norm(pt3d);

          if ((t < depth_min_) || ( t > depth_max_))
            continue;

          // Do we have a better ray candidate?
          double ddot = dot(dot_dir, (1.0 / t) * pt3d);

          if (max_ddot < ddot)
            {
              ray_idx = j;
              ray_pt3d = pt3d;
              max_ddot = ddot;
            }
        }

      // Do we have a candidate ray?
      if (ray_idx == -1)
        continue;

      // Is it within the allowed angular error?
      if (max_ddot < max_angular_error_)
        continue;

      // 3D reconstruction
      auto &det = detections[ray_idx];

      if (det.id1 == 0)
        {
          // The candidate ray HAS NOT been used for a different dot reconstruction
          det.id1 = 1; // we are hijacking 'id1' to denote a reconstructed detection
          det.u = fpreal16(dots_[i][0]).getValue();
          det.v = fpreal16(dots_[i][1]).getValue();
          det.x = ConvertToInt16(ray_pt3d[0], data3d_type_param_);
          det.y = ConvertToInt16(ray_pt3d[1], data3d_type_param_);
          det.z = ConvertToInt16(ray_pt3d[2], data3d_type_param_);
          det.confidence = fpreal16(max_ddot).getValue();
          det.uid = ids_[ray_idx];
          no_detections_++;
        }
      else
        {
          // The candidate ray HAS been used for a different dot reconstruction.
          // Since we cannot use one ray to explain two different dots, we need to
          // invalidate the previous detection as well
          if (det.confidence != 0)
            {
              det.confidence = 0;
              no_detections_--;
            }
        }
    }

  rect3d_timer_.stop();
  user_timer_.stop();

  return no_detections_;
}

// ============================================================================
// ============================================================================

uint32_t SiftGeomDetector::getMaxNoDetections(void) const
{
  return max_detections_;
}

// ============================================================================

uint32_t SiftGeomDetector::getStride(void) const
{
  return stride_;
}

// ============================================================================

mke::dd::Data3dType SiftGeomDetector::getData3dType(void) const
{
  return data3d_type_param_;
}

// ============================================================================
// ============================================================================

#ifdef MKEDD_RESERVED_API

void SiftGeomDetector::getDebugData(nlohmann::json &data) const
{
 data = dbgdata_;
 classifier_->getDebugData(data["classifier"]);
}

// ============================================================================

void SiftGeomDetector::getTriangulation(nlohmann::json &triangulation)
{
  throw std::runtime_error("[MkEDD] SiftGeomDetector: getTriangulation not implemented");
}

#endif

#endif // MKEDD_USE_BOXLUT3_DETECTOR
