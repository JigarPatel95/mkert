/*
 * SiftGeom: SIFT based Geometrical detector
 *
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifndef _DD_SIFTGEOM_H_
#define _DD_SIFTGEOM_H_

#include "data_types.h"
#include "dd/detector.h"
#include "dd/session_wrapper.h"
#include "dc/classifier.h"
#include "util/timers.h"

#include "mkecc/dets/detector.h"

#include "util/pmap.h"

#ifdef MKEDD_MULTITHREADING
#include "util/threadpool.h"
#endif

namespace mke {
namespace dd {
namespace dd {

/** \addtogroup mkedd
 *  @{
 */

class SiftGeomDetector : public Detector, public SessionWrapper {
private:

  // Params
  util::ParameterMap pmap_;
  static const nlohmann::json param_schema_;
  mke::dd::Data3dType data3d_type_param_;

  // Dot detector
  std::unique_ptr<mke::cc::dets::Detector> detector_;

  // Dot classifier
  std::unique_ptr<dc::Classifier> classifier_;
  nlohmann::json dots_json_;

  // 3D reconstruction
  std::vector<std::array<double, 2>> dots_;
  std::vector<std::array<double, 3>> dot_dirs_;

  double max_angular_error_;

  // Results;
  uint32_t no_detections_;

  // Runtime statistics
  util::StatsThreadTimer user_timer_;
  util::StatsThreadTimer ddet_timer_;
  util::StatsThreadTimer fproj_timer_;
  util::StatsThreadTimer rect3d_timer_;

  // Utils
#ifdef MKEDD_MULTITHREADING
  util::ThreadPool<util::SHARED_THREADPOOL> threadpool_;
#endif

#ifdef MKEDD_RESERVED_API
  static const nlohmann::json reserved_param_schema_;
  nlohmann::json dbgdata_;
  bool store_dbg_data_;
#endif


  // ==========================================================================
  // Data

  void setInitValues(void);

  // ==========================================================================
  // Parameters

  void updateParams(const nlohmann::json &params, bool value_only = false);

public:

  // ==========================================================================
  // Static methods

  static const std::string staticGetTypeString(void)
  {
    static const std::string typeString = "SiftGeom";
    return typeString;
  }

  static Detector* create(void)
  {
    return new SiftGeomDetector();
  }

  // ==========================================================================
  // Construction

  SiftGeomDetector()
  {
    // Initial parameter values
    pmap_.addSchema(param_schema_);

#ifdef MKEDD_RESERVED_API
    pmap_.addSchema(reserved_param_schema_);
#endif

    updateParams(pmap_.getParams(), true);

   setInitValues();
  }

  ~SiftGeomDetector()
    {}

  // ==========================================================================
  // Parameters

  void validateParams(const nlohmann::json &params);
  nlohmann::json getParamsSchema(void) const;
  void setParams(const nlohmann::json &params);
  void getParams(nlohmann::json &params) const;
  const std::string getTypeString(void);

  // ==========================================================================
  // Runtime statistics

  virtual void resetStats(void);
  virtual void getStats(nlohmann::json &stats) const;

  // ==========================================================================
  // Data loading

  bool canParse(const SectionType section);
  void load(std::istream *is, const SectionType section);

  // ==========================================================================
  // Processing

  uint32_t process(const uint8_t *data, std::vector<PointData>& detections);
  uint32_t getMaxNoDetections(void) const;
  uint32_t getStride(void) const;
  mke::dd::Data3dType getData3dType(void) const;

  // ==========================================================================
  // Reserved API

#ifdef MKEDD_RESERVED_API
  void getDebugData(nlohmann::json &data) const;
  void getTriangulation(nlohmann::json &triangulation);
#endif

};

/** @}*/

} /* namespace dd */
} /* namespace dd */
} /* namespace mke */


#endif // _DD_SIFTGEOM_H_
