/*
 * ddfactory
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#include "dd/factory.h"
#include "dd/geom.h"

#ifdef MKEDD_USE_BOXLUT_DETECTOR
#include "dd/boxlut.h"
#endif

#ifdef MKEDD_USE_BOXLUT2_DETECTOR
#include "dd/boxlut2.h"
#endif

#ifdef MKEDD_USE_BOXLUT3_DETECTOR
#include "dd/boxlut3.h"
#endif

#ifdef MKEDD_USE_INTLUT_DETECTOR
#include "dd/intlut.h"
#endif

#ifdef MKEDD_USE_INTLUT2_DETECTOR
#include "dd/intlut2.h"
#endif


#ifdef MKEDD_USE_MKECC
#include "dd/siftgeom.h"
#endif

using namespace mke::dd::dd;

const std::map<const std::string, Detector*(*)(void)> Factory::create_map_ = {
#ifdef MKEDD_USE_BOXLUT_DETECTOR
  {BoxLutDetector::staticGetTypeString(), &BoxLutDetector::create},
#endif
#ifdef MKEDD_USE_BOXLUT2_DETECTOR
  {BoxLut2Detector::staticGetTypeString(), &BoxLut2Detector::create},
#endif
#ifdef MKEDD_USE_BOXLUT3_DETECTOR
  {BoxLut3Detector::staticGetTypeString(), &BoxLut3Detector::create},
#endif
#ifdef MKEDD_USE_INTLUT_DETECTOR
  {IntLutDetector::staticGetTypeString(), &IntLutDetector::create},
#endif
#ifdef MKEDD_USE_INTLUT_DETECTOR
  {IntLut2Detector::staticGetTypeString(), &IntLut2Detector::create},
#endif
#ifdef MKEDD_USE_MKECC
  {SiftGeomDetector::staticGetTypeString(), &SiftGeomDetector::create},
#endif
  {GeomDetector::staticGetTypeString(), &GeomDetector::create}
};
