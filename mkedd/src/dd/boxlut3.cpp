/*
 * BoxLutDetector, revision 3
 *
 * Copyright (c) 2018, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifdef MKEDD_USE_BOXLUT3_DETECTOR

#include <exception>
#include <iostream>
#include <cmath>

#include "dd/boxlut3.h"
#include "dc/factory.h"

using namespace mke::dd::dd;
using namespace mke::dd::types;
using json = nlohmann::json;

const int32_t mke::dd::dd::BoxLut3Detector::csshift_;

// ============================================================================
// ============================================================================
// Parameters

const json BoxLut3Detector::param_schema_ = R"(
    {
      "type": "object",
      "properties" : {
        "type" : {
          "description" : "Detector type",
          "type" : "string",
          "enum" : ["BoxLut3"],
          "default" : "BoxLut3"
        },
        "threshold" : {
          "description" : "Accept only detection with ddbox response above this theshold",
          "type" : "number",
          "minimum" : 1,
          "maximum" : 200,
          "default" : 30
        },
        "curv_ratio" : {
          "description" : "Curv ratio",
          "type" : "number",
          "minimum" : 1,
          "maximum" : 3,
          "default" : 1.2
        },
        "fit_peaks" : {
          "description" : "Perform sub-path-element peak fitting",
          "type" : "boolean",
          "default" : true
        },
        "no_epochs" : {
          "description" : "Number of epochs for the box hysteresis filtering",
          "type" : "number",
          "minimum" : 0,
          "maximum" : 10,
          "multipleOf" : 1,
          "default" : 2
        },
        "box_delta" : {
          "description" : "Delta of the box hysteresis filtering interval",
          "type" : "number",
          "maximum" : 10,
          "exclusiveMinimum": 0,
          "default" : 2
        },
        "kernel_widths" : {
          "description" : "Convolution box function kernel widths",
          "type" : "array",
          "items" : {
            "type": "number",
            "minimum" : 3,
            "multipleOf" : 1
          },
          "minItems" : 3,
          "maxItems" : 20,
          "default" : [3, 5, 7, 9, 13, 17, 21, 29, 37, 53]
        },
        "classifier" : {
          "description" : "2D dot classifier type",
          "type" : "string",
          "enum" : ["PassThru", "CircLinear", "ExCircLinear", "NgCircLinear"],
          "default" : "PassThru"
        },
        "classifier_params" : {
          "description" : "2D dot classifier parameters",
          "type" : "object",
          "default" : {}
        },
        "no_threads" : {
          "description" : "Number of threads for the detector to use",
          "minimum" : 1,
          "maximum" : 10,
          "default" : 4
        }
      }
    }
  )"_json;

#ifdef MKEDD_RESERVED_API
const json BoxLut3Detector::reserved_param_schema_ = R"SCHEMA(
        {
          "type": "object",
            "properties" : {
              "store_dbg_data" : {
                "type" : "boolean",
                "default" : true
              }
            }
          }
       )SCHEMA"_json;
#endif

// ============================================================================

inline void BoxLut3Detector::updateParams(const nlohmann::json &params, bool value_only)
{
  if (params.find("kernel_widths") != params.end())
    {
      json kernel_widths_json = pmap_.getParam("kernel_widths");
      int no_widths = kernel_widths_json.size();
      kernel_widths_.resize(no_widths);

      for (int i = 0; i < no_widths; i++)
        kernel_widths_[i] = kernel_widths_json[i];

      if (!value_only)
        setupPathsSlices(no_threads_param_);
    }

  if (params.find("threshold") != params.end())
    threshold_param_ = uint8_t(pmap_.getParam("threshold").get<double>());

  if (params.find("curv_ratio") != params.end())
    curv_ratio_param_ = fpreal16(pmap_.getParam("curv_ratio").get<double>());

  if (params.find("fit_peaks") != params.end())
    fit_peaks_param_ = pmap_.getParam("fit_peaks").get<bool>();

  if (params.find("no_epochs") != params.end())
    {
      no_epochs_param_ = uint8_t(pmap_.getParam("no_epochs").get<double>());
      if (!value_only)
        {
          std::fill(valid_epochs_.begin(), valid_epochs_.end(), 0);
          std::fill(peak_history_.begin(), peak_history_.end(), -1);
        }
    }

  if (params.find("box_delta") != params.end())
    box_delta_param_ = fpreal16(pmap_.getParam("box_delta").get<double>());

  if (params.find("classifier") != params.end())
    {
      classifier_.reset(dc::Factory::create(pmap_.getParam("classifier").get<std::string>()));
      if (!value_only)
        classifier_->setFrameParams(iwidth_, iheight_, stride_);
    }

  if (classifier_.get() && (params.find("classifier_params") != params.end()))
    {
      classifier_->setParams(params.at("classifier_params"));
    }

#ifdef MKEDD_RESERVED_API
  if (params.find("store_dbg_data") != params.end())
    store_dbg_data_ = pmap_.getParam("store_dbg_data").get<bool>();
#endif

#ifdef MKEDD_MULTITHREADING
  if (params.find("no_threads") != params.end())
    {
      no_threads_param_ = uint32_t(pmap_.getParam("no_threads").get<double>());
      threadpool_.setNoThreads(no_threads_param_);
      if (!value_only)
        setupPathsSlices(no_threads_param_);
    }
#else
  no_threads_param_ = 1;
#endif

}

// ============================================================================

void BoxLut3Detector::validateParams(const nlohmann::json &params)
{
  pmap_.validateParams(params);

  if (classifier_.get() && (params.find("classifier_params") != params.end()))
    classifier_->validateParams(params);
}

// ============================================================================

nlohmann::json BoxLut3Detector::getParamsSchema(void) const
{
  return pmap_.getSchema();
}

// ============================================================================

void BoxLut3Detector::setParams(const nlohmann::json &params)
{
  pmap_.validateParams(params);
  pmap_.setParams(params);
  updateParams(params);
}

// ============================================================================

void BoxLut3Detector::getParams(nlohmann::json &params) const
{
  params = pmap_.getParams();
  json classifier_params;

  if (classifier_.get())
    {
      classifier_->getParams(classifier_params);

      if (classifier_params.is_null())
        classifier_params = json::object();

      params["classifier_params"].swap(classifier_params);
    }
}

// ============================================================================

const std::string BoxLut3Detector::getTypeString(void)
{
  return staticGetTypeString();
}

// ============================================================================
// ==========================================================================
// Runtime statistics

void BoxLut3Detector::resetStats(void)
{
  user_timer_.reset();
  pxlist_timer_.reset();
  acclist_timer_.reset();
  conv_timer_.reset();
  nms_timer_.reset();
  bpeak_timer_.reset();
}

// ============================================================================

void BoxLut3Detector::getStats(nlohmann::json &stats) const
{
  
#ifndef __nios2_arch__
  stats["user_timer_s"] = user_timer_.elapsed<double, std::ratio<1>>();

  stats["pxlist_timer_s"] = pxlist_timer_.elapsed<double, std::ratio<1>>();
  stats["acclist_timer_s"] = acclist_timer_.elapsed<double, std::ratio<1>>();
  stats["conv_timer_s"] = conv_timer_.elapsed<double, std::ratio<1>>();
  stats["nms_timer_s"] = nms_timer_.elapsed<double, std::ratio<1>>();
  stats["bpeak_timer_s"] = bpeak_timer_.elapsed<double, std::ratio<1>>();
#endif
}

// ============================================================================
// ============================================================================
// Data

void BoxLut3Detector::setInitValues(void)
{
  iwidth_ = 0;
  iheight_ = 0;
  stride_ = 0;
  max_pels_ = 0;
}

// ============================================================================

void BoxLut3Detector::setupPathsSlices(const unsigned int no_slices)
{
  path_slices_.resize(no_slices);

  for (uint32_t i = 0; i < no_slices; i++)
    {
      path_slices_[i].accum.reserve(max_pels_);
      path_slices_[i].ddbox.reserve(max_pels_, kernel_widths_);
      path_slices_[i].no_detections = 0;
      path_slices_[i].path_idx.clear();
      path_slices_[i].path_idx.reserve(paths_.size() / no_slices);
    }

  for (uint32_t i = 0; i < paths_.size(); i++)
    {
      path_slices_[i % no_slices].path_idx.push_back(i);
    }
}

// ============================================================================
// ============================================================================
// Data loading

void BoxLut3Detector::loadColLut(std::istream *is)
{
  // 2D buffer info
  is->read((char *) &iwidth_, sizeof(iwidth_));
  is->read((char *) &iheight_, sizeof(iheight_));
  is->read((char *) &stride_, sizeof(stride_));

  // 3D info
  is->read((char *) &depth_min_, sizeof(depth_min_));
  is->read((char *) &depth_max_, sizeof(depth_max_));

  // Kernel widths
  uint32_t no_kernels;
  is->read((char *) &no_kernels, sizeof(no_kernels));

  if (no_kernels)
    {
      json params;
      params["kernel_widths"] = json::array();
      json &kernel_widths_json = params["kernel_widths"];
      kernel_widths_json[no_kernels - 1] = nullptr;

      for (uint32_t i = 0; i < no_kernels; i++)
        {
          uint16_t kw;
          is->read((char *) &kw, sizeof(kw));
          kernel_widths_json[i] = kw;
        }

      pmap_.setParams(params);
      updateParams(params, true);
    }

  // Collated Paths
  CollatedLitePath::PelsVector pels;
  std::vector<uint8_t> mask;
  uint32_t no_paths;
  uint32_t no_ppels;
  uint32_t no_bytes;

  is->read((char *) &no_paths, sizeof(no_paths));
  is->read((char *) &no_ppels, sizeof(no_ppels));
  is->read((char *) &max_pels_, sizeof(max_pels_));

  paths_.resize(no_paths);

  for (uint32_t i = 0; i < no_paths; i++)
    {
      // Info
      uint16_t no_pels;
      is->read((char *) &no_pels, sizeof(no_pels));

      uint32_t uid, lid, did;
      is->read((char *) &uid, sizeof(uid));
      is->read((char *) &lid, sizeof(lid));
      is->read((char *) &did, sizeof(did));
      paths_[i].setIds(uid, lid, did);

      // Mask
      uint16_t lmin, lmax;
      is->read((char *) &lmin, sizeof(lmin));
      is->read((char *) &lmax, sizeof(lmax));
      paths_[i].setBounds(lmin, lmax);

      no_bytes = (no_pels + 7) >> 3;
      mask.resize(no_bytes);
      is->read((char *) mask.data(), sizeof(uint8_t) * no_bytes);
      paths_[i].setMask(std::move(mask), no_pels);

      // Elements LUT
      pels.resize(no_pels);
      is->read((char *) pels.data(), sizeof(PathElementLiteLut) * no_pels);
      paths_[i].setPathElementsLut(std::move(pels));
    }

  setupPathsSlices(no_threads_param_);
}

// ============================================================================

void BoxLut3Detector::load(std::istream *is, const SectionType section)
{
  if (section != DD_SECTION_COLLUT)
    throw std::runtime_error("[MkEDD] BoxLut2: Unable to parse detector section: " + std::to_string(section));

  try
  {
    if (section == DD_SECTION_COLLUT)
      loadColLut(is);

    if (paths_.size())
      {
        valid_epochs_.resize(paths_.size());
        peak_history_.resize(paths_.size());
        dets_history_.resize(paths_.size());

        std::fill(valid_epochs_.begin(), valid_epochs_.end(), 0);
        std::fill(peak_history_.begin(), peak_history_.end(), -1);
      }
  }
  catch (std::exception &e)
  {
    setInitValues();
    throw;
  }
}

// ============================================================================

bool BoxLut3Detector::canParse(const SectionType section)
{
  if (section == DD_SECTION_COLLUT)
    return true;
  else
    return false;
}

// ============================================================================
// ============================================================================
// Processing

inline int BoxLut3Detector::selectBestPeak(const uint8_t *data, PathsSlice &pslice, const CollatedLitePath &path)
{
  int best_det_idx = -1;

  auto &ddbox = pslice.ddbox;
  const auto *peaks = ddbox.getPeaks();
  const auto no_peaks = ddbox.getNoPeaks();
  const auto *accum_data = pslice.accum.getAccumData();
  uint32_t no_pels = path.getNumPathElements();

  for (unsigned int j = 0; j < no_peaks; j++)
    {
      const unsigned int &scale = peaks[j].scale;
      const unsigned int &off = peaks[j].offset;

      // 1-D Curvature ratio test
      int w = ddbox.getBoxWidth(scale) >> 1;

      int rb = off + w;
      if ((rb >= int(no_pels)) || (accum_data[rb] * curv_ratio_param_ >= accum_data[off]))
        continue;

      int lb = off - w;
      if ((lb < 0) || (accum_data[lb] * curv_ratio_param_ >= accum_data[off]))
        continue;

      // Pre-selection is done, its time to call full dot classifier
      const PathElementLiteLut &dl = path.getPathElement(off);

      uint16_t u = dl.ui;
      if ((dl.uvf >> 8) > 128) // fractional part of u > 0.5?
        u++;

      uint16_t v = dl.vi;
      if ((dl.uvf & 0x00FF) > 128) // fractional part of v > 0.5?
        v++;

      if (!classifier_->classify(data, u, v, path.getUid(), &(dl.x)))
        continue;

      if (best_det_idx != -1)
        {
          // We have found another dot, the decision is NOT to output any detection
          best_det_idx = -1;
          break;
        }
      else
        {
          best_det_idx = j;
        }
    }

  return best_det_idx;
  // Peak ratio test
}

// ============================================================================

inline void BoxLut3Detector::setEmptyDetection(const uint32_t pidx, PointData &detection)
{
  detection.confidence = 0;

#ifdef MKEDD_RESERVED_API
  if (store_dbg_data_)
    dbgdata_["det_idx"][pidx] = 0;
#endif
}

// ============================================================================

inline uint32_t BoxLut3Detector::setDetection(const uint32_t pidx, PathsSlice &pslice, PointData &detection)
{
  // Is the offset a valid detection?
  fpreal16 &offset = pslice.peak_offset;
  fpreal16 foffset = floor(offset);
  fpreal16 alpha(offset.getValue() & 0xFFFF, true); // frac(offset)

  uint32_t off1 = int(foffset);
  uint32_t off2 = off1 + 1;

  const auto &mask = paths_[pidx].getMask();
  bool oflag1 = mask[off1];
  bool oflag2 = mask[off2];

  if (!oflag1 && !oflag2)
    {
      setEmptyDetection(pidx, detection);
      return 0;
    }
  else if (!alpha)
    off2 = off1;
  else if (!oflag1)
    off1 = off2;
  else if (!oflag2)
    off2 = off1;

  // Read out detection LUT
  //*static_cast<PointDataLut *>(&detection) = paths_[pidx].getPathElement(off1);
  paths_[pidx].setPathElement(*static_cast<PointDataLut *>(&detection), off1);
  detection.confidence = pslice.peak_info->response.getValue() << csshift_;
  detection.sigma2D = pslice.ddbox.getSigma(pslice.peak_info->scale).getValue();

  if (off1 != off2)
    {
      // Linear interpolation of 3D data
      // NB: We should be doing the same for 2D data, maybe later
      const PathElementLiteLut &det2 = paths_[pidx].getPathElement(off2);
      const fpreal16 beta = fpreal16(1) - alpha;

      detection.x = (alpha.getValue() * det2.x + beta.getValue() * detection.x) >> 16;
      detection.y = (alpha.getValue() * det2.y + beta.getValue() * detection.y) >> 16;
      detection.z = (alpha.getValue() * det2.z + beta.getValue() * detection.z) >> 16;
    }

#ifdef MKEDD_RESERVED_API
    if (store_dbg_data_)
      dbgdata_["det_idx"][pidx] = off1;
#endif

    return 1;
}

// ============================================================================

inline void BoxLut3Detector::incValidEpochs(const uint32_t pidx, const fpreal16 offset)
{
  valid_epochs_[pidx] = std::min(no_epochs_param_, uint8_t(1 + valid_epochs_[pidx]));
  peak_history_[pidx] = offset;
}

// ============================================================================

inline void BoxLut3Detector::decValidEpochs(const uint32_t pidx)
{
  valid_epochs_[pidx] = std::max(0, valid_epochs_[pidx] - 1);
}

// ============================================================================


inline uint32_t BoxLut3Detector::applyBoxingFilter(const uint32_t pidx, PathsSlice &pslice, PointData &detection)
{
  uint32_t valid_detection = 0;
  fpreal16 &offset = pslice.peak_offset;

  if (!valid_epochs_[pidx])
    {
      // There is no history for this point
      if (offset < fpreal16(0))
        {
          // There is no current detection for this point
          setEmptyDetection(pidx, detection);
        }
      else
        {
          // Set point data from current detection, update history
          setDetection(pidx, pslice, detection);
          dets_history_[pidx] = detection;
          incValidEpochs(pidx, offset);
          valid_detection = 1;
        }
    }
  else
    {
      // There is a history for this point
      if (offset < fpreal16(0))
        {
          // There is no current detection, use historical value, decrease validity
          detection = dets_history_[pidx];
          decValidEpochs(pidx);
        }
      else if (offset < (peak_history_[pidx] - box_delta_param_))
        {
          //  The current detection lies left to the boxing boundary
          valid_detection = 1;
          fpreal16 offset = offset + box_delta_param_;

          if (!setDetection(pidx, pslice, detection))
            {
              // We were not able to update the point data, use historical value, decreace validity
              detection = dets_history_[pidx];
              decValidEpochs(pidx);
            }
          else
            {
              // We were able to update the point data, update history using current data
              dets_history_[pidx] = detection;
              incValidEpochs(pidx, offset);
            }
        }
      else if (offset > (peak_history_[pidx] + box_delta_param_))
        {
          //  The current detection lies right to the boxing boundary
          valid_detection = 1;
          fpreal16 offset = offset - box_delta_param_;

          if (!setDetection(pidx, pslice, detection))
            {
              // We were not able to update the point data, use historical value, decreace validity
              detection = dets_history_[pidx];
              decValidEpochs(pidx);
            }
          else
            {
              // We were able to update the point data, update history using current data
              dets_history_[pidx] = detection;
              incValidEpochs(pidx, offset);
            }
        }
      else
        {
          // The current detection lies withing the boxing interval, use historical value
          valid_detection = 1;
          detection = dets_history_[pidx];
          incValidEpochs(pidx, peak_history_[pidx]);
        }
    }

  return valid_detection;
}


// ============================================================================

 void BoxLut3Detector::processPathSlice(const uint8_t *data, PathsSlice &pslice, std::vector<PointData>& detections)
 {
   util::StatsThreadTimer pxlist;
   util::StatsThreadTimer ct;
   util::StatsThreadTimer nmst;
   util::StatsThreadTimer bpt;
   util::StatsThreadTimer acct;

   fpreal16 threshold = fpreal16(threshold_param_) >> csshift_;
   int best_peak_idx;

   const fpreal16 *cumsum = pslice.accum.getCumsumData();
   pslice.no_detections = 0;

   for (uint32_t pidx : pslice.path_idx)
     {
       const CollatedLitePath &path = paths_[pidx];
       uint32_t no_pels = path.getNumPathElements();
       uint16_t lmin = path.getLowerBound();
       uint16_t lmax = path.getUpperBound();
       PointData &detection = detections[pidx];

       // Accumulate
       pxlist.start();
       pslice.accum.processByPathElements(data, path, stride_);
       pxlist.stop();

       // Convolve
       acct.start();
       ct.start();
       pslice.ddbox.convolve(cumsum, no_pels, lmin, lmax);
       ct.stop();

       // Find tentative peaks
       nmst.start();
       pslice.ddbox.findPeaks(no_pels, threshold, lmin + 1, lmax - 1);
       nmst.stop();

       // Best peak selection
       bpt.start();
       best_peak_idx = selectBestPeak(data, pslice, path);
       bpt.stop();

       if (best_peak_idx >= 0)
         {
           const auto *peaks = pslice.ddbox.getPeaks();
           pslice.peak_info = peaks + best_peak_idx;

           // Peak position polishing
           if (fit_peaks_param_)
             pslice.peak_offset = pslice.ddbox.fitPeak(best_peak_idx, no_pels, csshift_);
           else
             pslice.peak_offset = fpreal16(int32_t(pslice.peak_info->offset), false);
         }
       else
         {
           pslice.peak_info = nullptr;
           pslice.peak_offset = fpreal16(-1);
         }

       // Lookup and filter the detection
       if (no_epochs_param_)
         {
           pslice.no_detections += applyBoxingFilter(pidx, pslice, detection);
         }
       else
         {
           if (best_peak_idx >= 0)
            pslice.no_detections += setDetection(pidx, pslice, detection);
           else
            setEmptyDetection(pidx, detection);
         }

       acct.stop();

#ifdef MKEDD_RESERVED_API
       if (store_dbg_data_)
         {
           const auto *accum_data = pslice.accum.getAccumData();
           nlohmann::json accum_json = nlohmann::json::array();

           if (no_pels)
             accum_json[no_pels - 1] = nullptr;

           for (uint32_t i = 0; i < no_pels; i++)
             accum_json[i] = double(accum_data[i]);

           dbgdata_["accums"][pidx].swap(accum_json);
         }
#endif
     }

   pxlist_timer_.addElapsed(pxlist);

   conv_timer_.addElapsed(ct);
   nms_timer_.addElapsed(nmst);
   bpeak_timer_.addElapsed(bpt);
   acclist_timer_.addElapsed(acct);
 }

// ============================================================================

uint32_t BoxLut3Detector::process(const uint8_t *data, std::vector<PointData>& detections)
{
  user_timer_.start();

#ifdef MKEDD_RESERVED_API
  classifier_->initDebugData();

  if (store_dbg_data_)
    {
      dbgdata_.clear();

      nlohmann::json accums_json = nlohmann::json::array();
      nlohmann::json det_idx = nlohmann::json::array();

      if (paths_.size())
        {
          accums_json[paths_.size() - 1] = nullptr;
          det_idx[paths_.size() - 1] = nullptr;
        }

      dbgdata_["det_idx"].swap(det_idx);
      dbgdata_["accums"].swap(accums_json);
    }
#endif

#ifdef MKEDD_MULTITHREADING
  for (unsigned int i = 0; i < path_slices_.size(); i++)
    threadpool_.process([this, &detections, data, i](){
        util::StatsThreadTimer t;
        t.start();

        processPathSlice(data, path_slices_[i], detections);

        t.stop();
        user_timer_.addElapsed(t);        
        return true;
    });

  threadpool_.wait();
#else
  for (unsigned int i = 0; i < path_slices_.size(); i++)
    processPathSlice(data, path_slices_[i], detections);
#endif

  no_detections_ = 0;
  for (unsigned int i = 0; i < path_slices_.size(); i++)
    no_detections_ += path_slices_[i].no_detections;

  user_timer_.stop();

  return no_detections_;
}

// ============================================================================
// ============================================================================

uint32_t BoxLut3Detector::getMaxNoDetections(void) const
{
  return paths_.size();
}

// ============================================================================

uint32_t BoxLut3Detector::getStride(void) const
{
  return stride_;
}

// ============================================================================

mke::dd::Data3dType BoxLut3Detector::getData3dType(void) const
{
  return DD_DATA3D_MM;
}

// ============================================================================
// ============================================================================

#ifdef MKEDD_RESERVED_API

void BoxLut3Detector::getDebugData(nlohmann::json &data) const
{
  data = dbgdata_;
  classifier_->getDebugData(data["classifier"]);
}

// ============================================================================

void BoxLut3Detector::getTriangulation(nlohmann::json &triangulation)
{
  throw std::runtime_error("[MkEDD] BoxLut3Detector: getTriangulation not implemented");
}

#endif

#endif // MKEDD_USE_BOXLUT3_DETECTOR
