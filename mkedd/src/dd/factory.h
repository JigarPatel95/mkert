/*
 * ddfactory
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifndef _DDFACTORY_H_
#define _DDFACTORY_H_

#include <stdexcept>
#include <map>

#include "detector.h"

namespace mke {
namespace dd {
namespace dd {
  
/** \addtogroup mkedd
 *  @{
 */

class Factory {
private:
  const static std::map<const std::string, Detector*(*)(void)> create_map_;
    
public:
    
  static Detector* create(const std::string &detector_type_str)
  {

    const auto create_func = create_map_.find(detector_type_str);
    
    if (create_func != create_map_.end())
      return create_func->second();
    else
      throw std::runtime_error("Unknown detector type: " + detector_type_str);
  }
};


/** @}*/

} /* namespace dd */
} /* namespace dd */
} /* namespace mke */


#endif // _DDFACTORY_H_
