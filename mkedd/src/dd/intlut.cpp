/*
 * IntLutDetector, BoxLut based detector with ray interpolation
 *
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifdef MKEDD_USE_INTLUT_DETECTOR

#include <exception>
#include <iostream>
#include <cmath>

#include "dd/intlut.h"
#include "types/convert.h"
#include "dc/factory.h"


using namespace mke::dd::dd;
using namespace mke::dd::types;
using json = nlohmann::json;

const int32_t mke::dd::dd::IntLutDetector::csshift_;

// ============================================================================
// ============================================================================
// Parameters

const json IntLutDetector::param_schema_ = R"PARAMS(
    {
      "type": "object",
      "properties" : {
        "type" : {
          "description" : "Detector type",
          "type" : "string",
          "enum" : ["IntLut"],
          "default" : "IntLut"
        },
        "subdivision_level" : {
          "description" : "Number of subdivisions, actual number will be limited by the number of reserved IDs per ray",
          "type" : "number",
           "minimum" : 0,
           "maximum" : 3,
           "multipleOf" : 1,
           "default" : 1
        },
        "fill_holes" : {
          "description" : "Perform hole filling procedure",
          "type" : "boolean",
          "default" : true
        },
        "hole_filling_support" : {
          "description" : "The number of neighboring points to support hole filling",
           "type" : "number",
           "minimum" : 3,
           "maximum" : 6,
           "multipleOf" : 1,
           "default" : 3
        },
        "hole_filling_level" : {
          "description" : "The number of hole filling loops. Set to 0 to fill until stable",
          "type" : "number",
          "minimum" : 0,
          "maximum" : 20,
          "multipleOf" : 1,
          "default" : 3
        },
        "adjacency_threshold" : {
          "description" : "Accept detection w/o neighnors within adjacency_threshold [px]. Set to zero to disable test.",
          "type" : "number",
          "minimum" : 0,
          "maximum" : 200,
          "default" : 5
        },
        "laplace_smoothing" : {
          "description" : "Number of iterations of the Laplace smoothing operator (when applicable)",
          "type" : "number",
          "minimum" : 0,
          "maximum" : 10,
          "default" : 0
        },
        "threshold" : {
          "description" : "Accept only detection with ddbox response above this theshold",
          "type" : "number",
          "minimum" : 1,
          "maximum" : 200,
          "default" : 30
        },
        "curv_ratio" : {
          "description" : "Curv ratio",
          "type" : "number",
          "minimum" : 1,
          "maximum" : 3,
          "default" : 1.2
        },
        "fit_peaks" : {
          "description" : "Perform sub-path-element peak fitting",
          "type" : "boolean",
          "default" : true
        },
        "no_epochs" : {
          "description" : "Number of epochs for the box hysteresis filtering",
          "type" : "number",
          "minimum" : 0,
          "maximum" : 10,
          "multipleOf" : 1,
          "default" : 2
        },
        "box_delta" : {
          "description" : "Delta of the box hysteresis filtering interval",
          "type" : "number",
          "maximum" : 10,
          "exclusiveMinimum": 0,
          "default" : 2
        },
        "kernel_widths" : {
          "description" : "Convolution box function kernel widths",
          "type" : "array",
          "items" : {
            "type": "number",
            "minimum" : 3,
            "multipleOf" : 1
          },
          "minItems" : 3,
          "maxItems" : 20,
          "default" : [3, 5, 7, 9, 13, 17, 21, 29, 37, 53]
        },
        "classifier" : {
          "description" : "2D dot classifier type",
          "type" : "string",
          "enum" : ["PassThru", "CircLinear", "ExCircLinear", "NgCircLinear"],
          "default" : "PassThru"
        },
        "classifier_params" : {
          "description" : "2D dot classifier parameters",
          "type" : "object",
          "default" : {}
        },
        "no_threads" : {
          "description" : "Number of threads for the detector to use",
          "minimum" : 1,
          "maximum" : 10,
          "default" : 4
        }
      }
    }
  )PARAMS"_json;

#ifdef MKEDD_RESERVED_API
const json IntLutDetector::reserved_param_schema_ = R"SCHEMA(
        {
          "type": "object",
            "properties" : {
              "store_dbg_data" : {
                "type" : "boolean",
                "default" : true
              },
              "compute_triangulation" : {
                "type" : "boolean",
                "default" : true
              }
            }
          }
       )SCHEMA"_json;
#endif

// ============================================================================

const std::vector<std::vector<std::array<mke::dd::types::fpreal16, 3>>> IntLutDetector::sd_bcoords_ =
{
  // 0 Level
  {
  },
  // 1 Level
  {
    {mke::dd::types::fpreal16(1.0/3.0), mke::dd::types::fpreal16(1.0/3.0), mke::dd::types::fpreal16(1.0/3.0)}
  },
  // 2 Level
  {
    {mke::dd::types::fpreal16(5.0/12.0), mke::dd::types::fpreal16(5.0/12.0), mke::dd::types::fpreal16(1.0/ 6.0)},
    {mke::dd::types::fpreal16(1.0/ 6.0), mke::dd::types::fpreal16(5.0/12.0), mke::dd::types::fpreal16(5.0/12.0)},
    {mke::dd::types::fpreal16(5.0/12.0), mke::dd::types::fpreal16(1.0/ 6.0), mke::dd::types::fpreal16(5.0/12.0)}
  },
  // 3 Level
  {
    {mke::dd::types::fpreal16(2.0/3.0), mke::dd::types::fpreal16(1.0/6.0), mke::dd::types::fpreal16(1.0/6.0)},
    {mke::dd::types::fpreal16(1.0/6.0), mke::dd::types::fpreal16(2.0/3.0), mke::dd::types::fpreal16(1.0/6.0)},
    {mke::dd::types::fpreal16(1.0/6.0), mke::dd::types::fpreal16(1.0/6.0), mke::dd::types::fpreal16(2.0/3.0)},
    {mke::dd::types::fpreal16(1.0/3.0), mke::dd::types::fpreal16(1.0/3.0), mke::dd::types::fpreal16(1.0/3.0)},
  }
};

// ============================================================================

inline void IntLutDetector::updateParams(const nlohmann::json &params, bool value_only)
{
  if (params.contains("kernel_widths"))
    {
      json kernel_widths_json = pmap_.getParam("kernel_widths");
      int no_widths = kernel_widths_json.size();
      kernel_widths_.resize(no_widths);

      for (int i = 0; i < no_widths; i++)
        kernel_widths_[i] = kernel_widths_json[i];

      if (!value_only)
        setupSlices(no_threads_param_);
    }

  if (params.contains("adjacency_threshold"))
    {
      double val = uint8_t(pmap_.getParam("adjacency_threshold").get<double>());
      adjacency_threshold_param_ = types::fpreal16(val * val);
    }

  if (params.contains("fill_holes"))
    fill_holes_param_ = pmap_.getParam("fill_holes").get<bool>();

  if (params.contains("hole_filling_support"))
    hole_filling_support_param_ = pmap_.getParam("hole_filling_support").get<int>();

  if (params.contains("hole_filling_level"))
    hole_filling_level_param_ = pmap_.getParam("hole_filling_level").get<int>();

  if (params.contains("laplace_smoothing"))
    laplace_smoothing_param_ = pmap_.getParam("laplace_smoothing").get<int>();

  if (params.contains("threshold"))
    threshold_param_ = uint8_t(pmap_.getParam("threshold").get<double>());

  if (params.contains("subdivision_level"))
    {
      sdlevel_param_ = int32_t(pmap_.getParam("subdivision_level").get<double>());

      if (!value_only)
        computeMaxSdLevels(ids_per_iray_, sdlevel_, sdlevel_rem_);
    }

  if (params.contains("curv_ratio"))
    curv_ratio_param_ = fpreal16(pmap_.getParam("curv_ratio").get<double>());

  if (params.contains("fit_peaks"))
    fit_peaks_param_ = pmap_.getParam("fit_peaks").get<bool>();

  if (params.contains("no_epochs"))
    {
      no_epochs_param_ = uint8_t(pmap_.getParam("no_epochs").get<double>());
      if (!value_only)
        {
          std::fill(valid_epochs_.begin(), valid_epochs_.end(), 0);
          std::fill(peak_history_.begin(), peak_history_.end(), -1);
        }
    }

  if (params.contains("box_delta"))
    box_delta_param_ = fpreal16(pmap_.getParam("box_delta").get<double>());

  if (params.contains("classifier"))
    {
      classifier_.reset(dc::Factory::create(pmap_.getParam("classifier").get<std::string>()));
      if (!value_only)
        classifier_->setFrameParams(iwidth_, iheight_, stride_);
    }

  if (classifier_.get() && params.contains("classifier_params"))
    {
      classifier_->setParams(params.at("classifier_params"));
    }

#ifdef MKEDD_RESERVED_API
  if (params.contains("store_dbg_data"))
    store_dbg_data_ = pmap_.getParam("store_dbg_data").get<bool>();

  if (params.contains("compute_triangulation"))
    compute_triangulation_param_ = pmap_.getParam("compute_triangulation").get<bool>();
#endif

#ifdef MKEDD_MULTITHREADING
  if (params.contains("no_threads"))
    {
      no_threads_param_ = uint32_t(pmap_.getParam("no_threads").get<double>());
      threadpool_.setNoThreads(no_threads_param_);
      if (!value_only)
        setupSlices(no_threads_param_);
    }
#else
  no_threads_param_ = 1;
#endif

}

// ============================================================================

void IntLutDetector::validateParams(const nlohmann::json &params)
{
  pmap_.validateParams(params);

  if (classifier_.get() && (params.find("classifier_params") != params.end()))
    classifier_->validateParams(params);
}

// ============================================================================

nlohmann::json IntLutDetector::getParamsSchema(void) const
{
  return pmap_.getSchema();
}

// ============================================================================

void IntLutDetector::setParams(const nlohmann::json &params)
{
  pmap_.validateParams(params);
  pmap_.setParams(params);
  updateParams(params);
}

// ============================================================================

void IntLutDetector::getParams(nlohmann::json &params) const
{
  params = pmap_.getParams();
  json classifier_params;

  if (classifier_.get())
    {
      classifier_->getParams(classifier_params);

      if (classifier_params.is_null())
        classifier_params = json::object();

      params["classifier_params"].swap(classifier_params);
    }
}

// ============================================================================

const std::string IntLutDetector::getTypeString(void)
{
  return staticGetTypeString();
}

// ============================================================================
// ==========================================================================
// Runtime statistics

void IntLutDetector::resetStats(void)
{
  user_timer_.reset();
  pxlist_timer_.reset();
  acclist_timer_.reset();
  conv_timer_.reset();
  nms_timer_.reset();
  bpeak_timer_.reset();
  irays_timer_.reset();
  alist_timer_.reset();
  hfill_timer_.reset();
}

// ============================================================================

void IntLutDetector::getStats(nlohmann::json &stats) const
{
  
#ifndef __nios2_arch__
  stats["user_timer_s"] = user_timer_.elapsed<double, std::ratio<1>>();

  stats["pxlist_timer_s"] = pxlist_timer_.elapsed<double, std::ratio<1>>();
  stats["acclist_timer_s"] = acclist_timer_.elapsed<double, std::ratio<1>>();
  stats["conv_timer_s"] = conv_timer_.elapsed<double, std::ratio<1>>();
  stats["nms_timer_s"] = nms_timer_.elapsed<double, std::ratio<1>>();
  stats["bpeak_timer_s"] = bpeak_timer_.elapsed<double, std::ratio<1>>();
  stats["irays_timer_s"] = irays_timer_.elapsed<double, std::ratio<1>>();
  stats["alist_timer_s"] = alist_timer_.elapsed<double, std::ratio<1>>();
  stats["hfill_timer_s"] = alist_timer_.elapsed<double, std::ratio<1>>();
#endif
}

// ============================================================================
// ============================================================================
// Data

void IntLutDetector::setInitValues(void)
{
  iwidth_ = 0;
  iheight_ = 0;
  stride_ = 0;
  max_pels_ = 0;
  ids_per_iray_ = 0;
  sdlevel_ = 0;
  data3d_type_ = mke::dd::DD_DATA3D_MM;
}

// ============================================================================

void IntLutDetector::computeMaxSdLevels(const uint32_t ids_per_ray, uint32_t &sdlevel, uint32_t &rem)
{
  uint32_t no_ilevels = ilevels_.size();

  if (no_ilevels == 0)
    {
      sdlevel = 0;
    }
  else if (no_ilevels > 1)
    {
      sdlevel = std::min(no_ilevels, sdlevel_param_);
    }
  else
    {
      uint32_t no_sdlevels = sd_bcoords_.size();

      sdlevel = 0;
      rem = ids_per_ray;

      for (int i = no_sdlevels - 1; i >= 0; i--)
        {
          int no_bcoords = sd_bcoords_[i].size();

          if ((ids_per_ray >= no_bcoords) && (i <= sdlevel_param_))
            {
              sdlevel = i;
              rem = ids_per_ray - no_bcoords;
              break;
            }
        }
    }
}

// ============================================================================

void IntLutDetector::setupSlices(const unsigned int no_slices)
{
  // Path slices
  path_slices_.resize(no_slices);

  for (uint32_t i = 0; i < no_slices; i++)
    {
      path_slices_[i].accum.reserve(max_pels_);
      path_slices_[i].ddbox.reserve(max_pels_, kernel_widths_);
      path_slices_[i].no_detections = 0;
      path_slices_[i].path_idx.clear();
      path_slices_[i].path_idx.reserve(paths_.size() / no_slices);
    }

  for (uint32_t i = 0; i < paths_.size(); i++)
    path_slices_[i % no_slices].path_idx.push_back(i);

  // IntRay slices
  uint32_t no_ilevels = ilevels_.size();
  irays_slices_.resize(no_ilevels);

  for (uint32_t j = 0; j < no_ilevels; j++)
    {
      irays_slices_[j].resize(no_slices);

      for (uint32_t i = 0; i < no_slices; i++)
        {
          irays_slices_[j][i].no_detections = 0;
          irays_slices_[j][i].ray_idx.clear();
        }
    }

  uint32_t idx1 = 0, idx2;

  for (uint32_t j = 0; j < no_ilevels; j++)
    {
      idx2 = ilevels_[j];

      for (uint32_t i = idx1; i < idx2; i++)
        irays_slices_[j][i % no_slices].ray_idx.push_back(i);

      idx1 = idx2;
    }
}

// ============================================================================
// ============================================================================
// Data loading

void IntLutDetector::loadLut(std::istream *is, const SectionType section)
{
  // 2D buffer info
  is->read((char *) &iwidth_, sizeof(iwidth_));
  is->read((char *) &iheight_, sizeof(iheight_));
  is->read((char *) &stride_, sizeof(stride_));

  // 3D info
  is->read((char *) &depth_min_, sizeof(depth_min_));
  is->read((char *) &depth_max_, sizeof(depth_max_));

  if (section == DD_SECTION_INTLUT)
    {
      uint32_t type;
      is->read((char *) &type, sizeof(type));
      data3d_type_ = mke::dd::Data3dType(type);
    }

  // Kernel widths
  uint32_t no_kernels;
  is->read((char *) &no_kernels, sizeof(no_kernels));

  if (no_kernels)
    {
      json params;
      params["kernel_widths"] = json::array();
      json &kernel_widths_json = params["kernel_widths"];
      kernel_widths_json[no_kernels - 1] = nullptr;

      for (uint32_t i = 0; i < no_kernels; i++)
        {
          uint16_t kw;
          is->read((char *) &kw, sizeof(kw));
          kernel_widths_json[i] = kw;
        }

      pmap_.setParams(params);
      updateParams(params, true);
    }

  // Collated Paths
  CollatedLitePath::PelsVector pels;
  std::vector<uint8_t> mask;
  uint32_t no_paths;
  uint32_t no_ppels;
  uint32_t no_bytes;

  is->read((char *) &no_paths, sizeof(no_paths));
  is->read((char *) &no_ppels, sizeof(no_ppels));
  is->read((char *) &max_pels_, sizeof(max_pels_));

  paths_.resize(no_paths);

  for (uint32_t i = 0; i < no_paths; i++)
    {
      // Info
      uint16_t no_pels;
      is->read((char *) &no_pels, sizeof(no_pels));

      uint32_t uid, lid, did;
      is->read((char *) &uid, sizeof(uid));
      is->read((char *) &lid, sizeof(lid));
      is->read((char *) &did, sizeof(did));
      paths_[i].setIds(uid, lid, did);

      // Mask
      uint16_t lmin, lmax;
      is->read((char *) &lmin, sizeof(lmin));
      is->read((char *) &lmax, sizeof(lmax));
      paths_[i].setBounds(lmin, lmax);

      no_bytes = (no_pels + 7) >> 3;
      mask.resize(no_bytes);
      is->read((char *) mask.data(), sizeof(uint8_t) * no_bytes);
      paths_[i].setMask(std::move(mask), no_pels);

      // Elements LUT
      pels.resize(no_pels);
      is->read((char *) pels.data(), sizeof(PathElementLiteLut) * no_pels);
      paths_[i].setPathElementsLut(std::move(pels));

      if (section == DD_SECTION_INTLUT)
        {
          // Adjacency list
          std::vector<uint32_t> alist;

          uint8_t no_ns;
          is->read((char *) &no_ns, sizeof(no_ns));

          alist.resize(no_ns);
          is->read((char *) alist.data(), sizeof(uint32_t) * no_ns);
          paths_[i].setNeighbors(std::move(alist));
        }
    }

  if (section == DD_SECTION_INTLUT)
    {
      // Levels
      uint32_t no_ilevels;
      is->read((char *) &no_ilevels, sizeof(no_ilevels));

      ilevels_.resize(no_ilevels);
      is->read((char *) ilevels_.data(), sizeof(uint32_t) * no_ilevels);

      // IDs per iray
      is->read((char *) &ids_per_iray_, sizeof(ids_per_iray_));      

      // irays
      uint32_t no_irays;
      is->read((char *) &no_irays, sizeof(no_irays));

      irays_.resize(no_irays);
      is->read((char *) irays_.data(), sizeof(InterpolatedRay) * no_irays);
    }

  setupSlices(no_threads_param_);
  computeMaxSdLevels(ids_per_iray_, sdlevel_, sdlevel_rem_);
}

// ============================================================================

void IntLutDetector::load(std::istream *is, const SectionType section)
{
  if ((section != DD_SECTION_COLLUT) && (section != DD_SECTION_INTLUT))
    throw std::runtime_error("[MkEDD] IntLut: Unable to parse detector section: " + std::to_string(section));

  try
  {
    if ((section == DD_SECTION_COLLUT) || (section == DD_SECTION_INTLUT))
      loadLut(is, section);

    if (paths_.size())
      {
        valid_epochs_.resize(paths_.size());
        peak_history_.resize(paths_.size());
        dets_history_.resize(paths_.size());

        std::fill(valid_epochs_.begin(), valid_epochs_.end(), 0);
        std::fill(peak_history_.begin(), peak_history_.end(), -1);

        valid_flags_.resize(paths_.size());
        laplace_.resize(getMaxNoDetections());
      }
  }
  catch (std::exception &e)
  {
    setInitValues();
    throw;
  }
}

// ============================================================================

bool IntLutDetector::canParse(const SectionType section)
{
  if ((section == DD_SECTION_COLLUT) || (section == DD_SECTION_INTLUT))
    return true;
  else
    return false;
}

// ============================================================================
// ============================================================================
// Accumulator Processing

inline int IntLutDetector::selectBestPeak(const uint8_t *data, PathsSlice &pslice, const CollatedLitePath &path)
{
  int best_det_idx = -1;

  auto &ddbox = pslice.ddbox;
  const auto *peaks = ddbox.getPeaks();
  const auto no_peaks = ddbox.getNoPeaks();
  const auto *accum_data = pslice.accum.getAccumData();
  uint32_t no_pels = path.getNumPathElements();

  for (unsigned int j = 0; j < no_peaks; j++)
    {
      const unsigned int &scale = peaks[j].scale;
      const unsigned int &off = peaks[j].offset;

      // 1-D Curvature ratio test
      int w = ddbox.getBoxWidth(scale) >> 1;

      int rb = off + w;
      if ((rb >= int(no_pels)) || (accum_data[rb] * curv_ratio_param_ >= accum_data[off]))
        continue;

      int lb = off - w;
      if ((lb < 0) || (accum_data[lb] * curv_ratio_param_ >= accum_data[off]))
        continue;

      // Pre-selection is done, its time to call full dot classifier
      const PathElementLiteLut &dl = path.getPathElement(off);

      uint16_t u = dl.ui;
      if ((dl.uvf >> 8) > 128) // fractional part of u > 0.5?
        u++;

      uint16_t v = dl.vi;
      if ((dl.uvf & 0x00FF) > 128) // fractional part of v > 0.5?
        v++;

      if (!classifier_->classify(data, u, v, path.getUid(), &(dl.x)))
        continue;

      if (best_det_idx != -1)
        {
          // We have found another dot, the decision is NOT to output any detection
          best_det_idx = -1;
          break;
        }
      else
        {
          best_det_idx = j;
        }
    }

  return best_det_idx;
  // Peak ratio test
}

// ============================================================================

inline void IntLutDetector::setEmptyDetection(const uint32_t pidx, PointData &detection)
{
  detection.confidence = 0;

#ifdef MKEDD_RESERVED_API
  if (store_dbg_data_)
    dbgdata_["det_idx"][pidx] = 0;
#endif
}

// ============================================================================

inline uint32_t IntLutDetector::setDetection(const uint32_t pidx, PathsSlice &pslice, PointData &detection)
{
  // Is the offset a valid detection?
  fpreal16 &offset = pslice.peak_offset;
  fpreal16 foffset = floor(offset);
  fpreal16 alpha(offset.getValue() & 0xFFFF, true); // frac(offset)

  uint32_t off1 = int(foffset);
  uint32_t off2 = off1 + 1;

  const auto &mask = paths_[pidx].getMask();
  bool oflag1 = mask[off1];
  bool oflag2 = mask[off2];

  if (!oflag1 && !oflag2)
    {
      setEmptyDetection(pidx, detection);
      return 0;
    }
  else if (!alpha)
    off2 = off1;
  else if (!oflag1)
    off1 = off2;
  else if (!oflag2)
    off2 = off1;

  // Read out detection LUT
  //*static_cast<PointDataLut *>(&detection) = paths_[pidx].getPathElement(off1);
  paths_[pidx].setPathElement(*static_cast<PointDataLut *>(&detection), off1);
  detection.confidence = pslice.peak_info->response.getValue() << csshift_;
  detection.sigma2D = pslice.ddbox.getSigma(pslice.peak_info->scale).getValue();

  if (off1 != off2)
    {
      // Linear interpolation of 3D data
      // NB: We should be doing the same for 2D data, maybe later
      const PathElementLiteLut &det2 = paths_[pidx].getPathElement(off2);
      const fpreal16 beta = fpreal16(1) - alpha;

      detection.x = (alpha.getValue() * det2.x + beta.getValue() * detection.x) >> 16;
      detection.y = (alpha.getValue() * det2.y + beta.getValue() * detection.y) >> 16;
      detection.z = (alpha.getValue() * det2.z + beta.getValue() * detection.z) >> 16;
    }

#ifdef MKEDD_RESERVED_API
    if (store_dbg_data_)
      dbgdata_["det_idx"][pidx] = off1;
#endif

    return 1;
}

// ============================================================================

inline void IntLutDetector::incValidEpochs(const uint32_t pidx, const fpreal16 offset)
{
  valid_epochs_[pidx] = std::min(no_epochs_param_, uint8_t(1 + valid_epochs_[pidx]));
  peak_history_[pidx] = offset;
}

// ============================================================================

inline void IntLutDetector::decValidEpochs(const uint32_t pidx)
{
  valid_epochs_[pidx] = std::max(0, valid_epochs_[pidx] - 1);
}

// ============================================================================

inline uint32_t IntLutDetector::applyBoxingFilter(const uint32_t pidx, PathsSlice &pslice, PointData &detection)
{
  uint32_t valid_detection = 0;
  fpreal16 &offset = pslice.peak_offset;

  if (!valid_epochs_[pidx])
    {
      // There is no history for this point
      if (offset < fpreal16(0))
        {
          // There is no current detection for this point
          setEmptyDetection(pidx, detection);
        }
      else
        {
          // Set point data from current detection, update history
          setDetection(pidx, pslice, detection);
          dets_history_[pidx] = detection;
          incValidEpochs(pidx, offset);
          valid_detection = 1;
        }
    }
  else
    {
      // There is a history for this point
      if (offset < fpreal16(0))
        {
          // There is no current detection, use historical value, decrease validity
          detection = dets_history_[pidx];
          decValidEpochs(pidx);
        }
      else if (offset < (peak_history_[pidx] - box_delta_param_))
        {
          //  The current detection lies left to the boxing boundary
          valid_detection = 1;
          fpreal16 offset = offset + box_delta_param_;

          if (!setDetection(pidx, pslice, detection))
            {
              // We were not able to update the point data, use historical value, decreace validity
              detection = dets_history_[pidx];
              decValidEpochs(pidx);
            }
          else
            {
              // We were able to update the point data, update history using current data
              dets_history_[pidx] = detection;
              incValidEpochs(pidx, offset);
            }
        }
      else if (offset > (peak_history_[pidx] + box_delta_param_))
        {
          //  The current detection lies right to the boxing boundary
          valid_detection = 1;
          fpreal16 offset = offset - box_delta_param_;

          if (!setDetection(pidx, pslice, detection))
            {
              // We were not able to update the point data, use historical value, decreace validity
              detection = dets_history_[pidx];
              decValidEpochs(pidx);
            }
          else
            {
              // We were able to update the point data, update history using current data
              dets_history_[pidx] = detection;
              incValidEpochs(pidx, offset);
            }
        }
      else
        {
          // The current detection lies withing the boxing interval, use historical value
          valid_detection = 1;
          detection = dets_history_[pidx];
          incValidEpochs(pidx, peak_history_[pidx]);
        }
    }

  return valid_detection;
}

// ============================================================================

void IntLutDetector::processPathSlice(const uint8_t *data, PathsSlice &pslice, std::vector<PointData>& detections)
 {
   util::StatsThreadTimer pxlist;
   util::StatsThreadTimer ct;
   util::StatsThreadTimer nmst;
   util::StatsThreadTimer bpt;
   util::StatsThreadTimer acct;

   fpreal16 threshold = fpreal16(threshold_param_) >> csshift_;
   int best_peak_idx;

   const fpreal16 *cumsum = pslice.accum.getCumsumData();
   pslice.no_detections = 0;

   for (uint32_t pidx : pslice.path_idx)
     {
       const CollatedLitePath &path = paths_[pidx];
       uint32_t no_pels = path.getNumPathElements();
       uint16_t lmin = path.getLowerBound();
       uint16_t lmax = path.getUpperBound();
       PointData &detection = detections[pidx];

       // Accumulate
       pxlist.start();
       pslice.accum.processByPathElements(data, path, stride_);
       pxlist.stop();

       // Convolve
       acct.start();
       ct.start();
       pslice.ddbox.convolve(cumsum, no_pels, lmin, lmax);
       ct.stop();

       // Find tentative peaks
       nmst.start();
       pslice.ddbox.findPeaks(no_pels, threshold, lmin + 1, lmax - 1);
       nmst.stop();

       // Best peak selection
       bpt.start();
       best_peak_idx = selectBestPeak(data, pslice, path);
       bpt.stop();

       if (best_peak_idx >= 0)
         {
           const auto *peaks = pslice.ddbox.getPeaks();
           pslice.peak_info = peaks + best_peak_idx;

           // Peak position polishing
           if (fit_peaks_param_)
             pslice.peak_offset = pslice.ddbox.fitPeak(best_peak_idx, no_pels, csshift_);
           else
             pslice.peak_offset = fpreal16(int32_t(pslice.peak_info->offset), false);
         }
       else
         {
           pslice.peak_info = nullptr;
           pslice.peak_offset = fpreal16(-1);
         }

       // Lookup and filter the detection
       if (no_epochs_param_)
         {
           pslice.no_detections += applyBoxingFilter(pidx, pslice, detection);
         }
       else
         {
           if (best_peak_idx >= 0)
            pslice.no_detections += setDetection(pidx, pslice, detection);
           else
            setEmptyDetection(pidx, detection);
         }

       acct.stop();

#ifdef MKEDD_RESERVED_API
       if (store_dbg_data_)
         {
           const auto *accum_data = pslice.accum.getAccumData();
           nlohmann::json accum_json = nlohmann::json::array();

           if (no_pels)
             accum_json[no_pels - 1] = nullptr;

           for (uint32_t i = 0; i < no_pels; i++)
             accum_json[i] = double(accum_data[i]);

           dbgdata_["accums"][pidx].swap(accum_json);
         }
#endif
     }

   pxlist_timer_.addElapsed(pxlist);

   conv_timer_.addElapsed(ct);
   nms_timer_.addElapsed(nmst);
   bpeak_timer_.addElapsed(bpt);
   acclist_timer_.addElapsed(acct);
 }

// ============================================================================

void IntLutDetector::processPaths(const uint8_t *data, std::vector<PointData>& detections)
{
#ifdef MKEDD_MULTITHREADING
  for (unsigned int i = 0; i < path_slices_.size(); i++)
    threadpool_.process([this, &detections, data, i](){
        util::StatsThreadTimer t;
        t.start();

        processPathSlice(data, path_slices_[i], detections);

        t.stop();
        user_timer_.addElapsed(t);
        return true;
    });

  threadpool_.wait();
#else
  for (unsigned int i = 0; i < path_slices_.size(); i++)
    processPathSlice(data, path_slices_[i], detections);
#endif

  for (unsigned int i = 0; i < path_slices_.size(); i++)
    no_detections_ += path_slices_[i].no_detections;
}

// ============================================================================
// ============================================================================
// Interpolated Rays

template <typename RandomAccessContainer>
inline bool distanceTest(RandomAccessContainer &dets, const int no_dets = 3)
{
  std::array<uint64_t, 20> ds; // Hope 20 neighbors is enough

  for (int i = 0; i < no_dets; i++)
    {
      auto &v = *dets[i];
      auto &d = ds[i];

      d  = uint32_t(v.x) * uint32_t(v.x);
      d += uint32_t(v.y) * uint32_t(v.y);
      d += uint32_t(v.z) * uint32_t(v.z);
    }

  if (no_dets == 2)
    {
      if (ds[0] > ds[1])
        std::swap(ds[0], ds[1]);
    }
  else if (no_dets == 3)
    {
      if (ds[0] > ds[1])
        std::swap(ds[0], ds[1]);
      if (ds[0] > ds[2])
        std::swap(ds[0], ds[2]);
      if (ds[1] > ds[2])
        std::swap(ds[1], ds[2]);
    }
  else
    {
      std::sort(ds.data(), ds.data() + no_dets);
    }

  if ((ds[0] + (ds[0] >> 2)) < ds[no_dets - 1])
    return false;
  else
    return true;
}

// ============================================================================

template <typename RandomAccessContainer>
inline bool distanceTest(RandomAccessContainer &dets, const uint8_t idx1, const uint8_t idx2)
{
  std::array<typename RandomAccessContainer::value_type, 2> dets2 = {{dets[idx1], dets[idx2]}};
  return distanceTest(dets2, 2);
}

// ============================================================================

template <typename RandomAccessContainer>
inline bool validityTest(RandomAccessContainer &dets)
{
  {
    for (int i = 0; i < 3; i++)
      {
        if (!dets[i]->confidence)
          return false;
      }
    return true;
  }
}

// ============================================================================

template <typename RandomAccessContainer>
inline bool validityTest(RandomAccessContainer &dets, const uint8_t idx1, const uint8_t idx2)
{
  if (dets[idx1]->confidence && dets[idx2]->confidence)
    return true;
  else
    return false;
}
// ============================================================================

inline void IntLutDetector::interpolateDetection(PointData &res, const uint32_t uid, TriangleInfo &triangle, const BaryCoords &bc)
{
  // Data point triangulation
  auto &dets = triangle.dets;

  PointData &v1 = *dets[0];
  PointData &v2 = *dets[1];
  PointData &v3 = *dets[2];

  res.x = int((fpreal16(v1.x) * bc[0] + fpreal16(v2.x) * bc[1] + fpreal16(v3.x) * bc[2]));
  res.y = int((fpreal16(v1.y) * bc[0] + fpreal16(v2.y) * bc[1] + fpreal16(v3.y) * bc[2]));
  res.z = int((fpreal16(v1.z) * bc[0] + fpreal16(v2.z) * bc[1] + fpreal16(v3.z) * bc[2]));

  res.uid = uid;
  res.confidence = 1;
}

// ============================================================================

inline void IntLutDetector::interpolateDetection(TriangleInfo &triangle, const BaryCoords &bc)
{
  // Data point triangulation
  auto &res = *triangle.res;
  auto &dets = triangle.dets;

  PointData &v1 = *dets[0];
  PointData &v2 = *dets[1];
  PointData &v3 = *dets[2];

  res.x = int((fpreal16(v1.x) * bc[0] + fpreal16(v2.x) * bc[1] + fpreal16(v3.x) * bc[2]));
  res.y = int((fpreal16(v1.y) * bc[0] + fpreal16(v2.y) * bc[1] + fpreal16(v3.y) * bc[2]));
  res.z = int((fpreal16(v1.z) * bc[0] + fpreal16(v2.z) * bc[1] + fpreal16(v3.z) * bc[2]));

  res.uid = triangle.uid;
  res.confidence = 1;

  // Adjacency list update
  if (laplace_smoothing_param_)
    {
      auto &tri = triangle.tri;
      auto &didx = triangle.didx;
      auto &buffer = laplace_[didx].buffer;

      buffer.push_back(tri[0]);
      buffer.push_back(tri[1]);
      buffer.push_back(tri[2]);

      laplace_[tri[0]].buffer.push_back(didx);
      laplace_[tri[1]].buffer.push_back(didx);
      laplace_[tri[2]].buffer.push_back(didx);
    }
}

// ============================================================================

inline void IntLutDetector::interpolateDetection(TriangleInfo &triangle, const uint8_t idx1, const uint8_t idx2)
{  
  // Data point triangulation
  auto &res = *triangle.res;
  auto &dets = triangle.dets;

  PointData &v1 = *dets[idx1];
  PointData &v2 = *dets[idx2];

  res.x = (int32_t(v1.x) + int32_t(v2.x)) >> 1;
  res.y = (int32_t(v1.y) + int32_t(v2.y)) >> 1;
  res.z = (int32_t(v1.z) + int32_t(v2.z)) >> 1;

  res.uid = triangle.uid;
  res.confidence = 1;

  // Adjacency list update
  if (laplace_smoothing_param_)
    {
      auto &tri = triangle.tri;
      auto &didx = triangle.didx;
      auto &buffer = laplace_[didx].buffer;

      buffer.push_back(tri[idx1]);
      buffer.push_back(tri[idx2]);

      laplace_[tri[idx1]].buffer.push_back(didx);
      laplace_[tri[idx2]].buffer.push_back(didx);
    }
}

// ============================================================================

inline void IntLutDetector::getTriangleInfo(const uint32_t ridx, const uint32_t didx, TriangleInfo &triang, std::vector<PointData>& detections)
{
  auto &iray = irays_[ridx];
  auto &tri = iray.getTriangulation();

  triang.tri = tri;
  triang.uid = iray.getId();
  triang.didx = didx;
  triang.res = &detections[didx];
  triang.dets[0] = &detections[tri[0]];
  triang.dets[1] = &detections[tri[1]];
  triang.dets[2] = &detections[tri[2]];
}

// ============================================================================

void IntLutDetector::clearRaySlice(InterpolatedRaysSlice &irslice, std::vector<PointData>& detections)
{
  irslice.no_detections = 0;
  uint32_t no_paths = paths_.size();

  for (uint32_t ridx : irslice.ray_idx)
    {
      uint32_t ir_idx = no_paths + ridx * ids_per_iray_;
      detections[ir_idx].confidence = 0;
    }
}

// ============================================================================

void IntLutDetector::processMultiLevelRaySlice(InterpolatedRaysSlice &irslice, std::vector<PointData>& detections)
{
  const std::array<const std::array<uint8_t, 2>, 3> vidx = {{{0, 1}, {0, 2}, {1, 2}}};
  uint32_t no_paths = paths_.size();
  TriangleInfo triangle;

  uint32_t no_detections = 0;

  for (uint32_t ridx : irslice.ray_idx)
    {
      uint32_t didx = no_paths + ridx * ids_per_iray_;
      auto &det = detections[didx];

      getTriangleInfo(ridx, didx, triangle, detections);
      det.confidence = 0; // This may change later

      if (validityTest(triangle.dets))
        {
          if (distanceTest(triangle.dets))
            {
              // Interpolate from all three vertices
              no_detections++;
              interpolateDetection(triangle, sd_bcoords_[1][0]);
            }
        }
      else
        {
          // Interpolate between two valid verices only
          for (int i = 0; i < 3; i++)
            {
              const auto &idx1 = vidx[i][0];
              const auto &idx2 = vidx[i][1];

              if (validityTest(triangle.dets, idx1, idx2) && distanceTest(triangle.dets, idx1, idx2))
                {
                  no_detections++;
                  interpolateDetection(triangle, idx1, idx2);
                  break;
                }
            }
        }

      // Invalidate reserved detections, if any
      for (int j = 1; j < ids_per_iray_; j++)
        detections[didx + j].confidence = 0;
    }

  irslice.no_detections = no_detections;
}

// ============================================================================

void IntLutDetector::processSingleLevelRaySlice(InterpolatedRaysSlice &irslice, std::vector<PointData>& detections)
{
  uint32_t no_paths = paths_.size();
  auto &bcoords = sd_bcoords_[sdlevel_];
  int no_sdrays = bcoords.size();
  TriangleInfo triangle;

  uint32_t no_detections = 0;

  for (uint32_t ridx : irslice.ray_idx)
    {
      uint32_t didx = no_paths + ridx * ids_per_iray_;
      uint32_t uid = irays_[ridx].getId();

      getTriangleInfo(ridx, didx, triangle, detections);

      if (validityTest(triangle.dets) && distanceTest(triangle.dets))
        {
          // Interpolate using barycentric coordinates
          for (int i = 0; i < no_sdrays; i++)
              interpolateDetection(detections[didx++], uid++, triangle, bcoords[i]);

          no_detections += no_sdrays;

          // Remove remaining reserved detections
          for (int i = 0; i < sdlevel_rem_; i++)
            detections[didx++].confidence = 0;
        }
      else
        {
          // Remove all reserved detections
          for (int i = 0; i < ids_per_iray_; i++)
            detections[didx++].confidence = 0;
        }
    }

  irslice.no_detections = no_detections;
}

// ============================================================================

void IntLutDetector::processInterpolatedRays(std::vector<PointData>& detections)
{
  irays_timer_.start();

  if (sdlevel_)
    {
      if ((ilevels_.size() > 1) || ((ilevels_.size() == 1) && (ids_per_iray_ == 1)))
        {
          // Multilevel interpolation
#ifdef MKEDD_MULTITHREADING
          for (uint32_t j = 0; j < sdlevel_; j++)
            {
              for (unsigned int i = 0; i < irays_slices_[j].size(); i++)
                {
                  threadpool_.process([this, &detections, i, j](){
                      util::StatsThreadTimer t;
                      t.start();

                      processMultiLevelRaySlice(irays_slices_[j][i], detections);

                      t.stop();
                      user_timer_.addElapsed(t);
                      irays_timer_.addElapsed(t);

                      return true;
                    });
                }

              threadpool_.wait();
            }

          for (uint32_t j = 0; j < sdlevel_; j++)
            for (unsigned int i = 0; i < irays_slices_[j].size(); i++)
              no_detections_ += irays_slices_[j][i].no_detections;

#else
          for (uint32_t j = 0; j < sdlevel_; j++)
            {
              for (unsigned int i = 0; i < irays_slices_[j].size(); i++)
               {
                  processMultiLevelRaySlice(irays_slices_[j][i], detections);
                  no_detections_ += irays_slices_[j][i].no_detections;
                }
            }
#endif

          for (uint32_t j = sdlevel_; j < ilevels_.size(); j++)
          {
            for (unsigned int i = 0; i < irays_slices_[j].size(); i++)
              clearRaySlice(irays_slices_[j][i], detections);
          }
        }
      else
        {
          // Singlelevel interpolation
#ifdef MKEDD_MULTITHREADING
          for (unsigned int i = 0; i < irays_slices_[0].size(); i++)
            threadpool_.process([this, &detections, i](){
                util::StatsThreadTimer t;
                t.start();

                processSingleLevelRaySlice(irays_slices_[0][i], detections);

                t.stop();
                user_timer_.addElapsed(t);
                irays_timer_.addElapsed(t);

                return true;
              });

          threadpool_.wait();
#else
          for (unsigned int i = 0; i < irays_slices_[0].size(); i++)
            processSingleLevelRaySlice(irays_slices_[0][i], detections);
#endif
          for (unsigned int i = 0; i < irays_slices_[0].size(); i++)
            no_detections_ += irays_slices_[0][i].no_detections;
        }
    }
  else
    {    
      // Interpolation disabled
      uint32_t no_paths = paths_.size();
      uint32_t no_irays = irays_.size();

      for (uint32_t i = 0; i < no_irays * ids_per_iray_; i++)
        detections[no_paths + i].confidence = 0;
    }

  irays_timer_.stop();
}

// ============================================================================
// ============================================================================
// Outlier removal

void IntLutDetector::processOutlierSlice(PathsSlice &pslice, std::vector<PointData>& detections)
{
 //
}

// ============================================================================

void IntLutDetector::performOutlierRemoval(std::vector<PointData>& detections)
{
  uint32_t no_paths = paths_.size();

  alist_timer_.start();

  for (uint32_t pidx = 0; pidx < no_paths; pidx++)
    {
      PointData &det = detections[pidx];
      const CollatedLitePath &path = paths_[pidx];
      const auto &ns = path.getNeighbors();

      for (int i = 0; i < ns.size(); i++)
        {
          uint32_t nidx = ns[i];
          PointData &n = detections[nidx];

          if (!n.confidence || (nidx > pidx))
            continue;

          types::fpreal16 du = types::fpreal16(det.u, true) - types::fpreal16(n.u, true);
          types::fpreal16 dv = types::fpreal16(det.v, true) - types::fpreal16(n.v, true);
          types::fpreal16 d2 = du * du + dv * dv;

          if (d2 < adjacency_threshold_param_)
            {
              n.confidence = 0;
              det.confidence = 0;
              no_detections_ -= 1;

              break;
            }
        }
    }

  alist_timer_.stop();
}

// ============================================================================
// ============================================================================
// Hole filling

void IntLutDetector::processHoleSlice(PathsSlice &pslice, std::vector<PointData>& detections)
{
  pslice.no_detections = 0;
  std::array<PointData*, 20> valid_ns; // Hope 20 neighbors is enough

  for (uint32_t pidx : pslice.path_idx)
    {
      PointData &det = detections[pidx];
      const CollatedLitePath &path = paths_[pidx];
      const auto &ns = path.getNeighbors();
      int no_validns = 0;

      if (valid_flags_[pidx])
        continue;

      for (int i = 0; i < ns.size(); i++)
        {
          uint32_t nidx = ns[i];

          if (!valid_flags_[nidx])
            continue;

          PointData &n = detections[nidx];
          valid_ns[no_validns++] = &n;
        }

      // Interpolate new 3D point
      if ((no_validns >= hole_filling_support_param_) && distanceTest(valid_ns, no_validns))
        {
          fpreal16 coef = fpreal16(1) / fpreal16(no_validns);
          fpreal16 x = 0;
          fpreal16 y = 0;
          fpreal16 z = 0;

          for (int i = 0; i < no_validns; i++)
            {
              PointData &n = *valid_ns[i];

              x += coef * fpreal16(n.x);
              y += coef * fpreal16(n.y);
              z += coef * fpreal16(n.z);
            }

          det.x = int(x);
          det.y = int(y);
          det.z = int(z);

          det.confidence = 1;
          det.uid = path.getUid();

          pslice.no_detections++;
        }
    }
}

// ============================================================================

void IntLutDetector::performHoleFilling(std::vector<PointData>& detections)
{
  hfill_timer_.start();
  bool cont_flag = false;
  int level = hole_filling_level_param_;

  for(;;)
    {
#ifdef MKEDD_MULTITHREADING  
      // Setup flags
      for (unsigned int i = 0; i < path_slices_.size(); i++)
        {
          auto &pslice = path_slices_[i];

          for (uint32_t pidx : pslice.path_idx)
            {
              PointData &det = detections[pidx];
              valid_flags_[pidx] = (det.confidence == 0) ? false : true;
            }
        }

      // Perform hole filling
      for (unsigned int i = 0; i < path_slices_.size(); i++)
        {
          threadpool_.process([this, &detections, i](){
              util::StatsThreadTimer t;
              t.start();

              processHoleSlice(path_slices_[i], detections);

              t.stop();
              hfill_timer_.addElapsed(t);

              return true;
            });
        }

      threadpool_.wait();

#else
      for (unsigned int i = 0; i < path_slices_.size(); i++)
        processHoleSlice(path_slices_[i], detections);
#endif

      cont_flag = false;

      for (unsigned int i = 0; i < path_slices_.size(); i++)
        {
          uint32_t no_dets = path_slices_[i].no_detections;
          no_detections_ += no_dets;

          if (no_dets)
            cont_flag = true;
        }

      if (!hole_filling_level_param_)
        {
          if (!cont_flag)
            break;
        }
      else
        {
          level--;
          if (!level)
            break;
        }
    }

  hfill_timer_.stop();
}

// ============================================================================
// ============================================================================
// Laplace smoothing

template <typename T1, typename T2>
inline bool laplaceValidityTest(const T1 iter, const T2 size)
{
  if (((iter == 0) && (size >= 4)) || ((iter > 0) && (size >= 2)))
    return true;
  else
    return false;
}

// ============================================================================

void IntLutDetector::updateLaplaceDetections(const uint32_t iter, std::vector<PointData>& detections)
{
  uint32_t no_detections = detections.size();

  for (uint32_t i = 0; i < no_detections; i++)
    {
      auto &lpdata = laplace_[i];
      auto &buffer = lpdata.buffer;

      if (laplaceValidityTest(iter, buffer.size()))
        {
          detections[i].x = lpdata.x;
          detections[i].y = lpdata.y;
          detections[i].z = lpdata.z;
        }

      if ((iter + 1) == laplace_smoothing_param_)
        buffer.clear();
    }
}

// ============================================================================

void IntLutDetector::processLaplaceSmoothingSlice(const uint32_t init_idx, const uint32_t step, const uint32_t iter, std::vector<PointData>& detections)
{
  uint32_t no_detections = detections.size();
  uint32_t idx = init_idx;

  while(idx < no_detections)
    {
      auto &lpdata = laplace_[idx];
      auto &det = detections[idx];
      auto &buffer = lpdata.buffer;
      idx += step;

      if (!laplaceValidityTest(iter, buffer.size()))
        continue;

      int32_t x = det.x;
      int32_t y = det.y;
      int32_t z = det.z;

      for (int i = 0; i < buffer.size(); i++)
        {
          auto &neighbor = detections[buffer[i]];

          x += neighbor.x;
          y += neighbor.y;
          z += neighbor.z;
        }

      int32_t denom = buffer.size() + 1;

      if (denom == 4)
        {
          lpdata.x = x >> 2;
          lpdata.y = y >> 2;
          lpdata.z = z >> 2;
        }
      else
        {
          lpdata.x = x / denom;
          lpdata.y = y / denom;
          lpdata.z = z / denom;
        }
    }
}

// ============================================================================

void IntLutDetector::performLaplaceSmoothing(std::vector<PointData>& detections)
{
  if (!sdlevel_)
    return;

  for (unsigned int j = 0; j < laplace_smoothing_param_; j++)
    {
#ifdef MKEDD_MULTITHREADING
      for (unsigned int i = 0; i < no_threads_param_; i++)
        {
          threadpool_.process([this, &detections, i, j](){
              util::StatsThreadTimer t;
              t.start();

              processLaplaceSmoothingSlice(i, no_threads_param_, j, detections);

              t.stop();
              user_timer_.addElapsed(t);
              irays_timer_.addElapsed(t);

              return true;
            });
        }

      threadpool_.wait();
#else
      processLaplaceSmoothingSlice(0, 1, j, detections);
#endif

      updateLaplaceDetections(j, detections);
    }
}

// ============================================================================
// ============================================================================
// Main processing loop

uint32_t IntLutDetector::process(const uint8_t *data, std::vector<PointData>& detections)
{
  user_timer_.start();

#ifdef MKEDD_RESERVED_API
  classifier_->initDebugData();

  if (store_dbg_data_)
    {
      dbgdata_.clear();

      nlohmann::json accums_json = nlohmann::json::array();
      nlohmann::json det_idx = nlohmann::json::array();

      if (paths_.size())
        {
          accums_json[paths_.size() - 1] = nullptr;
          det_idx[paths_.size() - 1] = nullptr;
        }

      dbgdata_["det_idx"].swap(det_idx);
      dbgdata_["accums"].swap(accums_json);
    }
#endif

  no_detections_ = 0;

  // Paths
  processPaths(data, detections);

  // Outliers
  if (adjacency_threshold_param_)
    performOutlierRemoval(detections);

  // Hole filling
  if (fill_holes_param_)
    performHoleFilling(detections);

  // InterpolatedRays
  processInterpolatedRays(detections);

  // Laplace smoothing
  if (laplace_smoothing_param_)
    performLaplaceSmoothing(detections);

#ifdef MKEDD_RESERVED_API
  if (compute_triangulation_param_)
    {
      json triangulation;

      if(getTriangulation(detections, triangulation))
         dbgdata_["triangulation"].swap(triangulation);
    }
#endif

  user_timer_.stop();

  return no_detections_;
}

// ============================================================================
// ============================================================================
// Detector info

uint32_t IntLutDetector::getMaxNoDetections(void) const
{
  return paths_.size() + irays_.size() * ids_per_iray_;
}

// ============================================================================

uint32_t IntLutDetector::getStride(void) const
{
  return stride_;
}

// ============================================================================

mke::dd::Data3dType IntLutDetector::getData3dType(void) const
{
  return data3d_type_;
}

// ============================================================================
// ============================================================================
// Reserved API

#ifdef MKEDD_RESERVED_API

void IntLutDetector::getDebugData(nlohmann::json &data) const
{
  data = dbgdata_;
  classifier_->getDebugData(data["classifier"]);
}

// ============================================================================

void IntLutDetector::getTriangulation(nlohmann::json &triangulation)
{
  triangulation.swap(dbgdata_["triangulation"]);
}

// ============================================================================

bool IntLutDetector::getTriangulation(std::vector<PointData>& detections, nlohmann::json &triangulation)
{
  if (!sdlevel_)
    return false;

  triangulation.clear();
  auto &tri = triangulation["tri"];
  auto &coords = triangulation["coords"];
  auto &uids = triangulation["uid"];

  uint32_t t1 = (sdlevel_ == 1) ? 0 : ilevels_[sdlevel_ - 2];
  uint32_t t2 = ilevels_[sdlevel_ - 1];
  int no_paths = paths_.size();

  std::array<PointData*, 3> dets;
  std::map<uint32_t, uint32_t> didx2tidx;
  std::vector<uint32_t> tidxs;

  for (int i = t1; i < t2; i++)
    {
      int idx = no_paths + i;

      if (detections[idx].confidence)
        {
          auto &dtri_idx = irays_[i].getTriangulation();

          dets[0] = &detections[dtri_idx[0]];
          dets[1] = &detections[dtri_idx[1]];
          dets[2] = &detections[dtri_idx[2]];

          if (validityTest(dets))
            {
              tri.push_back({0, 0, 0});
              auto &ttri_idx = tri.back();

              for (int j = 0; j < 3; j++)
                {
                  uint32_t didx = dtri_idx[j];
                  uint32_t tidx = 0;

                  auto it = didx2tidx.find(didx);

                  if (it == didx2tidx.end())
                    {
                      tidx = tidxs.size();
                      didx2tidx[didx] = tidx;
                      tidxs.push_back(didx);
                    }
                  else
                    {
                      tidx = (*it).second;
                    }

                  ttri_idx[j] = tidx;
                }
            }
        }
    }

  // Save coord, uids
  int no_pts = tidxs.size();

  if (no_pts)
    {
      coords[no_pts - 1] = nullptr;
      uids[no_pts - 1] = nullptr;
    }

  for (int i = 0; i < no_pts; i++)
    {
      uint32_t didx = tidxs[i];
      auto &det = detections[didx];

      coords[i] = {types::Convert(det.x, data3d_type_),
                   types::Convert(det.y, data3d_type_),
                   types::Convert(det.z, data3d_type_)};

      uids[i] = det.uid;
    }

  return true;
}

#endif

#endif // MKEDD_USE_INTLUT_DETECTOR
