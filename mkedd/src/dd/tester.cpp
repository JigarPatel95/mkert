/*
 * Tester
 *
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */


#ifdef MKEDD_RESERVED_API

#include "enum.h"

#include "dd/tester.h"
#include "types/fpreal.h"

#include "dd/ddbox.h"
#include "dd/accum.h"

BETTER_ENUM(NmsMethodType, int,
            UNKNOWN_METHOD = 0,
            NAIVE_METHOD = 1,
            PHAM_METHOD = 2,
            FPR16_METHOD = 100,
            NAIVE_FPR16_METHOD = 101,
            PHAM_FPR16_METHOD = 102,
            NEON_FPR16_METHOD = 103,
            AVX2_FPR16_METHOD = 104)

BETTER_ENUM(ConvMethodType, int,
            UNKNOWN_METHOD = 0,
            NAIVE_METHOD = 1,
            FPR16_METHOD = 100,
            NAIVE_FPR16_METHOD = 101,
            NEON_FPR16_METHOD = 102,
            AVX2_FPR16_METHOD = 103,
            HALIDE_FPR16_METHOD = 104)

using namespace mke::dd::dd;
using namespace mke::dd::types;
using nlohmann::json;

// ============================================================================

template<class Accum, class DdBox>
void copyConvData(const Accum &accum, const DdBox &ddbox, const int length, json &cumsum_json, json &cconv_json)
{
  if (length)
    {
      const auto *cumsum = accum.getCumsumData();
      cumsum_json[length - 1] = nullptr;

      for (int i = 0; i < length; i++)
        cumsum_json[i] = double(cumsum[i]);
    }

  uint32_t no_scales = ddbox.getNoScales();

  if (no_scales)
    {
      cconv_json[no_scales - 1] = nullptr;

      for (uint32_t j = 0; j < no_scales; j++)
        {
          cconv_json[j] = nlohmann::json::array();
          cconv_json[j][length - 1] = nullptr;
          for (uint32_t i = 0; i < length; i++)
            cconv_json[j][i] = double(ddbox.getData(i, j));
        }
    }

}

// ============================================================================

template<class Nms>
void copyMaximas(const Nms &nms, const int no_max, json &maximas_json)
{
  const typename Nms::Maximum *maximas = nms.getMaximas();

  if (no_max)
    {
      maximas_json[no_max - 1] = nullptr;

      for (int i = 0; i < no_max; i++)
        {
          json &max_json = maximas_json[i];
          max_json["x"] = maximas[i].x;
          max_json["y"] = maximas[i].y;
          max_json["s"] = 0;
        }
    }
}

// ============================================================================

template<class Accum, class DdBox, class Nms, typename T>
void curvatureTest(const Accum &accum, const DdBox &ddbox, const Nms &nms,
                   const int no_max, const int length, const T curv_ratio, json &maximas_json)
{
  const typename Nms::Maximum *maximas = nms.getMaximas();
  const auto *accum_data = accum.getAccumData();

  if (no_max)
    {
      for (int i = 0; i < no_max; i++)
        {
          const unsigned int &scale = maximas[i].y;
          const unsigned int &off = maximas[i].x;
          int w = ddbox.getBoxWidth(scale) >> 1;

          int rb = off + w;
          if ((rb >= length) || ((accum_data[rb] * curv_ratio) >= accum_data[off]))
              continue;

          int lb = off - w;
          if ((lb < 0) || ((accum_data[lb] * curv_ratio) >= accum_data[off]))
            continue;

          json &max_json = maximas_json[i];
          max_json["s"] = max_json["s"].get<double>() + 1;
        }
    }
}

// ============================================================================

template<class Accum, class DdBox>
void convolve(ConvMethodType method, Accum &accum, DdBox &ddbox,
              std::vector<int32_t> &kernel_widths,
              const int length, const int lmin, const int lmax, double *data,
              json &results)
{
  accum.reserve(length);
  accum.processFromArray(data, length);

  if (kernel_widths.size())
    ddbox.reserve(length, kernel_widths);
  else
    ddbox.reserve(length);

  if (method == +ConvMethodType::NAIVE_METHOD)
    ddbox.convolveNaive(accum.getCumsumData(), length, lmin, lmax);
  else if (method == +ConvMethodType::NAIVE_FPR16_METHOD)
    ddbox.convolveNaive(accum.getCumsumData(), length, lmin, lmax);
  else if (method == +ConvMethodType::NEON_FPR16_METHOD)
#if defined(USE_NEON) && defined(__ARM_NEON)
    ddbox.convolveNeon(accum.getCumsumData(), length, lmin, lmax);
#else
    results["status"] = "NEON_FPR16_METHOD method not available";
#endif
  else if (method == +ConvMethodType::AVX2_FPR16_METHOD)
#if defined(USE_AVX2) && defined(__AVX2__)
    ddbox.convolveAvx2(accum.getCumsumData(), length, lmin, lmax);
#else
    results["status"] = "AVX2_FPR16_METHOD method not available";
#endif
  else if (method == +ConvMethodType::HALIDE_FPR16_METHOD)
#if defined(USE_HALIDE)
    ddbox.convolveHalide(accum.getCumsumData(), length, lmin, lmax);
#else
    results["status"] = "HALIDE_FPR16_METHOD method not available";
#endif
}

// ============================================================================

template<class Nms>
int nmsDouble(Nms &nms, double *data, const NmsMethodType method,
              const int width, const int height, json &results)
{
  int no_max = 0;
  nms.reserve(width, height);

  if (method == +NmsMethodType::NAIVE_METHOD)
    no_max = nms.computeNaive(data, width, height, 1, width - 2);
  else if (method == +NmsMethodType::PHAM_METHOD)
    no_max = nms.computePham(data, width, height, 1, width - 2);

  return no_max;
}

// ============================================================================

template<class Nms>
int nmsFpr16(Nms &nms, fpreal16 *data, const NmsMethodType method,
             const int width, const int height, json &results)
{
  int no_max = 0;
  nms.reserve(width, height);

  if (method == +NmsMethodType::NAIVE_FPR16_METHOD)
    no_max = nms.computeNaive(data, width, height, 1, width - 2);
  else if (method == +NmsMethodType::PHAM_FPR16_METHOD)
    no_max = nms.computePham(data, width, height, 1, width - 2);
  else if (method == +NmsMethodType::NEON_FPR16_METHOD)
#if defined(USE_NEON) && defined(__ARM_NEON)
    no_max = nms.computeNeonInt32(reinterpret_cast<const int32_t *>(data),
                                  width, height, 1, width - 2);
#else
    results["nms_status"] = "NEON_FPR16_METHOD method not available";
#endif
  else if (method == +NmsMethodType::AVX2_FPR16_METHOD)
#if defined(USE_AVX2) && defined(__AVX2__)
    no_max = nms.computeAvx2(reinterpret_cast<const int32_t *>(data),
                                  width, height, 1, width - 2);
#else
    results["nms_status"] = "AVX2_FPR16_METHOD method not available";
#endif

  return no_max;
}

// ============================================================================

template<typename T>
void copyArray1D(const json &array, std::vector<T> &data)
{
  int length = array.size();

  data.resize(length);

  for (int i = 0; i < length; i++)
    data[i] = T(array[i]);
}

// ============================================================================

template<typename T>
void copyArray2D(const json &array, std::vector<T> &data)
{
  int height = array.size();

  if (!height)
    return;

  int width = array[0].size();

  data.resize(width * height);

  int k = 0;

  for (const auto &line: array)
    for (const auto &datum : line)
      data[k++] = T(datum.get<double>());
}

// ============================================================================
// ============================================================================

void Tester::peaks1D(const nlohmann::json &params, nlohmann::json &results)
{
  throw std::runtime_error("[dd::Tester::peaks1D] Not implemented");
}

// ============================================================================

void Tester::convolution(const nlohmann::json &params, nlohmann::json &results)
{
  ConvMethodType method = +ConvMethodType::NAIVE_METHOD;
  NmsMethodType nms_method = +NmsMethodType::UNKNOWN_METHOD;

  if (params.find("method") != params.end())
    method = ConvMethodType::_from_string(params.at("method").get<std::string>().c_str());

  if (params.find("nms_method") != params.end())
    {
      nms_method = NmsMethodType::_from_string(params.at("nms_method").get<std::string>().c_str());
      results["nms_method"] = nms_method._to_string();
    }

  results["method"] = method._to_string();
  results["status"] = "OK";

  const auto &data_json = params.at("data");
  int length = data_json.size();

  if (!length)
    {
      results["status"] = "data length == 0";
      return;
    }

  std::vector<double> data;
  copyArray1D(data_json, data);

  uint16_t lmin = 0;
  uint16_t lmax = length - 1;

  std::vector<int32_t> kernel_widths;
  if (params.find("kernel_widths") != params.end())
    {
      const auto &kernel_widths_json = params["kernel_widths"];

      int no_widths = kernel_widths_json.size();
      kernel_widths.resize(no_widths);

      for (int i = 0; i < no_widths; i++)
        kernel_widths[i] = kernel_widths_json[i];
    }

  json cumsum_json = json::array();
  json cconv_json = json::array();
  json maximas_json = json::array();
  unsigned int no_max = 0;

  if (method < +ConvMethodType::FPR16_METHOD)
    {
      Accumulator<double> accum;
      DdBoxDetector<double> ddbox;

      // Convolution
      convolve(method, accum, ddbox, kernel_widths, length, lmin, lmax, data.data(), results);
      copyConvData(accum, ddbox, length, cumsum_json, cconv_json);

      // Nms
      if (nms_method != +NmsMethodType::UNKNOWN_METHOD)
        {
          if (nms_method > +NmsMethodType::FPR16_METHOD)
            throw std::runtime_error(
              std::string("[dd::Tester::convolution] Cannot combine FPR16 and Non-FP16 methods: ") +
              method._to_string() + ", " + nms_method._to_string());

          results["nms_status"] = "OK";

          Nms3x3<double> nms;
          no_max = nmsDouble(nms, ddbox.getData(), nms_method, length, ddbox.getNoScales(), results);

          // Copy maximas
          copyMaximas(nms, no_max, maximas_json);

          // Curvature test
          double curv_ratio = 1.15;

          if (params.find("curv_ratio") != params.end())
            curv_ratio = params["curv_ratio"].get<double>();

          curvatureTest(accum, ddbox, nms, no_max, length, curv_ratio, maximas_json);
        }
    }
  else
    {
      Accumulator<types::fpreal16> accum;
      DdBoxDetector<types::fpreal16> ddbox;

      // Convolution
      convolve(method, accum, ddbox, kernel_widths, length, lmin, lmax, data.data(), results);
      copyConvData(accum, ddbox, length, cumsum_json, cconv_json);

      // Nms
      if (nms_method != +NmsMethodType::UNKNOWN_METHOD)
        {
          if (nms_method < +NmsMethodType::FPR16_METHOD)
            throw std::runtime_error(
              std::string("[dd::Tester::convolution] Cannot combine FPR16 and Non-FP16 methods: ") +
              method._to_string() + ", " + nms_method._to_string());

          results["nms_status"] = "OK";

          Nms3x3<types::fpreal16> nms;
          no_max = nmsFpr16(nms, ddbox.getData(), nms_method, length, ddbox.getNoScales(), results);

          // Copy results
          copyMaximas(nms, no_max, maximas_json);

          // Curvature test
          types::fpreal16 curv_ratio = 1.15;

          if (params.find("curv_ratio") != params.end())
            curv_ratio = types::fpreal16(params["curv_ratio"].get<double>());

          curvatureTest(accum, ddbox, nms, no_max, length, curv_ratio, maximas_json);
        }
    }

  // Save results
  results["cumsum"].swap(cumsum_json);
  results["cconv"].swap(cconv_json);
  results["maximas"].swap(maximas_json);
}

// ============================================================================

void Tester::nonMaximaSuppression(const nlohmann::json &params, nlohmann::json &results)
{
  NmsMethodType method = +NmsMethodType::NAIVE_METHOD;

  unsigned int no_max = 0;
  results["status"] = "OK";

  if (params.find("method") != params.end())
    method = NmsMethodType::_from_string(params.at("method").get<std::string>().c_str());

  const auto &data_json = params.at("data");
  int height = data_json.size();

  if (!height)
    {
      results["status"] = "data height == 0";
      return;
    }

  int width = data_json[0].size();

  results["maximas"] = json::array();
  results["method"] = method._to_string();

  json maximas_json = json::array();

  if (method < +NmsMethodType::FPR16_METHOD)
    {
      std::vector<double> data;   
      copyArray2D(data_json, data);

      Nms3x3<double> nms;      
      no_max = nmsDouble(nms, data.data(), method, width, height, results);

      // Copy results
      copyMaximas(nms, no_max, maximas_json);
    }
  else
    {
      std::vector<fpreal16> data;
      copyArray2D(data_json, data);

      Nms3x3<fpreal16> nms;
      no_max = nmsFpr16(nms, data.data(), method, width, height, results);

      // Copy results
      copyMaximas(nms, no_max, maximas_json);
    }

  results["maximas"].swap(maximas_json);
}

// ============================================================================
// ============================================================================

void Tester::process(const nlohmann::json &params, nlohmann::json &results)
{
  std::string method = params.at("method").get<std::string>();

  if (method == "nonMaximaSuppression")
    nonMaximaSuppression(params.at("params"), results);
  else if (method == "convolution")
    convolution(params.at("params"), results);
  else if (method == "peaks1D")
    peaks1D(params.at("params"), results);
  else
    throw std::runtime_error(std::string("[dd::Tester::process] Unknown method name :")
                                         + method);
}

#endif // MKEDD_RESERVED_API
