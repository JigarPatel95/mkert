/*
 * Accumulator
 *
 * Copyright (c) 2018, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */


#ifndef _DD_ACCUMULATOR_H_
#define _DD_ACCUMULATOR_H_

#include <cstdint>
#include <vector>
#include <boost/align/aligned_allocator.hpp>

#ifdef __ARM_NEON
#include "neon/neon_bits.h"
#endif

#include "types/fpreal.h"
#include "dd/pxlist.h"
#include "dd/colpath.h"

namespace mke {
namespace dd {
namespace dd {

/** \addtogroup mkedd
 *  @{
 */

template <typename T, int csshift = 8>
class Accumulator {
private:
 std::vector<T, boost::alignment::aligned_allocator<T, 16>> accum_;
 std::vector<T, boost::alignment::aligned_allocator<T, 16>> cumsum_;

 uint32_t length_;
 uint32_t clength_;

public:
 void reserve(const uint32_t length)
 {
   length_ = length;

   uint32_t size = (length & 0xFFFC) + 4; // (length / 4) * 4 + 4

   clength_ = size;

   accum_.reserve(size);
   cumsum_.reserve(size);
 }

 // ===========================================================================

 inline void clear(void)
 {
   memset(accum_.data(), 0, accum_.size() * sizeof(T));
   memset(cumsum_.data(), 0, cumsum_.size() * sizeof(T));
 }

 // ===========================================================================

 inline void processByPixelList(const uint8_t *data, const CollatedPath &path)
 {
   // Clear accumulator
   memset(accum_.data(), 0, ((clength_ * sizeof(T)) & 0xFFFC) + 4);
   clength_ = path.getNumPathElements();

   // Accumulate
   const auto &pxlist = path.getPixelList();
   const auto &stlist = pxlist.getStepInfoList();
   const uint8_t *ptr = data + pxlist.getBufferOffset();

   for (const auto &stinfo : stlist)
     {
       ptr += stinfo.step;

       T weight = T(stinfo.w, true);
       accum_[stinfo.j] += T(int32_t(*ptr)) * weight;
     }

   // Cumulative sum
   T csum = 0;

   for (uint32_t i = 0; i < clength_; i++)
     {
       T accel = accum_[i] >> csshift;
       csum += accel;
       cumsum_[i] = csum;
     }
 }

 // ===========================================================================

 /*
 #ifndef  __ARM_NEON
 inline void processByPathElements(const uint8_t *data, const CollatedPath &path, uint32_t stride)
 {
   clength_ = path.getNumPathElements();
   T csum = 0;

   int32x4_t fmask = vdupq_n_s32(0xFFFF);
   int32x4_t shr16 = vdupq_n_s32(-16);
asm("nop");
   // Accumulate via bilinear interpolation
   for (uint32_t i = 0; i < clength_; i++)
     {
       int32x2_t uv, ld, ud, psum;
       int32x4_t xs, ys, ds, ws;

       const auto &pel = path.getPathElement(i);

       uint16_t x = pel.u >> 16;
       uint16_t y = pel.v >> 16;

       uint32_t off1 = y * stride + x;
       uint32_t off2 = off1 + stride;

       ld[1] = int32_t(data[off1]);
       ld[0] = int32_t(data[off1 + 1]);
       ud[1] = int32_t(data[off2]);
       ud[0] = int32_t(data[off2 + 1]);
       ds = vcombine_s32(ud, ld);

       uv[0] = pel.u;
       uv[1] = ~pel.u;
       xs = vcombine_s32(uv, uv);

       uv[0] = pel.v;
       uv[1] = pel.v;
       ys = vcombine_s32(uv, ~uv);

       xs = vandq_s32(xs, fmask);
       ys = vandq_s32(ys, fmask);
       ws = reinterpret_cast<int32x4_t>(vshlq_u32(reinterpret_cast<uint32x4_t>(vmulq_s32(xs, ys)), shr16));

       ds = vmulq_s32(ds, ws);
       psum = vadd_s32(vget_high_s32(ds), vget_low_s32(ds));

       T accel;
       accel.assign(vget_lane_s32(vpadd_s32(psum, psum), 0));

       // Accumulator
       accum_[i] = accel;

       // Cumulative sum
       csum += accel >> csshift;
       cumsum_[i] = csum;
     }
   asm("nop");
 }
#else
*/
 inline void processByPathElements(const uint8_t *data,
                                   const CollatedPath &path,
                                   uint32_t stride)
 {
   clength_ = path.getNumPathElements();
   T csum = 0;

   // Accumulate via bilinear interpolation
   for (uint32_t i = 0; i < clength_; i++)
     {
       const auto &pel = path.getPathElement(i);

       uint32_t x1 = pel.u & 0xFFFF; // fractional part
       uint32_t x0 = 0x10000 - x1;
       uint16_t x = pel.u >> 16;     // integer part

       uint32_t y1 = pel.v & 0xFFFF; // fractional part
       uint32_t y0 = 0x10000 - y1;
       uint16_t y = pel.v >> 16;     // integer part

       uint32_t w00 = (x0 * y0) >> 16;
       uint32_t w10 = (x1 * y0) >> 16;
       uint32_t w01 = (x0 * y1) >> 16;
       uint32_t w11 = (x1 * y1) >> 16;

       uint32_t off1 = y * stride + x;
       uint32_t off2 = off1 + stride;

       uint16_t d0 = *((uint16_t *) (data + off1));
       uint16_t d1 = *((uint16_t *) (data + off2));

       T accel;

       accel.assign(w00 * uint32_t(d0 & 0xFF) + w10 * uint32_t(d0 >> 8) +
                    w01 * uint32_t(d1 & 0xFF) + w11 * uint32_t(d1 >> 8));

       // Accumulator
       accum_[i] = accel;

       // Cumulative sum
       csum += accel >> csshift;
       cumsum_[i] = csum;
     }
 }

 // ===========================================================================
 // ===========================================================================

 inline void processByPathElements(const uint8_t *data,
                                   const CollatedLitePath &path,
                                   uint32_t stride)
 {
   clength_ = path.getNumPathElements();
   T csum = 0;

   // Accumulate via bilinear interpolation
   for (uint32_t i = 0; i < clength_; i++)
     {
       const auto &pel = path.getPathElement(i);

       uint32_t x1 = uint32_t(pel.uvf >> 8); // fractional part
       uint32_t x0 = 0x100 - x1;
       uint32_t x = uint32_t(pel.ui);         // integer part

       uint32_t y1 = uint32_t(pel.uvf & 0x00FF); // fractional part
       uint32_t y0 = 0x100 - y1;
       uint32_t y = uint32_t(pel.vi);     // integer part

       uint32_t w00 = (x0 * y0); // uint32_t * uint32_t => uint32_t
       uint32_t w10 = (x1 * y0);
       uint32_t w01 = (x0 * y1);
       uint32_t w11 = (x1 * y1);

       uint32_t off1 = y * stride + x;
       uint32_t off2 = off1 + stride;

       uint16_t d0 = *((uint16_t *) (data + off1));
       uint16_t d1 = *((uint16_t *) (data + off2));

       T accel;

       accel.assign(w00 * uint32_t(d0 & 0xFF) + w10 * uint32_t(d0 >> 8) +
                    w01 * uint32_t(d1 & 0xFF) + w11 * uint32_t(d1 >> 8));

       // Accumulator
       accum_[i] = accel;

       // Cumulative sum
       csum += accel >> csshift;
       cumsum_[i] = csum;
     }
 }

 // ===========================================================================

 /*
 inline void processByPathElements(const uint8_t *data,
                                       const CollatedLitePath &path,
                                       uint32_t stride)
 {
//   std::cout  << std::endl  << "processByPathElements" << __FILE__ << ":" << __LINE__ << ":" << std::endl;
   clength_ = path.getNumPathElements();
   T csum = 0;

   int steps = clength_ >> 2; //  / 4
   int rem = (clength_ & 3); // % 4

   const int32x4_t _8s = vdupq_n_s32(8);
   const int32x4_t _m8s = vdupq_n_s32(-8);
   const uint32x4_t _0x100s = vdupq_n_u32(0x100);
   const uint32x4_t _0xFFs = vdupq_n_u32(0xFF);
   const uint32x4_t strides = vdupq_n_u32(stride);
  // const uint16_t *dptr = (uint16_t *) data;

   // Accumulate via bilinear interpolation
   uint32_t i = 0;
   uint32x4_t *aptr = reinterpret_cast<uint32x4_t *>(accum_.data());
   uint32x4_t *cptr = reinterpret_cast<uint32x4_t *>(cumsum_.data());
   //uint32x4_t dptr = vdupq_n_u32(reinterpret_cast<uint32x4_t *>(data));

   for (uint32_t s = 0; s < steps; s++, i += 4)
     {
      // const auto &pel = path.getPathElement(i);
//std::cout << __FILE__ << ":" << __LINE__ << ":" << s << std::endl;
       uint32x4x4_t mpels, tmpels;

       mpels.val[0] = mke::dd::neon::load_from_uint16x4(&(path.getPathElement(i + 0)));
       mpels.val[1] = mke::dd::neon::load_from_uint16x4(&(path.getPathElement(i + 1)));
       mpels.val[2] = mke::dd::neon::load_from_uint16x4(&(path.getPathElement(i + 2)));
       mpels.val[3] = mke::dd::neon::load_from_uint16x4(&(path.getPathElement(i + 3)));

       tmpels = mke::dd::neon::transpose4x4(mpels);

//std::cout << "   " <<__LINE__ << std::endl;

       uint32x4_t x1 = vshlq_u32(tmpels.val[2], _m8s); // fractional part
       uint32x4_t x0 = _0x100s - x1;
       uint32x4_t x = tmpels.val[0];         // integer part

//std::cout << "   " <<__LINE__ << std::endl;

       uint32x4_t y1 = tmpels.val[2] & _0xFFs; // fractional part
       uint32x4_t y0 = _0x100s - y1;
       uint32x4_t y = tmpels.val[1];         // integer part

//std::cout << "   " <<__LINE__ << std::endl;



       uint32x4_t off1 = y * strides + x;
       uint32x4_t off2 = off1 + strides;

//std::cout << "   " <<__LINE__ << std::endl;
       uint32x4_t d0s;
       uint32x4_t d1s;

//std::cout << "   " <<__LINE__ << std::endl;


//mke::dd::neon::print4(x);
//mke::dd::neon::print4(y);

//std::cout << "   " <<__LINE__ << std::endl;

       mke::dd::neon::gather<uint16_t>(data, off1, d0s);
       mke::dd::neon::gather<uint16_t>(data, off2, d1s);

       uint32x4_t w00 = x0 * y0; // uint32_t * uint32_t => uint32_t
       uint32x4_t w10 = x1 * y0;
       uint32x4_t w01 = x0 * y1;
       uint32x4_t w11 = x1 * y1;

//mke::dd::neon::print4(d0s);
//mke::dd::neon::print4(d1s);

//std::cout << "   " <<__LINE__ << std::endl;

       uint32x4_t as = w00 * (d0s & _0xFFs) + w10 * vshlq_u32(d0s, _m8s) +
                       w01 * (d1s & _0xFFs) + w11 * vshlq_u32(d1s, _m8s);

       *aptr++ = as;

//mke::dd::neon::print4fpr16(as);
       as = vshlq_u32(as, _m8s);

//mke::dd::neon::print4fpr16(as);
//std::cout << "   " <<__LINE__ << std::endl;

        uint32x4_t cs;

#pragma unroll
       for (int j = 0; j < 4; j++)
         {
           csum += types::fpreal16(as[j], true);
           cs[j] = csum.getValue();
         }

       *cptr++ = cs;

     }

  // exit(0);

//std::cout  << std::endl  << "CPU " << __FILE__ << ":" << __LINE__ << ":" << std::endl;

   // Accumulate via bilinear interpolation
   for (uint32_t j = 0; j < rem; j++, i++)
     {
//std::cout   << "CPU " << __FILE__ << ":" << __LINE__ << ":" << i << std::endl;
       const auto &pel = path.getPathElement(i);

       uint32_t x1 = uint32_t(pel.uvf >> 8); // fractional part
       uint32_t x0 = 0x100 - x1;
       uint32_t x = uint32_t(pel.ui);         // integer part

       uint32_t y1 = uint32_t(pel.uvf & 0x00FF); // fractional part
       uint32_t y0 = 0x100 - y1;
       uint32_t y = uint32_t(pel.vi);     // integer part

       uint32_t w00 = (x0 * y0); // uint32_t * uint32_t => uint32_t
       uint32_t w10 = (x1 * y0);
       uint32_t w01 = (x0 * y1);
       uint32_t w11 = (x1 * y1);

       uint32_t off1 = y * stride + x;
       uint32_t off2 = off1 + stride;

       uint16_t d0 = *((uint16_t *) (data + off1));
       uint16_t d1 = *((uint16_t *) (data + off2));

       T accel;

       accel.assign(w00 * uint32_t(d0 & 0xFF) + w10 * uint32_t(d0 >> 8) +
                    w01 * uint32_t(d1 & 0xFF) + w11 * uint32_t(d1 >> 8));

       // Accumulator
       accum_[i] = accel;

       // Cumulative sum
       csum += accel >> csshift;
       cumsum_[i] = csum;
     }
 }
 */

 // ===========================================================================
 // ===========================================================================

 inline void processFromArray(const double *data, const uint32_t length)
 {
   if (std::is_same<T, types::fpreal16>::value)
     processFromArrayFpr16(data, length);
   else
     processFromArrayNoShift(data, length);
 }

 // ===========================================================================

 inline void processFromArrayNoShift(const double *data, const uint32_t length)
 {
   T csum = 0;

   // Copy data from array
   for (uint32_t i = 0; i < length; i++)
     {
       // Copy data
       T accel = T(data[i]);
       accum_[i] = accel;
       csum += accel;
       cumsum_[i] = csum;
     }
 }

 // ===========================================================================

 inline void processFromArrayFpr16(const double *data, const uint32_t length)
 {
   types::fpreal16 csum = 0;

   // Copy data from array
   for (uint32_t i = 0; i < length; i++)
     {
       // Copy data
       types::fpreal16 accel = types::fpreal16(data[i]);
       accum_[i] = T(accel);

       csum += accel >> csshift;
       cumsum_[i] = T(csum);
     }
 }

 // ===========================================================================
 // ===========================================================================

 inline const T* getAccumData(void) const
 {
   return accum_.data();
 }

 // ===========================================================================

 inline const T* getCumsumData(void) const
 {
   return cumsum_.data();
 }

 // ===========================================================================

};

/** @}*/

} // namespace dd
} // namespace cc
} // namespace mke

#endif // _DD_ACCUMULATOR_H_
