/*
 * IntLutDetector, BoxLut based detector with ray interpolation
 *
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifndef _DD_INTLUT_H_
#define _DD_INTLUT_H_


#include <vector>

#include "types/fpreal.h"
#include "dd/ddbox.h"
#include "dd/detector.h"
#include "dd/colpath.h"
#include "dd/iray.h"
#include "dd/accum.h"
#include "dc/classifier.h"
#include "util/datalut.h"
#include "util/ringbuffer.h"
#include "util/timers.h"

#include "util/pmap.h"

#ifdef MKEDD_MULTITHREADING
#include "util/threadpool.h"
#endif

namespace mke {
namespace dd {
namespace dd {

/** \addtogroup mkedd
 *  @{
 */

class IntLutDetector : public Detector {
private:

  // Params
  std::vector<int32_t> kernel_widths_;
  uint8_t threshold_param_;
  types::fpreal16 adjacency_threshold_param_;
  bool fill_holes_param_;
  uint32_t hole_filling_support_param_;
  uint32_t hole_filling_level_param_;
  types::fpreal16 peak_ratio_param_;
  types::fpreal16 curv_ratio_param_;
  bool fit_peaks_param_;
  types::fpreal16 box_delta_param_;
  uint8_t no_epochs_param_;
  uint32_t no_threads_param_;
  uint32_t sdlevel_param_;
  uint32_t laplace_smoothing_param_;

  util::ParameterMap pmap_;
  static const nlohmann::json param_schema_;

  // Dot classifier
  std::unique_ptr<dc::Classifier> classifier_;

  // 2D buffer info
  uint32_t iwidth_;
  uint32_t iheight_;
  uint32_t stride_;

  // 3D info
  uint32_t depth_min_;
  uint32_t depth_max_;
  mke::dd::Data3dType data3d_type_;

  // Paths
  struct PathsSlice {
    std::vector<uint32_t> path_idx;
    DdBoxDetector<types::fpreal16> ddbox;
    Accumulator<types::fpreal16> accum;
    const DdBoxDetector<types::fpreal16>::PeakInfo *peak_info;
    types::fpreal16 peak_offset;
    uint32_t no_detections;
  };

  std::vector<CollatedLitePath> paths_;
  std::vector<PathsSlice> path_slices_;
  uint32_t max_pels_;
  std::vector<bool> valid_flags_;

  // IntRays
  struct InterpolatedRaysSlice {
    std::vector<uint32_t> ray_idx;
    uint32_t no_detections;
  };

  std::vector<uint32_t> ilevels_;
  uint32_t ids_per_iray_;
  uint32_t sdlevel_;
  uint32_t sdlevel_rem_;
  std::vector<InterpolatedRay> irays_;
  std::vector<std::vector<InterpolatedRaysSlice>> irays_slices_;

  // Laplace smoothing
  struct LaplaceData
  {
    util::RingBuffer<uint32_t, 8> buffer;
    int16_t x, y, z;
  };

  std::vector<LaplaceData> laplace_;

  // History
  std::vector<uint8_t> valid_epochs_;
  std::vector<types::fpreal16> peak_history_;
  std::vector<PointData> dets_history_;

  // Results
  uint32_t no_detections_;

  // Runtime statistics
  util::StatsThreadTimer user_timer_;
  util::StatsThreadTimer pxlist_timer_;
  util::StatsThreadTimer acclist_timer_;
  util::StatsThreadTimer conv_timer_;
  util::StatsThreadTimer nms_timer_;
  util::StatsThreadTimer bpeak_timer_;
  util::StatsThreadTimer irays_timer_;
  util::StatsThreadTimer alist_timer_;
  util::StatsThreadTimer hfill_timer_;

  // Utils
  typedef std::array<types::fpreal16, 3> BaryCoords;

  struct TriangleInfo
  {
    std::array<PointData*, 3> dets;
    InterpolatedRay::tri_t tri;
    PointData *res;
    uint32_t didx;
    uint32_t uid;
  };

  const static uint32_t max_dpp_ = 100;
  const static int32_t csshift_ = 8;  
  const static std::vector<std::vector<BaryCoords>> sd_bcoords_;

#ifdef MKEDD_MULTITHREADING
  util::ThreadPool<util::SHARED_THREADPOOL> threadpool_;
#endif

#ifdef MKEDD_RESERVED_API
  static const nlohmann::json reserved_param_schema_;
  nlohmann::json dbgdata_;
  bool store_dbg_data_;  
  bool compute_triangulation_param_;
  nlohmann::json triangulation_;
#endif

  // ==========================================================================
  // Data

  void setInitValues(void);
  void computeMaxSdLevels(const uint32_t ids_per_ray, uint32_t &sdlevel, uint32_t &rem);

  // ==========================================================================
  // Parameters

  void updateParams(const nlohmann::json &params, bool value_only = false);

  // ==========================================================================
  // Data loading

  void loadLut(std::istream *is, const SectionType section);

  // ==========================================================================
  // Processing

  int selectBestPeak(const uint8_t *data, PathsSlice &pslice, const CollatedLitePath &path);

  void setEmptyDetection(const uint32_t pidx, PointData &detection);
  uint32_t setDetection(const uint32_t pidx, PathsSlice &pslice, PointData &detection);

  void incValidEpochs(const uint32_t pidx, const types::fpreal16 offset);
  void decValidEpochs(const uint32_t pidx);
  uint32_t applyBoxingFilter(const uint32_t pidx, PathsSlice &pslice, PointData &detection);

  void processPathSlice(const uint8_t *data, PathsSlice &pslice, std::vector<PointData>& detections);
  void processPaths(const uint8_t *data, std::vector<PointData>& detections);
  void setupSlices(const unsigned int no_slices);

  void interpolateDetection(PointData &res, const uint32_t uid, TriangleInfo &triangle, const BaryCoords &bc);
  void interpolateDetection(TriangleInfo &triangle, const BaryCoords &bc);
  void interpolateDetection(TriangleInfo &triangle, const uint8_t idx1, const uint8_t idx2);
  void getTriangleInfo(const uint32_t ridx, const uint32_t didx, TriangleInfo &triangle, std::vector<PointData>& detections);
  void clearRaySlice(InterpolatedRaysSlice &irslice, std::vector<PointData>& detections);
  void processMultiLevelRaySlice(InterpolatedRaysSlice &irslice, std::vector<PointData>& detections);
  void processSingleLevelRaySlice(InterpolatedRaysSlice &irslice, std::vector<PointData>& detections);
  void processInterpolatedRays(std::vector<PointData>& detections);

  void processOutlierSlice(PathsSlice &irslice, std::vector<PointData>& detections);
  void performOutlierRemoval(std::vector<PointData>& detections);

  void processHoleSlice(PathsSlice &pslice, std::vector<PointData>& detections);
  void performHoleFilling(std::vector<PointData>& detections);

  void updateLaplaceDetections(const uint32_t iter, std::vector<PointData>& detections);
  void processLaplaceSmoothingSlice(const uint32_t init_idx, const uint32_t step,
                                    const uint32_t iter, std::vector<PointData>& detections);
  void performLaplaceSmoothing(std::vector<PointData>& detections);

#ifdef MKEDD_RESERVED_API
  bool getTriangulation(std::vector<PointData>& detections, nlohmann::json &triangulation);
#endif

public:

  // ==========================================================================
  // Static methods

  static const std::string staticGetTypeString(void)
  {
    static const std::string typeString = "IntLut";
    return typeString;
  }

  static Detector* create(void)
  {
    return new IntLutDetector();
  }

  // ==========================================================================
  // Construction

  IntLutDetector()
  {
    // Initial parameter values
    pmap_.addSchema(param_schema_);

#ifdef MKEDD_RESERVED_API
    pmap_.addSchema(reserved_param_schema_);
#endif

    updateParams(pmap_.getParams(), true);

   setInitValues();
  }

  ~IntLutDetector()
    {}

  // ==========================================================================
  // Parameters

  void validateParams(const nlohmann::json &params);
  nlohmann::json getParamsSchema(void) const;
  void setParams(const nlohmann::json &params);
  void getParams(nlohmann::json &params) const;
  const std::string getTypeString(void);

  // ==========================================================================
  // Runtime statistics

  virtual void resetStats(void);
  virtual void getStats(nlohmann::json &stats) const;

  // ==========================================================================
  // Data loading

  bool canParse(const SectionType section);
  void load(std::istream *is, const SectionType section);

  // ==========================================================================
  // Processing

  uint32_t process(const uint8_t *data, std::vector<PointData>& detections);
  uint32_t getMaxNoDetections(void) const;
  uint32_t getStride(void) const;
  Data3dType getData3dType(void) const;

  // ==========================================================================
  // Reserved API

#ifdef MKEDD_RESERVED_API
  void getDebugData(nlohmann::json &data) const;
  void getTriangulation(nlohmann::json &triangulation);
#endif

};

/** @}*/

} /* namespace dd */
} /* namespace dd */
} /* namespace mke */


#endif // _DD_INTLUT_H_
