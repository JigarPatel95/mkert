/*
 * mkedd - MkE DdBoxLut
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifndef _DD_BOXLUT_H_ 
#define _DD_BOXLUT_H_ 

#include <vector>

#include "types/fpreal.h"
#include "dd/ddbox.h"
#include "dd/detector.h"
#include "dc/classifier.h"
#include "util/datalut.h"
#include "util/timers.h"

#include "util/pmap.h"

#ifdef MKEDD_MULTITHREADING
#include "util/threadpool.h"
#endif

namespace mke {
namespace dd {
namespace dd {
  
/** \addtogroup mkedd
 *  @{
 */
  

class BoxLutDetector : public Detector {
private:
  
  typedef DdBoxDetector<types::fpreal16> DdBox;
  typedef DdBoxDetector<types::fpreal16>::PeakInfo PeakInfo;
  
  /* Keep this 32-bit aligned */
  struct PixelEntry {
    uint16_t offset;
    uint16_t acc_idx;
    uint16_t ael_idx;
    uint16_t weight;
  };  
  
  struct AccumSlice {
    uint32_t accum_min;
    uint32_t accum_max;
    DdBox ddbox;
    uint32_t no_detections;
  };
  
  struct PixelSlice {
    uint32_t pix_min;
    uint32_t pix_max;
    uint32_t offset;
  };
  
  // Params
  std::vector<int32_t> kernel_widths_;
  uint8_t threshold_param_;
  types::fpreal16 peak_ratio_param_;
  types::fpreal16 curv_ratio_param_;
  types::fpreal16 box_delta_param_;
  uint8_t no_epochs_param_;  
#ifdef MKEDD_RESERVED_API
  std::string accdump_file_param_;
#endif
  uint32_t no_threads_param_;

  util::ParameterMap pmap_;
  static const nlohmann::json param_schema_;
  
  // Calib Lut
  util::DataLut<PointDataLut> lut_;
  
  // Dot classifier
  std::unique_ptr<dc::Classifier> classifier_;
 
  // 2D buffer info
  uint32_t iwidth_;
  uint32_t iheight_;
  uint32_t stride_;
  
  // 3D info
  uint32_t depth_min_;
  uint32_t depth_max_;
  
  // Accums
  uint32_t no_accums_;
  uint32_t no_accels_;
  uint32_t no_pxs_;
  
  std::vector<PixelEntry> pixellist_;
  std::vector<uint8_t> aflags_;
  std::vector<uint32_t> afaidx_;  
  std::vector<uint32_t> afbidx_;
  std::vector<uint32_t> acclens_;
  std::vector<uint32_t> accoffs_; 
  std::vector<types::fpreal16> accums_;
  std::vector<types::fpreal16> accumsums_;
  std::vector<AccumSlice> accslices_;
  std::vector<PixelSlice> pixslices_;

  // History  
  std::vector<uint8_t> valid_epochs_; 
  std::vector<types::fpreal16> peak_history_;
  std::vector<PointData> dts_history_;
  
  // Results
  unsigned int no_detections_;  
  
  const static uint32_t max_dpp_ = 100;
  const static int32_t csshift_ = 8;
  uint32_t max_acclen_;
  
#ifdef MKEDD_MULTITHREADING
  util::ThreadPool<util::SHARED_THREADPOOL> threadpool_;
#endif  
  
  // ==========================================================================
  // Data

  void setInitValues(void);
  void allocAccumMemory(void);
  void allocPixelListMemory(void);
  void setupAccumSlices(void);
  
  // ==========================================================================
  // Parameters 

  void updateParams(const nlohmann::json &params, bool value_only = false);

  // ==========================================================================
  // Runtime statistics
  util::StatsThreadTimer user_timer_;
  util::StatsThreadTimer pxlist_timer_;
  util::StatsThreadTimer acclist_timer_;
  util::StatsThreadTimer conv_timer_;
  util::StatsThreadTimer nms_timer_;  
  util::StatsThreadTimer bpeak_timer_;
  
#ifdef MKEDD_MULTITHREADING
  bool isValidNoThreads(const uint32_t &no_threads);
#endif
  
  // ==========================================================================
  // Data loading
  
  void loadPixelList(std::istream *is);
  void loadAccumLengths(std::istream *is); 
  void loadAccumFlags(std::istream *is);
  void loadAccumFlagsBounds(std::istream *is);

  void loadAccumulatorsXY(std::istream *is); 
  void loadLut3D2DID(std::istream *is);
  void loadLutsV2(std::istream *is);
  
  // ==========================================================================
  // Processing
  
  void processPixelList(const uint8_t *data);
  void processPixelSlice(const uint8_t *data, const unsigned int ps_idx);
  void processAccumulators(const uint8_t *data, std::vector<PointData>& detections);
  void processAccumSlice(const uint8_t *data, const unsigned int acc_idx, std::vector<PointData>& detections);
  void computeConvolution(const unsigned int slength, const unsigned int aidx, const unsigned int bidx, const types::fpreal16 *accum, types::fpreal16 *accsum, DdBox &ddbox);
  unsigned int findPeaks(const unsigned int slength, const unsigned int aidx, const unsigned int bidx, const uint8_t *accflags, DdBox &ddbox);
  int selectBestPeak(const unsigned int slength, const unsigned int accoff, const types::fpreal16 *accum, const uint8_t *data, DdBox &ddbox);
  void setEmptyDetection(const unsigned int acc_idx, PointData &detection);
  bool setDetection(const unsigned int accum_idx, const unsigned int accel_offset, const types::fpreal16 offset, const DdBox &ddbox, const PeakInfo &peak, PointData &detection);
  unsigned int applyBoxingFilter(const unsigned int accum_idx, const unsigned int accel_offset, const types::fpreal16 offset, const DdBox &ddbox, const PeakInfo &peak, PointData &detection);
  void decValidEpochs(const unsigned int accum_idx);
  void incValidEpochs(const unsigned int accum_idx, const types::fpreal16 offset);
  
  
  // ==========================================================================
  // Reserved API
  
#ifdef MKEDD_RESERVED_API
  std::vector<uint32_t> det_accels_;

  void dumpAccumulators(void);
#endif
  
  // ==========================================================================
  // ==========================================================================
  
public:

  // ==========================================================================
  // Static methods
  
  static const std::string staticGetTypeString(void)
  {
    static const std::string typeString = "BoxLut";
    return typeString;
  }
  
  static Detector* create(void)
  {
    return new BoxLutDetector();
  }

  // ==========================================================================
  // Construction 
  
  BoxLutDetector()
  { 
    setInitValues();
  }
  
  ~BoxLutDetector()
    {}

  // ==========================================================================
  // Parameters 

  void validateParams(const nlohmann::json &params);
  nlohmann::json getParamsSchema(void) const;
  void setParams(const nlohmann::json &params);
  void getParams(nlohmann::json &params) const;
  const std::string getTypeString(void);

  // ==========================================================================
  // Runtime statistics
  
  virtual void resetStats(void);
  virtual void getStats(nlohmann::json &stats) const;
  
  // ==========================================================================
  // Data loading
  
  bool canParse(const SectionType section);
  void load(std::istream *is, const SectionType section);
  
  // ==========================================================================
  // Processing
  
  uint32_t process(const uint8_t *data, std::vector<PointData>& detections);
  uint32_t getMaxNoDetections(void) const;
  uint32_t getStride(void) const;
  mke::dd::Data3dType getData3dType(void) const;

  // ==========================================================================
  // Reserved API

#ifdef MKEDD_RESERVED_API
  void getDebugData(nlohmann::json &data) const;
  void getTriangulation(nlohmann::json &triangulation);
#endif
    
};

/** @}*/

} /* namespace dd */
} /* namespace dd */
} /* namespace mke */


#endif // _DD_BOXLUT_H_ 
