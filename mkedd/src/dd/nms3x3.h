/*
 * nm3x3 - implementation of 3x3 non-maxima supression,
 * 
 * Copyright (c) 2016--2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifndef _NMS3X3_H_
#define _NMS3X3_H_

#include <vector>
#include <cstring>
#include <cassert>
#include <cstdint>
#include <type_traits>

#include "types/fpreal.h"

#if defined(USE_NEON) && defined(__ARM_NEON)
#include "neon/neon_bits.h"
#endif

#if defined(USE_AVX2) && defined(__AVX2__)
#include <immintrin.h>
#endif


namespace mke {
namespace dd {
namespace dd {


/** \addtogroup mkedd
 *  @{
 */
  
template <typename T> class Nms3x3 {
public:
    struct Maximum
    {
        int x;
        int y;
    };
    
private:
    int width_;
    int height_;
    
    int orig_width_;
    int orig_height_;
    
    const T           *data_; 
    std::vector<char> scan_;
    std::vector<Maximum> res_;
    
    void alloc() 
    {
      scan_.resize(orig_width_ * 2, 0);
      res_.resize((orig_width_ - 1) * (orig_height_ - 1), Maximum{0, 0});
    }
    
    void dealloc() 
    {
      //
    }
    
    void inline setData(const T* data) 
    {
      data_ = data;
    }
    
    inline T get(int x, int y) 
    {
      int idx = y * width_ + x;
      
      assert(idx <= (orig_width_ * orig_height_ - 1));
      
      return data_[idx];
    }

    inline void addPeak(int &no_max, const int &x, const int &y)
    {
      res_[no_max].x = x;
      res_[no_max].y = y;
      no_max++;
    }

#if defined(USE_AVX2) && defined(__AVX2__)
    inline void gatherNmsGridAxv2(
        const int32_t *ptr, const int32_t stride,
      __m256i &ul, __m256i &uc, __m256i &ur,
      __m256i &cl, __m256i &cc, __m256i &cr,
      __m256i &dl, __m256i &dc, __m256i &dr)
    {
       ul = _mm256_loadu_si256((__m256i const *) (ptr - stride - 1));
       uc = _mm256_loadu_si256((__m256i const *) (ptr - stride));
       ur = _mm256_loadu_si256((__m256i const *) (ptr - stride + 1));

       cl = _mm256_loadu_si256((__m256i const *) (ptr - 1));
       cc = _mm256_loadu_si256((__m256i const *) (ptr));
       cr = _mm256_loadu_si256((__m256i const *) (ptr + 1));

       dl = _mm256_loadu_si256((__m256i const *) (ptr + stride - 1));
       dc = _mm256_loadu_si256((__m256i const *) (ptr + stride));
       dr = _mm256_loadu_si256((__m256i const *) (ptr + stride + 1));
    }

    inline void getNmsMaskAxv2(
      __m256i &mask, const __m256i &th,
      const __m256i &ul, const __m256i &uc, const __m256i &ur,
      const __m256i &cl, const __m256i &cc, const __m256i &cr,
      const __m256i &dl, const __m256i &dc, const __m256i &dr)
    {
      mask = _mm256_cmpgt_epi32(cc, ul) & _mm256_cmpgt_epi32(cc, uc) & _mm256_cmpgt_epi32(cc, ur) &
             _mm256_cmpgt_epi32(cc, cl) & _mm256_cmpgt_epi32(cc, th) & _mm256_cmpgt_epi32(cc, cr) &
             _mm256_cmpgt_epi32(cc, dl) & _mm256_cmpgt_epi32(cc, dc) & _mm256_cmpgt_epi32(cc, dr);
    }

    inline void addPeaksAvx2(int &no_max, const int &x, const int &y, const int *masks)
    {
      for (int i = 0; i < 8; i++)
        if (masks[i])
          addPeak(no_max, x + i, y);
    }

    inline void avx2Int32Step(const int32_t *ptr, const int &stride, const __m256i &thresh,
                              const int &x, const int &y, int &no_max, const int &rem = 0)
    {
      __m256i ul, uc, ur, cl, cc, cr, dl, dc, dr;
      __m256i mask;

      gatherNmsGridAxv2(ptr + x, stride, ul, uc, ur, cl, cc, cr, dl, dc, dr);
      getNmsMaskAxv2(mask, thresh, ul, uc, ur, cl, cc, cr, dl, dc, dr);

      int masks[8] __attribute__((aligned(32)));
      _mm256_store_si256((__m256i *) masks, mask);

      if (rem)
        {
          for (int i = 0; i < rem; i++)
            masks[i] = 0;
        }

      addPeaksAvx2(no_max, x, y, masks);
    }
#endif

#if defined(USE_NEON) && defined(__ARM_NEON)

    inline void gatherNmsGrid(const int32_t *ptr, uint32_t stride, 
      int32x4_t &ul, int32x4_t &u, int32x4_t &ur,
      int32x4_t &cl, int32x4_t &c, int32x4_t &cr,
      int32x4_t &dl, int32x4_t &d, int32x4_t &dr)
    {

      /*
      ul = neon::load4(ptr -stride - 1);
      u =  neon::load4(ptr - stride);
      ur = neon::load4(ptr -stride + 1);

      cl = neon::load4(ptr -1);
      c  = neon::load4(ptr);
      cr = neon::load4(ptr + 1);

      dl = neon::load4(ptr + stride - 1);
      d  = neon::load4(ptr + stride);
      dr = neon::load4(ptr + stride + 1);
      */

      // Load data
      int32_t ull = *(ptr - stride - 1);
      u = neon::load4(ptr - stride);
      int32_t urr = *(ptr - stride + 4);

      int32_t cll = *(ptr - 1);
      c = neon::load4(ptr);
      int32_t crr = *(ptr + 4);

      int32_t dll = *(ptr + stride - 1);
      d = neon::load4(ptr + stride);
      int32_t drr = *(ptr + stride + 4);
    
      // Gather data
      ul = neon::shift_from_lsb(u, ull);
      ur = neon::shift_from_msb(u, urr);
    
      cl = neon::shift_from_lsb(c, cll);
      cr = neon::shift_from_msb(c, crr);
     
      dl = neon::shift_from_lsb(d, dll);
      dr = neon::shift_from_msb(d, drr);

    }
    
    inline void getNmsMask(uint32x4_t &mask, const int32x4_t &thresh,
      const int32x4_t &ul, const int32x4_t &u, const int32x4_t &ur,
      const int32x4_t &cl, const int32x4_t &c, const int32x4_t &cr,
      const int32x4_t &dl, const int32x4_t &d, const int32x4_t &dr)
    {
      mask = neon::get_gt_mask(c, ul) & neon::get_gt_mask(c, u)      & neon::get_gt_mask(c, ur) &
             neon::get_gt_mask(c, cl) & neon::get_ge_mask(c, thresh) & neon::get_gt_mask(c, cr) &
             neon::get_gt_mask(c, dl) & neon::get_gt_mask(c, d)      & neon::get_gt_mask(c, dr);
    }
    
    inline void addPeaks(int &no_max, const int &x, const int &y, const uint32x4_t &mask)
    {
//      if (!mask[0] & !mask[1] & !mask[2] & !mask[3])
//          return;

//      if (!vreinterpretq_u64_u32(mask)[0])
//        return;

      if (mask[0])
        addPeak(no_max, x, y);
      if (mask[1])
        addPeak(no_max, x + 1, y);
      if (mask[2])
        addPeak(no_max, x + 2, y);
      if (mask[3])
        addPeak(no_max, x + 3, y);       
    } 
   
    inline void neonInt32Step(const int32_t *ptr, const int &stride, const int32x4_t &thresh, const int &x, const int &y, int &no_max, const int &rem = 0)
    {
      int32x4_t ul, u, ur, cl, c, cr, dl, d, dr;
      uint32x4_t mask;
      
      gatherNmsGrid(ptr + x, stride, ul, u, ur, cl, c, cr, dl, d, dr);  
      getNmsMask(mask, thresh, ul, u, ur, cl, c, cr, dl, d, dr);

      if (rem)
        {

          for (int i = 0; i < rem; i++)
            mask[i] = 0;
        }

      addPeaks(no_max, x, y, mask);
    }

#endif
      
public:
    Nms3x3()
    {
      orig_width_  = 0;
      orig_height_ = 0;
      
      width_ = 0;
      height_ = 0;
    }
    
    ~Nms3x3() {
      dealloc();
    }

    void reserve(int width, int height) {
      dealloc();

      orig_width_ = width;
      orig_height_ = height;
      
      width_ = width;
      height_ = height;
      
      alloc();
    }
    
    const Maximum* getMaximas() const {
      return &res_[0];
    }

    int computeNaive(const T* data, const int &width, const int &height, const int &x1, const int &x2, const T thresh = 0)
    {
      setData(data);
      width_ = width;
      height_ = height;

      int no_max = 0;

      for(int y = 1; y < height - 1; y++)
        {
          for(int x = x1; x <= x2; x++)
            {
              T ul = get(x - 1, y - 1);
              T uc = get(x    , y - 1);
              T ur = get(x + 1, y - 1);

              T cl = get(x - 1, y    );
              T cc = get(x    , y    );
              T cr = get(x + 1, y    );

              T dl = get(x - 1, y + 1);
              T dc = get(x    , y + 1);
              T dr = get(x + 1, y + 1);

              if ((ul < cc) && (uc < cc) && (ur < cc) &&
                  (cl < cc) && (thresh < cc) && (cr < cc) &&
                  (dl < cc) && (dc < cc) && (dr < cc))
                {
                  res_[no_max].x = x;
                  res_[no_max].y = y;
                  no_max++;
                }
            }
        }

       return no_max;
    }
    
    //Tuan Q. Pham: Non-maximum Suppression Using Fewer than Two Comparisons per Pixel
    int computePham(const T* data, const int &width, const int &height, const int &x1, const int &x2, const T thresh = 0) 

    {        
      int x, y, cur = x1, next = x2 - x1 + 1;
      int no_max = 0;
      T val = 0;
        
      setData(data);
      width_ = width;
      height_ = height;
      
      std::memset(&scan_[0], 0, sizeof(char) * width * 2);

      for (y = 1; y + 1 < height_; y++)
        {
          x = x1 - 1;
          
          while (x < x2)
            {
              scan_[cur + x] = 0;
              x++;
              
              if (scan_[cur + x])
                continue;
              
              val = get(x, y);
              
              if (val <= get(x + 1, y))
                {
                  scan_[cur + x] = 0;
                  x++;
                  val = get(x, y);
                  
                  while ((x < x2) && (val <= get(x + 1, y)))
                    {
                      scan_[cur + x] = 0;
                      x++;
                      val = get(x, y);
                    }
                  
                  if (x == x2)
                    break;                  
                }
              else
                {
                  if (val <= get(x - 1, y))
                    continue;    
                }
              
              scan_[cur + x + 1] = 1;
              
              if (val <= get(x - 1, y + 1))
                continue;
              
              scan_[next + x - 1] = 1;
             
              if (val <= get(x, y + 1))
                continue;              
              
              scan_[next + x] = 1;
                
              if (val <= get(x + 1, y + 1))
                continue;
              
              scan_[next + x + 1] = 1;
              
              if (val < thresh)
                  continue;

              if (val <= get(x - 1, y - 1))
                continue;
         
              if (val <= get(x, y - 1))
                continue;

              if (val <= get(x + 1, y - 1))
                continue;
              
              res_[no_max].x = x;
              res_[no_max].y = y;
              no_max++;
            }
          
          scan_[cur + x] = 0;
          
          int tmp = cur;
          cur = next;
          next = tmp;
        }   
  
      return no_max;
    }    
    
#if defined(USE_NEON) && defined(__ARM_NEON)
    int computeNeonInt32(const int32_t *data, const int &stride, const int &height, const int &x1, const int &x2, const int32_t thresh = 0) 
    {
      int x, y, i, no_max = 0;
      width_ = stride;
      height_ = height;
      
      assert(x1 >= 1);
      assert(x2 < stride);

      int width = x2 - x1 + 1;
      int steps = width >> 2; //  / 4
      int steps2 = steps >> 1;
      int srem = steps & 1; 
      int rem = 4 - (width & 3); // % 4
      int32x4_t thresh4 = vdupq_n_s32(thresh);

      for (y = 1; y + 1 < height_; y++)
        {
          
          const int32_t *ptr = data + y * stride;
          x = x1;

          for (i = 0; i < steps2; i++)
            {
              neonInt32Step(ptr, stride, thresh4, x, y, no_max);
              neonInt32Step(ptr, stride, thresh4, x + 4, y, no_max);
              x += 8;
            }

          if (srem)
            {
              neonInt32Step(ptr, stride, thresh4, x, y, no_max);
              x += 4;
            }

          if (rem)
            {
              x -= rem;
              neonInt32Step(ptr, stride, thresh4, x, y, no_max, rem);
            }                
        }

      return no_max;
    }   
#endif    

#if defined(USE_AVX2) && defined(__AVX2__)
    int computeAvx2(const int32_t *data, const int &stride, const int &height, const int &x1, const int &x2, const int32_t thresh = 0)
    {
      int x, y, i, no_max = 0;
      width_ = stride;
      height_ = height;

      assert(x1 >= 1);
      assert(x2 < stride);

      if (stride < 12)
        return computeNaive(reinterpret_cast<const T *>(data), stride, height, x1, x2, thresh);

      int width = x2 - x1 + 1;
      int steps = width >> 3; //  / 8
      int rem = 8 - (width & 7); // % 8
      __m256i thresh8 = _mm256_set_epi32(thresh, thresh, thresh, thresh,
                                         thresh, thresh, thresh, thresh);

      for (y = 1; y + 1 < height_; y++)
        {
          const int32_t *ptr = data + y * stride;
          x = x1;

          for (i = 0; i < steps; i++, x += 8)
            avx2Int32Step(ptr, stride, thresh8, x, y, no_max);

          if (rem)
            {
              x -= rem;
              avx2Int32Step(ptr, stride, thresh8, x, y, no_max, rem);
            }
        }

      return no_max;
    }
#endif
    
    int compute(const T* data, const int &width, const int &height, const int &x1, const int &x2, const T thresh = 0)
    {
#if defined(USE_NEON) && defined(__ARM_NEON)
      if (std::is_same<T, types::fpreal16>::value)
        return computeNeonInt32(reinterpret_cast<const int32_t *>(data), width, height, x1, x2, thresh.getValue());
      else
        return computeNaive(data, width, height, x1, x2, thresh);
#elif defined(USE_AVX2) && defined(__AVX2__)
      if (std::is_same<T, types::fpreal16>::value)
        return computeAvx2(reinterpret_cast<const int32_t *>(data), width, height, x1, x2, thresh.getValue());
      else
        return computeNaive(data, width, height, x1, x2, thresh);
#else
      return computeNaive(data, width, height, x1, x2, thresh);
#endif
    }
    
    int compute(const T* data, const int &width, const int &height, const T thresh = 0)
    {
      return compute(data, width, height, 1, width - 2, thresh);
    }
};

/** @}*/

} /* namespace dd */
} /* namespace dd */
} /* namespace mke */


#endif // _NMS3X3_H_

