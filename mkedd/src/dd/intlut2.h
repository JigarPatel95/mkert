/*
 * IntLutDetector2, BoxLut based detector with ray interpolation
 *
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifndef _DD_INTLUT2_H_
#define _DD_INTLUT2_H_


#include <vector>

#include "types/fpreal.h"
#include "dd/ddbox.h"
#include "dd/boxlutbase.h"
#include "dd/colpath.h"
#include "dd/iray.h"
#include "dd/accum.h"
#include "dc/classifier.h"
#include "util/datalut.h"
#include "util/ringbuffer.h"
#include "util/timers.h"

#include "util/pmap.h"

#ifdef MKEDD_MULTITHREADING
#include "util/threadpool.h"
#endif

namespace mke {
namespace dd {
namespace dd {

/** \addtogroup mkedd
 *  @{
 */

class IntLut2Detector : public BoxLutBase {
private:

  // Params
  types::fpreal16 adjacency_threshold_param_;
  bool fill_holes_param_;
  uint32_t hole_filling_support_param_;
  uint32_t hole_filling_level_param_;
  uint32_t sdlevel_param_;
  uint32_t laplace_smoothing_param_;

  static const nlohmann::json param_schema_;

  // 3D info
  mke::dd::Data3dType data3d_type_;

  // IntRays
  struct InterpolatedRaysSlice {
    std::vector<uint32_t> ray_idx;
    uint32_t no_detections;
  };

  std::vector<uint32_t> ilevels_;
  uint32_t ids_per_iray_;
  uint32_t sdlevel_;
  uint32_t sdlevel_rem_;
  std::vector<InterpolatedRay> irays_;
  std::vector<std::vector<InterpolatedRaysSlice>> irays_slices_;
  std::vector<bool> valid_flags_;

  // Laplace smoothing
  struct LaplaceData
  {
    util::RingBuffer<uint32_t, 8> buffer;
    int16_t x, y, z;
  };

  std::vector<LaplaceData> laplace_;

  // Runtime statistics
  util::StatsThreadTimer irays_timer_;
  util::StatsThreadTimer alist_timer_;
  util::StatsThreadTimer hfill_timer_;

  // Utils
  typedef std::array<types::fpreal16, 3> BaryCoords;

  struct TriangleInfo
  {
    std::array<PointData*, 3> dets;
    InterpolatedRay::tri_t tri;
    PointData *res;
    uint32_t didx;
    uint32_t uid;
  };

  const static std::vector<std::vector<BaryCoords>> sd_bcoords_;

#ifdef MKEDD_RESERVED_API
  static const nlohmann::json reserved_param_schema_;
  bool compute_triangulation_param_;
  nlohmann::json triangulation_;
#endif

  // ==========================================================================
  // Data

  void setInitValues(void);
  void computeMaxSdLevels(const uint32_t ids_per_ray, uint32_t &sdlevel, uint32_t &rem);

  // ==========================================================================
  // Parameters

  void updateParams(const nlohmann::json &params, bool value_only = false);

  // ==========================================================================
  // Data loading

  void loadLut(std::istream *is, const SectionType section);
  void setupSlices(const unsigned int no_slices);

  // ==========================================================================
  // Processing

  // Ray interpolation
  void interpolateDetection(PointData &res, const uint32_t uid, TriangleInfo &triangle, const BaryCoords &bc);
  void interpolateDetection(TriangleInfo &triangle, const BaryCoords &bc);
  void interpolateDetection(TriangleInfo &triangle, const uint8_t idx1, const uint8_t idx2);
  void getTriangleInfo(const uint32_t ridx, const uint32_t didx, TriangleInfo &triangle);
  void clearRaySlice(InterpolatedRaysSlice &irslice);
  void processMultiLevelRaySlice(InterpolatedRaysSlice &irslice);
  void processSingleLevelRaySlice(InterpolatedRaysSlice &irslice);
  void processInterpolatedRays(void);

  // Outlier removal
  void processOutlierSlice(PathsSlice &irslice);
  void performOutlierRemoval(void);

  // Hole filling
  void processHoleSlice(PathsSlice &pslice);
  void performHoleFilling(void);

  // Laplace smoothing
  void updateLaplaceDetections(const uint32_t iter);
  void processLaplaceSmoothingSlice(const uint32_t init_idx, const uint32_t step, const uint32_t iter);
  void performLaplaceSmoothing(void);

#ifdef MKEDD_RESERVED_API
  bool getTriangulation(std::vector<PointData>& detections, nlohmann::json &triangulation);
#endif

public:

  // ==========================================================================
  // Static methods

  static const std::string staticGetTypeString(void)
  {
    static const std::string typeString = "IntLut2";
    return typeString;
  }

  static Detector* create(void)
  {
    return new IntLut2Detector();
  }

  // ==========================================================================
  // Construction

  IntLut2Detector()
  {
    // Initial parameter values
    pmap_.addSchema(param_schema_);

#ifdef MKEDD_RESERVED_API
    pmap_.addSchema(reserved_param_schema_);
#endif

    updateParams(pmap_.getParams(), true);

   setInitValues();
  }

  ~IntLut2Detector()
    {}

  // ==========================================================================
  // Parameters

  void validateParams(const nlohmann::json &params);
  void setParams(const nlohmann::json &params);
  void getParams(nlohmann::json &params) const;
  const std::string getTypeString(void);

  // ==========================================================================
  // Runtime statistics

  virtual void resetStats(void);
  virtual void getStats(nlohmann::json &stats) const;

  // ==========================================================================
  // Data loading

  bool canParse(const SectionType section);
  void load(std::istream *is, const SectionType section);

  // ==========================================================================
  // Processing

  uint32_t process(const uint8_t *data, std::vector<PointData>& detections);
  uint32_t getMaxNoDetections(void) const;
  uint32_t getStride(void) const;
  Data3dType getData3dType(void) const;

  // ==========================================================================
  // Reserved API

#ifdef MKEDD_RESERVED_API
  void getDebugData(nlohmann::json &data) const;
  void getTriangulation(nlohmann::json &triangulation);
#endif

};

/** @}*/

} /* namespace dd */
} /* namespace dd */
} /* namespace mke */


#endif // _DD_INTLUT2_H_
