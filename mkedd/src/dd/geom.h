#ifndef _DD_GEOM_H_ 
#define _DD_GEOM_H_ 

#include <iostream>
#include "dd/detector.h"


namespace mke {
namespace dd {
namespace dd {
  
/** \addtogroup mkedd
 *  @{
 */
  
class GeomDetector : public Detector {
public:
  
  
  static const std::string staticGetTypeString(void)
  {
    static const std::string typeString = "DdGeom";
    return typeString;
  }
  
  static Detector* create(void)
  {
    return new GeomDetector();
  }
  
  GeomDetector() {}


  void validateParams(const nlohmann::json &params)
  {
  }

  nlohmann::json getParamsSchema(void) const
  {
    return nlohmann::json::object();
  }

  void setParams(const nlohmann::json &params)
  {
  }

  void getParams(nlohmann::json &params) const
  {
  }

  const std::string getTypeString(void)
  {
    return staticGetTypeString();
  }
  
  
  void resetStats(void)
  {
  }

  void getStats(nlohmann::json &) const
  {
  }
  
  bool canParse(const SectionType section)
  {
    return false;
  }
  
  void load(std::istream *is, const SectionType section) 
  {
    
  }  
    
  uint32_t process(const uint8_t *data, std::vector<PointData> &detections)
  {
    return 0;
  }
  
  uint32_t getMaxNoDetections(void) const
  {
    return 0;  
  }

  uint32_t getStride(void) const
  {
    return 0;
  }

  mke::dd::Data3dType getData3dType(void) const
  {
    return mke::dd::DD_DATA3D_MM;
  }

#ifdef MKEDD_RESERVED_API
  void getDebugData(nlohmann::json &data) const
  {
    data = nullptr;
  }

  void getTriangulation(nlohmann::json &triangulation)
  {
    throw std::runtime_error("[MkEDD] GeomDetector: getTriangulation not implemented");
  }

#endif
};

/** @}*/

} /* namespace dd */
} /* namespace dd */
} /* namespace mke */


#endif // _DD_GEOM_H_ 
