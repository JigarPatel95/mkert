/*
 * ddbox - 1D Convolutional detector using box filter
 *
 * Copyright (c) 2016--2018, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifndef _DDBOX_H_
#define _DDBOX_H_

#include <cassert>
#include <stdint.h>
#include <vector>
#include <array>
#include <cstring>
#include <iostream>

#include <boost/align/aligned_allocator.hpp>

#if defined(USE_NEON) && defined(__ARM_NEON)
#include "neon/neon_bits.h"
#endif

#if defined(USE_AVX2) && defined(__AVX2__)
#include "sse/avx2_bits.h"
#endif


//#include "fpreal.h"
#include "dd/nms3x3.h"

#ifdef USE_HALIDE
#include "HalideBuffer.h"
#include "halide_convolve.h"
#endif

namespace mke {
namespace dd {
namespace dd {

/** \addtogroup mkedd
 *  @{
 */

template <typename T> class DdBoxDetector {
public:

  struct PeakInfo
  {
    uint32_t offset;
    uint32_t scale;
    T response;
  };

private:

  unsigned int slength_;
  unsigned int no_scales_;

  std::vector<T, boost::alignment::aligned_allocator<T, 32>> spdata_;

  Nms3x3<T> nms_;
  std::vector<PeakInfo> peaks_;
  unsigned int no_peaks_;

  std::vector<int32_t> kwidths_;

  // ========================================================================
  // ========================================================================

  void alloc()
  {
    peaks_.reserve(slength_);
    spdata_.reserve(slength_ * no_scales_);
    std::memset(reinterpret_cast<void *>(spdata_.data()), 0, sizeof(T) * slength_ * no_scales_);
    nms_.reserve(slength_, no_scales_);
  }
  // ========================================================================

  void dealloc()
  {
    //
  }

  // ========================================================================
  // ========================================================================

#if defined(USE_NEON) && defined(__ARM_NEON)

  inline void _convStepNeonInt32(const int32_t* cssig, const int32x4_t &idx, const int32x4_t &idx_off,
                                 const int32x4_t &lb, const int32x4_t &ub,
                                 const int32x4_t &ilb, const int32x4_t &ilb_off,
                                 const int32x4_t &iub, const int32x4_t &iub_off,
                                 const int32_t ibw, int32_t* cres)
  {
    int32x4_t cidx;
    int32x4_t ilb_ext, iub_ext, ext;
    int32x4_t v, u, mu;

    const uint32_t vms[] = {0x0, 0x0, 0xFFFFFFFF, 0x0};
    const uint32_t ums[] = {0xFFFFFFFF, 0x0, 0xFFFFFFFF, 0x0};

    const uint32x4_t v_mask = neon::load4u(vms);// neon::get_mask(0x0, 0x0, 0xFFFFFFFF, 0x0);
    const uint32x4_t u_mask = neon::load4u(ums);//neon::get_mask(0xFFFFFFFF, 0x0, 0xFFFFFFFF, 0x0);

    cidx = neon::clip_value(idx + idx_off, lb, ub);
    v = neon::gather(cssig, cidx);

    ilb_ext = neon::keep_if_gt(ilb + ilb_off, lb, cidx); // ilb_ext = (lb > cidx) ? cilb : 0;
    iub_ext = neon::keep_if_gt(iub + iub_off, cidx, ub); // iub_ext = (cidx > ub) ? ciub : 0;
    ext = ilb_ext + iub_ext;

    v = v + ext;
    u = v - neon::reverse(v);
    mu = neon::keep_if_mask(u, v_mask);
    u = u + mu + mu;

    u = neon::keep_if_mask(u, u_mask);
    u = u + neon::reverse(u);

    int64x2_t su = vpaddlq_s32(u);
    int32x2_t w = vdup_n_s32(ibw);
    su = vmull_s32(int32x2_t(vget_low_s64(su)), w);
    su = vshrq_n_s64(su, 16);
    int32x2_t res = int32x2_t(vget_low_s64(su));
    vst1_lane_s32(cres, res, 0);
  }

  // ========================================================================

  inline void convStepNeonInt32(const types::fpreal16 * __restrict__ cssig, const int32x4_t &idx, const int32x4_t &idx_off,
                                const int32x4_t &lb, const int32x4_t &ub,
                                const int32x4_t &ilb, const int32x4_t &ilb_off,
                                const int32x4_t &iub, const int32x4_t &iub_off,
                                const types::fpreal16 ibw ,types::fpreal16 * __restrict__ cres)
  {
    int32x4_t cidx, oidx;
    int32x4_t ilb_ext, iub_ext, ext;

    oidx = idx + idx_off;
    cidx = neon::clip_value(oidx, lb, ub);

    ilb_ext = neon::keep_if_gt(ilb + ilb_off, lb, oidx); // ilb_ext = (lb > idx) ? cilb : 0;
    iub_ext = neon::keep_if_gt(iub + iub_off, oidx, ub); // iub_ext = (idx > ub) ? ciub : 0;
    ext = ilb_ext + iub_ext;

    types::fpreal16 v1 = cssig[cidx[0]];
    types::fpreal16 v2 = cssig[cidx[1]];
    types::fpreal16 v3 = cssig[cidx[2]];
    types::fpreal16 v4 = cssig[cidx[3]];

    v3 += - v2 + types::fpreal16(ext[2], true) - types::fpreal16(ext[1], true);
    v1 += - v4 + types::fpreal16(ext[0], true) - types::fpreal16(ext[3], true);
    v3 += v3 + v3 + v1;

    *cres = v3 * ibw;
  }

  // ========================================================================

  void convddboxNeonInt32(const types::fpreal16 * __restrict__ cssig,
                          types::fpreal16 * __restrict__ cres,
                          const unsigned int slength,
                          unsigned int a,  unsigned int b, const int bwidth)
  {
    assert(~(bwidth & 1) && (bwidth >= 3));
    assert(bwidth > 0);

    if (slength == 0)
      return;

    int32_t offset = (bwidth - 1) >> 1;
    int32_t sl = slength - 1;
    types::fpreal16 ibw = types::fpreal16(1) / int(bwidth * 4);

    int32x4_t idxs;
    idxs[1] = -offset - 1;
    idxs[0] = idxs[1] - bwidth;
    idxs[2] = offset;
    idxs[3] = idxs[2] + bwidth;

    const int32x4_t _0s = vdupq_n_s32(0);
    const int32x4_t _1s = vdupq_n_s32(1);
    const int32x4_t _2s = vdupq_n_s32(2);

    int32x4_t idx = idxs + vdupq_n_s32(a); // idx = idxs + a

    const int32x4_t lb = _0s;                     // lb = 0
    const int32x4_t vlb = vdupq_n_s32(cssig[0].getValue());
    const int32x4_t ulb = vdupq_n_s32(cssig[1].getValue());
    const int32x4_t dlb = ulb - vlb;    // dlb = cssig[1] - ccsig[0]
    int32x4_t ilb = vmulq_s32(idx, dlb);    // ilb = idx * dlb
    const int32x4_t dlb2 = dlb + dlb;

    const int32x4_t ub = vdupq_n_s32(sl);         // ub = sl
    const int32x4_t vub = vdupq_n_s32(cssig[sl].getValue());
    const int32x4_t uub = vdupq_n_s32(cssig[sl - 1].getValue());
    const int32x4_t dub = vub - uub;    // dub = cssig[sl] - cssig[sl - 1]
    int32x4_t iub = vmulq_s32(idx - ub, dub); // iub = (idx - ub) * dub
    const int32x4_t dub2 = dub + dub;

    int ewidth = (b - a + 1);
    int steps = ewidth >> 1; //  / 2
    int rem = (ewidth & 1); // % 2

    int x = a;

    for (int i = 0; i < steps; i++)
      {
        convStepNeonInt32((cssig), idx, _0s, lb, ub, ilb,  _0s, iub,  _0s, ibw, (cres) + x);
        convStepNeonInt32((cssig), idx, _1s, lb, ub, ilb,  dlb, iub,  dub, ibw, (cres) + x + 1);

        idx = idx + _2s;
        ilb = ilb + dlb2;
        iub = iub + dub2;
        x += 2;
      }

    for (int i = 0; i < rem; i++)
      {
        convStepNeonInt32((cssig), idx, _0s, lb, ub, ilb,  _0s, iub,  _0s, ibw, (cres) + x);

        idx = idx + _1s;
        ilb = ilb + dlb;
        iub = iub + dub;
        x += 1;
      }
  }

#endif

  // ========================================================================
  // ========================================================================

#if defined(USE_AVX2) && defined(__AVX2__)
  inline void convStepAvx2(const types::fpreal16 * __restrict__ cssig,
                           const mke::dd::sse::int32x8_t &idx,
                           const mke::dd::sse::int32x8_t &lb,
                           const mke::dd::sse::int32x8_t &ub,
                           const mke::dd::sse::int32x8_t &ilb,
                           const mke::dd::sse::int32x8_t &iub,
                           const types::fpreal16 ibw,
                           types::fpreal16 * __restrict__ cres,
                           const int no_steps)
  {
    using namespace mke::dd::sse;

    int32x8_t cidx = clip_value(idx, lb, ub);
    int32x8_t ilb_ext = keep_if_gt(ilb, lb, idx); // ilb_ext = (lb > idx) ? cilb : 0;
    int32x8_t iub_ext = keep_if_gt(iub, idx, ub); // iub_ext = (idx > ub) ? ciub : 0;
    int32x8_t ext = ilb_ext + iub_ext;
    int32x8_t vvs = _mm256_i32gather_epi32(reinterpret_cast<const int *>(cssig), cidx, 4);

    // vvs = vvs & mask1 & mask2;
    ext = ext.shuffle(0, 2, 2, 0) - ext;
    vvs = vvs.shuffle(0, 2, 2, 0) + (ext - vvs);
    vvs = vvs + vvs + vvs + vvs.shuffle(3, 3, 3, 3);

    int o = 0;
#pragma unroll
    for (int i = 0; i < no_steps; i++, o += 4)
      *(cres + i) = ibw * types::fpreal16(vvs[1 + o], true);
  }

  // ========================================================================

  void convddboxAvx2(const types::fpreal16 * __restrict__ cssig,
                     types::fpreal16 * __restrict__ cres,
                     const unsigned int slength,
                     unsigned int a,  unsigned int b, const int bwidth)
  {
    using namespace mke::dd::sse;

    if (slength == 0)
      return;

    int32_t offset = (bwidth - 1) >> 1;
    int32_t sl = slength - 1;
    types::fpreal16 ibw = types::fpreal16(1) / int(bwidth * 4);

    int idxs[8] __attribute__((aligned(32)));

    idxs[1] = -offset - 1;
    idxs[0] = idxs[1] - bwidth;
    idxs[2] = offset;
    idxs[3] = idxs[2] + bwidth;

    idxs[5] = idxs[1];
    idxs[4] = idxs[0];
    idxs[6] = idxs[2];
    idxs[7] = idxs[3];

    int32x8_t idx(idxs, false);
    idx += int32x8_t(a, a, a, a, a + 1, a + 1, a + 1, a + 1); // idx = idxs + a/a+1

    const int32x8_t _0s = int32x8_t(0);
    const int32x8_t _2s = int32x8_t(2);

    const int32x8_t lb = _0s;                      // lb = 0
    const int32x8_t vlb(cssig[0].getValue());      // vlb = ccsig[0]
    const int32x8_t ulb(cssig[1].getValue());      // ulb = cssig[1]
    const int32x8_t dlb = ulb - vlb;               // dlb = cssig[1] - ccsig[0]
    const int32x8_t dlb2 = dlb + dlb;              // dlb2 = dlb + dlb
    int32x8_t ilb = idx * dlb;                     // ilb = idx * dlb

    const int32x8_t ub = int32x8_t(sl);            // ub = sl
    const int32x8_t vub(cssig[sl].getValue());
    const int32x8_t uub(cssig[sl - 1].getValue());
    const int32x8_t dub = vub - uub;               // dub = cssig[sl] - cssig[sl - 1]
    const int32x8_t dub2 = dub + dub;              // dub2 = dub + dub
    int32x8_t iub = (idx - ub) * dub;              // iub = (idx - ub) * dub

    int ewidth = (b - a + 1);
    int steps = ewidth >> 1; //  / 2
    int rem = (ewidth & 1); // % 2

    int x = a;

    for (int i = 0; i < steps; i++)
      {
        convStepAvx2(cssig, idx, lb, ub, ilb, iub, ibw, cres + x, 2);

        idx = idx + _2s;
        ilb = ilb + dlb2;
        iub = iub + dub2;
        x += 2;
      }

    if (rem)
      convStepAvx2(cssig, idx, lb, ub, ilb, iub, ibw, cres + x, 1);
  }
#endif

  // ========================================================================
  // ========================================================================

  inline T clamp_read(const T* data, const int n, const int a, const T ba, const T da, const int b, const T bb, const T db)
  {
    if (n < a)
      return ba - ((a - n) * da);
    else if (n > b)
      return bb + ((n - b) * db);
    else
      return data[n];
  }

  // ========================================================================

  void convddbox(const T* cssig, T* cres, const unsigned int slength, unsigned int a,  unsigned int b, const int bwidth)
  {
    assert(~(bwidth & 1) && (bwidth >= 3));
    assert(bwidth > 0);

    if (slength == 0)
      return;

    int offset = (bwidth - 1) >> 1;
    int sl = slength - 1;

    int i2 = -offset - 1;
    int i1 = i2 - bwidth;
    int i3 = offset;
    int i4 = i3 + bwidth;
    int bw3 = bwidth * 4;

    i1 += a;
    i2 += a;
    i3 += a;
    i4 += a;

    // boundary values
    T ba = cssig[0];
    T da = cssig[1] - ba;
    T bb = cssig[sl];
    T db = bb - cssig[sl - 1];

    T ibw3 = T(1) / int32_t(bw3);

    // std::memset(cres, 0, sizeof(T) * a);

    for (unsigned int i = a; i <= b; i++)
      {
        T v1 = clamp_read(cssig, i1++, 0, ba, da, sl, bb, db);
        T v2 = clamp_read(cssig, i2++, 0, ba, da, sl, bb, db);
        T v3 = clamp_read(cssig, i3++, 0, ba, da, sl, bb, db);
        T v4 = clamp_read(cssig, i4++, 0, ba, da, sl, bb, db);

        v3 = v3 - v2;
        v1 = v1 - v4;
        v3 += v3 + v3 + v1;

        cres[i] = v3 * ibw3;
      }
  }

  // ========================================================================
  // ========================================================================

public:

  DdBoxDetector()
  {
    reserve(1, 10);
    no_peaks_ = 0;
  }

  ~DdBoxDetector()
  {
    dealloc();
  }

  // ========================================================================

  void reserve(const unsigned int slength, unsigned int no_scales = 10)
  {
    dealloc();

    slength_ = slength;

    //const static int kw[] = {3, 5, 7, 9, 13, 17, 25, 33, 49, 65};
    const static std::array<int32_t, 10> kw = {3, 5, 7, 9, 13, 17, 21, 29, 37, 53};

    if (no_scales > kw.size())
      no_scales_ = kw.size();
    else
      no_scales_ = no_scales;

    kwidths_.resize(no_scales_);

    for (unsigned int i = 0; i < no_scales_; i++)
      kwidths_[i] = kw[i];

    alloc();
  }

  // ========================================================================

  void reserve(const unsigned int slength, std::vector<int32_t> &kwidths)
  {
    dealloc();

    slength_ = slength;
    no_scales_ = kwidths.size();
    kwidths_.resize(no_scales_);

    for (unsigned int i = 0; i < no_scales_; i++)
      kwidths_[i] = kwidths[i];

    alloc();
  }

  // ========================================================================
  // ========================================================================

  inline unsigned int getNoScales(void) const
  {
    return no_scales_;
  }

  // ========================================================================

  inline const PeakInfo* getPeaks(void)
  {
    return &peaks_[0];
  }

  // ========================================================================

  inline unsigned int getNoPeaks(void)
  {
    return no_peaks_;
  }

  // ========================================================================

  inline double getSigmaDouble(const unsigned int s) const
  {
    return double(kwidths_[s]) * 12.0f / 30.0f;
  }

  // ========================================================================

  inline T getSigma(const unsigned int s) const {
    return T(double(double(kwidths_[s]) * 12.0f / 30.f));
  }

  // ========================================================================

  unsigned int getBoxWidth(const unsigned int s) const
  {
    return kwidths_[s] * 3;
  }

  // ========================================================================
  // ========================================================================

  inline T getData(const unsigned int x, const unsigned int s) const
  {
    assert((x > 0) && (x + 1 < slength_));
    assert((s > 0) && (s + 1 < no_scales_));

    return spdata_[s * slength_ + x];
  }

  // ========================================================================

  inline T* getData(void)
  {
    return spdata_.data();
  }

  // ========================================================================

  inline T getData(const unsigned int x, const unsigned int s, const unsigned sl, const int32_t csshift = 0) const
  {
    assert((x > 0) && (x + 1 < slength_));
    assert((s > 0) && (s + 1 < no_scales_));

    if (csshift)
      return spdata_[s * sl + x] << csshift;
    else
      return spdata_[s * sl + x];
  }

  // ========================================================================
  // ========================================================================

  inline T diffX(const unsigned int x, const unsigned int s, const unsigned sl, const int32_t csshift = 0) const
  {
    const static T half = T(0.5);
    return half * (getData(x + 1, s, sl, csshift) - getData(x - 1, s, sl, csshift));
  }

  // ========================================================================

  inline T diffS(const unsigned int x, const unsigned int s, const unsigned sl, const int32_t csshift = 0) const
  {
    const static T half = T(0.5);
    return half * (getData(x, s + 1, sl, csshift) - getData(x, s - 1, sl, csshift));
  }

  // ========================================================================

  inline T diffXX(const unsigned int x, const unsigned int s, const unsigned sl, const int32_t csshift = 0) const
  {
    const static T two = T(2);
    return getData(x + 1, s, sl, csshift) + getData(x - 1, s, sl, csshift) - two * getData(x, s, sl, csshift);
  }

  // ========================================================================

  inline T diffSS(const unsigned int x, const unsigned int s, const unsigned sl, const int32_t csshift = 0) const
  {
    const static T two = T(2);
    return getData(x, s + 1, sl, csshift) + getData(x, s - 1, sl, csshift) - two * getData(x, s, sl, csshift);
  }

  // ========================================================================

  inline T diffXS(const unsigned int x, const unsigned int s, const unsigned sl, const int32_t csshift = 0) const
  {
    const static T quarter = T(0.25);
    return quarter * (getData(x + 1, s + 1, sl, csshift) + getData(x - 1, s - 1, sl, csshift) -
                      getData(x - 1, s + 1, sl, csshift) - getData(x + 1, s - 1, sl, csshift));
  }

  // ========================================================================
  // ========================================================================

  inline T fitPeak(unsigned int peak_idx, const unsigned sl, const int32_t csshift = 0) const
  {
    const unsigned int &scale = peaks_[peak_idx].scale;
    const unsigned int &off = peaks_[peak_idx].offset;

    // Find maximum of Taylor polynomial of the 2-nd degree
    T dx = diffX(off, scale, sl, csshift);
    T ddx = diffXX(off, scale, sl, csshift);

    T peak_offset = T((int)off);

    if (ddx != T(0))
      {
        T delta = -(dx / ddx);
        // Report the new offset only if it is reasonably close to the original
        if (abs(delta) < T(1))
          return peak_offset + delta;
      }

    return peak_offset;
  }

  // ========================================================================
  // ========================================================================

  void convolve(const T* cssig, const unsigned int sl, const unsigned int a, const unsigned int b)
  {
#ifdef USE_HALIDE
    if (std::is_same<T, types::fpreal16>::value)
      {
        Halide::Runtime::Buffer<int32_t> input((int32_t *)cssig, sl);
        Halide::Runtime::Buffer<int32_t> scales(&kwidths_[0], no_scales_);
        Halide::Runtime::Buffer<int32_t> output((int32_t *)spdata_.data(), sl, no_scales_);
        output.crop(0, a, b - a + 1);
        halide_convolve(a, b, input, scales, output);
      }
    else
      {
        convolveNaive(cssig, sl, a, b);
      }
#elif defined(USE_NEON) && defined(__ARM_NEON)
    if (std::is_same<T, types::fpreal16>::value)
      {
        for (unsigned int i = 0; i < no_scales_; i++)
          convddboxNeonInt32(cssig, spdata_.data() + (i * sl), sl, a, b, kwidths_[i]);
      }
    else
      {
        convolveNaive(cssig, sl, a, b);
      }
#elif defined(USE_AVX2) && defined(__AVX2__)
    if (std::is_same<T, types::fpreal16>::value)
      {
        for (unsigned int i = 0; i < no_scales_; i++)
          convddboxAvx2(reinterpret_cast<const types::fpreal16 *>(cssig),
                        reinterpret_cast<types::fpreal16 *>(spdata_.data() + (i * sl)),
                        sl, a, b, kwidths_[i]);
      }
    else
      {
        convolveNaive(cssig, sl, a, b);
      }
#else
    convolveNaive(cssig, sl, a, b);
#endif
  }

  // ========================================================================

  void convolveHalide(const T* cssig, const unsigned int sl, const unsigned int a, const unsigned int b)
  {
#if defined(USE_HALIDE)
    if (std::is_same<T, types::fpreal16>::value)
      {
        Halide::Runtime::Buffer<int32_t> input((int32_t *)cssig, sl);
        Halide::Runtime::Buffer<int32_t> scales(&kwidths_[0], no_scales_);
        Halide::Runtime::Buffer<int32_t> output((int32_t *)spdata_.data(), sl, no_scales_);
        output.crop(0, a, b - a + 1);
        halide_convolve(a, b, input, scales, output);
      }
    else
      {
        throw std::runtime_error("[DdBoxDetector::convolveHalide] Available for fpreal16 datatype only");
      }
#else
    throw std::runtime_error("[DdBoxDetector::convolveHalide] Not available");
#endif
  }

  // ========================================================================

  void convolveNaive(const T* cssig, const unsigned int sl, const unsigned int a, const unsigned int b)
  {
    for (unsigned int i = 0; i < no_scales_; i++)
      convddbox(cssig, spdata_.data() + (i * sl), sl, a, b, kwidths_[i]);
  }

  // ========================================================================

  void convolveAvx2(const T* cssig, const unsigned int sl, const unsigned int a, const unsigned int b)
  {
#if defined(USE_AVX2) && defined(__AVX2__)
    if (std::is_same<T, types::fpreal16>::value)
      {
        for (unsigned int i = 0; i < no_scales_; i++)
          convddboxAvx2(reinterpret_cast<const types::fpreal16 *>(cssig),
                        reinterpret_cast<types::fpreal16 *>(spdata_.data() + (i * sl)),
                        sl, a, b, kwidths_[i]);
      }
    else
      {
        throw std::runtime_error("[DdBoxDetector::convolveAvx2] Available for fpreal16 datatype only");
      }
#else
    throw std::runtime_error("[DdBoxDetector::convolveAvx2] Not available");
#endif
  }

  // ========================================================================

  void convolveNeon(const T* cssig, const unsigned int sl, const unsigned int a, const unsigned int b)
  {
#if defined(USE_NEON) && defined(__ARM_NEON)
    if (std::is_same<T, types::fpreal16>::value)
      {
        for (unsigned int i = 0; i < no_scales_; i++)
          convddboxNeonInt32(reinterpret_cast<const types::fpreal16 *>(cssig),
                             reinterpret_cast<types::fpreal16 *>(spdata_.data() + (i * sl)),
                             sl, a, b, kwidths_[i]);
      }
    else
      {
        throw std::runtime_error("[DdBoxDetector::convolveNeon] Available for fpreal16 datatype only");
      }
#else
    throw std::runtime_error("[DdBoxDetector::convolveNeon] Not available");
#endif
  }

  // ========================================================================

  void convolve(const T* cssig, const T& thresh)
  {
    return convolve(cssig, slength_, thresh);
  }

  // ========================================================================
  // ========================================================================

  unsigned int findPeaks(const unsigned int sl, const T &thresh, const int x1, const int x2, const uint8_t *aflags = NULL)
  {
    unsigned int no_peaks = nms_.compute(spdata_.data(), sl, no_scales_, x1, x2, thresh);
    auto *maximas = nms_.getMaximas();

    // output results
    unsigned int j = 0;
    for (unsigned int i = 0; i < no_peaks; i++)
      {
        const unsigned int &x = maximas[i].x;
        const unsigned int &y = maximas[i].y;

        // continue if 'x' is a non-active offset
        //  if (aflags && !aflags[x])
        //    continue;

        peaks_[j].offset = x;
        peaks_[j].scale = y;
        peaks_[j].response = getData(x, y, sl);
        j++;
      }

    no_peaks_ = j;
    return j;
  }
};

/** @}*/

} /* namespace dd */
} /* namespace dd */
} /* namespace mke */

#endif /* _DDBOX_H */
