/*
 * mkedd - MkE detector
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifndef _DETECTOR_H_
#define _DETECTOR_H_

#include <iosfwd>
#include <vector>

#include "json.hpp"

#include "data_types.h"

#ifdef MKEDD_RESERVED_API
#include "json.hpp"
#endif

namespace mke {
namespace dd {
namespace dd {

/** \addtogroup mkedd
 *  @{
 */
  
class Detector {
public:
  enum SectionType {
    DD_SECTION_UNKNOWN = 0,
    DD_SECTION_LUT3D2DID = 1,
    DD_SECTION_LUTSV2 = 2,
    DD_SECTION_COLLUT = 3,
    DD_SECTION_INTLUT = 4,
    DD_SECTION_MSF = 101
  };
  
  // Construction =============================================================  
  
  Detector() {}
  virtual ~Detector() {}
  
  // Virtual methods ==========================================================
    
  virtual void validateParams(const nlohmann::json &) = 0;
  virtual nlohmann::json getParamsSchema(void) const = 0;
  virtual void setParams(const nlohmann::json &) = 0;
  virtual void getParams(nlohmann::json &) const = 0;
  virtual const std::string getTypeString(void) = 0;

  virtual void resetStats(void) = 0;
  virtual void getStats(nlohmann::json &) const = 0;
  
  virtual bool canParse(const SectionType section) = 0;
  virtual void load(std::istream *is, const SectionType section) = 0; 

  virtual uint32_t process(const uint8_t *data, std::vector<PointData>& detections) = 0;
  virtual uint32_t getMaxNoDetections(void) const = 0;
  virtual uint32_t getStride(void) const = 0;
  virtual Data3dType getData3dType(void) const = 0;

#ifdef MKEDD_RESERVED_API
  virtual void getDebugData(nlohmann::json &data) const = 0;
  virtual void getTriangulation(nlohmann::json &triangulation) = 0;
#endif
};


/** @}*/

} /* namespace dd */
} /* namespace dd */
} /* namespace mke */



#endif /* _DETECTOR_H_ */
