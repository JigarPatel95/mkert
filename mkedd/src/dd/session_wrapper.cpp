/*
 * SessionWrapper
 *
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifdef MKEDD_USE_MKECC

#include <iostream>

#include "json.hpp"
#include "dd/session_wrapper.h"

using namespace mke::dd::dd;
using json = nlohmann::json;
using namespace mke::cc::session;

// ============================================================================
// ============================================================================

void SessionWrapper::setSession(std::shared_ptr<Session> session)
{
  session_ = session;

  // If stripped session, try to prepare session data.
  // Otherwise, we expect the session to have been properly prepared/executed before saving.
  if (session_->hasNode("extdata"))
    session_->executeIfEmpty("extdata");

  // Camera calibrations
  json camera_models = session_->findScenarioData("camera_model");

  if (!camera_models.size())
   throw std::runtime_error("[SessionWrapper::setSession] 'camera_models' data not found");

  camera_model_ = camera_models[0];
  iwidth_ = camera_model_.at("width");
  iheight_ = camera_model_.at("height");

  mke::cc::camera::Model model;
  model.set(camera_model_);
  camera_.setModel(model);

  // Laser pattern, stride
  json desc = session->getScenarioDescription();
  json laser_pattern = json::array();
  bool mkedet_found = false;
  bool raysamper_found = false;

  for (auto &node : desc)
    {
      if (node.at("type") == "MkedetExporter")
        {
          json ps = node.at("parameters");
          laser_pattern = ps.at("laser_pattern");
          stride_ = ps.at("stride");
          mkedet_found = true;
        }
      else if (node.at("type") == "RaySampler")
        {
          json ps = node.at("parameters");
          depth_min_ = ps.at("min_depth").get<int>();
          depth_max_ = ps.at("max_depth").get<int>();
          raysamper_found = true;
        }
    }

  if (!mkedet_found)
     throw std::runtime_error("[SessionWrapper::setSession] Unable to find 'MkedetExporter' node");

  if (!raysamper_found)
     throw std::runtime_error("[SessionWrapper::setSession] Unable to find 'RaySampler' node");

  no_doe_groups_ = laser_pattern.size();
  laser_pattern_.resize(no_doe_groups_);

  for (size_t i = 0; i < no_doe_groups_; i++)
    {
      for (size_t j = 0; j < laser_pattern[i].size(); j++)
        laser_pattern_[i] |= laser_pattern[i][j].get<int>();
    }
}

// ============================================================================

void SessionWrapper::setDetIdx(const int det_idx)
{
  // Ray groups
  json ray_groups = session_->findScenarioData("ray_groups");

  if (!ray_groups.size())
    throw std::runtime_error("[SessionWrapper::setSession] 'ray_groups' data not found");

  ray_groups_ = ray_groups.at(0);

  // Rays
  json &rays_json = ray_groups_.at(det_idx).at("rays");
  max_detections_ = rays_json.size();
  det_idx_ = det_idx;
  ray_orgs_.resize(max_detections_);
  ray_dirs_.resize(max_detections_);

  for (size_t i = 0; i < max_detections_; i++)
    {
      for (int j = 0; j < 3; j++)
        ray_orgs_[i][j] = rays_json[i][j];

      for (int j = 0; j < 3; j++)
        ray_dirs_[i][j] = rays_json[i][j + 3];
    }

  // IDs
  json &ids_json = ray_groups_.at(det_idx).at("ids");
  ids_.resize(max_detections_);

  for (size_t i = 0; i < max_detections_; i++)
    ids_[i] = ids_json[i][0].get<double>();
}

// ============================================================================

int SessionWrapper::getNumDoeGroups(void) const
{
  return laser_pattern_.size();
}

// ============================================================================

uint32_t SessionWrapper::getLaserPattern(const int det_idx) const
{
  return laser_pattern_[det_idx];
}

// ============================================================================

const nlohmann::json& SessionWrapper::getInfoData(void) const
{
  return session_->getInfoData();
}

#endif
