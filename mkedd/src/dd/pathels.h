/*
 * Path elements, various path elements data types
 *
 * Copyright (c) 2018, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifndef _DD_PATHELS_H_
#define _DD_PATHELS_H_
namespace mke {
namespace dd {
namespace dd {

/** \addtogroup mkedd
 *  @{
 */

struct PathElementLut {
  int32_t u;          // fpreal16
  int32_t v;          // fpreal16
  int32_t sigma3D;    // fpreal16
  int16_t x;
  int16_t y;
  int16_t z;
};

// ==============================================================================

struct PathElement2DLut {
  uint16_t ui;
  uint16_t vi;
  uint16_t uvf;
  uint16_t dummy;
};

// ==============================================================================

struct PathElement3DLut {
  int16_t x;
  int16_t y;
  int16_t z;
};

// ==============================================================================

struct PathElementLiteLut {
  uint16_t ui;
  uint16_t vi;
  uint16_t uvf;
  int16_t x;
  int16_t y;
  int16_t z;
};

/** @}*/

} // namespace dd
} // namespace dd
} // namespace mke

#endif // _DD_PATHELS_H_
