/*
 * mkedd - MkE DdBoxLut
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifdef MKEDD_USE_BOXLUT_DETECTOR

#include <exception>
#include <iostream>
#include <cmath>

#include "dd/boxlut.h"
#include "dc/factory.h"

using namespace mke::dd::dd;
using namespace mke::dd::types;
using json = nlohmann::json;

// ============================================================================
// ============================================================================
// Data

const int32_t mke::dd::dd::BoxLutDetector::csshift_;

void BoxLutDetector::setInitValues(void)
{
  iwidth_ = 0;
  iheight_ = 0;
  stride_ = 0;
  
  depth_min_ = 0;
  depth_max_ = 0;
 
  no_accums_ = 0;
  no_accels_ = 0;
  no_pxs_ = 0;
  
  // Accums
  pixellist_.clear();
  aflags_.clear();
  afaidx_.clear();
  afbidx_.clear();
  acclens_.clear();
  accoffs_.clear();
  accums_.clear();
  accumsums_.clear();
  pixslices_.clear();
  accslices_.clear();
  
  // History
  valid_epochs_.clear();
  peak_history_.clear();
  
#ifdef MKEDD_RESERVED_API
  det_accels_.clear();
  accdump_file_param_ = "";
#endif
  
  // Dot classifier
  classifier_ = NULL;
  
  // Results
  no_detections_ = 0;
  
  max_acclen_ = 0;
  
  // LUT
  lut_.clear();
  
  // Initial parameter values
  pmap_.addSchema(param_schema_);
  updateParams(pmap_.getParams(), true);
}

// ============================================================================

void BoxLutDetector::allocAccumMemory(void)
{  
  aflags_.resize(no_accels_);
  afaidx_.resize(no_accums_);
  afbidx_.resize(no_accums_);
  acclens_.resize(no_accums_);
  accoffs_.resize(no_accums_);
  accums_.resize(no_accels_);
  accumsums_.resize(no_accels_);
  
  valid_epochs_.resize(no_accums_);
  peak_history_.resize(no_accums_);
  dts_history_.resize(no_accums_);
  
  for (unsigned int i = 0; i < no_accums_; i++)
    {
      valid_epochs_[i] = 0;
      peak_history_[i] = -1;
    }
  
#ifdef MKEDD_RESERVED_API
  det_accels_.resize(no_accums_);
#endif  
}

// ============================================================================

void BoxLutDetector::allocPixelListMemory(void)
{
  pixellist_.resize(no_pxs_);
  pixslices_.resize(no_threads_param_);
}

// ============================================================================

void BoxLutDetector::setupAccumSlices(void)
{
  accslices_.resize(no_threads_param_);
  uint32_t accum_step = std::ceil(double(no_accums_) / double(no_threads_param_));
  uint32_t accum_delta = 0;
  uint32_t accum_counter = 0;
  
  for (unsigned int i = 0; i < no_threads_param_; i++)
  {
    if ((i + 1) == no_threads_param_)
      accum_delta = no_accums_ - accum_counter;
    else
      accum_delta = accum_step;
    
    accslices_[i].accum_min = accum_counter;
    accslices_[i].accum_max = accum_counter + accum_delta;
    accslices_[i].no_detections = 0;
    accslices_[i].ddbox.reserve(max_acclen_, kernel_widths_);

    accum_counter += accum_step;
  }
}

// ============================================================================
// ============================================================================
// Parameters 

const json BoxLutDetector::param_schema_ = R"(
    {
      "type": "object",
      "properties" : {
        "type" : {
          "description" : "Detector type",
          "type" : "string",
          "enum" : ["BoxLut"],
          "default" : "BoxLut"
        },
        "threshold" : {
          "description" : "Accept only detection with ddbox response above this theshold",
          "type" : "number",
          "minimum" : 1,
          "maximum" : 200,
          "default" : 30
        },
        "peak_ratio" : {
          "description" : "Peak ratio",
          "type" : "number",
          "minimum" : 1,
          "maximum" : 2,
          "default" : 1.2
        },
        "curv_ratio" : {
          "description" : "Curv ratio",
          "type" : "number",
          "minimum" : 1,
          "maximum" : 3,
          "default" : 1.2
        },
        "no_epochs" : {
          "description" : "Number of epochs for the box hysteresis filtering",
          "type" : "number",
          "minimum" : 0,
          "maximum" : 10,
          "multipleOf" : 1,
          "default" : 2
        },
        "box_delta" : {
          "description" : "Delta of the box hysteresis filtering interval",
          "type" : "number",
          "maximum" : 10,
          "exclusiveMinimum": 0,
          "default" : 2
        },
        "kernel_widths" : {
          "description" : "Convolution box function kernel widths",
          "type" : "array",
          "items" : {
            "type": "number",
            "minimum" : 3,
            "multipleOf" : 1
          },
          "minItems" : 3,
          "maxItems" : 20,
          "default" : [3, 5, 7, 9, 13, 17, 21, 29, 37, 53]
        },        
        "classifier" : {
          "description" : "2D dot classifier type",
          "type" : "string",
          "enum" : ["PassThru", "CircLinear", "ExCircLinear"],
          "default" : "PassThru"
        },
        "classifier_params" : {
          "description" : "2D dot classifier parameters",
          "type" : "object",
          "default" : {}
        },
        "no_threads" : {
          "description" : "Number of threads for the detector to use",
          "minimum" : 1,
          "maximum" : 10,
          "default" : 4
        }
      }
    }
  )"_json;
  
// ============================================================================

/*
inline void BoxLutDetector::setDefaultParams(void)
{
  pmap_.setSchema(param_schema_);
  setParams(pmap_.getParams());
}
*/

// ============================================================================

inline void BoxLutDetector::updateParams(const nlohmann::json &params, bool value_only)
{
  if (params.find("kernel_widths") != params.end())
    {
      json kernel_widths_json = pmap_.getParam("kernel_widths");
      int no_widths = kernel_widths_json.size();
      kernel_widths_.resize(no_widths);

      for (int i = 0; i < no_widths; i++)
        kernel_widths_[i] = kernel_widths_json[i];

      if (!value_only)
        setupAccumSlices();
    }

  if (params.find("threshold") != params.end())
    threshold_param_ = uint8_t(pmap_.getParam("threshold").get<double>());

  if (params.find("peak_ratio") != params.end())
    peak_ratio_param_ = fpreal16(pmap_.getParam("peak_ratio").get<double>());

  if (params.find("curv_ratio") != params.end())
    curv_ratio_param_ = fpreal16(pmap_.getParam("curv_ratio").get<double>());

  if (params.find("no_epochs") != params.end())
    {
      no_epochs_param_ = uint8_t(pmap_.getParam("no_epochs").get<double>());
      if (!value_only)
        {
          for (unsigned int i = 0; i < no_accums_; i++)
            {
              valid_epochs_[i] = 0;
              peak_history_[i] = -1;
            }
        }
    }

  if (params.find("box_delta") != params.end())
    box_delta_param_ = fpreal16(pmap_.getParam("box_delta").get<double>());

  if (params.find("classifier") != params.end())
    {
      classifier_.reset(dc::Factory::create(pmap_.getParam("classifier").get<std::string>()));
      if (!value_only)
        classifier_->setFrameParams(iwidth_, iheight_, stride_);
    }

  if (classifier_.get() && (params.find("classifier_params") != params.end()))
    {
      classifier_->setParams(params.at("classifier_params"));
    }

  if (params.find("no_threads") != params.end())
    {
      no_threads_param_ = uint32_t(pmap_.getParam("no_threads").get<double>());
#ifdef MKEDD_MULTITHREADING
      threadpool_.setNoThreads(no_threads_param_);
#endif
      if (!value_only)
        setupAccumSlices();
    }
}

// ============================================================================

void BoxLutDetector::validateParams(const nlohmann::json &params)
{
  pmap_.validateParams(params);

  if (classifier_.get() && (params.find("classifier_params") != params.end()))
    classifier_->validateParams(params);
}

// ============================================================================

nlohmann::json BoxLutDetector::getParamsSchema(void) const
{
  return pmap_.getSchema();
}

// ============================================================================

void BoxLutDetector::setParams(const nlohmann::json &params)
{
  pmap_.setParams(params);
  updateParams(params);
}

// ============================================================================

void BoxLutDetector::getParams(nlohmann::json &params) const
{
  params = pmap_.getParams();
  json classifier_params;

  if (classifier_.get())
    {
      classifier_->getParams(classifier_params);

      if (classifier_params.is_null())
        classifier_params = json::object();

      params["classifier_params"].swap(classifier_params);
    }
}

// ============================================================================

const std::string BoxLutDetector::getTypeString(void)
{
  return staticGetTypeString();
}

// ============================================================================
// ==========================================================================
// Runtime statistics

void BoxLutDetector::resetStats(void)
{
  user_timer_.reset();  
  pxlist_timer_.reset();
  acclist_timer_.reset();
  conv_timer_.reset();
  nms_timer_.reset();
  bpeak_timer_.reset();
}

// ============================================================================

void BoxLutDetector::getStats(nlohmann::json &stats) const
{
#ifndef __nios2_arch__
  stats["user_timer_s"] = user_timer_.elapsed<double, std::ratio<1>>();
  stats["pxlist_timer_s"] = pxlist_timer_.elapsed<double, std::ratio<1>>();
  stats["acclist_timer_s"] = acclist_timer_.elapsed<double, std::ratio<1>>();
  stats["conv_timer_s"] = conv_timer_.elapsed<double, std::ratio<1>>();
  stats["nms_timer_s"] = nms_timer_.elapsed<double, std::ratio<1>>();
  stats["bpeak_timer_s"] = bpeak_timer_.elapsed<double, std::ratio<1>>();
#endif
}

// ============================================================================
// ============================================================================
// Data loading
  
void BoxLutDetector::loadPixelList(std::istream *is)
{
  uint16_t px[10];
  uint32_t offset = 0, offset_prev = 0;
  uint32_t slice_idx = 0;
  uint32_t slice_counter = 0;
  uint32_t slice_step = std::ceil(double(no_pxs_) / double(pixslices_.size()));
  
  for (unsigned int i = 0; i < no_pxs_; i++)
    {
      is->read((char *) px, 10);

      offset = uint32_t(px[1]) * uint32_t(stride_) + uint32_t(px[0]);
      
      if (slice_counter == 0)
        {
          pixellist_[i].offset = 0;

          pixslices_[slice_idx].offset = offset;
          pixslices_[slice_idx].pix_min = i;
          slice_idx++;
          slice_counter++;
        }
      else if ((slice_counter == slice_step) && (slice_step != 0))
        {
          pixellist_[i].offset = 0;
          pixslices_[slice_idx - 1].pix_max = i;

          pixslices_[slice_idx].offset = offset;
          pixslices_[slice_idx].pix_min = i;
          slice_idx++;
          slice_counter = 1;          
        }
      else
        {
          pixellist_[i].offset = uint16_t(offset - offset_prev);
          slice_counter++;
        }
  
      offset_prev = offset;
      
      pixellist_[i].acc_idx = px[2]; 
      pixellist_[i].ael_idx = px[3];
      pixellist_[i].weight = px[4];
    }
    
  if ((slice_counter != 1) && (slice_idx > 0))
    pixslices_[slice_idx - 1].pix_max = no_pxs_;       
}  
  
// ============================================================================

void BoxLutDetector::loadAccumLengths(std::istream *is)
{
  // Accum lengths
  is->read((char *) &acclens_[0], no_accums_ * sizeof(uint32_t)); 
  
  // Compute max/sum of accum lengths
  uint32_t csum = 0;
  max_acclen_ = 0;
  for (unsigned int i = 0; i < no_accums_; i++)
    {
      accoffs_[i] = csum;
      csum += acclens_[i];
      if (acclens_[i] > max_acclen_)
        max_acclen_ = acclens_[i];
    }    
}

// ============================================================================

void BoxLutDetector::loadAccumFlags(std::istream *is)
{
  is->read((char *) &aflags_[0], no_accels_ * sizeof(uint8_t));   
}

// ============================================================================

void BoxLutDetector::loadAccumFlagsBounds(std::istream *is)
{
  for (unsigned int i = 0; i < no_accums_; i++)
    {
      uint32_t bounds[2];
      is->read((char *) &bounds, 2 * sizeof(uint32_t)); 
      afaidx_[i] = bounds[0];
      afbidx_[i] = bounds[1];      
    }
}

// ============================================================================

void BoxLutDetector::loadAccumulatorsXY(std::istream *is)
{
  is->read((char *) &no_accums_, sizeof(no_accums_)); 
  is->read((char *) &no_accels_, sizeof(no_accels_)); 
  is->read((char *) &no_pxs_, sizeof(no_pxs_)); 

  allocAccumMemory();

  // Load Accum lengths
  loadAccumLengths(is);
    
  // Initialize ddbox
  setupAccumSlices();

  // Accum flags
  loadAccumFlags(is);
  
  // Find shoulder boundaries
  for (unsigned int i = 0; i < no_accums_; i++)
    {
      afaidx_[i] = 0;
      afbidx_[i] = 0;
      
      if (!acclens_[i])
        continue;
      
      for (unsigned int j = 1; j < acclens_[i] - 1; j++)
        {
          if (aflags_[accoffs_[i] + j])
            {
              afaidx_[i] = j - 1;
              break;
            }               
        }

      for (int j = acclens_[i] - 1; j > 0; j--)
        {
          if (aflags_[accoffs_[i] + j])
            {
              afbidx_[i] = j + 1;
              break;
            }               
        }
        
      if (afaidx_[i] > afbidx_[i])
        {
            afaidx_[i] = 0;
            afbidx_[i] = 0;
        }
    }
  
  // Load Pixel list
  allocPixelListMemory();
  loadPixelList(is);
}

// ============================================================================

void BoxLutDetector::loadLut3D2DID(std::istream *is)
{  
  is->read((char *) &iwidth_, sizeof(iwidth_)); 
  is->read((char *) &iheight_, sizeof(iheight_)); 
  is->read((char *) &stride_, sizeof(stride_));
  
  classifier_->setFrameParams(iwidth_, iheight_, stride_);
  
  loadAccumulatorsXY(is);
  
  lut_.loadDataBinary(is);
}

// ============================================================================

void BoxLutDetector::loadLutsV2(std::istream *is)
{   
  // 2D buffer info
  is->read((char *) &iwidth_, sizeof(iwidth_)); 
  is->read((char *) &iheight_, sizeof(iheight_)); 
  is->read((char *) &stride_, sizeof(stride_));

  // 3D info
  is->read((char *) &depth_min_, sizeof(iheight_)); 
  is->read((char *) &depth_max_, sizeof(stride_));

  // PixelList
  is->read((char *) &no_pxs_, sizeof(no_pxs_));
  allocPixelListMemory();
  loadPixelList(is);  

  // Paths
  is->read((char *) &no_accums_, sizeof(no_accums_)); 
  is->read((char *) &no_accels_, sizeof(no_accels_)); 
  allocAccumMemory();
  loadAccumLengths(is);
  loadAccumFlags(is);
  loadAccumFlagsBounds(is);

  // Path ID LUT 
  util::DataLutUInt32 lut_id;
  lut_id.loadDataBinary(is, no_accums_, 3);

  // Path data LUT
  lut_.reserve(no_accels_, 1);

  int k = 0;
  for (unsigned int i = 0; i < no_accums_; i++)
    {
      auto id = lut_id.getData(i);
      for (unsigned int j = 0; j < acclens_[i]; j++)
        {
          PointDataLut det;
          is->read((char *) &det, sizeof(PointDataLut) - 3 * sizeof(int16_t)); // DetectorLut - uid - id1 - id2
          det.uid = int16_t(id[0]);
          det.id1 = int16_t(id[1]);
          det.id2 = int16_t(id[2]);
          lut_.setData(k++, &det);
        }
    }
}
  
// ============================================================================

void BoxLutDetector::load(std::istream *is, const SectionType section)
{
  if ((section != DD_SECTION_LUT3D2DID) && (section != DD_SECTION_LUTSV2))
    throw std::runtime_error("BoxLut: Unable to parse detector section: " + std::to_string(section)); 
  
  try 
  {
    if (section == DD_SECTION_LUT3D2DID)
      loadLut3D2DID(is);  
    else if (section == DD_SECTION_LUTSV2)
      loadLutsV2(is);  
  }
  catch (std::exception &e)
  {
    setInitValues();
    throw;
  }
}  
  
bool BoxLutDetector::canParse(const SectionType section)
{
  if (section == DD_SECTION_LUT3D2DID)
    return true;
  else if (section == DD_SECTION_LUTSV2)
    return true;  
  else
    return false;
}

// ============================================================================
// ============================================================================
// Processing

void BoxLutDetector::processPixelSlice(const uint8_t *data, const unsigned int ps_idx)
{
  const uint8_t *px_ptr = data + pixslices_[ps_idx].offset;
  uint16_t aidx, eidx;
  
  // fill accumulators from data
  unsigned int pix_min = pixslices_[ps_idx].pix_min;
  unsigned int pix_max = pixslices_[ps_idx].pix_max;
  

  for (unsigned int i = pix_min; i < pix_max; i++)
    {
      px_ptr += pixellist_[i].offset;
      fpreal16 weight = fpreal16(pixellist_[i].weight, true);
      aidx = pixellist_[i].acc_idx;
      eidx = pixellist_[i].ael_idx;

      int32_t pixel_value = (fpreal16(int32_t(*px_ptr), false) * weight).getValue();
      int32_t *acc = (int32_t *) (&accums_[0] + (accoffs_[aidx] + eidx));
      
      *acc += pixel_value;
     // __sync_fetch_and_add(acc, pixel_value);
    }  
    
}

// ============================================================================

void BoxLutDetector::processPixelList(const uint8_t *data)
{
  // clear accumulators
  memset(&accums_[0], 0, sizeof(fpreal16) * no_accels_);
  
   // fill accumulators from data
#ifdef MKEDD_MULTITHREADING  
  for (unsigned int i = 0; i < pixslices_.size(); i++)
    threadpool_.process([this, data, i](){
        util::StatsThreadTimer t;

        t.start();
        this->processPixelSlice(data, i);
        t.stop();

        user_timer_.addElapsed(t);
        pxlist_timer_.addElapsed(t);

        return true;
    });
  
  threadpool_.wait();
#else
  pxlist_timer_.start();  
  
  for (unsigned int i = 0; i < pixslices_.size(); i++)
    processPixelSlice(data, i);
  
  pxlist_timer_.stop();        
#endif  
}

// ============================================================================

inline void BoxLutDetector::computeConvolution(const unsigned int slength, const unsigned int aidx, const unsigned int bidx, const fpreal16 *accum, fpreal16 *accsum, DdBox &ddbox)
{
  // convert accumulator to cumsum and divide by 2^csshift
  fpreal16 csum = 0;

  // Compute cummulative sum of accum shifted down by accel_min 
  for (unsigned int j = 0; j < slength; j++)
    {
      fpreal16 accel = accum[j] >> csshift_;
      csum += accel;
      accsum[j] = csum;
    }

  // convolve accumulator w/ 2nd derivative Gaussian w/ varying kernel sizes
  ddbox.convolve(accsum, slength, aidx, bidx);
}

// ============================================================================

inline unsigned int BoxLutDetector::findPeaks(const unsigned int slength, const unsigned int aidx, const unsigned int bidx, const uint8_t *accflags, DdBox &ddbox)
{
  // Select tentative dots as local maxima that are > threshold and at a legal position   
  fpreal16 thresh = fpreal16(threshold_param_) >> csshift_;
  unsigned int no_peaks = ddbox.findPeaks(slength, thresh, aidx, bidx, accflags); 
  return no_peaks;    
}

// ============================================================================

inline int BoxLutDetector::selectBestPeak(const unsigned int slength, const unsigned int accoff, const fpreal16 *accum, const uint8_t *data, DdBox &ddbox)
{
  int best_det_idx = -1;
  fpreal16 resp1 = 0;
  fpreal16 resp2 = 0;

  const DdBoxDetector<fpreal16>::PeakInfo *peaks = ddbox.getPeaks();
  const unsigned int no_peaks = ddbox.getNoPeaks();
  

  for (unsigned int j = 0; j < no_peaks; j++)
    {
      const unsigned int &scale = peaks[j].scale;
      const unsigned int &off = peaks[j].offset;
      const fpreal16 &cur_resp = peaks[j].response;
                    
      // 1-D Curvature ratio test
      int w = ddbox.getBoxWidth(scale) >> 1;
          
      int rb = off + w;
      if ((rb >= int(slength)) || (accum[rb] * curv_ratio_param_ >= accum[off]))
        continue;
          
      int lb = off - w;
      if ((lb < 0) || (accum[lb] * curv_ratio_param_ >= accum[off]))
        continue;
      
      /*
      if ((accoff > (slength >> 1)) && (w > 13))
        continue;

      if ((accoff < (slength >> 1)) && (w < 13))
        continue;
      */
      
      // Pre-selection is done, its time to call
      // full dot classifier
      fpreal16r u, v;
      uint32_t accel_idx = accoff + off;
      const PointDataLut *dl = lut_.getData(accel_idx);
      u.assign(dl->u);
      v.assign(dl->v);
          
      if (!classifier_->classify(data, int(u), int(v), dl->uid, &(dl->x)))
        continue;

      // Keep the two highest peaks
      if (cur_resp > resp1)
        {
          best_det_idx = j;
          resp2 = resp1;
          resp1 = cur_resp;
        }
      else if (cur_resp > resp2)
        {
          resp2 = cur_resp;
        }
    }
    
  // Peak ratio test  
  if ((best_det_idx >= 0) && ((resp2 * peak_ratio_param_) <= resp1))
    return best_det_idx;
  else
    return -1;
}

// ============================================================================

inline void BoxLutDetector::decValidEpochs(const unsigned int accum_idx)
{
  valid_epochs_[accum_idx] = std::max(0, valid_epochs_[accum_idx] - 1);
}

// ============================================================================

inline void BoxLutDetector::incValidEpochs(const unsigned int accum_idx, const fpreal16 offset)
{
  valid_epochs_[accum_idx] = std::min(no_epochs_param_, uint8_t(1 + valid_epochs_[accum_idx]));
  peak_history_[accum_idx] = offset;
}

// ============================================================================

inline void BoxLutDetector::setEmptyDetection(const unsigned int accum_idx, PointData &detection)
{ 
  detection.confidence = 0;  
  
  #ifdef MKEDD_RESERVED_API
  det_accels_[accum_idx] = 0;
  #endif  
}

// ============================================================================

inline bool BoxLutDetector::setDetection(const unsigned int accum_idx, const unsigned int accel_offset, const fpreal16 offset, const DdBox &ddbox, const PeakInfo &peak, PointData &detection)
{
  // Is the offset a valid detection?
  fpreal16 foffset = floor(offset);
  fpreal16 alpha = offset - foffset;
  unsigned int off1 = accel_offset + int(foffset);
  unsigned int off2 = off1 + 1;
  bool oflag1 = aflags_[off1];
  bool oflag2 = aflags_[off2];

  if (!oflag1 && !oflag2)
    {
      setEmptyDetection(accum_idx, detection);
      return false;
    }
  else if (!oflag1)
    off1 = off2;
  else if (!oflag2)
    off2 = off1;
  
  // Read out detection LUT
  *static_cast<PointDataLut *>(&detection) = *lut_.getData(off1);
  detection.confidence = peak.response.getValue() << csshift_;
  detection.sigma2D = ddbox.getSigma(peak.scale).getValue();
   
  if (off1 != off2)
    {
      // Linear interpolation of 3D data
      // NB: We should be doing the same for 2D data, maybe later 
      const PointDataLut *det2_ptr = lut_.getData(off2);
      const fpreal16 beta = fpreal16(1) - alpha;

      detection.x = int(alpha * fpreal16(det2_ptr->x) + beta * fpreal16(detection.x));
      detection.y = int(alpha * fpreal16(det2_ptr->y) + beta * fpreal16(detection.y));
      detection.z = int(alpha * fpreal16(det2_ptr->z) + beta * fpreal16(detection.z));
    }

    #ifdef MKEDD_RESERVED_API
    det_accels_[accum_idx] = off1;
    #endif  
    return true;
}

// ============================================================================

inline unsigned int BoxLutDetector::applyBoxingFilter(const unsigned int accum_idx, const unsigned int accel_offset, const fpreal16 offset, const DdBox &ddbox, const PeakInfo &peak, PointData &detection)
{
  unsigned int valid_detection = 0;
 
  if (!valid_epochs_[accum_idx])
    {
      if (offset < fpreal16(0))
        {
          setEmptyDetection(accum_idx, detection);
          decValidEpochs(accum_idx);
        }
      else
        {
          setDetection(accum_idx, accel_offset, offset, ddbox, peak, detection);
          dts_history_[accum_idx] = detection;
          incValidEpochs(accum_idx, offset);
          valid_detection++;
        }
    }
  else
    {
      if (offset < fpreal16(0))
        {
           detection = dts_history_[accum_idx];
           decValidEpochs(accum_idx);
        }
      else if (offset < (peak_history_[accum_idx] - box_delta_param_))
        {
          valid_detection++;
          fpreal16 updated_offset = offset + box_delta_param_;
          if (!setDetection(accum_idx, accel_offset, updated_offset, ddbox, peak, detection))
            {
              detection = dts_history_[accum_idx];
              decValidEpochs(accum_idx);
            }
          else
            {
              dts_history_[accum_idx] = detection;
              incValidEpochs(accum_idx, offset);
            }
        }
      else if (offset > (peak_history_[accum_idx] + box_delta_param_))
        {
          valid_detection++;
          fpreal16 updated_offset = offset - box_delta_param_;
          if (!setDetection(accum_idx, accel_offset, updated_offset, ddbox, peak, detection))
            {
              detection = dts_history_[accum_idx];
              decValidEpochs(accum_idx);
            }
          else
            {
              dts_history_[accum_idx] = detection;
              incValidEpochs(accum_idx, offset);            
            }
        }
      else
        {
          valid_detection++;
          detection = dts_history_[accum_idx];
          incValidEpochs(accum_idx, peak_history_[accum_idx]);
        }
    }  
    
    return valid_detection;
}

// ============================================================================

void BoxLutDetector::processAccumSlice(const uint8_t *data, const unsigned int acc_idx, std::vector<PointData>& detections)
{  
  uint32_t acc_min = accslices_[acc_idx].accum_min;
  uint32_t acc_max = accslices_[acc_idx].accum_max;
  uint32_t &no_detections = accslices_[acc_idx].no_detections;
  DdBox &ddbox = accslices_[acc_idx].ddbox;
  
  util::StatsThreadTimer ct;
  util::StatsThreadTimer nmst;
  util::StatsThreadTimer bpt;
  
  no_detections = 0;
    
  for (unsigned int i = acc_min; i < acc_max; i++)
    {
      unsigned int accoff = accoffs_[i];
      fpreal16 *accum = &accums_[accoff];
      fpreal16 *accsum = &accumsums_[accoff];
      uint8_t *accflags = &aflags_[accoff];
      unsigned int slength = acclens_[i];
      unsigned int aidx = afaidx_[i];
      unsigned int bidx = afbidx_[i];      
      PointData &detection = detections[i];
      
      // 1D signal convolution
      ct.start();
      computeConvolution(slength, aidx, bidx, accum, accsum, ddbox);
      ct.stop();

      // Preliminary peak detection in 1D accum signal
      nmst.start();
      findPeaks(slength, aidx + 1, bidx - 1, accflags, ddbox);
      nmst.stop();
      
      const PeakInfo *peaks = ddbox.getPeaks();
      
      // More precise peak detection and selection
      bpt.start();
      int best_peak_idx = selectBestPeak(slength, accoff, accum, data, ddbox);
      bpt.stop();
      
      const PeakInfo &best_peak = peaks[best_peak_idx];
      fpreal16 peak_offset = fpreal16(-1);
      
      // Sub-sample peak fitting
      if (best_peak_idx >= 0)
        peak_offset = ddbox.fitPeak(best_peak_idx, slength, csshift_);

      // lookup and filter the detection
      no_detections += applyBoxingFilter(i, accoff, peak_offset, ddbox, best_peak, detection);
    }
    
  conv_timer_.addElapsed(ct);
  nms_timer_.addElapsed(nmst);
  bpeak_timer_.addElapsed(bpt);
}

// ============================================================================

void BoxLutDetector::processAccumulators(const uint8_t *data, std::vector<PointData>& detections)
{
 
#ifdef MKEDD_MULTITHREADING
  for (unsigned int i = 0; i < accslices_.size(); i++)
    threadpool_.process([this, &detections, data, i](){
        util::StatsThreadTimer t;

        t.start();
        this->processAccumSlice(data, i, detections); 
        t.stop();

        user_timer_.addElapsed(t);
        acclist_timer_.addElapsed(t);

        return true;
    });
  
  threadpool_.wait();
#else
  acclist_timer_.start();

  for (unsigned int i = 0; i < accslices_.size(); i++)
    processAccumSlice(data, i, detections);

  acclist_timer_.stop();
#endif    
  
  no_detections_ = 0;
  for (unsigned int i = 0; i < accslices_.size(); i++)
    no_detections_ += accslices_[i].no_detections;
}

// ============================================================================
// ============================================================================

uint32_t BoxLutDetector::process(const uint8_t *data, std::vector<PointData>& detections)
{
  user_timer_.start();

#ifdef MKEDD_RESERVED_API
  classifier_->initDebugData();
#endif

  // Update accumulators using image data
  processPixelList(data);
  
  // Process accumulators
  processAccumulators(data, detections);
  
#ifdef MKEDD_RESERVED_API
  // We need to re-update the accumulators because at this point
  // it is already converted to the cumsum version
  processPixelList(data);

 if (accdump_file_param_ != "")
   dumpAccumulators();
#endif

  user_timer_.stop();
 
 return no_detections_;
}
  
// ============================================================================

uint32_t BoxLutDetector::getMaxNoDetections(void) const
{
  return no_accums_;  
}

// ============================================================================

uint32_t BoxLutDetector::getStride(void) const
{
  return stride_;
}

// ============================================================================

mke::dd::Data3dType BoxLutDetector::getData3dType(void) const
{
  return DD_DATA3D_MM;
}

// ============================================================================
// ============================================================================

#ifdef MKEDD_RESERVED_API
void BoxLutDetector::dumpAccumulators(void)
{
  if (accdump_file_param_ == "")
   throw std::logic_error("File name parameter cannot be empty string");

  std::ofstream bFile(accdump_file_param_.c_str());

  if (!bFile)
   throw std::runtime_error(std::string("Cannot open accumulator file for writing: ") + accdump_file_param_);

  // Dump detection det_accels
  bFile << no_detections_ << " " << 0 << std::endl;  
  for (int i = 0; i < no_accums_; i++)
    {
      if (det_accels_[i])
        bFile << det_accels_[i] << " " << 0 << std::endl;
    }
  
  // Dump accumulator: accval double(accval)
  for (int i = 0; i < no_accels_; i++)
    {
      int32_t accval = accums_[i].getValue();
      bFile << accval << " " << double(accums_[i]) << std::endl;
    }

  bFile.close();
}

// ============================================================================

void BoxLutDetector::getDebugData(nlohmann::json &data) const
{
  data.clear();

  if (!no_accums_)
    {
      data["det_idx"] = nlohmann::json::array();
      data["accums"] = nlohmann::json::array();
      return;
    }

  nlohmann::json accums_json = nlohmann::json::array();
  nlohmann::json det_idx = nlohmann::json::array();

  accums_json[no_accums_ - 1] = nullptr;
  det_idx[no_accums_ - 1] = nullptr;

  for (int i = 0; i < no_accums_; i++)
    {
      if (!det_accels_[i])
        det_idx[i] = 0;
      else
        det_idx[i] = det_accels_[i] - accoffs_[i];

      nlohmann::json accum_json = nlohmann::json::array();

      if (acclens_[i])
        {
          accum_json[acclens_[i] - 1] = nullptr;

          for (uint32_t j = 0; j < acclens_[i]; j++)
            accum_json[j] = double(accums_[accoffs_[i] + j]);

          accums_json[i].swap(accum_json);
        }
    }

  data["det_idx"].swap(det_idx);
  data["accums"].swap(accums_json);

  classifier_->getDebugData(data["classifier"]);
}

// ============================================================================

void BoxLutDetector::getTriangulation(nlohmann::json &triangulation)
{
  throw std::runtime_error("[MkEDD] BoxLutDetector: getTriangulation not implemented");
}

#endif 
 
  
#endif
