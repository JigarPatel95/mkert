/*
 * BoxLutDetector, revision 3
 *
 * Copyright (c) 2018, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifndef _DD_BOXLUT3_H_
#define _DD_BOXLUT3_H_


#include <vector>

#include "types/fpreal.h"
#include "dd/ddbox.h"
#include "dd/detector.h"
#include "dd/colpath.h"
#include "dd/accum.h"
#include "dc/classifier.h"
#include "util/datalut.h"
#include "util/timers.h"

#include "util/pmap.h"

#ifdef MKEDD_MULTITHREADING
#include "util/threadpool.h"
#endif

namespace mke {
namespace dd {
namespace dd {

/** \addtogroup mkedd
 *  @{
 */

class BoxLut3Detector : public Detector {
private:

  // Params
  std::vector<int32_t> kernel_widths_;
  uint8_t threshold_param_;
  types::fpreal16 peak_ratio_param_;
  types::fpreal16 curv_ratio_param_;
  bool fit_peaks_param_;
  types::fpreal16 box_delta_param_;
  uint8_t no_epochs_param_;
  uint32_t no_threads_param_;

  util::ParameterMap pmap_;
  static const nlohmann::json param_schema_;

  // Dot classifier
  std::unique_ptr<dc::Classifier> classifier_;

  // 2D buffer info
  uint32_t iwidth_;
  uint32_t iheight_;
  uint32_t stride_;

  // 3D info
  uint32_t depth_min_;
  uint32_t depth_max_;

  // Paths
  struct PathsSlice {
    std::vector<uint32_t> path_idx;
    DdBoxDetector<types::fpreal16> ddbox;
    Accumulator<types::fpreal16> accum;
    const DdBoxDetector<types::fpreal16>::PeakInfo *peak_info;
    types::fpreal16 peak_offset;
    uint32_t no_detections;
  };

  std::vector<CollatedLitePath> paths_;
  std::vector<PathsSlice> path_slices_;
  uint32_t max_pels_;

  // History
  std::vector<uint8_t> valid_epochs_;
  std::vector<types::fpreal16> peak_history_;
  std::vector<PointData> dets_history_;

  // Results
  uint32_t no_detections_;

  // Runtime statistics
  util::StatsThreadTimer user_timer_;
  util::StatsThreadTimer pxlist_timer_;
  util::StatsThreadTimer acclist_timer_;
  util::StatsThreadTimer conv_timer_;
  util::StatsThreadTimer nms_timer_;
  util::StatsThreadTimer bpeak_timer_;

  // Utils
  const static uint32_t max_dpp_ = 100;
  const static int32_t csshift_ = 8;

#ifdef MKEDD_MULTITHREADING
  util::ThreadPool<util::SHARED_THREADPOOL> threadpool_;
#endif

#ifdef MKEDD_RESERVED_API
  static const nlohmann::json reserved_param_schema_;
  nlohmann::json dbgdata_;
  bool store_dbg_data_;
#endif

  // ==========================================================================
  // Data

  void setInitValues(void);

  // ==========================================================================
  // Parameters

  void updateParams(const nlohmann::json &params, bool value_only = false);

  // ==========================================================================
  // Data loading

  void loadColLut(std::istream *is);

  // ==========================================================================
  // Processing

  int selectBestPeak(const uint8_t *data, PathsSlice &pslice, const CollatedLitePath &path);

  void setEmptyDetection(const uint32_t pidx, PointData &detection);
  uint32_t setDetection(const uint32_t pidx, PathsSlice &pslice, PointData &detection);

  void incValidEpochs(const uint32_t pidx, const types::fpreal16 offset);
  void decValidEpochs(const uint32_t pidx);
  uint32_t applyBoxingFilter(const uint32_t pidx, PathsSlice &pslice, PointData &detection);

  void processPathSlice(const uint8_t *data, PathsSlice &pslice, std::vector<PointData>& detections);
  void setupPathsSlices(const unsigned int no_slices);

public:

  // ==========================================================================
  // Static methods

  static const std::string staticGetTypeString(void)
  {
    static const std::string typeString = "BoxLut3";
    return typeString;
  }

  static Detector* create(void)
  {
    return new BoxLut3Detector();
  }

  // ==========================================================================
  // Construction

  BoxLut3Detector()
  {
    // Initial parameter values
    pmap_.addSchema(param_schema_);

#ifdef MKEDD_RESERVED_API
    pmap_.addSchema(reserved_param_schema_);
#endif

    updateParams(pmap_.getParams(), true);

   setInitValues();
  }

  ~BoxLut3Detector()
    {}

  // ==========================================================================
  // Parameters

  void validateParams(const nlohmann::json &params);
  nlohmann::json getParamsSchema(void) const;
  void setParams(const nlohmann::json &params);
  void getParams(nlohmann::json &params) const;
  const std::string getTypeString(void);

  // ==========================================================================
  // Runtime statistics

  virtual void resetStats(void);
  virtual void getStats(nlohmann::json &stats) const;

  // ==========================================================================
  // Data loading

  bool canParse(const SectionType section);
  void load(std::istream *is, const SectionType section);

  // ==========================================================================
  // Processing

  uint32_t process(const uint8_t *data, std::vector<PointData>& detections);
  uint32_t getMaxNoDetections(void) const;
  uint32_t getStride(void) const;
  mke::dd::Data3dType getData3dType(void) const;

  // ==========================================================================
  // Reserved API

#ifdef MKEDD_RESERVED_API
  void getDebugData(nlohmann::json &data) const;
  void getTriangulation(nlohmann::json &triangulation);
#endif

};

/** @}*/

} /* namespace dd */
} /* namespace dd */
} /* namespace mke */


#endif // _DD_BOXLUT3_H_
