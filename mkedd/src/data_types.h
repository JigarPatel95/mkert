/*
 * mkedd: Data types
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifndef _DATA_TYPES_H_
#define _DATA_TYPES_H_

#include <stdint.h>

namespace mke {
namespace dd {

/** \addtogroup mkedd
 *  @{
 */

enum Data3dType {
  DD_DATA3D_MM = 0,
  DD_DATA3D_MM2 = 1,
  DD_DATA3D_MM4 = 2,
  DD_DATA3D_MM8 = 3,
  DD_DATA3D_MM16 = 4
};

struct PointDataLut {
  int32_t u;          // fpreal16
  int32_t v;          // fpreal16
  int32_t sigma3D;    // fpreal16
  int16_t x;
  int16_t y;
  int16_t z;
  uint16_t uid;
  uint16_t id1;
  uint16_t id2;
}; 

struct PointData : public PointDataLut {
  /*
  int32_t u;          // fpreal16
  int32_t v;          // fpreal16
  int32_t sigma3D;    // fpreal16
  int16_t x;
  int16_t y;
  int16_t z;
  int16_t uid;
  int16_t id1;
  int16_t id2;
  */
  int32_t sigma2D;    // fpreal16
  int32_t confidence; // fpreal16
};


/** @}*/

} /* namespace dd */
} /* namespace mke */

#endif // _DATA_TYPES_H_
