/*
 * dc_circlin - Dot classifier based on circular mask descriptor and linear classification
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifndef _DC_CIRCLIN_
#define _DC_CIRCLIN_

#include "dc/classifier.h"
#include "util/pmap.h"

#ifdef MKEDD_RESERVED_API
#include <thread>
#include <mutex>
#endif

namespace mke {
namespace dd {
namespace dc {
  
/** \addtogroup mkedd
 *  @{
 */


class CircLinClassifier : public Classifier {
private:
  static const int pixel_list_[][3];
  static const int no_mask_pxs_[3];
  static const int mask_width_ = 7;
  static const int no_masks_ = 3;
  static const int no_pxs_ = 69;

  util::ParameterMap pmap_;
  static nlohmann::json param_schema_;

#ifdef MKEDD_RESERVED_API
  static nlohmann::json reserved_param_schema_;
  bool store_dbg_data_;
  nlohmann::json patches_;
  std::mutex patches_mutex;
#endif

  static void initSchema(void);
  
public:
  // ==========================================================================
  // Construction 

  CircLinClassifier()
  {
    initSchema();
    pmap_.setSchema(param_schema_);

#ifdef MKEDD_RESERVED_API
    pmap_.addSchema(reserved_param_schema_);
    store_dbg_data_ = pmap_.getParam("store_dbg_data").get<bool>();
    patches_ = nlohmann::json::array();
#endif
  }
  
  ~CircLinClassifier()
    {}

  // ==========================================================================
  // Static methods
  
  static const std::string getTypeString(void)
  {
    static const std::string typeString = "CircLinear";
    return typeString;
  }
  
  static Classifier* create(void)
  {
    return new CircLinClassifier();
  }

  // Virtual methods ==========================================================  
  
  bool classify(const uint8_t *data, const int x, const int y,
                const uint32_t uid, const int16_t *pt3d);

  void validateParams(const nlohmann::json &);
  void setParams(const nlohmann::json &);
  void getParams(nlohmann::json &) const;

#ifdef MKEDD_RESERVED_API
  void initDebugData(void);
  void getDebugData(nlohmann::json &dbgdata);
#endif
};

} // dc
} // dd
} // mke

#endif // _DC_CIRCLIN_
