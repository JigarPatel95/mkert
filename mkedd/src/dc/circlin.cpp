/*
 * dc_circlin - Dot classifier based on circular mask descriptor and linear classification
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifdef MKEDD_USE_CIRCLIN_CLASSIFIER

#include "types/fpreal.h"
#include "dc/circlin.h"

#include <iostream>
#include <fstream>


using namespace mke::dd::dc;
using namespace mke::dd::types;

using json = nlohmann::json;


const int CircLinClassifier::no_mask_pxs_[3] = { 9, 20, 40};

const int CircLinClassifier::pixel_list_[][3] = {
  { -7,  -2,   2}, { -7,  -1,   2}, { -7,   0,   2}, { -7,   1,   2}, 
  { -7,   2,   2}, { -6,  -4,   2}, { -6,  -3,   2}, { -6,   3,   2}, 
  { -6,   4,   2}, { -5,  -5,   2}, { -5,   5,   2}, { -4,  -6,   2}, 
  { -4,  -1,   1}, { -4,   0,   1}, { -4,   1,   1}, { -4,   6,   2}, 
  { -3,  -6,   2}, { -3,  -2,   1}, { -3,   2,   1}, { -3,   6,   2}, 
  { -2,  -7,   2}, { -2,  -3,   1}, { -2,   3,   1}, { -2,   7,   2}, 
  { -1,  -7,   2}, { -1,  -4,   1}, { -1,  -1,   0}, { -1,   0,   0}, 
  { -1,   1,   0}, { -1,   4,   1}, { -1,   7,   2}, {  0,  -7,   2}, 
  {  0,  -4,   1}, {  0,  -1,   0}, {  0,   0,   0}, {  0,   1,   0}, 
  {  0,   4,   1}, {  0,   7,   2}, {  1,  -7,   2}, {  1,  -4,   1}, 
  {  1,  -1,   0}, {  1,   0,   0}, {  1,   1,   0}, {  1,   4,   1}, 
  {  1,   7,   2}, {  2,  -7,   2}, {  2,  -3,   1}, {  2,   3,   1}, 
  {  2,   7,   2}, {  3,  -6,   2}, {  3,  -2,   1}, {  3,   2,   1}, 
  {  3,   6,   2}, {  4,  -6,   2}, {  4,  -1,   1}, {  4,   0,   1}, 
  {  4,   1,   1}, {  4,   6,   2}, {  5,  -5,   2}, {  5,   5,   2}, 
  {  6,  -4,   2}, {  6,  -3,   2}, {  6,   3,   2}, {  6,   4,   2}, 
  {  7,  -2,   2}, {  7,  -1,   2}, {  7,   0,   2}, {  7,   1,   2}, 
  {  7,   2,   2}
};

json CircLinClassifier::param_schema_;

#ifdef MKEDD_RESERVED_API
json CircLinClassifier::reserved_param_schema_;
#endif

// ============================================================================

void CircLinClassifier::initSchema(void)
{
  if (param_schema_.is_null())
    {
       param_schema_ = R"SCHEMA(
        {
          "type": "object",
            "properties" : {
            }
          }
       )SCHEMA"_json;
 
#ifdef MKEDD_RESERVED_API
       reserved_param_schema_ = R"SCHEMA(
        {
          "type": "object",
            "properties" : {
              "store_dbg_data" : {
                "type" : "boolean",
                "default" : true
              }
            }
          }
       )SCHEMA"_json;
#endif
    }
}

// ============================================================================

bool CircLinClassifier::classify(const uint8_t *data, const int x, const int y, const uint32_t uid, const int16_t *pt3d)
{
  
 // static const fpreal16 n1 = -0.567737488599837;
 // static const fpreal16 n2 = -0.823209659830562;
 // static const fpreal16 b = -1.485612249505697;
 
  static const fpreal16 n1 = 0.613515937546589;
  static const fpreal16 n2 = 0.789682337637313;
  static const fpreal16 b = -1.484369171309642;
  
  if ((x - mask_width_ < 0) || (x + mask_width_ >= width_) ||
      (y - mask_width_ < 0) || (y + mask_width_ >= height_))
    {
      return false;
    }

  fpreal16 maxs[no_masks_] = {0};
  fpreal16 means[no_masks_] = {0};
   
  for (int i = 0; i < no_pxs_; i++)
    {
      const int &u = pixel_list_[i][0];
      const int &v = pixel_list_[i][1];
      const int &m = pixel_list_[i][2];
     
      fpreal16 val = getValue(data, x + u, y + v);
     
      if (maxs[m] < val)
        maxs[m] = val;
     
      means[m] += val;     
    }
   
  for (int i = 0; i < no_masks_; i++)
    means[i] = means[i] / fpreal16(no_mask_pxs_[i]);
 
 fpreal16 desc1 = (maxs[1].getValue() == 0) ? fpreal16(0) : means[0] / maxs[1];
 fpreal16 desc2 = (maxs[2].getValue() == 0) ? fpreal16(0) : means[1] / maxs[2];
 //fpreal16 decision = b - (n1 * desc1 + n2 * desc2);
 fpreal16 confidence = n1 * desc1 + n2 * desc2 + b;
 bool is_dot = (confidence.getValue() > 0) ? true : false;

 #ifdef MKEDD_RESERVED_API
 if (store_dbg_data_)
   {
     json patch;

     patch["desc"] = {double(desc1), double(desc2)};
     patch["confidence"] = double(confidence);
     patch["is_dot"] = is_dot;
     patch["uid"] = uid;
     patch["cx"] = x;
     patch["cy"] = y;
     patch["width"] = 15;

     json patch_data = json::array();
     patch_data[14] = nullptr;

     for (int j = -7; j < 8; j++)
       {
         json &dataline = patch_data[j + 7];
         dataline = json::array();
         dataline[14] = nullptr;

         for (int i = -7; i < 8; i++)
           dataline[i + 7] = int(getValue(data, x + i, y + j));
        }

     patch["data"].swap(patch_data);

     {
       std::lock_guard<std::mutex> lock(patches_mutex);
       patches_.push_back(nullptr);
       patches_.back().swap(patch);
     }
   }
 #endif

 return is_dot;
}

// ============================================================================
// Parameters

void CircLinClassifier::validateParams(const nlohmann::json &params)
{
  pmap_.validateParams(params);
}

void CircLinClassifier::setParams(const nlohmann::json &params)
{
  pmap_.setParams(params);

#ifdef MKEDD_RESERVED_API
  if (params.find("store_dbg_data") != params.end())
    store_dbg_data_ = pmap_.getParam("store_dbg_data").get<bool>();
#endif
}

void CircLinClassifier::getParams(nlohmann::json &params) const
{
  params = pmap_.getParams();
}

// ============================================================================
// Debugging

#ifdef MKEDD_RESERVED_API

void CircLinClassifier::initDebugData(void)
{
  patches_.clear();
}

// ============================================================================

void CircLinClassifier::getDebugData(nlohmann::json &dbgdata)
{
  dbgdata["patches"].swap(patches_);
  patches_ = json::array();
}

#endif

#endif // MKEDD_USE_CIRCLIN_CLASSIFIER
