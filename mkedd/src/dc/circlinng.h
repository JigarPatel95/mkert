/*
 * Dot classifier based on circular mask descriptor and linear classification, new generation
 * 
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifndef _DC_CIRCLINNG_
#define _DC_CIRCLINNG_

#include "dc/classifier.h"
#include "util/pmap.h"
#include "types/fpreal.h"

#include <vector>
#include <array>

#ifdef MKEDD_RESERVED_API
#include <thread>
#include <mutex>
#endif

namespace mke {
namespace dd {
namespace dc {
  
/** \addtogroup mkedd
 *  @{
 */


class NgCircLinClassifier : public Classifier {
private:
  static const std::vector<std::vector<std::array<int, 3>>> pixel_lists_;
  static const std::vector<std::array<int, 3>> no_masks_pxs_;
  static const std::array<int, 2> mask_widths_;
  static const std::array<int, 2> no_masks_;

  static const std::vector<std::vector<std::array<mke::dd::types::fpreal16, 3>>> linconstrs_;

  // Parameters
  struct Constraint {
    uint32_t type;
    uint16_t idx1;
    uint16_t idx2;
    mke::dd::types::fpreal16 value;
    uint32_t dist;
    bool value_gt;
    bool dist_gt;
  };

  std::vector<Constraint> constraints_;
  int mask_idx_;

  util::ParameterMap pmap_;
  static nlohmann::json param_schema_;

#ifdef MKEDD_RESERVED_API
  static nlohmann::json reserved_param_schema_;
  bool store_dbg_data_;
  nlohmann::json patches_;
  std::mutex patches_mutex_;
  static nlohmann::json dummy_patch_;
#endif

  static void initSchema(void);
  
public:
  // ==========================================================================
  // Construction 

  NgCircLinClassifier()
  {
    initSchema();
    pmap_.setSchema(param_schema_);
    mask_idx_ = pmap_.getParam("mask_idx").get<int>();

#ifdef MKEDD_RESERVED_API
    pmap_.addSchema(reserved_param_schema_);
    store_dbg_data_ = pmap_.getParam("store_dbg_data").get<bool>();
    patches_ = nlohmann::json::array();
#endif
  }
  
  ~NgCircLinClassifier()
    {}

  // ==========================================================================
  // Static methods
  
  static const std::string getTypeString(void)
  {
    static const std::string typeString = "NgCircLinear";
    return typeString;
  }
  
  static Classifier* create(void)
  {
    return new NgCircLinClassifier();
  }

  // Virtual methods ==========================================================  
  
  bool classify(const uint8_t *data, const int x, const int y,
                const uint32_t uid, const int16_t *pt3d);

  void validateParams(const nlohmann::json &);
  void setParams(const nlohmann::json &);
  void getParams(nlohmann::json &) const;

#ifdef MKEDD_RESERVED_API
  void initDebugData(void);
  void getDebugData(nlohmann::json &dbgdata);
  nlohmann::json& addPatch(void);
  void fillPatchData(const uint8_t *data, const uint32_t uid,
                     const int x, const int y, const int w,
                     mke::dd::types::fpreal16 *features,
                     mke::dd::types::fpreal16 *descs,
                     nlohmann::json &patch);
#endif
};

} // dc
} // dd
} // mke

#endif // _DC_CIRCLINNG_
