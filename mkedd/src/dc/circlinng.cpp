/*
 * Dot classifier based on circular mask descriptor and linear classification, new generation
 *
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifdef MKEDD_USE_NGCIRCLIN_CLASSIFIER

#include "types/fpreal.h"
#include "dc/circlinng.h"

#include <iostream>
#include <fstream>

using namespace mke::dd::dc;
using namespace mke::dd::types;
using json = nlohmann::json;

// ============================================================================
// ============================================================================
// Static initialization

const std::vector<std::array<int, 3>> NgCircLinClassifier::no_masks_pxs_
{
   {9, 20, 40},
   {9, 16, 28}
};

const std::array<int, 2> NgCircLinClassifier::no_masks_ = {3, 3};
const std::array<int, 2> NgCircLinClassifier::mask_widths_ = {7, 4};

const std::vector<std::vector<std::array<int, 3>>> NgCircLinClassifier::pixel_lists_ =
{
  {
    {-7, -2,  2},  {-7, -1,  2},  {-7,  0,  2},  {-7,  1,  2},
    {-7,  2,  2},  {-6, -4,  2},  {-6, -3,  2},  {-6,  3,  2},
    {-6,  4,  2},  {-5, -5,  2},  {-5,  5,  2},  {-4, -6,  2},
    {-4, -1,  1},  {-4,  0,  1},  {-4,  1,  1},  {-4,  6,  2},
    {-3, -6,  2},  {-3, -2,  1},  {-3,  2,  1},  {-3,  6,  2},
    {-2, -7,  2},  {-2, -3,  1},  {-2,  3,  1},  {-2,  7,  2},
    {-1, -7,  2},  {-1, -4,  1},  {-1, -1,  0},  {-1,  0,  0},
    {-1,  1,  0},  {-1,  4,  1},  {-1,  7,  2},  { 0, -7,  2},
    { 0, -4,  1},  { 0, -1,  0},  { 0,  0,  0},  { 0,  1,  0},
    { 0,  4,  1},  { 0,  7,  2},  { 1, -7,  2},  { 1, -4,  1},
    { 1, -1,  0},  { 1,  0,  0},  { 1,  1,  0},  { 1,  4,  1},
    { 1,  7,  2},  { 2, -7,  2},  { 2, -3,  1},  { 2,  3,  1},
    { 2,  7,  2},  { 3, -6,  2},  { 3, -2,  1},  { 3,  2,  1},
    { 3,  6,  2},  { 4, -6,  2},  { 4, -1,  1},  { 4,  0,  1},
    { 4,  1,  1},  { 4,  6,  2},  { 5, -5,  2},  { 5,  5,  2},
    { 6, -4,  2},  { 6, -3,  2},  { 6,  3,  2},  { 6,  4,  2},
    { 7, -2,  2},  { 7, -1,  2},  { 7,  0,  2},  { 7,  1,  2},
    { 7,  2,  2}
  },
  {
    {-4, -2,  2},  {-4, -1,  2},  {-4,  0,  2},  {-4,  1,  2},
    {-4,  2,  2},  {-3, -3,  2},  {-3, -2,  2},  {-3,  2,  2},
    {-3,  3,  2},  {-2, -3,  2},  {-2, -2,  1},  {-2, -1,  1},
    {-2,  0,  1},  {-2,  1,  1},  {-2,  2,  1},  {-2,  3,  2},
    {-1, -4,  2},  {-1, -2,  1},  {-1, -1,  0},  {-1,  0,  0},
    {-1,  1,  0},  {-1,  2,  1},  {-1,  4,  2},  { 0, -4,  2},
    { 0, -2,  1},  { 0, -1,  0},  { 0,  0,  0},  { 0,  1,  0},
    { 0,  2,  1},  { 0,  4,  2},  { 1, -4,  2},  { 1, -2,  1},
    { 1, -1,  0},  { 1,  0,  0},  { 1,  1,  0},  { 1,  2,  1},
    { 1,  4,  2},  { 2, -3,  2},  { 2, -2,  1},  { 2, -1,  1},
    { 2,  0,  1},  { 2,  1,  1},  { 2,  2,  1},  { 2,  3,  2},
    { 3, -3,  2},  { 3, -2,  2},  { 3,  2,  2},  { 3,  3,  2},
    { 4, -2,  2},  { 4, -1,  2},  { 4,  0,  2},  { 4,  1,  2},
    { 4,  2,  2}
  }
};

const std::vector<std::vector<std::array<mke::dd::types::fpreal16, 3>>> NgCircLinClassifier::linconstrs_ = {
  {
    {0.613515937546589, 0.789682337637313, -1.484369171309642},
    {0.687517555726208, 0.054718102309593, -0.654103266011069} // -0.724103266011069
  },
  {
    {0.613515937546589, 0.789682337637313, -1.484369171309642},
    {0.687517555726208, 0.054718102309593, -0.624103266011069}
  }
};

json NgCircLinClassifier::param_schema_;

#ifdef MKEDD_RESERVED_API
json NgCircLinClassifier::reserved_param_schema_;
json NgCircLinClassifier::dummy_patch_;
#endif

// ============================================================================
// ============================================================================
// Initialization

void NgCircLinClassifier::initSchema(void)
{
  if (param_schema_.is_null())
    {
       param_schema_ = R"SCHEMA(
          {
            "type": "object",
            "properties": {
              "mask_idx": {
                "description": "Mask index",
                "type": "number",
                "minimum": 0,
                "maximum": 1,
                "multipleOf": 1,
                "default": 0
              },
              "constraints": {
                "description": "Constraints description. Met constraint implies NOT_A_DOT result",
                "type": "array",
                "minItems": 0,
                "maxItems": 20,
                "default": [],
                "items": [
                  {
                    "type": "object",
                    "properties": {
                      "type": {
                        "description": "Constraint type",
                        "type": "string",
                        "enum": [
                          "value",
                          "ratio"
                        ]
                      },
                      "index1": {
                        "description": "First descriptor index",
                        "type": "number",
                        "minimum": 0,
                        "maximum": 5,
                        "multipleOf": 1
                      },
                      "index2": {
                        "description": "Second descriptor index",
                        "type": "number",
                        "minimum": 0,
                        "maximum": 5,
                        "multipleOf": 1
                      },
                      "value": {
                        "description": "Value threshold",
                        "type": "number"
                      },
                      "value_gt": {
                        "description": "Value threshold inequiality sign. True for '>', false for '<='",
                        "type": "boolean"
                      },
                      "distance": {
                        "description": "Distance threshold",
                        "type": "number"
                      },
                      "distance_gt": {
                        "description": "Distance threshold inequiality sign. True for '>', false for '<='",
                        "type": "boolean"
                      },
                      "additionalProperties": false,
                      "required": [ "type", "index1", "index2", "value", "value_gt", "distance", "distance_gt" ]
                    }
                  }
                ]
              }
            }
          }
       )SCHEMA"_json;
 
#ifdef MKEDD_RESERVED_API
       reserved_param_schema_ = R"SCHEMA(
        {
          "type": "object",
            "properties" : {
              "store_dbg_data" : {
                "type" : "boolean",
                "default" : true
              }
            }
          }
       )SCHEMA"_json;
#endif
    }
}

// ============================================================================
// ============================================================================
// Classification

bool NgCircLinClassifier::classify(const uint8_t *data, const int x, const int y,
                                   const uint32_t uid, const int16_t *pt3d)
{
  int no_masks = no_masks_[mask_idx_];
  fpreal16 features[2 * no_masks] = {0};
  fpreal16 descs[2];

#ifdef MKEDD_RESERVED_API
  std::lock_guard<std::mutex> lock(patches_mutex_);

  json &patch = addPatch();

  descs[0] = 0;
  descs[1] = 0;
#endif

  int mask_width = mask_widths_[mask_idx_];

  // Are we within the image?
  if ((x - mask_width < 0) || (x + mask_width >= width_) ||
      (y - mask_width < 0) || (y + mask_width >= height_))
    {
#ifdef MKEDD_RESERVED_API
      fillPatchData(data, uid, x, y, 0, features, descs, patch);
#endif
      return false;
    }

  // Compute CircLin features and descriptors
  fpreal16 *maxs = features;
  fpreal16 *means = features + 3;

  const auto &pixel_list = pixel_lists_[mask_idx_];
  const auto &no_mask_pxs = no_masks_pxs_[mask_idx_];
  int no_pxs = pixel_list.size();

  for (int i = 0; i < no_pxs; i++)
    {
      const int &u = pixel_list[i][0];
      const int &v = pixel_list[i][1];
      const int &m = pixel_list[i][2];

      fpreal16 val = getValue(data, x + u, y + v);

      if (maxs[m] < val)
        maxs[m] = val;

      means[m] += val;
    }

  for (int i = 0; i < no_masks; i++)
    means[i] = means[i] / fpreal16(no_mask_pxs[i]);

  descs[0] = (maxs[1].getValue() == 0) ? means[0] : means[0] / maxs[1];
  descs[1] = (maxs[2].getValue() == 0) ? means[1] : means[1] / maxs[2];

  //descs[1] = fpreal16(2) * descs[1];

#ifdef MKEDD_RESERVED_API
  fillPatchData(data, uid, x, y, 15, features, descs, patch);
#endif

  // Apply linear constraints
  const auto &lcs = linconstrs_[mask_idx_];

  if (mask_idx_ == 0)
    {
      if (descs[1] < 0.75)
        return false;

#pragma unroll
      for (int i = 0; i < lcs.size(); i++)
        {
          fpreal16 confidence = lcs[i][0] * descs[0] + lcs[i][1] * descs[1] + lcs[i][2];

          if (confidence.getValue() < 0)
            return false;
        }
    }
  else if (mask_idx_ == 1)
    {
      if (descs[1] < 0.93)
        return false;

      if (descs[0] < 0.6)
        return false;
      /*
#pragma unroll
      for (int i = 0; i < lcs.size(); i++)
        {
          fpreal16 confidence = lcs[i][0] * descs[0] + lcs[i][1] * descs[1] + lcs[i][2];

          if (confidence.getValue() < 0)
            return false;
        }
    */
    }

 // Apply user supplied constraints
 for (const auto &constraint : constraints_)
   {
     if (constraint.type == 0)
       {
         fpreal16 value = features[constraint.idx1];

         if (( constraint.value_gt && (constraint.value <= value)) ||
             (!constraint.value_gt && (constraint.value > value)))
           {
             uint32_t dist2 = pt3d[0] * pt3d[0] +
                              pt3d[1] * pt3d[1] +
                              pt3d[2] * pt3d[2];

             if (( constraint.dist_gt && (constraint.dist <= dist2)) ||
                 (!constraint.dist_gt && (constraint.dist > dist2)))
               {
#ifdef MKEDD_RESERVED_API
                 patch["is_dot"] = false;
#endif
                 return false;
               }
           }
       }
   }

#ifdef MKEDD_RESERVED_API
 patch["is_dot"] = true;
#endif

 return true;
}

// ============================================================================
// ============================================================================
// Parameters

void NgCircLinClassifier::validateParams(const nlohmann::json &params)
{
  pmap_.validateParams(params);
}

// ============================================================================

void NgCircLinClassifier::setParams(const nlohmann::json &params)
{
  pmap_.setParams(params);

  if (params.find("mask_idx") != params.end())
    {
      mask_idx_ = pmap_.getParam("mask_idx").get<int>();
    }

  if (params.find("constraints") != params.end())
    {
      json constraints_json = pmap_.getParam("constraints");
      int no_constraints = constraints_json.size();
      constraints_.resize(no_constraints);

      for (int i = 0; i < no_constraints; i++)
        {
          const json &constraint = constraints_json[i];

          if (constraint["type"].get<std::string>() == "value")
            constraints_[i].type = 0;
          else if (constraint["type"].get<std::string>() == "ratio")
            constraints_[i].type = 1;

          constraints_[i].idx1 = constraint["index1"].get<double>();
          constraints_[i].idx2 = constraint["index2"].get<double>();

          constraints_[i].value = fpreal16(constraint["value"].get<double>());
          constraints_[i].value_gt = constraint["value_gt"].get<bool>();

          constraints_[i].dist = constraint["distance"].get<double>();
          constraints_[i].dist_gt = constraint["distance_gt"].get<bool>();
          constraints_[i].dist *= constraints_[i].dist;

        }
    }

#ifdef MKEDD_RESERVED_API
  if (params.find("store_dbg_data") != params.end())
    store_dbg_data_ = pmap_.getParam("store_dbg_data").get<bool>();
#endif
}

// ============================================================================

void NgCircLinClassifier::getParams(nlohmann::json &params) const
{
  params = pmap_.getParams();
}

// ============================================================================
// ============================================================================
// Debugging

#ifdef MKEDD_RESERVED_API

void NgCircLinClassifier::initDebugData(void)
{
  patches_.clear();
}

// ============================================================================

void NgCircLinClassifier::getDebugData(nlohmann::json &dbgdata)
{
  dbgdata["patches"].swap(patches_);
  patches_ = json::array();
}

// ============================================================================

nlohmann::json& NgCircLinClassifier::addPatch(void)
{ 
  if (store_dbg_data_)
    {
      json patch;
      patches_.push_back(patch);
      return patches_.back();
    }
  else
    {
      return dummy_patch_;
    }
}

// ============================================================================

void NgCircLinClassifier::fillPatchData(const uint8_t *data, const uint32_t uid,
                                        const int x, const int y, const int w,
                                        fpreal16 *features, fpreal16 *descs, json &patch)
{
  if (store_dbg_data_)
    {
      // Store debug information
      patch["uid"] = uid;
      patch["cx"] = x;
      patch["cy"] = y;
      patch["width"] = w;
      patch["is_dot"] = false;
      patch["constraint_type"] = -1;
      patch["desc"] = {double(descs[0]), double(descs[1])};

      patch["features"] = json::array();
      json &features_json = patch["features"];
      features_json[5] = nullptr;
      for (int i = 0; i < 6; i++)
        features_json[i] = double(features[i]);

      json patch_data = json::array();

      if (w != 0)
        {
          patch_data[14] = nullptr;

          for (int j = -7; j < 8; j++)
            {
              json &dataline = patch_data[j + 7];
              dataline = json::array();
              dataline[14] = nullptr;

              for (int i = -7; i < 8; i++)
                dataline[i + 7] = int(getValue(data, x + i, y + j));
            }
        }

      patch["data"].swap(patch_data);
    }
}

#endif

// ============================================================================

#endif // MKEDD_USE_NGCIRCLIN_CLASSIFIER
