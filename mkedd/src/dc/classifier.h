/*
 * classifier - Dot classifier
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifndef _CLASSIFIER_H_
#define _CLASSIFIER_H_

#include "json.hpp"
#include "data_types.h"

namespace mke {
namespace dd {
namespace dc {
  
/** \addtogroup mkedd
 *  @{
 */

class Classifier {
protected:
  int width_;
  int height_;
  int stride_;
  
  inline uint8_t getValue(const uint8_t *data, const int x, const int y) const
  {
    return data[y * stride_ + x];
  }
  
public:
  
  // Construction =============================================================
  
  Classifier() {}
  virtual ~Classifier() {}
  
  void setFrameParams(const int width, const int height, const int stride)
  {
    width_ = width;
    height_ = height;
    stride_ = stride;
  }
    
  // Virtual methods ==========================================================
    
  virtual void validateParams(const nlohmann::json &) = 0;
  virtual void setParams(const nlohmann::json &) = 0;
  virtual void getParams(nlohmann::json &) const = 0;

  virtual bool classify(const uint8_t *data, const int x, const int y,
                        const uint32_t uid, const int16_t *pt3d) = 0;

#ifdef MKEDD_RESERVED_API
  virtual void initDebugData(void) = 0;
  virtual void getDebugData(nlohmann::json &dbgdata) = 0;
#endif

};


} // dc
} // dd
} // mke

#endif // _CLASSIFIER_H_
