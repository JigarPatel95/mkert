/*
 * dc_circlinex - Dot classifier based on circular mask descriptor and linear classification
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifdef MKEDD_USE_EXCIRCLIN_CLASSIFIER

#include "types/fpreal.h"
#include "dc/circlinex.h"

#include <iostream>
#include <fstream>


using namespace mke::dd::dc;
using namespace mke::dd::types;

using json = nlohmann::json;


const int ExCircLinClassifier::no_mask_pxs_[3] = { 9, 20, 40};

const int ExCircLinClassifier::pixel_list_[][3] = {
  { -7,  -2,   2}, { -7,  -1,   2}, { -7,   0,   2}, { -7,   1,   2}, 
  { -7,   2,   2}, { -6,  -4,   2}, { -6,  -3,   2}, { -6,   3,   2}, 
  { -6,   4,   2}, { -5,  -5,   2}, { -5,   5,   2}, { -4,  -6,   2}, 
  { -4,  -1,   1}, { -4,   0,   1}, { -4,   1,   1}, { -4,   6,   2}, 
  { -3,  -6,   2}, { -3,  -2,   1}, { -3,   2,   1}, { -3,   6,   2}, 
  { -2,  -7,   2}, { -2,  -3,   1}, { -2,   3,   1}, { -2,   7,   2}, 
  { -1,  -7,   2}, { -1,  -4,   1}, { -1,  -1,   0}, { -1,   0,   0}, 
  { -1,   1,   0}, { -1,   4,   1}, { -1,   7,   2}, {  0,  -7,   2}, 
  {  0,  -4,   1}, {  0,  -1,   0}, {  0,   0,   0}, {  0,   1,   0}, 
  {  0,   4,   1}, {  0,   7,   2}, {  1,  -7,   2}, {  1,  -4,   1}, 
  {  1,  -1,   0}, {  1,   0,   0}, {  1,   1,   0}, {  1,   4,   1}, 
  {  1,   7,   2}, {  2,  -7,   2}, {  2,  -3,   1}, {  2,   3,   1}, 
  {  2,   7,   2}, {  3,  -6,   2}, {  3,  -2,   1}, {  3,   2,   1}, 
  {  3,   6,   2}, {  4,  -6,   2}, {  4,  -1,   1}, {  4,   0,   1}, 
  {  4,   1,   1}, {  4,   6,   2}, {  5,  -5,   2}, {  5,   5,   2}, 
  {  6,  -4,   2}, {  6,  -3,   2}, {  6,   3,   2}, {  6,   4,   2}, 
  {  7,  -2,   2}, {  7,  -1,   2}, {  7,   0,   2}, {  7,   1,   2}, 
  {  7,   2,   2}
};

json ExCircLinClassifier::param_schema_;

#ifdef MKEDD_RESERVED_API
json ExCircLinClassifier::reserved_param_schema_;
#endif

// ============================================================================

void ExCircLinClassifier::initSchema(void)
{
  if (param_schema_.is_null())
    {
       param_schema_ = R"SCHEMA(
        {
          "type":"object",
          "properties":{
            "constraints":{
              "description":"Constraints description. Met constraint implies NOT_A_DOT result",
              "type":"array",
              "minItems":0,
              "maxItems":20,
              "default":[],
              "items":[
                {
                  "type":"object",
                  "properties":{
                    "type":{
                      "description":"Constraint type",
                      "type": "string",
                      "enum":[
                        "value",
                        "ratio"
                      ]
                    },
                    "index1":{
                      "description":"First descriptor index",
                      "type":"number",
                      "minimum":0,
                      "maximum":5,
                      "multipleOf":1
                    },
                    "index2":{
                      "description":"Second descriptor index",
                      "type":"number",
                      "minimum":0,
                      "maximum":5,
                      "multipleOf":1
                    },
                    "value":{
                      "description":"Value threshold",
                      "type":"number"
                    },
                    "value_gt":{
                      "description":"Value threshold inequiality sign. True for '>', false for '<='",
                      "type":"boolean"
                    },
                    "distance":{
                      "description":"Distance threshold",
                      "type":"number"
                    },
                    "distance_gt":{
                      "description":"Distance threshold inequiality sign. True for '>', false for '<='",
                      "type":"boolean"
                    },
                    "additionalProperties": false,
                    "required": ["type", "index1", "index2", "value", "value_gt", "distance", "distance_gt"]
                  }
                }
              ]
            }
          }
        }
       )SCHEMA"_json;
 
#ifdef MKEDD_RESERVED_API
       reserved_param_schema_ = R"SCHEMA(
        {
          "type": "object",
            "properties" : {
              "store_dbg_data" : {
                "type" : "boolean",
                "default" : true
              }
            }
          }
       )SCHEMA"_json;
#endif
    }
}

// ============================================================================

bool ExCircLinClassifier::classify(const uint8_t *data, const int x, const int y, const uint32_t uid, const int16_t *pt3d)
{
  
 // static const fpreal16 n1 = -0.567737488599837;
 // static const fpreal16 n2 = -0.823209659830562;
 // static const fpreal16 b = -1.485612249505697;
 
  static const fpreal16 n1 = 0.613515937546589;
  static const fpreal16 n2 = 0.789682337637313;
  static const fpreal16 b = -1.484369171309642;
  
  if ((x - mask_width_ < 0) || (x + mask_width_ >= width_) ||
      (y - mask_width_ < 0) || (y + mask_width_ >= height_))
    {
      return false;
    }

  fpreal16 descs[2 * no_masks_] = {0};

  fpreal16 *maxs = descs;
  fpreal16 *means = descs + 3;

  //fpreal16 maxs[no_masks_] = {0};
  //fpreal16 means[no_masks_] = {0};
   
  for (int i = 0; i < no_pxs_; i++)
    {
      const int &u = pixel_list_[i][0];
      const int &v = pixel_list_[i][1];
      const int &m = pixel_list_[i][2];
     
      fpreal16 val = getValue(data, x + u, y + v);
     
      if (maxs[m] < val)
        maxs[m] = val;
     
      means[m] += val;     
    }
   
  for (int i = 0; i < no_masks_; i++)
    means[i] = means[i] / fpreal16(no_mask_pxs_[i]);
 
 fpreal16 desc1 = (maxs[1].getValue() == 0) ? fpreal16(0) : means[0] / maxs[1];
 fpreal16 desc2 = (maxs[2].getValue() == 0) ? fpreal16(0) : means[1] / maxs[2];
 //fpreal16 decision = b - (n1 * desc1 + n2 * desc2);
 fpreal16 confidence = n1 * desc1 + n2 * desc2 + b;
 bool is_dot = (confidence.getValue() > 0) ? true : false;

 #ifdef MKEDD_RESERVED_API
 if (store_dbg_data_)
   {
     json patch;

     patch["desc"] = {double(desc1), double(desc2)};
     patch["confidence"] = double(confidence);
     patch["is_dot"] = is_dot;
     patch["uid"] = uid;
     patch["cx"] = x;
     patch["cy"] = y;
     patch["width"] = 15;

     patch["constraint_type"] = -1;
     patch["descs"] = json::array();
     json &descs_json = patch["descs"];
     descs_json[5] = nullptr;
     for (int i = 0; i < 6; i++)
       descs_json[i] = double(descs[i]);

    // patch["constraint_type"] = 0;


     json patch_data = json::array();
     patch_data[14] = nullptr;

     for (int j = -7; j < 8; j++)
       {
         json &dataline = patch_data[j + 7];
         dataline = json::array();
         dataline[14] = nullptr;

         for (int i = -7; i < 8; i++)
           dataline[i + 7] = int(getValue(data, x + i, y + j));
        }

     patch["data"].swap(patch_data);

     {
       std::lock_guard<std::mutex> lock(patches_mutex);
       patches_.push_back(nullptr);
       patches_.back().swap(patch);
     }
   }
 #endif

 if (!is_dot)
   return is_dot;

 for (const auto &constraint : constraints_)
   {
     if (constraint.type == 0)
       {
         fpreal16 value = descs[constraint.idx1];
      //   std::cout << " CVAL = " << double(constraint.value) <<  " VAL = " << double(value) << " GT " << constraint.value_gt << " | ";

         if (( constraint.value_gt && (constraint.value <= value)) ||
             (!constraint.value_gt && (constraint.value > value)))
           {
             uint32_t dist2 = pt3d[0] * pt3d[0] + pt3d[1] * pt3d[1] + pt3d[2] * pt3d[2];
       //      std::cout  << " CDST = " << std::sqrt(double(constraint.dist)) << " DST = " << std::sqrt(double(dist2)) << constraint.dist_gt << " | ";

             if (( constraint.dist_gt && (constraint.dist <= dist2)) ||
                 (!constraint.dist_gt && (constraint.dist > dist2)))
               {
       //          std::cout << " REMOVED!" << std::endl;
                 is_dot = false;
                 #ifdef MKEDD_RESERVED_API
                 if (store_dbg_data_)
                   {
                     std::lock_guard<std::mutex> lock(patches_mutex);
                     json &patch = patches_.back();
                  //   patch["constraint_type"] = 0;
                  //   patch["constraint_val"] = double(value);
                   //  patch["constraint_dist2"] = dist2;
                     patch["is_dot"] = is_dot;
                     patch["constraint_type"] = 0;
                   }
#endif
                 return is_dot;
               }
           }
     //     std::cout << std::endl;
       }
   }


 return is_dot;
}

// ============================================================================
// Parameters

void ExCircLinClassifier::validateParams(const nlohmann::json &params)
{
  pmap_.validateParams(params);
}

void ExCircLinClassifier::setParams(const nlohmann::json &params)
{
  pmap_.setParams(params);

  if (params.find("constraints") != params.end())
    {
      json constraints_json = pmap_.getParam("constraints");
      int no_constraints = constraints_json.size();
      constraints_.resize(no_constraints);

      for (int i = 0; i < no_constraints; i++)
        {
          const json &constraint = constraints_json[i];

          if (constraint["type"].get<std::string>() == "value")
            constraints_[i].type = 0;
          else if (constraint["type"].get<std::string>() == "ratio")
            constraints_[i].type = 1;

          constraints_[i].idx1 = constraint["index1"].get<double>();
          constraints_[i].idx2 = constraint["index2"].get<double>();

          constraints_[i].value = fpreal16(constraint["value"].get<double>());
          constraints_[i].value_gt = constraint["value_gt"].get<bool>();

          constraints_[i].dist = constraint["distance"].get<double>();
          constraints_[i].dist_gt = constraint["distance_gt"].get<bool>();
          constraints_[i].dist *= constraints_[i].dist;

        }
    }

#ifdef MKEDD_RESERVED_API
  if (params.find("store_dbg_data") != params.end())
    store_dbg_data_ = pmap_.getParam("store_dbg_data").get<bool>();
#endif
}

void ExCircLinClassifier::getParams(nlohmann::json &params) const
{
  params = pmap_.getParams();
}

// ============================================================================
// Debugging

#ifdef MKEDD_RESERVED_API

void ExCircLinClassifier::initDebugData(void)
{
  patches_.clear();
}

// ============================================================================

void ExCircLinClassifier::getDebugData(nlohmann::json &dbgdata)
{
  dbgdata["patches"].swap(patches_);
  patches_ = json::array();
}

#endif

#endif // MKEDD_USE_EXCIRCLIN_CLASSIFIER
