/*
 * dcfactory
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifndef _DCFACTORY_H_
#define _DCFACTORY_H_

#include <stdexcept>
#include <map>

#include "dc/classifier.h"

namespace mke {
namespace dd {
namespace dc {
  
/** \addtogroup mkedd
 *  @{
 */

class Factory {
private:
  const static std::map<const std::string, Classifier*(*)(void)> create_map_;
  
public:

  static bool canCreate(const std::string &classifier_type_str)
  {
    const auto create_func = create_map_.find(classifier_type_str);
    
    if (create_func != create_map_.end())
      return true;
    else
      return false;
  }
    
  static Classifier* create(const std::string &classifier_type_str)
  {
    const auto create_func = create_map_.find(classifier_type_str);
    
    if (create_func != create_map_.end())
      return create_func->second();
    else
      throw std::runtime_error("Unknown post classifier type: " + classifier_type_str);
  }
  
};


/** @}*/

} /* namespace dc */
} /* namespace dd */
} /* namespace mke */


#endif // _DCFACTORY_H_
