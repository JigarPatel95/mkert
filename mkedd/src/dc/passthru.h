/*
 * dc_passthru - MkE pass through dot classifier
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */


#ifndef _DC_PASSTRHU_H_
#define _DC_PASSTRHU_H_

#include "classifier.h"

namespace mke {
namespace dd {
namespace dc {

/** \addtogroup mkedd
 *  @{
 */

class PassThruClassifier : public Classifier {  
public:
  
  // ==========================================================================
  // Static methods
  
  static const std::string getTypeString(void)
  {
    static const std::string typeString = "PassThru";
    return typeString;
  }
  
  static Classifier* create(void)
  {
    return new PassThruClassifier();
  }  
  
  // ==========================================================================
  // Construction 
  
  PassThruClassifier()
    {}
       
  // ==========================================================================
  // Processing  
  
  bool classify(const uint8_t *data, const int x, const int y,
                const uint32_t uid, const int16_t *pt3d)
  {
    return true;
  }
  
  // ==========================================================================
  // Parameters

  void validateParams(const nlohmann::json &params)
  {
  }

  void setParams(const nlohmann::json &params)
  {
  }

  void getParams(nlohmann::json &params) const
  {
      params = nlohmann::json::object();
  }

  // ==========================================================================
  // Debugging

#ifdef MKEDD_RESERVED_API
  void initDebugData(void)
  {
  }

  void getDebugData(nlohmann::json &dbgdata)
  {
  }
#endif
};


/** @}*/

} /* namespace dc */
} /* namespace dd */
} /* namespace mke */



#endif /* _DC_PASSTRHU_H_ */
