/*
 * dcfactory
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#include "dc/factory.h"

#include "dc/passthru.h"

#ifdef MKEDD_USE_CIRCLIN_CLASSIFIER
#include "dc/circlin.h"
#endif

#ifdef MKEDD_USE_EXCIRCLIN_CLASSIFIER
#include "dc/circlinex.h"
#endif

#ifdef MKEDD_USE_NGCIRCLIN_CLASSIFIER
#include "dc/circlinng.h"
#endif

using namespace mke::dd::dc;

const std::map<const std::string, Classifier*(*)(void)> Factory::create_map_ = {
#ifdef MKEDD_USE_CIRCLIN_CLASSIFIER
  {CircLinClassifier::getTypeString(), &CircLinClassifier::create},
#endif
#ifdef MKEDD_USE_EXCIRCLIN_CLASSIFIER
  {ExCircLinClassifier::getTypeString(), &ExCircLinClassifier::create},
#endif
#ifdef MKEDD_USE_NGCIRCLIN_CLASSIFIER
  {NgCircLinClassifier::getTypeString(), &NgCircLinClassifier::create},
#endif
  {PassThruClassifier::getTypeString(), &PassThruClassifier::create}
};
