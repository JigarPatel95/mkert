/*
 * Timers - utility classes to measure elapsed time
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#pragma once

#ifndef __nios2_arch__ 

#include <chrono>
#include <time.h>
#include <mutex>

#endif

/* -------------------------------------------------------------------------- */

namespace mke {
namespace dd {
namespace util {

/* -------------------------------------------------------------------------- */

#ifdef __nios2_arch__ 
// There is no chrono/mutex on Nios2 platform, we need completely dummy classes

template<typename R, typename P, bool dummy_timer = false>
class WallClockTimer 
{
public:
  inline void start()
    {}

  inline void stop()
    {}

  inline void reset()
    {}

  inline R elapsed() const
  {
    return R(0);
  }
};

template<bool dummy_timer = false>
class ThreadTimer {
public:
  inline void start()
    {}

  inline void stop()
    {}

  inline void reset()
    {}

  template<typename R, typename P>
  inline R elapsed() const
  {
    return R(0);
  }

  inline void addElapsed(ThreadTimer<dummy_timer> &t)
    {}
};

/* -------------------------------------------------------------------------- */
#else 

template<typename R, typename P, bool dummy_timer = false>
class WallClockTimer 
{
  std::chrono::time_point<std::chrono::high_resolution_clock> start_point_;
  std::chrono::duration<R, P> duration_;
  bool is_running_;

public:
  WallClockTimer()
  {
    duration_ = std::chrono::seconds{0};  
  }
  
  inline void reset()
  {
    if (dummy_timer)
      return;      

    duration_ = std::chrono::seconds{0};  

    if (is_running_)
      start();
  }

  inline void start()
  {
    if (dummy_timer)
      return;

    start_point_ = std::chrono::high_resolution_clock::now();
    is_running_ = true;
  }

  inline void stop()
  {
    if (dummy_timer || !is_running_)
      return;

    std::chrono::time_point<std::chrono::high_resolution_clock> stop_point =
      std::chrono::high_resolution_clock::now();

    duration_ += stop_point - start_point_;
    is_running_ = false;
  }
  
  inline R elapsed() const
  {
    if (dummy_timer)
      return R(0);
    
    std::chrono::duration<R, P> d;

    if (is_running_)  
      {
        d = duration_ + (std::chrono::high_resolution_clock::now() - start_point_); 
      }
    else
      {
        d = duration_;
      }
    
    return d.count();
  }

  inline void addElapsed(R &elapsed_time, std::mutex &mutex)
  {
    if (dummy_timer)
      return;

    R el = elapsed();

    {
      std::lock_guard<std::mutex> mlock(mutex);
      elapsed_time += el;  
    }
  }
  
  inline bool isRunning() const
  {
    return is_running_;
  }
};


/* -------------------------------------------------------------------------- */

template<bool dummy_timer = false>
class ThreadTimer {
private:

  std::mutex mutex_;
  struct timespec duration_;
  struct timespec start_point_;
  bool is_running_;

  inline void addTimeDifference(const struct timespec &start, const struct timespec &stop, struct timespec &d) const
  {
    if ((stop.tv_nsec - start.tv_nsec) < 0)
      {
        d.tv_sec += stop.tv_sec - start.tv_sec - 1;
        d.tv_nsec += stop.tv_nsec - start.tv_nsec + 1000000000;
      }
    else
      {
        d.tv_sec += stop.tv_sec - start.tv_sec;
        d.tv_nsec += stop.tv_nsec - start.tv_nsec;
      }
  }

  public:

  // ThreadTimer is non-copyable
  ThreadTimer<dummy_timer>(const ThreadTimer<dummy_timer>&) = delete;
  ThreadTimer<dummy_timer>(ThreadTimer<dummy_timer>&&) = default;
  ThreadTimer<dummy_timer>& operator=(const ThreadTimer<dummy_timer>&) = delete;
  ThreadTimer<dummy_timer>& operator=(ThreadTimer<dummy_timer> &&) = default;

  ThreadTimer()
  {
    duration_.tv_sec = 0;
    duration_.tv_nsec = 0;
    is_running_ = false;
  }

  inline void reset()
  {
    if (dummy_timer)
      return;

    duration_.tv_sec = 0;
    duration_.tv_nsec = 0;

    if (is_running_)
      start();
  }
  
  inline void start()
  {
    if (dummy_timer)
      return;

    clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start_point_);
    is_running_ = true;
  }

  inline void stop()
  {
     if (dummy_timer || !is_running_)
       return;

     struct timespec stop_point;
     clock_gettime(CLOCK_THREAD_CPUTIME_ID, &stop_point);

     addTimeDifference(start_point_, stop_point, duration_);
     
     is_running_ = false;
  }

  
  inline void elapsed(struct timespec &d) const
  {
    if (dummy_timer)
      return;
    
    if (is_running_)
      {
        struct timespec lap_point;
        clock_gettime(CLOCK_THREAD_CPUTIME_ID, &lap_point);

        d = duration_;
        addTimeDifference(start_point_, lap_point, d);      
      }
    else
      { 
        d = duration_;
      }    
  }
      
  template<typename R, typename P>
  inline R elapsed() const
  {
    if (dummy_timer)
      return R(0);

    std::chrono::duration<R, P> d;

     struct timespec t_duration;
     elapsed(t_duration);
     
     d = std::chrono::seconds{t_duration.tv_sec}
       + std::chrono::nanoseconds{t_duration.tv_nsec};

    return d.count();
  }

  inline void addElapsed(ThreadTimer<dummy_timer> &t, std::mutex &mutex)
  {
    if (dummy_timer)
      return;

    struct timespec t_duration;
    t.elapsed(t_duration);
    
    {
      std::lock_guard<std::mutex> mlock(mutex);
      duration_.tv_sec += t_duration.tv_sec;
      duration_.tv_nsec += t_duration.tv_nsec;
      
      if (duration_.tv_nsec > 1000000000)
        {
          duration_.tv_sec++;
          duration_.tv_nsec -= 1000000000;
        }
    }     
  }

  inline void addElapsed(ThreadTimer<dummy_timer> &t)
  {
    addElapsed(t, mutex_);
  }
    
  inline bool isRunning() const
  {
    return is_running_;
  }  
};

#endif

/* -------------------------------------------------------------------------- */

#ifdef MKEDD_RUNTIME_STATS
using StatsThreadTimer = ThreadTimer<false>;
template <typename R, typename P>
using StatsWallClockTimer = WallClockTimer<R, P, false>;
#else
using StatsThreadTimer = ThreadTimer<true>;
template <typename R, typename P>
using StatsWallClockTimer = WallClockTimer<R, P, true>;
#endif

/* -------------------------------------------------------------------------- */

} // namespace util
} // namespace dd
} // namespace mke

/* -------------------------------------------------------------------------- */


