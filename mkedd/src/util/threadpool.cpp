/* ThreadPool
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifdef MKEDD_MULTITHREADING

#include "util/threadpool.h"

using namespace mke::dd::util;

size_t ThreadPoolSharedBase::no_shared_instances_ = 0;

size_t ThreadPoolSharedBase::max_threads_ = std::thread::hardware_concurrency();

std::mutex ThreadPoolSharedBase::shared_mutex_;

// ==========================================================================

//template<>
std::vector<std::thread> ThreadPoolBase<true>::threads_;

//template<>
std::queue<ThreadPoolWorkUnit> ThreadPoolBase<true>::work_queue_;

//template<>
std::condition_variable ThreadPoolBase<true>::queue_avail_cond_;

//template<>
std::mutex ThreadPoolBase<true>::queue_mutex_;

#endif
