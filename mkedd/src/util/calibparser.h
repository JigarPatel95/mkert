/* CalibParser : Calibration data file parser
 *
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifndef _DD_CALIBPARSER_H_
#define _DD_CALIBPARSER_H_

#include "iosfwd"
#include "json.hpp"

#include "dd/detector.h"

namespace mke {
namespace dd {
namespace util {

/** \addtogroup mkedd
 *  @{
 */


class CalibParser {
  static void parseMkeDet01DdCollut(std::ifstream &ifs, nlohmann::json &dd,
                  mke::dd::dd::Detector::SectionType stype = mke::dd::dd::Detector::DD_SECTION_COLLUT);
  static void parseMkeDet01PpEmpty(std::ifstream &ifs, nlohmann::json &pp);
  static void parseMkeDet01(std::ifstream &ifs, nlohmann::json &mkedetfile);

public:
  static void parse(const char *file_name, nlohmann::json &calibdata);
};

/** @}*/

} /* namespace util */
} /* namespace dd */
} /* namespace mke */

#endif // _DD_CALIBPARSER_H_
