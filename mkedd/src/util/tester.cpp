/*
 * Tester
 *
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */


#ifdef MKEDD_RESERVED_API

#include <iostream>
#include <fstream>

// realpath (unix), _fullpath (win)
#include <limits.h>
#include <stdlib.h>

#include <regex>

#include "mkedd.h"
#include "json.hpp"
#include "util/info.h"
#include "util/tester.h"
#include "dd/tester.h"

#include "util/timers.h"
#include "util/tester.h"

#include "CImg.h"
namespace ci = cimg_library;

using namespace mke::dd::util;
using nlohmann::json;

// ============================================================================

void Tester::setPathRoot(std::string root)
{
  root_ = root;
}

// ============================================================================

void Tester::loadFromFile(const std::string &fpath, json &params, json &results, json &info)
{
  std::ifstream ifs(fpath.c_str());
  json tests_json = json::parse(ifs);

  if (tests_json.is_array())
    {
      params = tests_json;
      results = nullptr;
      info = nullptr;
    }
  else
    {
      params = tests_json["params"];
      results = tests_json["results"];
      info = tests_json["info"];
    }
}

// ============================================================================

void Tester::saveToFile(const std::string &fpath, const json &params,
                        const json &results, const json &info)
{
  json info_json;

  if (info.size())
    {
      info_json = info;
    }
  else
    {
      info_json = json::object();
      info_json["hostname"] = GetHostnameString();
      info_json["timestamp"] = GetTimestampString();
      info_json["system"] = GetSystemInfoString();
      info_json["username"] = GetUsernameString();
      info_json["version"] = GetVersionString();
      info_json["info"] = GetInfoString();
    }

  json tests_json;
  tests_json["params"] = params;
  tests_json["results"] = results;
  tests_json["info"] = info_json;

  std::ofstream ofs(fpath.c_str());
  ofs << tests_json.dump(2);
}

// ============================================================================

void Tester::processFromFile(const std::string &fpath, json &params, json &results)
{
  // TODO: Make the path handling save
  
  char buf[PATH_MAX + 1];
  char *res = NULL;

#if defined(_WIN32)
  res = _fullpath(buf, fpath.c_str(), _MAX_PATH);
#elif defined(__unix__)
  res = realpath(fpath.c_str(), buf);
#else
  throw std::runtime_error("[Tester::processFromFile] Canonical path not implemented");
#endif

  if (!res)
    throw std::runtime_error(std::string("Cannot recover canonical path: '") + fpath + "'");

  root_ = std::string(buf);

#if defined(_WIN32)
  root_ = root_.substr(0, root_.find_last_of("\\") + 1);
#elif defined(__unix__)
  root_ = root_.substr(0, root_.find_last_of("/") + 1);
#endif

  json info;
  loadFromFile(fpath, params, results, info);
  process(params, results);
}

// ============================================================================

void Tester::process(const json &params, json &results)
{
  if (!params.is_array())
    throw std::runtime_error("[Tester::process] params not an JSON array");

  size_t no_params = params.size();
  results = json::array();

  if (no_params == 0)
    return;

  results[no_params - 1] = nullptr;

  for (size_t i = 0; i < no_params; i++)
    {
      const auto &ps = params[i];
      auto &rs = results[i];

      if (ps.find("module") != ps.end())
        {
          std::string module = ps["module"].get<std::string>();

          if (module == "dd")
            mke::dd::dd::Tester::process(ps, rs);
          else
            throw std::runtime_error(std::string("[Tester::process] Unknown module name: ") +
                                     module);
        }
      else if (ps.find("runs") != ps.end())
        {
          processRuns(ps, rs);
        }
    }
}

// ============================================================================

void Tester::processRuns(const json &test, json &results)
{
  mke::dd::DepthSensor dsensor;
  std::string profile_str;

  std::cout << std::endl;
  std::cout << "=================================================================" << std::endl;
  std::cout << "Performing test: " << test["name"].get<std::string>() << std::endl;

  // Add calibration file name to a list
  std::string cal_fname = root_ + test["calib"].get<std::string>();
  std::cout << "  Setting calibration file: " << cal_fname << std::endl;
  dsensor.addCalibFile(cal_fname.c_str());

  // Set profile (triggers calibration file load)
  profile_str = test["profile"].dump(2);
  std::cout << "  Setting profile: " << profile_str << std::endl;
  dsensor.setProfile(profile_str.c_str());

  uint32_t no_detectors = dsensor.getNoDetectors();
  std::cout << "  Number of detectors: " << no_detectors << std::endl;
  for (uint32_t i = 0; i < no_detectors; i++)
    std::cout << "  Stride[" << i << "]: " << dsensor.getStride(i) << std::endl;

  // Load image data
  std::vector<ci::CImg<uint8_t>> imgs;
  std::vector<uint32_t> detidx;
  int no_images = 0;

  if ((test.find("images") != test.end()) &&
      (test.find("detidx") != test.end()))
    {
      no_images = test["images"].size();

      imgs.resize(no_images);
      detidx.resize(no_images);

      for (int i = 0; i < no_images; i++)
        {
          std::string im_fname = root_ + test["images"][i].get<std::string>();
          std::cout << "  Loading image data: " << im_fname << std::endl;

          auto &img = imgs[i];
          img.load(im_fname.c_str());
          std::cout << "  Image size: [" << img.width() << "x" <<
                       img.height() << "x" << img.depth() << "]" << std::endl;

          detidx[i] = test["detidx"][i];
        }
    }
  else
    {
      throw std::runtime_error("Cannot determine test type");
    }

  int runs = 1;
  if (test.find("runs") != test.end())
      runs = test["runs"].get<double>();

  std::cout << "  Running the detector " << runs << " times... " << std::endl;

  mke::dd::util::WallClockTimer<double, std::ratio<1>, false> timer;
  timer.start();

  for (int i = 0; i < runs; i++)
    {
      for (int j = 0; j < no_images; j++)
        dsensor.process(detidx[j], imgs[j].data());
    }

  timer.stop();
  double time = timer.elapsed();
  std::cout << "  Elapsed time: " << time << "s (" << ((runs * no_images) /time) << " Hz)" << std::endl;

  std::string stats = dsensor.getStats();
  std::cout << "  Sensor statistics: " << stats << std::endl;

  results["stats"] = json::parse(stats);

  // Save results of the last dsensor detector
  if (runs == 1)
    {
      dsensor.getResults(detidx[no_images - 1], results["results"]);
      dsensor.getDebugData(detidx[no_images - 1], results["dbgdata"]);
    }
}

#endif // MKEDD_RESERVED_API
