/* CalibParser : Calibration data file parser
 *
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#include <iostream>
#include <fstream>
#include <string>
#include <cstdint>

#include "types/fpreal.h"
#include "types/convert.h"
#include "util/calibparser.h"
#include "dd/detector.h"
#include "pp/postproc.h"

#include "dd/colpath.h"
#include "dd/pathels.h"

using namespace mke::dd::util;
using nlohmann::json;

// ============================================================================

void CalibParser::parse(const char *file_name, nlohmann::json &calibdata)
{
  calibdata["file_name"] = file_name;

  std::ifstream ifs(file_name, std::ifstream::binary);

  char magic[9] = "";
  ifs.read(magic, sizeof(magic) - 1);
  std::string magic_str(magic);

  // Magic value
  if (!magic_str.compare(0, 8, "MKEDET01"))
    {
      calibdata["file_type"] = "CALIB_FILE_MKEDET01";
      json &mkedetfile = calibdata["MkEDetFile"];
      parseMkeDet01(ifs, mkedetfile);
    }
  else
    {
      calibdata["file_type"] = "CALIB_FILE_UNKNOWN";
    }
}

// ============================================================================

void CalibParser::parseMkeDet01(std::ifstream &ifs, nlohmann::json &mkedetfile)
{
  // HeaderSection
  json header;

  header["magik"] = "MKEDET01";

  uint32_t no_detectors;
  ifs.read((char *) &no_detectors, sizeof(no_detectors));
  header["no_detectors"] = no_detectors;

  if (!no_detectors)
    throw std::runtime_error("Number of detectors must not equal 0");

  uint32_t dd_section_type;
  ifs.read((char *) &dd_section_type, sizeof(dd_section_type));
  header["dd_section_type"] = dd_section_type;

  uint32_t pp_section_type;
  ifs.read((char *) &pp_section_type, sizeof(pp_section_type));
  header["pp_section_type"] = pp_section_type;

  mkedetfile["header"].swap(header);

  // InfoSection
  json info;

  uint32_t section_num_bytes;
  ifs.read((char *) &section_num_bytes, sizeof(section_num_bytes));
  info["section_num_bytes"] = section_num_bytes;

  char info_data[section_num_bytes + 1];
  ifs.read(info_data, section_num_bytes);
  info_data[section_num_bytes] = '\0';

  try
  {
    info["info_json"] = json::parse(info_data);
  }
  catch (...)
  {}

  info["info_data"] = std::string(info_data);

  mkedetfile["info"].swap(info);

  // LaserPatternSection
  json lp;

  std::vector<uint32_t> laser_patterns;
  laser_patterns.resize(no_detectors);
  ifs.read((char *) laser_patterns.data(), sizeof(uint32_t) * no_detectors);
  lp["laser_patterns"] = laser_patterns;

  mkedetfile["lpattern"].swap(lp);

  // DetectorSection
  json detectors = json::array();
  detectors[no_detectors - 1] = nullptr;

  for (uint32_t i = 0; i < no_detectors; i++)
    {
      json dd;
      mke::dd::dd::Detector::SectionType stype = mke::dd::dd::Detector::SectionType(dd_section_type);

      if (stype == mke::dd::dd::Detector::DD_SECTION_COLLUT)
        {
          parseMkeDet01DdCollut(ifs, dd, stype);
          detectors[i].swap(dd);
        }
      else if (stype == mke::dd::dd::Detector::DD_SECTION_INTLUT)
        {
          parseMkeDet01DdCollut(ifs, dd, stype);
          detectors[i].swap(dd);
        }
      else
        {
          throw std::runtime_error("Unrecognized Detector::SectionType: "
                                   + std::to_string(dd_section_type));
        }
    }

  mkedetfile["detectors"] = detectors;

  // PostProcSection
  json postprocs = json::array();
  postprocs[no_detectors - 1] = nullptr;

  for (uint32_t i = 0; i < no_detectors; i++)
    {
      json pp;

      if (mke::dd::pp::PostProcessor::SectionType(pp_section_type)
            == mke::dd::pp::PostProcessor::PP_SECTION_EMPTY)
        {
          parseMkeDet01PpEmpty(ifs, pp);
          postprocs[i].swap(pp);
        }
      else
        {
          throw std::runtime_error("Unrecognized PostProcessor::SectionType: "
                                   + std::to_string(pp_section_type));
        }
    }

  mkedetfile["postprocs"] = postprocs;
}


// ============================================================================

void CalibParser::parseMkeDet01DdCollut(std::ifstream &ifs, nlohmann::json &dd,
                                        mke::dd::dd::Detector::SectionType stype)
{
  dd = json::object();

  // Section size
  uint32_t section_num_bytes;
  ifs.read((char *) &section_num_bytes, sizeof(section_num_bytes));
  dd["section_num_bytes"] = section_num_bytes;

  // 2D buffer info
  uint32_t image_width;
  ifs.read((char *) &image_width, sizeof(image_width));
  dd["image_width"] = image_width;

  uint32_t image_height;
  ifs.read((char *) &image_height, sizeof(image_height));
  dd["image_height"] = image_height;

  uint32_t image_stride;
  ifs.read((char *) &image_stride, sizeof(image_stride));
  dd["image_stride"] = image_stride;

  // 3D info
  uint32_t depth_min;
  ifs.read((char *) &depth_min, sizeof(depth_min));
  dd["depth_min"] = depth_min;

  uint32_t depth_max;
  ifs.read((char *) &depth_max, sizeof(depth_max));
  dd["depth_max"] = depth_max;

  uint32_t data3d_type = mke::dd::DD_DATA3D_MM;
  if (stype == mke::dd::dd::Detector::DD_SECTION_INTLUT)
    {
      ifs.read((char *) &data3d_type, sizeof(data3d_type));
      dd["data3d_type"] = data3d_type;
    }

  // Kernel widths
  uint32_t no_kernels;
  ifs.read((char *) &no_kernels, sizeof(no_kernels));
  dd["no_kernels"] = no_kernels;

  std::vector<uint16_t> kwidths;
  kwidths.resize(no_kernels);
  ifs.read((char *) kwidths.data(), sizeof(uint16_t) * no_kernels);
  dd["kwidths"] = kwidths;

  // Paths
  uint32_t no_paths;
  ifs.read((char *) &no_paths, sizeof(no_paths));
  dd["no_paths"] = no_paths;

  uint32_t no_gpels;
  ifs.read((char *) &no_gpels, sizeof(no_gpels));
  dd["no_pels"] = no_gpels;

  uint32_t max_pels;
  ifs.read((char *) &max_pels, sizeof(max_pels));
  dd["max_pels"] = max_pels;

  // CollatedPath
  json paths = json::array();
  paths[no_paths - 1] = nullptr;

  std::vector<uint8_t> mask;
  mke::dd::dd::CollatedLitePath clpath;
  mke::dd::dd::CollatedLitePath::PelsVector pels;

  for (uint32_t i = 0; i < no_paths; i++)
    {
      json path = json::object();

      // Info
      uint16_t no_pels;
      ifs.read((char *) &no_pels, sizeof(no_pels));
      path["no_pels"] = no_pels;

      uint32_t uid;
      ifs.read((char *) &uid, sizeof(uid));
      path["uid"] = uid;

      uint32_t lid;
      ifs.read((char *) &lid, sizeof(lid));
      path["lid"] = lid;

      uint32_t did;
      ifs.read((char *) &did, sizeof(did));
      path["did"] = did;

      uint16_t lmin;
      ifs.read((char *) &lmin, sizeof(lmin));
      path["lmin"] = lmin;

      uint16_t lmax;
      ifs.read((char *) &lmax, sizeof(lmax));
      path["lmax"] = lmax;

      // Mask
      uint32_t no_bytes = (no_pels + 7) >> 3;
      mask.resize(no_bytes);
      ifs.read((char *) mask.data(), sizeof(uint8_t) * no_bytes);
      clpath.setMask(std::move(mask), no_pels);
      const auto &bmask = clpath.getMask();

      json bitmask = json::array();
      bitmask[no_pels - 1] = nullptr;

      for (uint16_t j = 0; j < no_pels; j++)
        bitmask[j] = bmask[j];

      path["mask"].swap(bitmask);

      // Elements LUT
      PointDataLut ipel;
      pels.resize(no_pels);
      ifs.read((char *) pels.data(), sizeof(mke::dd::dd::PathElementLiteLut) * no_pels);
      clpath.setPathElementsLut(std::move(pels));

      json pels_json = json::array();
      pels_json[no_pels - 1] = nullptr;

      for (uint16_t j = 0; j < no_pels; j++)
        {
          clpath.setPathElement(ipel, j);
          json pel = json::array();

          pel[4] = mke::dd::types::Convert(ipel.z, mke::dd::Data3dType(data3d_type));
          pel[3] = mke::dd::types::Convert(ipel.y, mke::dd::Data3dType(data3d_type));
          pel[2] = mke::dd::types::Convert(ipel.x, mke::dd::Data3dType(data3d_type));

          pel[0] = double(mke::dd::types::fpreal16(ipel.u, true));
          pel[1] = double(mke::dd::types::fpreal16(ipel.v, true));

          pels_json[j].swap(pel);
        }

      path["pels_lut"].swap(pels_json);

      if (stype == mke::dd::dd::Detector::DD_SECTION_INTLUT)
        {
          uint8_t no_neighbors;;
          ifs.read((char *) &no_neighbors, sizeof(no_neighbors));
          path["no_neighbors"] = no_neighbors;

          json path_idx = json::array();

          for (uint8_t j = 0; j < no_neighbors; j++)
            {
              uint32_t pidx;
              ifs.read((char *) &pidx, sizeof(pidx));
              path_idx.push_back(pidx);
            }

          path["path_idx"].swap(path_idx);
        }

      // Set to paths
      paths[i].swap(path);
    }

  if (stype == mke::dd::dd::Detector::DD_SECTION_INTLUT)
    {
      // Levels
      uint32_t no_ilevels;
      ifs.read((char *) &no_ilevels, sizeof(no_ilevels));
      dd["no_ilevels"] = no_ilevels;

      json ilevels = json::array();

      for (uint32_t j = 0; j < no_ilevels; j++)
        {
          uint32_t pidx;
          ifs.read((char *) &pidx, sizeof(pidx));
          ilevels.push_back(pidx);
        }

      dd["ilevels"].swap(ilevels);

      uint32_t ids_per_iray;
      ifs.read((char *) &ids_per_iray, sizeof(ids_per_iray));
      dd["ids_per_iray"] = ids_per_iray;

      // Interpolated rays
      uint32_t no_irays;
      ifs.read((char *) &no_irays, sizeof(no_irays));
      dd["no_irays"] = no_irays;

      json irays = json::array();

      if (no_irays)
        irays[no_irays - 1] = nullptr;

      for (uint32_t j = 0; j < no_irays; j++)
        {
          json iray;
          json idx = json::array();

          uint32_t data[4];
          ifs.read((char *) &data, sizeof(data));

          iray["uid"] = data[0];

          idx[0] = data[1];
          idx[1] = data[2];
          idx[2] = data[3];

          iray["idx"].swap(idx);
          irays[j].swap(iray);
        }

      dd["irays"].swap(irays);
    }

  dd["paths"].swap(paths);
}

// ============================================================================

void CalibParser::parseMkeDet01PpEmpty(std::ifstream &ifs, nlohmann::json &pp)
{
  pp = json::object();

  uint32_t section_num_bytes;
  ifs.read((char *) &section_num_bytes, sizeof(section_num_bytes));
  pp["section_num_bytes"] = section_num_bytes;
}
