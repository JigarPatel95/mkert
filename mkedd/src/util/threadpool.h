/* ThreadPool
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */
 
#ifndef _THREAD_POOL_H_ 
#define _THREAD_POOL_H_ 

#include <mutex>
#include <condition_variable>
#include <thread>
#include <functional>
#include <exception>
#include <stdexcept>
#include <queue>

#include <pthread.h>


namespace mke {
namespace dd {
namespace util {

constexpr bool SHARED_THREADPOOL = true;
constexpr bool PRIVATE_THREADPOOL = false;
    
/** \addtogroup mkedd
 *  @{
 */

typedef std::function<bool(void)> work_functor_t;

// ==========================================================================

class ThreadPoolWorkUnit {
public:
  work_functor_t functor_;
  size_t *counter_;
  std::mutex *counter_mutex_;
  std::condition_variable *counter_cond_;
  std::mutex *queue_mutex_;
  std::exception_ptr *teptr_;
  
ThreadPoolWorkUnit(work_functor_t functor, size_t *counter, 
                   std::mutex *counter_mutex, std::condition_variable *counter_cond,
                   std::mutex *queue_mutex, std::exception_ptr *teptr)
  : functor_(functor), counter_(counter), 
    counter_mutex_(counter_mutex), counter_cond_(counter_cond),
    queue_mutex_(queue_mutex), teptr_(teptr)
    {}
    
 ThreadPoolWorkUnit()
   {}
};

// ==========================================================================

template<bool shared>
class ThreadPoolBase
{
};

// ==========================================================================

template<>
class ThreadPoolBase<false> {
protected:
  
  std::vector<std::thread> threads_;
  std::queue<ThreadPoolWorkUnit> work_queue_;
  std::condition_variable queue_avail_cond_;
  std::mutex queue_mutex_;
};

// ==========================================================================

template<>
class ThreadPoolBase<true> {
protected:
    
  static std::vector<std::thread> threads_;
  static std::queue<ThreadPoolWorkUnit> work_queue_;
  static std::condition_variable queue_avail_cond_;
  static std::mutex queue_mutex_;
};

// ==========================================================================

class ThreadPoolSharedBase {
protected:
    
  static std::mutex shared_mutex_;
  static size_t max_threads_;
  static size_t no_shared_instances_;
};


// ==========================================================================
// ==========================================================================

template<bool shared = false>    
class ThreadPool : public ThreadPoolBase<shared>, public ThreadPoolSharedBase {
private:  

  size_t counter_;
  std::condition_variable counter_cond_;
  std::mutex counter_mutex_;
  std::exception_ptr teptr_;

  using ThreadPoolBase<shared>::threads_;
  using ThreadPoolBase<shared>::work_queue_;
  using ThreadPoolBase<shared>::queue_avail_cond_;
  using ThreadPoolBase<shared>::queue_mutex_;
  
  void startThreads(const size_t no_threads)
  {    
    for (size_t i = 0; i < no_threads; i++)
      threads_.push_back(std::thread([this](){this->doWork();}));
    
    /*
    unsigned num_cpus = std::thread::hardware_concurrency();
    
    for (size_t i = 0; i < no_threads; i++)
    {
      cpu_set_t cpuset;
      CPU_ZERO(&cpuset);
      CPU_SET((i * 2) % num_cpus, &cpuset);
      pthread_setaffinity_np(threads_[i].native_handle(),
                             sizeof(cpu_set_t), &cpuset);
    }
    */
  }
  
  void stopThreads(void)
  {
    for (size_t i = 0; i < threads_.size(); i++)
      pushWorkUnit([](){return false;});

    for (size_t i = 0; i < threads_.size(); i++)
      threads_[i].join();
    
    threads_.clear();
  }
  
  void doWork(void)
  {
    for (;;)
      {
        ThreadPoolWorkUnit work_unit;
        popWorkUnit(work_unit);  
        
        bool rval = true;
        
        try 
          {
            rval = work_unit.functor_();
          }
        catch (...)
          {
            setException(std::current_exception(), work_unit.queue_mutex_, work_unit.teptr_);
          }
        
        if (rval)
          decCounter(work_unit.counter_, work_unit.counter_mutex_, work_unit.counter_cond_);
        else
          break;
      }
  }
  
  void pushWorkUnit(const work_functor_t &item)
  {  
    {
      std::lock_guard<std::mutex> mlock(queue_mutex_);
      work_queue_.push(ThreadPoolWorkUnit(item, &counter_, &counter_mutex_, &counter_cond_, &queue_mutex_, &teptr_));
    }

    queue_avail_cond_.notify_one();
  }
   
  void popWorkUnit(ThreadPoolWorkUnit &item)
  {
    std::unique_lock<std::mutex> mlock(queue_mutex_);
    while (work_queue_.empty())
      queue_avail_cond_.wait(mlock);

    item = work_queue_.front();
    work_queue_.pop();
  }
  
  inline void decCounter(size_t *counter, std::mutex *counter_mutex, std::condition_variable *counter_cond)
  {
    {
      std::lock_guard<std::mutex> mlock(*counter_mutex);
      (*counter)--;
    }

    (*counter_cond).notify_one();
  }


  inline void setException(std::exception_ptr eptr, std::mutex *queue_mutex, std::exception_ptr *weptr)
  {
    std::lock_guard<std::mutex> mlock(*queue_mutex);
    *weptr = eptr;
  }
  
  // ==========================================================================
  
public:
  ThreadPool()
  {
    if (shared)
      {
        std::lock_guard<std::mutex> mlock(shared_mutex_);
        counter_ = 0;
        teptr_ = nullptr;
        no_shared_instances_++;  
      }
    else
      {
        counter_ = 0;
        teptr_ = nullptr;
      }
  }
  
  ~ThreadPool()
  {
    if (shared)
      {
        std::lock_guard<std::mutex> mlock(shared_mutex_);
        no_shared_instances_--;
        
        if (!no_shared_instances_)
          stopThreads();
      }
    else
      {
        stopThreads();
      }
  }
  
  // ThreadPool is con-copyable

  ThreadPool(const ThreadPool&) = delete;
  ThreadPool(ThreadPool&&) = default;
  ThreadPool& operator=(const ThreadPool&) = delete;
  ThreadPool& operator=(ThreadPool &&) = default;

  // ==========================================================================
  
  inline void process(const work_functor_t &func)
  {
    if (!threads_.size())
      setNoThreads(max_threads_);

    incCounter();
    pushWorkUnit(func);
  }
  
  inline void wait(void)
  {
    std::unique_lock<std::mutex> mlock(counter_mutex_);
    while(counter_)
      counter_cond_.wait(mlock);    
  }
 
  inline void setCounter(const size_t counter)
  {
    std::lock_guard<std::mutex> mlock(counter_mutex_);
    counter_ = counter;
  }
  
  inline void decCounter(void)
  {
    decCounter(&counter_, &counter_mutex_, &counter_cond_);
  }
  
  inline void incCounter(void)
  {
    std::lock_guard<std::mutex> mlock(counter_mutex_);
    counter_++;
  }
  
  inline bool hasException(void)
  {
    std::lock_guard<std::mutex> mlock(queue_mutex_);
    return teptr_ != nullptr;
  }
  
  inline std::exception_ptr getException(void)
  {
    std::unique_lock<std::mutex> mlock(queue_mutex_);  
    std::exception_ptr eptr = teptr_;
    teptr_ = nullptr;
    return eptr;
  }
 
  inline void setException(std::exception_ptr eptr)
  {
    std::lock_guard<std::mutex> mlock(queue_mutex_);
    teptr_ = eptr;
  }
 
  inline void setNoThreads(const size_t no_threads)
  {
    size_t no_th = (no_threads > max_threads_) ? max_threads_ : no_threads;
    if (threads_.size() == no_th)
      return;
   
    stopThreads();
    startThreads(no_threads);
  }

  inline size_t getNoThreads(void)
  {
    return threads_.size();
  }
};
  
/** @}*/

} /* namespace util */
} /* namespace dd */
} /* namespace mke */


#endif // _THREAD_POOL_H_ 
