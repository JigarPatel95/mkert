/* 
 * ParameterMap
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#ifndef _PMAP_H_
#define _PMAP_H_

#include <string>
#include "json.hpp"

namespace mke {
namespace dd {
namespace util {


/** \addtogroup mkecc
 *  @{
 */

class ParameterMap {
  public:
      
  ParameterMap();
  ~ParameterMap();
  
  ParameterMap& operator=(const ParameterMap& pmap) = delete;
  ParameterMap& operator=(ParameterMap &&pmap) noexcept;  
  
  void setSchema(const nlohmann::json &schema);
  void addSchema(const nlohmann::json &schema);
  void setSchemaStr(const std::string &schema_str);
  void addSchemaStr(const std::string &schema_str);
  
  void getSchema(std::string &schema_str, const size_t indent = -1) const;
  void getSchema(nlohmann::json &schema) const;
  nlohmann::json getSchema() const;
  
  void getParam(const std::string &param_name, double &val) const;
  void getParam(const std::string &param_name, std::string &val) const;
  void getParam(const std::string &param_name, nlohmann::json &val) const;
  nlohmann::json getParam(const std::string &param_name) const;
  nlohmann::json getParams() const;
  
  void setParam(const std::string &param_name, const double &val);
  void setParam(const std::string &param_name, const nlohmann::json &val);
  void setParams(const nlohmann::json &vals);
  void validateParams(const nlohmann::json &vals);
  
private:
  class Impl;  // Forward declaration of the implementation class
  Impl *impl_; // PIMPL
};

/** @}*/

} // namespace util
} // namespace dd
} // namespace mke


#endif // _PMAP_H_
