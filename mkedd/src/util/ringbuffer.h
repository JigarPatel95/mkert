/*
 * RingBuffer
 *
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */


#ifndef _RINGBUFFER_H_
#define _RINGBUFFER_H_

#include <array>

namespace mke {
namespace dd {
namespace util {

/** \addtogroup mkedd
 *  @{
 */

template<class T, unsigned int N>
class RingBuffer {
private:
  unsigned int init_idx_;
  unsigned int size_;

  std::array<T, N> buffer_;

public:
  RingBuffer() : init_idx_(0), size_(0)
    {}

  // ==========================================================================

  inline unsigned int size(void)
  {
    return size_;
  }

  // ==========================================================================

  inline void clear(void)
  {
    init_idx_ = 0;
    size_ = 0;
  }

  // ==========================================================================

  inline T& operator[](const unsigned int idx)
  {
    unsigned int real_idx = (init_idx_ + idx) % N;
    return buffer_[real_idx];
  }

  // ==========================================================================

  inline T& front(void)
  {
    return buffer_[init_idx_];
  }

  // ==========================================================================

  inline T& back(void)
  {
    return (*this)[size_ - 1];
  }

  // ==========================================================================

  void push_back(const T &datum)
  {
    if (size_ < N)
      {
        buffer_[size_++] = datum;
      }
    else
      {
        (*this)[size_] = datum;
        init_idx_++;
      }
  }

};

/** @}*/

} /* namespace util */
} /* namespace dd */
} /* namespace mke */


#endif /* _RINGBUFFER_H_ */
