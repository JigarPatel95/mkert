#ifndef _DATALUT_H_
#define _DATALUT_H_

#include <stdint.h>
#include <fstream>
#include <cassert>
#include <stdexcept>

namespace mke {
namespace dd {
namespace util {
  
/** \addtogroup mkedd
 *  @{
 */  
    

/* Class DataLut translates an index of type uint32_t 
   to associated data as a pointer to a type T. */

template <typename T> class DataLut {
private:
  unsigned int no_dim_;
  unsigned int no_entries_;
  T            *data_;

  void alloc() {
    data_ = new T[no_dim_ * no_entries_];
  }

  
  void dealloc() {
    if (data_) 
      delete [] data_;
        
    data_ = NULL;
    no_dim_ = 0;
    no_entries_ = 0;
  }

public:
  DataLut() {
    no_dim_ = 0;
    no_entries_ = 0;
    data_ = NULL;
  }

  ~DataLut() {
    dealloc();
  }
  
  void init(unsigned int no_dim, unsigned int no_entries) {    
    dealloc();
    no_dim_ = no_dim;
    no_entries_ = no_entries;
    alloc();
  }
  
  void clear() {
    dealloc();
    no_dim_ = 0;
    no_entries_ = 0;
  }

  /* Get pointer to data associated with index idx.
     If idx == UINT32_MAX, return NULL pointer. This is a mechanism to
     inform the user that if idx == UINT32_MAX was received, there is no 
     data associated with this position (e.g. user asked for a detection
     associated with a particular path, but the detector could not detect 
     any dot.
  */
  const T* getData(const uint32_t & idx) const {
    if (idx < no_entries_)
      return data_ + idx * no_dim_;
    else if (idx == UINT32_MAX)
      return NULL;
    else
      throw std::runtime_error("Lookup index out of bounds");
  }

  void setData(const uint32_t & idx, const T * data) {
    for (unsigned int i = 0; i < no_dim_; i++)
      {
        data_[idx * no_dim_ + i] = data[i];
      }
  }
  
  unsigned int getNoEntries(void) const {
    return no_entries_;
  }

  unsigned int getNoDim(void) const {
    return no_dim_;
  }

// File I/O ===================================================================

  // Dump data associated with indices in array idxs in Matlab readable format
  // This function is for debugging purposes only
  void dumpData(const char *fname, const uint32_t *idxs, const unsigned int no_data) {
    if (!fname)
      throw std::logic_error("File name parameter cannot be empty string");

    std::ofstream bFile(fname);

    if (!bFile)
      throw std::runtime_error(std::string("Cannot open file for writing: ") + fname);

    for (int i = 0; i < no_data; i++)
      {
        const T * datum = getData(idxs[i]);
        if (datum)
          {
            for (int j = 0; j < no_dim_; j++)
              bFile << datum[j] << " ";
            bFile << std::endl;
          }
        else
          {
            for (int j = 0; j < no_dim_; j++)
              bFile << "NaN ";
            bFile << std::endl;
          }	
      }
    bFile.close();
  }
   
  /*
  void loadData(const char *fname) {
    if (!fname)
      throw std::logic_error("File name parameter cannot be empty string");

    std::ifstream bFile(fname);

    if (!bFile)
      throw std::runtime_error(std::string("Cannot open data file: ") + fname);

    char magic[9] = ""; 
      bFile.read(magic, sizeof(magic) - 1);

    std::string magic_str(magic);

    if (magic_str.compare(0, 5, "MKELT"))
      throw std::runtime_error(std::string("Not a MagikEye lookup table file: ") + fname);
  
    if (magic_str.compare(5, 2, "01"))
      throw std::runtime_error(std::string("Unrecognized MagikEye lookup table file version: ") + fname);
   
    if (!magic_str.compare(7, 1, "A"))
      loadDataAscii(bFile);
    else if (!magic_str.compare(7, 1, "B"))
      loadDataBinary(bFile);
    else
      throw std::runtime_error(std::string("Unrecognized MagikEye lookup table file version: ") + fname);

    bFile.close();
  }
  */
  
  void loadDataAscii(std::istream *bFile)
  {
    std::string linebuf;

    dealloc();

    getline(*bFile, linebuf); // read the rest of the line

    *bFile >> no_entries_;
    *bFile >> no_dim_;
    getline(*bFile, linebuf); // read the rest of the line

    alloc();

    int c = 0;
    for (int i = 0; i < no_entries_; i++)
      {
        for (int j = 0; j < no_dim_; j++)
          *bFile >> data_[c++];
        getline(*bFile, linebuf); // read the rest of the line
      }
  }

  void loadDataBinary(std::istream *bFile)
  {
    uint32_t bpe;

    dealloc();

    bFile->read((char *) &no_entries_, sizeof(no_entries_)); 
    bFile->read((char *) &no_dim_, sizeof(no_dim_)); 
    bFile->read((char *) &bpe, sizeof(bpe)); 

    if (bpe != sizeof(T))
      throw std::runtime_error(std::string("File containts incompatible data type: ")
                               + std::to_string(bpe) + "!=" + std::to_string(sizeof(T)));

    alloc();
    bFile->read((char *) data_, sizeof(T) * no_entries_ * no_dim_); 
  }
  
  void loadDataBinary(std::istream *bFile, unsigned int no_entries, unsigned int no_dim)
  {
    dealloc();

    no_entries_ = no_entries;
    no_dim_ = no_dim;

    alloc();
    bFile->read((char *) data_, sizeof(T) * no_entries_ * no_dim_); 
  }  
  
  void reserve(unsigned int no_entries, unsigned int no_dim)
  {
    dealloc();
    no_entries_ = no_entries;
    no_dim_ = no_dim;
    alloc();
  }
  
}; /* DataLut */

typedef DataLut<int16_t> DataLutInt16;
typedef DataLut<uint16_t> DataLutUInt16;
typedef DataLut<int32_t> DataLutInt32;
typedef DataLut<uint32_t> DataLutUInt32;

/** @}*/

} /* namespace util */
} /* namespace dd */
} /* namespace mke */

#endif /* _DATALUT_H_ */

