/*
 * history - history ring buffer
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */


#ifndef _HISTORY_H_
#define _HISTORY_H_

namespace mke {
namespace dd {
namespace util {

/** \addtogroup mkedd
 *  @{
 */

template<typename epoch_type, typename meta_type, unsigned int no_epochs> 
class History {
private:
  struct HistoryItem {
    meta_type meta_data;
    epoch_type epoch_data[no_epochs];
  };
  
  std::vector<HistoryItem> history_;
  int current_epoch_;
  
public:
  History() : current_epoch_(0)
    {}
  
  void resize(unsigned int no_items)
  {
    history_.resize(no_items);
  }
  
  unsigned int size(void)
  {
    return history_.size();
  }
  
  unsigned int getNoEpochs(void)
  {
    return no_epochs;
  }
  
  inline meta_type& info(const unsigned int item_idx)
  {
    return history_[item_idx].meta_data;
  }
  
  inline epoch_type& at(const unsigned int item_idx, const unsigned int epoch_idx = 0)
  {
    /*
    int idx = (current_epoch_ + epoch_idx) % no_epochs;
    // Handle negative idx: if (idx < 0) { idx = no_epochs + idx; }
    unsigned int sig = ~(((unsigned int) idx >> (sizeof(int) * 8 - 1)) - 1);
    idx = (sig & no_epochs) + idx;
*/
    assert(epoch_idx < no_epochs);
    unsigned int idx = (current_epoch_ + epoch_idx) % no_epochs;
    return history_[item_idx].epoch_data[idx];  
  }
  
  inline epoch_type& curr(const unsigned int item_idx)
  {
    return at(item_idx, 0);
  }

  inline epoch_type& prev(const unsigned int item_idx)
  {
    return at(item_idx, 1);
  }
  
  History& operator++(int)
  {
    //current_epoch_ = (current_epoch_ + 1) % no_epochs;  
    current_epoch_ = (current_epoch_ == 0) ? no_epochs - 1 : current_epoch_ - 1;
    return *this;
  }
  
};

/** @}*/

} /* namespace util */
} /* namespace dd */
} /* namespace mke */



#endif /* _HISTORY_H_ */
