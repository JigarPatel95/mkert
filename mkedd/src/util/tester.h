/*
 * Tester
 *
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */


#ifdef MKEDD_RESERVED_API

#ifndef _UTIL_TESTER_H_
#define _UTIL_TESTER_H_

#include <string>
#include "json.hpp"

namespace mke {
namespace dd {
namespace util {

/** \addtogroup mkedd
 *  @{
 */

class Tester {
private:
  std::string root_;

public:
  void setPathRoot(std::string root);
  void processFromFile(const std::string &fpath, nlohmann::json &params, nlohmann::json &results);
  void loadFromFile(const std::string &fpath, nlohmann::json &params, nlohmann::json &results, nlohmann::json &info);
  void saveToFile(const std::string &fpath, const nlohmann::json &params, const nlohmann::json &results, const nlohmann::json &info = nullptr);

  void process(const nlohmann::json &params, nlohmann::json &results);
  void processRuns(const nlohmann::json &test, nlohmann::json &results);
};

/** @}*/

} // namespace util
} // namespace cc
} // namespace mke

#endif // _UTIL_TESTER_H_

#endif // MKEDD_RESERVED_API
