/* 
 * ParameterMap
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#include <iostream>
#include <json-schema.hpp>

#include "util/pmap.h"

// ============================================================================
// ============================================================================
// ParameterMap::Impl

using namespace mke::dd::util;
using json = nlohmann::json;
using nlohmann::json_schema::json_validator;

class  ParameterMap::Impl {
private:
  json_validator validator_;
  json params_schema_; 
  json params_;    
  
public:
  Impl() : params_schema_(nullptr), 
           params_(nullptr)           
    {
      static const json pschema = R"(
        {
          "type": "object",
          "properties" : {},
          "additionalProperties": false
        })"_json;
    
      params_schema_ = pschema;
      validator_.set_root_schema(params_schema_);
    } 
    
  ~Impl()
    {}

  // ==========================================================================

  inline void setSchema(const nlohmann::json &schema)
  {
    // Parse parameter JSON schema
    params_schema_ = schema.is_string() ? json::parse(schema.dump()) : schema;
    validator_ = json_validator();
    validator_.set_root_schema(params_schema_);
    
    // Read out default parameter values
    params_.clear();
    
    for (json::iterator it = params_schema_.at("properties").begin(); 
         it != params_schema_.at("properties").end(); 
         ++it)
      params_.emplace(it.key(), it.value().at("default"));
  }
  
  // ==========================================================================

  inline void addSchema(const nlohmann::json &schema)
  {
      json param_schema = params_schema_;
      
      for (json::const_iterator it = schema.at("properties").cbegin(); 
           it != schema.at("properties").cend(); ++it)
         param_schema.at("properties").emplace(it.key(), it.value());   
        
    setSchema(param_schema);
  }
  
  // ==========================================================================

  inline void setSchemaStr(const std::string &schema_str)
  {
    setSchema(json::parse(schema_str));
  }
  
  // ==========================================================================

  inline void addSchemaStr(const std::string &schema_str)
  {
    addSchema(json::parse(schema_str));
  }  
  
  // ==========================================================================
  
  inline void getSchema(std::string &schema, const size_t indent) const
  {
    schema = params_schema_.dump(indent);
  }
  
  // ==========================================================================
  
  inline void getSchema(json &schema) const
  {
    schema = params_schema_;  
  }  

  // ==========================================================================

  json getSchema()
  {
    return params_schema_;  
  }    
  
  // ==========================================================================
  // ==========================================================================

  inline void getParam(const std::string &param_name, double &val)
  {
    val = params_.at(param_name).get<double>();    
  }
  
  // ==========================================================================
  
  inline void getParam(const std::string &param_name, std::string &val)
  {
    val = params_.at(param_name).get<std::string>();    
  }
  
  // ==========================================================================
  
  inline void getParam(const std::string &param_name, json &val)
  {
    val = params_.at(param_name);    
  }  
  
  // ==========================================================================
  
  inline json getParam(const std::string &param_name)
  {
    return params_.at(param_name);    
  }  
  
  // ==========================================================================
  
  inline json getParams()
  {
    return params_;    
  }    
  
  // ==========================================================================
  // ==========================================================================
  
  inline void setParam(const std::string &param_name, const json &val)
  {
    json param;
    param.emplace(param_name, val);    
    validator_.validate(param);
    
    params_[param_name] = val;
  }    
  
  // ==========================================================================
  
  inline void setParams(const json &params)
  {
    if (params.is_null())
      return;

    validator_.validate(params);

    for (auto it = params.begin(); it != params.end(); ++it)    
      params_[it.key()] = it.value();
  }     
  
  // ==========================================================================
  
  inline void validateParams(const json &vals)
  {
    validator_.validate(vals);
  }   
  
};


// ============================================================================
// ============================================================================
// ParameterMap

// Create an implementation object in ctor
ParameterMap::ParameterMap()
: impl_(new Impl())
{}

ParameterMap::~ParameterMap()
{
  if (impl_ != nullptr)  
    delete impl_;
}

// ============================================================================

ParameterMap& ParameterMap::operator=(ParameterMap &&pmap) noexcept
{
  if (this != &pmap)
    std::swap(impl_, pmap.impl_);
   
  return *this;    
}

// ============================================================================

void ParameterMap::setSchema(const nlohmann::json &schema)
{
  impl_->setSchema(schema);
}


// ============================================================================

void ParameterMap::addSchema(const nlohmann::json &schema)
{
  impl_->addSchema(schema);
}

// ============================================================================

void ParameterMap::setSchemaStr(const std::string &schema_str)
{
  impl_->setSchemaStr(schema_str);
}


// ============================================================================

void ParameterMap::addSchemaStr(const std::string &schema_str)
{
  impl_->addSchemaStr(schema_str);
}


// ============================================================================

void ParameterMap::getSchema(std::string &schema_str, const size_t indent) const 
{
  impl_->getSchema(schema_str, indent);
}

// ============================================================================

void ParameterMap::getSchema(json &schema) const 
{
  impl_->getSchema(schema);
}

// ============================================================================

json ParameterMap::getSchema() const 
{
  return impl_->getSchema();
}

// ============================================================================
// ============================================================================

void ParameterMap::getParam(const std::string &param_name, double &val) const
{
  impl_->getParam(param_name, val);
}

// ============================================================================

void ParameterMap::getParam(const std::string &param_name, std::string &val) const
{
  impl_->getParam(param_name, val);
}

// ============================================================================

void ParameterMap::getParam(const std::string &param_name, json &val) const
{
  impl_->getParam(param_name, val);
}

// ============================================================================

json ParameterMap::getParam(const std::string &param_name) const
{
  return impl_->getParam(param_name);
}

// ============================================================================

json ParameterMap::getParams() const
{
  return impl_->getParams();
}

// ============================================================================
// ============================================================================

void ParameterMap::setParam(const std::string &param_name, const double &val)
{
  return impl_->setParam(param_name, json(val));
}

// ============================================================================

void ParameterMap::setParam(const std::string &param_name, const json &val)
{
  impl_->setParam(param_name, val);
}

// ============================================================================

void ParameterMap::setParams(const json &vals)
{
  impl_->setParams(vals);
}

// ============================================================================

void ParameterMap::validateParams(const json &vals)
{
  impl_->validateParams(vals);
}

