/* Info - utility functions for various info strings
 *
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */


#include "util/info.h"

#include <iostream>
#include <iomanip>
#include <ctime>
#include <sstream>

#ifdef __unix__
#include <unistd.h>
#include <sys/types.h>
#include <sys/utsname.h>
#include <pwd.h>
#endif

#ifdef MKEDD_USE_MKECC
#include "mkecc/util/info.h"
#endif

using namespace mke::dd::util;

// ============================================================================
// ============================================================================


std::string mke::dd::util::GetTimestampString(void)
{
  auto t = std::time(nullptr);
  auto tm = *std::localtime(&t);

  std::ostringstream oss;
  oss << std::put_time(&tm, "%d-%m-%Y %H:%M:%S");
  return oss.str();
}

// ============================================================================

std::string mke::dd::util::GetSystemInfoString(void)
{
#ifdef __unix__
  struct utsname buffer;

  errno = 0;
  if (uname(&buffer) != 0)
    return "N/A";

  std::string info = std::string("") + buffer.sysname + " " + buffer.nodename + " " +
                     buffer.release + " " + buffer.version + " " + buffer.machine;

  return info;
#else
  return "N/A";
#endif
}

// ============================================================================

std::string mke::dd::util::GetHostnameString(void)
{
#ifdef __unix__
  struct utsname buffer;

  errno = 0;
  if (uname(&buffer) != 0)
    return "N/A";

  return buffer.nodename;
#else
  return "N/A";
#endif
}

// ============================================================================

std::string mke::dd::util::GetUsernameString(void)
{
#ifdef __unix__
   uid_t uid = geteuid();
   struct passwd *pw = getpwuid(uid);

   if (!pw)
     return "N/A";

   return pw->pw_name;
#else
  return "N/A";
#endif
}

// ============================================================================

std::string mke::dd::util::GetVersionString(void)
{
  return MKEDD_VERSION;
}

// ============================================================================

std::string mke::dd::util::GetInfoString(void)
{
//  return "libmkedd " MKECC_GIT_INFO ", compiled on " MKECC_TIMESTAMP_STR;
  std::string info = std::string("libmkedd ") + MKEDD_VERSION;

#ifdef MKEDD_USE_MKECC
  info += " (" + mke::cc::util::GetInfoString() + ")";
#endif

  return info;
}


