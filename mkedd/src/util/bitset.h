/*
 * BitSet - class to represent a set of bits
 *          similar to vector<bool>, boost::dynamic_bitset
 * Copyright (c) 2018, Magik-Eye s.r.o., Prague
 * author: Jan Heller
 *
 * https://aticleworld.com/how-to-set-clear-and-toggle-a-single-bit-in-cc/
 *
 */

#ifndef _BITSET_H_
#define _BITSET_H_

#include <cstdint>
#include <vector>
#include <cstring>

namespace mke {
namespace dd {
namespace util {

/** \addtogroup mkedd
 *  @{
 */

// ==============================================================================


class BitSet {
private:
  uint32_t no_elements_;
  std::vector<uint8_t> blocks_;

public:
  BitSet()
    {
      no_elements_ = 0;
    }

  ~BitSet()
    {}

  // ==========================================================================

  inline void resize(uint32_t no_elements)
  {
    blocks_.resize((no_elements + 7) >> 3);
    no_elements_ = no_elements;
  }

  // ==========================================================================

  inline uint32_t size(void)
  {
    return no_elements_;
  }

  // ==========================================================================

  inline uint32_t getNoElements(void)
  {
    return no_elements_;
  }

  // ==========================================================================

  inline uint32_t getNoBlocks(void)
  {
    return blocks_.size();
  }

  // ==========================================================================

  inline const uint8_t* getBlocks(void)
  {
    return blocks_.data();
  }

  // ==========================================================================

  inline bool operator[](uint32_t pos) const
  {
    uint32_t bidx = pos >> 3;
    uint32_t bpos = pos & 0x0007;

    return blocks_[bidx] & (1 << bpos);
  }

  // ==========================================================================

  inline void set(uint32_t pos, bool bit)
  {
    uint32_t bidx = pos >> 3;
    uint32_t bpos = pos & 0x0007;

    if (bit)
      blocks_[bidx] |= ~(1 << bpos);
    else
      blocks_[bidx] &= (1 << bpos);
  }

  // ==========================================================================

  inline void set()
  {
    for (uint32_t i = 0; i < blocks_.size(); i++)
      blocks_[i] = 0xFF;
  }

  // ==========================================================================

  inline void set(std::vector<uint8_t> &&blocks, uint32_t no_elements)
  {
    resize(no_elements);
    no_elements_ = no_elements;
    blocks_.clear();
    blocks_.swap(blocks);
  }

  // ==========================================================================

  inline void clear(void)
  {
    no_elements_ = 0;
    blocks_.clear();
  }

};

// ==============================================================================

/** @}*/

} /* namespace util */
} /* namespace dd */
} /* namespace mke */


#endif // _THREAD_POOL_H_
