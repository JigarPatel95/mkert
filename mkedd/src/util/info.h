/* Info - utility functions for various info strings
 *
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifndef _MKEDD_INFO_H_
#define _MKEDD_INFO_H_

#include <string>

namespace mke {
namespace dd {
namespace util {

/** \addtogroup mkedd
 *  @{
 */

std::string GetTimestampString(void);

std::string GetSystemInfoString(void);

std::string GetHostnameString(void);

std::string GetUsernameString(void);

std::string GetVersionString(void);

std::string GetInfoString(void);

/** @}*/

} // namespace util
} // namespace dd
} // namespace mke

#endif // _MKEDD_INFO_H_
