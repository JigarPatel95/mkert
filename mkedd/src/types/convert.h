/*
 * convert: Data type conversion utilities
 *
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */


#ifndef _CONVERT_TYPES_H_
#define _CONVERT_TYPES_H_

#include <vector>
#include <cmath>

#include "data_types.h"
#include "types/fpreal.h"

namespace mke {
namespace dd {
namespace types {

/** \addtogroup mkedd
 *  @{
 */

inline fpreal16 ConvertToFpreal16(int16_t val, mke::dd::Data3dType type)
{
  if (type == mke::dd::DD_DATA3D_MM)
    return fpreal16(int(val));
  else if (type == mke::dd::DD_DATA3D_MM2)
    return fpreal16(uint32_t(val) << 14, true);
  else if (type == mke::dd::DD_DATA3D_MM4)
    return  fpreal16(uint32_t(val) << 13, true);
  else if (type == mke::dd::DD_DATA3D_MM8)
    return  fpreal16(uint32_t(val) << 12, true);
  else if (type == mke::dd::DD_DATA3D_MM16)
    return  fpreal16(uint32_t(val) << 11, true);
  else
    throw std::runtime_error("[MkEDD] Convert: Cannot covert Data3dType to fpreal16");
}

// ============================================================================

inline int16_t ConvertToInt16(double val, mke::dd::Data3dType type)
{
  if (type == mke::dd::DD_DATA3D_MM)
    return int16_t(std::round(val));
  else if (type == mke::dd::DD_DATA3D_MM2)
    return types::fhreal1r(val).getValue();
  else if (type == mke::dd::DD_DATA3D_MM4)
    return types::fhreal2r(val).getValue();
  else if (type == mke::dd::DD_DATA3D_MM8)
    return types::fhreal3r(val).getValue();
  else if (type == mke::dd::DD_DATA3D_MM16)
    return types::fhreal4r(val).getValue();
  else
    throw std::runtime_error("[MkEDD] Convert: Cannot covert Data3dType to fpreal16");
}

// ============================================================================

inline mke::dd::Data3dType ConvertToData3dType(const std::string &type_str)
{
  if (type_str == "DD_DATA3D_MM")
    return DD_DATA3D_MM;
  else if (type_str == "DD_DATA3D_MM2")
    return DD_DATA3D_MM2;
  else if (type_str == "DD_DATA3D_MM4")
    return DD_DATA3D_MM4;
  else if (type_str == "DD_DATA3D_MM8")
    return DD_DATA3D_MM8;
  else if (type_str == "DD_DATA3D_MM16")
    return DD_DATA3D_MM16;
  else
    throw std::runtime_error("[MkEDD] Convert: Cannot covert '" + type_str + "' to Data3dType");
}

// ============================================================================

inline double Convert(int16_t val, mke::dd::Data3dType type)
{
  if (type == mke::dd::DD_DATA3D_MM)
    return double(val);
  else if (type == mke::dd::DD_DATA3D_MM2)
    return double(types::fhreal1r(val, true));
  else if (type == mke::dd::DD_DATA3D_MM4)
    return double(types::fhreal2r(val, true));
  else if (type == mke::dd::DD_DATA3D_MM8)
    return double(types::fhreal3r(val, true));
  else if (type == mke::dd::DD_DATA3D_MM16)
    return double(types::fhreal4r(val, true));
  else
    throw std::runtime_error("[MkEDD] Convert: Cannot covert Data3dType to double");
}

// ============================================================================

template <typename Container>
inline std::vector<double> Convert(Container &c, mke::dd::Data3dType type)
{
  std::vector<double> res;
  res.resize(c.size());

  size_t i = 0;
  for (auto it = c.begin(); it != c.end(); it++)
    res[i++] = Convert(*it, type);

  return res;
}

/** @}*/

} /* namespace types */
} /* namespace dd */
} /* namespace mke */

#endif // _CONVERT_TYPES_H_
