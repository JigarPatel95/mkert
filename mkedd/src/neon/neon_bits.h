/*
 * neon bits - simple layer over NEON intrinsics
 * 
 * https://github.com/thenifty/neon-guide
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#pragma once 

#include <iostream>
#include <arm_neon.h>

namespace mke {
namespace dd {
namespace neon {


inline std::ostream& operator<<(std::ostream& stream, const int32x4_t& v) 
{
  return stream << "int32x4_t[" << v[0] << ", " << v[1] << ", " << v[2] << ", " << v[3] << "]";
}

inline std::ostream& operator<<(std::ostream& stream, const uint32x4_t& v)
{
  return stream << "uint32x4_t[" << v[0] << ", " << v[1] << ", " << v[2] << ", " << v[3] << "]";
}

inline static void print4(uint32x4_t v)
{
  uint32_t *p = (uint32_t *) &v;
  std::cout << "uint32x4_t[" << p[0] << ", " << p[1] << ", " << p[2] << ", " << p[3] << "]";
}

inline static void print4fpr16(uint32x4_t v)
{
  uint32_t *p = (uint32_t *) &v;
  std::cout << "[" << double(p[0]) / 0xFFFF <<
              ", " << double(p[1]) / 0xFFFF <<
              ", " << double(p[2]) / 0xFFFF <<
              ", " << double(p[3]) / 0xFFFF << "]";
}

inline void store(int32_t *ptr, const int32x4_t& v)
{
  vst1q_s32(ptr, v);
}

inline int32x4_t load4(const int32_t *ptr)
{
  return vld1q_s32(ptr);
}

inline int32x4_t load_from_int16x4(const void *ptr)
{
  const int16x4_t &vs = *(reinterpret_cast<const int16x4_t *>(ptr));
  return vmovl_s16(vs);
}

inline uint32x4_t load_from_uint16x4(const void *ptr)
{
  const uint16x4_t &vs = *(reinterpret_cast<const uint16x4_t *>(ptr));
  return vmovl_u16(vs);
}

inline uint32x4_t load4u(const uint32_t *ptr)
{
  return vld1q_u32(ptr);
}

inline int32x4_t gather(const int32_t *ptr, int32x4_t ofs)
{
  int32x4_t vs;

  vs[0] = ptr[ofs[0]];
  vs[1] = ptr[ofs[1]];
  vs[2] = ptr[ofs[2]];
  vs[3] = ptr[ofs[3]];
  return vs;
}

inline uint32x4_t gather(const uint8_t *ptr, int32x4_t ofs)
{
  uint32x4_t vs;

  vs[0] = ptr[ofs[0]];
  vs[1] = ptr[ofs[1]];
  vs[2] = ptr[ofs[2]];
  vs[3] = ptr[ofs[3]];
  return vs;
}

template <typename T4, typename T1, typename T2, typename T3>
inline static void gather(const T1 * const ptr, const T2 off, T3 &vals)
{
  vals[0] = (*((T4 *)(ptr + off[0])));
  vals[1] = (*((T4 *)(ptr + off[1])));
  vals[2] = (*((T4 *)(ptr + off[2])));
  vals[3] = (*((T4 *)(ptr + off[3])));
}

// =============================================================================

inline int32x4_t keep_if_mask(const int32x4_t &v, const uint32x4_t &mask)
{
  return v & reinterpret_cast<int32x4_t>(mask); 
}

inline uint32x4_t get_gt_mask(const int32x4_t &v1, const int32x4_t &v2)
{
  return vcgtq_s32(v1, v2);
}

inline uint32x4_t get_ge_mask(const int32x4_t &v1, const int32x4_t &v2)
{
  return vcgeq_s32(v1, v2);
}

inline int32x4_t keep_if_gt(const int32x4_t &v, const int32x4_t &o1, const int32x4_t &o2)
{
  uint32x4_t mask = get_gt_mask(o1, o2);
  return keep_if_mask(v, mask);
}


inline uint32x4_t get_mask(const uint32_t &m1, const uint32_t &m2, const uint32_t &m3, const uint32_t &m4)
{
  uint32x4_t mask;  
  mask = vsetq_lane_u32(m1, mask, 0);
  mask = vsetq_lane_u32(m2, mask, 1);
  mask = vsetq_lane_u32(m3, mask, 2);
  mask = vsetq_lane_u32(m4, mask, 3);

  return mask;  
}

inline int32x4_t keep_max(const int32x4_t &v1, const int32x4_t &v2)
{
  return vmaxq_s32(v1, v2);
}

inline int32x4_t keep_min(const int32x4_t &v1, const int32x4_t &v2)
{
  return vminq_s32(v1, v2);
}

inline int32x4_t clip_value(const int32x4_t &v1, const int32x4_t &lower_bound, const int32x4_t &upper_bound)
{
  return keep_min(keep_max(v1, lower_bound), upper_bound);
}

inline int32x4_t reverse(const int32x4_t &v)
{
 return vrev64q_s32(vextq_s32(v, v, 2));
}

// =============================================================================

static inline int32x4x4_t transpose4x4(const int32x4x4_t rows) {
  uint64x2x2_t row01, row23;

  row01.val[0] = vreinterpretq_u64_s32(rows.val[0]);
  row01.val[1] = vreinterpretq_u64_s32(rows.val[1]);
  row23.val[0] = vreinterpretq_u64_s32(rows.val[2]);
  row23.val[1] = vreinterpretq_u64_s32(rows.val[3]);

  const uint64x1_t row0h = vget_high_u64(row01.val[0]);
  const uint64x1_t row2l = vget_low_u64(row23.val[0]);
  const uint64x1_t row1h = vget_high_u64(row01.val[1]);
  const uint64x1_t row3l = vget_low_u64(row23.val[1]);
  row01.val[0] = vcombine_u64(vget_low_u64(row01.val[0]), row2l);
  row23.val[0] = vcombine_u64(row0h, vget_high_u64(row23.val[0]));
  row01.val[1] = vcombine_u64(vget_low_u64(row01.val[1]), row3l);
  row23.val[1] = vcombine_u64(row1h, vget_high_u64(row23.val[1]));

  const int32x4x2_t out01 = vtrnq_s32(vreinterpretq_s32_u64(row01.val[0]),
      vreinterpretq_s32_u64(row01.val[1]));
  const int32x4x2_t out23 = vtrnq_s32(vreinterpretq_s32_u64(row23.val[0]),
      vreinterpretq_s32_u64(row23.val[1]));

  int32x4x4_t out;
  out.val[0] = out01.val[0];
  out.val[1] = out01.val[1];
  out.val[2] = out23.val[0];
  out.val[3] = out23.val[1];
  return out;
}

// =============================================================================

static inline  uint32x4x4_t transpose4x4(const uint32x4x4_t rows) {
  uint64x2x2_t row01, row23;

  row01.val[0] = vreinterpretq_u64_u32(rows.val[0]);
  row01.val[1] = vreinterpretq_u64_u32(rows.val[1]);
  row23.val[0] = vreinterpretq_u64_u32(rows.val[2]);
  row23.val[1] = vreinterpretq_u64_u32(rows.val[3]);

  const uint64x1_t row0h = vget_high_u64(row01.val[0]);
  const uint64x1_t row2l = vget_low_u64(row23.val[0]);
  const uint64x1_t row1h = vget_high_u64(row01.val[1]);
  const uint64x1_t row3l = vget_low_u64(row23.val[1]);
  row01.val[0] = vcombine_u64(vget_low_u64(row01.val[0]), row2l);
  row23.val[0] = vcombine_u64(row0h, vget_high_u64(row23.val[0]));
  row01.val[1] = vcombine_u64(vget_low_u64(row01.val[1]), row3l);
  row23.val[1] = vcombine_u64(row1h, vget_high_u64(row23.val[1]));

  const uint32x4x2_t out01 = vtrnq_u32(vreinterpretq_u32_u64(row01.val[0]),
      vreinterpretq_u32_u64(row01.val[1]));
  const uint32x4x2_t out23 = vtrnq_u32(vreinterpretq_u32_u64(row23.val[0]),
      vreinterpretq_u32_u64(row23.val[1]));

  uint32x4x4_t out;
  out.val[0] = out01.val[0];
  out.val[1] = out01.val[1];
  out.val[2] = out23.val[0];
  out.val[3] = out23.val[1];
  return out;
}

// =============================================================================

inline int32x4_t shift_from_msb(int32x4_t v4, int32_t v)
{
    int32x4_t u4 = vdupq_n_s32(v);
    return vextq_s32(v4, u4, 1);
}

inline int32x4_t shift_from_lsb(int32x4_t v4, int32_t v)
{
    int32x4_t u4 = vdupq_n_s32(v);
    return vextq_s32(u4, v4, 3);
}

}
}
}

