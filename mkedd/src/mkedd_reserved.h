/*
 * mkedd - MkE depth sensor library, reserved API
 *
 * Copyright (c) 2018, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifndef _MKEDD_RESERVED_H_
#define _MKEDD_RESERVED_H_

#define MKEDD_RESERVED_API_DECLARATIONS \
    void getResults(const uint32_t det_idx, nlohmann::json &results); \
    void getDebugData(const uint32_t det_idx, nlohmann::json &dbgdata);

#endif // _MKEDD_RESERVED_H_
