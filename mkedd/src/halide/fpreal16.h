#pragma once

#include "Halide.h"

namespace Halide {

struct Fpreal16 {
  Expr v;

  explicit operator Expr() const {
    return v;
  }
};

Fpreal16 operator+(const Fpreal16 &x, const Fpreal16 &y)
{
  return Fpreal16{x.v + y.v};
}

Fpreal16 operator-(const Fpreal16 &x, const Fpreal16 &y)
{
  return Fpreal16{x.v - y.v};
}

Fpreal16 operator/(const Fpreal16 &x, const Fpreal16 &y)
{
  return Fpreal16{cast(Int(32), (cast(Int(64), x.v) << 16) / cast(Int(64), y.v))};
}

Fpreal16 operator*(const Fpreal16 &x, const Fpreal16 &y)
{
  return Fpreal16{cast(Int(32), (cast(Int(64), x.v) * cast(Int(64), y.v)) >> 16)};
}

}
