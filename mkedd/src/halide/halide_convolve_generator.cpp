#include "Halide.h"
#include "fpreal16.h"
using namespace Halide;

class HalideConvolve : public Halide::Generator<HalideConvolve> {
public:
  GeneratorParam<bool> parallel{"parallel", false};
  GeneratorParam<bool> hexagon{"hexagon", false};
  Input<int> a{"a", 0};
  Input<int> b{"b", 0};
  Input<Buffer<int32_t>> input{"input", 1};
  Input<Buffer<int32_t>> scales{"scales", 1};

  Output<Buffer<int32_t>> output{"output", 2};

  Func convolve;
  Var x, y;

  void generate()
  {
    Func offset, ibw3, x1, x2, x3, x4, v1, v2, v3, v4;
    offset(x) = (scales(x) - 1) >> 1;
    ibw3(x) = static_cast<Expr>(Fpreal16{1 << 16} / Fpreal16{scales(x) << (16 + 2)});
    Expr e = input.dim(0).extent();
    Expr ba = input(0) >> 16; // Fpreal16 -> int
    Expr da = (input(1) - input(0)) >> 16; // Fpreal16 -> int
    Expr bb = input(e - 1) >> 16; // Fpreal16 -> int
    Expr db = (input(e - 1) - input(e - 2)) >> 16; // Fpreal16 -> int
 
    x1(x, y) = (x - offset(y) - 1) - scales(y);
    v1(x, y) = clamp_read(input, x1(x, y), 0, ba, da, e - 1, bb, db);

    x2(x, y) = x - offset(y) - 1;
    v2(x, y) = clamp_read(input, x2(x, y), 0, ba, da, e - 1, bb, db);

    x3(x, y) = x + offset(y);
    v3(x, y) = clamp_read(input, x3(x, y), 0, ba, da, e - 1, bb, db);

    x4(x, y) = x + offset(y) + scales(y);
    v4(x, y) = clamp_read(input, x4(x, y), 0, ba, da, e - 1, bb, db);

#if 1
    v3(x, y) = static_cast<Expr>(Fpreal16{v3(x, y)} - Fpreal16{v2(x, y)});
    v1(x, y) = static_cast<Expr>(Fpreal16{v1(x, y)} - Fpreal16{v4(x, y)});
    v3(x, y) = static_cast<Expr>(Fpreal16{v3(x, y)} + Fpreal16{v3(x, y)} + Fpreal16{v3(x, y)} + Fpreal16{v1(x, y)});
#else
    v3(x, y) = v3(x, y) - v2(x, y);
    v1(x, y) = v1(x, y) - v4(x, y);
    v3(x, y) = v3(x, y) + v3(x, y) + v3(x, y) + v1(x, y);
#endif
    convolve(x, y) = static_cast<Expr>(Fpreal16{v3(x, y)} * Fpreal16{ibw3(y)});

    // output(x, y) = select(x < a, 0, x > b, 0, convolve(x, y));
    output(x, y) = convolve(x, y);
  }

  void schedule()
  {
    if (hexagon) {
      convolve.hexagon();
      output.vectorize(x, 128 * 2);
    } else {
      output.vectorize(x, natural_vector_size(output.type()));
    }
    if (parallel) {
      output.parallel(y);
    }
  }

private:
  Expr clamp_read(const Func &data, const Expr n, const Expr a, const Expr ba, const Expr da, const Expr b, const Expr bb, const Expr db)
  {
    return select(
      n < a, (ba - (a - n) * da) << 16, // int -> Fpreal16
      n > b, (bb + (n - b) * db) << 16, // int -> Fpreal16
      input(clamp(n, a, b))
    );
  }
};

HALIDE_REGISTER_GENERATOR(HalideConvolve, halide_convolve)
