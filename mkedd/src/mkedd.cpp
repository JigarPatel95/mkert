#include <stdexcept>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <memory>

#include <map>
#include <list>

#include <boost/ptr_container/ptr_vector.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/split.hpp>

#ifdef MKEDD_USE_EXTERNAL_MD5
#include "md5.h"
#else
#include <boost/interprocess/file_mapping.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/uuid/detail/md5.hpp>
#include <boost/algorithm/hex.hpp>
#endif

#ifdef MKEDD_USE_MKECC
#include "mkecc/session/session.h"
#include "dd/session_wrapper.h"
#include "dd/siftgeom.h"
#endif

#include "json.hpp"
using nlohmann::json;

#include <json-schema.hpp>
using nlohmann::json_schema::json_validator;

#include "mkedd.h"
#include "util/info.h"

#include "dd/factory.h"
#include "pp/factory.h"

#ifdef MKEDD_RESERVED_API
#include "types/fpreal.h"
#include "types/convert.h"
#include "util/calibparser.h"
#endif

#ifndef MKEDD_VERSION_MAJOR
#define MKEDD_VERSION_MAJOR "0"
#endif

#ifndef MKEDD_VERSION_MINOR
#define MKEDD_VERSION_MINOR "0"
#endif

#ifndef MKEDD_VERSION_PATCH
#define MKEDD_VERSION_PATCH "0"
#endif


// ============================================================================
// DepthSensor::Impl

using namespace mke::dd;
using namespace mke::dd::dd;
using namespace mke::dd::pp;

class  DepthSensor::Impl {
private:
  enum CalibFileType {
    CALIB_FILE_UNKNOWN = 0,
    CALIB_FILE_MKEDET01 = 1,
    CALIB_FILE_MSF = 2
  };
  
  struct CalibFileInfo {
    std::string file_name;
    CalibFileType file_type;
    Detector::SectionType dd_section_type;
    PostProcessor::SectionType pp_section_type;
    json header;
    std::string md5sum;
  };
  
  std::list<CalibFileInfo> calib_files_;
  const static std::string default_profiles_;

  boost::ptr_vector<Detector> dds_;
  boost::ptr_vector<PostProcessor> pps_;
  std::vector<std::vector<PointData>> dets_;
  std::vector<uint32_t> laser_patterns_;
  std::vector<uint32_t> no_detections_;
  
  unsigned int no_detectors_;
  std::string current_detector_type_str_;
  std::string current_postproc_type_str_;
  CalibFileInfo *current_calib_file_info_;
  
  FileOpenCallback file_open_callback_;
  FileCloseCallback file_close_callback_;
  
  // ==========================================================================
  // Calib files ==============================================================
  
  CalibFileInfo getCalibFileInfo(const char *file_name)
  {
    CalibFileInfo file_info;
    CalibFileType file_type = CALIB_FILE_UNKNOWN;
    Detector::SectionType dd_section_type = Detector::DD_SECTION_UNKNOWN;
    PostProcessor::SectionType pp_section_type = PostProcessor::PP_SECTION_UNKOWN;

    // File extensions
    std::vector<std::string> tokens;
    boost::split(tokens, file_name, boost::is_any_of("."));
    size_t no_tokens = tokens.size();

    // Can the file be opened?    
    std::istream *file_stream = nullptr;
    file_open_callback_(file_name, &file_stream);
        
    char magik[9] = ""; 
    file_stream->read(magik, sizeof(magik) - 1);
    std::string magik_str(magik);
    
    if (!magik_str.compare(0, 8, "MKEDET01"))
      {
        uint32_t dst, pst;
        file_stream->read((char *) &dst, sizeof(dst)); // no_detectors
        file_stream->read((char *) &dst, sizeof(dst)); // dd_section_type
        file_stream->read((char *) &pst, sizeof(pst)); // pp_section_type

        file_type = CALIB_FILE_MKEDET01;
        dd_section_type = Detector::SectionType(dst);
        pp_section_type = PostProcessor::SectionType(pst);
      }
    else if (no_tokens > 0)
      {
        const std::string &ext1 = tokens[no_tokens - 1];

        if (boost::iequals(ext1, "bz2")  || boost::iequals(ext1, "bzip2") ||
            boost::iequals(ext1, "json") || boost::iequals(ext1, "cbor") ||
            boost::iequals(ext1, "msf"))
          {
            file_type = CALIB_FILE_MSF;
            dd_section_type = Detector::DD_SECTION_MSF;
            pp_section_type = PostProcessor::PP_SECTION_EMPTY;
          }
      }
         
    file_close_callback_(&file_stream);

    if (file_type == CALIB_FILE_UNKNOWN)
      throw std::runtime_error("Unrecognized file type: " + std::string(file_name));

    file_info.file_name = std::string(file_name);
    file_info.file_type = file_type;
    file_info.dd_section_type = dd_section_type;
    file_info.pp_section_type = pp_section_type;
    file_info.header = json::object();
    file_info.md5sum = "";

    return file_info;
  }

  // ==========================================================================

  CalibFileInfo* findCompatibleCalibFile(const std::string &detector_type_str, const std::string &postproc_type_str)
  {
    std::unique_ptr<Detector> dd = std::unique_ptr<Detector>(dd::Factory::create(detector_type_str));
    std::unique_ptr<PostProcessor> pp = std::unique_ptr<PostProcessor>(pp::Factory::create(postproc_type_str));
        
    for (CalibFileInfo &file_info : calib_files_)
      {
        if (file_info.file_type == CALIB_FILE_UNKNOWN)
          continue;

        if (dd->canParse(file_info.dd_section_type) && pp->canParse(file_info.pp_section_type))
          return &file_info;
      }
      
    // Return CALIB_FILE_UNKNOWN
    return &(calib_files_.front());
  }
  
  // ==========================================================================

  void loadMkeDet01CalibFile(CalibFileInfo *file_info, const std::string &detector_type_str, const std::string &postproc_type_str)
  {
    std::istream *file_stream = nullptr;
    file_open_callback_(file_info->file_name.c_str(), &file_stream);
    
    char magic[9] = ""; 
    file_stream->read(magic, sizeof(magic) - 1);    
    std::string magic_str(magic);
 
    // Magic value
    if (magic_str.compare(0, 8, "MKEDET01"))
      throw std::runtime_error("Not a MKEDET01 file: " + file_info->file_name);
    
    // Number of detectors
    uint32_t no_detectors;
    file_stream->read((char *) &no_detectors, sizeof(no_detectors)); // no_detectors

    // Section types
    uint32_t dst, pst;
    file_stream->read((char *) &dst, sizeof(dst)); // dd_section_type
    file_stream->read((char *) &pst, sizeof(pst)); // pp_section_type    
    
    // Test the section/detector/postprocessor compatibility
    std::unique_ptr<Detector> dd = std::unique_ptr<Detector>(dd::Factory::create(detector_type_str));
    std::unique_ptr<PostProcessor> pp = std::unique_ptr<PostProcessor>(pp::Factory::create(postproc_type_str));

    if (!dd->canParse(Detector::SectionType(dst)) ||
        !pp->canParse(PostProcessor::SectionType(pst)))    
      {
        throw std::runtime_error("MKEDET01 file not compatible with requested detector/postprocessor: " + file_info->file_name);
      }
 
    // Read header
    uint32_t header_num_bytes;
    file_stream->read((char *) &header_num_bytes, sizeof(header_num_bytes));

#ifdef __nios2_arch__ 
    // Here, we don't care about the contents of the header info, so we skip it.
    // Unfortunately, the nios2 platform has a problem with the seekg method.

    char dummy_char;
    for (unsigned int i = 0; i < header_num_bytes; i++)
      file_stream->read((char *) &dummy_char, sizeof(dummy_char));
#else
    char info_data[header_num_bytes + 1];
    file_stream->read(info_data, header_num_bytes);
    info_data[header_num_bytes] = '\0';
    try
    {
      file_info->header = json::parse(info_data);
    }
    catch (...)
    {
      //
    }
#endif
    
    // Every exception beyond this point means that the DepthSensor object state
    // is invalid. We need to catch the expection, clear DepthSensor object and re-throw.
    
    try 
      {
        clearState();
        no_detectors_ = no_detectors;
        
        // Laser pattern
        laser_patterns_.reserve(no_detectors_);
        for (unsigned int i = 0; i < no_detectors_; i++)
          {
            uint32_t lp;
            file_stream->read((char *) &lp, sizeof(lp));
            laser_patterns_.push_back(lp);
          }
          
        // Detectors
        dds_.reserve(no_detectors_);
        dets_.resize(no_detectors_);
        
        for (unsigned int i = 0; i < no_detectors_; i++)
          {
            // Read detector section size
            uint32_t section_num_bytes;
            file_stream->read((char *) &section_num_bytes, sizeof(section_num_bytes));

            // Read detector section data
            dds_.push_back(dd::Factory::create(detector_type_str));
            dds_[i].load(file_stream, Detector::SectionType(dst));
            dets_[i].resize(dds_[i].getMaxNoDetections(), PointData{});
          }            

        // Profiles
        pps_.reserve(no_detectors_);
        for (unsigned int i = 0; i < no_detectors_; i++)
          {
            // Read postprocessor section size
            uint32_t section_num_bytes;
            file_stream->read((char *) &section_num_bytes, sizeof(section_num_bytes));

            // Read postprocessor section data
            pps_.push_back(pp::Factory::create(postproc_type_str));
            pps_[i].load(file_stream, PostProcessor::SectionType(pst));
            pps_[i].setMaxDetections(dds_[i].getMaxNoDetections());
          }
      }
    catch (std::exception &e)
      {
        clearState();
        file_close_callback_(&file_stream);
        current_calib_file_info_ = &calib_files_.front();

        throw;
      }
      
    file_close_callback_(&file_stream);
  }
  
  // ==========================================================================

  void loadMsfCalibFile(CalibFileInfo *file_info, const std::string &detector_type_str, const std::string &postproc_type_str)
  {
#ifdef MKEDD_USE_MKECC
    //auto session = std::make_shared<mke::cc::session::Session>();
    auto session = std::shared_ptr<mke::cc::session::Session>(new mke::cc::session::Session());
    // Load session
    session->load(file_info->file_name);

    mke::dd::dd::SessionWrapper session_wrapper;
    session_wrapper.setSession(session);

    uint32_t no_detectors = session_wrapper.getNumDoeGroups();
    file_info->header = session_wrapper.getInfoData();

    // Every exception beyond this point means that the DepthSensor object state
    // is invalid. We need to catch the expection, clear DepthSensor object and re-throw.
    try
      {
        clearState();
        no_detectors_ = no_detectors;

        // Laser pattern
        laser_patterns_.reserve(no_detectors_);

        for (unsigned int i = 0; i < no_detectors_; i++)
          laser_patterns_.push_back(session_wrapper.getLaserPattern(i));

        // Detectors
        dds_.reserve(no_detectors_);
        dets_.resize(no_detectors_);

        for (unsigned int i = 0; i < no_detectors_; i++)
          {
            // Read detector section data
            dds_.push_back(dd::Factory::create(detector_type_str));

            if (dds_[i].getTypeString() == "SiftGeom")
              {
                mke::dd::dd::SiftGeomDetector* siftgeom = reinterpret_cast<mke::dd::dd::SiftGeomDetector*>(&(dds_[i]));
                siftgeom->setSession(session);
                siftgeom->setDetIdx(i);
              }
            else
              {
                throw std::runtime_error("Unsupported Detector type: " + dds_[i].getTypeString());
              }

            dets_[i].resize(dds_[i].getMaxNoDetections(), PointData{});
          }

        // Profiles
        pps_.reserve(no_detectors_);
        for (unsigned int i = 0; i < no_detectors_; i++)
          {
            // Read postprocessor section data
            pps_.push_back(pp::Factory::create(postproc_type_str));
            pps_[i].setMaxDetections(dds_[i].getMaxNoDetections());
          }          
       }
    catch (std::exception &e)
      {
        clearState();
        current_calib_file_info_ = &calib_files_.front();

        throw;
      }
#else
    throw std::runtime_error("Cannot load MSF (Mkecc session file) as calibration file. Compiled without Mkecc support");
#endif
  }

  // ==========================================================================

  void loadCalibFile(CalibFileInfo *file_info, const std::string &detector_type_str, const std::string &postproc_type_str)
  {
    if (file_info->file_type == CALIB_FILE_MKEDET01)
      loadMkeDet01CalibFile(file_info, detector_type_str, postproc_type_str);
    else if (file_info->file_type == CALIB_FILE_MSF)
      loadMsfCalibFile(file_info, detector_type_str, postproc_type_str);
    else
      throw std::runtime_error("Unrecognized file type: " + std::to_string(file_info->file_type));
    
    no_detections_.resize(dds_.size(), 0);
    
    // Set current state
    current_detector_type_str_ = detector_type_str;
    current_postproc_type_str_ = postproc_type_str;
    current_calib_file_info_ = file_info;   
  }

  // ==========================================================================
  
  void clearState(void)
  {
    no_detectors_ = 0;
    dds_.clear();
    pps_.clear();
    laser_patterns_.clear();
    no_detections_.clear();
    
    current_calib_file_info_ = nullptr;
    current_detector_type_str_ = "";
    current_postproc_type_str_ = "";   
  }
  
  // ==========================================================================
  // File I/O =================================================================
  
  void fileOpenCallback(const char *file_name, std::istream **is)
  {
    if (*is)
      fileCloseCallback(is);
    
    std::ifstream *file_stream = new std::ifstream(file_name, std::ifstream::binary);
    
    if (!file_stream->good())
    {
      *is = nullptr;
      delete file_stream;
      throw std::runtime_error("Cannot open file for reading: " + std::string(file_name));    
    }

    *is = static_cast<std::istream *>(file_stream);
  }
  
  // ==========================================================================

  void fileCloseCallback(std::istream **is)
  {
    std::ifstream *file_stream = dynamic_cast<std::ifstream *>(*is);
    file_stream->close();  
    delete file_stream;
    *is = nullptr;
  }  
  
  // ==========================================================================

  void getMd5Sum(CalibFileInfo *file_info)
  {
    if (!file_info)
      return;

#ifdef MKEDD_USE_EXTERNAL_MD5
    unsigned char sig[MD5_SIZE];
    char buffer[4096];
    md5::md5_t md5;

    std::ifstream ifs(file_info->file_name.c_str(), std::ios::binary);
    do
      {
        ifs.read(buffer, 4096);
        md5.process(buffer, ifs.gcount());
      } while (ifs);

    md5.finish(sig);
    md5::sig_to_string(sig, buffer, sizeof(buffer));
    file_info->md5sum = buffer;
#else
    boost::interprocess::file_mapping mapping(file_info->file_name.c_str(), boost::interprocess::read_only);
    boost::interprocess::mapped_region mapped_rgn(mapping, boost::interprocess::read_only);
    char const* const mmaped_data = static_cast<char*>(mapped_rgn.get_address());
    std::size_t const mmap_size = mapped_rgn.get_size();

    boost::uuids::detail::md5 hash;
    boost::uuids::detail::md5::digest_type digest;

    hash.process_bytes(mmaped_data, mmap_size);
    hash.get_digest(digest);

    const auto charDigest = reinterpret_cast<const char *>(&digest);
    file_info->md5sum = "";
    boost::algorithm::hex(charDigest, charDigest + sizeof(boost::uuids::detail::md5::digest_type),
                          std::back_inserter(file_info->md5sum));

    std::transform(file_info->md5sum.begin(), file_info->md5sum.end(),
                   file_info->md5sum.begin(), ::tolower);
#endif
  }

// ============================================================================
// ============================================================================  
  
public:
  
  Impl()
  {
    clearState();
    clearCalibFiles();

    file_open_callback_ = std::bind(&DepthSensor::Impl::fileOpenCallback, this, std::placeholders::_1, std::placeholders::_2);
    file_close_callback_ = std::bind(&DepthSensor::Impl::fileCloseCallback, this, std::placeholders::_1);
  }
  
  ~Impl() 
  {
    clearState();
  }

  // ==========================================================================
  // Parameter Profiles =======================================================

  void validateProfile(const char *profile)
  {

    std::cout << "validateProfile" << std::endl;

    json profile_json = json::parse(profile);

    auto dd_it = profile_json.find("detector");
    auto pp_it = profile_json.find("postproc");

    if ((dd_it == profile_json.end()) && (pp_it == profile_json.end()))
      throw std::runtime_error("[MkEDD] Invalid profile: both /detector and /postproc items are missing");

    if (dd_it != profile_json.end())
      {
        auto type_it = dd_it->find("type");

        if (type_it != dd_it->end())
          {
            std::unique_ptr<Detector> dd = std::unique_ptr<Detector>(dd::Factory::create(type_it->get<std::string>()));
            dd->validateParams(*dd_it);
          }
        else if (dds_.size() > 0)
          {
            dds_[0].validateParams(*dd_it);
          }
        else
          {
            throw std::runtime_error("[MkEDD] Invalid profile: /detector/type item not set");
          }
      }

    if (pp_it != profile_json.end())
      {
        auto type_it = pp_it->find("type");

        if (type_it != pp_it->end())
          {
            std::unique_ptr<PostProcessor> pp = std::unique_ptr<PostProcessor>(pp::Factory::create(type_it->get<std::string>()));
            pp->validateParams(*pp_it);
          }
        else if (pps_.size() > 0)
          {
            pps_[0].validateParams(*pp_it);
          }
        else
          {
            throw std::runtime_error("[MkEDD] Invalid profile: /postproc/type item not set");
          }
      }
  }

  // ==========================================================================

  void setProfile(const char *profile)
  {
    json profile_json = json::parse(profile);
    json &dd_json = profile_json["detector"];
    json &pp_json = profile_json["postproc"];

    auto dd_type_it = dd_json.find("type");
    auto pp_type_it = pp_json.find("type");

    std::string new_detector_type_str = current_detector_type_str_;
    std::string new_postproc_type_str = current_postproc_type_str_;

    if (dd_type_it != dd_json.end())
      new_detector_type_str = dd_type_it->get<std::string>();

    if (pp_type_it != pp_json.end())
       new_postproc_type_str = pp_type_it->get<std::string>();

    if ((new_detector_type_str != current_detector_type_str_) ||
        (new_postproc_type_str != current_postproc_type_str_))
      {
        // We need to reset and reload detectors/postprocessors
        if (dds_.size() && (new_detector_type_str == current_detector_type_str_))
          {
            // Try to set and save current parameters
            if (!dd_json.is_null())
              dds_[0].setParams(dd_json);
            dds_[0].getParams(dd_json);
          }

        if (pps_.size() && (new_postproc_type_str == current_postproc_type_str_))
          {
            // Try to set and save current parameters
            if (!pp_json.is_null())
              pps_[0].setParams(pp_json);
            pps_[0].getParams(pp_json);
          }

        // Find calib file compatible with requested detector and postprocessor types
        CalibFileInfo *file_info = findCompatibleCalibFile(new_detector_type_str, new_postproc_type_str);
    
        if (file_info->file_type == CALIB_FILE_UNKNOWN)
          throw std::runtime_error("[MkEDD] No calibration file found for " +
            new_detector_type_str + " and " + new_postproc_type_str);

        // Reset and load detectors/postprocessors
        loadCalibFile(file_info, new_detector_type_str, new_postproc_type_str);

        // Set new profile parameters
        for (unsigned int i = 0; i < no_detectors_; i++)
        {
          dds_[i].setParams(dd_json);
          pps_[i].setParams(pp_json);
        }
      }
    else
      {
        if (!dds_.size() && !pps_.size())
          throw std::runtime_error("[MkEDD] Incomplete profile, please set full profile for detector initialization");

        // Set new profile parameters if applicable
        for (unsigned int i = 0; i < no_detectors_; i++)
        {
          if (!dd_json.is_null())
            dds_[i].setParams(dd_json);
          if (!pp_json.is_null())
            pps_[i].setParams(pp_json);
        }
      }
  }  

  // ==========================================================================

  void setDefaultProfile(void)
  {    
    json profiles = json::parse(default_profiles_);

    for (const json &profile: profiles)
      {
        try
        {
          setProfile(profile.dump().c_str());
          return;
        }
        catch (std::exception &e)
        {
           // next iteration
        }
      }

    throw std::runtime_error("[MkEDD] No suitable default profile can be applied");
  }

  // ==========================================================================

  std::string getDefaultProfile(void) const
  {
    return default_profiles_;
  }

  // ==========================================================================

  std::string getProfile(void)
  {
    json profile_json;

    if ((dds_.size() > 0) && (pps_.size() > 0))
      {
        dds_[0].getParams(profile_json["detector"]);
        profile_json["detector"]["type"] = dds_[0].getTypeString();

        pps_[0].getParams(profile_json["postproc"]);
        profile_json["postproc"]["type"] = pps_[0].getTypeString();
      }
    else
      {
        profile_json = json::object();
      }

    return profile_json.dump(2);
  }

  // ==========================================================================

  std::string getCurrentProfileSchema(void)
  {
    json schema_json;

    if ((dds_.size() > 0) && (pps_.size() > 0))
      {
        schema_json = R"SCHEMA(
        {
          "type" : "object",
          "additionalProperties" : false,
          "properties" : {
            "detector" : {},
            "postproc" : {}
          }
        }
        )SCHEMA"_json;

        schema_json["properties"]["detector"] = dds_[0].getParamsSchema();
        schema_json["properties"]["detector"]["description"] = "Detector parameters";

        schema_json["properties"]["postproc"] = pps_[0].getParamsSchema();
        schema_json["properties"]["postproc"]["description"] = "Postprocessor parameters";
      }
    else
      {
        schema_json = json::object();
      }

    return schema_json.dump(2);
  }


  // ==========================================================================
  // Runtime statistics =======================================================
  
  void resetStats(void) 
  {
    for (unsigned int i = 0; i < dds_.size(); i++)
      dds_[i].resetStats();  

    for (unsigned int i = 0; i < pps_.size(); i++)
      pps_[i].resetStats();
  }

  // ==========================================================================

  std::string getStats(void)
  {
    if ((dds_.size() > 0) && (pps_.size() > 0))
      {
        json stats_json;

        // Calibration file info
        json file_info_json = json::object();

        if (current_calib_file_info_->md5sum == "")
          {
            try
            {
              getMd5Sum(current_calib_file_info_);
            }
            catch (...)
            {
              current_calib_file_info_->md5sum = "N/A";
            }
          }

        file_info_json["md5sum"] = current_calib_file_info_->md5sum;
        file_info_json["header"] = current_calib_file_info_->header;
        stats_json["file_info"].swap(file_info_json);

        // Laser pattern
        json laser_pattern_json = json::array();
        for (unsigned int i = 0; i < laser_patterns_.size(); i++)
          laser_pattern_json.push_back(laser_patterns_[i]);

        stats_json["laser_patterns"].swap(laser_pattern_json);

        // Detectors
        json dds_stats_array_json;
        for (unsigned int i = 0; i < dds_.size(); i++)
          {
            json dds_stats_json;
            dds_[i].getStats(dds_stats_json);
            dds_stats_array_json.push_back(nullptr);
            dds_stats_array_json.back().swap(dds_stats_json);
          }

        stats_json["dds"].swap(dds_stats_array_json);

        // Postprocessors
        json pps_stats_array_json;
        for (unsigned int i = 0; i < pps_.size(); i++)
          {
            json pps_stats_json;
            pps_[i].getStats(pps_stats_json);
            pps_stats_array_json.push_back(nullptr);
            pps_stats_array_json.back().swap(pps_stats_json);
          }

        stats_json["pps"].swap(pps_stats_array_json);

        // Library info
        stats_json["version"] = mke::dd::util::GetInfoString();

        return stats_json.dump(2);
      }
    else
      {
        return "{}";
      }
  }

  // ==========================================================================
  // Calib files ==============================================================
  
  void addCalibFile(const char *file_name)
  {
    CalibFileInfo file_info = getCalibFileInfo(file_name);
    calib_files_.push_back(file_info);
  }

  // ==========================================================================

  void clearCalibFiles(void) 
  {
    calib_files_.clear();

    CalibFileInfo unkown_file_info;
    unkown_file_info.file_type = CALIB_FILE_UNKNOWN;
    unkown_file_info.file_name = "";

    calib_files_.push_back(unkown_file_info);
    current_calib_file_info_ = &calib_files_.front();
  }
  
  // ==========================================================================
  // File I/O =================================================================

  void setCallback(const FileOpenCallback &fnc)
  {
    file_open_callback_ = fnc;
  }

  // ==========================================================================

  void setCallback(const FileCloseCallback &fnc)
  {
    file_close_callback_ = fnc;
  }  
  
  // ==========================================================================
  // Processing ===============================================================
  
  uint32_t process(const uint32_t det_idx, const uint8_t *data)
  {
    if (!no_detectors_)
      return 0;
    
    uint32_t no_detections = dds_[det_idx].process(data, dets_[det_idx]);
    no_detections = pps_[det_idx].process(dets_[det_idx], no_detections);

    no_detections_[det_idx] = no_detections;
    return no_detections;
  }
  
  // ==========================================================================

  void getDetections(const uint32_t det_idx, const PointData* &detections, uint32_t &detections_size)
  {
    detections = &(dets_[det_idx][0]);
    detections_size = dets_[det_idx].size();
  }

  // ==========================================================================

  void clearDetections(const uint32_t det_idx)
  {
    no_detections_[det_idx] = 0;
  }

#ifdef MKEDD_RESERVED_API
  // ==========================================================================

  void getResults(const uint32_t det_idx, nlohmann::json &results)
  {
    const PointData* dets;
    uint32_t no_dets, no_vdets, idx;

    getDetections(det_idx, dets, no_dets);

    mke::dd::Data3dType type = dds_[det_idx].getData3dType();

    nlohmann::json lut2d_json = nlohmann::json::array();
    nlohmann::json lut3d_json = nlohmann::json::array();
    nlohmann::json lutid_json = nlohmann::json::array();
    nlohmann::json uid_json = nlohmann::json::array();
    nlohmann::json resp_json = nlohmann::json::array();
    nlohmann::json sigma2d_json = nlohmann::json::array();
    nlohmann::json sigma3d_json = nlohmann::json::array();

    results["lut2d"].swap(lut2d_json);
    results["lut3d"].swap(lut3d_json);
    results["lutid"].swap(lutid_json);
    results["uid"].swap(uid_json);
    results["resp"].swap(resp_json);
    results["sigma2d"].swap(sigma2d_json);
    results["sigma3d"].swap(sigma3d_json);

    if (!no_dets)
      return;

    no_vdets = 0;
    for (uint32_t i = 0; i < no_dets; i++)
      {
        if (dets[i].confidence)
          no_vdets++;
      }

    if (!no_vdets)
      return;

    lut2d_json[no_vdets - 1] = nullptr;
    lut3d_json[no_vdets - 1] = nullptr;
    lutid_json[no_vdets - 1] = nullptr;
    uid_json[no_vdets - 1] = nullptr;
    resp_json[no_vdets - 1] = nullptr;
    sigma2d_json[no_vdets - 1] = nullptr;
    sigma3d_json[no_vdets - 1] = nullptr;

    idx = 0;

    for (uint32_t i = 0; i < no_dets; i++)
      {
        if (!dets[i].confidence)
          continue;

        double u = double(types::fpreal16r(dets[i].u, true));
        double v = double(types::fpreal16r(dets[i].v, true));
        double sigma3D = double(types::fpreal16r(dets[i].sigma3D, true));
        double sigma2D = double(types::fpreal16r(dets[i].sigma2D, true));
        double confidence = double(types::fpreal16r(dets[i].confidence, true));

        lut2d_json[idx] = {u, v};
        lut3d_json[idx] = {types::Convert(dets[i].x, type),
                           types::Convert(dets[i].y, type),
                           types::Convert(dets[i].z, type)};
        lutid_json[idx] = {dets[i].id1, dets[i].id2};
        uid_json[idx] = dets[i].uid;
        resp_json[idx] = confidence;
        sigma2d_json[idx] = sigma2D;
        sigma3d_json[idx] = sigma3D;

        idx++;
      }

    results["lut2d"].swap(lut2d_json);
    results["lut3d"].swap(lut3d_json);
    results["lutid"].swap(lutid_json);
    results["uid"].swap(uid_json);
    results["resp"].swap(resp_json);
    results["sigma2d"].swap(sigma2d_json);
    results["sigma3d"].swap(sigma3d_json);
 }

  // ==========================================================================

  void getDebugData(const uint32_t det_idx, nlohmann::json &dbgdata)
  {

    dbgdata.clear();

    if (dds_.size() < det_idx)
      return;

    dds_[det_idx].getDebugData(dbgdata);
  }

  // ==========================================================================

  void getTriangulation(const uint32_t det_idx, nlohmann::json &triangulation)
  {

    triangulation.clear();

    if (dds_.size() < det_idx)
      return;

    dds_[det_idx].getTriangulation(triangulation);
  }

  // ==========================================================================

  void parseCalibFile(const char *file_name, nlohmann::json &calibdata)
  {
    mke::dd::util::CalibParser::parse(file_name, calibdata);
  }

#endif

  // ==========================================================================
  // Info =====================================================================
  
  uint32_t getMaxNoDetections(void) const
  {
    uint32_t no_dets = 0;
    for (unsigned int i = 0; i < dds_.size(); i++)
      no_dets += dds_[i].getMaxNoDetections();
    return no_dets;
  }

  // ==========================================================================
  
  uint32_t getNoDetections(const uint32_t det_idx) const
  {
    return no_detections_[det_idx];
  }

  // ==========================================================================

  uint32_t getLaserPattern(const uint32_t det_idx) const
  {
    return laser_patterns_[det_idx];
  }

  // ==========================================================================
  
  uint32_t getNoDetectors(void) const
  {
    return dds_.size();
  }

  // ==========================================================================

  uint32_t getStride(const uint32_t det_idx) const
  {
    if (det_idx >= dds_.size())
      throw std::runtime_error("[MkEDD] getStride: Detector index out of bounds");

    return dds_[det_idx].getStride();
  }

  // ==========================================================================

  Data3dType getData3dType(const uint32_t det_idx) const
  {
    if (det_idx >= dds_.size())
      throw std::runtime_error("[MkEDD] getStride: Detector index out of bounds");

    return dds_[det_idx].getData3dType();
  }

};

// ============================================================================
// ============================================================================
// ============================================================================

const std::string DepthSensor::Impl::default_profiles_ = R"JSON(
 [
  {
    "detector" :
      {
        "type" : "BoxLut",
        "classifier" : "CircLinear",
        "threshold" : 8,
        "peak_ratio" : 1.2,
        "curv_ratio" : 1.15,
        "no_epochs" : 0,
        "box_delta" : 2.0,
        "no_threads" : 8
      },
    "postproc":
      {
        "type" : "PassThru"
      }
  },
)JSON"
#ifdef MKEDD_USE_MKECC
R"JSON(
  {
    "detector" :
      {
        "type" : "SiftGeom",
        "classifier" : "PassThru"
      },
    "postproc":
      {
        "type" : "PassThru"
      }
  },
)JSON"
#endif
R"JSON(
  {
    "detector" :
      {
        "type" : "BoxLut3",
        "classifier" : "CircLinear",
        "threshold" : 8,
        "curv_ratio" : 1.15,
        "no_epochs" : 0,
        "box_delta" : 2.0,
        "no_threads" : 8
      },
    "postproc":
      {
        "type" : "PassThru"
      }
  },
  {
    "detector" :
      {
        "type" : "IntLut2",
        "classifier" : "NgCircLinear",
        "threshold" : 8,
        "curv_ratio" : 1.15,
        "no_epochs" : 0,
        "box_delta" : 2.0,
        "no_threads" : 8
      },
    "postproc":
      {
        "type" : "PassThru"
      }
    }
 ]
 )JSON";

// ============================================================================
// ============================================================================
// DepthSensor 

// Create an implementation object in ctor
mke::dd::DepthSensor::DepthSensor()
: impl_(new Impl())
{}

mke::dd::DepthSensor::~DepthSensor() 
{
  delete impl_;
}

void mke::dd::DepthSensor::getVersion(uint32_t &major, uint32_t &minor, uint32_t &patch)
{
  major = std::stoi(MKEDD_VERSION_MAJOR);
  minor = std::stoi(MKEDD_VERSION_MINOR);
  patch = std::stoi(MKEDD_VERSION_PATCH);
}

// Parameter profiles =========================================================

void mke::dd::DepthSensor::setProfile(const char *profile)
{
  impl_->setProfile(profile);
}

void mke::dd::DepthSensor::validateProfile(const char *profile) const
{
  impl_->validateProfile(profile);
}

std::string mke::dd::DepthSensor::getProfile(void) const
{
  return impl_->getProfile();
}

void mke::dd::DepthSensor::setDefaultProfile(void)
{
  impl_->setDefaultProfile();
}

std::string mke::dd::DepthSensor::getDefaultProfile(void) const
{
  return impl_->getDefaultProfile();
}

std::string mke::dd::DepthSensor::getCurrentProfileSchema(void) const
{
  return impl_->getCurrentProfileSchema();
}

// Runtime statistics =========================================================

 void mke::dd::DepthSensor::resetStats(void)
 {
   impl_->resetStats();
 }
 
 std::string mke::dd::DepthSensor::getStats(void) const
 {
   return impl_->getStats();    
 }

// Calib files ================================================================

void mke::dd::DepthSensor::addCalibFile(const char *file_name)
{
  impl_->addCalibFile(file_name);
}

void mke::dd::DepthSensor::clearCalibFiles(void)
{
  impl_->clearCalibFiles();
}

// File I/O ===================================================================

void mke::dd::DepthSensor::setCallback(const FileOpenCallback &fnc)
{
  impl_->setCallback(fnc);
}

void mke::dd::DepthSensor::setCallback(const FileCloseCallback &fnc)
{
  impl_->setCallback(fnc);
}

// Processing =================================================================

uint32_t mke::dd::DepthSensor::process(const uint32_t det_idx, const uint8_t *data)
{
  return impl_->process(det_idx, data);
}

void mke::dd::DepthSensor::getDetections(const uint32_t det_idx, const PointData *&detections, uint32_t &detections_size)
{
  impl_->getDetections(det_idx, detections, detections_size);
}

void mke::dd::DepthSensor::clearDetections(const uint32_t det_idx)
{
  impl_->clearDetections(det_idx);
}

// Info =======================================================================

uint32_t mke::dd::DepthSensor::getMaxNoDetections(void) const
{
  return impl_->getMaxNoDetections();
}

uint32_t mke::dd::DepthSensor::getNoDetections(const uint32_t det_idx) const
{
  return impl_->getNoDetections(det_idx);
}

uint32_t mke::dd::DepthSensor::getLaserPattern(const uint32_t det_idx) const
{
  return impl_->getLaserPattern(det_idx);
}

uint32_t mke::dd::DepthSensor::getNoDetectors(void) const
{
  return impl_->getNoDetectors();
}

uint32_t mke::dd::DepthSensor::getStride(const uint32_t det_idx) const
{
  return impl_->getStride(det_idx);
}

Data3dType mke::dd::DepthSensor::getData3dType(const uint32_t det_idx) const
{
  return impl_->getData3dType(det_idx);
}

// Reserved API ===============================================================

#ifdef MKEDD_RESERVED_API
void mke::dd::DepthSensor::getResults(const uint32_t det_idx, nlohmann::json &detections)
{
  impl_->getResults(det_idx, detections);
}

void mke::dd::DepthSensor::getDebugData(const uint32_t det_idx, nlohmann::json &dbgdata)
{
  impl_->getDebugData(det_idx, dbgdata);
}

void mke::dd::DepthSensor::getTriangulation(const uint32_t det_idx, nlohmann::json &dbgdata)
{
  impl_->getTriangulation(det_idx, dbgdata);
}

void mke::dd::DepthSensor::parseCalibFile(const char *file_name, nlohmann::json &calibdata)
{
  impl_->parseCalibFile(file_name, calibdata);
}
#endif
