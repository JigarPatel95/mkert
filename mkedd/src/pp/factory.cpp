/*
 * ppfactory
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#include "pp/factory.h"

#include "pp/passthru.h"
#include "pp/sma.h"
#include "pp/ema.h"
#include "pp/adaema.h"
//#include "pp_m3ema.h"
//#include "pp_median3.h"

using namespace mke::dd::pp;

const std::map<const std::string, PostProcessor*(*)(void)> Factory::create_map_ = {
      {PassThru::staticGetTypeString(), &PassThru::create},
      {PpSma::staticGetTypeString(), &PpSma::create},
      {PpEma::staticGetTypeString(), &PpEma::create},
      {PpAdaEma::staticGetTypeString(), &PpAdaEma::create}
//      {PpM3Ema::staticGetTypeString(), &PpM3Ema::create},
//      {PpMedian3::staticGetTypeString(), &PpMedian3::create}
};
