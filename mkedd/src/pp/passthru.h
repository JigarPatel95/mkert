/*
 * pp_passthru - MkE pass through post processor
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */


#ifndef _PP_PASSTRHU_H_
#define _PP_PASSTRHU_H_

#include "pp/postproc.h"

namespace mke {
namespace dd {
namespace pp {

/** \addtogroup mkedd
 *  @{
 */

class PassThru : public PostProcessor {
private:
  unsigned int max_detections_;
  
public:
  
  // ==========================================================================
  // Static methods
  
  static const std::string staticGetTypeString(void)
  {
    static const std::string typeString = "PassThru";
    return typeString;
  }
  
  static PostProcessor* create(void)
  {
    return new PassThru();
  }  
  
  // ==========================================================================
  // Construction 
  
  PassThru() : max_detections_(0)
  {
  }
  
  // ==========================================================================
  // Parameters 
        
  void validateParams(const nlohmann::json &params)
    {}
    
  nlohmann::json getParamsSchema(void) const
  {
    nlohmann::json schema = R"SCHEMA(
    {
      "type" : "object",
      "additionalProperties" : false,
      "properties" : {
        "type" : {
          "description" : "Postprocessor type",
          "type" : "string",
          "enum" : ["PassThru"],
          "default" : "PassThru"
        }
      }
    }
    )SCHEMA"_json;

   return schema;
  }

  void setParams(const nlohmann::json &params)
    {}
    
  void getParams(nlohmann::json &params) const
    {}
    
  const std::string getTypeString(void)
  {
    return staticGetTypeString();
  }       
     
  bool canParse(const SectionType section)
  {
    if (section == PP_SECTION_EMPTY)
      return true;
    else
      return false;
  }
    
  void load(std::istream *is, const SectionType section) 
    {}    
    
  void setMaxDetections(unsigned int max_detections)
  {
    max_detections_ = max_detections;
  }
  
  // ==========================================================================
  // Runtime statistics
  
  void resetStats(void)
  {
  }
  
  void getStats(nlohmann::json &params) const
  {
  }    
  
  // ==========================================================================
  // Processing  
  
  uint32_t process(std::vector<PointData> &detections, uint32_t no_detections)
  {
    return no_detections;
  }

};
  



/** @}*/

} /* namespace pp */
} /* namespace dd */
} /* namespace mke */



#endif /* _PP_PASSTRHU_H_ */
