/*
 * ppfactory
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */

#ifndef _PPFACTORY_H_
#define _PPFACTORY_H_

#include <stdexcept>
#include <map>

#include "pp/postproc.h"

namespace mke {
namespace dd {
namespace pp {
  
/** \addtogroup mkedd
 *  @{
 */

class Factory {
private:
  const static std::map<const std::string, PostProcessor*(*)(void)> create_map_;
  
public:

  static PostProcessor* create(const std::string &postproc_type_str)
  {
    const auto create_func = create_map_.find(postproc_type_str);
    
    if (create_func != create_map_.end())
      return create_func->second();
    else
      throw std::runtime_error("Unknown post processor type: " + postproc_type_str);
  }
  
};


/** @}*/

} /* namespace pp */
} /* namespace dd */
} /* namespace mke */


#endif // _PPFACTORY_H_
