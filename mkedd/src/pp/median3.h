/*
 * pp_median3 - Median3 value post processor
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */


#ifndef _PP_MEDIAN3_H_
#define _PP_MEDIAN3_H_

#include "postproc.h"
#include "util/history.h"
#include "fpreal.h"

#include <iostream>
#include <cmath>

#ifdef MKEDD_MULTITHREADING
#include "thread_pool.h"
#endif

namespace mke {
namespace dd {

/** \addtogroup mkedd
 *  @{
 */

class PpMedian3 : public PostProcessor {
private:
  struct DetectionSlice {
    uint32_t det_min;
    uint32_t det_max;
    uint32_t no_detections;
  };
  
  unsigned int max_detections_;
  static const int max_epochs_ = 5;
  
  int history_length_;
  int no_threads_;
  std::vector<DetectionSlice> detslices_;
  
#ifdef MKEDD_MULTITHREADING
  ThreadPool thread_pool_;
#endif    
  
  struct HistoryInfo {
    int valid_epochs;
  };
  
  util::History<PointData, HistoryInfo, 2> history_;
  
  void clearHistory(void)
  {
    for (unsigned int i = 0; i < history_.size(); i++)
    {
      history_.info(i).valid_epochs = 0;      
      history_.at(0, 0).confidence = 0;
    }
    history_++;
  }

  void setupDetectionSlices(void)
  {
    detslices_.resize(no_threads_);
    uint32_t step = std::ceil(double(max_detections_) / double(no_threads_));
    uint32_t delta = 0;
    uint32_t counter = 0;
    
    for (int i = 0; i < no_threads_; i++)
      {
        delta = ((i + 1) == no_threads_) ? (max_detections_ - counter) : step;
    
        detslices_[i].det_min = counter;
        detslices_[i].det_max = counter + delta;
        detslices_[i].no_detections = 0;

        counter += step;
      }   
  }
  
  void setInitValues(void)
  {
#ifdef MKEDD_MULTITHREADING
    no_threads_ = 4;
    thread_pool_.setNoThreads(no_threads_);
#else
    no_threads_ = 1;
#endif     
  
    setupDetectionSlices();
    history_length_ = 2;
    history_.resize(max_detections_);
    clearHistory();
  }
  
public:
  
  // ==========================================================================
  // Construction 
  
  PpMedian3() : max_detections_(0)
  {
    setInitValues();
  }
  
  ~PpMedian3() 
    {}  
  
  // ==========================================================================
  // Static methods
  
  static const std::string staticGetTypeString(void)
  {
    static const std::string typeString = "Median3";
    return typeString;
  }
  
  static PostProcessor* create(void)
  {
    return new PpMedian3();
  }
  
  // ==========================================================================
  // Parameters
  
  inline bool isValidHistoryLength(const int &history_length)
  {
    return (history_length < 1) || (history_length > max_epochs_) ? false : true;
  } 

#ifdef MKEDD_MULTITHREADING
  inline bool isValidNoThreads(const uint32_t &no_threads)
  {
    return (no_threads < 1) || (no_threads > 4) ? false : true;
  }
#endif

  void validateAndSetParams(const boost::property_tree::ptree &ptree, bool validate_only)  
  {    
    boost::optional<double> history_length = ptree.get_optional<double>("history");  
    if (history_length)
      {
        if (!isValidHistoryLength(*history_length))
          throw std::runtime_error("Median3: history out of bounds: " + std::to_string(*history_length));
        if (!validate_only)
          history_length_ = *history_length;
      }
     
#ifdef MKEDD_MULTITHREADING
  boost::optional<uint32_t> no_threads = ptree.get_optional<uint32_t>("no_threads");
  if (no_threads)
    {
      if (!isValidNoThreads(*no_threads))
        throw std::runtime_error("Median3: no_threads out of bounds: " + std::to_string(*no_threads));
      if (!validate_only)
        {
          no_threads_ = *no_threads;
          thread_pool_.setNoThreads(no_threads_);
          setupDetectionSlices();
        }
    }
#endif      
  }  


  // ==========================================================================
  // Parameters 
      
  
  void validateParams(const boost::property_tree::ptree &ptree)
  {
    validateAndSetParams(ptree, true);
  }
    
  void setParams(const boost::property_tree::ptree &ptree)
  {
    validateAndSetParams(ptree, false);
  }
  
  void getParams(boost::property_tree::ptree &ptree) const
  {
    ptree.put("history", history_length_);
  
#ifdef MKEDD_MULTITHREADING    
    ptree.put("no_threads", no_threads_);
#endif
  }    
  
  const std::string getTypeString(void)
  {
    return staticGetTypeString();
  }   
     
  bool canParse(const SectionType section)
  {
    return (section == PP_SECTION_EMPTY) ? true : false;
  }
  
  void load(std::istream *is, const SectionType section) 
    {}    
    
  void setMaxDetections(unsigned int max_detections)
  {
    max_detections_ = max_detections;
    history_.resize(max_detections_);
    clearHistory();
    setupDetectionSlices();
  }
  
  // ==========================================================================
  // Runtime statistics
  
  void resetStats(void)
  {
  }
  
  void getStats(boost::property_tree::ptree &) const
  {
  }    
  
  // ==========================================================================
  // Processing  
  
  bool getPrediction(const uint32_t det_idx, std::vector<PointData> &detections, PointData &prediction)
  {
    int &valid_epochs = history_.info(det_idx).valid_epochs;
    
    if (detections[det_idx].confidence > 0)
      {
        prediction = detections[det_idx];
        valid_epochs++;
        valid_epochs = std::min(valid_epochs, history_length_);
        return true;
      }
    else if (valid_epochs)
      {
        prediction = history_.prev(det_idx);  
        valid_epochs--;
        valid_epochs = std::max(valid_epochs, 0);
        return true;
      }
    
    return false;
  }
  
  void updatePrediction(const uint32_t det_idx, int valid_epochs, PointData &prediction)
  {
    if (valid_epochs >= 2)
      {
        PointData &d1 = history_.at(det_idx, 1);
        PointData &d2 = history_.at(det_idx, 2);
        int16_t &z0 = prediction.z; 
        int16_t &z1 = d1.z;
        int16_t &z2 = d2.z;
     
        if (z0 < z1) 
          {
            if (z1 < z2)      // z0 z1 z2 
              prediction = d1;
            else if (z0 < z2) // z0 z2 z1
              prediction = d2;
            else              // z2 z0 z1
              prediction = prediction;
          } 
        else 
          {
            if (z0 < z2)      // z1 z0 z2
              prediction = prediction;
            else if (z2 < z1) // z2 z1 z0 
              prediction = d1;
            else              // z1 z2 z0
              prediction = d2;
          }
      }
  }
  
  void processDetectionSlice(std::vector<PointData> &detections, const unsigned int ds_idx)
  {
    uint32_t &det_min = detslices_[ds_idx].det_min;
    uint32_t &det_max = detslices_[ds_idx].det_max;
    uint32_t &no_dets = detslices_[ds_idx].no_detections;
    
    no_dets = 0;
    
    for (unsigned int i = det_min; i < det_max; i++)
      {
        int valid_epochs = history_.info(i).valid_epochs;
        
        // Can we make a prediction?
        if (!getPrediction(i, detections,  history_.curr(i)))
          continue;

        // Update prediction
        updatePrediction(i, valid_epochs,  history_.curr(i));
        detections[i] = history_.curr(i);
        
        no_dets++;      
      }    
  }
  
  uint32_t process(std::vector<PointData> &detections, uint32_t no_detections)
  {
#ifdef MKEDD_MULTITHREADING
    thread_pool_.setCounter(detslices_.size());
  
    for (unsigned int i = 0; i < detslices_.size(); i++)
      thread_pool_.process([this, &detections, i](){this->processDetectionSlice(detections, i); return true;});
  
    thread_pool_.waitForCounter();
#else
    for (unsigned int i = 0; i < detslices_.size(); i++)
      processDetectionSlice(detections, i);
#endif
  
    uint32_t no_dets = 0;
    for (unsigned int i = 0; i < detslices_.size(); i++)
      no_dets += detslices_[i].no_detections;  
 
    // Increment history epoch
    history_++;
     
    return no_dets;
  }

};

/** @}*/

} /* namespace dd */
} /* namespace mke */



#endif /* _PP_MEDIAN3_H_ */
