/*
 * postproc - MkEDD post processor
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */


#ifndef _POSTPROC_H_
#define _POSTPROC_H_

#include <stdint.h>
#include <iosfwd>
#include <vector>

#include "json.hpp"

#include "data_types.h"

namespace mke {
namespace dd {
namespace pp {

/** \addtogroup mkedd
 *  @{
 */

class PostProcessor {
public:
  
  enum SectionType {
    PP_SECTION_UNKOWN = 0,
    PP_SECTION_EMPTY = 1
  };
  
  PostProcessor()
    {}
    
  virtual ~PostProcessor()
    {}

  virtual void validateParams(const nlohmann::json &) = 0;
  virtual nlohmann::json getParamsSchema(void) const = 0;
  virtual void setParams(const nlohmann::json &) = 0;
  virtual void getParams(nlohmann::json &) const = 0;
  virtual const std::string getTypeString(void) = 0;

  /*
  virtual void validateParams(const boost::property_tree::ptree &) = 0;
  virtual void setParams(const boost::property_tree::ptree &) = 0;
  virtual void getParams(boost::property_tree::ptree &) const = 0;
  */

  virtual void resetStats(void) = 0;
  virtual void getStats(nlohmann::json &) const = 0;
  
  virtual bool canParse(const SectionType section) = 0;
  virtual void load(std::istream *is, const SectionType section) = 0;
  
  virtual void setMaxDetections(unsigned int max_detections) = 0;
  virtual uint32_t process(std::vector<PointData>& detections, uint32_t no_detections) = 0;
};

/** @}*/

} /* namespace pp */
} /* namespace dd */
} /* namespace mke */



#endif /* _POSTPROC_H_ */
