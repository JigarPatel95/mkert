/*
 * pp_ema - Exponential moving average post processor
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 */


#ifndef _PP_EMA_H_
#define _PP_EMA_H_

#include "pp/postproc.h"
#include "types/fpreal.h"
#include "util/history.h"

#include <iostream>
#include <cmath>

#ifdef MKEDD_MULTITHREADING
#include "util/threadpool.h"
#endif

namespace mke {
namespace dd {
namespace pp {

/** \addtogroup mkedd
 *  @{
 */

class PpEma : public PostProcessor {
private:
  struct DetectionSlice {
    uint32_t det_min;
    uint32_t det_max;
    uint32_t no_detections;
  };
  
  unsigned int max_detections_;
  static const int max_epochs_ = 10;
  
  int history_length_;
  int no_threads_;
  mke::dd::types::fpreal16 alpha_;
  std::vector<DetectionSlice> detslices_;
  
#ifdef MKEDD_MULTITHREADING
  util::ThreadPool<util::SHARED_THREADPOOL> threadpool_;
#endif    
  
  struct HistoryInfo {
    int valid_epochs;
  };
  
  util::History<PointData, HistoryInfo, 2> history_;
  
  void clearHistory(void)
  {
    for (unsigned int i = 0; i < history_.size(); i++)
    {
      history_.info(i).valid_epochs = 0;      
      history_.at(0, 0).confidence = 0;
    }
    history_++;
  }

  void setupDetectionSlices(void)
  {
    detslices_.resize(no_threads_);
    uint32_t step = std::round(double(max_detections_) / double(no_threads_));
    uint32_t delta = 0;
    uint32_t counter = 0;
    
    for (int i = 0; i < no_threads_; i++)
      {
        delta = ((i + 1) == no_threads_) ? (max_detections_ - counter) : step;
    
        detslices_[i].det_min = counter;
        detslices_[i].det_max = counter + delta;
        detslices_[i].no_detections = 0;

        counter += step;
      }   
  }
  
  void setInitValues(void)
  {
#ifdef MKEDD_MULTITHREADING
    no_threads_ = 4;
    threadpool_.setNoThreads(no_threads_);
#else
    no_threads_ = 1;
#endif     
  
    setupDetectionSlices();
    history_length_ = 1;
    alpha_ = mke::dd::types::fpreal16(0.5f);
    history_.resize(max_detections_);
    clearHistory();
  }
  
public:
  
  // ==========================================================================
  // Construction 
  
  PpEma() : max_detections_(0)
  {
    setInitValues();
  }
  
  ~PpEma() 
    {}  
  
  // ==========================================================================
  // Static methods
  
  static const std::string staticGetTypeString(void)
  {
    static const std::string typeString = "EMA";
    return typeString;
  }
  
  static PostProcessor* create(void)
  {
    return new PpEma();
  }
  
  // ==========================================================================
  // Parameters
  
  inline bool isValidHistoryLength(const int &history_length)
  {
    return (history_length < 1) || (history_length > max_epochs_) ? false : true;
  } 

  inline bool isValidAlpha(const double &alpha)
  {
    return (alpha > 1) || (alpha <= 0) ? false : true;
  }
  
#ifdef MKEDD_MULTITHREADING
  inline bool isValidNoThreads(const uint32_t &no_threads)
  {
    return (no_threads < 1) || (no_threads > 4) ? false : true;
  }
#endif

  void validateAndSetParams(const nlohmann::json &params, bool validate_only)
  {
    if (params.find("alpha") != params.end())
      {
        double alpha = params["alpha"].get<double>();

        if (!isValidAlpha(alpha))
          throw std::runtime_error("EMA: alpha value out of bounds: " + std::to_string(alpha));
        if (!validate_only)
          alpha_ = mke::dd::types::fpreal16(alpha);
      }    
    
    if (params.find("history") != params.end())
      {
        int history_length = params["history"].get<double>();
        if (!isValidHistoryLength(history_length))

          throw std::runtime_error("EMA: history out of bounds: " + std::to_string(history_length));
        if (!validate_only)
          history_length_ = history_length;
      }
     
#ifdef MKEDD_MULTITHREADING
  if (params.find("no_threads") != params.end())
    {
      uint32_t no_threads = params["no_threads"].get<double>();
      if (!isValidNoThreads(no_threads))
        throw std::runtime_error("EMA: no_threads out of bounds: " + std::to_string(no_threads));
      if (!validate_only)
        {
          no_threads_ = no_threads;
          threadpool_.setNoThreads(no_threads_);
          setupDetectionSlices();
        }
    }
#endif      
  }  
  
  // ==========================================================================
  // Parameters 
      
  void validateParams(const nlohmann::json &params)
  {
    validateAndSetParams(params, true);
  }

  nlohmann::json getParamsSchema(void) const
  {
    nlohmann::json schema = R"SCHEMA(
    {
      "type" : "object",
      "additionalProperties" : false,
      "properties" : {
        "type" : {
          "description" : "Postprocessor type",
          "type" : "string",
          "enum" : ["EMA"],
          "default" : "EMA"
        },
        "history" : {
          "type" : "number",
          "description" : "History length",
          "minimum" : 1,
          "maximum" : 5,
          "multipleOf" : 1
        },
        "alpha" : {
          "type" : "number",
          "description" : "Blending parameter"
          "minimum" : 0,
          "maximum" : 1
        },
        "no_threads" : {
          "description" : "Number of threads to use for computation",
          "minimum" : 1,
          "maximum" : 4,
          "multipleOf" : 1
        }
      }
    }
    )SCHEMA"_json;

   return schema;
  }

  void setParams(const nlohmann::json &params)
  {
     validateAndSetParams(params, false);
  }

  void getParams(nlohmann::json &params) const
  {
    params["history"] = history_length_;
    params["alpha"] = double(alpha_);
#ifdef MKEDD_MULTITHREADING
    params["no_threads"] = no_threads_;
#endif
  }

  const std::string getTypeString(void)
  {
    return staticGetTypeString();
  }    
     
  bool canParse(const SectionType section)
  {
    return (section == PP_SECTION_EMPTY) ? true : false;
  }
  
  void load(std::istream *is, const SectionType section) 
    {}    
    
  void setMaxDetections(unsigned int max_detections)
  {
    max_detections_ = max_detections;
    history_.resize(max_detections_);
    clearHistory();
    setupDetectionSlices();
  }
  
  // ==========================================================================
  // Runtime statistics
  
  void resetStats(void)
  {
  }

  void getStats(nlohmann::json &params) const
  {
  }
  
  // ==========================================================================
  // Processing  
  
  bool getPrediction(const uint32_t det_idx, std::vector<PointData> &detections, PointData &prediction)
  {
    int &valid_epochs = history_.info(det_idx).valid_epochs;
    
    if (detections[det_idx].confidence > 0)
      {
        prediction = detections[det_idx];
        valid_epochs++;
        valid_epochs = std::min(valid_epochs, history_length_);
        return true;
      }
    else if (valid_epochs)
      {
        prediction = history_.prev(det_idx);  
        valid_epochs--;
        valid_epochs = std::max(valid_epochs, 0);
        return true;
      }
    
    return false;
  }
  
  void updatePrediction(const uint32_t det_idx, int valid_epochs, PointData &prediction)
  {
    if (valid_epochs)
      {
        mke::dd::types::fpreal16 updated_x = alpha_ * mke::dd::types::fpreal16(prediction.x);
        mke::dd::types::fpreal16 updated_y = alpha_ * mke::dd::types::fpreal16(prediction.y);
        mke::dd::types::fpreal16 updated_z = alpha_ * mke::dd::types::fpreal16(prediction.z);

        PointData &d = history_.prev(det_idx);
        updated_x += (mke::dd::types::fpreal16(1) - alpha_) * mke::dd::types::fpreal16(d.x);
        updated_y += (mke::dd::types::fpreal16(1) - alpha_) * mke::dd::types::fpreal16(d.y);
        updated_z += (mke::dd::types::fpreal16(1) - alpha_) * mke::dd::types::fpreal16(d.z);
          
        prediction.x = int(updated_x);
        prediction.y = int(updated_y);
        prediction.z = int(updated_z);
      }
  }
  
  void processDetectionSlice(std::vector<PointData> &detections, const unsigned int ds_idx)
  {
    uint32_t &det_min = detslices_[ds_idx].det_min;
    uint32_t &det_max = detslices_[ds_idx].det_max;
    uint32_t &no_dets = detslices_[ds_idx].no_detections;
    
    no_dets = 0;
    
    for (unsigned int i = det_min; i < det_max; i++)
      {
        int valid_epochs = history_.info(i).valid_epochs;
        
        // Can we make a prediction?
        if (!getPrediction(i, detections,  history_.curr(i)))
          continue;

        // Update prediction
        updatePrediction(i, valid_epochs,  history_.curr(i));
        detections[i] = history_.curr(i);
        
        no_dets++;      
      }    
  }
  
  uint32_t process(std::vector<PointData> &detections, uint32_t no_detections)
  {
#ifdef MKEDD_MULTITHREADING
  //  thread_pool_.setCounter(detslices_.size());
  
    for (unsigned int i = 0; i < detslices_.size(); i++)
      threadpool_.process([this, &detections, i](){this->processDetectionSlice(detections, i); return true;});
  
    threadpool_.wait();
#else
    for (unsigned int i = 0; i < detslices_.size(); i++)
      processDetectionSlice(detections, i);
#endif
  
    uint32_t no_dets = 0;
    for (unsigned int i = 0; i < detslices_.size(); i++)
      no_dets += detslices_[i].no_detections;  
 
    // Increment history epoch
    history_++;
     
    return no_dets;
  }

};

/** @}*/

} /* namespace pp */
} /* namespace dd */
} /* namespace mke */




#endif /* _PP_EMA_H_ */
