#include <iostream>
#include <fstream>
#include <stdexcept>
#include <string>

#include <unistd.h>

#include "mkedd.h"

#include "CImg.h"
namespace ci = cimg_library;

int main(int argc, char *argv[]) 
{
  mke::dd::DepthSensor dsensor;

  char *cal_fname = NULL;
  char *out_fname = NULL;
  char *im_fname = NULL;
  char *profile = NULL;
  int det_idx = 0;
  int c;
  opterr = 0;
    
  try 
    {
    
     while ((c = getopt (argc, argv, "c:i:o:p:d:f:")) != -1)
     switch (c)
      {  
      case 'c':
        cal_fname = optarg;
        break;
      case 'i':
        im_fname = optarg;
        break;
      case 'o':
        out_fname = optarg;
         break;
      case 'p':
        profile = optarg;
        break;
      case 'd':
        det_idx = atoi(optarg) - 1;
        break;
      case '?':
        if ((optopt == 'c') || (optopt == 'i') || (optopt == 'o') || (optopt == 'p') || 
            (optopt == 'd') || (optopt == 'f'))
          fprintf (stderr, "Option -%c requires an argument.\n", optopt);
        else if (isprint (optopt))
          fprintf (stderr, "Unknown option `-%c'.\n", optopt);
        else
          fprintf (stderr, "Unknown option character `\\x%x'.\n", optopt);
        return 1;        
        
      default:
        abort();
      }   
    
    if (!cal_fname)
      throw std::runtime_error("Calib file not set (-c)");

    if (!im_fname)
      throw std::runtime_error("Image file not set (-i)");

    if (!profile)
      throw std::runtime_error("Profile not set (-p)");

    if (!out_fname)
      throw std::runtime_error("Output file not set (-o)");
    
    // Load image data 
    std::cout << "Loading image data: " << im_fname << std::endl;
    ci::CImg<uint8_t> img;
    img.load(im_fname);
    std::cout << "  Image size: [" << img.width() << "x" << img.height() << "x" << img.depth() << "]" << std::endl;     
    
    // Add calibration file name to a list
    std::cout << "Setting calibration file: " << cal_fname << std::endl;
    dsensor.addCalibFile(cal_fname);
    
    // Set profile (triggers calibration file load)
    std::cout << "Setting profile: " << profile << std::endl;
    dsensor.setProfile(profile);

    if (dsensor.getNoDetectors() < det_idx + 1)
      throw std::runtime_error("Number of available detector is lower than requested det_idx");

    // Process data
    std::cout << "Processing data... ";
    //for (int i = 0; i < 1000; i++)
    dsensor.process(det_idx, img.data());
    
    std::cout << "done"<< std::endl;
    
    // Save results
    std::cout << "Saving results: "<< out_fname <<  std::endl;
    std::ofstream file_stream(out_fname, std::ofstream::binary);
    
    const mke::dd::PointData* detections;
    uint32_t no_detections;
    dsensor.getDetections(det_idx, detections, no_detections);
     std::cout << "Number of detections: " << no_detections << std::endl;

    for (unsigned int i = 0; i < no_detections; i++)
      {
        if (detections[i].confidence)
          {
            file_stream << i << " " << detections[i].u << " " << detections[i].v << " " << detections[i].sigma3D << " " <<
                           detections[i].x << " " << detections[i].y << " " << detections[i].z << " " <<
                           detections[i].uid << " " << detections[i].id1 << " " << detections[i].id2 << " " <<
                           detections[i].sigma2D << " " << detections[i].confidence << std::endl;
          }
      }
    file_stream.close();
  }
  catch(std::exception& e)
    { 
      std::cerr << "FATAL ERROR: " << e.what() << std::endl;
      return 1;
    }
}
