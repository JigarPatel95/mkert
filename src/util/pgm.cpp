/*
 * RasPiEngine - specific engine for Raspberry PI
 *    controls all devices (camera, lasers) and sync them
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar, Jan Heller
 *
 */

/* -------------------------------------------------------------------------- */

#include "pgm.h"
#include <cstdio>
#include <cstring>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <exception>

/* -------------------------------------------------------------------------- */

using namespace mke;
using namespace mke::util;
using namespace mke::dev;

/* -------------------------------------------------------------------------- */

#define PGM_HEADER_FMT "P5 %d %d 255\n"

/* -------------------------------------------------------------------------- */

int PGM::fillHeader(int w, int h, char * buff)

{
  return std::sprintf(buff, PGM_HEADER_FMT, w, h);
}

/* -------------------------------------------------------------------------- */

size_t PGM::getHeaderSize(int w, int h)

{
  char buff[32];
  
  return std::sprintf(buff, PGM_HEADER_FMT, w, h);
}

/* -------------------------------------------------------------------------- */

size_t PGM::getBufferSize(int w, int h)
{
  return PGM::getHeaderSize(w, h) + w*h;
}

/* -------------------------------------------------------------------------- */

size_t PGM::encode(const Image<Pix8 > & src, void * dst)

{
  char * p = reinterpret_cast<char *>(dst);
  int offset = PGM::fillHeader(src.w, src.h, p);
  p += offset;
  
  for(int r = 0; r < src.h; ++r, p += src.w)
    std::memcpy(p, src.row(r), src.w);
  
  return p-reinterpret_cast<char *>(dst);
}
  

/* -------------------------------------------------------------------------- */

void PGM::write(const Image<Pix8> & im, const std::string & path)

{

  std::ofstream output(path, std::ios::binary);
  
  char buff[32];
  int len = PGM::fillHeader(im.w, im.h, buff);
  output.write(buff, len);
  
  for(int r = 0; r < im.h; ++r)
    {
      const char * d = reinterpret_cast<const char *>(im.row(r));
      
      output.write(d, im.w);
    }
  
  output.close();  
}

/* -------------------------------------------------------------------------- */

// read element

std::string readElement(std::ifstream & in, int limit)
{
  char c;
  in.read(&c, 1);
  std::string buff;

  // skip spaces
  
  while(c == ' ' || c == '\t' || c == '\n' || c == '\r')
    in.read(&c, 1);
  
  // skip comment if presets
  
  if(c == '#')
    {
      while(c != '\n' && c != '\r')
        in.read(&c, 1);
      
      return readElement(in, limit);
    }

  // read element
  while(c != ' ' && c != '\t' && c != '\n' && c != '\r' && limit > 0)
    {
      buff += c;
      in.read(&c, 1);
      limit--;
    }
  
  if(limit < 0)
    throw std::runtime_error("Invalid file format");
  
  return buff;
}

/* -------------------------------------------------------------------------- */

void PGM::read(const std::string & path, MemImage<mke::dev::Pix8> & out, int stride)

{
  std::ifstream input(path, std::ios::binary);

  if(readElement(input,2) != "P5")
    throw std::runtime_error("Unsupported type");
  
  int w = std::atoi(readElement(input, 4).c_str());
  int h = std::atoi(readElement(input, 4).c_str());
  readElement(input, 4);
  
  out.reallocate(w, h, stride > w ? stride : w);
  
  for(int r = 0; r < h; ++r)
    {
      input.read((char *) out.row(r), w);
      if(!input)
        throw std::runtime_error("Corrupted image file.");
    }
  input.close();  
}

/* -------------------------------------------------------------------------- */
  