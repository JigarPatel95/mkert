/*
 * WorkerModule - module that is possible to serve API calls and process images
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef _IMAVG_H_
#define _IMAVG_H_

/* -------------------------------------------------------------------------- */

#include <cstring>
#include "dev/img.h"

/* -------------------------------------------------------------------------- */

namespace mke {
namespace util {

/* -------------------------------------------------------------------------- */

template <typename PixIn, typename SumType>
class ImageAverager
{
protected:
  dev::MemImage<SumType>    sums_;      // computed sum
  int                       cnt_;       // number of summed images
  
  /**
   * @brief initialize internal structures
   */
  void init(int w, int h)
  {
    sums_.reallocate(w, h, w);
    
    std::memset(reinterpret_cast<void *>(sums_.mem), 0, 
                  sizeof(SumType)*sums_.stride*sums_.h);
  }
  
public:
  /**
   * @brief constructor
   */
  ImageAverager() :
    cnt_(0)
  {};
  
  /**
   * @brief get width
   */
  inline int getW() { return sums_.w; };
  
  /**
   * @brief get width
   */
  inline int getH() { return sums_.h; };
  
  inline int getCnt() { return cnt_; };
  
  /**
   * @brief add image into averager
   */
  void add(const dev::Image<PixIn> & im)
  {
    // first image ?
    
    if(sums_.w == 0)
      init(im.w, im.h);
    
    // check dimensions
    
    assert(sums_.w == im.w && sums_.h == im.h);
    
    // add to sums
    
    for(int r = 0; r < sums_.h; ++r)
      {
        const PixIn * p     = im.row(r);
        const PixIn * pE    = p + im.w;
        SumType     * s     = sums_.row(r);
        
        for(; p != pE; ++p, ++s)
          *s += *p;
      }
    
    cnt_++;
  }

  /**
   * @brief return averaged image
   */
  template <typename OutType>
  void avg(dev::Image<OutType> & out)
  {
    // check output dimensions
    
    assert(out.w == sums_.w && out.h == sums_.h);
    
    // write out average
    
    for(int r = 0; r < sums_.h; ++r)
      {
        PixIn           * p     = out.row(r);
        const PixIn     * pE    = p + out.w;
        const SumType   * s     = sums_.row(r);
        
        for(; p != pE; ++p, ++s)
          *p = *s/cnt_;
      }
  }
};

/* -------------------------------------------------------------------------- */

} // end of namespace mke::util
} // end of namespace mke

/* -------------------------------------------------------------------------- */

#endif // _IMAVG_H_

/* -------------------------------------------------------------------------- */
