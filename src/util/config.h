/* 
 * ConfigService
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#ifndef _CONFIG_H_
#define _CONFIG_H_

#include <string>
#include <vector>
#include <fstream>
#include <algorithm>
#include <iostream>
#include <stdexcept>

#include <boost/algorithm/string.hpp>
#include <boost/program_options.hpp>

namespace po = boost::program_options;

#include "logging.h"

namespace mke {
namespace util {
  
class ConfigService {

protected:
  typedef enum {
    GENERAL = 0,
    CMD_ONLY = 1,
    CFG_ONLY = 2
  } OptionType;


  // list of options
  std::vector<bool*>        bool_opts;
  std::vector<OptionType>   bool_opts_type;
  std::vector<std::string>  bool_opts_name;
  std::vector<std::string>  bool_opts_info;
  std::vector<bool>         bool_opts_ini;

  std::vector<int*>         int_opts;
  std::vector<OptionType>   int_opts_type;
  std::vector<std::string>  int_opts_name;
  std::vector<std::string>  int_opts_info;
  std::vector<int>          int_opts_ini;

  std::vector<std::string*> string_opts;
  std::vector<OptionType>   string_opts_type;
  std::vector<std::string>  string_opts_name;
  std::vector<std::string>  string_opts_info;
  std::vector<std::string>  string_opts_ini;

  std::vector<std::vector<int>*>   vint_opts;
  std::vector<OptionType>   vint_opts_type;
  std::vector<std::string>  vint_opts_name;
  std::vector<std::string>  vint_opts_info;
  std::vector<std::string>  vint_opts_ini;
  std::vector<std::pair<int,int> > vint_opts_bnds;

  std::vector<std::vector<std::string>*>   vstr_opts;
  std::vector<OptionType>   vstr_opts_type;
  std::vector<std::string>  vstr_opts_name;
  std::vector<std::string>  vstr_opts_info;
  std::vector<std::string>  vstr_opts_ini;

  // boost options
  po::options_description cmdline_options;
  po::options_description cfgfile_options;
  po::variables_map       vm;

  // Helper methods
  void static str2vint(const std::string &str, std::vector<int> * const vint) {
    std::vector<std::string> fields;

    if (str == "")
      {
        vint->clear();
        return;
      }

    boost::split(fields, str, boost::is_any_of( "," ), boost::token_compress_on);

    vint->clear();
    for (int i = 0; i < (int) fields.size(); i++)
      if (fields[i].length() > 0)
        vint->push_back(boost::lexical_cast<int>(fields[i].c_str()));
  }
  
  void static str2vstr(const std::string &str, std::vector<std::string> * const vstr) {
    std::vector<std::string> fields;

    if (str == "")
      {
        vstr->clear();
        return;
      }

    boost::split(fields, str, boost::is_any_of( "," ), boost::token_compress_on);

    vstr->clear();
    for (int i = 0; i < (int) fields.size(); i++)
      if (fields[i].length() > 0)
        vstr->push_back(fields[i].c_str());
  }

  void registerBln(bool &bopt, const OptionType &type, const std::string &name, const std::string &info, bool inivalue) {
    bool_opts.push_back(&bopt);
    bool_opts_type.push_back(type);
    bool_opts_name.push_back(name);
    bool_opts_info.push_back(info);
    bool_opts_ini.push_back(inivalue);

    bopt = inivalue;
  };

  void registerInt(int &iopt, const OptionType &type, const std::string &name, const std::string &info, int inivalue) {
    int_opts.push_back(&iopt);
    int_opts_type.push_back(type);
    int_opts_name.push_back(name);
    int_opts_info.push_back(info);
    int_opts_ini.push_back(inivalue);

    iopt = inivalue;
  };

  void registerStr(std::string &sopt, const OptionType &type, const std::string &name, const std::string &info, const std::string &inivalue) {
    string_opts.push_back(&sopt);
    string_opts_type.push_back(type);
    string_opts_name.push_back(name);
    string_opts_info.push_back(info);
    string_opts_ini.push_back(inivalue);

    sopt = inivalue;
  };

  void registerVnt(std::vector<int> &viopt, const OptionType &type, const std::string &name, const std::string &info, const int mins, const int maxs, const std::string &inivalue) {
    vint_opts.push_back(&viopt);
    vint_opts_type.push_back(type);
    vint_opts_name.push_back(name);
    vint_opts_info.push_back(info);
    vint_opts_ini.push_back(inivalue);
    vint_opts_bnds.push_back(std::pair<int,int>(mins, maxs));

    str2vint(inivalue, &viopt);
    if (((mins > 0) && ((int)viopt.size() < mins)) || ((maxs > 0) && ((int)viopt.size() > maxs)))
      LOG_FATAL << "Initial std::vector length out of bounds: " << viopt.size() << " [" << mins << ", " << maxs << "]";
  };
  
  void registerVstr(std::vector<std::string> &vsopt, const OptionType &type, const std::string &name, const std::string &info, const std::string &inivalue) {
    vstr_opts.push_back(&vsopt);
    vstr_opts_type.push_back(type);
    vstr_opts_name.push_back(name);
    vstr_opts_info.push_back(info);
    vstr_opts_ini.push_back(inivalue);

    str2vstr(inivalue, &vsopt);
  };

  void updateConfig(void) {
    po::options_description opts_shared("Options");
    po::options_description opts_cmd("Command line options only");
    po::options_description opts_cfg("Config file options only");
    po::options_description *opts[] = {&opts_shared, &opts_cmd, &opts_cfg};

    for (int i = 0; i < (int) bool_opts.size(); i++)
      {
        opts[bool_opts_type[i]]->add_options()
            (bool_opts_name[i].c_str(),
             po::value<bool>()->implicit_value(true),
             bool_opts_info[i].c_str())
        ;
      }

    for (int i = 0; i < (int) int_opts.size(); i++)
      {
        opts[int_opts_type[i]]->add_options()
            (int_opts_name[i].c_str(),
             po::value<int>(int_opts[i])->default_value(int_opts_ini[i]),
             int_opts_info[i].c_str())
        ;
      }

    for (int i = 0; i < (int) string_opts.size(); i++)
      {
        opts[string_opts_type[i]]->add_options()
            (string_opts_name[i].c_str(),
             po::value<std::string>(string_opts[i])->default_value(string_opts_ini[i].c_str()),
             string_opts_info[i].c_str())
        ;
      }

    for (int i = 0; i < (int) vint_opts.size(); i++)
      {
        opts[vint_opts_type[i]]->add_options()
            (vint_opts_name[i].c_str(),
             po::value<std::string>()->default_value(vint_opts_ini[i].c_str()),
             vint_opts_info[i].c_str())
        ;
      }

    for (int i = 0; i < (int) vstr_opts.size(); i++)
      {
        opts[vstr_opts_type[i]]->add_options()
            (vstr_opts_name[i].c_str(),
             po::value<std::string>()->default_value(vstr_opts_ini[i].c_str()),
             vstr_opts_info[i].c_str())
        ;
      }

    cmdline_options.add(opts_cmd).add(opts_shared);
    cfgfile_options.add(opts_cfg).add(opts_shared);
  }

  void storeConfig(void) {
    // Store options that are not parsed automatically by boost
    for (int i = 0; i < (int) bool_opts.size(); i++)
      {
        if (vm.count(bool_opts_name[i]))
          {
            if (vm[bool_opts_name[i]].as<bool>())
              *(bool_opts[i]) = true;
            else
              *(bool_opts[i]) = false;
          }
      }

    for (int i = 0; i < (int) vint_opts.size(); i++)
      {
        if (vm.count(vint_opts_name[i]))
          {
            str2vint(vm[vint_opts_name[i]].as<std::string>(), vint_opts[i]);
            int mins = vint_opts_bnds[i].first;
            int maxs = vint_opts_bnds[i].second;
            if (((mins > 0) && ((int)vint_opts[i]->size() < mins)) || ((maxs > 0) && ((int)vint_opts[i]->size() > maxs)))
              LOG_FATAL << "Initial std::vector length out of bounds: " << vint_opts[i]->size() << " [" << mins << ", " << maxs << "]";
          }
      }

    for (int i = 0; i < (int) vstr_opts.size(); i++)
      {
        if (vm.count(vstr_opts_name[i]))
          str2vstr(vm[vstr_opts_name[i]].as<std::string>(), vstr_opts[i]);
      }      
  }

public:
  ConfigService() { }

  void parseFromCfgFile(const std::string &cfg_file) {
    try
      {
        std::ifstream ifs(cfg_file.c_str());
        if (!ifs)
          {
            std::ostringstream oss;
            oss << "Cannot open configuration file: " << cfg_file;
            throw std::runtime_error(oss.str());
          }
          

        po::store(parse_config_file(ifs, cfgfile_options), vm);
        po::notify(vm);

        storeConfig();
      }
    catch(std::exception &)
      {
        throw;
      }
    catch(...)
      {
        throw std::runtime_error("Exception of unknown type while parsing command line / config file");
      }
  }

  void parseFromCmdLine(const int ac, const char * const av[]) {

    po::store(po::parse_command_line(ac, av, cmdline_options), vm);
    po::notify(vm);

    storeConfig();    
  }
  
  void getCommandLineHelp(std::ostream &fout) {
    fout << cmdline_options;
  }
};

} // namespace util
} // namespace mke


#endif // _CONFIG_H_
