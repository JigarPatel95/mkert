/* ------------------------------------------------------------------------- */

#pragma once

/* ------------------------------------------------------------------------- */

#include <chrono>

/* ------------------------------------------------------------------------- */

namespace mke {
namespace util {

/* ------------------------------------------------------------------------- */

// Counts FPS every 10s
class FpsCounter 
{
    double          interval_;
    bool            autolog_;
    std::chrono::high_resolution_clock::time_point
                    last_stamp_;
    int             cnt_;
    double          last_fps_;
    
  
public:
    FpsCounter(double interval, bool autolog = false);
    
    void tick();
    
    double getFps();
};

/* ------------------------------------------------------------------------- */

} // end of mke::util namespace
} // end of mke namespace

/* ------------------------------------------------------------------------- */
