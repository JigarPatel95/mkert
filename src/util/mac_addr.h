/*
 * List of available devices and their MAC adresses
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller
 *
 */

#ifndef _MAC_ADDR_H_
#define _MAC_ADDR_H_

#ifndef __MINGW32__
#include <sys/ioctl.h>
#include <net/if.h> 
#include <unistd.h>
#include <netinet/in.h>
#endif
#include <string.h>
#include <stdio.h>      


#include <map>

namespace mke {
namespace util {

class MacAddresses {
public:
  
  static void get(std::map<std::string, std::string> &addrs)
  {
#ifndef __MINGW32__
    struct ifreq ifr;
    struct ifconf ifc;
    char buf[1024];

    addrs.clear();

    int sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
    if (sock == -1) 
      return;

    ifc.ifc_len = sizeof(buf);
    ifc.ifc_buf = buf;
    if (ioctl(sock, SIOCGIFCONF, &ifc) == -1)
      return;

    struct ifreq* it = ifc.ifc_req;
    const struct ifreq* const end = it + (ifc.ifc_len / sizeof(struct ifreq));

    for (; it != end; ++it) 
      {
        strcpy(ifr.ifr_name, it->ifr_name);
        if (ioctl(sock, SIOCGIFFLAGS, &ifr) == 0) 
          {
            if (! (ifr.ifr_flags & IFF_LOOPBACK))
              {
                if (ioctl(sock, SIOCGIFHWADDR, &ifr) == 0) 
                  {
                    std::string dev_name;
                    dev_name.assign(it->ifr_name);
                    
                    std::string addr;  
                    char byte[3];
                    for (int j = 0; j < 6; j++)
                      {
                        sprintf(byte, "%02x", (unsigned char) ifr.ifr_addr.sa_data[j]);
                        addr += byte;
                      }

                    addrs[dev_name] = addr;
                  }
              }
          }
      }
#endif
  }

};

} // namespace mke::util
} // namespace mke

#endif
