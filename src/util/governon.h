/*
 * Governon - checks if sensor can run
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar, Jan Heller
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef _GOVERNON_H_
#define _GOVERNON_H_

/* -------------------------------------------------------------------------- */

#include "util/timers.h"

/* -------------------------------------------------------------------------- */

namespace mke {
namespace util {

/* -------------------------------------------------------------------------- */

class Governon 
{
protected:
  const int max_on_;
  const int min_off_;
  const double off_factor_;
  bool applied_;
  bool is_on_;
  
  WallClockTimer<double, std::ratio<1>> timer_;
  double last_off_;
  
public:
  Governon(bool is_on = false, int max_on = -1, int min_off = 0, double off_factor = 0)
  : max_on_(max_on), min_off_(min_off), off_factor_(off_factor), 
  applied_(max_on_ >= 0 || min_off_ > 0 || off_factor > 0),
  is_on_(is_on), last_off_(max_on)
  {
    timer_.start();    
  };
  
  bool canRun() const;
  
  float countdown() const;
  
  void setOn();
  
  void setOff();
  
  int maxOnLimit() const { return max_on_; }
};

/* -------------------------------------------------------------------------- */

} // namespace mke::util
} // namespace mke

/* -------------------------------------------------------------------------- */

#endif // GOVERNON_H

/* -------------------------------------------------------------------------- */
