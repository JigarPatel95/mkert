/* 
 * SimpleLogger
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#ifndef LOGGING_H_
#define LOGGING_H_

#include <sstream>
#include <string>
#include <thread>
#include "boost/date_time/posix_time/posix_time.hpp"

#ifdef USE_SYSLOG
  #include <syslog.h>
#endif

namespace mke {
namespace util {

namespace pt = boost::posix_time;

class SimpleLogger {
public:
  typedef enum {
    INFO,
    WARNING,
    FATAL
  } LogLevel;

  static SimpleLogger& getInstance() {
    static SimpleLogger instance;
    return instance;
  }

  static void setLevel(const LogLevel _vlevel) {
    getInstance().vlevel = _vlevel;
  }

  static bool isEnabled(const LogLevel _vlevel) {
    return (getInstance().vlevel <= _vlevel);
  }

  static void openLogger() {
#ifdef USE_SYSLOG
    openlog(NULL, LOG_CONS | LOG_PID, LOG_DAEMON);
#endif
  }

  static void closeLogger() {
#ifdef USE_SYSLOG
    closelog();
#endif
  }

#ifdef NDEBUG
  static std::string getHeader(const LogLevel _vlevel, const char * const time) {
    std::stringstream ss;

    ss << "[";
    if (_vlevel == WARNING)
      ss << "WARNING ";
    else if (_vlevel == FATAL)
      ss << "FATAL ";
    ss << time << ", " << std::this_thread::get_id() << "] ";

    return ss.str();
  }
#else
  static std::string getHeader(const LogLevel _vlevel, const char * const time, const char * const file, const int line) {
    std::stringstream ss;

    ss << "[";
    if (_vlevel == WARNING)
      ss << "WARNING ";
    else if (_vlevel == FATAL)
      ss << "FATAL ";
    ss << time << ", " << std::this_thread::get_id() << " - " << file << ":" << line << "] ";

    return ss.str();
  }
#endif

  static void log(const std::string s, const LogLevel _vlevel) {

    if (_vlevel != SimpleLogger::FATAL)
      {
        #pragma omp critical(logging)
        std::cout << s << std::endl;
      }
    else
      throw std::runtime_error(s);
  }

private:
  LogLevel vlevel;

  SimpleLogger() {vlevel = WARNING;};
  SimpleLogger(SimpleLogger const&);
  void operator=(SimpleLogger const&);
};

class ScopedLogger {
public:

  ScopedLogger(const SimpleLogger::LogLevel _vlevel, const std::string s) 
  : vlevel(_vlevel) , header_(s)
  {};

  std::stringstream& stream() {
    return ss;
  }

  ~ScopedLogger() {
#ifdef USE_SYSLOG
    static int map[] = { LOG_INFO, LOG_WARNING, LOG_ERR };
    syslog(map[vlevel], "%s", ss.str().c_str());
#endif

    SimpleLogger::getInstance().log(header_ + ss.str(), vlevel);
  }

private:
  SimpleLogger::LogLevel vlevel;
  std::stringstream ss;
  std::string header_;  
};

#ifdef USE_SYSLOG
  #undef LOG_INFO
#endif

#ifdef NDEBUG
#define LOG_INFO mke::util::SimpleLogger::getInstance().isEnabled(mke::util::SimpleLogger::INFO) && \
                    mke::util::ScopedLogger(mke::util::SimpleLogger::INFO, mke::util::SimpleLogger::getInstance().getHeader( \
                    mke::util::SimpleLogger::INFO, \
                    boost::posix_time::to_simple_string(boost::posix_time::microsec_clock::local_time()).c_str())).stream()
#define LOG_WARN mke::util::SimpleLogger::getInstance().isEnabled(mke::util::SimpleLogger::WARNING) && \
                    mke::util::ScopedLogger(mke::util::SimpleLogger::WARNING, \
                    mke::util::SimpleLogger::getInstance().getHeader(mke::util::SimpleLogger::WARNING, boost::posix_time::to_simple_string(boost::posix_time::microsec_clock::local_time()).c_str())).stream()
#define LOG_FATAL mke::util::SimpleLogger::getInstance().isEnabled(mke::util::SimpleLogger::FATAL) && \
                    mke::util::ScopedLogger(mke::util::SimpleLogger::FATAL, mke::util::SimpleLogger::getInstance().getHeader( \
                    mke::util::SimpleLogger::FATAL, \
                    boost::posix_time::to_simple_string(boost::posix_time::microsec_clock::local_time()).c_str())).stream()
#else  /* NDEBUG */
#define LOG_INFO mke::util::SimpleLogger::getInstance().isEnabled(mke::util::SimpleLogger::INFO) && \
        mke::util::ScopedLogger(mke::util::SimpleLogger::INFO, mke::util::SimpleLogger::getInstance().getHeader(mke::util::SimpleLogger::INFO, boost::posix_time::to_simple_string(boost::posix_time::microsec_clock::local_time()).c_str(), __FILE__, __LINE__)).stream()
#define LOG_WARN mke::util::SimpleLogger::getInstance().isEnabled(mke::util::SimpleLogger::WARNING) && \
        mke::util::ScopedLogger(mke::util::SimpleLogger::WARNING, mke::util::SimpleLogger::getInstance().getHeader(mke::util::SimpleLogger::WARNING, boost::posix_time::to_simple_string(boost::posix_time::microsec_clock::local_time()).c_str(), __FILE__, __LINE__)).stream()
#define LOG_FATAL mke::util::SimpleLogger::getInstance().isEnabled(mke::util::SimpleLogger::FATAL) && \
        mke::util::ScopedLogger(mke::util::SimpleLogger::FATAL, mke::util::SimpleLogger::getInstance().getHeader(mke::util::SimpleLogger::FATAL, boost::posix_time::to_simple_string(boost::posix_time::microsec_clock::local_time()).c_str(), __FILE__, __LINE__)).stream()
#endif /* NDEBUG */
        
} // namespace util
} // namespace mke

#endif /* LOGGING_H_ */
