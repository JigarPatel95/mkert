/* -------------------------------------------------------------------------- */

#pragma once

/* -------------------------------------------------------------------------- */

#include <vector>
#include <png.h>
#include <stdexcept>
#include "dev/img.h"
#include <iostream>

/* -------------------------------------------------------------------------- */

namespace mke {
namespace util {

/* -------------------------------------------------------------------------- */

class PNG {

  
  struct data_static_struct {
    char * marker;
    size_t len;
  };

  static void mem_write_data(png_structp png_ptr, png_bytep data, png_size_t length);
  static void mem_flush(png_structp png_ptr);
  
  static void mem_write_data_static(png_structp png_ptr, png_bytep data, png_size_t length);
  static void mem_read_data_static(png_structp png_ptr, png_bytep data, png_size_t length);

public:

  // compression should be between 0 and 9. -1 denotes default value
  
  template <typename PixType>
  static void encode(int w, int h, int stride, const PixType * data, 
                std::vector<char> & out, int compression = -1)
  {
    png_byte color_type = PNG_COLOR_TYPE_GRAY;
    png_byte bit_depth = 8 * sizeof(PixType);

    png_structp png_ptr;
    png_infop info_ptr;

    /* initialize stuff */
    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

    if(!png_ptr)
      throw std::runtime_error("[write_png_file] png_create_write_struct failed");

    info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr)
      throw std::runtime_error("[write_png_file] png_create_info_struct failed");

    if (setjmp(png_jmpbuf(png_ptr)))
      throw std::runtime_error("[write_png_file] Error during init_io");

    png_set_write_fn(png_ptr, reinterpret_cast<void *>(&out), mem_write_data, mem_flush);

    /* write header */
    if (setjmp(png_jmpbuf(png_ptr)))
      throw std::runtime_error("[write_png_file] Error during writing header");

    png_set_IHDR(png_ptr, info_ptr, w, h,
                  bit_depth, color_type, PNG_INTERLACE_NONE,
                  PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

    png_write_info(png_ptr, info_ptr);

    png_set_compression_level(png_ptr, compression);
    
    /* write bytes */
    if (setjmp(png_jmpbuf(png_ptr)))
      throw std::runtime_error("[write_png_file] Error during writing bytes");

    png_bytep p = const_cast<png_bytep>(data);
    for(int r = 0; r < h; ++r, p += stride)
      png_write_row(png_ptr, p);

    /* end write */
    if(setjmp(png_jmpbuf(png_ptr)))
      throw std::runtime_error("[write_png_file] Error during end of write");

    png_write_end(png_ptr, NULL);
    
    // now we should be alredy written bytes
  }
  
  // compression should be between 0 and 9. -1 denotes default value
  
  template <typename PixType>
  static size_t encode(int w, int h, int stride, const PixType * data, 
                     char * buff, size_t buff_size, int compression = -1)
  {
    png_byte color_type = PNG_COLOR_TYPE_GRAY;
    png_byte bit_depth = 8 * sizeof(PixType);

    png_structp png_ptr;
    png_infop info_ptr;

    /* initialize stuff */
    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

    if(!png_ptr)
      throw std::runtime_error("[write_png_file] png_create_write_struct failed");

    info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr)
      throw std::runtime_error("[write_png_file] png_create_info_struct failed");

    if (setjmp(png_jmpbuf(png_ptr)))
      throw std::runtime_error("[write_png_file] Error during init_io");

    data_static_struct marker = {buff, buff_size};
    png_set_write_fn(png_ptr, reinterpret_cast<void *>(&marker), mem_write_data_static, mem_flush);

    /* write header */
    if (setjmp(png_jmpbuf(png_ptr)))
      throw std::runtime_error("[write_png_file] Error during writing header");

    png_set_IHDR(png_ptr, info_ptr, w, h,
                  bit_depth, color_type, PNG_INTERLACE_NONE,
                  PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

    png_set_compression_level(png_ptr, compression);

    png_write_info(png_ptr, info_ptr);
    
    /* write bytes */
    if (setjmp(png_jmpbuf(png_ptr)))
      throw std::runtime_error("[write_png_file] Error during writing bytes");

    png_bytep p = const_cast<png_bytep>(data);
    for(int r = 0; r < h; ++r, p += stride)
      png_write_row(png_ptr, p);

    /* end write */
    if(setjmp(png_jmpbuf(png_ptr)))
      throw std::runtime_error("[write_png_file] Error during end of write");

    png_write_end(png_ptr, NULL);
    
    // now we should be alredy written bytes
    
    return buff_size - marker.len;
  }
  
  // compression should be between 0 and 9. -1 denotes default value
  
  template <typename PixType>
  static size_t decode(const PixType * data, size_t len, std::vector<PixType> & out)
  {
    png_structp png = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if(!png) abort();

    png_infop info = png_create_info_struct(png);
    if(!info) abort();

    if(setjmp(png_jmpbuf(png))) abort();

//    png_init_io(png, fp);
    data_static_struct marker;
    png_set_read_fn(png, &marker, mem_read_data_static);

    png_read_info(png, info);

    int width      = png_get_image_width(png, info);
    int height     = png_get_image_height(png, info);
    int color_type = png_get_color_type(png, info);
    int bit_depth  = png_get_bit_depth(png, info);

    // Read any color_type into 8bit depth, RGBA format.
    // See http://www.libpng.org/pub/png/libpng-manual.txt

    if((bit_depth < 8 && sizeof(PixType) == 1) 
                  || bit_depth != 8*sizeof(PixType))
      png_error(png, "Bad pixel size");

    if(color_type == PNG_COLOR_TYPE_PALETTE)
      png_set_palette_to_rgb(png);

    // PNG_COLOR_TYPE_GRAY_ALPHA is always 8 or 16bit depth.
    if(color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8)
      png_set_expand_gray_1_2_4_to_8(png);

    if(png_get_valid(png, info, PNG_INFO_tRNS))
      png_set_tRNS_to_alpha(png);

    // These color_type don't have an alpha channel then fill it with 0xff.
    if(color_type == PNG_COLOR_TYPE_RGB ||
        color_type == PNG_COLOR_TYPE_GRAY ||
        color_type == PNG_COLOR_TYPE_PALETTE)
      png_set_filler(png, 0xFF, PNG_FILLER_AFTER);

    if(color_type == PNG_COLOR_TYPE_GRAY ||
        color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
      png_set_gray_to_rgb(png);

    png_read_update_info(png, info);

    size_t stride = width*(bit_depth<8?1:(bit_depth/8)); // no padding
    out.resize(height*stride);
    char * im_data = out.data();
    for(int y = 0; y < height; y++, im_data += stride)
      png_read_row(png, im_data, nullptr);
    
    png_read_end(png, info);
  }
  
  // compression should be between 0 and 9. -1 denotes default value
  
  template <typename PixType>
  static void decode(const std::string & filename, mke::dev::MemImage<PixType> & out, int stride = -1)
  {
    FILE * fp = fopen(filename.c_str(), "rb");
    png_structp png = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if(!png) abort();

    png_infop info = png_create_info_struct(png);
    if(!info) abort();

    if(setjmp(png_jmpbuf(png))) abort();

    png_init_io(png, fp);

    png_read_info(png, info);

    int width      = png_get_image_width(png, info);
    int height     = png_get_image_height(png, info);
    int color_type = png_get_color_type(png, info);
    int bit_depth  = png_get_bit_depth(png, info);

    // Read any color_type into 8bit depth, RGBA format.
    // See http://www.libpng.org/pub/png/libpng-manual.txt

    if((bit_depth < 8 && sizeof(PixType) == 1) 
                  || bit_depth != 8*sizeof(PixType))
      png_error(png, "Bad pixel size");

    if(color_type == PNG_COLOR_TYPE_PALETTE)
      png_set_palette_to_rgb(png);

    // PNG_COLOR_TYPE_GRAY_ALPHA is always 8 or 16bit depth.
    if(color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8)
      png_set_expand_gray_1_2_4_to_8(png);

    if(png_get_valid(png, info, PNG_INFO_tRNS))
      png_set_tRNS_to_alpha(png);
/*
    // These color_type don't have an alpha channel then fill it with 0xff.
    if(color_type == PNG_COLOR_TYPE_RGB ||
        color_type == PNG_COLOR_TYPE_GRAY ||
        color_type == PNG_COLOR_TYPE_PALETTE)
      png_set_filler(png, 0xFF, PNG_FILLER_AFTER);
*/
//    if(color_type == PNG_COLOR_TYPE_GRAY ||
//        color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
//      png_set_gray_to_rgb(png);

    png_read_update_info(png, info);

    if(stride < 0)
      stride = width*(bit_depth<8?1:(bit_depth/8)); // no padding

    out.reallocate(width, height, stride);
    for(int y = 0; y < height; y++)
      png_read_row(png, out.row(y), nullptr);
    
    png_read_end(png, info);
    fclose(fp);
  }

};

/* -------------------------------------------------------------------------- */
/*
template <typename PixType>
class PngEncoder {

  png_structp png_ptr_;       // png handle
  png_infop   info_ptr_;      // png info 
  
  // initialize
  void init(const char * filename);
  
public:
  // constructor
  PngEncoder(const char * filename);

  // decode dimensions from file
  void decode_header(int & w, int & h);
  
  // read row from file - out should be allocated at least w * pixsize of memory
  void decode_row(const char * out);
  
  // out should be allocated stride * height of image
  void decode(const char * out, size_t stride);
    
  virtual ~PngEncoder();
};
*/
/* -------------------------------------------------------------------------- */
/*
template <typename PixType>
void PngEncoder<PixType>::decode_header(int & w, int & h)
{
  
}
*/
/* -------------------------------------------------------------------------- */
/*
template <typename PixType>
void PngEncoder<PixType>::decode(const char * out, size_t stride)
{
  int w, h;
  decode_header(w, h);
  
  for(int r = 0; r < h; ++r, out += stride)
    decode_row(out);
}
*/
/* -------------------------------------------------------------------------- */

}
}

/* -------------------------------------------------------------------------- */
