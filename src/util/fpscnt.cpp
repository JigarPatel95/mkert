/* ------------------------------------------------------------------------- */

#include "fpscnt.h"
#include <iostream>
#include <iomanip>

/* ------------------------------------------------------------------------- */

using namespace mke::util;

/* ------------------------------------------------------------------------- */

FpsCounter::FpsCounter(double interval, bool autolog)
: interval_(interval), autolog_(autolog), 
last_stamp_(std::chrono::high_resolution_clock::now())
{
}

/* ------------------------------------------------------------------------- */

void FpsCounter::tick()
{
    auto now_stamp = std::chrono::high_resolution_clock::now();
    auto diff = std::chrono::duration_cast<std::chrono::seconds>(now_stamp - last_stamp_).count();
    if(diff > interval_)
    {
        last_fps_ = double(cnt_) / diff;
        last_stamp_ = now_stamp;
        cnt_ = 0;
        if(autolog_)
            std::cerr << "FPS: " << std::fixed <<  std::setprecision(2) << last_fps_ << std::endl;
    }
    else
    {
        cnt_++;
    }
}

/* ------------------------------------------------------------------------- */

double FpsCounter::getFps()
{
    return last_fps_;
}

/* ------------------------------------------------------------------------- */
