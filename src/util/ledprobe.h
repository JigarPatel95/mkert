/*
 * StatusProbe - publish current status by LED's
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef _LEDPROBE_H_
#define _LEDPROBE_H_

/* -------------------------------------------------------------------------- */

#include <string>
#include <map>
#include <thread>
#include <mutex>
#include <condition_variable>

#include "dev/cmldclib/cmldclib.h"

/* -------------------------------------------------------------------------- */

namespace mke {
namespace util {

/* -------------------------------------------------------------------------- */

template<typename KeyType = int>
class LedProbe
{
protected:
  std::map<KeyType, std::string>    profiles_;
  std::string                       loop_;
  std::string                       finalizeProfile_;
  std::thread                       thread_;
  std::mutex                        shared_;
  std::condition_variable           cv_;
  std::exception_ptr                fatal_err_;
  
  std::string                       curr_code_;
  const char *                      curr_pos_;
  const char *                      loop_start_;
    
public:  
  LedProbe()
  : fatal_err_(nullptr), curr_pos_(nullptr), loop_start_(nullptr)
  {
    setFinalizeCode("");
    applyCode("");
    
    thread_ = std::thread(std::bind(&LedProbe<KeyType>::doThreadProc, this));
  };

  virtual ~LedProbe()
  {
    applyCode(finalizeProfile_);
    
    thread_.join();  
  }
  
  void addProfile(const KeyType & key, const std::string & profile)
  {
    profiles_[key] = profile;
  };
  
  void setProfile(const KeyType & key)
  {
    applyCode(profiles_[key]);
  }
  
  void applyCode(const std::string & code)
  {
    std::unique_lock<std::mutex> lock(shared_);

    if(fatal_err_)
      std::rethrow_exception (fatal_err_);
    
    curr_code_ = code;
    loop_start_ = nullptr;
    curr_pos_ = curr_code_.c_str();
    
    cv_.notify_all();
  };
  
  inline void setFinalizeCode(const std::string & code)
  {
    finalizeProfile_ = code + "f";
  }
  
  inline void ledControl(bool on)
  {
#ifdef USE_RPICAM
    cmldc_set_user_led(on ? 1 : 0);
#else
    // do nothing
#endif
  }
  
  void doThreadProc()
  {
    try
      {
        int ret;
        while(true)
          {
            std::unique_lock<std::mutex> lock(shared_);
            
            ret = parseCode();
            if(ret < 0)           // end of thread
              return;
            else if(ret == 0)     // end of code
              cv_.wait(lock);
            else                  // just wait
              cv_.wait_for(lock, std::chrono::milliseconds(ret));
          }
      }
    catch(std::exception & ex)
      {
        fatal_err_ = std::current_exception();
      }
  }
  
  /* supported commands:
   * +, -, W(milisec), L....., ..F
   * 
   * returns >0 ... wait, 0 .. end of code, <0 .. end of thread life
   */
  int parseCode()
  {
    int val;
    while(true)
      {
        char cmd = *curr_pos_++;
        switch(cmd)
          {
            case '+':   // turn on LED/s
              ledControl(true);
              break;
            case '-':   // turn off LED/s
              ledControl(false);
              break;
            case 'W':   // wait
              
              if(*curr_pos_ != '(')
                throw std::runtime_error("LedProbe: Command 'w' expects '('");
              val = 0;
              curr_pos_++;
              while(*curr_pos_ && *curr_pos_ != ')')
                {
                  if(*curr_pos_ < '0' || *curr_pos_ > '9')
                    throw std::runtime_error("LedProbe: Command 'w' expects only numbers in the brackets");
                  
                  val = val*10 + *curr_pos_++ - '0';
                }
              if(!*curr_pos_)
                throw std::runtime_error("LedProbe: Command 'w' expects ')'");
              curr_pos_++;
              
              return val;
            case 'L':
              loop_start_ = curr_pos_;
              break;
            case '\0':
              if(loop_start_)
                curr_pos_ = loop_start_;
              else
                return 0;
              break;
            case 'F':
              return -1;
            default:
              throw std::runtime_error("LedProbe: Bad command");
          }
      }
  }
};

/* -------------------------------------------------------------------------- */

} // namespace mke::util
} // namespace mke

/* -------------------------------------------------------------------------- */

#endif // _LEDPROBE_H

/* -------------------------------------------------------------------------- */

