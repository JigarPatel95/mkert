/*
 * Governon - checks if sensor can run
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar, Jan Heller
 *
 */

/* -------------------------------------------------------------------------- */

#include "governon.h"

#include <cassert>
#include <iostream>

/* -------------------------------------------------------------------------- */

using namespace mke::util;

/* -------------------------------------------------------------------------- */

void Governon::setOn()
{
  assert(canRun());
  
  last_off_ = std::min(last_off_ + timer_.elapsed(), (double) max_on_);
  timer_.reset();
  is_on_ = true;
}

/* -------------------------------------------------------------------------- */

void Governon::setOff()
{
  if(!is_on_)
    return;
  
  last_off_ = std::max(last_off_ - timer_.elapsed(), 0.0);
  timer_.reset();
  is_on_ = false;
}

/* -------------------------------------------------------------------------- */

bool Governon::canRun() const
{
  if(is_on_)
     return max_on_ < 0 ? true :  timer_.elapsed() < std::min((double) max_on_, (last_off_ * off_factor_));
  else
    return (timer_.elapsed() > min_off_);
}

/* -------------------------------------------------------------------------- */

float Governon::countdown() const
{
  if(is_on_)
    return max_on_ < 0 ? 0 : (std::min((double) max_on_, last_off_ * off_factor_) - timer_.elapsed());
  else
    {
      float lim = std::min((double) max_on_, timer_.elapsed() + last_off_ * off_factor_);
      return -lim;
    }
}

/* -------------------------------------------------------------------------- */
