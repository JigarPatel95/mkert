/*
 * RasPiEngine - specific engine for Raspberry PI
 *    controls all devices (camera, lasers) and sync them
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar, Jan Heller
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef _PGM_H_
#define _PGM_H_

/* -------------------------------------------------------------------------- */

#include "dev/img.h"
#include <string>

/* -------------------------------------------------------------------------- */

namespace mke {
namespace util {

/* -------------------------------------------------------------------------- */

class PGM

{
  static int fillHeader(int w, int h, char * buff);
  
public:  
  /**
   * @brief get size of header itself of PGM for given image
   * 
   * @param im      image should be decoded
   * @result        size of required memory in bytes
   */
  static size_t getHeaderSize(int w, int h);
  
  /**
   * @brief get required memory size for decoded image into PGM
   * 
   * @param im      image should be decoded
   * @result        size of required memory in bytes
   */
  static size_t getBufferSize(int w, int h);
  
  /**
   * @brief encode given image into PGM format
   * 
   * @param src     source image that will be decoded
   * @param dst     preallocated memory with minimal length given by getBufferSize
   * @result        number of written bytes
   */
  static size_t encode(const mke::dev::Image<mke::dev::Pix8> & src, void * dst);
  
  /**
   * @brief write given image into file
   * 
   * @param src     source image
   * @param path    path where image should be saved
   */
  static void write(const mke::dev::Image<mke::dev::Pix8> & src, const std::string & path);
  
  /**
   * @brief write given image into file
   * 
   * - dst image will be reallocated with stride given by param (if stride > w)
   * 
   * @param path    path from image should be loaded
   * @param dst     destination image - will be reallocated
   */
  static void read(const std::string & path, mke::dev::MemImage<mke::dev::Pix8> & dst, int stride = -1);
};

/* -------------------------------------------------------------------------- */

} // namespace mke::util
} // namespace mke

/* -------------------------------------------------------------------------- */

#endif // _PGM_H_

/* -------------------------------------------------------------------------- */
