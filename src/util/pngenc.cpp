
/* -------------------------------------------------------------------------- */

#include "util/pngenc.h"
#include <png.h>
#include <stdexcept>
#include <iostream>
#include <cstring>

/* -------------------------------------------------------------------------- */

using namespace mke::util;

/* -------------------------------------------------------------------------- */

void PNG::mem_write_data(png_structp png_ptr, png_bytep data, png_size_t length)
{
  /* with libpng15 next line causes pointer deference error; use libpng12 */
  std::vector<char> * vec = reinterpret_cast<std::vector<char> *>(png_get_io_ptr(png_ptr)); /* was png_ptr->io_ptr */
  size_t nsize = vec->size() + length;
  // need to allocate new memmory?
  
  if(nsize > vec->capacity())
    {
      size_t ncap = vec->capacity();
      while(ncap < nsize)
        ncap *=2;
      
      std::vector<char> nw;
      nw.reserve(ncap);
      nw.resize(nsize);
      
      memcpy(nw.data(), vec->data(), vec->size());
      nw.swap(*vec);
    }
  else
    {
      vec->resize(nsize);      
    }

  // copy the rest
  memcpy(vec->data() + vec->size() - length, data, length);
}

/* -------------------------------------------------------------------------- */

void PNG::mem_write_data_static(png_structp png_ptr, png_bytep data, png_size_t length)
{

  data_static_struct * marker = reinterpret_cast<data_static_struct *>(png_get_io_ptr(png_ptr));
  if(marker->len < length)
    {
      png_error(png_ptr, "Too small buffer. Unable to fill it");
      return;
    }

  // copy the rest
  
  memcpy(marker->marker, data, length);
  marker->marker += length;
  marker->len -= length;
}

/* -------------------------------------------------------------------------- */

void PNG::mem_flush(png_structp png_ptr)

{
}

/* -------------------------------------------------------------------------- */

void PNG::mem_read_data_static(png_structp png_ptr, png_bytep data, png_size_t length)
{

  data_static_struct * marker = reinterpret_cast<data_static_struct *>(png_get_io_ptr(png_ptr));
  if(marker->len < length)
    {
      png_error(png_ptr, "Too small buffer. Unable to fill it");
      return;
    }

  // copy the rest
  
  memcpy(data, marker->marker, length);
  marker->marker += length;
  marker->len -= length;
}

/* -------------------------------------------------------------------------- */
