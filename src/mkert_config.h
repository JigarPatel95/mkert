/*
 * MkertConfig
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com 
 *         Ondra Fisar, fisar@magik-eye.com
 */

#ifndef _MKERT_CONFIG_H_
#define _MKERT_CONFIG_H_

#include <vector>

#include "util/config.h"

namespace mke {

class MkertConfig : public util::ConfigService {
public:
  
  /**
   * @brief Configuration of dispatcher
   */
  struct DispatcherConfig {
    std::vector<int>    tcp_ports;              // list of listened TCP port
    std::vector<std::string>
                        serial_ports;           // list of listened serial ports
    int                 serial_baudrate;        // serial baudrate
    std::string         serial_parity;          // serial parity
    std::string         serial_stopbits;        // serial stopbits
    std::string         serial_flowcontrol;     // serial flow control
  };
  
  /**
   * @brief Configuration of API
   */
  struct APIConfig {
    int                 max_basic_replies;      // max. num. of basic API requests 
    int                 max_frame_replies;      // max. num. of frame API requests
    int                 max_dynamic_replies;    // max. num. of dynamic API requests
    
    int                 max_request_delay;      // maximul delay before API request will be processed

    bool                reserved_mode;          // is reserved mode active
  };
  
  /**
   * @brief Configuration of RasPiCam
   */
  struct CameraConfig {
    
    int                 camera_id;              // ID of used camera
    int                 sensor_mode;            // mode of camera/sensor
    
    int                 num_buffers;            // num. of used buffers
    bool                compressed;             // use compressed stream
    
    bool                auto_exposure;          // let the engine prepare auto exposure
    
    int                 fps;                    // frame rate
    int                 shutter_speed;          // shutter speed
    int                 iso;                    // ISO 0=auto
    int                 exp_comp;               // exposure compensation
    int                 width;                  // image width
    int                 height;                 // image height
    
    std::string         sensordata_file;        // path to sensor data file - for onsemi
    std::string         presets_file;           // path to presets ini file - for onsemi    
    std::string         use_preset;             // preset name from the presets file -for onsemi    

    std::string         firmware_image_file;    // path to TOF firmware image file - for panasonic
    int                 range_mode;             // range mode - for panasonic
    bool                use_raw;                // use raw image
    bool                use_int;                // use raw interpolation
#ifdef __ANDROID__
    int                 usb_vender_id;          // USB vender ID
    int                 usb_product_id;         // USB product ID
    int                 usb_fd;                 // USB device file descriptor
#endif
  };
  
  /**
   * @brief General configuration of engine - both LOAD and RSPI
   */
  struct EngineConfig {
    
    std::string         type;                   // type of Engine: LOAD/RSPI
    std::string         load_dir;               // LOAD: loading directory (FAKE=fake images)
    int                 load_stride;            // LOAD: simulated stride of image in memory

    std::string         strobe_type;            // type of strobe
    int                 strobe_offset;          // offset of strobe
    int                 strobe_length;          // length of laser strobe
    
    int                 phase_shift;            // phase shift
    
    bool                use_phase_estimator;    // use laser phase estimator
  };
  
  /**
   * @brief General configuration
   */
  struct GeneralConfig {
    
    bool                failsafe;               // use failsafe mode
    std::string         upload_exec;            // what should be called when uploaded
    std::string         upload_target;          // where should be stored uploaded package

    std::string         profile1;              
    std::string         profile2;              
    std::string         profile3;     
    std::string         profiles_path;          // path to JSON file with array of profiles
    std::string         default_profile;             
    
    int                 gov_max_on;             // limit how long should be sensing on
    int                 gov_min_off;            // limit how long should be device off after on
    int                 gov_off_factor;         // compute off limit by factoring on time

    std::vector<int>    temp_idxs;              // temperature indexes should be used
    std::vector<int>    temp_idle_limits;       // upper temperature limit when the device goes to IDLE state
    
    bool                use_user_led;           // use user LED for signaling application state

    int                 max_debug_images;       // maximum debug images could be aquiered
  };
  
  /**
   * @brief General detector configuration
   */
  struct DetectorConfig {
    bool                dry_run;                // run without detection
    std::vector<std::string>
                        mkedet_path;            // path to a mke detector file 
  };

  /**
   * @brief Auto exposure configurations
   */
  
  struct AutoExpConfig {
    bool                enabled;                // is auto exposure enabled
    
    std::vector<int>    shutters;               // list of shutter to used
    std::vector<int>    isos;                   // list of ISOs to use
    
    int                 repetition;             // how many trigger repetitions to apply

    int                 up_value;               // limitation value (lower than -> trigger)
    int                 up_quant;               // sampled quantile

    int                 down_value;             // limitation value (grater than ->
    int                 down_quant;             // sampled quantile
    
    int                 max_pts;                // maximum number of points to apply autoexpose
  };
    
private:
  bool                  help;                   // print help
  bool                  verbose;                // print detail information
  bool                  version;                // print application version
  bool                  ignore_reboot;          // ignore reboot commands

  DispatcherConfig      dispatcher;             // dispather config
  APIConfig             api;                    // API config
  CameraConfig          camera;                 // camera config
  EngineConfig          engine;                 // engine config
  GeneralConfig         general;                // general configuration
  DetectorConfig        detector;               // general detector config
  AutoExpConfig         autoexp;                // auto exposure config
                        
  std::string           config_file;            // path to config file
  std::string           cmd;                    // command line
  
  
public:
  /**
   * @brief should be help printed
   */
  inline bool doHelp(void) const {
    return help;
  }
  
  /**
   * @brief should be application version printed
   */
  inline bool doVersion(void) const {
    return version;
  }
  
  /**
   * @brief should be detailed info printed
   */
  inline bool doVerbose(void) const {
    return verbose;
  }  
  
  /**
   * @brief should be reboot ignored
   */
  inline bool isRebootIgnored(void) const {
    return ignore_reboot;
  }
  
  /**
   * @brief get path to config file
   */
  inline std::string getConfigFile(void) const {
    return config_file;
  }
  
  /**
   * @brief get command line
   */
  inline std::string getCmd(void) const  {
    return cmd;
  }
  
  /**
   * @brief get dispatcher config
   */
  inline const DispatcherConfig & getListenerConfig(void) const {
    return dispatcher;
  }

  /**
   * @brief get API config
   */
  inline const APIConfig & getAPIConfig(void) const {
    return api;
  }

  /**
   * @brief get RasPiCam config
   */
  inline const CameraConfig & getCameraConfig(void) const {
    return camera;
  }
  
  /**
   * @brief get general detector config
   */
  inline const DetectorConfig & getDetectorConfig(void) const {
    return detector;
  }
  
  /**
   * @brief get general Engine config - both LOAD and RSPI
   */
  inline const EngineConfig & getEngineConfig(void) const {
    return engine;
  }
  
  /**
   * @brief get general Engine config - both LOAD and RSPI
   */
  inline const GeneralConfig & getGeneralConfig(void) const {
    return general;
  }

  /**
   * @brief get general Engine config - both LOAD and RSPI
   */
  inline const AutoExpConfig & getAutoExpConfig(void) const {
    return autoexp;
  }
  
  /**
   * @brief MkertConfig constructor
   */
  MkertConfig(const MkertConfig * orig = nullptr) { 

    // General
    
    registerBln(help, CMD_ONLY, "help", "Produce this help message", false);
    registerBln(verbose, CMD_ONLY, "verbose", "Print verbose runtime information", true);
    registerBln(version, CMD_ONLY, "version", "Print version information", false);
    registerBln(ignore_reboot, CMD_ONLY, "ignore_reboot", "Ignore reboot and power-off commands", false);
    registerStr(config_file, CMD_ONLY, "config", "Path to a configuration file", "");
      
    // Listener
    
    registerVnt(dispatcher.tcp_ports, GENERAL, "Dispatcher.tcp_ports", "List of listened TCP ports", 0, 10, "8888");
    registerVstr(dispatcher.serial_ports, GENERAL, "Dispatcher.serial_ports", "List of listener Serial ports", "");
    registerInt(dispatcher.serial_baudrate, GENERAL, "Dispatcher.serial_baudrate", "Baudrate for serial port", 115200);
    registerStr(dispatcher.serial_parity, GENERAL, "Dispatcher.serial_parity", "Parity for serial port (NONE/EVEN/ODD)", "NONE");
    registerStr(dispatcher.serial_stopbits, GENERAL, "Dispatcher.serial_stopbits", "Number of stopbits for serial port (1/1.5/2)", "1");
    registerStr(dispatcher.serial_flowcontrol, GENERAL, "Dispatcher.serial_flowcontrol", "Flow control for serial port (NONE/HW/SW)", "NONE");
    
    
    // API
    
    registerInt(api.max_basic_replies, GENERAL, "API.max_basic_replies", "Maximum number of basic API requests", 4);
    registerInt(api.max_frame_replies, GENERAL, "API.max_frame_replies", "Maximum number of frame API requests", 4);
    registerInt(api.max_dynamic_replies, GENERAL, "API.max_dynamic_replies", "Maximum number of dynamic API requests", 4);
    registerInt(api.max_request_delay, GENERAL, "API.max_request_delay", "Maximum delay before API request will be processed [ms] (100 default)", 100);
#ifdef MKE_USE_RESERVED_API
    registerBln(api.reserved_mode, GENERAL, "API.reserved_mode", "Switch to reserved mode", false);
#else
    api.reserved_mode = false;
#endif    
    // Camera
    
    registerInt(camera.camera_id, GENERAL, "Camera.id", "Camera ID", 0);
    registerInt(camera.sensor_mode, GENERAL, "Camera.mode", "Sensor mode (0=auto)", 0);
    registerInt(camera.num_buffers, GENERAL, "Camera.num_buffers", "Number of used buffers (>=3)", 10);
    registerBln(camera.auto_exposure, GENERAL, "Camera.auto_exposure", "Using auto exposure", false);
    registerInt(camera.fps, GENERAL, "Camera.fps", "Frame rate [Hz] (0=auto)", 0);
    registerInt(camera.shutter_speed, GENERAL, "Camera.shutter_speed", "Exposure time [us] (0=auto)", 0);
    registerInt(camera.iso, GENERAL, "Camera.iso", "ISO value (0=auto)", 0);
    registerInt(camera.exp_comp, GENERAL, "Camera.exp_comp", "Exposure compensation [ev] (0=auto)", -24);
    registerInt(camera.width, GENERAL, "Camera.width", "Image width [px]", 1296);
    registerInt(camera.height, GENERAL, "Camera.height", "Image height [px]", 972);
    registerBln(camera.compressed, GENERAL, "Camera.compressed", "Use compressed stream for the camera", false);
    registerStr(camera.sensordata_file, GENERAL, "Camera.sensordata_file", "Set sensor_data file path", "");
    registerStr(camera.presets_file, GENERAL, "Camera.presets_file", "Set presets ini file path", "");
    registerStr(camera.use_preset, GENERAL, "Camera.use_preset", "Set preset from the presets file", "");
    registerStr(camera.firmware_image_file, GENERAL, "Camera.firmware_image_file", "Set firmware image file path", "");
    registerInt(camera.range_mode, GENERAL, "Camera.range_mode", "Range mode", 0);
    registerBln(camera.use_raw, GENERAL, "Camera.use_raw", "Using raw image", true);
    registerBln(camera.use_int, GENERAL, "Camera.use_int", "Using raw image interpolation", false);
#ifdef __ANDROID__
    registerInt(camera.usb_vender_id, GENERAL, "Camera.usb_vender_id", "USB vender ID", 0);
    registerInt(camera.usb_product_id, GENERAL, "Camera.usb_product_id", "USB product ID", 0);
    registerInt(camera.usb_fd, GENERAL, "Camera.usb_fd", "USB file descriptor", 0);
#endif
    
    // Engine

    const char * def_engine;
#if defined(USE_RPICAM)
    def_engine = "RSPI";
#elif defined(USE_FPCAM)
    def_engine = "FPCAM";
#elif defined(USE_ONSEMI)
    def_engine = "ONSEMI";
#elif defined(USE_PANA)
    def_engine = "PANA";
#else
    def_engine = "LOAD";
#endif
    registerStr(engine.type, GENERAL, "Engine.type", "Type of engine (RSPI,FPCAM,LOAD)", def_engine);
    registerStr(engine.load_dir, GENERAL, "Engine.load_dir", "Loading directory for LOAD engine (NOISE=generated)", "NOISE");
    registerInt(engine.load_stride, GENERAL, "Engine.load_stride", "Simulate stride for loaded images [px] (-1=auto)", -1);
    registerStr(engine.strobe_type, GENERAL, "Engine.strobe_type", "Laser strobe controler (AUTO/HW/SW/FAKE)", "AUTO");
    registerInt(engine.strobe_length, GENERAL, "Engine.strobe_length", "Laser strobe length [us]", 0);
    registerInt(engine.strobe_offset, GENERAL, "Engine.strobe_offset", "Laser strobe offset [us]", 0);
    registerInt(engine.phase_shift, GENERAL, "Engine.phase_shift", "Laser pattern phase shift in synchronization", -1);
    registerBln(engine.use_phase_estimator, GENERAL, "Engine.use_phase_estimator", "Use laser phase estimator", false);

    // General 
    
    registerBln(general.failsafe, GENERAL, "General.failsafe", "Should be used failsafe mode", true);
    registerStr(general.upload_exec, GENERAL, "General.upload_exec", "What should be called when upload package called", "/bin/sh /mkert/updater.sh");
    registerStr(general.upload_target, GENERAL, "General.upload_target", "File where will be saved uploaded package", "/tmp/package.tar");
    registerStr(general.profile1, GENERAL, "General.profile1", "Profile string to be added", "");
    registerStr(general.profile2, GENERAL, "General.profile2", "Profile string to be added", "");
    registerStr(general.profile3, GENERAL, "General.profile3", "Profile string to be added", "");
    registerStr(general.profiles_path, GENERAL, "General.profiles_path", "Path to JSON file with profiles (as JSON array)", "");
    registerStr(general.default_profile, GENERAL, "General.default_profile", "Name of the default profile", "");
    registerInt(general.gov_max_on, GENERAL, "General.gov_max_on", "Upper limit of sensing time [s] (-1=OFF)", -1);
    registerInt(general.gov_min_off, GENERAL, "General.gov_min_off", "Lower limit of idle time after sensing [s] (0=OFF)", 0);
    registerInt(general.gov_off_factor, GENERAL, "General.gov_off_factor", "Lower limit defined by factor of sension time (0=OFF)", 0);
    registerVnt(general.temp_idxs, GENERAL, "General.temp_idxs", "List of termometer indexes will be used", 0, 10, "");    registerVnt(general.temp_idle_limits, GENERAL, "General.temp_idle_limits", "Temperature limits for switching to IDLE state (-300 ~ ignore)", 0, 10, "");

    registerVnt(general.temp_idle_limits, GENERAL, "General.temp_idle_limits", "Temperature limits for switching to IDLE state (-300 ~ ignore)", 0, 10, "");
    registerBln(general.use_user_led, GENERAL, "General.use_user_led", "Use user LED for signal application state", 
                                                                  orig ? orig->getGeneralConfig().use_user_led : false);
    registerInt(general.max_debug_images, GENERAL, "General.max_debug_images", "Maximum allowed number of aquired debug images", 30);

    
    // Detector
    
    registerVstr(detector.mkedet_path, GENERAL, "Detector.mkedet_path", "Path to mke detector file.", "");
    registerBln(detector.dry_run, GENERAL, "Detector.dry_run", "Run depth sensing without detection", false);
  
    // AutoExposure
    
    registerBln(autoexp.enabled, GENERAL, "AutoExposure.enabled", "En/Disable SW auto exposure", false);
    registerVnt(autoexp.shutters, GENERAL, "AutoExposure.shutters", "List of shutters could be used", 0, 10, "");
    registerVnt(autoexp.isos, GENERAL, "AutoExposure.isos", "List of ISOs could be used", 0, 10, "");

    registerInt(autoexp.repetition, GENERAL, "AutoExposure.repetition", "How many frames the trigger must be repeated to change exposure", 10);
    registerInt(autoexp.max_pts, GENERAL, "AutoExposure.max_pts", "Maximum detected points to apply auto exposure", 50);

    registerInt(autoexp.up_value, GENERAL, "AutoExposure.up_value", "Quantile value must be lower to increase exposure", 150);
    registerInt(autoexp.up_quant, GENERAL, "AutoExposure.up_quantile", "Quantile to compute up_value (in promile)", 990);

    registerInt(autoexp.down_value, GENERAL, "AutoExposure.down_value", "Quantile value must be bigger to decrease exposure", 50);
    registerInt(autoexp.down_quant, GENERAL, "AutoExposure.down_qvanlite", "Quantile to compute down_value (in promile)", 500);
        
    
    updateConfig();
  }
  
};  
  
} // namespace mke

#endif // _MKERT_CONFIG_H
