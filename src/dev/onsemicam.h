/*
 * onsemicam.cpp - wrapper for OnSemi development board
 * 
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 * 
 * Based on DevWareX SDK - ApBase COM
 */

/* -------------------------------------------------------------------------- */

#ifndef _ONSEMICAM_H_
#define _ONSEMICAM_H_

/* -------------------------------------------------------------------------- */

// STL includes

#include <exception>
#include <stdexcept>
#include <iostream>
#include <sstream>

#include <mutex>
#include <thread>
#include <queue>

#include <cmath>

#include "img.h"

/* -------------------------------------------------------------------------- */

#define ONSEMICAM_ROWTIME 4.4   //us
#define ONSEMICAM_FLASHUNIT 23.2   //ns

/* -------------------------------------------------------------------------- */

namespace mke {
namespace dev {


/* -------------------------------------------------------------------------- */

// Error thrown by OnSemi ApBase COM

class OnSemiError : public std::runtime_error

{
  int                           ret_status_;	// returned value if passed
  std::string                   desc_;
  
public:
  // constructor with reason
  OnSemiError(const char* desc, int ret_status = -1);
  
  // destructor
  virtual ~OnSemiError()
  {};
  
  // describe exception
  virtual const char* what() const throw();
};

/* -------------------------------------------------------------------------- */

// camera class

class OnSemiCam

{  
public:    
  // constructor
  OnSemiCam(const std::string & sensor_data_file, const std::string & presets_file_, 
            const std::string & preset, int cameraid, int buffer_size);
  
  // destructor
  virtual ~OnSemiCam();
  
  // get sensor name
  const char * getName() { return camera_name_.c_str(); };
  
  void setShutterSpeed(uint32_t shutter);
  
  void setGain(double gain);
  
  uint32_t getShutterSpeed();
  
  double getAnalogGain();
  double getDigitalGain();
  
  void setLaserSequence(uint16_t pattern, int num);
  
  uint16_t getLaserSequence();
  
  // start capturing
  void startCapture();
  
  // stop capture
  void stopCapture();
  
  // set FPS
  void setFps(int fps);
  
  void returnBackBuffer(const Pix8 * buff);
  
  virtual void videoFrameReceived(const Pix8 * buff, int w, int h, int memw, uint16_t phase) = 0;  
  
  int getImageWidth() const { return image_width_; }

  int getImageHeight() const { return image_height_; }

  uint32_t getRegister(const char * regname, const char * bitname = nullptr) const;
  
  void setRegister(const char * regname, uint32_t value, const char * bitname = nullptr);

  void applyProfile(const char * profile_name);
  
protected:

  typedef std::vector<Pix8> Buffer;
  typedef void * HANDLE; // to avoid outer dependency on ftd2xx.h
  
  const std::string     sensor_data_file_;
  const std::string     presets_file_;
  const std::string     preset_;
  int                   port_id_;         // camera identification
  unsigned int          num_buffers_;     // number of buffer to be used
  std::string           camera_name_;
  
  HANDLE                ap_handle_;
  
  std::thread           thread_;
  volatile bool         running_;

  uint16_t              laser_pattern_;
  uint16_t              laser_pattern_len_;
  std::mutex            shared_;
  
  std::vector<Buffer>   buffer_;
  size_t                buff_size_;
  std::queue<Pix8 *>    available_;
  
  unsigned int          image_width_;
  unsigned int          image_height_;
  std::string           image_type_;
  
  uint32_t              fll_;
  
  std::exception_ptr    err_;
  
  void initialize();
  
  void finalize();
  
  void threadProc();    
  
  // takes array of 10bit (2Bytes) pixels and makes 8bit values from the by shifting right >>2
  static void conv10b8(Pix8 * im, size_t num_pixels);
  
  void checkStatus(int ret_code, const char * err_desc, int success_code = 0) const;
  void checkLastError(const char * err_desc) const;
};

/* -------------------------------------------------------------------------- */

}   // end of mke::dev
}   // end of mke

/* -------------------------------------------------------------------------- */

#endif // _FP_CAM_

