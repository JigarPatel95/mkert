/*
 * V4L2Cam.cpp - wrapper for OnSemi Development board
 * 
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 * 
 * Based on DevWareX - ApBase COM
 */

/* -------------------------------------------------------------------------- */

#include "v4l2cam.h"

#include <thread>
#include <cmath>
#include <cassert>
#include <exception>
#include <cstdio>
#include <cstring>

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#include <linux/videodev2.h>

#define CLEAR(x) memset(&(x), 0, sizeof(x))

/* -------------------------------------------------------------------------- */

using namespace mke::dev;

#define V4L2CAM_TAG "V4L2Cam: "
#define DEBUGLOG(str) std::cerr << V4L2CAM_TAG << str << std::endl;

/* -------------------------------------------------------------------------- */

static int xioctl(int fh, int request, void *arg)
{
    int r;

    do {
        r = ioctl(fh, request, arg);
    } while (-1 == r && EINTR == errno);

    return r;
}

/* -------------------------------------------------------------------------- */

// constructor with reason
V4L2Error::V4L2Error(const char* desc, bool store_errno) 
: std::runtime_error(desc)

{
    std::ostringstream oss;
    oss.str("");
    oss << "V4L2Error: " << runtime_error::what();
    if (store_errno)
      oss << "(no. " << errno << ": " << strerror(errno) << ")";
        
    desc_ = oss.str();  
};

/* -------------------------------------------------------------------------- */

const char * V4L2Error::what() const throw()

{
    return desc_.c_str();
}

/* -------------------------------------------------------------------------- */

V4L2Cam::Buffer::~Buffer() 
{
    clear();  
}

/* -------------------------------------------------------------------------- */

void V4L2Cam::Buffer::clear()
{
    for(auto & a: *this)
        if(a.start && a.start != MAP_FAILED)
          {
            if (-1 == munmap(a.start, a.length))
                throw V4L2Error("Error during uninit of MMAP buffers");
            a.start = nullptr;
          }

    std::vector<Item>::clear();
}

/* -------------------------------------------------------------------------- */

void V4L2Cam::Buffer::prepare(size_t num)
{
    assert(size() == 0);
    resize(num, Item());
}

/* -------------------------------------------------------------------------- */

V4L2Cam::V4L2Cam(int videoid, int num_buffers)
: video_id_(videoid), num_buffers_(num_buffers), fd_(-1), running_(false)
    
{
    std::ostringstream oss;
    camera_name_ = "V4L2 camera";
    sprintf(device_name_, "/dev/video%d", video_id_);
    
    initialize();
}

/* -------------------------------------------------------------------------- */

V4L2Cam::~V4L2Cam()
{ 
    std::cerr << V4L2CAM_TAG << "Destroying camera" << std::endl;
    finalize();
}

/* -------------------------------------------------------------------------- */

void V4L2Cam::initialize()
{
    // open device -------------------------------------------------------------
  
    struct stat st;

    if (-1 == stat(device_name_, &st))
        throw V4L2Error("Cannot identify device");

    if (!S_ISCHR(st.st_mode))
        throw V4L2Error("Device must be char device");

    this->fd_ = open(device_name_, O_RDWR /* required */ | O_NONBLOCK, 0);

    if (-1 == fd_)
        throw V4L2Error("Cannot open device");
    
    
    // initialize device -------------------------------------------------------
    try 
      {
        struct v4l2_capability cap;

        if (-1 == xioctl(fd_, VIDIOC_QUERYCAP, &cap)) {
            if (EINVAL == errno) 
                throw V4L2Error("Device is no V4L2 device");
            else
                throw V4L2Error("At query 'VIDIOC_QUERYCAP'");
        }

        if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE))
            throw V4L2Error("Device is no video capture device");

        if (!(cap.capabilities & V4L2_CAP_STREAMING))
            throw V4L2Error("Device does not support streaming i/o");

        getVideoFormat(image_width_, image_height_, image_stride_, pixel_format_);
        if (pixel_format_ != v4l2_fourcc('G','R','E','Y') 
            && pixel_format_ != v4l2_fourcc('Y','1','0',' '))
          throw V4L2Error("Unsupported pixel format. Supported 'GREY' or 'Y10' only");
        initializeMmap();
      }
    catch(...)  // thrown in constructor - destruct the object
      {
        finalize();
        throw;
      }

}

/* -------------------------------------------------------------------------- */

void V4L2Cam::getVideoFormat(uint32_t & w, uint32_t & h, uint32_t & stride, 
                             uint32_t & pixtype) const
{
    struct v4l2_format  format;
    
    CLEAR(format);

    format.type                = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    format.fmt.pix.width       = w;
    format.fmt.pix.height      = h;
    format.fmt.pix.pixelformat = V4L2_PIX_FMT_Y10;

    checkStatus(xioctl(fd_, VIDIOC_G_FMT, &format), "Unable to get video format");

    w = format.fmt.pix.width;
    h = format.fmt.pix.height;
    stride = format.fmt.pix.bytesperline;
    pixtype = format.fmt.pix.pixelformat;

    const char * spix = (const char *) &pixtype;
    std::cerr << V4L2CAM_TAG << "Camera resolution " << w << "x" << h 
              << ", stride: " << stride << ", pixtype: "
              << spix[0] << spix[1] << spix[2] << spix[3] << std::endl;
}

/* -------------------------------------------------------------------------- */

void V4L2Cam::setVideoFormat(uint32_t w, uint32_t h, uint32_t pixtype)
{
    struct v4l2_format  format;
    
    CLEAR(format);

    format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    format.fmt.pix.width       = w;
    format.fmt.pix.height      = h;
    format.fmt.pix.pixelformat = pixtype;
    format.fmt.pix.field       = V4L2_FIELD_INTERLACED;

    checkStatus(xioctl(fd_, VIDIOC_S_FMT, &format), "Unable to set video format");

    getVideoFormat(image_width_,image_height_,image_stride_, pixel_format_);
}

/* -------------------------------------------------------------------------- */

void V4L2Cam::initializeMmap()
{
    struct v4l2_requestbuffers req;

    CLEAR(req);

    req.count = num_buffers_;
    req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = V4L2_MEMORY_MMAP;

    if (-1 == xioctl(fd_, VIDIOC_REQBUFS, &req)) {
        if (EINVAL == errno) 
          throw V4L2Error("Device does not support memory mappingn");
        else
          throw V4L2Error("Error during query VIDIOC_REQBUFS");
    }

    if (req.count < 2)
        throw V4L2Error("Insufficient buffer memory");

    assert(buffer_.size() == 0);
    buffer_.prepare(req.count);

    for (unsigned i = 0; i < req.count; ++i) {
        struct v4l2_buffer buf;

        CLEAR(buf);

        buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory      = V4L2_MEMORY_MMAP;
        buf.index       = i;

        checkStatus(xioctl(fd_, VIDIOC_QUERYBUF, &buf), "VIDIOC_QUERYBUF");

        buffer_[i].start = mmap(NULL /* start anywhere */,
                                  buf.length,
                                  PROT_READ | PROT_WRITE /* required */,
                                  MAP_SHARED /* recommended */,
                                  fd_, buf.m.offset);
        buffer_[i].length = buf.length;

        if (MAP_FAILED == buffer_[i].start)
            throw V4L2Error("Error during init of MMAP buffers");
    }  
}

/* -------------------------------------------------------------------------- */

void V4L2Cam::finalizeMmap()
{
    buffer_.clear();
}

/* -------------------------------------------------------------------------- */

void V4L2Cam::finalize()
{
    if (running_)
        stopCapture();
    
    finalizeMmap();
    if(fd_ >= 0)
      close(fd_);
    
    fd_ = -1;
}

/* -------------------------------------------------------------------------- */

void V4L2Cam::checkStatus(int ret_code, const char * err_desc) const
{
    if(ret_code == -1)
        throw V4L2Error(err_desc);
}

/* -------------------------------------------------------------------------- */

void V4L2Cam::startCapture()
{
    DEBUGLOG("START")
    assert(running_ == false);

    struct v4l2_buffer buf;
    for (unsigned int i = 0; i < buffer_.size(); ++i) 
      {
        CLEAR(buf);
        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_MMAP;
        buf.index = i;

        checkStatus(xioctl(fd_, VIDIOC_QBUF, &buf), "VIDIOC_QBUF(startCapture)");
      }

    enum v4l2_buf_type type;
    type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    checkStatus(xioctl(fd_, VIDIOC_STREAMON, &type), "VIDIOC_STREAMON");

    thread_ = std::thread(&V4L2Cam::threadProc, this);
}

/* -------------------------------------------------------------------------- */

void V4L2Cam::stopCapture()
{
    DEBUGLOG("STOP");
    if(!running_)
        return;
        
    running_ = false;
    thread_.join();
    
    if(err_)
      std::rethrow_exception(err_);
}

/* -------------------------------------------------------------------------- */

void V4L2Cam::threadProc()
{
    running_ = true;

    try
    {
        while(running_)
        {
            // capture              
            for(;;)
              {
                fd_set fds;
                struct timeval tv;
                int r;

                FD_ZERO(&fds);
                FD_SET(fd_, &fds);

                /* Timeout. */
                tv.tv_sec = 2;
                tv.tv_usec = 0;

                r = select(fd_ + 1, &fds, NULL, NULL, &tv);

                if (-1 == r) {
                    if (EINTR == errno)
                        continue;
                    throw V4L2Error("Error during capturing");
                }

                if (0 == r) {
                    throw V4L2Error("Timeout error");
                }

                if (readFrame())
                    break;
                
                // EAGAIN repeat select
              }
        }         
        
        // stop capturing

        enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        checkStatus(xioctl(fd_, VIDIOC_STREAMOFF, &type), "VIDIOC_STREAMOFF");
    }
    catch (std::exception & ex) 
    {
        std::cerr << ex.what() << std::endl;
        err_ = std::current_exception();
    }
    catch (...)
    {
        std::cerr << "unknown exception" << std::endl;
        err_ = std::current_exception();
    }
        
    running_ = false;
}

/* -------------------------------------------------------------------------- */

bool V4L2Cam::readFrame()
{
    struct v4l2_buffer buf;
    
    CLEAR(buf);

    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;

    if (-1 == xioctl(fd_, VIDIOC_DQBUF, &buf)) {
        switch (errno) 
        {
          case EAGAIN:
            return false;

          case EIO:
            /* Could ignore EIO, see spec. */

            /* fall through */

          default:
            throw V4L2Error("VIDIOC_DQBUF");
        }
    }

    assert(buf.index < buffer_.size());

    processImage(buffer_[buf.index].start, buf.bytesused);
    
    return true;
}

/* -------------------------------------------------------------------------- */

void V4L2Cam::processImage(const void *p, int size)
{
    if (pixel_format_ == v4l2_fourcc('Y','1','0',' '))
      conv10p8((Pix8 *) p, image_stride_, image_height_, size);

    videoFrameReceived((const Pix8 *) p, (int) image_width_, image_height_, image_stride_);
}

/* -------------------------------------------------------------------------- */

int32_t V4L2Cam::getControlRegister(uint32_t addr) const
{
    struct v4l2_control ctl;

    CLEAR(ctl);

    ctl.id = addr;

    checkStatus(xioctl(fd_, VIDIOC_G_CTRL, &ctl), "VIDIOC_G_CTRL");

    return ctl.value;
}

/* -------------------------------------------------------------------------- */

void V4L2Cam::setControlRegister(uint32_t addr, int32_t value)
{
    struct v4l2_control ctl;

    CLEAR(ctl);

    ctl.id = addr;
    ctl.value = value;

    checkStatus(xioctl(fd_, VIDIOC_S_CTRL, &ctl), "VIDIOC_S_CTRL");
}

/* -------------------------------------------------------------------------- */

int32_t V4L2Cam::getShutterSpeed() const
{
    int32_t value =  getControlRegister(V4L2_CID_EXPOSURE);
    return value;
}
void V4L2Cam::setShutterSpeed(int32_t value)
{
    setControlRegister(V4L2_CID_EXPOSURE, value);
}

/* -------------------------------------------------------------------------- */

int32_t V4L2Cam::getGain() const
{
    int32_t value =  getControlRegister(V4L2_CID_GAIN);
    return value;
}
void V4L2Cam::setGain(int32_t value)
{
    setControlRegister(V4L2_CID_GAIN, value);
}

/* -------------------------------------------------------------------------- */

void V4L2Cam::returnBackBuffer(const Pix8 * im)
{
//    std::lock_guard<std::mutex> lock(shared_);
        
    // skip if not running, V4L2 will remove all queued buffers
    if (!running_)
      return;
    
    struct v4l2_buffer buf;
    CLEAR(buf);

    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;
    buf.index = 0xffff;
    
    for (size_t i = 0; i < buffer_.size(); ++i)
      if (buffer_[i].start == im)
        {
          buf.index = i;
          break;
        }
        
    checkStatus(xioctl(fd_, VIDIOC_QBUF, &buf), "VIDIOC_QBUF(return)");
}

/* -------------------------------------------------------------------------- */

void V4L2Cam::conv10p8(Pix8 * im, size_t stride, size_t h, size_t num_bytes)
{
    assert(num_bytes >= stride*h);
    
    for(size_t r = 0; r < h; ++r)
      {
        Pix8 * pix10 = im + r*stride;
        Pix8 * pix10E = pix10 + stride;
        uint32_t * pix8 = (uint32_t *) pix10;

        for(; pix10 < pix10E; pix8++,pix10+=5)
            *pix8 = *((uint32_t *) pix10);
      }
}

/* -------------------------------------------------------------------------- */
