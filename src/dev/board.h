/* 
 * Board info - class provide info about board
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondrej Fisar, fisar@magik-eye.com
 *
 */

/* -------------------------------------------------------------------------- */

#pragma once

/* -------------------------------------------------------------------------- */

extern "C" bool isCm3board();

/* -------------------------------------------------------------------------- */
