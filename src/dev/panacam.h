/*
 * PanaCam - wrapper for Panasonic TOF Camera
 * 
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * Copyright (c) 2019, Magik-Eye ltd.
 * author: Ondra Fisar, Jyunji Kondo
 * 
 */

/* -------------------------------------------------------------------------- */

#ifndef _PANACAM_H_
#define _PANACAM_H_

/* -------------------------------------------------------------------------- */

// STL includes

#include <exception>
#include <stdexcept>
#include <iostream>
#include <sstream>

#include <mutex>
#include <thread>
#include <queue>

#include <cmath>

#include "img.h"

/* -------------------------------------------------------------------------- */

namespace mke {
namespace dev {

/* -------------------------------------------------------------------------- */

class PanaError : public std::runtime_error
{
  int                           ret_status_;	// returned value if passed
  std::string                   desc_;
  
public:
  // constructor with reason
  PanaError(const char* desc, int ret_status = -1);
  
  // destructor
  virtual ~PanaError()
  {};
  
  // describe exception
  virtual const char* what() const throw();
};

/* -------------------------------------------------------------------------- */

// camera class

class PanaCam
{  
public:    
  // constructor
  PanaCam(const std::string & img_file, int range_mode, bool use_raw, bool use_int, int buffer_size);
  
  // destructor
  virtual ~PanaCam();
  
  // get sensor name
  const char * getName() { return camera_name_.c_str(); };
  
  void setShutterSpeed(uint32_t shutter);
  
  void setGain(double gain);
  
  uint32_t getShutterSpeed();
  
  double getAnalogGain();
  double getDigitalGain();
  
  void setLaserSequence(uint16_t pattern, int num);
  
  uint16_t getLaserSequence();
  
  // start capturing
  void startCapture();
  
  // stop capture
  void stopCapture();
  
  // set FPS
  void setFps(int fps);
  
  void returnBackBuffer(const Pix8 * buff);
  
  virtual void videoFrameReceived(const Pix8 * buff, int w, int h, int memw, uint16_t phase) = 0;  
  
  int getImageWidth() const { return image_width_; }

  int getImageHeight() const { return image_height_; }

  float getCamTemp() const;

  void setLaserPattern(int pattern);
  
protected:

  typedef std::vector<Pix8> Buffer;
  
  const std::string     img_file_;
  int                   range_mode_;
  bool                  use_raw_;
  bool                  use_int_;
  std::string           camera_name_;
  
  std::thread           thread_;
  volatile bool         running_;

  uint16_t              laser_pattern_;
  uint16_t              laser_pattern_len_;
  std::mutex            shared_;
  
  unsigned short        image_width_;
  unsigned short        image_height_;
  unsigned short        original_image_width_;
  unsigned short        original_image_height_;
  unsigned short        bpp_;
  
  std::exception_ptr    err_;
  
  bool                  *mode_;
  std::vector<unsigned short> max_exp_;

  void initialize();
  
  void finalize();
  
  void threadProc();    
  
  // takes array of 12bit (2Bytes) pixels and makes 8bit values from the by shifting right >>4
  static void conv12b8(Pix8 * im, size_t num_pixels, bool use_int);
  
  void checkStatus(int ret_code, const char * err_desc, int success_code = 0) const;

private:
  class Impl;
  Impl *impl_;
};

/* -------------------------------------------------------------------------- */

}   // end of mke::dev
}   // end of mke

/* -------------------------------------------------------------------------- */

#endif // _PANACAM_H_

