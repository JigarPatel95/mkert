#ifndef _RPSYNCLIB_H
#define _RPSYNCLIB_H 1

#ifdef __cplusplus
extern "C" {
#endif

/*
   int rpsync_open(void);

      Open and initialize the laser contoller device.
      All lasers will be turned OFF.

      Returns zero if success.
      Returns negative value if error.
*/
int rpsync_open(void);

/*
   int rpsync_close(void);

      Close the laser controller device.
      Laser states will not be changed.

      Returns zero if success.
      Returns negative value if error.
*/
int rpsync_close(void);

/*
   int rpsync_set(int bitmap);
  
      Set the laser state.

      bitmap: Laser state bitmap.
         bit 0 of bitmap value corresponds laser unit 1, bit 1 for unit 2,...
         bit 2 for unit 4. Higher bits will be ignored.
         For example:
            bitmap = 0 will turn off all lasers.
            bitmap = -1 will turn on all lasers.
            bitmap = 0x5 will turn on 1 and 3, turn off 2 and 4.

      Returns zero if success.
      Returns negative value if error.
*/
int rpsync_set(int bitmap);

/*
   int rpsync_set_sec(int bitmap, double sec);
  
      Flash lasers for specified seconds.

      bitmap: Laser state bitmap. (same as rpsync_set)
      sec: Laser enable time in seconds.

      Returns zero if success.
      Returns negative value if error.
*/
int rpsync_set_sec(int bitmap, double sec);

/*
   int rpsync_enable_laser(int enb)

      enable or disable laser

      enb: 1 for enable, 0 for disable

      Returns zero if success.
      Returns negative value if error.
*/
int rpsync_enable_laser(int enb);

#define RPSYNC_SEQUENCE_ALL    0xffff
#define RPSYNC_SEQUENCE_13_24  0xa5a5
#define RPSYNC_SEQUENCE_1234   0x8421
    
/*
   int rpsync_start_periodic(int sequence, double startTimeOffset,
                             double onTime, double offTime);

      Start periodic flashing of lasers.

      sequence: Sequence pattern of laser flashing
         RPSYNC_SEQUENCE_ALL    All lasers on
         RPSYNC_SEQUENCE_13_24  Laser 1+3 and 2+4 alternatively
         RPSYNC_SEQUENCE_1234   Laser 1 2 3 4 sequentially

      startTimeOffset: Start time offset in seconds.
      onTime: Laser on time in seconds.
      offTime: Laser off time in seconds.

      Returns zero if success.
      Returns negative value if error.
*/
int rpsync_start_periodic(int sequence, double startTimeOffset,
			  double onTime, double offTime);

/*
   int rpsync_shift_periodic_phase(double shiftTime);

      Shift the laser flashing time phase.

      shiftTime: Shift time offset in seconds.

      Returns zero if success.
      Returns negative value if error.
*/
int rpsync_shift_periodic_phase(double shiftTime);

/*
   int rpsync_stop_periodic(void);

      Stop periodic laser flashing previously started.

      Returns zero if success.
      Returns negative value if error.
*/
int rpsync_stop_periodic(void);
    
#ifdef __cplusplus
}
#endif

#endif
