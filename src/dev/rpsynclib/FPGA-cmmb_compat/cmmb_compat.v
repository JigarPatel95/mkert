`default_nettype none
`timescale 1ns/1ps

`define CMD_NOP               8'd0
`define CMD_RESET             8'd1
`define CMD_SET_MODE          8'd2
`define CMD_SET_RELOAD        8'd3
`define CMD_SET_ALT_RELOAD    8'd4
`define CMD_SET_COMPARE       8'd5

module cmmb_compat(
   // Clock
   input            CLK,
   // From camera
   input            FSTROBE1,
   input            FSTROBE2,
   // SPI interface
   input            SCLK,
   input            NCS,
   input            MOSI,
   output           MISO,
   // Laser enable
   input            LASERENB,
   // Laser drive (3.3V)
   output reg [3:0] LASERDRV,
   // LED drive
   output           LEDCTL, 
   // GPIO
   input            CONTROL,
   // Frame Detection
   input            MDN0,
   input            MDP0,
   input            MDN1,
   input            MDP1,
   input            MCN,
   input            MCP,
   output           TS2

);

   // ------------------------------------------
   //  Register

   reg reset;
   reg [7:0] mode_byte;
   reg [23:0] reload_value;
   reg [23:0] alt_reload_value;
   reg [23:0] compare_value;
   reg [15:0] laser_ctl;
   reg [1:0] laser_phase;

   wire cnt_enable = mode_byte[0];
   wire auto_reload = mode_byte[1];
   wire fpos_reload = mode_byte[2];
   wire laser_compare_on = mode_byte[6];
   wire laser_force_on = mode_byte[7];

   // ------------------------------------------
   //  SPI

   wire miso_int;
   assign MISO = NCS ? 1'bZ : miso_int;

   wire spi_last_bit;
   wire [31:0] spi_data_in;
   wire [31:0] spi_data_out;
   spi32 spi32_ (.clk(SCLK), .ncs(NCS), .mosi(MOSI),
                 .miso(miso_int), .data_in(spi_data_in),
                 .data_out(spi_data_out), .last_bit(spi_last_bit));

   wire [23:0] spi_argument
       = { spi_data_out[15:8], spi_data_out[23:16], spi_data_out[31:24] };
   wire [7:0] spi_command = spi_data_out[7:0];
   reg alt_reload_cmd;
   always @(posedge SCLK) begin
      if (~NCS & spi_last_bit) begin
         case (spi_command)
           `CMD_RESET:
             begin
                reset <= 1;
                reload_value <= 0;
                alt_reload_value <= 0;
                alt_reload_cmd <= 0;
                compare_value <= 0;
                mode_byte <= 0;
                laser_ctl <= 0;
             end
           `CMD_SET_MODE:
             begin
                reset <= 0;
                alt_reload_cmd <= 0;
                mode_byte <= spi_argument[23:16];
                laser_ctl <= spi_argument[15:0];
             end
           `CMD_SET_RELOAD:
             begin
                reset <= 0;
                alt_reload_cmd <= 0;
                reload_value <= spi_argument;
             end
           `CMD_SET_ALT_RELOAD:
             begin
                reset <= 0;
                alt_reload_cmd <= 1;
                alt_reload_value <= spi_argument;
             end
           `CMD_SET_COMPARE:
             begin
                reset <= 0;
                alt_reload_cmd <= 0;
                compare_value <= spi_argument;
             end
           default:
             begin
                reset <= 0;
                alt_reload_cmd <= 0;
             end
         endcase // case (spi_command)
      end
   end // always @ (posedge SCLK)

   reg alt_reload_set;
   reg d_alt_reload_cmd, dd_alt_reload_cmd;
   always @(posedge CLK) begin
      alt_reload_set <= d_alt_reload_cmd & ~dd_alt_reload_cmd;
      dd_alt_reload_cmd <= d_alt_reload_cmd;
      d_alt_reload_cmd <= alt_reload_cmd;
   end

   // ------------------------------------------
   //  FSTROBE edge detector

   wire FSTROBE = FSTROBE1 & FSTROBE2;
/*
   wire fpos;
   edge_detector edge_detector_ (.clk(CLK), .in(FSTROBE),
                                 .out_pos(fpos));
 */

   // ------------------------------------------
   //  SPI bit assignment

   assign spi_data_in
     = { mode_byte, FSTROBE, 23'h583101 };

   // ------------------------------------------
   //  Counter
   
   wire compare_out, iszero;
   wire reload = (auto_reload & iszero);
//   | (fpos_reload & fpos);
   timer24 timer24_(.clk(CLK), .reset(reset),
                    .enable(cnt_enable), .reload(reload),
                    .alt_reload_set(alt_reload_set),
                    .reload_value(reload_value),
                    .alt_reload_value(alt_reload_value),
                    .compare_value(compare_value),
                    .compare_out(compare_out), .iszero(iszero));

   // ------------------------------------------
   //  Laser Control
   
   always @(posedge CLK) begin
      if (reset)
        laser_phase <= 0;
      else if (reload & cnt_enable)
        laser_phase <= laser_phase + 2'd1;
   end

   wire laser_on = LASERENB &
        (laser_force_on | (compare_out & laser_compare_on));
   always @(posedge CLK) begin
     if (laser_on)
       case (laser_phase)
         2'd0: LASERDRV = laser_ctl[3:0];
         2'd1: LASERDRV = laser_ctl[7:4];
         2'd2: LASERDRV = laser_ctl[11:8];
         2'd3: LASERDRV = laser_ctl[15:12];
       endcase // case (laser_phase)
     else
       LASERDRV = 4'd0;
   end // always @ *

   assign LEDCTL = laser_phase[0];

   // Frame Detection
   assign TS2 = MDN0 & MDP0 & MDN1 & MDP1 & MCN & MCP;

endmodule // rpsync

/*
module edge_detector (
   input  clk,
   input  in,
   output out_pos
);

   reg d_in;
   reg dd_in;

   always @(posedge clk) begin
      dd_in <= d_in;
      d_in <= in;
   end

   assign out_pos = (~dd_in) & d_in;

endmodule // edge_detector
*/

module spi32 (
   input         clk,
   input         ncs,
   input         mosi,
   output        miso,
   input [31:0]  data_in,
   output [31:0] data_out,
   output        last_bit
);

   reg [4:0] bitcnt;
   always @(negedge clk, posedge ncs) begin
      if (ncs)
        bitcnt <= 6'd31;
      else
        bitcnt <= bitcnt - 6'd1;
   end

   assign miso = data_in[bitcnt];

   reg [31:0] data_out_reg;
   always @(posedge clk) begin
      if (~ncs)
        data_out_reg[bitcnt] <= mosi;
   end

   assign data_out = { data_out_reg[31:1], mosi };

   assign last_bit = bitcnt == 0;

endmodule // spi32

module timer24 (
   input        clk,
   input        reset,
   input        enable,
   input        reload,
   input        alt_reload_set,
   input [23:0] reload_value,
   input [23:0] alt_reload_value,
   input [23:0] compare_value,
   output reg   compare_out,
   output       iszero
);

   reg [23:0] value;
   assign iszero = value == 24'd0;

   reg use_alt_reload;
   always @(posedge clk) begin
      if (alt_reload_set)
        use_alt_reload <= 1;
      else if (reset)
        use_alt_reload <= 0;
      else if (enable & reload)
        use_alt_reload <= 0;
   end

   always @(posedge clk) begin
      if (reset)
        value <= 0;
      else if (enable) begin
         if (reload)
           value <= use_alt_reload ? alt_reload_value : reload_value;
         else if (~iszero)
           value <= value - 24'd1;
      end
   end

   always @(posedge clk) begin
      compare_out <= (value <= compare_value) & ~iszero;
   end

endmodule // timer24

