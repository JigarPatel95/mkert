`default_nettype none
`timescale 1ns/1ps

module test_bench();

   reg clock, control, fstrobe1, fstrobe2, laserenb;
   reg sclk, ncs, mosi;
   wire miso;
   wire [3:0] laserdrv;
   wire ledctl;

   cmmb_compat DUT (.CLK(clock), .CONTROL(control),
               .FSTROBE1(fstrobe1), .FSTROBE2(fstrobe2),
               .SCLK(sclk), .NCS(ncs), .MOSI(mosi), .MISO(miso),
               .LASERENB(laserenb), .LASERDRV(laserdrv),
               .LEDCTL(ledctl));

   initial begin
      clock <= 0;
      control <= 1;
      fstrobe1 <= 1;
      fstrobe2 <= 1;
      laserenb <= 0;
      sclk <= 0;
      ncs <= 1;
      mosi <= 1'bZ;
   end

   always @(DUT.reset or DUT.cnt_enable) begin
      $display("%t: DUT.reset: %1b DUT.cnt_enable: %1b",
               $time, DUT.reset, DUT.cnt_enable);
   end

   always @(laserdrv or ledctl) begin
      $display("%t: laser: %04b led: %1b", $time, laserdrv, ledctl);
   end

   always #41.666 begin
      clock <= ~clock;
   end

   task send_spi(input [31:0] send);
      integer i;
      reg [31:0] recv;
      begin
         //$display("DUT.spi_data_in: %16x", DUT.spi_data_in);
         $display("%t: SPI send: %08x", $time, send);
         recv = 0;
         #500 ncs <= 0;
         for (i = 0; i < 32; i = i + 1) begin
            mosi <= send[31 - i];
            #500 sclk <= 1;
            recv <= { recv[30:0], miso };
            #500 sclk <= 0;
         end
         #500 ncs <= 1;
         mosi <= 1'bZ;
         $display("%t: SPI recv: %08x", $time, recv);
         //$display("DUT.spi_data_out: %16x", DUT.spi_data_out);
      end
   endtask // send_spi

   initial begin
      $display("Simulation start!");
      control <= 0;
      send_spi(32'h00_00_00_00); // NOP
      send_spi(32'h00_00_00_01); // RESET
      laserenb <= 1;
      $display("force select test");
      send_spi(32'h11_11_80_02);
      send_spi(32'h22_22_80_02);
      send_spi(32'h44_44_80_02);
      send_spi(32'h88_88_80_02);
      $display("auto cycle test");
      send_spi(32'h00_00_00_01); // RESET
      send_spi(32'h80_00_00_05); // set COMPARE
      send_spi(32'h00_40_00_03); // set RELOAD
      send_spi(32'h21_84_43_02); // START
      #10_000_000
        $finish();
   end

endmodule // test_bench
