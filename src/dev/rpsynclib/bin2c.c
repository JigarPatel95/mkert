#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

void print_header(const char* name)
{
    printf("/* AUTO GENERATED FILE. DO NOT EDIT */\n");
    printf("static const uint8_t %s[] = {", name);
}

void print_footer(const char* name, uint32_t size)
{
    printf("\n};\n");
    printf("static const uint32_t %s_size=%u;\n", name, size);
    printf("/* END OF FILE */\n");
}

int main(int argc, char** argv)
{
    if (argc != 3) {
        fprintf(stderr, "usage: bin2c binary-file name\n");
        exit(1);
    }

    const char* name = argv[2];

    FILE* file = fopen(argv[1], "rb");
    if (!file) {
        perror(argv[1]);
        exit(1);
    }

    print_header(name);

    uint32_t count = 0;
    for (;;) {
        int byte = fgetc(file);
        if (byte < 0) {
            break;
        }
        if ((count % 10) == 0) {
            printf("\n   ");
        }
        printf(" 0x%02X,", byte);
        count++;
    }

    print_footer(name, count);

    return 0;
}
