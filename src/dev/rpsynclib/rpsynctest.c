#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "rpsynclib.h"

void usage(void)
{
    fprintf(stderr, "usage: rpsynctest <laser-bitmap> [<sec>]\n");
    fprintf(stderr, "       rpsynctest enable\n");
    fprintf(stderr, "       rpsynctest disable\n");
    fprintf(stderr,
	"       rpsynctest <pattern> <offset-sec> <on-sec> <off-sec>\n");
    fprintf(stderr, "           Start periodic\n");
    fprintf(stderr, "           pattern='all' or '13_24' or '1234'\n");
    
    fprintf(stderr, "       rpsynctest offset <offset-sec>\n");
    fprintf(stderr, "           Shift periodic phase\n");
    fprintf(stderr, "       rpsynctest stop\n");
    fprintf(stderr, "           Stop periodic\n");
}

int main(int argc, char** argv)
{
    if (argc < 2 || argc > 5) {
        usage();
        exit(1);
    }

    if (rpsync_open() < 0) {
        exit(1);
    }

    if (argc == 2 && strcmp(argv[1], "enable") == 0) {
        if (rpsync_enable_laser(1) < 0) {
	    return -1;
	}
        printf("laser power enabled\n");
        return 0;
    }
    if (argc == 2 && strcmp(argv[1], "disable") == 0) {
        if (rpsync_enable_laser(0) < 0) {
	    return -1;
	}
        printf("laser power disabled\n");
        return 0;
    }
    if (argc == 2 && strcmp(argv[1], "stop") == 0) {
	if (rpsync_stop_periodic() < 0) {
	    return -1;
	}
	printf("periodic laser stopped\n");
	return 0;
    }
    if (argc == 3 && strcmp(argv[1], "offset") == 0) {
	double offset = atof(argv[2]);
	if (rpsync_shift_periodic_phase(offset) < 0) {
	    return -1;
	}
	printf("periodic phase shifted for %f sec.\n", offset);
	return 0;
    }

    if (argc == 5) {
	int pattern;
	if (strcmp(argv[1], "all") == 0) {
	    pattern = RPSYNC_SEQUENCE_ALL;
	} else if (strcmp(argv[1], "13_24") == 0) {
	    pattern = RPSYNC_SEQUENCE_13_24;
	} else if (strcmp(argv[1], "1234") == 0) {
	    pattern = RPSYNC_SEQUENCE_1234;
	} else {
	    fprintf(stderr, "bad pattern: %s\n", argv[1]);
	    exit(1);
	}
	double offset = atof(argv[2]);
	double onTime = atof(argv[3]);
	double offTime = atof(argv[4]);
	rpsync_start_periodic(pattern, offset, onTime, offTime);
	return 0;
    }
    
    char* endp = 0;
    int bitmap = (int) strtol(argv[1], &endp, 0);
    if (endp && *endp != '\0') {
        fprintf(stderr, "invalid number: %s\n", argv[1]);
        exit(1);
    }

    int i;
    for (i = 0; i < 4; i++) {
        if (bitmap & (1 << i)) {
            printf("*");
        } else {
            printf("-");
        }
    }
    printf("\n");

    if (argc == 2) {
	rpsync_set(bitmap);
    } else if (argc == 3) {
	double sec = atof(argv[2]);
	rpsync_set_sec(bitmap, sec);
    } else {
	usage();
    }
    return 0;
}
