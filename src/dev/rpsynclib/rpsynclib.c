#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>

#include <wiringPi.h>
#include <wiringPiSPI.h>

// define PI_MODLE for CM3

#ifndef PI_MODEL_CM3
#define PI_MODEL_CM3 10
#endif

#include "rpsync_bitmap.h"
#include "cmmb_compat_bitmap.h"

#define RPSYNC_GPIO_CRESET  26  /* Pi pin 37, CM pin 87 */
#define RPSYNC_GPIO_CDONE   4   /* Pi pin  7, CM pin 15  */
#define RPSYNC_GPIO_NCS0    8   /* Pi pin 24, CM pin 27, SPI NCS0 */
#define RPSYNC_GPIO_CTRL    5   /* Pi pin 29 */
#define RPSYNC_GPIO_LENB    6   /* Pi pin 31 */
#define CMMB1_GPIO_CTRL     34  /* CM pin 52 */
#define CMMB1_GPIO_LENB     35  /* CM pin 54 */

#define FPGA_CONFIG_BUFFER_SIZE  1024
#define FPGA_CONFIG_DUMMY_SIZE   16
static uint8_t fpga_config_buffer[FPGA_CONFIG_BUFFER_SIZE];

#define CMD_NOP               0
#define CMD_RESET             1
#define CMD_SET_MODE          2
#define CMD_SET_RELOAD        3
#define CMD_SET_ALT_RELOAD    4
#define CMD_SET_COMPARE       5

static bool board_is_cmmb1;

static int spi_send_recv(uint32_t send, uint32_t* recvp)
{
    uint32_t data = send;
    
    if (wiringPiSPIDataRW(0, (void*) &data, 4) < 0) {
	perror("wiringPiSPIDataRW");
	return -1;
    }
    if (recvp) {
	*recvp = data;
    }

    return 0;
}

static int spi_command(int command, int arg)
{
    uint32_t send = (command << 24) | (arg & 0xffffff);
    uint32_t recv;
    if (spi_send_recv(send, &recv) < 0) {
	return -1;
    }
    //printf("send: %08X  recv: %08X\n", send, recv);
    return 0;
}

static int configure_fpga(void)
{
    pinMode(RPSYNC_GPIO_NCS0, OUTPUT);
    pinMode(RPSYNC_GPIO_CRESET, OUTPUT);
    pinMode(RPSYNC_GPIO_CDONE, INPUT);
    if (board_is_cmmb1) {
	pinMode(CMMB1_GPIO_CTRL, OUTPUT);
	pinMode(CMMB1_GPIO_LENB, OUTPUT);

	digitalWrite(CMMB1_GPIO_CTRL, 0);
	digitalWrite(CMMB1_GPIO_LENB, 0);
    } else {
	pinMode(RPSYNC_GPIO_CTRL, OUTPUT);
	pinMode(RPSYNC_GPIO_LENB, OUTPUT);

	digitalWrite(RPSYNC_GPIO_CTRL, 0);
	digitalWrite(RPSYNC_GPIO_LENB, 0);
    }

    
    int spi = wiringPiSPISetupMode(0, 25000000, 0);
    if (spi < 0) {
        perror("wiringPiSPISetupMode");
        return -1;
    }

    // Assert CRESET
    digitalWrite(RPSYNC_GPIO_NCS0, 0); // ensure SPI Peripheral mode
    digitalWrite(RPSYNC_GPIO_CRESET, 0);
    delayMicroseconds(200+100);
    digitalWrite(RPSYNC_GPIO_CRESET, 1);
    delayMicroseconds(600+100);

    // Check CDONE
    if (digitalRead(RPSYNC_GPIO_CDONE) != 0) {
        fprintf(stderr, "ERROR: FPGA failed (CDONE not low)\n");
	return -1;
    }

    const uint8_t* ptr = board_is_cmmb1 ? cmmb_compat_bitmap : rpsync_bitmap;
    uint32_t count
	= board_is_cmmb1 ? cmmb_compat_bitmap_size : rpsync_bitmap_size;
    while (count > 0) {
	int len = count;
	if (len >= FPGA_CONFIG_BUFFER_SIZE) {
	    len = FPGA_CONFIG_BUFFER_SIZE;
	}
	memcpy(fpga_config_buffer, ptr, len);
	if (wiringPiSPIDataRW(0, fpga_config_buffer, len) < 0) {
	    perror("wiringPiSPIDataRW");
	    return -1;
	}
	ptr += len;
	count -= len;
    }

    memset(fpga_config_buffer, 0, FPGA_CONFIG_DUMMY_SIZE);
    if (wiringPiSPIDataRW(0, fpga_config_buffer, FPGA_CONFIG_DUMMY_SIZE) < 0) {
	perror("wiringPiSPIDataRW");
	return -1;
    }

    // Check CDONE
    if (digitalRead(RPSYNC_GPIO_CDONE) == 0) {
        fprintf(stderr, "ERROR: FPGA failed (CDONE not high)\n");
	return -1;
    }
    
    return 0;
}

static int bitmap_expand_4x(int bitmap)
{
    bitmap &= 0xf;
    return (bitmap << 12) | (bitmap << 8) | (bitmap << 4) | bitmap;
}

static int sec_double_to_int(double sec)
{
    return (int) lround(sec * 12e6) & 0xffffff;
}

static void checkBoardType(void)
{
    int model, rev, mem, maker, overVolted;
    piBoardId(&model, &rev, &mem, &maker, &overVolted);
#if 0
    fprintf(stderr,
	    "piBoardId: model=%d, rev=%d, mem=%d, maker=%d, overvolted=%d\n",
	    model, rev, mem, maker, overVolted);
#endif
    if (model == PI_MODEL_CM || model == PI_MODEL_CM3) {
	board_is_cmmb1 = true;
    } else {
	board_is_cmmb1 = false;
    }
}

// ----------- API -----------

int rpsync_open(void)
{
    if (geteuid() != 0) {
        fprintf(stderr, "ERROR: must be root\n");
	return -1;
    }

    wiringPiSetupGpio();

    checkBoardType();

    if (configure_fpga() < 0) {
        return -1;
    }
    spi_command(CMD_RESET, 0);
    if (board_is_cmmb1) {
	digitalWrite(CMMB1_GPIO_LENB, 1);
    } else {
	digitalWrite(RPSYNC_GPIO_LENB, 1);
    }
    
    return 0;
}

int rpsync_close(void)
{
    return 0;
}

int rpsync_set(int bitmap)
{
    return spi_command(CMD_SET_MODE, 0x800000 + bitmap_expand_4x(bitmap));
}

int rpsync_set_sec(int bitmap, double sec)
{
    int i = sec_double_to_int(sec);
    spi_command(CMD_SET_COMPARE, i);
    spi_command(CMD_SET_RELOAD, i);
    int b = bitmap_expand_4x(bitmap);
    spi_command(CMD_SET_MODE, 0x430000 + b);
    return spi_command(CMD_SET_MODE, 0x410000 + b);
}

int rpsync_enable_laser(int enb)
{
    if (board_is_cmmb1) {
	digitalWrite(CMMB1_GPIO_LENB, enb ? 1 : 0);
    } else {
	digitalWrite(RPSYNC_GPIO_LENB, enb ? 1 : 0);
    }
    return 0;
}

static double last_period;

int rpsync_start_periodic(int sequence, double startTimeOffset,
			  double onTime, double offTime)
{
    int compare = sec_double_to_int(onTime);
    int reload1 = sec_double_to_int(onTime + startTimeOffset);
    int reload2 = sec_double_to_int(onTime + offTime);
    spi_command(CMD_SET_COMPARE, compare);
    spi_command(CMD_SET_RELOAD, reload1);
    spi_command(CMD_SET_MODE, 0x430000 + (sequence & 0xffff));
    spi_command(CMD_SET_RELOAD, reload2);
    last_period = onTime + offTime;
    return 0;
}

int rpsync_shift_periodic_phase(double shiftTime)
{
    return spi_command(CMD_SET_ALT_RELOAD,
		       sec_double_to_int(last_period + shiftTime));
}

int rpsync_stop_periodic(void)
{
    return spi_command(CMD_RESET, 0);
}
