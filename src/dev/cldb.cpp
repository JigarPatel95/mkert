/*
 * cldb.cpp - wrapper for Okada's Ultra96+ CLDB board
 * 
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 * 
 * Based on libcldb
 */

/* -------------------------------------------------------------------------- */

#include "cldb.h"

#include <thread>
#include <cmath>

#include <exception>

#include <cldblib.h>
#include <string.h>

/* -------------------------------------------------------------------------- */

using namespace mke::dev;

/* -------------------------------------------------------------------------- */

const int IMAGE_BUFFER_SIZE = 2048 * 1024;

/* -------------------------------------------------------------------------- */

Cldb::Cldb(int buffsize)
: handle_(nullptr), camera_name_("MkE CLDB: Not initialized"), buffer_(buffsize), 
laser_pattern_(0), gain_(-1), explines_(-1), frame_delay_(0)
    
{    
    initialize();
}

/* -------------------------------------------------------------------------- */

Cldb::~Cldb()
{
    finalize();
}

/* -------------------------------------------------------------------------- */

void Cldb::initialize()
{
    // initialize objects

    CLDB* cldb = cldb_open();
    handle_ = cldb;
    
    if (!cldb)
        throw std::runtime_error("Unable to connect to CLDB");

    switch (cldb->camtype) {
    case CLDB_CAMERA_NOT_AVAILABLE:
        camera_name_ = "MKE CLDB: No Camera";
        throw std::runtime_error("No camera connected to CLDB");

    case CLDB_CAMERA_TYPE_MT9V034:
        camera_name_ = "MKE CLDB: MT9V034";
        image_height_ = 480;
        image_width_ = image_pitch_ = 752;
        break;
    case CLDB_CAMERA_TYPE_AR0134CS:
        camera_name_ = "MKE CLDB: AR0134CS";
        image_height_ = 960;
        image_width_ = image_pitch_ = 1280;
        break;
    case CLDB_CAMERA_TYPE_AR0135CS:
        camera_name_ = "MKE CLDB: AR0135CS";
        image_height_ = 960;
        image_width_ = image_pitch_ = 1280;
        break;
    case CLDB_CAMERA_TYPE_AR0135AT:
        camera_name_ = "MKE CLDB: AR0135AT";
        image_height_ = 960;
        image_width_ = image_pitch_ = 1280;
        break;
    default:
        printf("Camera: %d\n", cldb->camtype);
        throw std::runtime_error("MKE CLDB: Unsupported camera type");
    }
    switch (cldb->camtype) {
    case CLDB_CAMERA_TYPE_MT9V034:
    case CLDB_CAMERA_TYPE_AR0134CS:
    case CLDB_CAMERA_TYPE_AR0135CS:
    case CLDB_CAMERA_TYPE_AR0135AT:
    case CLDB_CAMERA_NOT_AVAILABLE:
        sync_source_ = CLDB_SOURCE_PCAM;
        break;
    default:
        sync_source_ = CLDB_SOURCE_OFF;
        break;
    }

    std::cout << "CLDB: " << camera_name_.c_str() << std::endl;    
    
    // little hack to start camera capturing
    
    setLaserSequence(0x0001);
    setLaserSequence(0x0000);

    // initialize buffers
    
    for(std::vector<Buffer>::iterator b = buffer_.begin(); b != buffer_.end(); ++b)
      {
        b->resize(IMAGE_BUFFER_SIZE);
        available_.push(b->data());
      }
    
    assert(running_ == false);
    
    if(buffer_.size() < 3)
        throw std::runtime_error("Too less number of buffers for compressed stream");
}

/* -------------------------------------------------------------------------- */

void Cldb::finalize()
{
    if (running_)
        stopCapture();
    
    cldb_close(reinterpret_cast<CLDB*>(handle_));
}

/* -------------------------------------------------------------------------- */

void Cldb::startCapture()
{
    thread_ = std::thread(&Cldb::threadProc, this);
}

/* -------------------------------------------------------------------------- */

void Cldb::stopCapture()
{
    running_ = false;
    thread_.join();
}

/* -------------------------------------------------------------------------- */

void Cldb::setShutterSpeed(float shutter)
{
    std::lock_guard<std::mutex> lock(shared_);

//    explines_ = round(shutter/0.00002961f);
    explines_ = round(shutter/0.0000187f);
    cldb_cam_set_exp_gain(reinterpret_cast<CLDB*>(handle_), explines_, gain_);
}

/* -------------------------------------------------------------------------- */

void Cldb::setGain(float gain)
{
    std::lock_guard<std::mutex> lock(shared_);

    cldb_cam_set_exp_gain(reinterpret_cast<CLDB*>(handle_), explines_, gain);
    gain_ = gain;
}

/* -------------------------------------------------------------------------- */

float Cldb::getShutterSpeed() const
{
    std::lock_guard<std::mutex> lock(shared_);
    return cldb_get_exposure_time(reinterpret_cast<CLDB*>(handle_));
}

/* -------------------------------------------------------------------------- */

float Cldb::getGain() const
{
    std::lock_guard<std::mutex> lock(shared_);
    return gain_;
}

/* -------------------------------------------------------------------------- */

void Cldb::setLaserSequence(uint16_t pattern)
{
    std::lock_guard<std::mutex> lock(shared_);

    CLDB_SOURCE src = pattern == 0 ? CLDB_SOURCE_OFF : CLDB_SOURCE(sync_source_);
    cldb_set_laser(reinterpret_cast<CLDB*>(handle_), src, pattern);
    laser_pattern_ = pattern;
}

/* -------------------------------------------------------------------------- */

uint16_t Cldb::getLaserSequence() const
{
    return laser_pattern_;
}

/* -------------------------------------------------------------------------- */

void Cldb::threadProc()
{
    running_ = true;


    auto last_frame = std::chrono::high_resolution_clock::now() - std::chrono::milliseconds(frame_delay_);
    
    try
    {
        while(running_)
        {
            
            auto now = std::chrono::high_resolution_clock::now();
            
            last_frame = now;
            
            // capture              
            
            Pix8 * buff = nullptr;
            {
                std::lock_guard<std::mutex> lock(shared_);
                if (available_.size() > 0)
                {
                    buff = available_.front();
                    available_.pop();                    
                }  
            }
            
            if(buff)
                {
                    uint8_t * image_ptr;
                    uint32_t stamp;
                    uint8_t phase;
                    float exp;
                    
                    while(running_)
                        {
                            if (!cldb_capture_frame(reinterpret_cast<CLDB*>(handle_), &image_ptr, &stamp, &phase, &exp))
                                continue;
                            memcpy(buff, image_ptr, image_pitch_*image_height_);
                            videoFrameReceived(buff, image_width_, image_height_, image_pitch_, phase);
                            break;
			}
                    
                    if (!running_)
                        returnBackBuffer(buff);

                    std::this_thread::sleep_for(std::chrono::milliseconds(frame_delay_));
                    
                }
            else
                {
                    std::cerr << "NO BUFFER" << std::endl;
                }
        } 
    }
    catch (std::exception & ex) 
    {
        std::cerr << ex.what() << std::endl;
        err_ = std::current_exception();
    }
    catch (...)
    {
        std::cerr << "unknown exception" << std::endl;
        err_ = std::current_exception();
    }
    
    running_ = false;
}

/* -------------------------------------------------------------------------- */

void Cldb::returnBackBuffer(const Pix8 * buff)
{
    std::lock_guard<std::mutex> lock(shared_);
    
    available_.push(const_cast<Pix8 *>(buff));
}

/* -------------------------------------------------------------------------- */

float Cldb::getBoardTemperature(int sensorId) const 
{
    return cldb_read_temperature(reinterpret_cast<CLDB*>(handle_), sensorId);
}

/* -------------------------------------------------------------------------- */


