/*
 * cldb.h - wrapper for Okada Ultra96 Camera Laser Daughter Board
 * 
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 * 
 * Based on libcldb
 */

/* -------------------------------------------------------------------------- */

#ifndef _CLDBCAM_H_
#define _CLDBCAM_H_

/* -------------------------------------------------------------------------- */

// STL includes

#include <exception>
#include <stdexcept>
#include <iostream>
#include <sstream>

#include <mutex>
#include <thread>
#include <queue>

#include <cmath>

#include "img.h"

/* -------------------------------------------------------------------------- */

namespace mke {
namespace dev {

/* -------------------------------------------------------------------------- */

// camera class

class Cldb

{  
public:
  // constructor
  Cldb(int buffer_size);
  
  // destructor
  virtual ~Cldb();
  
  // get sensor name
  const char * getName() { return camera_name_.c_str(); };
  
  // negative number -> autoexp
  void setShutterSpeed(float shutter);
  
  // negative number -> auto gain
  void setGain(float gain);
  
  float getShutterSpeed() const;
  
  float getGain() const;
  
  void setLaserSequence(uint16_t pattern);
  
  uint16_t getLaserSequence() const;
  
  // start capturing
  void startCapture();
  
  // stop capture
  void stopCapture();
    
  // set FPS
  void setFps(int fps) { this->frame_delay_ = fps == 0 ? 0 : std::round(1000./fps); }
  
  void returnBackBuffer(const Pix8 * buff);
  
  virtual void videoFrameReceived(const Pix8 * buff, int w, int h, int memw, uint16_t phase) = 0;  
  
  int getImageWidth() const { return image_width_; }

  int getImageHeight() const { return image_height_; }
  
  float getBoardTemperature(int sensorId) const;

protected:    
  typedef std::vector<Pix8> Buffer;
  
  typedef void * HANDLE; // to avoid outer dependency on cldb.h

  HANDLE                handle_;
  
  std::string           camera_name_;
  
  std::thread           thread_;
  volatile bool         running_;
  
  uint16_t              laser_pattern_;
  mutable std::mutex    shared_;
  
  std::vector<Buffer>   buffer_;
  std::queue<Pix8 *>    available_;

  float                 gain_;
  int                   explines_;
  int                   image_width_;
  int                   image_height_;
  int                   image_pitch_;
  
  int                   sync_source_;
  
  uint32_t              frame_delay_;
    
  std::exception_ptr    err_;
  
  void initialize();
  
  void finalize();
  
  void threadProc();  
};

/* -------------------------------------------------------------------------- */

}   // end of mke::dev
}   // end of mke

/* -------------------------------------------------------------------------- */

#endif // _CLDBCAM_H_
