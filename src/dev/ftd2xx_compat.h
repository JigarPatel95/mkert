/*
 * ftd2xx_compat.h - ftd2xx compatible functions using libftdi
 * 
 * Copyright (c) 2018, Magik-Eye Inc.
 * author: Jyunji Kondo
 */

#ifndef FTD2XX_COMPAT_H
#define FTD2XX_COMPAT_H

#include <libftdi1/ftdi.h>
#ifdef __ANDROID__
#include <android/log.h>

#define LOGE(...) ((void)__android_log_print(ANDROID_LOG_ERROR, "mke::dev::FtdiLib", __VA_ARGS__))
#else
#define LOGE(...)
#endif

typedef unsigned int DWORD;
typedef unsigned int ULONG;
typedef void *PVOID;
typedef unsigned char UCHAR;
typedef unsigned short USHORT;
typedef PVOID LPVOID;
typedef DWORD *LPDWORD;

typedef PVOID FT_HANDLE;
typedef ULONG FT_STATUS;

enum {
	FT_OK,
	FT_OTHER_ERROR,
};

#define FT_BITMODE_RESET BITMODE_RESET
#define FT_BITMODE_SYNC_FIFO BITMODE_SYNCFF

#define FT_FLOW_RTS_CTS SIO_RTS_CTS_HS

#define FT_PURGE_RX 1

static struct ftdi_context *ftdi_;
static int vender_id_;
static int product_id_;
static int fd_;
static bool first_read_ = true;

FT_STATUS FT_Open(int deviceNumber, FT_HANDLE *pHandle)
{
	ftdi_ = ftdi_new();
	ftdi_register_fd(ftdi_, vender_id_, product_id_, fd_);
	if (ftdi_usb_open(ftdi_, vender_id_, product_id_) != 0) {
		LOGE("can't open usb device");
		return FT_OTHER_ERROR;
    	}
	return FT_OK;
}

FT_STATUS FT_SetBitMode(FT_HANDLE ftHandle, UCHAR ucMask, UCHAR ucEnable)
{
	if (ftdi_set_bitmode(ftdi_, ucMask, ucEnable) != 0) {
		LOGE("can't set bit mode");
		return FT_OTHER_ERROR;
	}
	return FT_OK;
}

FT_STATUS FT_SetLatencyTimer(FT_HANDLE ftHandle, UCHAR ucLatency)
{
	if (ftdi_set_latency_timer(ftdi_, ucLatency) != 0) {
		LOGE("can't set latency timer");
		return FT_OTHER_ERROR;
	}
	return FT_OK;
}

FT_STATUS FT_SetUSBParameters(FT_HANDLE ftHandle, ULONG ulInTransferSize, ULONG ulOutTransferSize)
{
	if (ftdi_read_data_set_chunksize(ftdi_, ulInTransferSize) != 0) {
		LOGE("can't set read data chunk size");
		return FT_OTHER_ERROR;
	}
	if (ftdi_write_data_set_chunksize(ftdi_, ulOutTransferSize) != 0) {
		LOGE("can't set write data chunk size");
		return FT_OTHER_ERROR;
	}
	return FT_OK;
}

FT_STATUS FT_SetFlowControl(FT_HANDLE ftHandle, USHORT FlowControl, UCHAR XonChar, UCHAR XoffChar)
{
	if (ftdi_setflowctrl(ftdi_, FlowControl) != 0) {
		LOGE("can't set flow control");
		return FT_OTHER_ERROR;
	}
	return FT_OK;
}

FT_STATUS FT_Purge(FT_HANDLE ftHandle, ULONG Mask)
{
	if (Mask != FT_PURGE_RX) {
		LOGE("only purging rx buffer supported");
		return FT_OTHER_ERROR;
	}
	if (ftdi_usb_purge_rx_buffer(ftdi_) != 0) {
		LOGE("can't purge rx buffer");
		return FT_OTHER_ERROR;
	}
	return FT_OK;
}

FT_STATUS FT_SetTimeouts(FT_HANDLE ftHandle, ULONG ReadTimeout, ULONG WriteTimeout)
{
	ftdi_->usb_read_timeout = ReadTimeout;
	ftdi_->usb_write_timeout = WriteTimeout;
	return FT_OK;
}

FT_STATUS FT_Read(FT_HANDLE ftHandle, LPVOID lpBuffer, DWORD dwBytesToRead, LPDWORD lpBytesReturned)
{
	// *lpBytesReturned = ftdi_read_data(ftdi_, (unsigned char *)lpBuffer, dwBytesToRead);
	// if (*lpBytesReturned < 0) {
	// 	LOGE("%s failed: %d", __FUNCTION__, *lpBytesReturned);
	// 	return FT_OTHER_ERROR;
	// }
	// return FT_OK;

	int retry = 5;
    while (true) {
        int r = ftdi_read_data(ftdi_, (unsigned char *)lpBuffer, dwBytesToRead);

        {
            /**
             * Workaround:
             * Skip misterious 2 bytes read at first time.
             */
            if (r == 2 && first_read_) {
                first_read_ = false;
                LOGE("Skip misterious 2 bytes read.");
                continue;
            }
        }

        {
            /**
             * Workaround:
             * Fill misterious bytes shortage.
             */
            switch (dwBytesToRead - r) {
                case 2:
                    ((unsigned char *)lpBuffer)[dwBytesToRead - 2] = 0;
                    /* FALLTHROUGH */
                case 1:
                    ((unsigned char *)lpBuffer)[dwBytesToRead - 1] = 0;
                    r = dwBytesToRead;
                    LOGE("Fill misterious bytes shortage.");
                    break;
                default:
                    break;
            }
        }
        
        if (r < 0) {
            LOGE("%s: ftdi_read_data failed: %d", __FUNCTION__, r);
            return FT_OTHER_ERROR;
        }
		if (r == 0 && retry > 0) {
			// retry if read 0 byte.
			retry--;
			continue;
		}
		*lpBytesReturned = r;
		return FT_OK;
    }
}

FT_STATUS FT_Write(FT_HANDLE ftHandle, LPVOID lpBuffer, DWORD dwBytesToWrite, LPDWORD lpBytesWritten)
{
	*lpBytesWritten = ftdi_write_data(ftdi_, (unsigned char *)lpBuffer, dwBytesToWrite);
	if (*lpBytesWritten < 0) {
		LOGE("%s failed: %d", __FUNCTION__, *lpBytesWritten);
		return FT_OTHER_ERROR;
	}
	return FT_OK;
}

FT_STATUS FT_GetStatus(FT_HANDLE ftHandle, DWORD *dwRxBytes, DWORD *dwTxBytes, DWORD *dwEventDWord)
{
	return FT_OK;
}

void _registerUsbFd(int vid, int pid, int fd)
{
    vender_id_ = vid;
    product_id_ = pid;
    fd_ = fd;
}

#endif	/* FTD2XX_COMPAT_H */
