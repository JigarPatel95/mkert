/*
 * RasPiCam.cpp - wrapper for Raspberry PI camera
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 * 
 * Ideas based on code from RaspiVidYUV cloned from 
 * 									https://github.com/raspberrypi/userland.git
 */

/* -------------------------------------------------------------------------- */

#ifndef _RASPICAM_H_
#define _RASPICAM_H_

/* -------------------------------------------------------------------------- */

// STL includes

#include <exception>
#include <stdexcept>
#include <iostream>
#include <sstream>

/* -------------------------------------------------------------------------- */

// mmal struct

struct MMAL_COMPONENT_T;
struct MMAL_POOL_T;
struct MMAL_PORT_T;
struct MMAL_BUFFER_HEADER_T;
struct MMAL_PARAMETER_CAMERA_SETTINGS_T;

/* -------------------------------------------------------------------------- */

namespace mke {
namespace dev {
	
// Error thrown by RasPiCamera

class MmalError : public std::runtime_error

{
  int                           mmal_status_;	// MMAL status raised the exception
  std::string                   desc_;
  
public:
  // constructor with reason
  MmalError(int mmal_status, const char* desc);
  
  // destructor
  virtual ~MmalError()
  {};
  
  // describe exception
  virtual const char* what() const throw();

  // convert MMAL_STATUS to const char*
  static const char * statusToStr(int status);
};

/* -------------------------------------------------------------------------- */

// Raspberry PI camera

/* -------------------------------------------------------------------------- */

/* Supported sensor modes: - for OV5647
 * 
 *   ID| resolution     | FPS       | Video | Image | FOV	| Binning
 * ----+----------------+-----------+-------+-------+-----------+---------
 *    0: auto           | ?         | ?     | ?     | ?         | ?
 *    1: 1920x1080      | 1-30      | x     |       | partial   | none
 *    2: 2592x1944      | 1-15      | x     | x     | full      | none
 *    3: 2592x1944      | 0.1666-1  | x     | x     | full      | none
 *    4: 1296x972       | 1-42      | x     |       | full      | 2x2
 *    5: 1296x730       | 1-49      | x     |       | full      | 2x2
 *    6: 640x480        | 42.1-60   | x     |       | full      | 4x4
 *    7: 640x480        | 60.1-90   | x     |       | full      | 4x4
 */

/* -------------------------------------------------------------------------- */

// camera class

class RasPiCam

{
protected:
  
  // MMAL variables
  
  MMAL_COMPONENT_T  *   camera_component_;  // camera component
  MMAL_PORT_T       *   video_port_;        // port for video
  MMAL_PORT_T       *   control_port_;        // port for video
  MMAL_POOL_T       *   pool_;              // buffer pool used by video
/*
  RASPICAM_CAMERA_PARAMETERS 
						  camera_parameters_; // Camera setup parameters
*/
  // camera internal parameters
  
  int                   camera_id_;         // camera identification
  int                   sensor_mode_;       // Sensor mode.0=auto.Check docs
  uint                  num_buffers_;       // number of buffer to be used
  
  // video stream parameters
  // TODO: crop should be added for full functionality
  
  int                   width_;             // image width
  int                   height_;            // image height
  int                   stride_;            // row memory width
  int                   framerate_;         // FPS
  
  bool                  camera_control_enabled_; // is camera control callback enabled
  
  int                   max_width_;         // maximum width given from sensor
  int                   max_height_;        // maximum height given from sensor
  std::string           camera_name_;       // camera name
  
  // check for MMAL error
  inline static void checkError(int mmal_status, const char * err_desc);
  
  // create the camera component, set up its ports
  void initCamera(int sensor_mode);

  // parse camera info
  void parseCameraInfo();
  
  // destroy the camera component
  void shutdownCamera();
  
  // set fixed params
  void setFixedParams(bool fixed = true);
  
  // prepare video port
  void prepareVideoPort();

  // enable video port
  void enableVideoPort(uint buffersize);
  
  // disable video port
  void disableVideoPort();
  
  // FIXME: not necessary right now
  // Callback will dump buffer data to the specific file
  static void cameraControlCallback(MMAL_PORT_T *port, 
									MMAL_BUFFER_HEADER_T *buffer);
	
  // Callback will dump buffer data to internal buffer
  static void cameraBufferCallback(MMAL_PORT_T *port, 
								   MMAL_BUFFER_HEADER_T *buffer);

  // process video frame
  virtual void videoFrameReceived(MMAL_PORT_T *port, 
                                  MMAL_BUFFER_HEADER_T *buffer) = 0;
                                  
  // process control frame
  virtual void controlFrameReceived(MMAL_PORT_T * port, uint32_t shutter_speed,
                                    uint32_t gain_analog, uint32_t gain_digital) = 0;

  // -- some wrapping for mmal
                                  
  // return back buffer to port queue
  void returnBackBuffer(MMAL_PORT_T *port, MMAL_BUFFER_HEADER_T *buffer);
  
  // lock buffer
  static void lockBuffer(MMAL_BUFFER_HEADER_T * buffer);
  
  // lock buffer
  static void unlockBuffer(MMAL_BUFFER_HEADER_T * buffer);
  
  // get info from buffer
  static void getInfo(MMAL_BUFFER_HEADER_T * buffer, 
                             void * & data, int & length);

/* -------------------------------------------------------------------------- */
  
public:
  // own exposure table
  enum ExposureMode {
    EXPOSURE_OFF,
    EXPOSURE_AUTO,
    EXPOSURE_VERYLONG,
    EXPOSURE_NIGHT,
    EXPOSURE_FIXEDFPS,
    EXPOSURE_SPORTS,
    EXPOSURE_MAX
  };
  
  // constructor
  RasPiCam(int cameraid, int sensormode, int num_buffers = 10, bool enable_control_frames = false);
  
  // destructor
  virtual ~RasPiCam();
  
  // set video mode
  void setVideoMode(int sensormode);
  
  // get sensor name
  const char * getName() { return camera_name_.c_str(); };
  
  // set shutter speed in microseconds
  void setShutterSpeed(uint32_t shutter);
  
  // set prefered ISO
  void setISO(uint32_t iso);
  
  // set white balance
  void setWB(bool autowb, unsigned int r = 0, unsigned int b = 0);

  // set exposure mode
  void setExposureMode(ExposureMode mode);
  
  // set exposure compensation
  void setExposureCompensation(int ev);
  
  // set video format
  void setVideoFormat(int width, int height, int framerate);
  
  // start capturing
  void startCapture();
  
  // stop capture
  void stopCapture();
};

/* -------------------------------------------------------------------------- */

};	// end of mke::dev namespace
};	// end of mke namespace

/* -------------------------------------------------------------------------- */

#endif // _RASPICAM_H_
