/*
 * FpCam.cpp - wrapper for Okada FPGA camera - FpCam
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 * 
 * Based on FTDI D2XX protocol 
 */

/* -------------------------------------------------------------------------- */

#ifndef _FPCAM_H_
#define _FPCAM_H_

/* -------------------------------------------------------------------------- */

// STL includes

#include <exception>
#include <stdexcept>
#include <iostream>
#include <sstream>

#include <mutex>
#include <thread>
#include <queue>

#include <cmath>

#include "img.h"

/* -------------------------------------------------------------------------- */

namespace mke {
namespace dev {

/* -------------------------------------------------------------------------- */

// camera class

class FpCam

{  
public:
  struct SystemStatus {
    uint32_t magic;

    uint32_t exposure_time;

    uint32_t fpga_chip_id[2];
    uint32_t fpga_system_id;
    uint32_t fpga_system_timestamp;

    uint16_t temp_sensor_value[4];
    uint16_t imu_value[10];
  };
    
  // constructor
  FpCam(int portid, int buffer_size, bool compressed = false);
  
  // destructor
  virtual ~FpCam();
  
  // get sensor name
  const char * getName() { return camera_name_.c_str(); };
  
  void setShutterSpeed(double shutter, bool auto_exp = false);
  
  void setGain(double gain, bool auto_gain = false);
  
  double getShutterSpeed();
  
  double getGain();
  
  void setLaserSequence(uint16_t pattern, int num);
  
  uint16_t getLaserSequence();
  
  int getLaserSequenceLength();
  
  SystemStatus getStatus();
  
  // start capturing
  void startCapture();
  
  // stop capture
  void stopCapture();
  
  // set FPS
  void setFps(int fps) { this->frame_delay_ = fps == 0 ? 0 : std::round(1000./fps); }
  
  void returnBackBuffer(const Pix8 * buff);
  
  virtual void videoFrameReceived(const Pix8 * buff, int w, int h, int memw, uint16_t phase) = 0;  
  
  int getImageWidth() const { return image_width_; }

  int getImageHeight() const { return image_height_; }

#ifdef __ANDROID__
  void registerUsbFd(int vid, int pid, int fd);
#endif

protected:
  enum CommFlags {  COMM_WRITE_LASERS = 0x01,
                    COMM_WRITE_EXPOSURE = 0x02,
                    COMM_CAPTURE = 0x04 };

  enum ApiDict { API_CAPTURE = 0x00,
                 API_CAPTURE_COMPRESSED,
                 API_CAPTURE_MASK,
                 API_GET_IMAGE,
                 API_GET_IMAGE_MASK,
                 API_GET_IMAGE_SEGMENT_MASK,
                 API_SEND_STATUS,
                 API_LASER_CTL,
                 API_EXP_GAIN_CTL,
                 API_STOP_CAPTURE,
                 API_RESUME_CAPTURE,
                 API_SEGMENT_SIZE,
                 __API_NUM };
                    
  const uint32_t STATUS_MAGIC_NOCAM   = 0x30456b4d; // 'MkE0'
  const uint32_t STATUS_MAGIC_MT9V034 = 0x73456b4d; // 'MkEs'
  const uint32_t STATUS_MAGIC_AR0134  = 0x48456b4d; // 'MkEH'
  
  struct ExpGainParam {

    bool auto_exposure, auto_gain;
    int16_t exposure_value, gain_value;

    ExpGainParam() : auto_exposure(true), auto_gain(true),
        exposure_value(1), gain_value(16) {}
  };
    
  typedef std::vector<Pix8> Buffer;
  
  typedef void * HANDLE; // to avoid outer dependency on ftd2xx.h
  
  int                   port_id_;         // camera identification
  unsigned int          num_buffers_;     // number of buffer to be used
  std::string           camera_name_;
  bool                  compressed_;      // should be used compressed channel
  
  HANDLE                ft_handle_;
  
  std::thread           thread_;
  volatile bool         running_ = false;
  volatile uint32_t     flags_ = 0;
  
  int imageSegmentCount;
  
  SystemStatus          curr_status_;
  ExpGainParam          exp_params_;
  uint16_t              laser_pattern_;
  uint16_t              laser_pattern_len_;
  uint8_t               capture_mask_;
  volatile bool         alternate_;
  std::mutex            shared_;
  
  std::vector<Buffer>   buffer_;
  std::queue<Pix8 *>    available_;
  
  uint32_t              lastMagic_;
  int                   image_width_;
  int                   image_height_;
  int                   image_pitch_;
  
  uint32_t              frame_delay_;
  
  int                   api_version_;
  static int            api_v1_[__API_NUM];
  static int            api_v2_[__API_NUM];
  const int         *   api_dict_;
  
  std::exception_ptr    err_;
  
  void initialize();
  
  void finalize();
  
  void threadProc();  
  
  void checkCameraType();
  
  bool checkApiVersion();
  
  template <typename RetType>
  inline RetType apiCmd(ApiDict api_key)
  {
      int val = api_dict_[api_key];
      
      if(val < 0)
          throw std::runtime_error("BAD API cmd call");
      
      return RetType(val);
  };
  
  // it is necessary to have intilialized whole memory
  static bool decompress(Pix8 * data, uint32_t len, Pix8 * dst, uint32_t buff_size);
};

/* -------------------------------------------------------------------------- */

}   // end of mke::dev
}   // end of mke

/* -------------------------------------------------------------------------- */

#endif // _FP_CAM_
