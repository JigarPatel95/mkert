/*
 * RasPiCam.cpp - wrapper for Raspberry PI camera
 *
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 * Ideas based on code from RaspiVidYUV cloned from
 * 									https://github.com/raspberrypi/userland.git
 */

// FIXME: 1st three images are broken??
/* -------------------------------------------------------------------------- */

// MMAL includes

#include "bcm_host.h"

#include "interface/mmal/mmal.h"
#include "interface/mmal/mmal_logging.h"
#include "interface/mmal/mmal_buffer.h"
#include "interface/mmal/util/mmal_util.h"
#include "interface/mmal/util/mmal_util_params.h"
#include "interface/mmal/util/mmal_default_components.h"
#include "interface/mmal/util/mmal_connection.h"

/* -------------------------------------------------------------------------- */

#include <assert.h>
#include <stdio.h>

/* -------------------------------------------------------------------------- */

#include "raspicam.h"

/* -------------------------------------------------------------------------- */

// preface or logging macros

#ifdef LOG_WARN
#undef LOG_WARN
#endif 

#ifdef LOG_INFO
#undef LOG_INFO
#endif 

#ifdef LOG_FATAL
#undef LOG_FATAL
#endif 

#include "util/logging.h"

/* -------------------------------------------------------------------------- */

using namespace mke::dev;		// use our namespace

/* -------------------------------------------------------------------------- */

// defines

// Standard port setting for the camera component
#define MMAL_CAMERA_VIDEO_PORT 1 // MUST BE 1 FOR VIDEO MODE

/* -------------------------------------------------------------------------- */

// MmalError class

/* -------------------------------------------------------------------------- */

const char * MmalError::statusToStr(int status)

{
  switch(status)
    {
    case MMAL_ENOMEM:
      return "Out of memory";
    case MMAL_ENOSPC:
      return "Out of resources (other than memory)";
    case MMAL_EINVAL:
      return"Argument is invalid";
    case MMAL_ENOSYS:
      return "Function not implemented";
    case MMAL_ENOENT:
      return "No such file or directory";
    case MMAL_ENXIO:
      return "No such device or address";
    case MMAL_EIO:
      return "I/O error";
    case MMAL_ESPIPE:
      return "Illegal seek";
    case MMAL_ECORRUPT:
      return "Data is corrupt \attention FIXME: not POSIX";
    case MMAL_ENOTREADY:
      return "Component is not ready \attention FIXME: not POSIX";
    case MMAL_ECONFIG:
      return "Component is not configured \attention FIXME: not POSIX";
    case MMAL_EISCONN:
      return "Port is already connected ";
    case MMAL_ENOTCONN:
      return "Port is disconnected";
    case MMAL_EAGAIN:
      return "Resource temporarily unavailable. Try again later";
    case MMAL_EFAULT:
      return "Bad address";
    default :
      return "Unknown status error";
    }
}

/* -------------------------------------------------------------------------- */

// constructor with reason
MmalError::MmalError(int mmal_status, const char* desc) : 
std::runtime_error(desc), mmal_status_(mmal_status)

{
  std::ostringstream oss;
  oss.str("");
  oss << "MMALError no.";
  oss << mmal_status_ << "(" << statusToStr(mmal_status_) << "): "
      << runtime_error::what();
      
  desc_ = oss.str();  
};

/* -------------------------------------------------------------------------- */

const char * MmalError::what() const throw()

{
  return desc_.c_str();
}

/* -------------------------------------------------------------------------- */

// RasPiCam class

/* -------------------------------------------------------------------------- */

/* -------------------------------------------------------------------------- */

RasPiCam::RasPiCam(int cameraid, int sensormode, int num_buffers, bool enable_control_frames)
  : camera_component_(NULL), video_port_(NULL), pool_(NULL),
    camera_id_(cameraid), sensor_mode_(sensormode), num_buffers_(num_buffers),
    camera_control_enabled_(enable_control_frames),
    max_width_(0), max_height_(0), camera_name_("")

{
  // must be called because BroadCom GPU

  bcm_host_init();

  // read camera info 

  parseCameraInfo();
  
  LOG_INFO << "Try to connect camera " << camera_name_ << " with max_resolution: " << max_width_ << "x" << max_height_;
  
  // create camera component and setup all parameters

  initCamera(sensormode);

  // prepare video port of camera
  
  prepareVideoPort();
}

/* -------------------------------------------------------------------------- */

RasPiCam::~RasPiCam()

{
  // is already down ? 
  
  if(camera_component_)
    shutdownCamera();
}

/* -------------------------------------------------------------------------- */

void RasPiCam::checkError(int status, const char * err_desc)

{
  if(status != MMAL_SUCCESS)
    throw MmalError(status, err_desc);
}

/* -------------------------------------------------------------------------- */

void RasPiCam::initCamera(int sensor_mode)

{
  // check class members

  assert(camera_component_ == NULL);

  // prepare local members

  MMAL_COMPONENT_T	*	camera = NULL;

  // Create the component

  checkError(mmal_component_create(MMAL_COMPONENT_DEFAULT_CAMERA, &camera),
             "Failed to create camera component");
  
  // return back given objects

  camera_component_ = camera;

  // select camera

  MMAL_PARAMETER_INT32_T camera_num =
                {{MMAL_PARAMETER_CAMERA_NUM, sizeof(camera_num)}, camera_id_};

  std::cout << "look for camera " << camera_id_ << std::endl;
                
  checkError(mmal_port_parameter_set(camera->control, &camera_num.hdr),
             "Could not select camera");

  // check camera output ports

  if(!camera->output_num)
    checkError(MMAL_ENOSYS, "Camera doesn't have output ports");

  setVideoMode(sensor_mode);
  
  if(camera_control_enabled_)
    {
      MMAL_PARAMETER_CHANGE_EVENT_REQUEST_T change_event_request =
          {{MMAL_PARAMETER_CHANGE_EVENT_REQUEST, sizeof(MMAL_PARAMETER_CHANGE_EVENT_REQUEST_T)},
            MMAL_PARAMETER_CAMERA_SETTINGS, 1};

      checkError(mmal_port_parameter_set(camera->control, &change_event_request.hdr),
                "No camera settings events");
    
      // set reference back to object

      camera->control->userdata =(struct MMAL_PORT_USERDATA_T *) this;
      
      // Enable the camera, and tell it its control callback function

      checkError(mmal_port_enable(camera->control, RasPiCam::cameraControlCallback),
                "Unable to enable control port.");
    }  			   
/*
  	// set up the camera configuration - most parameters given from example

  	std::cout << width << ", " << height << ", " << framerate << std::endl;

  	{
  		MMAL_PARAMETER_CAMERA_CONFIG_T cam_config =
  		{
  			{ MMAL_PARAMETER_CAMERA_CONFIG, sizeof(cam_config) },
  			.max_stills_w = width,
  			.max_stills_h = height,
  			.stills_yuv422 = 0,
  			.one_shot_stills = 0,
  			.max_preview_video_w = width,
  			.max_preview_video_h = height,
  			.num_preview_video_frames = 3,
  			.stills_capture_circular_buffer_height = 0,
  			.fast_preview_resume = 0,
  			.use_stc_timestamp = MMAL_PARAM_TIMESTAMP_MODE_RESET_STC
  		};
  		mmal_port_parameter_set(camera->control, &cam_config.hdr);
  	}
  */

  // obsolved: video port setup was here

  // Enable component

//  checkError(mmal_component_enable(camera),
//               "Camera component couldn't be enabled");

  // setup all parameters
  // TODO: make lib no dependent ??
  /*
  	checkError(static_cast<MMAL_STATUS_T>(
  		raspicamcontrol_set_all_parameters(camera, &camera_parameters_)),
  			   "Unable to set parameters to camera");
  */
  
  setFixedParams();
}

/* -------------------------------------------------------------------------- */

void RasPiCam::setVideoMode(int sensor_mode)
{
  sensor_mode_ = sensor_mode;
  
  // set sensor mode

  checkError(mmal_port_parameter_set_uint32(camera_component_->control,
                      MMAL_PARAMETER_CAMERA_CUSTOM_SENSOR_CONFIG, sensor_mode_),
              "Could not set sensor mode");
}

/* -------------------------------------------------------------------------- */
  
void RasPiCam::parseCameraInfo()
{
  MMAL_COMPONENT_T *camera_info;

  // Default to the OV5647 setup
  
  camera_name_ = "OV5647";

  // Try to get the camera name and maximum supported resolution
  checkError(mmal_component_create(MMAL_COMPONENT_DEFAULT_CAMERA_INFO, 
                                   &camera_info),
             "Unable to create camera info component.");
  
  MMAL_PARAMETER_CAMERA_INFO_T param;
  param.hdr.id = MMAL_PARAMETER_CAMERA_INFO;
  param.hdr.size = sizeof(param)-4;  // Deliberately undersize to check firmware veresion
  MMAL_STATUS_T status = mmal_port_parameter_get(camera_info->control, &param.hdr);

  if(status != MMAL_SUCCESS)
    {
      // Running on newer firmware
      
      param.hdr.size = sizeof(param);
      status = mmal_port_parameter_get(camera_info->control, &param.hdr);
      
      if(status == MMAL_SUCCESS && param.num_cameras > static_cast<uint>(camera_id_))
        {
          max_width_ = param.cameras[camera_id_].max_width;
          max_height_ = param.cameras[camera_id_].max_height;
          camera_name_ = std::string(param.cameras[camera_id_].camera_name, 
                                    param.cameras[camera_id_].camera_name + MMAL_PARAMETER_CAMERA_INFO_MAX_STR_LEN);
        }
      else
        {
          LOG_WARN << "Unable to read camera info, keeping the defaults";
        }
    }
  else
    {
      LOG_WARN << "Unable to read camera info because old firmware, keeping the defaults";
    }
  
  mmal_component_destroy(camera_info);
}

/* -------------------------------------------------------------------------- */

void RasPiCam::setFixedParams(bool fixed)

{
  int zero_params[] = { //MMAL_PARAMETER_SHARPNESS,
			MMAL_PARAMETER_SATURATION,
			MMAL_PARAMETER_CONTRAST,
                        -1
		      };
		      
  MMAL_RATIONAL_T value = {0, 100};	// -100 .. 100
  for(int * param = zero_params; *param >= 0; ++param)
    {
      checkError(mmal_port_parameter_set_rational(camera_component_->control, 
						  *param, value),
		"Unable to fix parameters");
      
    }

  // brightness
  
  value = {50, 100};	// 0 .. 100
  checkError(mmal_port_parameter_set_rational(camera_component_->control, 
					      MMAL_PARAMETER_BRIGHTNESS, value),
	     "Unable to fix brightness");
}

/* -------------------------------------------------------------------------- */

void RasPiCam::prepareVideoPort()

{
  // check class members

  assert(pool_ == NULL);
  
  video_port_ = camera_component_->output[MMAL_CAMERA_VIDEO_PORT];  

  // prepare local members

  // Create pool of buffer headers for the output port to consume

  video_port_->buffer_num = num_buffers_;
  pool_ = mmal_port_pool_create(video_port_, 0, video_port_->buffer_size);

  if(!pool_)
    checkError(MMAL_ENOSYS,
               "Failed to create buffer header pool for camera video port");
}

/* -------------------------------------------------------------------------- */

void RasPiCam::enableVideoPort(uint num_buffers)

{
  checkError(mmal_component_enable(camera_component_), "Cannot enable camera component. Probably another instance of a camera application running.");  
  
  assert(video_port_ != NULL);

  // Ensure there are enough buffers to avoid dropping frames

  if(video_port_->buffer_num < num_buffers)
    video_port_->buffer_num = num_buffers;

  // Create pool of buffer headers for the output port to consume

  checkError(mmal_pool_resize(pool_, video_port_->buffer_num, video_port_->buffer_size),
             "Failed to resize buffer header pool for camera video port");

  // set reference back to object

  video_port_->userdata =(struct MMAL_PORT_USERDATA_T *)this;

  // enable the camera video port and tell it its callback function

  checkError(mmal_port_enable(video_port_, RasPiCam::cameraBufferCallback),
             "Unable to setup camera setup");

  // send all the buffers to the camera video port

  int num = mmal_queue_length(pool_->queue);
  for(int q=0; q < num; q++)
    {
      MMAL_BUFFER_HEADER_T *buffer = mmal_queue_get(pool_->queue);

      if(!buffer)
        checkError(MMAL_EIO,
                   "Unable to get a required buffer from pool queue");
      
      checkError(mmal_port_send_buffer(video_port_, buffer),
                 "Unable to send a buffer to camera video port.");
    }
    
}

/* -------------------------------------------------------------------------- */

void RasPiCam::disableVideoPort()

{
  assert(video_port_ != NULL);

  if(video_port_->is_enabled)
  {
    checkError(mmal_port_disable(video_port_), "port disable");
    checkError(mmal_component_disable(camera_component_), "component disable");
  } 
}

/* -------------------------------------------------------------------------- */

void RasPiCam::setVideoFormat(int width, int height, int framerate)

{
  assert(video_port_ != NULL);

  // keep variables in class
  
  if((width <= 0 && max_width_ <= 0) || (height <= 0 && max_height_ <= 0))
    throw MmalError(MMAL_EINVAL, "Unable to set resolution, no default available.");

  width_     = width > 0 ? width : max_width_;
  height_    = height > 0 ? height : max_height_;
  stride_    = VCOS_ALIGN_UP(width, 32);
  framerate_ = framerate;

  // can't change video format with enabled port

  disableVideoPort();

  
  //  set up the camera configuration
  {
    MMAL_PARAMETER_CAMERA_CONFIG_T cam_config =
    {
      { MMAL_PARAMETER_CAMERA_CONFIG, sizeof(cam_config) },
      .max_stills_w = uint32_t(width_),
      .max_stills_h = uint32_t(height_),
      .stills_yuv422 = 0,
      .one_shot_stills = 0,
      .max_preview_video_w = uint32_t(width_),
      .max_preview_video_h = uint32_t(height_),
      .num_preview_video_frames = 3,
      .stills_capture_circular_buffer_height = 0,
      .fast_preview_resume = 0,
      .use_stc_timestamp = MMAL_PARAM_TIMESTAMP_MODE_RESET_STC
    };
    
    mmal_port_parameter_set(camera_component_->control, &cam_config.hdr);
  }  
  
  MMAL_ES_FORMAT_T 	*	format;

  // Set the encode format on the video  port

  format = video_port_->format;

  // set format of frames

  format->encoding = MMAL_ENCODING_I420;
  format->encoding_variant = MMAL_ENCODING_I420;

  format->es->video.width = stride_;
  format->es->video.height = VCOS_ALIGN_UP(height_, 16);
  format->es->video.crop.x = 0;
  format->es->video.crop.y = 0;
  format->es->video.crop.width = width_;
  format->es->video.crop.height = height_;
  format->es->video.frame_rate.num = framerate;
  format->es->video.frame_rate.den = 1;

  checkError(mmal_port_format_commit(video_port_),
             "Camera video format couldn't be set");

  enableVideoPort(num_buffers_);
}

/* -------------------------------------------------------------------------- */

void RasPiCam::shutdownCamera()

{
  // disable port

  disableVideoPort();

  video_port_ = nullptr;
  if(pool_ != nullptr)
    {
      mmal_pool_destroy(pool_);
      pool_ = nullptr;
    }

  // destroy camera

  if(camera_component_)
    {
      mmal_component_destroy(camera_component_);
      camera_component_ = NULL;
    }
}

/* -------------------------------------------------------------------------- */

void RasPiCam::startCapture()

{
  checkError(mmal_port_parameter_set_boolean(video_port_,
                                             MMAL_PARAMETER_CAPTURE, 1),
             "Unable to start capture on video port.");
}

/* -------------------------------------------------------------------------- */

void RasPiCam::stopCapture()

{
  checkError(mmal_port_parameter_set_boolean(video_port_,
                                             MMAL_PARAMETER_CAPTURE, 0),
             "Unable to stop capture on video port.");
}

/* -------------------------------------------------------------------------- */

void RasPiCam::setShutterSpeed(uint32_t speed)
{
  checkError(mmal_port_parameter_set_uint32(camera_component_->control, 
                                            MMAL_PARAMETER_SHUTTER_SPEED, speed),
             "Unable to set shutter speed");
}  

/* -------------------------------------------------------------------------- */

void RasPiCam::setISO(uint32_t iso)
{
  checkError(mmal_port_parameter_set_uint32(camera_component_->control, 
                                            MMAL_PARAMETER_ISO, iso),
             "Unable to set shutter speed");
}

/* -------------------------------------------------------------------------- */

void RasPiCam::setWB(bool autowb, unsigned int r, unsigned int b)

{
  if(autowb)
    {
      MMAL_PARAMETER_AWBMODE_T param = {{MMAL_PARAMETER_AWB_MODE,sizeof(param)}, 
                                          MMAL_PARAM_AWBMODE_AUTO};

      checkError(mmal_port_parameter_set(camera_component_->control, &param.hdr),
                "Unable to set auto WB");
    }
  else
    {
      MMAL_PARAMETER_AWBMODE_T param = {{MMAL_PARAMETER_AWB_MODE,sizeof(param)}, 
                                          MMAL_PARAM_AWBMODE_OFF};

      checkError(mmal_port_parameter_set(camera_component_->control, &param.hdr),
                "Unable to set WB");
      
      MMAL_PARAMETER_AWB_GAINS_T gains = {{MMAL_PARAMETER_CUSTOM_AWB_GAINS,sizeof(gains)}, {0,0}, {0,0}};

      gains.r_gain.num = r;
      gains.b_gain.num = b;
      gains.r_gain.den = gains.b_gain.den = 256; // 65536;
      checkError(mmal_port_parameter_set(camera_component_->control, &gains.hdr),
                 "Unable to set AWB gains");      
    }
}

/* -------------------------------------------------------------------------- */

void RasPiCam::setExposureCompensation(int ev)

{
  // exposure compensation
  
  checkError(mmal_port_parameter_set_int32(camera_component_->control, 
                                           MMAL_PARAMETER_EXPOSURE_COMP , ev),
             "Unable to fix exposure compensation");
}

/* -------------------------------------------------------------------------- */

void RasPiCam::setExposureMode ( RasPiCam::ExposureMode mode )

{
  static const MMAL_PARAM_EXPOSUREMODE_T exposureMapping[] = { 
                                          MMAL_PARAM_EXPOSUREMODE_OFF,
                                          MMAL_PARAM_EXPOSUREMODE_AUTO,
                                          MMAL_PARAM_EXPOSUREMODE_VERYLONG,
                                          MMAL_PARAM_EXPOSUREMODE_NIGHT,
                                          MMAL_PARAM_EXPOSUREMODE_FIXEDFPS,
                                          MMAL_PARAM_EXPOSUREMODE_SPORTS,
                                          MMAL_PARAM_EXPOSUREMODE_MAX};
                                          
    MMAL_PARAMETER_EXPOSUREMODE_T param = {{MMAL_PARAMETER_EXPOSURE_MODE, sizeof(param)}, 
                                            exposureMapping[(int) mode]};

    checkError(mmal_port_parameter_set(camera_component_->control, &param.hdr),
              "Unable to set exposure mode");
}

/* -------------------------------------------------------------------------- */

//	FIXME: maybe later

void RasPiCam::cameraControlCallback(MMAL_PORT_T *port,
                                     MMAL_BUFFER_HEADER_T *buffer)
{
  if (buffer->cmd == MMAL_EVENT_PARAMETER_CHANGED)
    {
      MMAL_EVENT_PARAMETER_CHANGED_T *param = 
                                (MMAL_EVENT_PARAMETER_CHANGED_T *) buffer->data;

      if (param->hdr.id == MMAL_PARAMETER_CAMERA_SETTINGS)
        {
          // We pass our object pointer via the userdata field.

          RasPiCam * cam = (RasPiCam *) port->userdata;

          // call callback to the object

          MMAL_PARAMETER_CAMERA_SETTINGS_T * settings = 
                    reinterpret_cast<MMAL_PARAMETER_CAMERA_SETTINGS_T *>(param);
          
          cam->controlFrameReceived(port, settings->exposure,
            (settings->analog_gain.num*1000)/settings->analog_gain.den,
            (settings->digital_gain.num*1000)/settings->digital_gain.den);
        }          
    }
  else if (buffer->cmd == MMAL_EVENT_ERROR)
    {
      LOG_WARN << "No data received from sensor. Check all connections, including the Sunny one on the camera board";
    }
  else
    {
      LOG_WARN << "Received unexpected camera control callback event.";
    }

  mmal_buffer_header_release(buffer);
}

/* -------------------------------------------------------------------------- */

void RasPiCam::returnBackBuffer(MMAL_PORT_T *port, MMAL_BUFFER_HEADER_T *buffer)

{
  // release buffer back to the pool
  
  mmal_buffer_header_release(buffer);

  // and send one back to the port (if still open)

  if(port->is_enabled)
    {
      MMAL_BUFFER_HEADER_T * new_buffer = mmal_queue_get(pool_->queue);

      if(!new_buffer)
        RasPiCam::checkError(MMAL_ENOMEM, "Unable to get buffer from queue");
      
      RasPiCam::checkError(mmal_port_send_buffer(port, new_buffer),
                 "Unable to return a buffer to the camera port.");
    }    
}

/* -------------------------------------------------------------------------- */

void RasPiCam::lockBuffer(MMAL_BUFFER_HEADER_T* buffer)

{
  mmal_buffer_header_mem_lock(buffer);
}

/* -------------------------------------------------------------------------- */

void RasPiCam::unlockBuffer(MMAL_BUFFER_HEADER_T* buffer)

{
  mmal_buffer_header_mem_unlock(buffer);
}

/* -------------------------------------------------------------------------- */

void RasPiCam::getInfo(MMAL_BUFFER_HEADER_T* buffer, void*& data, int& length)

{
  data = buffer->data;
  length = buffer->length;
}

/* -------------------------------------------------------------------------- */

void RasPiCam::cameraBufferCallback(MMAL_PORT_T *port,
                                      MMAL_BUFFER_HEADER_T *buffer)
{
  // We pass our object pointer via the userdata field.

  RasPiCam * cam = (RasPiCam *) port->userdata;

  // call callback to the object
  
  cam->videoFrameReceived(port, buffer);
}

/* -------------------------------------------------------------------------- */
