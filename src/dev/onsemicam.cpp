/*
 * OnSemiCam.cpp - wrapper for OnSemi Development board
 * 
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 * 
 * Based on DevWareX - ApBase COM
 */

/* -------------------------------------------------------------------------- */

#include "onsemicam.h"

#include <thread>
#include <cmath>
#include <cassert>
#include <exception>

#include <apbase.h>

/* -------------------------------------------------------------------------- */

using namespace mke::dev;

#define ONSEMICAM_TAG "OnSemiCam: "

/* -------------------------------------------------------------------------- */

// constructor with reason
OnSemiError::OnSemiError(const char* desc, int ret_status) : 
std::runtime_error(desc), ret_status_(ret_status)

{
  std::ostringstream oss;
  oss.str("");
  oss << "OnSemiError no.";
  oss << ret_status_ << ": " << runtime_error::what();
      
  desc_ = oss.str();  
};

/* -------------------------------------------------------------------------- */

const char * OnSemiError::what() const throw()

{
  return desc_.c_str();
}

/* -------------------------------------------------------------------------- */

OnSemiCam::OnSemiCam(const std::string & sensor_data_file, 
                     const std::string & presets_file,
                     const std::string & preset, int portid, int buffsize)
: sensor_data_file_(sensor_data_file), presets_file_(presets_file), preset_(preset),
  port_id_(portid), buffer_(buffsize), fll_(0)
    
{
    std::ostringstream oss;
    camera_name_ = "OnSemi Development Board";
    
    initialize();
}

/* -------------------------------------------------------------------------- */

OnSemiCam::~OnSemiCam()
{
    finalize();
}

/* -------------------------------------------------------------------------- */

void OnSemiCam::initialize()
{
    std::cerr << ONSEMICAM_TAG << "Probing for sensor defined in " << sensor_data_file_.c_str() << std::endl;
    ap_DeviceProbe(sensor_data_file_.c_str());

    ap_handle_ = ap_Create(port_id_);
    if (ap_handle_ == NULL)
        throw OnSemiError("Unable to connect to camera AP_HANDLE == NULL");

    std::cout << "SideEffect flags: " << ap_CheckSensorState(ap_handle_, 0) << std::endl;
    
    if(presets_file_.length() > 0)
    {
        std::cerr << ONSEMICAM_TAG << "Loading INI presets from " << presets_file_.c_str() << std::endl;
        ap_LoadIniPreset(ap_handle_, presets_file_.c_str(), preset_.length() ? preset_.c_str() : nullptr);

        if(preset_.length())
            applyProfile(preset_.c_str());
    }

    // stop streaming
 //   setRegister("RESET_REGISTER", 0x0018);
    
    camera_name_ = camera_name_ + ap_GetPartNumber(ap_handle_);
        
    buff_size_ = ap_GrabFrame(ap_handle_, NULL, 0);
    char type[64];
    ap_GetImageFormat(ap_handle_, &image_width_, &image_height_, type, sizeof(type));
    checkLastError("Unable to get image format");
    image_type_ = type;
        
    if(image_type_ != "BAYER-10" && image_type_ != "BAYER-8")
        throw OnSemiError("Currently only BAYER-8 or BAYER-10 image type supported");
    
    // initialize buffers
    
    for(std::vector<Buffer>::iterator b = buffer_.begin(); b != buffer_.end(); ++b)
      {
        b->resize(buff_size_);
        available_.push(b->data());
      }

    // start streaming
    
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    setRegister("RESET_REGISTER", 0x001C);
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    
    unsigned char * buff = new unsigned char[buff_size_];
    ap_s32 err;
    for (int j = 0; j < 5; j++) 
    {
        size_t len = ap_GrabFrame(ap_handle_, buff, buff_size_);
        err = ap_GetLastError();
        
        if(err == MI_CAMERA_SUCCESS)
            break;
        
        std::cerr << "#" << std::flush;
    }

    std::cout << "Reset: " << getRegister("RESET_REGISTER") << std::endl;
    // stop streaming
//    setRegister("RESET_REGISTER", 0x0018);
    
    if (err == MI_CAMERA_SUCCESS)
        std::cout << " done" << std::endl;
    else
        std::cout << " ERR" << std::endl;

    delete[] buff;
//    assert(running_ == false);
//    thread_ = std::thread(&OnSemiCam::threadProc, this);
}

/* -------------------------------------------------------------------------- */

void OnSemiCam::finalize()
{
    if (running_)
        stopCapture();
    
    std::cerr << ONSEMICAM_TAG << "Destroying camera" << std::endl;
    
    ap_Destroy(ap_handle_);
    ap_Finalize();
    
    ap_handle_ = nullptr;
}

/* -------------------------------------------------------------------------- */

void OnSemiCam::checkStatus(int ret_code, const char * err_desc, int success_code) const
{
    if(ret_code != success_code)
        throw OnSemiError(err_desc, ret_code);
}

/* -------------------------------------------------------------------------- */

void OnSemiCam::checkLastError(const char * err_desc) const
{
    ap_s32 err = ap_GetLastError();
    if(err != AP_CAMERA_SUCCESS)
        throw OnSemiError(err_desc, err);
}

/* -------------------------------------------------------------------------- */

uint32_t OnSemiCam::getRegister(const char * regname, const char * bitname) const
{
    ap_u32 val;
    checkStatus(ap_GetSensorRegister(ap_handle_, regname, bitname, &val, 0),
        "Unable to get register");
    
    return val;
}

/* -------------------------------------------------------------------------- */

void OnSemiCam::setRegister(const char * regname, uint32_t value, const char * bitname)
{
    checkStatus(ap_SetSensorRegister(ap_handle_, regname, bitname, value, 0),
        "Unable to set register");
}

/* -------------------------------------------------------------------------- */

void OnSemiCam::applyProfile(const char * profile_name)
{
    std::cout << "Applying preset " << profile_name << std::endl;
    ap_LoadIniPreset(ap_handle_, presets_file_.c_str(), preset_.c_str());
}

/* -------------------------------------------------------------------------- */

void OnSemiCam::startCapture()
{
    std::cerr << "START" << std::endl;

    assert(running_ == false);
    thread_ = std::thread(&OnSemiCam::threadProc, this);
    
//    setRegister("RESET_REGISTER", 0x001C);
    
}

/* -------------------------------------------------------------------------- */

void OnSemiCam::stopCapture()
{
    if(!running_)
        return;
    std::cerr << "STOP" << std::endl;
//    setRegister("RESET_REGISTER", 0x0018);

    running_ = false;
    thread_.join();
}

/* -------------------------------------------------------------------------- */

void OnSemiCam::setShutterSpeed(uint32_t shutter)
{
    std::lock_guard<std::mutex> lock(shared_);

    setRegister("COARSE_INTEGRATION_TIME", shutter);   // CIT < FLL
    if(fll_ <= 0)
        setRegister("FRAME_LENGTH_LINES", shutter + 0x0004); 
    else 
        setRegister("FRAME_LENGTH_LINES", std::max(shutter + 0x0004, fll_)); 
    setRegister("LINE_LENGTH_PCK", 0x02F8);  // by default
    setRegister("RESET_REGISTER", 0x4014);    
}

/* -------------------------------------------------------------------------- */

void OnSemiCam::setFps(int fps)
{
    if (fps <= 0)
        fll_ = 0;
    else
        fll_ = std::ceil(1000000.0/fps/ONSEMICAM_ROWTIME);
}

/* -------------------------------------------------------------------------- */

void OnSemiCam::setGain(double gain)
{
    std::lock_guard<std::mutex> lock(shared_);
    // 
    
    ap_u32 v = round(std::log2(gain)*16);
    v &= 0x007F;    // mask only analog part of gain
    v |= 0x2000;    // set digital gain to 1x ~ 64
    
    setRegister("GLOBAL_GAIN", v);
    setRegister("RESET_REGISTER", 0x4014);
}

/* -------------------------------------------------------------------------- */

uint32_t OnSemiCam::getShutterSpeed()
{
    std::lock_guard<std::mutex> lock(shared_);
        
    return getRegister("FRAME_LENGTH_LINES");
}

/* -------------------------------------------------------------------------- */

double OnSemiCam::getAnalogGain()
{
    std::lock_guard<std::mutex> lock(shared_);

    ap_u32 v = getRegister("GLOBAL_GAIN", "GLOBAL_COLUMN_AMP_GAIN") 
                           | (getRegister("GLOBAL_GAIN","GLOBAL_ADC_GAIN") << 4);
    return exp2(v/16.0);
}

/* -------------------------------------------------------------------------- */

double OnSemiCam::getDigitalGain()
{
    std::lock_guard<std::mutex> lock(shared_);

    return getRegister("GLOBAL_GAIN", "DIGITAL_GAIN_FOR_GLOBAL")/64.0;
}

/* -------------------------------------------------------------------------- */

uint16_t OnSemiCam::getLaserSequence()
{
     return laser_pattern_;
}

/* -------------------------------------------------------------------------- */

void OnSemiCam::threadProc()
{
    running_ = true;

    ap_u32 len;
    ap_s32 err;

    bool do_conv = image_type_ == "BAYER-10";
    try
    {
        while(running_)
        {
            // capture              
            
            Pix8 * buff = nullptr;
            {
                std::lock_guard<std::mutex> lock(shared_);
                if (available_.size() > 0)
                {
                    buff = available_.front();
                    available_.pop();                    
                }  
            }
            
            if(buff)
            {
                for(int i = 0; i < 2; ++i)
                {
                    len = ap_GrabFrame(ap_handle_, buff, buff_size_);
                    err = ap_GetLastError();
                    
                    if (err == MI_CAMERA_SUCCESS)
                        break;
                    else
                        std::cerr << "GrabFrame: An error occured no. " << err << std::endl;
                }
                if (err != MI_CAMERA_SUCCESS)
                {
                    returnBackBuffer(buff);
                    std::this_thread::sleep_for(std::chrono::milliseconds(10));
                }
                else
                {
                    
                    if(do_conv)
                        conv10b8(buff, image_width_*image_height_);
                    videoFrameReceived(buff, image_width_, image_height_, image_width_, 0);
                }
            }
            else
            {
                std::cerr << "NO BUFFER" << std::endl;
            }            
        } 
    }
    catch (std::exception & ex) 
    {
        std::cerr << ex.what() << std::endl;
        err_ = std::current_exception();
    }
    catch (...)
    {
        std::cerr << "unknown exception" << std::endl;
        err_ = std::current_exception();
    }
        
    running_ = false;
}

/* -------------------------------------------------------------------------- */

void OnSemiCam::returnBackBuffer(const Pix8 * buff)
{
    std::lock_guard<std::mutex> lock(shared_);
    
    available_.push(const_cast<Pix8 *>(buff));
}

/* -------------------------------------------------------------------------- */

void OnSemiCam::conv10b8(Pix8 * im, size_t num_pixels)
{
    uint16_t * pix10 = reinterpret_cast<uint16_t *>(im);
    Pix8 * pix8 = im;
    uint16_t * pix10E = pix10 + num_pixels;
    
    for(; pix10 < pix10E; ++pix8,++pix10)
        *pix8 = *pix10>>2;
}

/* -------------------------------------------------------------------------- */
