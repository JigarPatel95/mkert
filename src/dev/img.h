/*
 * img.h - really simple interface for cooperating with images
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef _IMG_H_
#define _IMG_H_

/* -------------------------------------------------------------------------- */

// global includes

#include <stdint.h>
#include <cassert>
#include <vector>
#include <time.h>

/* -------------------------------------------------------------------------- */

namespace mke {
namespace dev {

// define pixel type

typedef uint8_t Pix8;

/* -------------------------------------------------------------------------- */

// Image - basic structure

template<typename PixType>
struct Image
{
public:
  PixType * mem;

  int       w;
  int       h;
  int       stride;
  
  // contruct image above memmory
  Image(PixType * mem, int w, int h, int stride)
  : mem(mem), w(w), h(h), stride(stride)
  {
    assert(stride >= w);
  }
  
  PixType * row(int r)
  {
    return mem + r*stride;
  }
  
  const PixType * row(int r) const
  {
    return mem + r*stride;
  }
  
  // destruction of image
  virtual ~Image() {}
};
  
/* -------------------------------------------------------------------------- */

// MemImage - memmory saved image

template<typename PixType>
struct MemImage : public Image<PixType>

{
protected:
  std::vector<PixType>  data_holder_;
  
public:
  // empty constructor
  MemImage():
    Image<PixType>(nullptr, 0, 0, 0)
  {};
  
  // specific constructor
  MemImage(int w, int h, int stride = -1):
    Image<PixType>(nullptr, 0, 0, 0)
  {
    assert(stride < 0 || stride >= w);
    
    if(stride < w)
      stride = w;
    
    reallocate(w, h, stride);
  }
  
  // reallocate space
  void reallocate(int w, int h, int stride)
  {
    assert(stride >= w);
    
    data_holder_.resize(h*stride);
    
    this->mem = data_holder_.data();
    this->w = w;
    this->h = h;
    this->stride = stride;
  }
};

/* -------------------------------------------------------------------------- */

template<typename PixType>
struct CamFrame : public Image<PixType>
{
public:
  uint64_t    timer;
  uint32_t    frame_id;
  uint16_t    phase;
  
  // full constructor
  CamFrame(PixType* mem, int w, int h, int stride, uint64_t timer, uint32_t frame_id, uint16_t phase)
  : Image<PixType>(mem, w, h, stride), timer(timer), frame_id(frame_id), phase(phase)
  {};
  
  // constructor by Image
  CamFrame(Image<PixType> & im, uint64_t timer, uint32_t frame_id, uint16_t phase)
  : Image<PixType>(im), timer(timer), frame_id(frame_id), phase(phase)
  {};
  
  // default constructor
  CamFrame()
  : Image<PixType>(nullptr, 0,0,0), timer(0), frame_id(0), phase(0)
  {};

  // get current timer for this moment - in milliseconds
  inline static void setTimerToNow(uint64_t & timer)
  {
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    timer = static_cast<uint64_t>(ts.tv_sec) * 1000ULL 
                            + static_cast<uint64_t>(ts.tv_nsec) / 1000000ULL;
  }  
};

/* -------------------------------------------------------------------------- */

} // end of mke::dev namespace
} // end of mke namespace

/* -------------------------------------------------------------------------- */

#endif // _IMG_H_

/* -------------------------------------------------------------------------- */
