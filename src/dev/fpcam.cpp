/*
 * FpCam.cpp - wrapper for Okada FPGA camera - FpCam
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 * 
 * Based on FTDI D2XX protocol 
 */

/* -------------------------------------------------------------------------- */

#include "fpcam.h"

#include <thread>
#include <cmath>

#include <exception>

#ifdef USE_FTD2XX
#include <ftd2xx.h>
#elif defined USE_FTDI
#include "ftd2xx_compat.h"
#endif

/* -------------------------------------------------------------------------- */

using namespace mke::dev;

/* -------------------------------------------------------------------------- */

const int IMAGE_BUFFER_SIZE = 2048 * 1024;
const int API_CHECKS_NUM_ATTEMPTS = 2;

#define V2_COMMAND_CAPTURE        0x90 // 0x91-0x9f
#define V2_COMMAND_CAPTURE_COMPRESSED 0xc0 // 0xc1-0xcf
#define V2_COMMAND_CAPTURE_MASK   0xf0

#define V2_COMMAND_GET_IMAGE      0xa0 // 0xa1-0xbf
#define V2_COMMAND_GET_IMAGE_MASK 0xe0
#define V2_COMMAND_GET_IMAGE_SEGMENT_MASK 0x1f

#define V2_COMMAND_SEND_STATUS    0xf0
#define V2_COMMAND_LASER_CTL      0xf1
#define V2_COMMAND_EXP_GAIN_CTL   0xf2
#define V2_COMMAND_STOP_CAPTURE   0xf8
#define V2_COMMAND_RESUME_CAPTURE 0xf9
#define V2_IMAGE_SEGMENT_SIZE     65536


#define V1_COMMAND_SEND_STATUS    0x80
#define V1_COMMAND_CAPTURE        0x90 // 0x91-0x9f
#define V1_COMMAND_CAPTURE_MASK   0xf0
#define V1_COMMAND_GET_IMAGE      0xa0 // 0xa1-0xa5
#define V1_COMMAND_GET_IMAGE_MASK 0xf0
#define V1_COMMAND_GET_IMAGE_SEGMENT_MASK 0x0f
#define V1_COMMAND_LASER_CTL      0xb0
#define V1_COMMAND_EXP_GAIN_CTL   0xb1
#define V1_IMAGE_SEGMENT_SIZE     752*80

/* -------------------------------------------------------------------------- */

int FpCam::api_v1_[__API_NUM] = { 
    V1_COMMAND_CAPTURE,                 // API_CAPTURE
    -1,                                 // API_CAPTURE_COMPRESSED,
    V1_COMMAND_CAPTURE_MASK,            // API_CAPTURE_MASK,
    V1_COMMAND_GET_IMAGE,               // API_GET_IMAGE,
    V1_COMMAND_GET_IMAGE_MASK,          // API_GET_IMAGE_MASK,
    V1_COMMAND_GET_IMAGE_SEGMENT_MASK,  // API_GET_IMAGE_SEGMENT_MASK,
    V1_COMMAND_SEND_STATUS,             // API_SEND_STATUS,
    V1_COMMAND_LASER_CTL,               // API_LASER_CTL,
    V1_COMMAND_EXP_GAIN_CTL,            // API_EXP_GAIN_CTL,
    -1,                                 // API_STOP_CAPTURE,
    -1,                                 // API_RESUME_CAPTURE,
    V1_IMAGE_SEGMENT_SIZE               // API_SEGMENT_SIZE,
};

int FpCam::api_v2_[__API_NUM] = { 
    V2_COMMAND_CAPTURE,                 // API_CAPTURE
    V2_COMMAND_CAPTURE_COMPRESSED,      // API_CAPTURE_COMPRESSED,
    V2_COMMAND_CAPTURE_MASK,            // API_CAPTURE_MASK,
    V2_COMMAND_GET_IMAGE,               // API_GET_IMAGE,
    V2_COMMAND_GET_IMAGE_MASK,          // API_GET_IMAGE_MASK,
    V2_COMMAND_GET_IMAGE_SEGMENT_MASK,  // API_GET_IMAGE_SEGMENT_MASK,
    V2_COMMAND_SEND_STATUS,             // API_SEND_STATUS,
    V2_COMMAND_LASER_CTL,               // API_LASER_CTL,
    V2_COMMAND_EXP_GAIN_CTL,            // API_EXP_GAIN_CTL,
    V2_COMMAND_STOP_CAPTURE,            // API_STOP_CAPTURE,
    V2_COMMAND_RESUME_CAPTURE,          // API_RESUME_CAPTURE,
    V2_IMAGE_SEGMENT_SIZE               // API_SEGMENT_SIZE,
};



/* -------------------------------------------------------------------------- */

void checkFtStatus(FT_STATUS status, const char * msg)
{
    if(status != FT_OK)
      {
        std::cerr << msg;
        throw std::runtime_error(msg);
      }
}

/* -------------------------------------------------------------------------- */

void writeUSB(FT_HANDLE ftHandle, uint8_t byte)
{
    uint8_t buffer[1];
    buffer[0] = byte;
    DWORD written;
    checkFtStatus(FT_Write(ftHandle, buffer, 1, &written),
                  "FT_Write failed");
}

/* -------------------------------------------------------------------------- */

FpCam::FpCam(int portid, int buffsize, bool compressed)
: port_id_(portid), buffer_(buffsize), compressed_(compressed), alternate_(false),
  lastMagic_(STATUS_MAGIC_NOCAM), api_version_(-1), api_dict_(nullptr)
    
{
    std::ostringstream oss;
    camera_name_ = oss.str();
    
    setLaserSequence(0x0000,0);
}

/* -------------------------------------------------------------------------- */

FpCam::~FpCam()
{
    finalize();
}

/* -------------------------------------------------------------------------- */

void FpCam::initialize()
{
    checkFtStatus(FT_Open(port_id_, &ft_handle_), 
                  "Cannot open FPCAM device");
    checkFtStatus(FT_SetBitMode(ft_handle_, 0xff, FT_BITMODE_RESET),
                  "FT_SetBitmode (RESET) failed");

    std::this_thread::sleep_for(std::chrono::milliseconds(10));

    checkFtStatus(FT_SetBitMode(ft_handle_, 0xff, FT_BITMODE_SYNC_FIFO),
                  "FT_SetBitmode (SYNC_FIFO) failed");

    FT_SetLatencyTimer(ft_handle_, 2);
    FT_SetUSBParameters(ft_handle_, 0x10000, 0x10000);
    FT_SetFlowControl(ft_handle_, FT_FLOW_RTS_CTS, 0x0, 0x0);
    FT_Purge(ft_handle_, FT_PURGE_RX);

    // initialize buffers
    
    for(std::vector<Buffer>::iterator b = buffer_.begin(); b != buffer_.end(); ++b)
      {
        b->resize(IMAGE_BUFFER_SIZE);
        available_.push(b->data());
      }
    
    assert(running_ == false);
    thread_ = std::thread(&FpCam::threadProc, this);
    
    if(compressed_ && buffer_.size() < 3)
        throw std::runtime_error("Too less number of buffers for compressed stream");
}

/* -------------------------------------------------------------------------- */

void FpCam::finalize()
{
    if (running_)
        stopCapture();
    
    running_ = false;
    thread_.join();
}

/* -------------------------------------------------------------------------- */

void FpCam::checkCameraType()
{
    if (curr_status_.magic == lastMagic_) {
        return;
    }
    std::lock_guard<std::mutex> lock(shared_);


    if (curr_status_.magic == STATUS_MAGIC_MT9V034) {
        image_width_ = 752;
        image_height_ = 480;
        image_pitch_ = image_width_;
     } else if (curr_status_.magic == STATUS_MAGIC_AR0134) {
        image_width_ = 1280;
        image_height_ = 960;
        image_pitch_ = image_width_;
    } else {
        return;
    }

    imageSegmentCount = int(ceil(double(image_width_ * image_height_) /
                                 double(apiCmd<int>(API_SEGMENT_SIZE))));

    std::cout << curr_status_.magic << " " << image_width_ << " " << image_height_ << " " << image_pitch_ << std::endl;
    lastMagic_ = curr_status_.magic;
}

/* -------------------------------------------------------------------------- */

void FpCam::startCapture()
{
    flags_ |= COMM_CAPTURE;
    std::cerr << "START" << std::endl;
}

/* -------------------------------------------------------------------------- */

void FpCam::stopCapture()
{
    flags_ &= ~((uint32_t) COMM_CAPTURE);
    std::cerr << "STOP" << std::endl;
}

/* -------------------------------------------------------------------------- */

void FpCam::setShutterSpeed(double shutter, bool auto_shut)
{
    std::lock_guard<std::mutex> lock(shared_);
    
    exp_params_.auto_exposure = auto_shut;
//    exp_params_.exposure_value = std::min(std::max<int>(round(shutter*480),1),32765);
    exp_params_.exposure_value = std::min(std::max<int>(round(shutter*28),1),32765);
    std::cerr << shutter << "~" << exp_params_.exposure_value << std::endl;
    flags_ |= COMM_WRITE_EXPOSURE;
}

/* -------------------------------------------------------------------------- */

void FpCam::setGain(double gain, bool auto_gain)
{
    std::lock_guard<std::mutex> lock(shared_);

    exp_params_.auto_gain = auto_gain;
    exp_params_.gain_value = std::min(std::max<int>(round(gain*16), 16), 64);
    flags_ |= COMM_WRITE_EXPOSURE;
}

/* -------------------------------------------------------------------------- */

double FpCam::getShutterSpeed()
{
    std::lock_guard<std::mutex> lock(shared_);
    return double(curr_status_.exposure_time) / 24e3;
}

/* -------------------------------------------------------------------------- */

double FpCam::getGain()
{
    std::lock_guard<std::mutex> lock(shared_);
    return exp_params_.auto_gain ? -1 : (exp_params_.gain_value/16.0);
}

/* -------------------------------------------------------------------------- */

void FpCam::setLaserSequence(uint16_t pattern, int num)
{
    std::lock_guard<std::mutex> lock(shared_);

    laser_pattern_ = pattern;
    laser_pattern_len_ = num;
    uint16_t cap_mask = 0;
    alternate_ = false;
    uint16_t pat = pattern;
    for(int i = 0; i < num; ++i) {
        cap_mask = (cap_mask << 1) | 0x01;
        
        if((pat & 0x0f) != (pattern & 0x0f))
            alternate_ = true;
        pat <<= 4;
    }
    capture_mask_ = cap_mask;
    flags_ |= COMM_WRITE_LASERS;
    
    std::cerr << "CAPTURE MASK: " << int(capture_mask_) << std::endl;
}

/* -------------------------------------------------------------------------- */

uint16_t FpCam::getLaserSequence()
{
    return laser_pattern_;
}

/* -------------------------------------------------------------------------- */

int FpCam::getLaserSequenceLength()
{
    uint16_t cap_mask = capture_mask_;
    for(int i = 0; i < 4; ++i)
        if ((cap_mask & 0x0001) == 0)
            return i;
        cap_mask <<= 1;
    return 4;
}

/* -------------------------------------------------------------------------- */

bool FpCam::checkApiVersion()
{
    // set timeouts
    
    checkFtStatus(FT_SetTimeouts(ft_handle_, 100, 100), "Unable to set Timeouts for FT driver");
    
    // try V2

    DWORD bytesReceived;
    writeUSB(ft_handle_, V2_COMMAND_SEND_STATUS);
    checkFtStatus(FT_Read(ft_handle_, &curr_status_, sizeof(curr_status_), &bytesReceived),
                "FT_Read failed");    

    // default
    api_version_ = -1;
    
    if(bytesReceived == sizeof(curr_status_))
    {
        api_version_ = 2;
        api_dict_ = FpCam::api_v2_;
        
        checkFtStatus(FT_SetTimeouts(ft_handle_, 0, 0), "Unable to UNSET Timeouts for FT driver");
        return true;
    }
    
    // try V1

    writeUSB(ft_handle_, V1_COMMAND_SEND_STATUS);
    checkFtStatus(FT_Read(ft_handle_, &curr_status_, sizeof(curr_status_), &bytesReceived),
                "FT_Read failed");    

    if(bytesReceived == sizeof(curr_status_))
    {
        api_version_ = 1;
        api_dict_ = FpCam::api_v1_;
        
        checkFtStatus(FT_SetTimeouts(ft_handle_, 0, 0), "Unable to UNSET Timeouts for FT driver");
        return true;
    }
    
    return false;
}

/* -------------------------------------------------------------------------- */

void FpCam::threadProc()
{
    running_ = true;
    std::this_thread::sleep_for(std::chrono::milliseconds(200));


    DWORD rxBytes, txBytes, event, bytesReceived;
    auto last_status = std::chrono::high_resolution_clock::now();
    auto last_frame = std::chrono::high_resolution_clock::now() - std::chrono::milliseconds(frame_delay_);
    
    try
    {
        for(int i = 0; i < API_CHECKS_NUM_ATTEMPTS; i++)
            if(checkApiVersion())
                break;
            else
                std::cerr << "Attempt no. " << (i+1) << "  to detect API failed." << std::endl;
        
        if(api_version_ > 0)
            std::cerr << "Detected FPGA API v" << api_version_ << std::endl; 
        else
            throw std::runtime_error("Unable to distinguish FPGA API version");

        checkFtStatus(FT_SetTimeouts(ft_handle_, 0, 0), "Unable to SET Timeouts for FT driver");

        int image_segment_bytes_ = apiCmd<int>(API_SEGMENT_SIZE);
        uint8_t cap_seq = alternate_ ? laser_pattern_len_ : 0;
        uint8_t capture_mask;
        
        while(running_)
        {
            auto diff = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - last_status).count();
            if (diff >= 100)
            {
                // get status from camera

                writeUSB(ft_handle_, apiCmd<uint8_t>(API_SEND_STATUS));

                SystemStatus read_status;
                checkFtStatus(FT_Read(ft_handle_, &read_status, sizeof(read_status), &bytesReceived),
                            "FT_Read failed");
                    
                if(bytesReceived == sizeof(read_status))
                {
                    {
                        std::lock_guard<std::mutex> lock(shared_);
                        curr_status_ = read_status;
                        last_status = std::chrono::high_resolution_clock::now();
                    }

                    checkCameraType();  
                }
                
            }   
            
            // set exposure if changed

            if (flags_ & COMM_WRITE_EXPOSURE) {

                std::lock_guard<std::mutex> lock(shared_);
                
                int expvalue = 0xffff;
                int gainvalue = 0xffff;
                if (!exp_params_.auto_exposure)  {
                    expvalue = exp_params_.exposure_value;
                }
                if (!exp_params_.auto_gain) {
                    gainvalue = exp_params_.gain_value;
                }
                writeUSB(ft_handle_, apiCmd<uint8_t>(API_EXP_GAIN_CTL));
                writeUSB(ft_handle_, expvalue);
                writeUSB(ft_handle_, expvalue >> 8);
                writeUSB(ft_handle_, gainvalue);
                writeUSB(ft_handle_, gainvalue >> 8);
                std::cout << "[" << expvalue << "," << gainvalue << "]" << std::endl;
                
                flags_ &= ~((uint32_t) COMM_WRITE_EXPOSURE);
            }
            
            // set lasers
            
            if (flags_ & COMM_WRITE_LASERS)
            {
                std::lock_guard<std::mutex> lock(shared_);
                writeUSB(ft_handle_, apiCmd<uint8_t>(API_LASER_CTL));
                writeUSB(ft_handle_, laser_pattern_);
                writeUSB(ft_handle_, laser_pattern_ >> 8);            

                flags_ &= ~((uint32_t) COMM_WRITE_EXPOSURE);
            }
            
            
            if(flags_ & COMM_CAPTURE)
            {
                auto diff = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - last_frame).count();
                if(diff < frame_delay_)
                    continue;
                
                last_frame = std::chrono::high_resolution_clock::now();
                // capture              
                
                Pix8 * buff = nullptr;
                Pix8 * compressed_buff = nullptr;
                {
                    std::lock_guard<std::mutex> lock(shared_);
                    if (available_.size() > 0)
                    {
                        buff = available_.front();
                        available_.pop();                    
                    }  
                    if (compressed_ && available_.size() > 0)
                    {
                        compressed_buff = available_.front();
                        available_.pop();                    
                    }  
                }
                
                if(buff && (!compressed_ || compressed_buff))
                    {
                        if (alternate_) {
                            cap_seq++;
                            if(cap_seq >= laser_pattern_len_)
                                cap_seq = 0;
                            
                            capture_mask = 0x01 << cap_seq;
                        } else if (capture_mask_ == 0 || capture_mask_ > 0xf) {
                            capture_mask = 0xf;
                        } else {
                            capture_mask = capture_mask_;
                        }
                        
                        for (int seg = 0; seg < imageSegmentCount; seg++) 
                        {
                            if (seg == 0) {
                                if(compressed_)
                                    writeUSB(ft_handle_, apiCmd<uint8_t>(API_CAPTURE_COMPRESSED) | capture_mask);
                                else
                                    writeUSB(ft_handle_, apiCmd<uint8_t>(API_CAPTURE) | capture_mask);
                            } else {
                                writeUSB(ft_handle_, apiCmd<uint8_t>(API_GET_IMAGE) + uint8_t(seg));
                            }

                            uint32_t p = 0;
                            const int max_rep = 20;
                            int rep = 0;
                            int ii = 0;
#ifdef USE_FTDI
                            // There is no GetStatus function in libftdi. So set remaining bytes to rxBytes.
                            rxBytes = image_segment_bytes_;
#endif
                            while (p < image_segment_bytes_ && rep < max_rep) {
                                
                                if(!running_)
                                    return;
                                
                                FT_GetStatus(ft_handle_, &rxBytes, &txBytes, &event);
                                if (rxBytes > 0) {
                                    std::cerr << "<< " << rxBytes << std::endl;
                                    checkFtStatus(FT_Read(ft_handle_,
                                                    buff + seg * image_segment_bytes_ + p,
                                                    rxBytes, &bytesReceived),
                                                "FT_Read failed");
                                    std::cerr << "::" << std::endl;

#ifdef USE_FTDI
                                    // When reading image segment is timeout, bytesReceived is set to zero.
                                    // In that case, wait 1ms then retry.
                                    if (bytesReceived == 0) {
                                        std::this_thread::sleep_for(std::chrono::microseconds(1000));
                                        rep++;
                                        continue;
                                    }
#endif
                                    p += bytesReceived;
                                    rxBytes -= bytesReceived;
                                    rep = 0;
                                    ii = 0;
                                } else {
                                    std::this_thread::sleep_for(std::chrono::microseconds(25000));
                                    std::cerr << "<>" << std::endl;
                                    if(p > 0)
                                        rep++;
                                    else
                                    {
                                        ii++;
                                        if ((ii % 100) == 0)
                                            std::cerr << "WW ii" << std::endl;
                                    }
                                }
                            }
                            
                            if(rep >= max_rep)
                            {
                                returnBackBuffer(buff);
                                if(compressed_buff)
                                    returnBackBuffer(compressed_buff);

                                std::cerr << "FPCAM skipped because REP" << std::endl;
                                continue;
                            }
                            
                            if(compressed_buff)
                            {
                                uint32_t complen = *reinterpret_cast<uint32_t*>(buff);
                                if (complen <= uint32_t((seg+1)*image_segment_bytes_)) {
                                    // got all data
                                    if(!decompress(buff, complen, compressed_buff, IMAGE_BUFFER_SIZE))
                                    {
                                        returnBackBuffer(buff);
                                        returnBackBuffer(compressed_buff);
                                        buff = compressed_buff = nullptr;
                                    }
                                    break;
                                }                            
                            }
                        }
                        
                        if(compressed_buff)
                        {
                            returnBackBuffer(buff);
                            
                            buff = nullptr;
                            videoFrameReceived(compressed_buff, image_width_, image_height_, image_pitch_, cap_seq);
                        }
                        else if(buff)
                            videoFrameReceived(buff, image_width_, image_height_, image_pitch_, cap_seq);
                    }
                else
                    {
                        std::cerr << "NO BUFFER" << std::endl;
                    }
            }
            
        } 
    }
    catch (std::exception & ex) 
    {
        std::cerr << ex.what() << std::endl;
        err_ = std::current_exception();
    }
    catch (...)
    {
        std::cerr << "unknown exception" << std::endl;
        err_ = std::current_exception();
    }
    
    std::cerr << "END OF FPCAM LOOP" << std::endl;
    
    running_ = false;
}

/* -------------------------------------------------------------------------- */

void FpCam::returnBackBuffer(const Pix8 * buff)
{
    std::lock_guard<std::mutex> lock(shared_);
    
    available_.push(const_cast<Pix8 *>(buff));
}

/* -------------------------------------------------------------------------- */

const uint8_t LONGBIT = 0x08;

bool FpCam::decompress(Pix8 * src, uint32_t len, Pix8 * dst, uint32_t buff_size)
{
    uint8_t* codep = src + 4;
    
    int dstSize = IMAGE_BUFFER_SIZE;
    
    int cnt = 0;
    for (uint32_t i = 0; i < len; i++) {
        uint8_t code = *codep++;
        int run;
        if ((code & LONGBIT) != 0) {
            uint8_t code2 = *codep++;
            i++;
            run = (int(code & 7) << 8) + code2;
        } else {
            run = code & 7;
        }
        uint8_t pix = code & 0xf0;
        for (int j = 0; j <= run; j++) {
            if (++cnt > dstSize) {
                std::cerr << "Decoder Overflow!" << std::endl;
                return false;
            }
            *dst++ = pix;
        }
    }
    return true;
}

/* -------------------------------------------------------------------------- */

#ifdef __ANDROID__

void FpCam::registerUsbFd(int vid, int pid, int fd)
{
    _registerUsbFd(vid, pid, fd);
}

#endif

/* -------------------------------------------------------------------------- */
