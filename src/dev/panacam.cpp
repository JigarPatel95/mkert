/*
 * PanaCam - wrapper for Panasonic TOF Camera
 * 
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * Copyright (c) 2019, Magik-Eye ltd.
 * author: Ondra Fisar, Jyunji Kondo
 * 
 */

/* -------------------------------------------------------------------------- */

#include "panacam.h"

#include <thread>
#include <cmath>
#include <cassert>
#include <exception>
#include <cstring>

#include <PanaLib.h>
#include <InterPanaLib.h>

/* -------------------------------------------------------------------------- */

using namespace mke::dev;

#define PANACAM_TAG "PanaCam: "

#define EMISSION_DISABLE    1

#define EXP_MIN 4

/* -------------------------------------------------------------------------- */

// constructor with reason
PanaError::PanaError(const char* desc, int ret_status) : 
std::runtime_error(desc), ret_status_(ret_status)

{
  std::ostringstream oss;
  oss.str("");
  oss << "PanaError no.";
  oss << ret_status_ << ": " << runtime_error::what();
      
  desc_ = oss.str();  
};

/* -------------------------------------------------------------------------- */

const char * PanaError::what() const throw()

{
  return desc_.c_str();
}

/* -------------------------------------------------------------------------- */

class PanaCam::Impl {
private:
    std::mutex              shared_;
    bool                    use_raw_;
    bool                    use_int_;
    IplImage                *ir_image_; // for depth/ir
    std::vector<IplImage *> bg_images_;
    IplImage                *a0_image_; // for raw
    IplImage                *a1_image_; // for raw
    IplImage                *a2_image_; // for raw
    std::queue<IplImage *>  available_;

public:
    Impl(bool use_raw, bool use_int, int buff_size)
    : use_raw_(use_raw), use_int_(use_int), bg_images_(buff_size)
    {

    }

    ~Impl()
    {

    }

    void initialize(unsigned short image_width, unsigned short image_height, unsigned short bpp)
    {
        // initialize buffers

        if (use_raw_) {
            for(std::vector<IplImage *>::iterator b = bg_images_.begin(); b != bg_images_.end(); ++b) {
                *b = cvCreateImage(cvSize(image_width, image_height * 2), bpp, 1);
                available_.push(*b);
            }
            a0_image_ = cvCreateImage(cvSize(image_width, image_height), bpp, 1);
            a1_image_ = cvCreateImage(cvSize(image_width, image_height), bpp, 1);
            a2_image_ = cvCreateImage(cvSize(image_width, image_height), bpp, 1);
        } else {
            for(std::vector<IplImage *>::iterator b = bg_images_.begin(); b != bg_images_.end(); ++b) {
                *b = cvCreateImage(cvSize(image_width, image_height), bpp, 1);
                available_.push(*b);
            }
            ir_image_ = cvCreateImage(cvSize(image_width, image_height), bpp, 1);
        }
    }

    void finalize()
    {
        if (use_raw_) {
            for(std::vector<IplImage *>::iterator b = bg_images_.begin(); b != bg_images_.end(); ++b) {
                IplImage *p = *b;
                cvReleaseImage(&p);
            }
            cvReleaseImage(&a0_image_);
            cvReleaseImage(&a1_image_);
            cvReleaseImage(&a2_image_);
        } else {
            for(std::vector<IplImage *>::iterator b = bg_images_.begin(); b != bg_images_.end(); ++b) {
                IplImage *p = *b;
                cvReleaseImage(&p);
            }
            cvReleaseImage(&ir_image_);
        }
    }

    IplImage *get_ir_image()
    {
        return ir_image_;
    }

    IplImage *get_bg_image()
    {
        std::lock_guard<std::mutex> lock(shared_);

        if (available_.size() > 0) {
            IplImage *image = available_.front();
            available_.pop();
            return image;
        } else {
            return nullptr;
        }
    }

    void put_bg_image(char *imageData)
    {
        std::lock_guard<std::mutex> lock(shared_);

        for(std::vector<IplImage *>::iterator b = bg_images_.begin(); b != bg_images_.end(); ++b) {
            if ((*b)->imageData == imageData) {
                available_.push(*b);
                return;
            }
        }
    }

    IplImage *get_a0_image()
    {
        return a0_image_;
    }

    IplImage *get_a1_image()
    {
        return a1_image_;
    }

    IplImage *get_a2_image()
    {
        return a2_image_;
    }

    void rearrange_image(IplImage *a2_image, IplImage *bg_image)
    {
        int w = a2_image->width;
        int h = a2_image->height;
        unsigned short *a2Data = (unsigned short *)a2_image->imageData;
        unsigned short *bgData = (unsigned short *)bg_image->imageData;

        if (!use_int_) {
            std::memcpy(bg_image->imageData, a2_image->imageData, w * h * sizeof(unsigned short));
            return;
        }

#if 0
        // 0 2 4
        //  1 3
        for (int r = 0; r < h; r++) {
            unsigned short *res0 = &bgData[(r * 2) * w];
            unsigned short *res1 = &bgData[(r * 2 + 1) * w];
            for (int c = 0; c < w; c++) {
                unsigned short *rp = &a2Data[r * w];
                unsigned short *prp = r > 0 ? rp - w : nullptr;
                if (c % 2 == 0) {
                    if (prp) {
                        res0[c] = rp[c] + prp[c + 1] * 2 + rp[c + 2];
                    } else {
                        res0[c] = rp[c] * 2 + rp[c + 2] * 2;
                    }
                    res1[c] = rp[c] + rp[c + 1] * 2 + rp[c + 2];
                } else {
                    if (prp) {
                        res0[c] = prp[c] + rp[c + 1] * 2 + prp[c + 2];
                    } else {
                        res0[c] = rp[c + 1] * 4;
                    }
                    res1[c] = rp[c] + rp[c + 1] * 2 + rp[c + 2];
                }
            }
        }
#else
        //  1 3
        // 0 2 4
        for (int r = 0; r < h; r++) {
            unsigned short *res0 = &bgData[(r * 2) * w];
            unsigned short *res1 = &bgData[(r * 2 + 1) * w];
            for (int c = 0; c < w; c++) {
                unsigned short *rp = &a2Data[r * w];
                unsigned short *nrp = r < h - 1 ? rp + w : nullptr;
                if (c % 2 == 0) {
                    res0[c] = rp[c] + rp[c + 1] * 2 + rp[c + 2];
                    if (nrp) {
                        res1[c] = rp[c] + nrp[c + 1] * 2 + rp[c + 2];
                    } else {
                        res1[c] = rp[c] * 2 + rp[c + 2] * 2;
                    }
                } else {
                    res0[c] = rp[c] + rp[c + 1] * 2 + rp[c + 2];
                    if (nrp) {
                        res1[c] = nrp[c] + rp[c + 1] * 2 + nrp[c + 2];
                    } else {
                        res1[c] = rp[c + 1] * 4;
                    }
                }
            }
        }
#endif
    }
};

/* -------------------------------------------------------------------------- */

PanaCam::PanaCam(const std::string & img_file, int range_mode, bool use_raw, bool use_int, int buff_size)
: img_file_(img_file), range_mode_(range_mode), use_raw_(use_raw), use_int_(use_int), impl_(new Impl(use_raw, use_int, buff_size))
{
    std::ostringstream oss;
    camera_name_ = "Panasonic TOF Camera";
    
    initialize();
}

/* -------------------------------------------------------------------------- */

PanaCam::~PanaCam()
{
    finalize();
}

/* -------------------------------------------------------------------------- */

void PanaCam::initialize()
{
    std::cout << PANACAM_TAG << "Initializing camera" << std::endl;
    if (use_raw_) {
        checkStatus(iPANALIB_Init(PANALIB_FORMAT_RAW, img_file_.c_str()), "Unable to initialize the camera");
        checkStatus(iPANALIB_GetRawSize(&original_image_width_, &original_image_height_, &bpp_), "Unable to get raw size");
        if (use_int_) {
            image_width_ = original_image_width_;
            image_height_ = original_image_height_ * 2;
        } else {
            image_width_ = original_image_width_;
            image_height_ = original_image_height_;
        }
    } else {
        checkStatus(iPANALIB_Init(PANALIB_FORMAT_DEPTHIR, img_file_.c_str()), "Unable to initialize the camera");
        checkStatus(iPANALIB_GetDepthSize(&original_image_width_, &original_image_height_, &bpp_), "Unable to get depth size");
        image_width_ = original_image_width_;
        image_height_ = original_image_height_;
        unsigned short width, height, bpp;
        checkStatus(iPANALIB_GetIrSize(&width, &height, &bpp), "Unable to get ir size");
        if (image_width_ != width || image_height_ != height || bpp_ != bpp)
        {
            throw PanaError("depth and ir size mismatch");
        }
        checkStatus(iPANALIB_SetOutputSel(PANALIB_OUTPUT_BG, PANALIB_OUTPUT_IR), "Unable to set output data format");
    }
    if (bpp_ != 16)
    {
        throw PanaError("Currently only 12bpp supported");
    }
    impl_->initialize(original_image_width_, original_image_height_, bpp_);
    std::cout << " done" << std::endl;

    mode_ = new bool(PANALIB_RANGE_MODE_MAX);
    max_exp_.resize(PANALIB_RANGE_MODE_MAX);

    sPANALIB_MODULE_INFO psInfo;
    sPANALIB_MODE_INFO modeInfo;
    unsigned short fovx, fovy;
    unsigned short exp;
    unsigned short coring;

    checkStatus(iPANALIB_GetModuleInfo(&psInfo), "Unable to get module info");
    std::cout << PANACAM_TAG << "module name: " << psInfo.cModuleName << std::endl;
    std::cout << PANACAM_TAG << "module version: " << psInfo.unModuleVer << std::endl;
    std::cout << PANACAM_TAG << "firmware version: " << psInfo.unFirmVer << std::endl;
    std::cout << PANACAM_TAG << "library version: " << psInfo.unLibraryVer << std::endl;

    checkStatus(iPANALIB_IsRangeMode(mode_), "Unable to get valid range modes");
    for (int i = 0; i < PANALIB_RANGE_MODE_MAX; i++) {
        if (!mode_[i]) {
            continue;
        }
        checkStatus(iPANALIB_GetModeInfo(&modeInfo, (ePANALIB_RANGE_MODE)i), "Unable to get mode info");
        std::cout << PANACAM_TAG << "mode" << i << " method: " << modeInfo.eMethod << std::endl;
        std::cout << PANACAM_TAG << "mode" << i << " dynamic ranging: " << modeInfo.eDr << std::endl;
        std::cout << PANACAM_TAG << "mode" << i << " near limit: " << modeInfo.unNearLimit << std::endl;
        std::cout << PANACAM_TAG << "mode" << i << " far limit: " << modeInfo.unFarLimit << std::endl;

        checkStatus(iPANALIB_GetExposureMax((ePANALIB_RANGE_MODE)i, &max_exp_[i]), "Unable to get max exposure");
        std::cout << PANACAM_TAG << "mode" << i << " max exposure: " << max_exp_[i] << std::endl;
    }

    checkStatus(iPANALIB_GetFov(&fovx, &fovy), "Unable to get FOV of the lens");
    std::cout << PANACAM_TAG << "FOV: h " << fovx << " v " << fovy << std::endl;

    std::cout << PANACAM_TAG << "raw size: w " << original_image_width_ << " h " << original_image_height_ << " bpp " << bpp_ << std::endl;

    if (range_mode_ >= PANALIB_RANGE_MODE_MAX || !mode_[range_mode_]) {
        throw PanaError("Illegal range mode");
    }
    checkStatus(iPANALIB_SetRangeMode((ePANALIB_RANGE_MODE)range_mode_), "Unable to set range modoe");
    std::cout << PANACAM_TAG << "Applying range mode " << range_mode_ << std::endl;

    checkStatus(iPANALIB_GetExpNum(&exp), "Unable to get current exposure");
    std::cout << PANACAM_TAG << "exposure: " << exp << std::endl;

    checkStatus(iPANALIB_GetCoringValue(&coring), "Unable to get current coring");
    std::cout << PANACAM_TAG << "coring: " << coring << std::endl;

    getCamTemp();

    // Turn laser off
    setLaserPattern(0);
}

/* -------------------------------------------------------------------------- */

void PanaCam::finalize()
{
    if (running_)
        stopCapture();
    
    std::cerr << PANACAM_TAG << "Destroying camera" << std::endl;
    iPANALIB_Fin();
    delete mode_;
    impl_->finalize();
}

/* -------------------------------------------------------------------------- */

void PanaCam::checkStatus(int ret_code, const char * err_desc, int success_code) const
{
    if(ret_code != success_code)
        throw PanaError(err_desc, ret_code);
}

/* -------------------------------------------------------------------------- */

void PanaCam::startCapture()
{
    std::cerr << "START" << std::endl;

    assert(running_ == false);
    iPANALIB_Start();
    thread_ = std::thread(&PanaCam::threadProc, this);
}

/* -------------------------------------------------------------------------- */

void PanaCam::stopCapture()
{
    if(!running_)
        return;
    std::cerr << "STOP" << std::endl;

    running_ = false;
    iPANALIB_Stop();
    thread_.join();
}

/* -------------------------------------------------------------------------- */

void PanaCam::setShutterSpeed(uint32_t shutter)
{
    std::lock_guard<std::mutex> lock(shared_);

    // Exposure time per alpha (us) = 10T * beta
    // 1T = 1 / 45MHz
    // beta = 12
    float exp_per_alpha_in_us = 1.0 / (45 * 1000 * 1000) * 10 * 12 * (1000 * 1000);
    unsigned short alpha = (float)shutter / exp_per_alpha_in_us;
    if (alpha < EXP_MIN) {
        alpha = EXP_MIN;
    } else if (alpha > max_exp_[range_mode_]) {
        alpha = max_exp_[range_mode_];
    }
    checkStatus(iPANALIB_SetExpNum(alpha), "Unable to set exposure");
    std::cout << PANACAM_TAG << "exp num: " << alpha << std::endl;
}

/* -------------------------------------------------------------------------- */

void PanaCam::setFps(int fps)
{
    // Always 30FPS, Ignored.
}

/* -------------------------------------------------------------------------- */

void PanaCam::setGain(double gain)
{
    unsigned short val = 0x1000 + 0x100 * gain;
    checkStatus(iPANALIB_AfeWrite_InternalUse((unsigned short)0xc371, val), "Unable to write AFE register");
    checkStatus(iPANALIB_SetIrGain((float)gain), "Unable to set ir gain");
    std::cout << PANACAM_TAG << "gain: " << val << std::endl;
}

/* -------------------------------------------------------------------------- */

uint32_t PanaCam::getShutterSpeed()
{
    std::lock_guard<std::mutex> lock(shared_);

    return 0; // XXX
}

/* -------------------------------------------------------------------------- */

double PanaCam::getAnalogGain()
{
    return 1.0; // No gain controll
}

/* -------------------------------------------------------------------------- */

double PanaCam::getDigitalGain()
{
    return 1.0; // No gain controll
}

/* -------------------------------------------------------------------------- */

uint16_t PanaCam::getLaserSequence()
{
     return laser_pattern_;
}

/* -------------------------------------------------------------------------- */

void PanaCam::threadProc()
{
    IplImage *ir_image = impl_->get_ir_image();
    IplImage *a0_image = impl_->get_a0_image();
    IplImage *a1_image = impl_->get_a1_image();
    IplImage *a2_image = impl_->get_a2_image();

    running_ = true;

    bool do_conv = bpp_ > 8;
    try
    {
        while(running_)
        {
            // capture              
            
            IplImage *image = impl_->get_bg_image();
            
            if(image)
            {
                if (use_raw_) {
                    checkStatus(iPANALIB_GetRaw(a0_image, a1_image, a2_image), "Unable to get raw image");
                    impl_->rearrange_image(a2_image, image);
                } else {
                    checkStatus(iPANALIB_GetDepthIr(ir_image, image), "Unable to get depth/ir image");
                }
                Pix8 *buff = (Pix8 *)image->imageData;
                if(do_conv)
                    conv12b8(buff, image_width_*image_height_, use_int_);

                videoFrameReceived(buff, image_width_, image_height_, image_width_, 0);
            }
            else
            {
                std::cerr << "NO BUFFER" << std::endl;
            }            
        } 
    }
    catch (std::exception & ex) 
    {
        std::cerr << ex.what() << std::endl;
        err_ = std::current_exception();
    }
    catch (...)
    {
        std::cerr << "unknown exception" << std::endl;
        err_ = std::current_exception();
    }
        
    running_ = false;
}

/* -------------------------------------------------------------------------- */

void PanaCam::returnBackBuffer(const Pix8 * buff)
{
    std::lock_guard<std::mutex> lock(shared_);

    impl_->put_bg_image((char *)buff);
}

/* -------------------------------------------------------------------------- */

float PanaCam::getCamTemp() const
{
    unsigned short warn;
    short temperature;

    checkStatus(iPANALIB_GetTemperature(&warn, &temperature), "Unable to get current temperature");
    std::cout << PANACAM_TAG << "temperature: " << (float)temperature * 0.25 << " warn " << warn << std::endl;

    return (float)temperature * 0.25;
}

/* -------------------------------------------------------------------------- */

void PanaCam::setLaserPattern(int pattern)
{
    unsigned short gpo5_val = pattern == 0 ? 0x0ef2 : 0x0ff2;
    unsigned short illu_val = pattern == 0 ? 0x1a : 0x10;

    checkStatus(iPANALIB_AfeWrite_InternalUse((unsigned short)0xc024, gpo5_val), "Unable to write AFE register");
    checkStatus(iPANALIB_AfeWrite_InternalUse((unsigned short)0xc084, illu_val), "Unable to write AFE register");
}

/* -------------------------------------------------------------------------- */

void PanaCam::conv12b8(Pix8 * im, size_t num_pixels, bool use_int)
{
    uint16_t * pix12 = reinterpret_cast<uint16_t *>(im);
    Pix8 * pix8 = im;
    uint16_t * pix12E = pix12 + num_pixels;
    int shift = use_int ? 6 : 4;
    
    for(; pix12 < pix12E; ++pix8,++pix12)
        *pix8 = *pix12 >> shift;
}

/* -------------------------------------------------------------------------- */
