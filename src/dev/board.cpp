/* 
 * Board info - class provide info about board
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondrej Fisar, fisar@magik-eye.com
 *
 */

/* -------------------------------------------------------------------------- */

#ifdef USE_RPICAM

#include "board.h"
#include "wiringPi.h"

/* -------------------------------------------------------------------------- */

// old library support

#ifndef PI_MODEL_CM3
#define PI_MODEL_CM3 10
#endif

/* -------------------------------------------------------------------------- */

bool isCm3board()

{
  int model, rev, mem, maker, overVolted;
  piBoardId(&model, &rev, &mem, &maker, &overVolted);
  
  return model == PI_MODEL_CM || model == PI_MODEL_CM3;
}

/* -------------------------------------------------------------------------- */

#else

bool isCm3board() { return false; }

#endif

/* -------------------------------------------------------------------------- */
