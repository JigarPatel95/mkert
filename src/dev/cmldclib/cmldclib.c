#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>

#include <wiringPi.h>
#include <wiringPiSPI.h>

#include "cmldc_bitmap.h"

#define CMMB1_GPIO_CRESET   26  /* CM pin 87 */
#define CMMB1_GPIO_CDONE    4   /* CM pin 15  */
#define CMMB1_GPIO_NCS0     8   /* CM pin 27, SPI NCS0 */
#define CMMB1_GPIO_RESET    34  /* CM pin 52 */
#define CMMB1_GPIO_LENB     35  /* CM pin 54 */
#define CMMB3_GPIO_USERLED  16  /* CM pin 57, CMMB3 only */

#define FPGA_CONFIG_BUFFER_SIZE  1024
#define FPGA_CONFIG_DUMMY_SIZE   16
static uint8_t fpga_config_buffer[FPGA_CONFIG_BUFFER_SIZE];

#define CMD_NOP               0
#define CMD_SET_CONTROL       1
#define CMD_SET_MIPI          2
#define CMD_SET_OFFSET        3
#define CMD_SET_ONTIME        4
#define CMD_SELECT_TEMP       5

#define CONTROL_LASER_FORCE_ON  0x800000
#define CONTROL_PHASE3          0x400000

#define MIPI_MASK_ALL           0x00003f
#define MIPI_EDGE               0x001000

static int spi_send_recv(uint32_t send, uint32_t* recvp)
{
    uint32_t data = send;
    
    if (wiringPiSPIDataRW(0, (void*) &data, 4) < 0) {
        perror("wiringPiSPIDataRW");
        return -1;
    }
    if (recvp) {
        *recvp = data;
    }

    return 0;
}

static int spi_command(int command, int arg)
{
    uint32_t send = (command << 24) | (arg & 0xffffff);
    uint32_t recv;
    if (spi_send_recv(send, &recv) < 0) {
        return -1;
    }
    //printf("send: %08X  recv: %08X\n", send, recv);
    return 0;
}

static int spi_command_recv(int command, int arg, uint32_t* recv)
{
    uint32_t send = (command << 24) | (arg & 0xffffff);
    if (spi_send_recv(send, recv) < 0) {
        return -1;
    }
    //printf("send: %08X  recv: %08X\n", send, *recv);
    return 0;
}

static int configure_fpga(void)
{
    pinMode(CMMB1_GPIO_NCS0, OUTPUT);
    pinMode(CMMB1_GPIO_CRESET, OUTPUT);
    pinMode(CMMB1_GPIO_CDONE, INPUT);
    pinMode(CMMB1_GPIO_RESET, OUTPUT);
    pinMode(CMMB1_GPIO_LENB, OUTPUT);

    digitalWrite(CMMB1_GPIO_RESET, 1);
    digitalWrite(CMMB1_GPIO_LENB, 0);
    
    int spi = wiringPiSPISetupMode(0, 16000000, 0);
    if (spi < 0) {
        perror("wiringPiSPISetupMode");
        return -1;
    }

    // Assert CRESET
    digitalWrite(CMMB1_GPIO_NCS0, 0); // ensure SPI Peripheral mode
    digitalWrite(CMMB1_GPIO_CRESET, 0);
    delayMicroseconds(200+100);
    digitalWrite(CMMB1_GPIO_CRESET, 1);
    delayMicroseconds(600+100);

    // Check CDONE
    if (digitalRead(CMMB1_GPIO_CDONE) != 0) {
        fprintf(stderr, "ERROR: FPGA failed (CDONE not low)\n");
        return -1;
    }

    const uint8_t* ptr = cmldc_bitmap;
    uint32_t count = cmldc_bitmap_size;
    while (count > 0) {
        int len = count;
        if (len >= FPGA_CONFIG_BUFFER_SIZE) {
            len = FPGA_CONFIG_BUFFER_SIZE;
        }
        memcpy(fpga_config_buffer, ptr, len);
        if (wiringPiSPIDataRW(0, fpga_config_buffer, len) < 0) {
            perror("wiringPiSPIDataRW");
            return -1;
        }
        ptr += len;
        count -= len;
    }

    memset(fpga_config_buffer, 0, FPGA_CONFIG_DUMMY_SIZE);
    if (wiringPiSPIDataRW(0, fpga_config_buffer, FPGA_CONFIG_DUMMY_SIZE) < 0) {
        perror("wiringPiSPIDataRW");
        return -1;
    }

    // Check CDONE
    if (digitalRead(CMMB1_GPIO_CDONE) == 0) {
        fprintf(stderr, "ERROR: FPGA failed (CDONE not high)\n");
        return -1;
    }
    
    return 0;
}

static int bitmap_expand_4x(int bitmap)
{
    bitmap &= 0xf;
    return (bitmap << 12) | (bitmap << 8) | (bitmap << 4) | bitmap;
}

static int sec_double_to_int(double sec)
{
    return (int) lround(sec * 12e6) & 0xffffff;
}

// ----------- API -----------

int cmldc_open(void)
{
    if (geteuid() != 0) {
        fprintf(stderr, "ERROR: must be root\n");
        return -1;
    }

    wiringPiSetupGpio();

    if (configure_fpga() < 0) {
        return -1;
    }

    spi_command(CMD_SET_CONTROL, 0);
    spi_command(CMD_SET_MIPI, MIPI_MASK_ALL);
    spi_command(CMD_SET_OFFSET, 0);
    spi_command(CMD_SET_ONTIME, 0);

    digitalWrite(CMMB1_GPIO_LENB, 1);
    digitalWrite(CMMB1_GPIO_RESET, 0);
    
    return 0;
}

int cmldc_close(void)
{
    return 0;
}

int cmldc_set_force_on(int bitmap)
{
    return spi_command(CMD_SET_CONTROL,
                       CONTROL_LASER_FORCE_ON + bitmap_expand_4x(bitmap));
}

int cmldc_start_sync(int sequence, double startTimeOffset, double onTime)
{
    if (startTimeOffset < -1 || startTimeOffset > 1 ||
        onTime <= 0 || onTime > 1) {
        fprintf(stderr, "cmldc_start_sync: time value out of range\n");
        return -1;
    }

    int control = sequence & 0xffff;
    if ((sequence & 0x10000) != 0) {
        control |= CONTROL_PHASE3;
    }

    int mipiFlags = MIPI_MASK_ALL;
    if (startTimeOffset < 0) {
        startTimeOffset = - startTimeOffset;
        mipiFlags |= MIPI_EDGE;
    }
    
    spi_command(CMD_SET_CONTROL, 0);
    spi_command(CMD_SET_MIPI, mipiFlags);
    spi_command(CMD_SET_OFFSET, sec_double_to_int(startTimeOffset));
    spi_command(CMD_SET_ONTIME, sec_double_to_int(onTime));
    spi_command(CMD_SET_CONTROL, control);

    return 0;
}

int cmldc_stop_sync(void)
{
    return spi_command(CMD_SET_CONTROL, 0);
}

int cmldc_get_laser_phase(void)
{
    uint32_t recv = 0;
    spi_command_recv(CMD_NOP, 0, &recv);
    return (recv >> 4) & 0x3;
}

double cmldc_read_temperature(int n)
{
    if (n < 0 || n > 3) {
        return -1000.0;
    }
    spi_command(CMD_SELECT_TEMP, n);
    uint32_t recv = 0;
    spi_command_recv(CMD_NOP, 0, &recv);
    uint32_t value = ((recv >> 8) & 0xf00) | ((recv >> 24) & 0xff);
    if (value < 15) {
        return -1000.0;
    } else {
        return (value / 4096.0) * 256.0 - 50.0;
    }
}

void cmldc_set_user_led(int on)
{
    pinMode(CMMB3_GPIO_USERLED, OUTPUT);
    digitalWrite(CMMB3_GPIO_USERLED, on ? 0 : 1);
}
