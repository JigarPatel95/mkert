cmldclib
========

Laser control library for CMMB1, synchronized mode.
See cmldclib.h for API.
