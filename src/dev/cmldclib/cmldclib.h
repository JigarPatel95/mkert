#ifndef _CMLDCLIB_H
#define _CMLDCLIB_H 1

#ifdef __cplusplus
extern "C" {
#endif

/*
   int cmldc_open(void);

      Open and initialize the laser contoller device.
      All lasers will be turned OFF.

      Returns zero if success.
      Returns negative value if error.
*/
int cmldc_open(void);

/*
   int cmldc_close(void);

      Close the laser controller device.
      Laser states will not be changed.

      Returns zero if success.
      Returns negative value if error.
*/
int cmldc_close(void);

/*
   int cmldc_set_force_on(int bitmap);
  
      Set the laser state statically.

      bitmap: Laser state bitmap.
         bit 0 of bitmap value corresponds laser unit 1, bit 1 for unit 2,...
         bit 2 for unit 4. Higher bits will be ignored.
         For example:
            bitmap = 0 will turn off all lasers.
            bitmap = -1 will turn on all lasers.
            bitmap = 0x5 will turn on 1 and 3, turn off 2 and 4.

      Returns zero if success.
      Returns negative value if error.
*/
int cmldc_set_force_on(int bitmap);

#define CMLDC_SEQUENCE_ALL    0x0ffff
#define CMLDC_SEQUENCE_13_24  0x0a5a5
#define CMLDC_SEQUENCE_1234   0x08421
#define CMLDC_SEQUENCE_123    0x10421
    
/*
   int cmldc_start_sync(int sequence, double startTimeOffset, double onTime);

      Start periodic flashing of lasers, synchronized to video capture timing.

      sequence: Sequence pattern of laser flashing
         CMLDC_SEQUENCE_ALL    All lasers on
         CMLDC_SEQUENCE_13_24  Laser 1+3 and 2+4 alternately
         CMLDC_SEQUENCE_1234   Laser 1 2 3 4 sequentially
         CMLDC_SEQUENCE_123    Laser 1 2 3 sequentially

      startTimeOffset: Start time offset in seconds.
          Timing is measured from MIPI transfer end if positive,
          and measured from MIPI transfer start if negative.
          (absolute value will be used)
      onTime: Laser on time in seconds.

      Arguments are subject to the following restrictions:
          -1.0 <= startTimeOffset <= 1.0
          0 < onTime <= 1.0

      Returns zero if success.
      Returns negative value if error.
*/
int cmldc_start_sync(int mode, double startTimeOffset, double onTime);

/*
   int cmldc_stop_sync(void);

      Stop periodic laser flashing previously started.

      Returns zero if success.
      Returns negative value if error.
*/
int cmldc_stop_sync(void);

/*
   int cmldc_get_laser_phase(void);

      Get current laser phase.

      Returns current laser phase:
          0,1,2,3 (2 or 4 cycle mode)
          0,1,2   (3 cycle mode)
*/
int cmldc_get_laser_phase(void);

/*
   double cmldc_read_temperature(int n);

      Get temperature sensor value (CMMB3 only)

      n: Sensor number (0-3)

      Returns
          temperature in centigrade,
          -300 or less if error.
*/
double cmldc_read_temperature(int n);
    
/*
   void cmldc_set_user_led(int on);

      Set user led state (CMMB3 only)
*/
void cmldc_set_user_led(int on);

#ifdef __cplusplus
}
#endif

#endif
