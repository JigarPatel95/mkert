/*
 * v4lcam.cpp - wrapper for V4L camera
 * 
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 * 
 * Based on https://linuxtv.org/docs.php capture example
 *   and inspired by vcmipidemo.c by Visual Components
 */

/* -------------------------------------------------------------------------- */

#ifndef _V4L2CAM_H_
#define _V4L2CAM_H_

/* -------------------------------------------------------------------------- */

// STL includes

#include <exception>
#include <stdexcept>
#include <iostream>
#include <sstream>

#include <mutex>
#include <thread>
#include <queue>

#include <cmath>

/* -------------------------------------------------------------------------- */

#include "img.h"

/* -------------------------------------------------------------------------- */

namespace mke {
namespace dev {

/* -------------------------------------------------------------------------- */

class V4L2Error : public std::runtime_error

{
  std::string                   desc_;
  int                           errno_;	  // returned value if passed
  std::string                   strerror_;
  
  
public:
  // constructor with reason
  V4L2Error(const char* desc, bool store_errno = true);
  
  // destructor
  virtual ~V4L2Error()
  {};
  
  // describe exception
  virtual const char* what() const throw();
};

/* -------------------------------------------------------------------------- */

// camera class

class V4L2Cam

{  
public:    
  // constructor
  V4L2Cam(int videoid, int num_buffers);
  
  // destructor
  virtual ~V4L2Cam();
  
  // get sensor name
  const char * getName() const { return camera_name_.c_str(); };
  
  // start capturing
  void startCapture();
  
  // stop capture
  void stopCapture();

  int32_t getShutterSpeed() const;

  void setShutterSpeed(int32_t shutter);

  int32_t getGain() const;

  void setGain(int32_t gain);
  
  void returnBackBuffer(const Pix8 * buff);
  
  virtual void videoFrameReceived(const Pix8 * buff, uint32_t w, uint32_t h, 
                                  uint32_t memw) = 0;  
  
  void setVideoFormat(uint32_t w, uint32_t h, uint32_t pixtype);
  
  void getVideoFormat(uint32_t & w, uint32_t & h, uint32_t & stride, uint32_t & pixtype) const;
  
protected:

  struct Item 
    {
      void      * start;
      size_t      length;
      
      // default constructor
      Item(void * start = nullptr, size_t len = 0)
      : start(start), length(len)
      {};
    };

  class Buffer : protected std::vector<Item>
    {      
    public:
      using std::vector<Item>::size;
      using std::vector<Item>::operator[];

      virtual ~Buffer();

      virtual void clear();

      void prepare(size_t num);
    };

  typedef int HANDLE; 
  
  int                   video_id_;        // camera identification
  char                  device_name_[50]; // device file
  unsigned int          num_buffers_;     // number of buffer to be used
  std::string           camera_name_;
  
  HANDLE                fd_;
  size_t                buff_size_;       // current size of one buffer
  
  std::thread           thread_;
  volatile bool         running_;

  std::mutex            shared_;
  
  Buffer                buffer_;
  
  uint32_t              image_width_;
  uint32_t              image_height_;
  uint32_t              image_stride_;
  uint32_t              pixel_format_;
  
  std::exception_ptr    err_;
  
  void initialize();
  
  void finalize();
  
  void threadProc();   
  
  bool readFrame();
  
  void processImage(const void *p, int size);
  
  // takes array of 10bit (progressive) pixels and makes 8bit values from the by shifting right >>2
  static void conv10p8(Pix8 * im, size_t stride, size_t h, size_t num_bytes);
  
  void checkStatus(int ret_code, const char * err_desc) const;
  
  void initializeMmap();
  
  void finalizeMmap();

  int32_t getControlRegister(uint32_t addr) const;

  void setControlRegister(uint32_t addr, int32_t value);
};

/* -------------------------------------------------------------------------- */

}   // end of mke::dev
}   // end of mke

/* -------------------------------------------------------------------------- */

#endif // _V4L2CAM_H_

