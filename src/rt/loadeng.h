/*
 * LoadEngine - loading images from drive
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef _LOADENG_H_
#define _LOADENG_H_

/* -------------------------------------------------------------------------- */

#include "engine.h"
#include <util/fpscnt.h>

/* -------------------------------------------------------------------------- */

namespace mke {
namespace rt {

/* -------------------------------------------------------------------------- */

// loading engine - is able open just one file in the momment
    
class LoadEngine : public Engine
{
protected:
  const MkertConfig::EngineConfig   &   eng_cfg_;       // general engine cfg
  
  std::vector<std::string>              files_;         // list of files in dir
  dev::MemImage<dev::Pix8>              curr_image_;    // currently processed image
  dev::CamFrame<dev::Pix8>              curr_frame_;    // wrap for current image
  uint32_t                              im_index_;      // image index
  uint32_t                              frame_index_;   // frame index
  uint16_t                              num_detectors_; // number of detectors
  
  mke::util::FpsCounter                 fps_cnt_;       // FPS counter
  
  bool                                  fake;           // generate fake images
  bool                                  ready;          // is possible take image ?
  bool                                  running;        // is engien running
  

  // generate random image
  void generateRandom(dev::MemImage<dev::Pix8> & out);
  
public:
  // constructor
  LoadEngine(const MkertConfig & cfg, int num_detectors):
    Engine(cfg), 
    eng_cfg_(cfg.getEngineConfig()),
    curr_image_(cfg.getCameraConfig().width, cfg.getCameraConfig().height, 
                std::max(eng_cfg_.load_stride, cfg.getCameraConfig().width)),
    curr_frame_(curr_image_,0,0,0),
    im_index_(0), frame_index_(0), num_detectors_(num_detectors), fps_cnt_(10, true),
    fake(false), ready(true), running(false)
  {};
  
  // initialization of devices
  virtual void initialize();

  // get next frame from camera
  virtual const dev::CamFrame<dev::Pix8> * getFrame(uint32_t timout = 0xffffffff);
  
  // release frame
  virtual void releaseFrame(const dev::CamFrame<dev::Pix8> * frame);
  
  // state has been changed
  virtual void stateChanged(mke::api::MkEStateType state) 
  { 
    running = state != mke::api::MKE_STATE_IDLE; 
  };
  
  // fire some pattern - ignored
  virtual void fireLaserPattern(int pattern, float strobe_len, float strobe_off) {};
  
  // set shutter speed
  virtual void setShutterSpeed(uint32_t exposure_) {};  
  
  // get shutter speed
  virtual uint32_t getShutterSpeed() { return 0; };
  
  // get analog gain
  virtual uint32_t getAnalogGain() { return 0; };
  
  // get digital gain
  virtual uint32_t getDigitalGain() { return 0; };
  
  // set ISO value
  virtual bool setISO(uint32_t) { return true; };
  
  virtual void setProfile(const ProfileType & profile) {};

  virtual void getProfile(ProfileType & profile) const {};  
  
  virtual void validateProfile(const ProfileType & profile) const {};
  
  virtual void getStats(StatsType & stats) const;
  
  virtual void resetStats() {};
};

/* -------------------------------------------------------------------------- */

} // end of mke::rt namespace
} // end of mke namespace

/* -------------------------------------------------------------------------- */

#endif // _LOADENG_H_
