/* 
 * SerialListener
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondrej Fisar, fisar@magik-eye.com
 *
 */

/* -------------------------------------------------------------------------- */

#pragma once

/* -------------------------------------------------------------------------- */

#include <boost/asio.hpp>

/* -------------------------------------------------------------------------- */

#include "util/logging.h"
#include "rt/listener.h"
#include "rt/api/memory_pool.h"
#include "rt/api/request_sink.h"

#include "rt/conn_session.h"

/* -------------------------------------------------------------------------- */

namespace mke {
namespace rt {

typedef ConnSession<boost::asio::serial_port> SerialSession;

class SerialListener : public Listener 
{
private:
    
  const std::string           &         port_;
  boost::asio::serial_port::baud_rate   baudrate_;
  boost::asio::serial_port::parity      parity_;
  boost::asio::serial_port::stop_bits   stopbits_;
  boost::asio::serial_port::flow_control flowcontrol_;
  boost::asio::serial_port              serial_;
  api::MemoryPool             &         api_pool_;
  api::RequestSink            &         request_sink_;
  
public:
  
  // constructor
  SerialListener(boost::asio::io_service& io_service, api::MemoryPool &api_pool, 
                 api::RequestSink &request_sink, const std::string & port, 
                 boost::asio::serial_port::baud_rate baudrate,
                 boost::asio::serial_port::parity parity,
                 boost::asio::serial_port::stop_bits stopbits,
                 boost::asio::serial_port::flow_control flowcontrol) : 
    port_(port),
    baudrate_(baudrate),
    parity_(parity),
    stopbits_(stopbits),
    flowcontrol_(flowcontrol),
    serial_(io_service, port),
    api_pool_(api_pool),
    request_sink_(request_sink)
  {};
  
  // destructor
  ~SerialListener () {
    LOG_INFO << "dtor Serial listener, port " << port_;
    stop();
  }
  
  // start listener
  virtual bool start(void) {
    LOG_INFO << "Starting Serial listener, port " << port_;

    boost::system::error_code ec; 

    serial_.set_option(baudrate_, ec);
    serial_.set_option(parity_, ec);
    serial_.set_option(flowcontrol_, ec);
    serial_.set_option(stopbits_, ec);
    serial_.set_option(boost::asio::serial_port::character_size(8), ec);

    if (ec)
      {
        LOG_WARN << ec.message() << ", " << port_;
        return false;
      }

    std::stringstream conn_id;
    conn_id << "Serial session, " << port_;
    
    std::make_shared<SerialSession>(
          std::move(serial_), api_pool_, request_sink_, conn_id.str())->start();  
    return true;  
  }

  // stop listener
  virtual void stop(void) {
    LOG_INFO << "Stopping listener, Serial:" << port_;
  }

};    

/* -------------------------------------------------------------------------- */

} // namespace rt
} // namespace mke

/* -------------------------------------------------------------------------- */