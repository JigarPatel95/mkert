/* 
 * TCPSession - Represents a single connection from a client.
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 * author: Ondrej Fisar, fisar@magik-eye.com
 *
 */

/* -------------------------------------------------------------------------- */

#pragma once

/* -------------------------------------------------------------------------- */

#include <sstream>
#include <boost/asio.hpp>

/* -------------------------------------------------------------------------- */

#include "rt/conn_session.h"

/* -------------------------------------------------------------------------- */

namespace mke {
namespace rt {

/// Represents a single connection from a client.
  
class TCPSession : public ConnSession<boost::asio::ip::tcp::socket>
{
public:
  // constructor
  TCPSession(boost::asio::ip::tcp::socket socket, api::MemoryPool &api_pool, 
             api::RequestSink &request_sink, const std::string & conn_id) : 
    ConnSession(std::move(socket), api_pool, request_sink, conn_id) 
  {}
  
  void closeAndEmit(void) {
    boost::system::error_code ec;
    bus_.shutdown(boost::asio::ip::tcp::socket::shutdown_both, ec);
    
    ConnSession<boost::asio::ip::tcp::socket>::closeAndEmit();
  }
};  

} // namespace rt
} // namespace mke

/* -------------------------------------------------------------------------- */
