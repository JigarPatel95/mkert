/*
 * CldbEngine - interface for engine that is connection to devices
 * 
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#include "cldbeng.h"

/* -------------------------------------------------------------------------- */

using namespace mke::rt;
using namespace mke;

/* -------------------------------------------------------------------------- */

CldbEngine::~CldbEngine()
{
    // stop the lasers
    Cldb::setLaserSequence(0);
    stopCapture();
}

/* -------------------------------------------------------------------------- */

void CldbEngine::initialize()
{
    // change the exposure settings
    
    CldbEngine::setShutterSpeed(cam_cfg_.shutter_speed);
    CldbEngine::setISO(cam_cfg_.iso);
    Cldb::setFps(cam_cfg_.fps);
}

/* -------------------------------------------------------------------------- */
  
void CldbEngine::stateChanged(mke::api::MkEStateType state)
{
  if(state == mke::api::MKE_STATE_IDLE)
    {
      if(last_state_ != mke::api::MKE_STATE_DEPTH_SENSOR)
        {
            // change the exposure settings
            
            CldbEngine::setShutterSpeed(cam_cfg_.shutter_speed);
            CldbEngine::setISO(cam_cfg_.iso);
        }

      // stop the lasers
      Cldb::setLaserSequence(0);
      stopCapture();
    }
  else
    {

      if(state == mke::api::MKE_STATE_DEPTH_SENSOR)
        {
            // start flickering with lasers
            startSync();
        }
      
      startCapture();
    }
    
  last_state_ = state;
}

/* -------------------------------------------------------------------------- */

void CldbEngine::startSync()
{
    uint16_t seq = 0x0000;
    unsigned int det_num = detector_.getNoDetectors();
    switch(det_num)
    {
      case 4:
          seq |= (0x0f & detector_.getLaserPattern(3)) << 12;
      case 3:
          seq |= (0x0f & detector_.getLaserPattern(2)) << 8;
      case 2:
          seq |= (0x0f & detector_.getLaserPattern(1)) << 4;
      case 1:
          seq |= 0x0f & detector_.getLaserPattern(0);
      break;
      default:
        throw std::runtime_error("Unsupported number of detectors");
    }
    LOG_INFO << "sync " << seq << " at " << det_num;

    if(det_num == 1)
    {
        det_num = 4;
        seq = seq | (seq << 4) | (seq << 8) | (seq << 12);
    }
    else if(det_num == 2)
    {
        det_num = 4;
        seq = seq | (seq << 8);
    }
    Cldb::setLaserSequence(seq); // by PCAM
}

/* -------------------------------------------------------------------------- */

void CldbEngine::videoFrameReceived(const dev::Pix8 * data, int w, int h, int memw, uint16_t phase)

{

  uint64_t now;
  mke::dev::CamFrame<dev::Pix8>::setTimerToNow(now);
  
  // lock the queue
  
  std::unique_lock<std::mutex> lock(mtx_frames_);
  
  unsigned int num_det = detector_.getNoDetectors();
  if(num_det == 0)
      num_det = 1;
  frames_received_++;

  if(frames_.size() < max_frames_)
    {
        frames_.push(dev::CamFrame<dev::Pix8>(const_cast<dev::Pix8 *>(data),
                                            w, h, memw, now, frames_received_-1, phase%num_det));
        
        lock.unlock();
        cv_frames_.notify_one();
        return;
    }
  else
    {
        frames_droped_++;
        if(dropped_stamp_ > now || (now-dropped_stamp_) > MKE_ENGINE_DROPLOG_TIMEOUT)
        {
          std::cout << "! Too much frames in Q. Already dropped " 
              << frames_droped_ << " frames." << std::endl;
          dropped_stamp_ = now;
        }
    }

  // return back buffer if not used
  
  returnBackBuffer(data);
}

/* -------------------------------------------------------------------------- */

const dev::CamFrame<dev::Pix8> * CldbEngine::getFrame(uint32_t timeout)

{
  // check for the exception in thread proc
    
  if (err_)
      std::rethrow_exception(err_);
    
  // lock the queue
  
  std::unique_lock<std::mutex> lock(mtx_frames_);
  
  // no new frame available
  
  while(!frames_.size())
    if(cv_frames_.wait_for(lock, std::chrono::milliseconds(timeout)) 
                                                    == std::cv_status::timeout)
      return NULL;

  // get next frame and lock it
  
  dev::CamFrame<dev::Pix8> & frame = frames_.front();
//  lockBuffer(frame.header_);

  // return it
  
  return &frame;
}

/* -------------------------------------------------------------------------- */

void CldbEngine::releaseFrame(const dev::CamFrame<dev::Pix8> * frame)

{
  // lock the queue
  
  std::lock_guard<std::mutex> lock(mtx_frames_);
  
  // check if releasing last getted frame
  
  assert(frame == &frames_.front());

  dev::CamFrame<dev::Pix8> & curr = frames_.front();
  
//  unlockBuffer(curr.header_);
  returnBackBuffer(frame->mem);
  
  // release frame from queue
  frames_.pop();
}

/* -------------------------------------------------------------------------- */

void CldbEngine::fireLaserPattern(int pattern, float strobe_len, float strobe_off) {
    uint16_t seq = 0x0f & pattern;
    seq = seq | (seq << 4) | (seq << 8) | (seq <<12);
    Cldb::setLaserSequence(seq);

    std::this_thread::sleep_for(std::chrono::milliseconds(200));
}

/* -------------------------------------------------------------------------- */

void CldbEngine::setShutterSpeed(uint32_t exposure) {
    if (exposure > 0)
        Cldb::setShutterSpeed(exposure/1000000.f);
    else
        Cldb::setShutterSpeed(-1);
}

/* -------------------------------------------------------------------------- */

uint32_t CldbEngine::getShutterSpeed() { 
    return round(Cldb::getShutterSpeed()*1e6);
}

/* -------------------------------------------------------------------------- */

uint32_t CldbEngine::getAnalogGain() { return 1000; }

/* -------------------------------------------------------------------------- */

uint32_t CldbEngine::getDigitalGain() { 
    return round(Cldb::getGain()*1000); 
}

/* -------------------------------------------------------------------------- */

bool CldbEngine::setISO(uint32_t value) { 
    if(value > 0)
        Cldb::setGain(value/100.); 
    else
        Cldb::setGain(-1);
    return true;
}

/* -------------------------------------------------------------------------- */

void CldbEngine::getStats(StatsType & stats) const
{
  stats["im_width"] = this->getImageWidth();
  stats["im_height"] = this->getImageHeight();
}

/* -------------------------------------------------------------------------- */

void CldbEngine::getProfile(ProfileType & json_profile) const
{
  json_profile["shutter_speed"] = round(Cldb::getShutterSpeed()*1e6);
  json_profile["gain_digital"] = round(Cldb::getGain()*1000);
  json_profile["fps"] = cam_cfg_.fps;
  
  json_profile["width"] = getImageWidth();
  json_profile["height"] = getImageHeight();
}

/* -------------------------------------------------------------------------- */

void CldbEngine::setProfile(const ProfileType & json_profile)
{
  // parse camera parameters
  
  if(json_profile.find("shutter_speed") != json_profile.end()) 
    {
      cam_cfg_.shutter_speed = json_profile["shutter_speed"];
      setShutterSpeed(cam_cfg_.shutter_speed);
    }
  
  if(json_profile.find("gain_digital") != json_profile.end()) 
    {
      cam_cfg_.iso = round(float(json_profile["gain_digital"])/10.f);
      setISO(cam_cfg_.iso);
    }
  
  if(json_profile.find("fps") != json_profile.end())
    {
      cam_cfg_.fps = round(float(json_profile["fps"]));
      setFps(cam_cfg_.fps);
    }
}
/* -------------------------------------------------------------------------- */
