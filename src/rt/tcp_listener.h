/* 
 * SerialListener
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

/* -------------------------------------------------------------------------- */

#pragma once

/* -------------------------------------------------------------------------- */

#include <boost/asio.hpp>

/* -------------------------------------------------------------------------- */

#include "util/logging.h"
#include "rt/listener.h"
#include "rt/api/memory_pool.h"
#include "rt/api/request_sink.h"

#include "rt/tcp_session.h"

/* -------------------------------------------------------------------------- */

namespace mke {
namespace rt {

class TCPListener : public Listener 
{
private:
    
  int                                 port_;
  boost::asio::ip::tcp::acceptor      acceptor_;
  boost::asio::ip::tcp::socket        socket_;
  api::MemoryPool             &       api_pool_;
  api::RequestSink            &       request_sink_;
  
public:
  
  // constructor
  TCPListener(boost::asio::io_service& io_service, api::MemoryPool &api_pool, 
              api::RequestSink &request_sink, int port) : 
    port_(port),
    acceptor_(io_service),
    socket_(io_service),
    api_pool_(api_pool),
    request_sink_(request_sink) 
  {};
  
  // destructor
  ~TCPListener () {
    LOG_INFO << "dtor TCP listener, port " << port_;
    stop();
  }
  
  // start listener
  virtual bool start(void) {
    LOG_INFO << "Starting TCP listener, port " << port_;

    boost::system::error_code ec; 
    boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::tcp::v4(), port_);
    acceptor_.open(endpoint.protocol(), ec);
    
    if (ec)
      {
        LOG_WARN << ec.message();
        return false;
      }
      
    acceptor_.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true), ec);
    acceptor_.bind(boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port_), ec);

    if (ec)
      {
        LOG_WARN << ec.message() << ", " << endpoint;
        return false;
      }

    acceptor_.listen();
    accept();
    return true;  
  }

  // stop listener
  virtual void stop(void) {
    LOG_INFO << "Stopping listener, TCP:" << port_;
    
    if (acceptor_.is_open())
    {
      acceptor_.cancel();
      acceptor_.close();
    }
  }

private:
  
  // accept new connection
  void accept()
    {
      acceptor_.async_accept(socket_,
          [this](boost::system::error_code ec)
          {          
            if (!ec)
              {
                LOG_INFO << "Accepting TCP connection, port " << port_;
                std::stringstream conn_id;
                conn_id << "TCP session, " << socket_.remote_endpoint();
                
                std::make_shared<TCPSession>(
                          std::move(socket_), api_pool_, request_sink_,
                          conn_id.str())->start();
              }
            else
              {
                LOG_INFO << "TCP Listener stop " << ec.message();
              }
              
            accept();
          });
    }
};    

/* -------------------------------------------------------------------------- */

} // namespace rt
} // namespace mke

/* -------------------------------------------------------------------------- */