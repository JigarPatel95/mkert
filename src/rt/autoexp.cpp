/*
 * AutoExposure -
 * 
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#include "autoexp.h"

/* -------------------------------------------------------------------------- */

using namespace mke::rt;
using namespace mke;

/* -------------------------------------------------------------------------- */

AutoExposure::AutoExposure(const MkertConfig::AutoExpConfig & ae_cfg, 
                           const MkertConfig::CameraConfig & cam_cfg,
                           Engine * engine)
: cfg_(ae_cfg), cam_cfg_(cam_cfg), engine_(engine), curr_exp_(-1), cnt_(0), 
rep_up_(0), rep_down_(0)
{
  if (cfg_.enabled && (cfg_.shutters.size() != cfg_.isos.size() || cfg_.shutters.size() < 1))
    throw std::runtime_error("Invalid size of shutters and isos parameter in AutoExposure section");

  reset();
}

/* -------------------------------------------------------------------------- */

void AutoExposure::reset()
{
  curr_exp_ = 0;
  for (int i = 0; i < cfg_.shutters.size(); ++i)
    if (cfg_.shutters[i] == cam_cfg_.shutter_speed 
        && cfg_.isos[i] == cam_cfg_.iso)
      curr_exp_ = i;
    
  if (cfg_.enabled && curr_exp_ < 0)
    throw std::runtime_error("Missing default exposure in exposures for AutoExposure");
}

/* -------------------------------------------------------------------------- */

void AutoExposure::computeHist(const dev::CamFrame<dev::Pix8> * frame)
{
   for (int i=0; i < 256; ++i)
      hist_[i] = 0;
    
    for (int r = 0; r < frame->h; ++r)
      {
        const dev::Pix8 * p = frame->row(r);
        const dev::Pix8 * pE = p + frame->w;
        
        for (; p != pE; ++p)
          hist_[*p]++;
      }
    
    cnt_ = frame->w * frame->h;
}  

/* -------------------------------------------------------------------------- */

bool AutoExposure::process(const dev::CamFrame<dev::Pix8> * frame)
{
  if (!cfg_.enabled || frame == nullptr)
    return false;
  
  computeHist(frame);
  
  // compute triggers
  
  uint32_t sum_up = 0;
  for(dev::Pix8 v = 0; v < cfg_.up_value; ++v)
    sum_up += hist_[v];
    
  uint32_t sum_down = 0;
  for(dev::Pix8 v = 0; v < cfg_.down_value; ++v)
    sum_down += hist_[v];
    
  uint32_t sum_down_h = 0;
  for(dev::Pix8 v = 0; v < cfg_.down_value/2; ++v)
    sum_down_h += hist_[v];
  
  // Q(up) < value
  bool trig_down_h = float(sum_down_h)/cnt_*1000 <= cfg_.down_quant;
  bool trig_up = float(sum_up)/cnt_*1000 > cfg_.up_quant && !trig_down_h;
  // Q(down) >= value
  bool trig_down = float(sum_down)/cnt_*1000 <= cfg_.down_quant;
  bool go_up = false, go_down = false;
 
  // no change or change both -> do nothing
  if (trig_up == trig_down)
    {
      rep_up_ = rep_down_ = 0;
      return false;
    }
    
  if (trig_up)
    {
      rep_down_ = 0;
      rep_up_++;
      if (rep_up_ > cfg_.repetition)
        {
          go_up = true;
          rep_up_ = 0;
        }
    }
  else
    {
      rep_up_ = 0;
      rep_down_++;
      if (rep_down_ > cfg_.repetition)
        {
          go_down = true;
          rep_down_ = 0;
        }
    }
  
  int next_exp = curr_exp_;
  if (go_down)
  {
    next_exp = std::max<int>(0, curr_exp_-1);
    LOG_INFO << "ExpDOWN: " << next_exp << " = " << cfg_.shutters[next_exp] << " % " << cfg_.isos[next_exp];
  }
  
  if (go_up)
  {
    next_exp = std::min<int>(cfg_.shutters.size()-1, curr_exp_+1);
    LOG_INFO << "ExpUP: " << next_exp << " = " << cfg_.shutters[next_exp] << " % " << cfg_.isos[next_exp];
  }
  
  if (engine_)
    {
      if (cfg_.shutters[curr_exp_] != cfg_.shutters[next_exp])
        engine_->setShutterSpeed(cfg_.shutters[next_exp]);
      
      if (cfg_.isos[curr_exp_] != cfg_.isos[next_exp])
        engine_->setShutterSpeed(cfg_.isos[next_exp]);
    }  
  curr_exp_ = next_exp;
  
  return true;
}

/* -------------------------------------------------------------------------- */

void AutoExposure::setEngine(Engine * engine)
{
  engine_ = engine;
}

/* -------------------------------------------------------------------------- */

void AutoExposure::setProfile(const ProfileType & profile)
{
  if(profile.find("enabled") != profile.end())
      cfg_.enabled = profile["enabled"];

  if(profile.find("repetition") != profile.end())
      cfg_.repetition = profile["repetition"];
  if(profile.find("max_pts") != profile.end())
      cfg_.repetition = profile["max_pts"];

  if(profile.find("up_value") != profile.end())
      cfg_.up_value = profile["up_value"];
  if(profile.find("up_quantile") != profile.end())
      cfg_.up_quant = profile["up_quantile"];

  if(profile.find("down_value") != profile.end())
      cfg_.down_value = profile["down_value"];
  if(profile.find("down_quantile") != profile.end())
      cfg_.down_quant = profile["down_quantile"];
}

/* -------------------------------------------------------------------------- */

void AutoExposure::getProfile(ProfileType & profile) const
{
  profile["enabled"] = cfg_.enabled;
  profile["repetition"] = cfg_.repetition;
  profile["max_pts"] = cfg_.max_pts;

  profile["up_value"] = cfg_.up_value;
  profile["up_quantile"] = cfg_.up_quant;
  profile["down_value"] = cfg_.down_value;
  profile["down_quantile"] = cfg_.down_quant;
  
  profile["curr_exp_idx"] = curr_exp_;
  
  profile["shutters"] = cfg_.shutters;
  profile["isos"] = cfg_.isos;
}

/* -------------------------------------------------------------------------- */

void AutoExposure::validateProfile(const ProfileType & profile) const
{}

/* -------------------------------------------------------------------------- */
