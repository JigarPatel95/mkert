/*
 * RasPiEngine - specific engine for Raspberry PI
 *    controls all devices (camera, lasers) and sync them
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef _RASPIENG_H_
#define _RASPIENG_H_

/* -------------------------------------------------------------------------- */

#include <time.h>
#include <vector>
#include <set>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cstring>
#include <memory>
#include <unistd.h>
#include <thread>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <chrono>

#include <boost/circular_buffer.hpp>


#include "engine.h"
#include "dev/raspicam.h"
#include "dev/rpsynclib/rpsynclib.h"
#include "detector.h"
#include "laserbase.h"

/* -------------------------------------------------------------------------- */

namespace mke {
namespace rt {
  
/* -------------------------------------------------------------------------- */

// Improved camera frame - takes buffer info with it

class BufferedCamFrame : public dev::CamFrame<dev::Pix8>
{
protected:                  
  dev::RasPiCam         * camera_;        // pointer to parent camera
  MMAL_PORT_T           * port_;          // pointer to parent port
  MMAL_BUFFER_HEADER_T  * header_;        // buffer header

  friend class RasPiEngine;               // engine could call my methods
                  
public:
  // constructor - buffer locked
  BufferedCamFrame(dev::RasPiCam * camera, MMAL_PORT_T * port, 
                   MMAL_BUFFER_HEADER_T * header, 
                   unsigned char * mem, int w, int h, int stride,
                   uint64_t timer, uint32_t frame_id, uint16_t phase)
  : dev::CamFrame<dev::Pix8>(mem, w, h, stride, timer, frame_id, phase),
  camera_(camera), port_(port), header_(header)
  {};
};

/* -------------------------------------------------------------------------- */

// camera with queue
    
class RasPiEngine : public Engine, protected dev::RasPiCam
{
protected:
  MkertConfig::CameraConfig
                        cam_cfg_;               // shortcut to camera cfg
  MkertConfig::EngineConfig
                        eng_cfg_;               // shortcut to laser cfg

  Detector           &  detector_;              // used detector
                     
  uint64_t              frames_received_;       // number of already received frames
  uint32_t              frames_droped_;         // number of droped frames
  uint64_t              dropped_stamp_;         // last dropped log timestamp

  std::unique_ptr<LaserBase> 
                        laser_;                 // laser object
  
  uint                  max_frames_;            // maximum stored frames in queue
  std::queue<BufferedCamFrame, boost::circular_buffer<BufferedCamFrame> >
                        frames_;                // queue of unprocessed images

  mke::api::MkEStateType 
                        last_state_;            // last setted state
                        
  bool                  initialized_;           // camera is initialized
  mutable std::mutex    mtx_frames_;            // mutex for frames queue
  std::condition_variable
                        cv_frames_;             // conditional variable for new frame
  std::mutex            mtx_control_;           // mutex for control frames
  std::condition_variable
                        cv_control_;            // conditional variable for control frames
  uint32_t              control_cnt_;           // how many control packet has been received
  uint32_t              init_cnt_;              // number of received frame during initialization
  uint32_t              shutter_speed_;
  uint32_t              gain_analog_;
  uint32_t              gain_digital_; 
  bool                  auto_fps_on_;           // is actived auto fps ?
  
  std::chrono::high_resolution_clock::time_point
                        stopwatch_timer_;       // how long takes get cnt images
  int                   stopwatch_cnt_;         // how many images already received

  // ignore frames
  void ignoreFrames(bool ignore = true);
  
  // wait for stable values of exposure params
  bool waitForStableExposureMs(uint max_timeout, uint stable_timeout);
  
  // wait for stable values of exposure params
  bool waitForStableExposureIter(uint max_iter, uint stable_iter);
  
  // wait for stable values of exposure params
  bool waitForGain(uint16_t analog, uint16_t digital, int max_frames, int max_diff);
  
  // wait for next control frame
  bool waitForControl(int timeout = 100);
  
  // check parameters of exposure
  bool checkExposure(int shutter = -1, int analog = -1, int digital = -1, int diff = 0);
  
  // finalization of devices
  void finalize();
  
  // set ISO with predefined shutter
  bool setISO(uint32_t value, uint32_t shutter);  
  
public:
  // constructor
  RasPiEngine(const MkertConfig & cfg, Detector & detector);
  
  virtual ~RasPiEngine() 
    {
      finalize();
    };
  
  // initialization of devices
  virtual void initialize();

  // get next frame from camera - !!! working just in one thread 
  virtual const dev::CamFrame<dev::Pix8> * getFrame(uint32_t timout = 0xffffffff);
  
  // release frame - !!! working just in one thread
  virtual void releaseFrame(const dev::CamFrame<dev::Pix8> * frame);
  
  // state has been changed
  virtual void stateChanged(mke::api::MkEStateType state);
  
  // fire some pattern
  virtual void fireLaserPattern(int pattern, float strobe_len, float strobe_offset);
  
  // set shutter speed
  virtual void setShutterSpeed(uint32_t exposure_us);  
  
  // get shutter speed
  virtual uint32_t getShutterSpeed();

  // get analog gain
  virtual uint32_t getAnalogGain();
  
  // get digital gain
  virtual uint32_t getDigitalGain();
  
  // set ISO - combination of analog and digital values
  virtual bool setISO(uint32_t value);
  
  virtual void setProfile(const ProfileType & profile);

  virtual void getProfile(ProfileType & profile) const;
  
  virtual void validateProfile(const ProfileType & profile) const;  
  
  virtual void getStats(StatsType & stats) const;
  
  virtual void resetStats();  
  
  
protected:  
  // initialize camera
  void initCamera(int fps, uint32_t shutter);
  
  // finalize camera
  void finalCamera();
  
  // initialize lasers
  void initLasers();
  
  // finalize lasers
  void finalLasers();
  
  // fire lasers and alternate them
  inline uint16_t fireSyncLasers(uint32_t frame_id) { return laser_->fireSync(frame_id); };
  
  // prepare video frame and encapsulate it
  virtual void videoFrameReceived(MMAL_PORT_T *port, 
                                  MMAL_BUFFER_HEADER_T * buff);
  
  virtual void controlFrameReceived(MMAL_PORT_T * port, uint32_t shutter_speed,
                                    uint32_t gain_analog, uint32_t gain_digital);
};

/* -------------------------------------------------------------------------- */

} // end of mke::rt namespace
} // end of mke namespace

/* -------------------------------------------------------------------------- */

#endif // _RASPIENG_H_
