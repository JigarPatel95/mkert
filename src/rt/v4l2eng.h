/*
 * V4L2Engine - engine which using V4L2 camera
 * 
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef _V4L2ENG_H_
#define _V4L2ENG_H_

/* -------------------------------------------------------------------------- */

#include <time.h>
#include <vector>
#include <set>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cstring>
#include <memory>
#include <unistd.h>
#include <thread>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <chrono>

#include <boost/circular_buffer.hpp>


#include "engine.h"
#include "dev/v4l2cam.h"
#include "dev/rpsynclib/rpsynclib.h"
#include "detector.h"
#include "laserbase.h"

/* -------------------------------------------------------------------------- */

namespace mke {
namespace rt {

/* -------------------------------------------------------------------------- */

// camera with queue
    
class V4L2Engine : public Engine, protected dev::V4L2Cam
{
protected:
  MkertConfig::CameraConfig
                        cam_cfg_;               // shortcut to camera cfg
  MkertConfig::EngineConfig
                        eng_cfg_;               // shortcut to laser cfg

  Detector           &  detector_;              // used detector
                     
  uint64_t              frames_received_;       // number of already received frames
  uint32_t              frames_droped_;         // number of droped frames
  uint64_t              dropped_stamp_;         // last dropped log timestamp

  std::unique_ptr<LaserBase> 
                        laser_;                 // laser object
  
  uint                  max_frames_;            // maximum stored frames in queue
  std::queue<dev::CamFrame<dev::Pix8>, boost::circular_buffer<dev::CamFrame<dev::Pix8>> >
                        frames_;                // queue of unprocessed images

  mke::api::MkEStateType 
                        last_state_;            // last setted state
                        
  bool                  initialized_;           // camera is initialized
  mutable std::mutex    mtx_frames_;            // mutex for frames queue
  std::condition_variable
                        cv_frames_;             // conditional variable for new frame
  
  std::chrono::high_resolution_clock::time_point
                        stopwatch_timer_;       // how long takes get cnt images
  int                   stopwatch_cnt_;         // how many images already received

  // finalization of devices
  void finalize();
  
public:
  // constructor
  V4L2Engine(const MkertConfig & cfg, Detector & detector);
  
  virtual ~V4L2Engine() 
    {
      finalize();
    };
  
  // initialization of devices
  virtual void initialize();

  // get next frame from camera - !!! working just in one thread 
  virtual const dev::CamFrame<dev::Pix8> * getFrame(uint32_t timout = 0xffffffff);
  
  // release frame - !!! working just in one thread
  virtual void releaseFrame(const dev::CamFrame<dev::Pix8> * frame);
  
  // state has been changed
  virtual void stateChanged(mke::api::MkEStateType state);
  
  // fire some pattern
  virtual void fireLaserPattern(int pattern, float strobe_len, float strobe_offset);
  
  // set shutter speed
  virtual void setShutterSpeed(uint32_t exposure_us);  
  
  // get shutter speed
  virtual uint32_t getShutterSpeed();

  // get analog gain
  virtual uint32_t getAnalogGain();
  
  // get digital gain
  virtual uint32_t getDigitalGain();
  
  // set ISO - combination of analog and digital values
  virtual bool setISO(uint32_t value);
  
  virtual void setProfile(const ProfileType & profile);

  virtual void getProfile(ProfileType & profile) const;
  
  virtual void validateProfile(const ProfileType & profile) const;  
  
  virtual void getStats(StatsType & stats) const;
  
  virtual void resetStats();  
  
  
protected:  
  // initialize camera
  void initCamera();
  
  // finalize camera
  void finalCamera();
  
  // initialize lasers
  void initLasers();
  
  // finalize lasers
  void finalLasers();
  
  // fire lasers and alternate them
  inline uint16_t fireSyncLasers(uint32_t frame_id) { return laser_->fireSync(frame_id); };
  
  // prepare video frame and encapsulate it
  virtual void videoFrameReceived(const dev::Pix8 * im, uint32_t w, uint32_t h, uint32_t stride);
};

/* -------------------------------------------------------------------------- */

} // end of mke::rt namespace
} // end of mke namespace

/* -------------------------------------------------------------------------- */

#endif // _V4L2ENG_H_

