/*
 * V4L2Engine - engine which using V4L2 camera
 * 
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */


/* -------------------------------------------------------------------------- */

// includes

#include "v4l2eng.h"
#include "dev/board.h"

#include <nlohmann/json.hpp>

/* -------------------------------------------------------------------------- */

using namespace mke;
using namespace mke::rt;
using namespace mke::dev;

using nlohmann::json;

/* -------------------------------------------------------------------------- */

// V4L2Engine implementation

/* -------------------------------------------------------------------------- */

V4L2Engine::V4L2Engine(const MkertConfig & cfg, Detector & detector): 
  Engine(cfg),
  V4L2Cam(cfg.getCameraConfig().camera_id, cfg.getCameraConfig().num_buffers), 
  cam_cfg_(cfg.getCameraConfig()), 
  eng_cfg_(cfg.getEngineConfig()),
  detector_(detector),
  frames_received_(0), 
  frames_droped_(0),
  dropped_stamp_(0),
  max_frames_(cfg.getCameraConfig().num_buffers > 2 ? 
                cfg.getCameraConfig().num_buffers-2 : 2),
  frames_(boost::circular_buffer<dev::CamFrame<dev::Pix8>>(max_frames_)),
  last_state_(mke::api::MKE_STATE_IDLE),
  initialized_(false)
{};

/* -------------------------------------------------------------------------- */

void V4L2Engine::initialize()

{
  // initialize devices - camera and lasers
  
  initLasers();
  
  initCamera();
}

/* -------------------------------------------------------------------------- */

void V4L2Engine::finalize()

{  
  finalCamera();
 
  finalLasers();
}

/* -------------------------------------------------------------------------- */

void V4L2Engine::getProfile(ProfileType & json_profile) const
{
  json_profile["strobe_length"] = eng_cfg_.strobe_length;
  json_profile["strobe_offset"] = eng_cfg_.strobe_offset;
  json_profile["phase_shift"] = eng_cfg_.phase_shift;
  json_profile["use_phase_estimator"] = eng_cfg_.use_phase_estimator;
  json_profile["shutter_speed"] = V4L2Cam::getShutterSpeed();
  json_profile["iso"] = V4L2Cam::getGain();
  json_profile["fps"] = cam_cfg_.fps;
  
  uint32_t w,h,stride,pixtype;
  V4L2Cam::getVideoFormat(w,h,stride,pixtype);
  const char * s = (const char *) &pixtype;
  char spixtype[] = {s[0],s[1],s[2],s[3],'\0'};
  json_profile["width"] = w;
  json_profile["height"] = h;
  json_profile["pixtype"] = spixtype;
  json_profile["stride"] = stride;
}

/* -------------------------------------------------------------------------- */

void V4L2Engine::validateProfile(const ProfileType & profile) const
{
  // check some parameters ??
}

/* -------------------------------------------------------------------------- */

void V4L2Engine::setProfile(const ProfileType & json_profile)
{
  // camera properties

  if(json_profile.find("iso") != json_profile.end())
    {
      setGain(json_profile["iso"]);
    }
  if(json_profile.find("shutter_speed") != json_profile.end())
    {
      setShutterSpeed(json_profile["shutter_speed"]);
    }

  // parse engine parameters

  bool restart_lasers = false;
  if(json_profile.find("strobe_length") != json_profile.end())
    {
      eng_cfg_.strobe_length = json_profile["strobe_length"];
      restart_lasers = true;
    }
  if(json_profile.find("strobe_offset") != json_profile.end())
    {
      eng_cfg_.strobe_offset = json_profile["strobe_offset"];
      restart_lasers = true;
    }
  if(json_profile.find("phase_shift") != json_profile.end())
    eng_cfg_.phase_shift = json_profile["phase_shift"];
  if(json_profile.find("use_phase_estimator") != json_profile.end())
    eng_cfg_.use_phase_estimator = json_profile["use_phase_estimator"];

  // update engine
  
  if(restart_lasers && last_state_ == mke::api::MKE_STATE_DEPTH_SENSOR)
    {
      // restart laser engine online
      
      laser_->stopSync();
      laser_->startSync(detector_);
    }
}

/* -------------------------------------------------------------------------- */

void V4L2Engine::getStats(StatsType & stats) const 

{
  std::lock_guard<std::mutex> lock(mtx_frames_);

  stats["dropped_frames"] = frames_droped_;
  stats["im_width"] = image_width_;
  stats["im_height"] = image_height_;
}

/* -------------------------------------------------------------------------- */

void V4L2Engine::resetStats()

{
  std::lock_guard<std::mutex> lock(mtx_frames_);

  frames_droped_ = 0;
}

/* -------------------------------------------------------------------------- */

void V4L2Engine::initCamera()

{  
  // set parameters

  setShutterSpeed(cam_cfg_.shutter_speed);
  setGain(cam_cfg_.iso);
}

/* -------------------------------------------------------------------------- */

void V4L2Engine::finalCamera()

{
  V4L2Cam::finalize();
  
  // nothing now
}

/* -------------------------------------------------------------------------- */

void V4L2Engine::initLasers()

{
  assert(laser_.get() == nullptr);
  
  std::string controller = eng_cfg_.strobe_type;
  
  // auto select ?
  
  if(controller == "AUTO")
    {
      controller = isCm3board() ? "HW" : "SW";

      LOG_INFO << "Automatic selected strobe_type to " << controller;
    }
  
  // create right one laser controller
  
  if(controller == "HW")
    laser_.reset(new HwSyncLaser(eng_cfg_));
  else if(controller == "SW")
    laser_.reset(new SwSyncLaser(eng_cfg_));
  else if(controller == "FAKE")
    laser_.reset(new FakeLaser());
  else
    throw std::runtime_error("Unknown laser controller (Engine.strobe_type)");
}

/* -------------------------------------------------------------------------- */

void V4L2Engine::finalLasers()

{
  assert(laser_.get() != nullptr);
  
  laser_.reset();
}

/* -------------------------------------------------------------------------- */

void V4L2Engine::stateChanged(mke::api::MkEStateType state)

{
  if(state == mke::api::MKE_STATE_IDLE)
    {
      if(last_state_ != mke::api::MKE_STATE_DEPTH_SENSOR)
        {
          V4L2Cam::setGain(cam_cfg_.iso);
          V4L2Cam::setShutterSpeed(cam_cfg_.shutter_speed);
        }

      laser_->stopSync();
      laser_->fire(0);
      stopCapture();
    }
  else
    {
      if(state == mke::api::MKE_STATE_SERVICE) {}
//        setVideoFormat(cam_cfg_.width, cam_cfg_.height, 0);
      else if(state == mke::api::MKE_STATE_DEPTH_SENSOR)
        laser_->startSync(detector_);
      
      startCapture();
      
      stopwatch_timer_  = std::chrono::high_resolution_clock::now();
      stopwatch_cnt_    = 0;
    }
    
  last_state_ = state;
}

/* -------------------------------------------------------------------------- */

void V4L2Engine::fireLaserPattern(int pattern, float strobe_len, float strobe_offset)

{
  if(strobe_len > 0)
    laser_->fire(pattern, strobe_len, strobe_offset);
  else
    laser_->fire(pattern);
}

/* -------------------------------------------------------------------------- */

void V4L2Engine::setShutterSpeed ( uint32_t exposure_us )
{
  V4L2Cam::setShutterSpeed((int32_t) exposure_us);
}

/* -------------------------------------------------------------------------- */

uint32_t V4L2Engine::getShutterSpeed()
{
//  std::lock_guard<std::mutex> lock(mtx_frames_);
  
  return (uint32_t) V4L2Cam::getShutterSpeed();
}

/* ------------------------------------------------------------------------- */

bool V4L2Engine::setISO(uint32_t value)
{
  V4L2Cam::setGain((uint32_t) value);
  return true;
}

/* -------------------------------------------------------------------------- */

uint32_t V4L2Engine::getAnalogGain()
{
//  std::lock_guard<std::mutex> lock(mtx_frames_);
  
  return (uint32_t) V4L2Cam::getGain();
}

/* -------------------------------------------------------------------------- */

uint32_t V4L2Engine::getDigitalGain()
{
//  std::lock_guard<std::mutex> lock(mtx_frames_);
  
  return 1000;
}

/* -------------------------------------------------------------------------- */

void V4L2Engine::videoFrameReceived(const dev::Pix8 * im, uint32_t w, uint32_t h, 
                                    uint32_t stride)

{
  uint64_t now;
  dev::CamFrame<Pix8>::setTimerToNow(now);
  
  // lock the queue
  
  std::unique_lock<std::mutex> lock(mtx_frames_);
  
  // 1st of all fire lasers for next frame
  
  uint16_t phase = fireSyncLasers(frames_received_+1);
  
  // make a stat
  
  stopwatch_cnt_++;
  if(stopwatch_cnt_ == cam_cfg_.fps*10)
    {
      auto end = std::chrono::high_resolution_clock::now();
      float fps = float(stopwatch_cnt_*1000) / 
        std::chrono::duration_cast<std::chrono::milliseconds>(end-stopwatch_timer_).count();
      
      LOG_INFO << "FPS: " << std::setprecision(2) << fps << "Hz";
      stopwatch_cnt_ = 0;
      stopwatch_timer_ = end;
    }
  
  frames_received_++;
  
  if(frames_.size() < max_frames_)
    {
      frames_.push(dev::CamFrame<dev::Pix8>((dev::Pix8*)im, w, h, stride, now, frames_received_-1, phase));
      
//          std::cout << "+ New frame no. " << frames_received_ 
//            << " added. Now there is " << frames_.size() << " frames in Q." << std::endl;
      
      lock.unlock();
      cv_frames_.notify_one();
      return;
    }
  else
    {
      frames_droped_++;
      if(dropped_stamp_ > now || (now-dropped_stamp_) > MKE_ENGINE_DROPLOG_TIMEOUT)
      {
        std::cout << "! Too much frames in Q. Already dropped " 
            << frames_droped_ << " frames." << std::endl;
        dropped_stamp_ = now;
      }
    }

  // return back buffer if not used
  
  returnBackBuffer(im);
}

/* -------------------------------------------------------------------------- */

const CamFrame<Pix8> * V4L2Engine::getFrame(uint32_t timeout)

{
  // lock the queue
  
  std::unique_lock<std::mutex> lock(mtx_frames_);
  
  // no new frame available
  
  while(!frames_.size())
    if(cv_frames_.wait_for(lock, std::chrono::milliseconds(timeout)) 
                                                    == std::cv_status::timeout)
      return NULL;

  // get next frame and lock it
  
  dev::CamFrame<dev::Pix8> & frame = frames_.front();

  // return it
  
  return &frame;
}

/* -------------------------------------------------------------------------- */

void V4L2Engine::releaseFrame(const CamFrame<Pix8> * frame)

{
  // lock the queue
  
  std::lock_guard<std::mutex> lock(mtx_frames_);
  
  // check if releasing last getted frame
  
  assert(frame == &frames_.front());

  dev::CamFrame<dev::Pix8> & curr = frames_.front();
  
  returnBackBuffer(curr.mem);
  
  // release frame from queue
  frames_.pop();
}

/* -------------------------------------------------------------------------- */
