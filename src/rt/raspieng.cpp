/*
 * RasPiEngine - specific engine for Raspberry PI
 *    controls all devices (camera, lasers) and sync them
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

// includes

#include "raspieng.h"
#include "dev/board.h"

#include <nlohmann/json.hpp>

/* -------------------------------------------------------------------------- */

#define RASPIENG_STABLE_MAX_ITER        500     // max frames to stabilize image
#define RASPIENG_STABLE_APROVE_ITER     10      // frame to approve stable values
#define RASPIENG_EXPOSURE_MAX_DIFF      110     //

/* -------------------------------------------------------------------------- */

using namespace mke;
using namespace mke::rt;
using namespace mke::dev;

using nlohmann::json;

/* -------------------------------------------------------------------------- */

// RasPiEngine implementation

/* -------------------------------------------------------------------------- */

RasPiEngine::RasPiEngine(const MkertConfig & cfg, Detector & detector): 
  Engine(cfg),
  RasPiCam(cfg.getCameraConfig().camera_id, cfg.getCameraConfig().sensor_mode,
           cfg.getCameraConfig().num_buffers, true), 
  cam_cfg_(cfg.getCameraConfig()), 
  eng_cfg_(cfg.getEngineConfig()),
  detector_(detector),
  frames_received_(0), 
  frames_droped_(0),
  dropped_stamp_(0),
  max_frames_(cfg.getCameraConfig().num_buffers > 2 ? 
                cfg.getCameraConfig().num_buffers-2 : 2),
  frames_(boost::circular_buffer<BufferedCamFrame>(max_frames_)),
  last_state_(mke::api::MKE_STATE_IDLE),
  initialized_(false),
  control_cnt_(0),
  init_cnt_(0),
  shutter_speed_(0),
  gain_analog_(0),
  gain_digital_(0)

{};

/* -------------------------------------------------------------------------- */

void RasPiEngine::initialize()

{
  // initialize devices - camera and lasers
  
  initLasers();
  
  initCamera(cam_cfg_.fps, cam_cfg_.shutter_speed);
}

/* -------------------------------------------------------------------------- */

void RasPiEngine::finalize()

{  
  finalCamera();
 
  finalLasers();
}

/* -------------------------------------------------------------------------- */

void RasPiEngine::getProfile(ProfileType & json_profile) const
{
  json_profile["strobe_length"] = eng_cfg_.strobe_length;
  json_profile["strobe_offset"] = eng_cfg_.strobe_offset;
  json_profile["phase_shift"] = eng_cfg_.phase_shift;
  json_profile["use_phase_estimator"] = eng_cfg_.use_phase_estimator;
  json_profile["shutter_speed"] = cam_cfg_.shutter_speed;
  json_profile["iso"] = cam_cfg_.iso;
  json_profile["fps"] = cam_cfg_.fps;
  
  json_profile["video_mode"] = cam_cfg_.sensor_mode;
  json_profile["width"] = cam_cfg_.width;
  json_profile["height"] = cam_cfg_.height;
}

/* -------------------------------------------------------------------------- */

void RasPiEngine::validateProfile(const ProfileType & profile) const
{
  // check some parameters ??
}

/* -------------------------------------------------------------------------- */

void RasPiEngine::setProfile(const ProfileType & json_profile)
{
  // parse camera parameters

  bool restart_camera = false;
  bool restart_format = false;
  bool restart_mode = false;
  if(json_profile.find("fps") != json_profile.end())
    {
      cam_cfg_.fps = json_profile["fps"];
      restart_format = restart_camera = true;
    }
  if(json_profile.find("iso") != json_profile.end())
    {
      cam_cfg_.iso = json_profile["iso"];
      restart_camera = true;
    }
  if(json_profile.find("shutter_speed") != json_profile.end())
    {
      cam_cfg_.shutter_speed = json_profile["shutter_speed"];
      restart_camera = true;
    }
  if(json_profile.find("width") != json_profile.end())
    {
      cam_cfg_.width = json_profile["width"];
      restart_format = restart_camera = true;
    }
  if(json_profile.find("height") != json_profile.end())
    {
      cam_cfg_.height = json_profile["height"];
      restart_format = restart_camera = true;
    }
  if(json_profile.find("video_mode") != json_profile.end())
    {
      cam_cfg_.sensor_mode = json_profile["video_mode"];
      restart_mode = restart_format = restart_camera = true;
    }

  // parse engine parameters

  bool restart_lasers = false;
  if(json_profile.find("strobe_length") != json_profile.end())
    {
      eng_cfg_.strobe_length = json_profile["strobe_length"];
      restart_lasers = true;
    }
  if(json_profile.find("strobe_offset") != json_profile.end())
    {
      eng_cfg_.strobe_offset = json_profile["strobe_offset"];
      restart_lasers = true;
    }
  if(json_profile.find("phase_shift") != json_profile.end())
    eng_cfg_.phase_shift = json_profile["phase_shift"];
  if(json_profile.find("use_phase_estimator") != json_profile.end())
    eng_cfg_.use_phase_estimator = json_profile["use_phase_estimator"];
  
  // update camera

  if(restart_camera)
    {
      
      if(last_state_ == mke::api::MKE_STATE_IDLE)
        {
          // reinitialize camera
          
          if(restart_mode)
          {
            shutdownCamera();
//            disableVideoPort();
            RasPiCam::initCamera(cam_cfg_.sensor_mode);
            RasPiCam::prepareVideoPort();
            //setVideoMode();
          }

          initCamera(cam_cfg_.fps, cam_cfg_.shutter_speed);
        }
      else if(last_state_ == mke::api::MKE_STATE_DEPTH_SENSOR)
        {
          
          // online change the setup
          
          if(restart_format)
            {
              stopCapture();
              if(restart_mode)
                setVideoMode(cam_cfg_.sensor_mode);
              setVideoFormat(cam_cfg_.width, cam_cfg_.height, cam_cfg_.fps);
              startCapture();
            }

          if(json_profile.find("iso") != json_profile.end())
            setISO(cam_cfg_.iso);

          if(json_profile.find("shutter_speed") != json_profile.end())
            setShutterSpeed(cam_cfg_.shutter_speed);
        }
    }
  
  // update engine
  
  if(restart_lasers && last_state_ == mke::api::MKE_STATE_DEPTH_SENSOR)
    {
      // restart laser engine online
      
      laser_->stopSync();
      laser_->startSync(detector_);
    }
  
}

/* -------------------------------------------------------------------------- */

void RasPiEngine::getStats(StatsType & stats) const 

{
  std::lock_guard<std::mutex> lock(mtx_frames_);

  stats["dropped_frames"] = frames_droped_;
  stats["im_width"] = cam_cfg_.width;
  stats["im_height"] = cam_cfg_.height;  
}

/* -------------------------------------------------------------------------- */

void RasPiEngine::resetStats()

{
  std::lock_guard<std::mutex> lock(mtx_frames_);

  frames_droped_ = 0;
}

/* -------------------------------------------------------------------------- */

void RasPiEngine::ignoreFrames(bool ignore)
{
  std::lock_guard<std::mutex> lock(mtx_control_);
  
  initialized_ = !ignore;
  
  init_cnt_ = 0;
}

/* -------------------------------------------------------------------------- */

bool RasPiEngine::waitForGain(uint16_t analog, uint16_t digital, int max_frames, 
                              int max_diff)
{
  for(int i = 0; i < max_frames; ++i)
    { 
      // lock the queue
      
      std::unique_lock<std::mutex> lock(mtx_control_);
      
      cv_control_.wait_for(lock, std::chrono::milliseconds(100));
      
      if(std::abs<int>(analog - gain_analog_) < max_diff && std::abs<int>(digital - gain_digital_) < max_diff)
        return true;
    }
  
  return false;
}

/* -------------------------------------------------------------------------- */

bool RasPiEngine::waitForControl(int timeout)

{
  // lock the queue
  
  std::unique_lock<std::mutex> lock(mtx_control_);
  
  return cv_control_.wait_for(lock, std::chrono::milliseconds(timeout)) 
                                                    != std::cv_status::timeout;
}

/* -------------------------------------------------------------------------- */

bool RasPiEngine::checkExposure(int shutter, int analog, int digital, int diff)

{
  std::lock_guard<std::mutex> lock(mtx_control_);
  
  // check shutter
  
  if(shutter > 0 && std::abs<int>(shutter_speed_ - shutter) > diff)
    {
      LOG_WARN << "Bad shutter: " << shutter << " !~ " << shutter_speed_;
      return false;
    }

  // if any parameter is in auto mode, any gain is not stable
  
  if(shutter == 0 || analog == 0 || cam_cfg_.exp_comp == 0)
    return true;
  
  // check gains
  
  if(analog > 0 && std::abs<int>(gain_analog_ - analog) > diff)
    {
      LOG_WARN << "Bad analog gain: " << analog << " !~ " << gain_analog_;
      return false;
    }
  if(digital >= 0 && std::abs<int>(gain_digital_ - digital) > diff)
    {
      LOG_WARN << "Bad digital gain: " << digital << " !~ " << gain_digital_;
      return false;
    }
  
  return true;
}

/* -------------------------------------------------------------------------- */

int isoToUpperGain(int iso)

{
  switch(iso)
    {
    case 100:
      return 2585;
    case 200:
      return 5226;
    case 400:
      return 7531;
    case 800:
      return 9847;
    }  
    
  return -1;
}

/* -------------------------------------------------------------------------- */

bool RasPiEngine::waitForStableExposureIter(uint max_timeout, uint stable_timeout)

{
  uint32_t last_cnt = 0; 
  uint32_t last_init_cnt = 0; 

  for(uint i = 0; i < max_timeout; ++i) 
    { 
      { 
        std::lock_guard<std::mutex> lock(mtx_control_); 

        last_cnt = control_cnt_; 
      } 

      std::unique_lock<std::mutex> lock(mtx_control_);
      
      cv_control_.wait_for(lock, std::chrono::milliseconds(500));

      if(last_cnt != control_cnt_) 
        last_init_cnt = init_cnt_; 
      else if(init_cnt_ - last_init_cnt > stable_timeout) 
        {
          LOG_INFO << "stable for " << (init_cnt_ - last_init_cnt) << " frames";
          return true;
        }
    }
  
  return false;
}

/* -------------------------------------------------------------------------- */

bool RasPiEngine::waitForStableExposureMs(uint max_timeout, uint stable_timeout)

{
  auto start = std::chrono::high_resolution_clock::now();

  // lock the queue
  
  std::unique_lock<std::mutex> lock(mtx_frames_);
  
  if(cv_control_.wait_for(lock, std::chrono::milliseconds(max_timeout))
                                                    == std::cv_status::timeout)
    {
      LOG_INFO << "no control frame received";
      return true;
    }
  
  // no new frame available
  
  while(true)
    {
      auto diff = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - start).count();
      if(diff > max_timeout)
        return false;
      
      if(cv_control_.wait_for(lock, std::chrono::milliseconds(stable_timeout))
                                                    == std::cv_status::timeout)
        {
          LOG_INFO << "stable for " << (diff) << " ms";
          return true;
        }
    }
  
  return false;
}

/* -------------------------------------------------------------------------- */

void RasPiEngine::initCamera(int fps, uint32_t shutter_speed)

{
  ignoreFrames(true);
  
  // set parameters
  
  setVideoFormat(cam_cfg_.width, cam_cfg_.height, fps);

  if(cam_cfg_.auto_exposure)
    {
      setExposureMode(EXPOSURE_AUTO);
      RasPiCam::setISO(cam_cfg_.iso);
      setShutterSpeed(shutter_speed);
      setExposureCompensation(cam_cfg_.exp_comp);

      // fixed exposure - let camera bump gain
    }
  if(!cam_cfg_.auto_exposure)
    {
      setWB(false, 256, 256);
      
      setExposureMode(EXPOSURE_AUTO);
      setShutterSpeed(10);
      RasPiCam::setISO(cam_cfg_.iso);

      startCapture();
      waitForStableExposureIter(RASPIENG_STABLE_MAX_ITER, RASPIENG_STABLE_APROVE_ITER);

      setExposureMode(EXPOSURE_OFF);
      setShutterSpeed(shutter_speed);
      
      waitForControl(1000);
      stopCapture();
      
      LOG_INFO << "Current exposure is: " << getShutterSpeed() 
              << "us, AG: " << getAnalogGain() 
              << ", DG: " << getDigitalGain() << ".";

      if(!checkExposure(shutter_speed, cam_cfg_.iso != 0 ? isoToUpperGain(cam_cfg_.iso) : 0, 
                        1000, RASPIENG_EXPOSURE_MAX_DIFF))
        throw std::runtime_error("Unable to reach optimal exposure.");
    }
  
  ignoreFrames(false);
}

/* -------------------------------------------------------------------------- */

void RasPiEngine::finalCamera()

{
  shutdownCamera();
  
  // nothing now
}

/* -------------------------------------------------------------------------- */

void RasPiEngine::initLasers()

{
  assert(laser_.get() == nullptr);
  
  std::string controller = eng_cfg_.strobe_type;
  
  // auto select ?
  
  if(controller == "AUTO")
    {
      controller = isCm3board() ? "HW" : "SW";

      LOG_INFO << "Automatic selected strobe_type to " << controller;
    }
  
  // create right one laser controller
  
  if(controller == "HW")
    laser_.reset(new HwSyncLaser(eng_cfg_));
  else if(controller == "SW")
    laser_.reset(new SwSyncLaser(eng_cfg_));
  else if(controller == "FAKE")
    laser_.reset(new FakeLaser());
  else
    throw std::runtime_error("Unknown laser controller (Engine.strobe_type)");
}

/* -------------------------------------------------------------------------- */

void RasPiEngine::finalLasers()

{
  assert(laser_.get() != nullptr);
  
  laser_.reset();
}

/* -------------------------------------------------------------------------- */

void RasPiEngine::stateChanged(mke::api::MkEStateType state)

{
  if(state == mke::api::MKE_STATE_IDLE)
    {
      if(last_state_ != mke::api::MKE_STATE_DEPTH_SENSOR)
        {
          setVideoFormat(cam_cfg_.width, cam_cfg_.height, cam_cfg_.fps);

          setISO(cam_cfg_.iso, cam_cfg_.shutter_speed);
        }

      laser_->stopSync();
      laser_->fire(0);
      stopCapture();
    }
  else
    {
      if(state == mke::api::MKE_STATE_SERVICE)
        setVideoFormat(cam_cfg_.width, cam_cfg_.height, 0);
      else if(state == mke::api::MKE_STATE_DEPTH_SENSOR)
        laser_->startSync(detector_);
      
      startCapture();
      
      stopwatch_timer_  = std::chrono::high_resolution_clock::now();
      stopwatch_cnt_    = 0;
    }
    
  last_state_ = state;
}

/* -------------------------------------------------------------------------- */

void RasPiEngine::fireLaserPattern(int pattern, float strobe_len, float strobe_offset)

{
  if(strobe_len > 0)
    laser_->fire(pattern, strobe_len, strobe_offset);
  else
    laser_->fire(pattern);
}

/* -------------------------------------------------------------------------- */

void RasPiEngine::setShutterSpeed ( uint32_t exposure_us )
{
  RasPiCam::setShutterSpeed(exposure_us);
}

/* -------------------------------------------------------------------------- */

uint32_t RasPiEngine::getShutterSpeed()
{
  std::lock_guard<std::mutex> lock(mtx_control_);
  
  return shutter_speed_;
}

/* ------------------------------------------------------------------------- */

bool RasPiEngine::setISO(uint32_t value)
{
  return setISO(value, getShutterSpeed());
}

/* ------------------------------------------------------------------------- */
  

bool RasPiEngine::setISO(uint32_t value, uint32_t shutter)

{
  int limit = isoToUpperGain(value);
  if(limit < 0)
    return false;

  ignoreFrames(true);
  stopCapture();
  RasPiCam::setISO(value);
  setShutterSpeed(9);
  setExposureCompensation(-24);
  setExposureMode(EXPOSURE_AUTO);
  startCapture();
  
  waitForStableExposureIter(RASPIENG_STABLE_MAX_ITER, 
                                        RASPIENG_STABLE_APROVE_ITER);

  bool fin = checkExposure(-1, limit, 1000, RASPIENG_EXPOSURE_MAX_DIFF);
    
  setExposureMode(EXPOSURE_OFF);
  RasPiCam::setShutterSpeed(shutter);
  waitForControl(1000);
  
  ignoreFrames(false);
  
  if(!fin)
    {
      LOG_WARN << "Unable stabilize gains";
      throw std::runtime_error("Cannot stabilize gain of camera.");
    }
    
  return true;
}

/* -------------------------------------------------------------------------- */

uint32_t RasPiEngine::getAnalogGain()
{
  std::lock_guard<std::mutex> lock(mtx_control_);
  
  return gain_analog_;
}

/* -------------------------------------------------------------------------- */

uint32_t RasPiEngine::getDigitalGain()
{
  std::lock_guard<std::mutex> lock(mtx_control_);
  
  return gain_digital_;
}

/* -------------------------------------------------------------------------- */

void RasPiEngine::videoFrameReceived(MMAL_PORT_T * port, MMAL_BUFFER_HEADER_T * buff)

{

  uint64_t now;
  mke::dev::CamFrame<Pix8>::setTimerToNow(now);
  
  // lock the queue
  
  std::unique_lock<std::mutex> lock(mtx_frames_);
  
  // store frame if is correct and we have a space for it
  int       len;
  void  *   data;
  
  getInfo(buff, data, len);
  
  if(!initialized_)
    {
      std::lock_guard<std::mutex> lock(mtx_control_);
      ++init_cnt_;
    }
  else if(len > 0)
    {
      // 1st of all fire lasers for next frame
      
      uint16_t phase = fireSyncLasers(frames_received_+1);
      
      // make a stat
      
      stopwatch_cnt_++;
      if(stopwatch_cnt_ == cam_cfg_.fps*10)
        {
          auto end = std::chrono::high_resolution_clock::now();
          float fps = float(stopwatch_cnt_*1000) / 
            std::chrono::duration_cast<std::chrono::milliseconds>(end-stopwatch_timer_).count();
          
          LOG_INFO << "FPS: " << std::setprecision(2) << fps << "Hz";
          stopwatch_cnt_ = 0;
          stopwatch_timer_ = end;
        }
      
      frames_received_++;
      
      if(frames_.size() < max_frames_)
        {
          frames_.push(BufferedCamFrame(this, port, buff, static_cast<Pix8 *>(data),
                          width_, height_, stride_, now, frames_received_-1, phase));
          
//          std::cout << "+ New frame no. " << frames_received_ 
//            << " added. Now there is " << frames_.size() << " frames in Q." << std::endl;
          
          lock.unlock();
          cv_frames_.notify_one();
          return;
        }
      else
        {
          frames_droped_++;
          if(dropped_stamp_ > now || (now-dropped_stamp_) > MKE_ENGINE_DROPLOG_TIMEOUT)
          {
            std::cout << "! Too much frames in Q. Already dropped " 
                << frames_droped_ << " frames." << std::endl;
            dropped_stamp_ = now;
          }
        }
    }

  // return back buffer if not used
  
  returnBackBuffer(port, buff);
}

/* -------------------------------------------------------------------------- */

void RasPiEngine::controlFrameReceived(MMAL_PORT_T * port, uint32_t shutter_speed,
                                       uint32_t gain_analog, uint32_t gain_digital)

{
  std::lock_guard<std::mutex> lock(mtx_control_);
  
  this->shutter_speed_ = shutter_speed;
  this->gain_analog_ = gain_analog;
  this->gain_digital_ = gain_digital;
  this->control_cnt_++;
  
  cv_control_.notify_all();
  
  LOG_INFO << "S: " << shutter_speed << ", A: " << gain_analog << ", D: " << gain_digital;
}

/* -------------------------------------------------------------------------- */

const CamFrame<Pix8> * RasPiEngine::getFrame(uint32_t timeout)

{
  // lock the queue
  
  std::unique_lock<std::mutex> lock(mtx_frames_);
  
  // no new frame available
  
  while(!frames_.size())
    if(cv_frames_.wait_for(lock, std::chrono::milliseconds(timeout)) 
                                                    == std::cv_status::timeout)
      return NULL;

  // get next frame and lock it
  
  BufferedCamFrame & frame = frames_.front();
  lockBuffer(frame.header_);

  // return it
  
  return &frame;
}

/* -------------------------------------------------------------------------- */

void RasPiEngine::releaseFrame(const CamFrame<Pix8> * frame)

{
  // lock the queue
  
  std::lock_guard<std::mutex> lock(mtx_frames_);
  
  // check if releasing last getted frame
  
  assert(frame == &frames_.front());

  BufferedCamFrame & curr = frames_.front();
  
  unlockBuffer(curr.header_);
  returnBackBuffer(curr.port_, curr.header_);
  
  // release frame from queue
  frames_.pop();
}

/* -------------------------------------------------------------------------- */
