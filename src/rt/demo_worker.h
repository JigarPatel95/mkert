#ifndef _DEMO_WORKER_H_
#define _DEMO_WORKER_H_

#include <functional>

#include "util/logging.h"
#include "mkert_config.h"

#include "mkeapi.h"

#include "rt/api/memory_pool.h"
#include "rt/api/resolver.h"

namespace mke {
namespace rt {

  
/**
 * @brief A template implementation of a MkE API worker
 * 
 * This class serves as a code template for a areal MkE API worker implementation.
 */
class DemoWorker {
private:
  const MkertConfig & config_;  
  api::MemoryPool & api_pool_;
  api::Resolver api_resolver_;
  
  api::Request *timer_request_ptr_;
  uint32_t timer_ticks_;
  
public:
  DemoWorker(const MkertConfig &config, api::MemoryPool &api_pool, api::Resolver &api_resolver) : 
    config_(config), 
    api_pool_(api_pool),
    api_resolver_(api_resolver),
    timer_request_ptr_(nullptr),
    timer_ticks_(0)
    {}
    

  void internalCallback(api::Request *request_ptr) {
    LOG_INFO << "Replying to MKE_REQUEST_INTERNAL";

    api::Request::InternalRequestType type = request_ptr->getInternalType();

    if (type == api::Request::INT_REQUEST_REPLY_SINK_INVALID)
      {
        // We are told that the sink referred to in this request is invalid
        // We need to check whether we have any outstanding request that area
        // connected with this sink and stop them.
        if (timer_request_ptr_ &&
           (timer_request_ptr_->getReplySink()->getID() == request_ptr->getReplySink()->getID()))
          {
            timer_ticks_ = 0;
            api_resolver_.release(timer_request_ptr_);
          }
      }
    
    // We do not reply to this request, but we *MUST* release it
    api_resolver_.release(request_ptr);
  }      
 
   /**
    * @brief Implements a callback for a test MkE API function Ping (`mke::api::MKE_REQUEST_PING`)
    * 
    * Note that every API callback should return as soon as possible
    * 
    * @param request p_request:...
    */
  void pingCallback(api::Request *request_ptr) {
    LOG_INFO << "Replying to MKE_REQUEST_TIMER, seqid = " << request_ptr->getId();
    
    api_resolver_.reply(mke::api::MKE_REPLY_OK, request_ptr);
  }  

  /**
   * @brief Implements a joint callback for a test MkE API function Timer 
   * (`mke::api::MKE_REQUEST_START_TIMER`, `mke::api::MKE_REQUEST_STOP_TIMER`)
   * 
   * Note that every API callback should return as soon as possible
   * 
   * @param request p_request:...
   */
  void timerCallback(api::Request *request_ptr) {
    if (request_ptr->getType() == mke::api::MKE_REQUEST_START_TIMER)
      {
        LOG_INFO << "Replying to MKE_REQUEST_START_TIMER, seqid = " << request_ptr->getId() <<
                    ", ticks = " << request_ptr->getParamsBuffer<mke::api::MkERequest_Timer>()->timer_ticks;
        
        if (timer_request_ptr_)
        {
          // Timer is already running and we can run only one timer at a time
          api_resolver_.reply(mke::api::MKE_REPLY_SERVER_BUSY, request_ptr);
          return;
        }
    
        timer_request_ptr_ = request_ptr;
        timer_ticks_ = request_ptr->getParamsBuffer<mke::api::MkERequest_Timer>()->timer_ticks;
    
        api_resolver_.reply(mke::api::MKE_REPLY_DATA_WILL_START, timer_request_ptr_, false);
      }
    else if (request_ptr->getType() == mke::api::MKE_REQUEST_STOP_TIMER)
      {
        LOG_INFO << "Replying to MKE_REQUEST_STOP_TIMER, seqid = " << request_ptr->getId();
        
        // Stop the timer        
        if (timer_request_ptr_)
          {
            timer_ticks_ = 0;   
            api_resolver_.release(timer_request_ptr_);
            api_resolver_.reply(mke::api::MKE_REPLY_OK, request_ptr);
          }
        else
          api_resolver_.reply(mke::api::MKE_REPLY_CLIENT_REQUEST_DOES_NOT_APPLY, request_ptr);          
      }
  }
  
  void timerUpdate(bool do_continue) {
    
    if (!do_continue) 
      {
        // Exit from worker has been requested, cleanup ASAP
        if (timer_request_ptr_)
          api_resolver_.release(timer_request_ptr_);
        return;
      }
    else if (timer_request_ptr_)
      {
        // There is a timer to be processed, do the `work`
        timer_ticks_--;
        struct timespec ts;
        clock_gettime(CLOCK_MONOTONIC, &ts);
        uint64_t timer = static_cast<uint64_t>(ts.tv_sec) * 1000ULL + static_cast<uint64_t>(ts.tv_nsec) / 1000000ULL;
        
        // OK, we are done, let's construct the reply        
        api::Reply *reply_ptr;
   
        if (!api_pool_.request(reply_ptr, api::Reply::REPLY_BUFFER_BASIC))
          {
            // Something went bad, we couldn't get `Request` object from `MemoryPool`.
            // We will reply with a stock reply and stop the timer            
            timer_ticks_ = 0;                 
            api_pool_.requestStock(reply_ptr, mke::api::MKE_REPLY_SERVER_INSUFFICIENT_RESOURCES);
            api_resolver_.reply(reply_ptr, timer_request_ptr_);  
          }
        else
          {
            // We have `Reply` object, let's fill it up
            mke::api::MkEReply_Timer *timer_params = reply_ptr->getParamsBuffer<mke::api::MkEReply_Timer>();            
            timer_params->timer_ticks = timer_ticks_;
            timer_params->timer = timer;
            
            if (timer_ticks_)
              {
                LOG_INFO << "Continuing to MKE_REQUEST_START_TIMER, seqid = " << timer_request_ptr_->getId() <<
                  ", timer_ticks = " << timer_ticks_ << ", timer = " << timer;
                reply_ptr->setup(timer_request_ptr_, mke::api::MKE_REPLY_DATA_WILL_CONTINUE);
                api_resolver_.reply(reply_ptr, timer_request_ptr_, false);  
              }
            else
              {
                LOG_INFO << "Finishing to MKE_REQUEST_START_TIMER, seqid = " << timer_request_ptr_->getId() << 
                  ", timer_ticks = " << timer_ticks_ << ", timer = " << timer;                
                reply_ptr->setup(timer_request_ptr_, mke::api::MKE_REPLY_OK);
                api_resolver_.reply(reply_ptr, timer_request_ptr_);  
              }                
          }
      }
  }
  
  void init(void) {
    LOG_INFO << "Worker initialization";
    
    // Camera, laser initialization goes here ...
    
    // MkE API callback initialization
    api_resolver_.registerCallback(mke::api::MKE_REQUEST_PING, std::bind(&DemoWorker::pingCallback, this, std::placeholders::_1));
    api_resolver_.registerCallback(mke::api::MKE_REQUEST_START_TIMER, std::bind(&DemoWorker::timerCallback, this, std::placeholders::_1));    
    api_resolver_.registerCallback(mke::api::MKE_REQUEST_STOP_TIMER, std::bind(&DemoWorker::timerCallback, this, std::placeholders::_1));    
    api_resolver_.registerCallback(mke::api::MKE_REQUEST_INTERNAL, std::bind(&DemoWorker::internalCallback, this, std::placeholders::_1));        
  }  

  bool process(void) {
    // This is the start of the worker process loop, presumably invoked by a camera callback

    // Resolver::process will consume all available request pending
    // in the API request queue and call the appropriate callbacks
    bool do_continue = api_resolver_.process();
    
    // Process any outstanding requests 
    timerUpdate(do_continue);
        
    return do_continue;
  }
  
  void start(void) {
    LOG_INFO << "Worker started";
    
    // Start should register `process()` as a camera callback
    // Here, we simulate the camera callback loop that exists
    // as soon as `process()` returns false.
    while (process())
      boost::this_thread::sleep(boost::posix_time::seconds(1));
  }
  
  };
  
} // namespace rt
} // namespace mke


#endif // _DEMO_WORKER_H_
