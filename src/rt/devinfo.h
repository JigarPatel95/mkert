/*
 * UPnPServer - serving UPnP messages - just discover
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#pragma once

/* -------------------------------------------------------------------------- */

#include <boost/algorithm/string.hpp>

#include "mkert_version.h"

/* -------------------------------------------------------------------------- */

namespace mke {
namespace rt {

/* -------------------------------------------------------------------------- */

struct DeviceInfo
{
  const int             device_id;
  const char    *       device_type;
  std::string           serial_no;
  
  std::string           xml_desc;
  
  DeviceInfo(const int id, const char * type, const char * sn = "000000000000") :
    device_id(id),
    device_type(type),
    serial_no(sn)
  {}
  
  void generateXml()
  {
    xml_desc = R"raw(<?xml version="1.0"?>
      <root xmlns="urn:schemas-upnp-org:device-1-0">
        <specVersion>
          <major>1</major>
          <minor>0</minor>
        </specVersion>
        <device>
          <deviceType>urn:schemas-upnp-org:device:Basic:1</deviceType>
          <friendlyName>UPnP Magik Eye Sensor</friendlyName>
          <manufacturer>Magik Eye Inc.</manufacturer>
          <manufacturerURL>http://www.magik-eye.com</manufacturerURL>
          <modelDescription>UPnP MagikEye Depth Sensor - Prototype</modelDescription>
          <modelName>{DEVICE_TYPE}</modelName>
          <modelNumber>)raw" MKERT_VERSION R"raw(</modelNumber>
          <modelURL>http://www.magik-eye.com</modelURL>
          <serialNumber>{SERIAL_NO}</serialNumber>
          <UDN>uuid:UPnP-MkE-{DEVICE_TYPE}-{SERIAL_NO}</UDN>
          <UPC>{SERIAL_NO}</UPC>
        </device>
      </root>
      )raw";
      
    boost::algorithm::replace_all(xml_desc, "{SERIAL_NO}", serial_no);
    boost::algorithm::replace_all(xml_desc, "{DEVICE_TYPE}", device_type);
  }
};

/* -------------------------------------------------------------------------- */

}       // end of mke::rt
}       // end of mke

/* -------------------------------------------------------------------------- */
