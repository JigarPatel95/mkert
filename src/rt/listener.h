/* 
 * Listener
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#ifndef _LISTENER_H_
#define _LISTENER_H_

namespace mke {
namespace rt {

class Listener {
public:
  virtual bool start(void) = 0;
  virtual void stop(void) = 0;
};

} // namespace rt
} // namespace mke

#endif // _LISTENER_H_
