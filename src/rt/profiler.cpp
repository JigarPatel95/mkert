/*
 * Profiler - manages the profiles
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar, Jan Heller
 *
 */

/* -------------------------------------------------------------------------- */

#include "profiler.h"

#include <cassert>
#include <iostream>


/* -------------------------------------------------------------------------- */

using namespace mke::rt;
using nlohmann::json;

/* -------------------------------------------------------------------------- */

void Profiler::init()
{
  if (cfg_.profile1 != "") 
   addProfile(cfg_.profile1.c_str()); 
     
  if (cfg_.profile2 != "") 
    addProfile(cfg_.profile2.c_str()); 
 
  if (cfg_.profile3 != "") 
    addProfile(cfg_.profile3.c_str()); 
  
  if (cfg_.profiles_path != "") 
    {
        std::ifstream ifs(cfg_.profiles_path, std::ifstream::in);
        json db = json::parse(ifs);
        
        for(auto & sub : db)
            addProfile(sub.dump().c_str());
    }
     
  if(cfg_.default_profile != "")
    setPolicy(cfg_.default_profile.c_str());
}

/* -------------------------------------------------------------------------- */

const char* Profiler::getPolicy() const
{
  return last_policy_.c_str();
}

/* -------------------------------------------------------------------------- */

#ifndef __ANDROID__
void Profiler::listPolicies(std::vector< std::__cxx11::string >& policies) const
#else
void Profiler::listPolicies(std::vector< std::string >& policies) const
#endif
{
  policies.clear();
  for(const auto & p : profiles_)
      policies.push_back(p.first);
}

/* -------------------------------------------------------------------------- */

void Profiler::setPolicy(const char* policy)
{
  std::map<std::string, std::string>::const_iterator node = profiles_.find(policy);
  if(node == profiles_.end())
    throw std::runtime_error("Unknown profile");
  
  // TODO: parse JSON and apply it to the profiler by the name
  
  setProfile(node->second.c_str());
  last_policy_ = policy;
}

/* -------------------------------------------------------------------------- */

void Profiler::validateProfile(const char* profile, ProfileType & json_profile) const
{  
  std::stringstream ss(profile);
  json_profile = json::parse(ss.str());
  
  if(!json_profile.is_object())
      throw std::runtime_error("Invalid profile");
  
  for(auto it = json_profile.begin(); it != json_profile.end(); ++it)
    {
      std::map<std::string, Profiled *>::const_iterator node = profiled_list_.find(it.key());
      if(node != profiled_list_.end())
          node->second->validateProfile(it.value());
    }
}

/* -------------------------------------------------------------------------- */

std::string Profiler::getProfile() const
{
  // TODO: assable whole profile togother - one by one
  
  ProfileType root;
  for(std::map<std::string, Profiled *>::const_iterator it = profiled_list_.begin(); it != profiled_list_.end(); ++it)
    {
      ProfileType sub;
      
      // FIXME: probably need to keep the memory of subtree
      
      it->second->getProfile(sub);
      
      root[it->first] = sub;
    }
    
  return root.dump().c_str();
}

/* -------------------------------------------------------------------------- */

std::string Profiler::addProfile(const char* profile)
{
  // 1st check the profile
  
  ProfileType tree;
  validateProfile(profile, tree);
  
  // propagate profile per-partes by the sections
    
  if(tree.find("name") != tree.end())
    {
      profiles_[tree["name"]] = profile;
      return tree["name"];
    }
  else 
    return "";
}

/* -------------------------------------------------------------------------- */

void Profiler::setProfile(const char* profile)
{
  // 1st check the profile
  
  ProfileType json_profile;
  validateProfile(profile, json_profile);
    
  // apply the profile to all objects
    
  for(auto it = json_profile.begin(); it != json_profile.end(); ++it)
    {
      std::map<std::string, Profiled *>::iterator node = profiled_list_.find(it.key());
      if(node != profiled_list_.end())
          node->second->setProfile(it.value());
    }  
}

/* -------------------------------------------------------------------------- */

void Profiler::registerProfiled(const char* name, Profiled* profiled)
{
  // assert(profiled_list_.find(name) == profiled_list_.end()); - possible to overide profile
  
  profiled_list_.insert( { name, profiled } );
}

/* -------------------------------------------------------------------------- */

void Profiler::unregisterProfiled(const char* name)
{
  assert(profiled_list_.find(name) != profiled_list_.end());
  
  profiled_list_.erase(name);
}

/* -------------------------------------------------------------------------- */
