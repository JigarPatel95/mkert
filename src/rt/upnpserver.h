/*
 * UPnPServer - serving UPnP messages - just discover
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef _UPNPSERVER_H_
#define _UPNPSERVER_H_

/* -------------------------------------------------------------------------- */

#ifdef MKERT_USE_UPNP
  #include <upnp/upnp.h>
#endif
#include <stdexcept>
#include <sstream>

#include "devinfo.h"

/* -------------------------------------------------------------------------- */

namespace mke {
namespace rt {

/* -------------------------------------------------------------------------- */

class UpnpServer

{
#ifdef MKERT_USE_UPNP
  UpnpDevice_Handle     device_handle_;   // device handle supplied by UPnP SDK.
#endif
  const DeviceInfo   &  device_info_;
  
  void checkError(int ret_status, const char * desc = nullptr);

public:  
  /**
   * @brief constructor
   */
  UpnpServer(const DeviceInfo & dev_info);

  /**
   * @brief destructor
   */
  virtual ~UpnpServer()
  {
#ifdef MKERT_USE_UPNP      
    UpnpUnRegisterRootDevice(device_handle_);
    UpnpFinish();
#endif
  }
};

/* -------------------------------------------------------------------------- */

}       // end of mke::rt
}       // end of mke

/* -------------------------------------------------------------------------- */

#endif // _UPNPSERVER_H_

/* -------------------------------------------------------------------------- */
