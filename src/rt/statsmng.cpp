/*
 * StatsMng - manages the computed stats
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#include "statsmng.h"

#include <cassert>
#include <iostream>
#include <nlohmann/json.hpp>

#include <boost/property_tree/ptree.hpp>

/* -------------------------------------------------------------------------- */

using namespace mke::rt;
using nlohmann::json;

/* -------------------------------------------------------------------------- */

std::string StatsManager::getStats() const
{
  // TODO: asseble whole stats together - one by one
  
  StatsType root;
  
  // Wall clock timers
  root["init_wall_clock_timer_s"] = wc_init_timer_.elapsed();
  root["reset_wall_clock_timer_s"] = wc_reset_timer_.elapsed();
  
#ifndef __MINGW32__
  // Process/system times
  struct tms tms_now;
  double clktck = 0;
  
  clktck = double(sysconf(_SC_CLK_TCK));
  times(&tms_now);
  
  root["init_user_timer_s"] = double(tms_now.tms_utime - tms_from_init_.tms_utime) / clktck;
  root["reset_user_timer_s"] = double(tms_now.tms_utime - tms_from_reset_.tms_utime) / clktck;

  root["init_system_timer_s"] = double(tms_now.tms_stime - tms_from_init_.tms_stime) / clktck;
  root["reset_system_timer_s"] = double(tms_now.tms_stime - tms_from_reset_.tms_stime) / clktck;
  
#endif
  // Get statistics from the registered object

  for(std::map<std::string, StatsGenerating *>::const_iterator it = registered_list_.begin(); 
      it != registered_list_.end(); ++it)
    {
      StatsType subtree;
      
      it->second->getStats(subtree);      
      root[it->first] = subtree;
    }
    
  return root.dump();
}

/* -------------------------------------------------------------------------- */

void StatsManager::resetStats()
{
  StatsType root;
  
  wc_reset_timer_.reset();
#ifndef __MINGW32__
  times(&tms_from_reset_);
#endif
  for(std::map<std::string, StatsGenerating *>::const_iterator it = registered_list_.begin(); 
      it != registered_list_.end(); ++it)
    {
      it->second->resetStats();
    }
}

/* -------------------------------------------------------------------------- */

void StatsManager::registerObject(const char* name, StatsGenerating * object)
{
  // assert(profiled_list_.find(name) == profiled_list_.end()); - possible to overide profile
  
  registered_list_.insert( { name, object } );
}

/* -------------------------------------------------------------------------- */

void StatsManager::unregisterObject(const char* name)
{
  // assert(profiled_list_.find(name) != profiled_list_.end());
  
  registered_list_.erase(name);
}

/* -------------------------------------------------------------------------- */
