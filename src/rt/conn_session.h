/* 
 * ConnSession - Represents a single connection from a client.
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 * author: Ondrej Fisar, fisar@magik-eye.com
 *
 */

/* -------------------------------------------------------------------------- */

#pragma once

/* -------------------------------------------------------------------------- */

#include <sstream>
#include <boost/asio.hpp>

/* -------------------------------------------------------------------------- */

#include "rt/api/reply_sink.h"
#include "rt/api/request_sink.h"
#include "rt/api/request.h"
#include "rt/api/request_parser.h"
#include "rt/api/queue.h"

#include <queue>
#include <mutex>

/* -------------------------------------------------------------------------- */

namespace mke {
namespace rt {

/* -------------------------------------------------------------------------- */

/// Represents a single connection from a client.

template <class AsioType>
class ConnSession : public api::ReplySink, 
                    public std::enable_shared_from_this<ConnSession<AsioType> >
{
protected:
  AsioType                      bus_;
  api::MemoryPool       &       api_pool_;
  api::RequestSink      &       request_sink_;
  std::string                   conn_id_;

  api::Request                  request_;
  api::RequestParser            request_parser_;

  typedef std::array<uint8_t, sizeof(mke::api::MkERequest)> buffer_type;
  buffer_type                   buffer_;
  buffer_type::iterator         buffer_start_;
  buffer_type::iterator         buffer_end_;
  
  std::queue<api::Reply *>      reply_queue_;
  std::mutex                    reply_mtx_;

public:
  // consruct
  ConnSession(AsioType bus, api::MemoryPool &api_pool, 
              api::RequestSink &request_sink, const std::string & conn_id) : 
    bus_(std::move(bus)),
    api_pool_(api_pool),
    request_sink_(request_sink),
    conn_id_(conn_id)
  {
    LOG_INFO << "ctor " << conn_id_;
  }

  // destructor
  virtual ~ConnSession() 
  {
    LOG_INFO << "dtor " << conn_id_;
    request_.release();
  }
  
  // start connection
  virtual void start(void) 
  {
    request_.init(this->shared_from_this());
    buffer_start_ = buffer_.data();
    buffer_end_ = buffer_.data();
    readRequestStart();    
  }
  
  // close connection
  virtual void closeAndEmit(void) 
  {
    boost::system::error_code ec;

    request_.release();       
    bus_.close();
            
    // signal worker that this session is broken, so that the worker can 
    // stop any and all work pertinent to this ReplySink
    api::Request *request_ptr;
    if (api_pool_.request(request_ptr))
      {
        request_ptr->init(this->shared_from_this());
        request_ptr->setInternalType(api::Request::INT_REQUEST_REPLY_SINK_INVALID);
        request_sink_.request(request_ptr);
      }    
  }

  // send a reply
  virtual void reply(api::Reply *reply) 
  {
    std::lock_guard<std::mutex> lock(reply_mtx_);
    
    
    if (!reply_queue_.size())
      {
        reply_queue_.push(reply);
        writeLoop();
      }
    else
      {
        reply_queue_.push(reply);
      }
  }
  
  // get id of connection
  virtual uint64_t getID(void) {
    return reinterpret_cast<uint64_t>(this);
  }

protected:
  // write reply
  virtual void writeLoop()
  {
    auto self(this->shared_from_this());
    
    api::Reply * reply = self->reply_queue_.front();
    boost::asio::async_write(bus_, boost::asio::buffer(reply->getBuffer(), reply->getSize()),
        [reply, self](boost::system::error_code ec, std::size_t ) mutable
        {
          self->api_pool_.release(reply);

          if (ec)
            self->closeAndEmit();
          else
            {
              std::lock_guard<std::mutex> lock(self->reply_mtx_);
              
              self->reply_queue_.pop();
              
              if (self->reply_queue_.size())
                self->writeLoop();
            }
        });
  }
  
private:
  
  // read basic request
  void readRequestStart() 
  {  
    if (!bus_.is_open())
      return;
    
    request_.resetBuffer();
    request_parser_.reset();
    
    parseRequest();
  }
  
  // parse basic request
  void parseRequest() 
  {
    api::RequestParser::Result result;
    
    std::tie(result, buffer_start_) = request_parser_.parse(request_, buffer_start_, buffer_end_);
    
    if (result == api::RequestParser::PARSER_ERROR) 
      {
       // LOG_INFO << "PARSER_ERROR";         
        reply(api_pool_.requestStock(mke::api::MKE_REPLY_CLIENT_MALFORMED_REQUEST));
        readRequestStart();
      }
    else if (result == api::RequestParser::PARSER_OK)
      {
       // LOG_INFO << "PARSER_OK " << request_.getType();
        api::Request *request_ptr;

        if (api_pool_.request(request_ptr))
        {
          // Copy the request buffer
          *request_ptr = request_;
          // Dispatch the request to the API queue
          request_sink_.request(request_ptr);
        }
        else
        {
          LOG_INFO << "API_QUEUE_FULL " << request_.getType();          
          reply(api_pool_.requestStock(mke::api::MKE_REPLY_SERVER_INSUFFICIENT_RESOURCES));
        }
        
        readRequestStart();
      }
    else if (result == api::RequestParser::PARSER_NEEDMOREDATA)
      {
        buffer_start_ = buffer_.data();
        buffer_end_ = buffer_.data();
        readRequestCont();
      }
  }    
  
  // read the rest of request
  void readRequestCont() 
  {    
    auto self(this->shared_from_this());
    bus_.async_read_some(boost::asio::buffer(buffer_start_, sizeof(mke::api::MkERequest)),
        [this, self](boost::system::error_code ec, std::size_t length)
        {
          if (!ec)
          {
            buffer_end_ += length;
            parseRequest();
          }
          else
          {
            closeAndEmit();
          }
        });
  }  
};  

/* -------------------------------------------------------------------------- */

} // namespace rt
} // namespace mke

/* -------------------------------------------------------------------------- */
