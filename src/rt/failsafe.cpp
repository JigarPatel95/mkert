/*
 * FailSafeMode - mode when fatal error occures
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

// includes

#include "failsafe.h"

/* -------------------------------------------------------------------------- */

using namespace mke;
using namespace mke::rt;

/* -------------------------------------------------------------------------- */

void FailSafeMode::start()
{
  if(probe_)
    probe_->applyCode("L+W(750)-W(250)");
  
  try
    {
      while(api_helper_.api_resolver.process())
        {
          std::this_thread::sleep_for(std::chrono::milliseconds(100));
        };
    }
  catch(...)
    {
      fatal_err_ = std::current_exception();
    }
}


/* -------------------------------------------------------------------------- */