/*
 * OnSemiEngine - interface for engine that is connection to devices
 * 
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#include "onsemieng.h"

/* -------------------------------------------------------------------------- */

using namespace mke::rt;
using namespace mke;

/* -------------------------------------------------------------------------- */

OnSemiEngine::~OnSemiEngine()

{
    fireLaserPattern(0, 0, 0);
    stopCapture();
}

/* -------------------------------------------------------------------------- */

void OnSemiEngine::initialize()
{
    // change the exposure settings
    
    OnSemiCam::setFps(cam_cfg_.fps);
    OnSemiEngine::setShutterSpeed(cam_cfg_.shutter_speed);
    OnSemiEngine::setISO(cam_cfg_.iso);
    
    OnSemiCam::setRegister("TEMPSENS_CTRL_REG", 0x0011);
}

/* -------------------------------------------------------------------------- */
  
void OnSemiEngine::stateChanged(mke::api::MkEStateType state)
{
  if(state == mke::api::MKE_STATE_IDLE)
    {
      if(last_state_ != mke::api::MKE_STATE_DEPTH_SENSOR)
        {
            // change the exposure settings
            
            OnSemiEngine::setShutterSpeed(cam_cfg_.shutter_speed);
            OnSemiEngine::setISO(cam_cfg_.iso);
        }

      // stop the lasers
      fireLaserPattern(0, 0, 0);
      stopCapture();
    }
  else
    {

      if(state == mke::api::MKE_STATE_DEPTH_SENSOR)
        {
            // start flickering with lasers
            startSync();
        }
      
      startCapture();
    }
    
  last_state_ = state;
}

/* -------------------------------------------------------------------------- */

void OnSemiEngine::startSync()
{
    uint16_t seq = 0x0000;
    unsigned int det_num = detector_.getNoDetectors();
    switch(det_num)
    {
      case 1:
          seq = 1;
      break;
      default:
        throw std::runtime_error("Unsupported number of detectors");
    }
    LOG_INFO << "sync " << seq << " at " << det_num;

    fireLaserPattern(seq, 0, 0);
}

/* -------------------------------------------------------------------------- */

void OnSemiEngine::videoFrameReceived(const dev::Pix8 * data, int w, int h, int memw, uint16_t phase)

{

  uint64_t now;
  mke::dev::CamFrame<dev::Pix8>::setTimerToNow(now);
  
  // lock the queue
  
  std::unique_lock<std::mutex> lock(mtx_frames_);
  
  unsigned int num_det = detector_.getNoDetectors();
  if(num_det == 0)
      num_det = 1;
  frames_received_++;
  
  fpser_.tick();

  if(frames_.size() < max_frames_)
    {
        frames_.push(dev::CamFrame<dev::Pix8>(const_cast<dev::Pix8 *>(data),
                                            w, h, memw, now, frames_received_-1, phase%num_det));
        
        lock.unlock();
        cv_frames_.notify_one();
        return;
    }
  else
    {
        frames_droped_++;
        if(dropped_stamp_ > now || (now-dropped_stamp_) > MKE_ENGINE_DROPLOG_TIMEOUT)
        {
          std::cout << "! Too much frames in Q. Already dropped " 
              << frames_droped_ << " frames." << std::endl;
          dropped_stamp_ = now;
        }
    }

  // return back buffer if not used
  
  returnBackBuffer(data);
}

/* -------------------------------------------------------------------------- */

const dev::CamFrame<dev::Pix8> * OnSemiEngine::getFrame(uint32_t timeout)

{
  // check for the exception in thread proc
    
  if (err_)
      std::rethrow_exception(err_);
    
  // lock the queue
  
  std::unique_lock<std::mutex> lock(mtx_frames_);
  
  // no new frame available
  
  while(!frames_.size())
    if(cv_frames_.wait_for(lock, std::chrono::milliseconds(timeout)) 
                                                    == std::cv_status::timeout)
      return NULL;

  // get next frame and lock it
  
  dev::CamFrame<dev::Pix8> & frame = frames_.front();
//  lockBuffer(frame.header_);

  // return it
  
  return &frame;
}

/* -------------------------------------------------------------------------- */

void OnSemiEngine::releaseFrame(const dev::CamFrame<dev::Pix8> * frame)

{
  // lock the queue
  
  std::lock_guard<std::mutex> lock(mtx_frames_);
  
  // check if releasing last getted frame
  
  assert(frame == &frames_.front());

  dev::CamFrame<dev::Pix8> & curr = frames_.front();
  
//  unlockBuffer(curr.header_);
  returnBackBuffer(frame->mem);
  
  // release frame from queue
  frames_.pop();
}

/* -------------------------------------------------------------------------- */

void OnSemiEngine::fireLaserPattern(int pattern, float strobe_len, float strobe_off) {    
//    char profile_name[64];
//    sprintf(profile_name, "MkERT_SetLaserPattern%d", pattern);
//    OnSemiCam::applyProfile(profile_name);
    if(pattern)
    {
        if (eng_cfg_.strobe_length > 0)
        {
            OnSemiCam::setRegister("GPIO_SELECT", 0xFC0C);
            if (cam_cfg_.fps > 0)
            {
                uint32_t flash_cnt = std::ceil((fll_-image_height_-OnSemiCam::getRegister("COARSE_INTEGRATION_TIME"))/ONSEMICAM_FLASHUNIT/256);
                OnSemiCam::setRegister("FLASH", 0x2688);
                OnSemiCam::setRegister("FLASH_COUNT", flash_cnt);
            }
            else
            {
                uint32_t flash_cnt = std::ceil((fll_-image_height_-OnSemiCam::getRegister("COARSE_INTEGRATION_TIME"))/ONSEMICAM_FLASHUNIT/256);
                OnSemiCam::setRegister("FLASH", 0xA608);
                OnSemiCam::setRegister("FLASH_COUNT", 0xFFFF);            
            }   
        }
        else
        {            
            OnSemiCam::setRegister("GPIO_SELECT", 0xFFFC);
        }
    }
    else
    {
        OnSemiCam::setRegister("GPIO_SELECT", 0xFE4C);
    }
    OnSemiCam::setRegister("RESET_REGISTER", 0x0014);
}

/* -------------------------------------------------------------------------- */

void OnSemiEngine::setShutterSpeed(uint32_t exposure) {
    if (exposure > 0)
        OnSemiCam::setShutterSpeed(exposure);
    else
        LOG_WARN << "Unsupported autoexposure for onsemi for now - will be ignored";
}

/* -------------------------------------------------------------------------- */

uint32_t OnSemiEngine::getShutterSpeed() { 
    return round(OnSemiCam::getShutterSpeed());
}

/* -------------------------------------------------------------------------- */

uint32_t OnSemiEngine::getAnalogGain() { 
    return OnSemiCam::getAnalogGain()*1000; 
}

/* -------------------------------------------------------------------------- */

uint32_t OnSemiEngine::getDigitalGain() { 
    return OnSemiCam::getDigitalGain()*1000; 
}

/* -------------------------------------------------------------------------- */

bool OnSemiEngine::setISO(uint32_t value) { 
    if(value > 0)
        OnSemiCam::setGain(value/100.0);
    else
        LOG_WARN << "Unsupported auto gain, will be ignored";
    return true;
}

/* -------------------------------------------------------------------------- */

void OnSemiEngine::getStats(StatsType & stats) const
{
  stats["im_width"] = this->getImageWidth();
  stats["im_height"] = this->getImageHeight();
  stats["cam_temp"] = this->getCamTemp();
  stats["cam_temp_raw"] = this->getCamTempRaw();
}

/* -------------------------------------------------------------------------- */

float OnSemiEngine::getCamTemp() const
{
  uint32_t regval = getRegister("TEMPSENS_DATA_REG");
  
  return regval*0.7 - 253.6;
}

/* -------------------------------------------------------------------------- */

uint32_t OnSemiEngine::getCamTempRaw() const
{
  return getRegister("TEMPSENS_DATA_REG");
}

/* -------------------------------------------------------------------------- */

