/*
 * FailSafeMode - mode when fatal error occures
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar, Jan Heller
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef _FAILSAFE_H_
#define _FAILSAFE_H_

/* -------------------------------------------------------------------------- */

#include "util/logging.h"
#include "mkert_config.h"

/* -------------------------------------------------------------------------- */

#include "workerbase.h"

#include "mkeapi.h"
#include "api/queue.h"
#include "api/resolver.h"
#include "detector.h"

#include "engine.h"
#include "loadeng.h"

#include "mod/wmodule.h"

#include "devinfo.h"

/* -------------------------------------------------------------------------- */

#ifdef USE_RPICAM
  #include "raspieng.h"
#endif

/* -------------------------------------------------------------------------- */

namespace mke {
namespace rt {

/* -------------------------------------------------------------------------- */

/**
 * @brief Worker = API request processor
 * 
 * This class serve clients' API request
 */

class FailSafeMode : public WorkerBase
{
public:
  /*
   * @brief Constructor of Worker that should serve clients requests
   * 
   * @param config          general configuration object
   * @param api_pool        API pool with requests
   * @param api_resolver    API resolver
   */
  FailSafeMode(const MkertConfig &config, api::MemoryPool &api_pool, 
         api::Resolver &api_resolver, const char * mkert_dir, const DeviceInfo & dev_info) : 
    WorkerBase(config, api_pool, api_resolver, mkert_dir, dev_info)
  {};

  /**
   * @brief Destructor
   */
  virtual ~FailSafeMode() {};
  
  /**
   * @brief Starts processing of requests
   */
  virtual void start();
};

/* -------------------------------------------------------------------------- */

} // namespace mke::rt
} // namespace mke

/* -------------------------------------------------------------------------- */

#endif // _FAILSAFE_H_
