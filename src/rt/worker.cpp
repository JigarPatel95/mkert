/*
 * Worker - serves most of API requests
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

// includes

#include "worker.h"
#include "devinfo.h"
#include "rt/api/apierror.h"

#include <fstream>
#include <boost/algorithm/string.hpp>

#include "mkert_version.h"
#include "mkert_buildinfo.h"

#include "dev/cmldclib/cmldclib.h"

/* -------------------------------------------------------------------------- */

#ifdef USE_RPICAM
  #include "raspieng.h"
#endif

#ifdef USE_FPCAM
  #include "fpeng.h"
#endif

#ifdef USE_ONSEMI
  #include "onsemieng.h"
#endif

#ifdef USE_PANA
  #include "panaeng.h"
#endif

#ifdef USE_CLDB
  #include "cldbeng.h"
#endif

#ifdef USE_V4L2
  #include "v4l2eng.h"
#endif

/* -------------------------------------------------------------------------- */

using namespace mke;
using namespace mke::rt;

/* -------------------------------------------------------------------------- */

#define OS_RELEASE_PATH         "/etc/os-release"

/* -------------------------------------------------------------------------- */

void Worker::init()

{  
  LOG_INFO << "Worker initialization";
  
  // initialization of engine
  
  std::string engtype = cfg_.getEngineConfig().type;
  LOG_INFO << "Loading engine: " << engtype;
  if(engtype == "LOAD")      
    engine_.reset(new LoadEngine(cfg_, detector_.getNoDetectors()));
  
#ifdef USE_RPICAM
  else if(engtype == "RSPI")
    engine_.reset(new RasPiEngine(cfg_, detector_));
#endif // end USE_RPICAM

#ifdef USE_FPCAM
  else if(engtype == "FPCAM")
      engine_.reset(new FpEngine(cfg_, detector_));
#endif // end USE_FPCAM
  
#ifdef USE_ONSEMI
  else if(engtype == "ONSEMI")
      engine_.reset(new OnSemiEngine(cfg_, detector_));
#endif // end USE_ONSEMI

#ifdef USE_PANA
  else if(engtype == "PANA")
      engine_.reset(new PanaEngine(cfg_, detector_));
#endif // end USE_PANA

#ifdef USE_CLDB
  else if(engtype == "CLDB")
      engine_.reset(new CldbEngine(cfg_, detector_));
#endif // end USE_CLDB

#ifdef USE_V4L2
  else if(engtype == "V4L2")
      engine_.reset(new V4L2Engine(cfg_, detector_));
#endif // end USE_V4L2
  else
    throw std::runtime_error("Unknown type of engine");
  
  engine_->initialize();
  autoexp_.setEngine(engine_.get());

  // register active workers modules
  
  modules_.push_back(&mod_depth_);

#ifdef MKE_USE_RESERVED_API  
  mod_service_.setEngine(engine_.get());
  if(is_reserved_mode_)
    modules_.push_back(&mod_service_);
#endif
  WorkerBase::init();

  if(probe_)
  {
    probe_->addProfile(PROBE_IDLE, "L+W(250)-W(1750)");
    probe_->addProfile(PROBE_MEASURE, "L+W(200)-W(200)");
    probe_->addProfile(PROBE_SERVICE, "L+W(200)-W(200)+W(200)-W(600)");
  }
  
  if(cfg_.getGeneralConfig().temp_idle_limits.size() > 0 &&
      cfg_.getGeneralConfig().temp_idle_limits.size() != cfg_.getGeneralConfig().temp_idxs.size())
      throw std::runtime_error("Bad number of temperature idle limis!");
  
  // MkE API callback initialization

  for(auto m : modules_)
    m->init();
  
  // add profiled objects to profiler
  
  profiler_.registerProfiled("mkedd", &detector_);
  profiler_.registerProfiled("engine", engine_.get());
  profiler_.registerProfiled("autoexposure", &autoexp_);
  
  profiler_.init();
  
  // add stats objects
  
  stats_.registerObject("detector", &detector_);
  stats_.registerObject("engine", engine_.get());
  stats_.registerObject("general", this);
}

/* -------------------------------------------------------------------------- */

void Worker::finalize()
{
  // first try to finalize objects
  
  try
    {
      WorkerBase::finalize();
      
      // finalize worker module/s
      
      for(auto m : modules_)
        m->finalize();
      
      engine_.reset();
    }
#ifdef USE_RPICAM        
  catch(mke::dev::MmalError & e) 
    {
      throw api::FatalError(mke::api::MKE_FATAL_BADCAMERA, e.what());
    }
#endif  
  catch(std::exception & e)
    {
      // prefer previous error if appeard
      
      WorkerBase::finalize();
      
      throw mke::rt::api::FatalError(mke::api::MKE_FATAL_RUNTIME,
                                     e.what());
    }
    
  WorkerBase::finalize();
}

/* -------------------------------------------------------------------------- */

#ifdef MKE_USE_RESERVED_API    

void Worker::getStats(StatsType & stats) const 

{
  stats["governon_countdown"] = gov_.countdown();
  stats["governon_limit"] = gov_.maxOnLimit();

  // add temperatures
  
  StatsType temp_root;
  const std::vector<int> & idxs = cfg_.getGeneralConfig().temp_idxs;
  const std::vector<int> & idle_limits = cfg_.getGeneralConfig().temp_idle_limits;
  char buff[16];

  for(size_t i = 0; i < idxs.size(); ++i)
    {
      StatsType leaf;
      double temp = getTemperature(idxs[i]);
      if(temp > TEMP_IGNORE)
        leaf["current_value"] = temp;
      else
        leaf["current_value"] = StatsType();

      if(idle_limits.size() > 0 && idle_limits[i] > TEMP_IGNORE)
        leaf["limit_idle"] = idle_limits[i];
      else
        leaf["limit_idle"] = StatsType();
      
      sprintf(buff, "%d", idxs[i]);
      temp_root[buff] = leaf;
    }
  
  stats["temperatures"] = temp_root;
}

#endif

/* -------------------------------------------------------------------------- */

void Worker::registerApiCallbacks()

{
  WorkerBase::registerApiCallbacks();
  
  // state callbacks
    
  api_helper_.api_resolver.registerCallback(mke::api::MKE_REQUEST_SET_STATE, 
            std::bind(&Worker::setStateCallback, this, std::placeholders::_1));

  api_helper_.api_resolver.registerCallback(mke::api::MKE_REQUEST_GET_POLICY,
      std::bind(&Worker::policyCallback, this, std::placeholders::_1));
  api_helper_.api_resolver.registerCallback(mke::api::MKE_REQUEST_SET_POLICY,
      std::bind(&Worker::policyCallback, this, std::placeholders::_1));  
  api_helper_.api_resolver.registerCallback(mke::api::MKE_REQUEST_LIST_POLICIES,
      std::bind(&Worker::policyCallback, this, std::placeholders::_1));  

#ifdef MKE_USE_RESERVED_API  
  if(isReservedMode())
    {
      api_helper_.api_resolver.registerCallback(mke::api::MKE_REQUEST_GET_RESERVED_INFO,
              std::bind(&Worker::reservedCallback, this, std::placeholders::_1));
      
      api_helper_.api_resolver.registerCallback(mke::api::MKE_REQUEST_SET_PROFILE,
          std::bind(&Worker::policyCallback, this, std::placeholders::_1));
      api_helper_.api_resolver.registerCallback(mke::api::MKE_REQUEST_GET_PROFILE,
          std::bind(&Worker::policyCallback, this, std::placeholders::_1));
      api_helper_.api_resolver.registerCallback(mke::api::MKE_REQUEST_GET_STATS,
          std::bind(&Worker::statsCallback, this, std::placeholders::_1));
      api_helper_.api_resolver.registerCallback(mke::api::MKE_REQUEST_RESET_STATS,
          std::bind(&Worker::statsCallback, this, std::placeholders::_1));
    }
#endif
}

/* -------------------------------------------------------------------------- */

void Worker::unregisterApiCallbacks()

{
  WorkerBase::unregisterApiCallbacks();

  // state callbacks
    
  api_helper_.api_resolver.unregisterCallback(mke::api::MKE_REQUEST_SET_STATE);

  api_helper_.api_resolver.unregisterCallback(mke::api::MKE_REQUEST_GET_POLICY);
  api_helper_.api_resolver.unregisterCallback(mke::api::MKE_REQUEST_SET_POLICY);  
  api_helper_.api_resolver.unregisterCallback(mke::api::MKE_REQUEST_LIST_POLICIES);  

#ifdef MKE_USE_RESERVED_API
  if(isReservedMode())
    {
      api_helper_.api_resolver.unregisterCallback(mke::api::MKE_REQUEST_GET_RESERVED_INFO);

      api_helper_.api_resolver.unregisterCallback(mke::api::MKE_REQUEST_SET_PROFILE);
      api_helper_.api_resolver.unregisterCallback(mke::api::MKE_REQUEST_GET_PROFILE);

      api_helper_.api_resolver.unregisterCallback(mke::api::MKE_REQUEST_GET_STATS);
      api_helper_.api_resolver.unregisterCallback(mke::api::MKE_REQUEST_RESET_STATS);
    }
#endif
}

/* -------------------------------------------------------------------------- */

void Worker::internalCallback(api::Request *request_ptr) 

{
  if(!request_ptr->isInternal())
    throw api::ApiError(mke::api::MKE_REPLY_CLIENT_ILLEGAL_REQUEST_TYPE);
    
  LOG_INFO << "Replying to MKE_REQUEST_INTERNAL";

  // signal to all sub-modules and me too
  
  internalSignal(request_ptr->getInternalType(), request_ptr);
  
  for(auto m: modules_)
    m->internalSignal(request_ptr->getInternalType(), request_ptr);
  
  // We do not reply to this request, but we *MUST* release it
  api_helper_.api_resolver.release(request_ptr);
}      

/* -------------------------------------------------------------------------- */

void Worker::setStateCallback ( api::Request* request_ptr )

{
  mke::api::MkEStateType new_state = static_cast<mke::api::MkEStateType>(
      request_ptr->getParamsBuffer<mke::api::MkERequest_SetState>()->new_state);
  
  switch (new_state)
    {
    case mke::api::MKE_STATE_IDLE:           // go to idle
      break;
    case mke::api::MKE_STATE_DEPTH_SENSOR:   // go to depth sensing
      if(detector_.getNoDetectors() == 0)
        throw api::ApiError(mke::api::MKE_REPLY_SERVER_ERROR);
      if(!gov_.canRun())
        throw api::ApiError(mke::api::MKE_REPLY_SERVER_BUSY);
      break;
#ifdef MKE_USE_RESERVED_API
    case mke::api::MKE_STATE_SERVICE:        // go to service state
      if(!is_reserved_mode_)
        throw api::ApiError(mke::api::MKE_REPLY_CLIENT_MALFORMED_REQUEST);
      break;
#endif      
    default:
      throw api::ApiError(mke::api::MKE_REPLY_CLIENT_MALFORMED_REQUEST);
    }

    // same state and forbidden to switch from one active mode to another
    
    if(api_helper_.api_resolver.getState() == new_state || (new_state != mke::api::MKE_STATE_IDLE 
            && api_helper_.api_resolver.getState() != mke::api::MKE_STATE_IDLE))
      {
        throw api::ApiError(mke::api::MKE_REPLY_CLIENT_REQUEST_DOES_NOT_APPLY);
      }
    
    // set to new state
    
    applyState(new_state);
    api_helper_.api_resolver.reply(mke::api::MKE_REPLY_OK, request_ptr);
}

/* -------------------------------------------------------------------------- */

void Worker::reservedCallback(api::Request* request_ptr)

{
  // just return true
  
  api_helper_.api_resolver.reply(mke::api::MKE_REPLY_OK, request_ptr);
}

/* -------------------------------------------------------------------------- */

void Worker::policyCallback(api::Request * request_ptr)

{
  const size_t max_profile_len = sizeof(mke::api::MkEReply_GetPolicy::profile_name);

  // do not check state .. can be called from everywhere
  
  if(request_ptr->getType() == mke::api::MKE_REQUEST_GET_POLICY)
    {
      api::Reply * reply_ptr = api_helper_.prepareBasicReply(request_ptr);
      mke::api::MkEReply_GetPolicy * params = reply_ptr->getParamsBuffer<mke::api::MkEReply_GetPolicy>();
      const std::string profile = profiler_.getPolicy();
      
      // cut profile name with final \0 char
      
      std::memcpy(reinterpret_cast<void *>(params->profile_name), profile.c_str(), 
                  std::min(max_profile_len, profile.length()+1));

      api_helper_.api_resolver.reply(reply_ptr, request_ptr);
    }
  else if(request_ptr->getType() == mke::api::MKE_REQUEST_SET_POLICY)
    {
      mke::api::MkERequest_SetPolicy * params = request_ptr->getParamsBuffer<mke::api::MkERequest_SetPolicy>();
      std::string profile(params->profile_name, params->profile_name+max_profile_len);
      
      try
      {
        profiler_.setPolicy(profile.c_str());
      }
      catch(std::exception & ex)
      {
        LOG_WARN << "Unable to set policy: " << ex.what();
        throw api::ApiError(mke::api::MKE_REPLY_CLIENT_MALFORMED_REQUEST);
      }

      api_helper_.api_resolver.reply(mke::api::MKE_REPLY_OK, request_ptr);
    }
  else if(request_ptr->getType() == mke::api::MKE_REQUEST_LIST_POLICIES)
    {
      std::vector<std::string> pols;
      profiler_.listPolicies(pols);
      int size = 0;
      for(const auto & p : pols)
        size += p.length() + 1;
      api::Reply * reply_ptr = api_helper_.prepareDynamicReply(request_ptr, size);
      mke::api::MkEReply_ListPolicies * params = reply_ptr->getParamsBuffer<mke::api::MkEReply_ListPolicies>();
      params->num_policies = pols.size();
      
      // cut profile name with final \0 char
      
      char * dst = reinterpret_cast<char *>(reply_ptr->getPayloadBuffer());
      for(const auto & p : pols)
        {
          std::memcpy(reinterpret_cast<void *>(dst), p.c_str(), p.length());
          dst += p.length();
          *dst++ = '\0';
        }

      api_helper_.api_resolver.reply(reply_ptr, request_ptr);
    }
#ifdef MKE_USE_RESERVED_API    
  else if(request_ptr->getType() == mke::api::MKE_REQUEST_GET_PROFILE)
    {
      const std::string profile = profiler_.getProfile();
      api::Reply * reply_ptr = api_helper_.prepareDynamicReply(request_ptr, profile.length());
      
      // cut profile name with final \0 char
      
      std::memcpy(reinterpret_cast<void *>(reply_ptr->getPayloadBuffer()), 
                  profile.c_str(), profile.length());

      api_helper_.api_resolver.reply(reply_ptr, request_ptr);
    }
  else if(request_ptr->getType() == mke::api::MKE_REQUEST_SET_PROFILE)
    {
      std::string profile(reinterpret_cast<const char *>(request_ptr->getPayloadBuffer()),
                          request_ptr->getPayloadSize());
      
      try
      {
        std::string policy = profiler_.addProfile(profile.c_str());
        if(policy.length() > 0)
          profiler_.setPolicy(policy.c_str());
        else 
          profiler_.setProfile(profile.c_str());
      }
      catch(std::exception & ex)
      {
        LOG_WARN << "Unable to set profile: " << ex.what();
        throw api::ApiError(mke::api::MKE_REPLY_CLIENT_MALFORMED_REQUEST);
      }

      api_helper_.api_resolver.reply(mke::api::MKE_REPLY_OK, request_ptr);
    }    
#endif
}

/* -------------------------------------------------------------------------- */
#ifdef MKE_USE_RESERVED_API    

void Worker::statsCallback(api::Request * request_ptr)

{
  // do not check state .. can be called from everywhere
  
  if(request_ptr->getType() == mke::api::MKE_REQUEST_GET_STATS)
    {
      const std::string whole = stats_.getStats();
      api::Reply * reply_ptr = api_helper_.prepareDynamicReply(request_ptr, whole.length());
      
      // cut profile name with final \0 char
      
      std::memcpy(reinterpret_cast<void *>(reply_ptr->getPayloadBuffer()), 
                  whole.c_str(), whole.length());

      api_helper_.api_resolver.reply(reply_ptr, request_ptr);
    }
  else if(request_ptr->getType() == mke::api::MKE_REQUEST_RESET_STATS)
    {
      stats_.resetStats();

      api_helper_.api_resolver.reply(mke::api::MKE_REPLY_OK, request_ptr);
    }    
}
#endif
/* -------------------------------------------------------------------------- */

void Worker::applyState(mke::api::MkEStateType new_state)

{
  // update governon
  switch (new_state)
    {
    case mke::api::MKE_STATE_IDLE:          
      gov_.setOff();
      if(probe_)
        probe_->setProfile(PROBE_IDLE);
      break;
    case mke::api::MKE_STATE_DEPTH_SENSOR:
      gov_.setOn();
      if(probe_)
        probe_->setProfile(PROBE_MEASURE);
      break;
#ifdef MKE_USE_RESERVED_API
    case mke::api::MKE_STATE_SERVICE:
      if(probe_)
        probe_->setProfile(PROBE_SERVICE);
      autoexp_.reset();
      break;
#endif
    default:
      break;
    }
  
  // turn off lasers
  
  engine_->stateChanged(new_state);
  
  // propagate state to modules
  
  for(auto m : modules_)
    m->stateChanged(new_state);
  
  // assign new state - propagate to API resolver
  
  api_helper_.api_resolver.setState(new_state);
}

/* -------------------------------------------------------------------------- */

bool Worker::process()

{
  // get new frame from camera
  
  curr_frame = engine_->getFrame(cfg_.getAPIConfig().max_request_delay);

  // process frame in each module
  
  processCameraFrame(curr_frame);
  for(auto m : modules_)
    m->processCameraFrame(curr_frame);
  
  // governon 
  if(api_helper_.api_resolver.getState() == mke::api::MKE_STATE_DEPTH_SENSOR && !gov_.canRun())
    applyState(mke::api::MKE_STATE_IDLE);
  
  // check Temperatures
  checkTemperatures();
  
  // apply auto exposure if enabled
  if(api_helper_.api_resolver.getState() == mke::api::MKE_STATE_DEPTH_SENSOR)
    try
      {
        autoexp_.process(curr_frame);
      }
    catch(std::exception & ex)
      {
        LOG_WARN << "An error occured during auto-exp: " << ex.what();
      }

  // Resolver::process will consume all available request pending
  // in the API request queue and call the appropriate callbacks
  bool do_continue = api_helper_.api_resolver.process();
  
  // release frame
  
  if(curr_frame)
    {
      engine_->releaseFrame(curr_frame);
      curr_frame = nullptr;
    }
  
  return do_continue;  
}

/* -------------------------------------------------------------------------- */

void Worker::start()
{
  if(probe_)
    probe_->setProfile(PROBE_IDLE);
    
  try
    {
      while(process())
        {};
    }
  catch(...)
    {
      fatal_err_ = std::current_exception();
    }
    
    // kill all long lives - done in finalize stage
}

/* -------------------------------------------------------------------------- */

void Worker::checkTemperatures()

{
  const std::vector<int> & idxs = cfg_.getGeneralConfig().temp_idxs;
  const std::vector<int> & idle_limits = cfg_.getGeneralConfig().temp_idle_limits;
  
  if(idle_limits.size() == 0)
      return;

  for(size_t i = 0; i < idxs.size(); ++i)
    {
      double temp = getTemperature(idxs[i]);
      if(idle_limits[i] > TEMP_IGNORE && temp > idle_limits[i])
        {
          LOG_WARN << "Temperature T" << idxs[i] << " has reached the limit (" << 
                          temp << ">=" << idle_limits[i] <<"). Switching to IDLE";
          applyState(mke::api::MKE_STATE_IDLE);
        }
    }
}

/* -------------------------------------------------------------------------- */

double Worker::getTemperature(int sensorId) const
{
    double ret = WorkerBase::getTemperature(sensorId);
#ifdef USE_CLDB
    CldbEngine * cldbeng = dynamic_cast<CldbEngine *>(engine_.get());
    if(cldbeng)
      return cldbeng->getBoardTemperature(sensorId);
#endif
    return ret;
}

/* -------------------------------------------------------------------------- */
