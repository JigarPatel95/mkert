/*
 * StatsMng - manages the computed stats
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef _STATSMNG_H_
#define _STATSMNG_H_

/* -------------------------------------------------------------------------- */

#include <string>
#include <map>

#ifndef __MINGW32__
  #include <sys/times.h>
#endif
#include "util/timers.h"
#include <nlohmann/json.hpp>

/* -------------------------------------------------------------------------- */

namespace mke {
namespace rt {

/* -------------------------------------------------------------------------- */

typedef nlohmann::json StatsType;

/* -------------------------------------------------------------------------- */

class StatsGenerating 

{
public:
  
  virtual void getStats(StatsType & stats) const = 0;
  
  virtual void resetStats() = 0;
};

/* -------------------------------------------------------------------------- */

class StatsManager {

  std::map<std::string, StatsGenerating *>     registered_list_; // list of registered profiled listeners
  mke::util::StatsWallClockTimer<double, std::ratio<1>> wc_reset_timer_;
  mke::util::StatsWallClockTimer<double, std::ratio<1>> wc_init_timer_;
#ifndef __MINGW32__
  struct tms tms_from_init_;
  struct tms tms_from_reset_;
#endif
public:  
  
  StatsManager()
  {
    wc_reset_timer_.start();
    wc_init_timer_.start();
#ifndef __MINGW32__
    times(&tms_from_init_);
    times(&tms_from_reset_);
#endif
  };
  
  std::string getStats() const;
  
  void resetStats();
  
  /**
   * @brief Register object
   */
  virtual void registerObject(const char * name, StatsGenerating * obj);

  /**
   * @brief Unregister object
   */
  virtual void unregisterObject(const char * name);
  
};


/* -------------------------------------------------------------------------- */

} // end of mke::rt
} // end of mke

/* -------------------------------------------------------------------------- */

#endif // _StATS_H_

/* -------------------------------------------------------------------------- */
