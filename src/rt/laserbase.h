/* 
 * LaserWrap - Wraps controlling of lasers by FPGA old/new generation
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondrej Fisar, fisar@magik-eye.com
 *
 */

/* -------------------------------------------------------------------------- */

#pragma once

/* -------------------------------------------------------------------------- */

#include "dev/rpsynclib/rpsynclib.h"
#include "dev/cmldclib/cmldclib.h"
#include "mkert_config.h"
#include "rt/detector.h"

#include <exception>

/* -------------------------------------------------------------------------- */

namespace mke {
namespace rt {

/* -------------------------------------------------------------------------- */

class LaserBase
{
public:
  virtual void fire(int pattern) = 0;
  
  virtual void fire(int pattern, float strobe_length, float strobe_offset) = 0;
 
  virtual void startSync(const Detector & det) = 0;
  
  virtual uint16_t fireSync(uint32_t frame_id) = 0;  
  
  virtual void stopSync() = 0;
  
  virtual ~LaserBase() {};
};

/* -------------------------------------------------------------------------- */

class FakeLaser : public LaserBase
{
public:
  virtual void fire(int pattern) {}
 
  virtual void fire(int pattern, float strobe_length, float strobe_offset) {}
 
  virtual void startSync(const Detector & det) {}
  
  virtual uint16_t fireSync(uint32_t frame_id) { return 0; }  
  
  virtual void stopSync() {}
  
  virtual ~FakeLaser() {}
};

/* -------------------------------------------------------------------------- */

class SwSyncLaser : public LaserBase

{
  const mke::MkertConfig::EngineConfig     &    eng_cfg_;
  const Detector                           *    detector_;  
  
  inline void checkError(int ret, const char * msg)
  {
    if (ret != 0)
      throw std::runtime_error(std::string("RPSyncLibError: ") + msg);
  };
  
public:
  SwSyncLaser(const mke::MkertConfig::EngineConfig & eng_cfg) :
    eng_cfg_(eng_cfg),
    detector_(nullptr)
  {
    checkError(rpsync_open(), "Cannot open device");
  }
  
  virtual ~SwSyncLaser() 
  {
    checkError(rpsync_close(), "Cannot close device");
  };
  
  virtual void fire(int pattern)
  {
    checkError(rpsync_set(pattern), "Cannot set laser pattern");
  }
  
  virtual void fire(int pattern, float strobe_length, float strobe_offset) 
  {
    checkError(rpsync_set_sec(pattern, strobe_length/1000000.0f), "Cannot set laser pattern with duration");
  }
  
  virtual void startSync(const Detector & det)
  {
    detector_ = &det;
  }
  
  virtual uint16_t fireSync(uint32_t frame_id)
  {
    if(!detector_ || detector_->getNoDetectors() <= 0)
      return 0;
    
    if(eng_cfg_.strobe_length < 0)
      checkError(rpsync_set(detector_->getLaserPattern(frame_id % detector_->getNoDetectors())), "Unable to fire still laser");
    else
      checkError(rpsync_set_sec(detector_->getLaserPattern(frame_id % detector_->getNoDetectors()),
                    eng_cfg_.strobe_length/1000000.0f), "Unable to fire laser with duration");
    
    return (frame_id-1) % detector_->getNoDetectors();
  }
  
  virtual void stopSync()
  {
     detector_ = nullptr;
  }
};

/* -------------------------------------------------------------------------- */

class HwSyncLaser : public LaserBase

{
  const mke::MkertConfig::EngineConfig     &    eng_cfg_;
  bool                                          sync_run_;
  uint32_t                                      curr_mode_;
  int                                           num_patterns_;
  int                                           next_phase_;
  int                                           phase_period_;
  int                                           miss_cnt_;
  
  inline void checkError(int ret, const char * msg)
  {
    if (ret != 0)
      throw std::runtime_error(std::string("CmldcLibError: ") + msg);
  };
  
  inline uint32_t compileMode(int num, uint8_t p1, uint8_t p2 = 0, uint8_t p3 = 0, uint8_t p4 = 0)
  {
    switch(num)
    {
      case 1:
        p2 = p3 = p4 = p1;
        break;
      case 2:
        p3 = p1;
        p4 = p2;
        break;
      case 3:
        p4 = 0;        
        break;
      default:
        break;
    }
    
    return (p1 & 0xf) | ((p2 & 0xf) << 4) | ((p3 & 0xf) << 8) | ((p4 & 0x0f) << 12)
            | (num == 3 ? (1 << 16) : 0);
  }
  
public:
  HwSyncLaser(const mke::MkertConfig::EngineConfig & eng_cfg) :
    eng_cfg_(eng_cfg),
    curr_mode_(0),
    num_patterns_(0),
    next_phase_(-1),
    miss_cnt_(0)
  {
  }
  
  virtual ~HwSyncLaser()
  {
    if(sync_run_)
      stopSync();
    
    checkError(cmldc_close(), "Cannot close device");
  }
  
  virtual void fire(int pattern)
  {
    if(sync_run_)
      checkError(cmldc_stop_sync(), "Cannot stop sync - hidden");
    
    checkError(cmldc_set_force_on(pattern), "Cannot set still laser pattern");
  }
  
  virtual void fire(int pattern, float strobe_length, float strobe_offset) 
  {
    if(sync_run_)
      checkError(cmldc_stop_sync(), "Cannot stop sync - hidden2");
    
    checkError(cmldc_start_sync(compileMode(1, pattern), strobe_offset/1000000.0f, strobe_length/1000000.0f), 
               "Cannot start sync - hidden2");
  }
  
  virtual void startSync(const Detector & det)
  {
    assert(!sync_run_);
    
    uint32_t mode;
    switch(det.getNoDetectors())
    {
      case 1:
        mode = compileMode(1, det.getLaserPattern(0));
      break;
      case 2:
        mode = compileMode(2, det.getLaserPattern(0), det.getLaserPattern(1));
      break;
      case 3:
        mode = compileMode(3, det.getLaserPattern(0), det.getLaserPattern(1),
                              det.getLaserPattern(2));
      break;
      case 4:
        mode = compileMode(4, det.getLaserPattern(0), det.getLaserPattern(1),
                              det.getLaserPattern(2), det.getLaserPattern(3));
      break;
      default:
        throw std::runtime_error("Unsupported number of detectors");
    }
    
    checkError(cmldc_start_sync(mode, 
                                eng_cfg_.strobe_offset/1000000.0f, 
                                eng_cfg_.strobe_length/1000000.0f), "Cannot start sync");
                                
    curr_mode_ = mode;
    num_patterns_ = det.getNoDetectors();

    phase_period_ = num_patterns_ == 3 ? 3 : 4;
    next_phase_ = -1;
    miss_cnt_ = 0;

    sync_run_ = true;
  }
  
  int estimatePhase(int ph)
  {
    assert(eng_cfg_.use_phase_estimator);
    
    if(next_phase_ >= 0)
      {
        int diff = (ph - next_phase_ + phase_period_) % phase_period_;
        
        if(next_phase_ == ph)
          {
            miss_cnt_ = 0;
          }
        else if(miss_cnt_ < 4 && diff == 1)
          {
            miss_cnt_++;
            ph = next_phase_;
          }
        else
          {
            miss_cnt_ = 0;
          }
      }

    next_phase_ = (ph + 1) % phase_period_;
    
    return ph;
  }
  
  virtual uint16_t fireSync(uint32_t frame_id)
  {
    assert(sync_run_);
    
    // get current phase and return it
    
    int ph = cmldc_get_laser_phase();
    if(eng_cfg_.use_phase_estimator)
      ph = estimatePhase(ph);
      
    return (ph + num_patterns_ + eng_cfg_.phase_shift) % num_patterns_; 

  }
  
  virtual void stopSync()
  {  
     assert(sync_run_);
     checkError(cmldc_stop_sync(), "Cannot stop sync");
     sync_run_ = false;
  }
};

/* -------------------------------------------------------------------------- */

} // end of mke::rt
} // end of mke

/* -------------------------------------------------------------------------- */
