/*
 * Profiler - manages the profiles
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar, Jan Heller
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef _PROFILER_H_
#define _PROFILER_H_

/* -------------------------------------------------------------------------- */

#include "mkert_config.h"

#include <string>
#include <map>

#include <nlohmann/json.hpp>

/* -------------------------------------------------------------------------- */

namespace mke {
namespace rt {

/* -------------------------------------------------------------------------- */

typedef nlohmann::json ProfileType;

/* -------------------------------------------------------------------------- */

class Profiled 

{
public:
  
  virtual void setProfile(const ProfileType & profile) = 0;

  virtual void getProfile(ProfileType & profile) const = 0;
  
  virtual void validateProfile(const ProfileType & profile) const = 0;
};

/* -------------------------------------------------------------------------- */

class Profiler {

  const MkertConfig::GeneralConfig    & cfg_;           // general configuration
  std::map<std::string, Profiled *>     profiled_list_; // list of rehistered profiled listeners
  std::map<std::string, std::string>    profiles_;      // list of allready setted profiles
  std::string                           last_policy_;   // last used policy 
  
  void validateProfile(const char * profile, ProfileType & tree) const;

public:  
  
  Profiler(const MkertConfig::GeneralConfig & cfg) :
  cfg_(cfg)
  {};
  
  void init();
  
  void setPolicy(const char * profile_name);
 
  const char * getPolicy() const;
  
  void listPolicies(std::vector<std::string> & policies) const;

  std::string addProfile(const char * profile);

  void setProfile(const char * profile);

  std::string getProfile() const;
  
  /**
   * @brief Register profiled object
   */
  virtual void registerProfiled(const char * name, Profiled * profiled);

  /**
   * @brief Unregister profiled object
   */
  virtual void unregisterProfiled(const char * name);
  
};


/* -------------------------------------------------------------------------- */

} // end of mke::rt
} // end of mke

/* -------------------------------------------------------------------------- */

#endif // _PROFILER_H_

/* -------------------------------------------------------------------------- */
