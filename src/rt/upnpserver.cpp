/*
 * UPnPServer - serving UPnP messages - just discover
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

// includes

#include "upnpserver.h"
#include "devinfo.h"
#include "util/logging.h"

#include <map>

/* -------------------------------------------------------------------------- */

using namespace mke;
using namespace mke::rt;

#ifdef MKERT_USE_UPNP

/* -------------------------------------------------------------------------- */

int upnpCallbackEventHandler(Upnp_EventType EventType, void *Event, void *Cookie) 

{ 
  return 0; 
} 

/* -------------------------------------------------------------------------- */

UpnpServer::UpnpServer(const DeviceInfo & dev_info) :
  device_handle_(-1),
  device_info_(dev_info)
  
{
  checkError(UpnpInit(NULL, 0), "Cannot initialize UPnP");
  const char  * host = UpnpGetServerIpAddress();
  int           port = UpnpGetServerPort();
  
  std::ostringstream so;
  so << "http://" << host << ":" << port;
  
  LOG_WARN << "UPnP service available on " << so.str();

  checkError(UpnpRegisterRootDevice2(
                  UPNPREG_BUF_DESC,
                  device_info_.xml_desc.c_str(),
                  device_info_.xml_desc.length(),
                  1, /* config_baseURL */
                  upnpCallbackEventHandler, 
                  (void *) this, &device_handle_), "Cannot register device");  
  checkError(UpnpSendAdvertisement(device_handle_, 100), "Cannot send advertisement");
}

/* -------------------------------------------------------------------------- */

void UpnpServer::checkError(int ret_status, const char* desc)
{
  if(ret_status != UPNP_E_SUCCESS)
    {
      std::ostringstream so;
      so << "An UPNP error occured no. " << ret_status;
      if(desc)
        so << ", " << desc;
      throw std::runtime_error(so.str());
    }
}

/* -------------------------------------------------------------------------- */
#else

UpnpServer::UpnpServer(const DeviceInfo & dev_info) :
  device_info_(dev_info)
  
{
    LOG_WARN << "UPNP library has been ignored during compilation procedure";
}

#endif
