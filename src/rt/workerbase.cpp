/*
 * WorkerBase - serves basic of API requests
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

// includes

#include "workerbase.h"

#include <fstream>
#include <boost/algorithm/string.hpp>

#include "mkert_version.h"
#include "mkert_buildinfo.h"

#include "util/crc32.h"

#include "dev/cmldclib/cmldclib.h"

/* -------------------------------------------------------------------------- */

using namespace mke;
using namespace mke::rt;

/* -------------------------------------------------------------------------- */

#define OS_RELEASE_PATH         "/etc/os-release"

/* -------------------------------------------------------------------------- */

WorkerBase::WorkerBase(const MkertConfig & cfg, api::MemoryPool &api_pool, 
                       api::Resolver &api_resolver, const char * mkert_dir, 
                       const DeviceInfo & dev_info) : 
  cfg_(cfg),
  api_helper_(api_pool, api_resolver),
  mkert_dir_(mkert_dir),
  dev_info_(dev_info)
{
  if(cfg_.getGeneralConfig().use_user_led)
  {
    probe_.reset(new mke::util::LedProbe<int>());
    probe_->applyCode("-");
    probe_->setFinalizeCode("-");
  }
};

/* -------------------------------------------------------------------------- */

WorkerBase::~WorkerBase()
{
  probe_.reset();
}

/* -------------------------------------------------------------------------- */

void WorkerBase::init()

{
  // get system info
  
  findSystemInfo();
  
  // 
  
  registerApiCallbacks();
}

/* -------------------------------------------------------------------------- */

void WorkerBase::finalize()
{
  unregisterApiCallbacks();
  
  // some error appeard ?

  try 
    {
      if(fatal_err_)
        std::rethrow_exception (fatal_err_);
    } 
    catch (const std::exception& e) 
    {
      throw mke::rt::api::FatalError(mke::api::MKE_FATAL_RUNTIME,
                                    e.what());
    }  
}

/* -------------------------------------------------------------------------- */

void WorkerBase::findSystemInfo()

{
  // set all numbers to 0
  
  for(int i = 0; i < 3; ++i)
    fw_ver_[i] = 0;
  
  // load file
  try
    {
      std::ifstream ifs(OS_RELEASE_PATH);
      
      std::string line;
      while (std::getline(ifs, line))
        {
          std::vector<std::string> key_val;
          boost::split(key_val, line, boost::is_any_of("="));
          
          // ignore empty lines
          
          if(key_val.size() < 2)
            continue;
          
          boost::trim(key_val[0]);
          boost::trim(key_val[1]);
          
          // ignore non VERSION_ID lines
          
          if(key_val[0] != "VERSION_ID")
            continue;
          std::string val;
          
          if((key_val[1].at(0) == '"' && key_val[1].back() == '"')
                        || (key_val[1].at(0) == '\'' && key_val[1].back() == '\''))
            val = key_val[1].substr(1,key_val[1].length()-2);
          else
            val = key_val[1];
          
          std::vector<std::string> vers;
          boost::split(vers, val, boost::is_any_of("."));
          
          for(unsigned int i = 0; i < vers.size() && i < sizeof(fw_ver_); ++i)
            fw_ver_[i] = std::atol(vers[i].c_str());
          
          break;
        }
    }
  catch(std::exception & e)
    {
      LOG_WARN << e.what();
      
      // ignore error it is not critical
    }
}

/* -------------------------------------------------------------------------- */

void WorkerBase::registerApiCallbacks()

{

  // state callbacks
    
  api_helper_.api_resolver.registerCallback(mke::api::MKE_REQUEST_INTERNAL, 
            std::bind(&WorkerBase::internalCallback, this, std::placeholders::_1));
  api_helper_.api_resolver.registerCallback(mke::api::MKE_REQUEST_GET_FIRMWARE_INFO,
            std::bind(&WorkerBase::getFwInfoCallback, this, std::placeholders::_1));
  api_helper_.api_resolver.registerCallback(mke::api::MKE_REQUEST_GET_DEVICE_INFO,
            std::bind(&WorkerBase::getDeviceInfoCallback, this, std::placeholders::_1));
  api_helper_.api_resolver.registerCallback(mke::api::MKE_REQUEST_GET_DEVICE_XML,
            std::bind(&WorkerBase::getDeviceXmlCallback, this, std::placeholders::_1));
  api_helper_.api_resolver.registerCallback(mke::api::MKE_REQUEST_UPLOAD_PACKAGE,
            std::bind(&WorkerBase::uploadPackageCallback, this, std::placeholders::_1));
  
#ifdef USE_EXTRA_API_CALLS
  api_helper_.api_resolver.registerCallback(mke::api::MKE_REQUEST_DOWNLOAD_FILE,
          std::bind(&WorkerBase::downloadFileCallback, this, std::placeholders::_1));
#endif
  
}

/* -------------------------------------------------------------------------- */

void WorkerBase::unregisterApiCallbacks()
{

  // state callbacks
    
  api_helper_.api_resolver.unregisterCallback(mke::api::MKE_REQUEST_INTERNAL);
  api_helper_.api_resolver.unregisterCallback(mke::api::MKE_REQUEST_GET_FIRMWARE_INFO);
  api_helper_.api_resolver.unregisterCallback(mke::api::MKE_REQUEST_GET_DEVICE_INFO);
  api_helper_.api_resolver.unregisterCallback(mke::api::MKE_REQUEST_GET_DEVICE_XML);
  api_helper_.api_resolver.unregisterCallback(mke::api::MKE_REQUEST_UPLOAD_PACKAGE);
  
#ifdef USE_EXTRA_API_CALLS
  api_helper_.api_resolver.unregisterCallback(mke::api::MKE_REQUEST_DOWNLOAD_FILE);
#endif
}

/* -------------------------------------------------------------------------- */

void WorkerBase::internalCallback(api::Request *request_ptr) 

{
  if(!request_ptr->isInternal())
    throw api::ApiError(mke::api::MKE_REPLY_CLIENT_ILLEGAL_REQUEST_TYPE);
    
  LOG_INFO << "Replying to MKE_REQUEST_INTERNAL";
  
  // We do not reply to this request, but we *MUST* release it
  api_helper_.api_resolver.release(request_ptr);
}      

/* -------------------------------------------------------------------------- */

uint32_t hextol(const char * str)

{
  const char * c = str;
  uint8_t nibble;
  
  uint32_t val = 0;
  while(*c != '\0')
    {
      if(*c >= '0' && *c <= '9')
        nibble = *c - '0';
      else if(*c >= 'a' && *c <= 'f')
        nibble = *c - 'a' + 10;
      else if(*c >= 'A' && *c <= 'F')
        nibble = *c - 'A' + 10;
      else
        continue;
      
      val = (val << 4) + nibble;
      ++c;
    }
    
    return val;
}

/* -------------------------------------------------------------------------- */

void WorkerBase::getFwInfoCallback(api::Request* request_ptr)

{
  api::Reply * reply_ptr = api_helper_.prepareBasicReply(request_ptr);
  
  mke::api::MkEReply_FirmwareInfo * params = 
                reply_ptr->getParamsBuffer<mke::api::MkEReply_FirmwareInfo>();
  
  params->posix_time = MKERT_TIMESTAMP;
  params->git_commit = hextol(MKERT_GIT_COMMIT);
  params->rt_ver_major = MKERT_VERSION_MAJOR;
  params->rt_ver_minor = MKERT_VERSION_MINOR;
  params->rt_ver_patch = MKERT_VERSION_PATCH;
  params->fw_ver_major = fw_ver_[0];
  params->fw_ver_minor = fw_ver_[1];
  params->fw_ver_patch = fw_ver_[2];
  
  api_helper_.api_resolver.reply(reply_ptr, request_ptr);
}

/* -------------------------------------------------------------------------- */

void WorkerBase::getDeviceInfoCallback(api::Request* request_ptr)

{
  api::Reply * reply_ptr = api_helper_.prepareBasicReply(request_ptr);
  
  mke::api::MkEReply_DeviceInfo * params = 
                reply_ptr->getParamsBuffer<mke::api::MkEReply_DeviceInfo>();

  // TBD: nothing to do now

  std::memcpy(params->unit_id, dev_info_.serial_no.c_str(), 
              std::min(sizeof(mke::api::MkEReply_DeviceInfo::unit_id), dev_info_.serial_no.length()+1));
  params->device_id = dev_info_.device_id;
  
  api_helper_.api_resolver.reply(reply_ptr, request_ptr);
}

/* -------------------------------------------------------------------------- */

void WorkerBase::getDeviceXmlCallback(api::Request* request_ptr)

{
  api::Reply * reply_ptr = 
      api_helper_.prepareDynamicReply(request_ptr, dev_info_.xml_desc.length());
  
  char * payload = reinterpret_cast<char *>(reply_ptr->getPayloadBuffer());
  std::memcpy(payload, dev_info_.xml_desc.c_str(), dev_info_.xml_desc.length());
  
  api_helper_.api_resolver.reply(reply_ptr, request_ptr);
}

/* -------------------------------------------------------------------------- */

void WorkerBase::uploadPackageCallback(api::Request* request_ptr)

{
  // check the CRC code
  
  uint32_t req_crc32 = request_ptr->getParamsBuffer<mke::api::MkERequest_Dynamic>()->params.param_upload_package.crc32;
  
  if(req_crc32 != 0x00000000)
    {
      uint32_t crc32 = 0xFFFFFFFF;
      crc32 = mke::util::Crc32::crcBuffer(request_ptr->getPayloadBuffer(), 
                                          request_ptr->getPayloadSize(), crc32);
      LOG_INFO << "Received CRC32: " << std::hex << req_crc32 << ", computed: " << crc32 << std::endl;
      if(crc32 != req_crc32)
        throw mke::rt::api::ApiError(mke::api::MKE_REPLY_CLIENT_MALFORMED_REQUEST);
    }  
  else
    {
      LOG_INFO << "No CRC32 available. File integrity check will be ignored." << std::endl;
    }
  
  // write package to tmp file
  
  std::ofstream ofs(cfg_.getGeneralConfig().upload_target);
  ofs.write(reinterpret_cast<const char *>(request_ptr->getPayloadBuffer()), 
            request_ptr->getPayloadSize());
  ofs.close();
  
  // call update script
  std::ostringstream oss("");
  oss << cfg_.getGeneralConfig().upload_exec;
  oss << " ";
  oss << cfg_.getGeneralConfig().upload_target;
  oss << " ";
  oss << mkert_dir_;

  LOG_INFO << oss.str() << std::endl;
  
  int ret = system(oss.str().c_str());
  
  if(ret == 0)
    api_helper_.api_resolver.reply(mke::api::MKE_REPLY_OK, request_ptr);
  else
    api_helper_.api_resolver.reply(mke::api::MKE_REPLY_SERVER_ERROR, request_ptr);
}

/* -------------------------------------------------------------------------- */

void WorkerBase::downloadFileCallback(api::Request* request_ptr)

{
  std::string path = std::string(request_ptr->getPayloadBuffer(), 
                                 request_ptr->getPayloadBuffer() + request_ptr->getPayloadSize());
  
  std::ifstream ifs(path, std::ifstream::binary | std::ifstream::ate);
  size_t size = ifs.tellg();
  ifs.seekg(std::ifstream::beg);

  api::Reply * reply_ptr = 
      api_helper_.prepareDynamicReply(request_ptr, size);
  
  char * payload = reinterpret_cast<char *>(reply_ptr->getPayloadBuffer());
  while(size > 0)
  {
    size_t len = 0;
    len = ifs.readsome(payload, size);
    payload += len;
    size -= len;
  }
  assert(ifs.eof());
  
  api_helper_.api_resolver.reply(reply_ptr, request_ptr);
}

/* -------------------------------------------------------------------------- */

double WorkerBase::getTemperature(int sensorId) const
{
#ifdef USE_RPICAM
    return cmldc_read_temperature(sensorId);
#else
    return TEMP_IGNORE;
#endif
}

/* -------------------------------------------------------------------------- */
