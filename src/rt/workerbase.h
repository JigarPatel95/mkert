/*
 * Worker - serves most of API requests
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef _WORKERBASE_H_
#define _WORKERBASE_H_

/* -------------------------------------------------------------------------- */

#include "util/logging.h"
#include "mkert_config.h"

/* -------------------------------------------------------------------------- */

#include "mkeapi.h"
#include "api/queue.h"
#include "api/resolver.h"
#include "api/helper.h"

#include "devinfo.h"
#include "statsmng.h"

#include "util/ledprobe.h"

/* -------------------------------------------------------------------------- */

#define TEMP_IGNORE             -274

/* -------------------------------------------------------------------------- */

namespace mke {
namespace rt {

/* -------------------------------------------------------------------------- */

/**
 * @brief BaseWorker = API request processor
 * 
 * This class serve clients' API request
 */

class WorkerBase : public StatsGenerating
{
protected:
  const MkertConfig     &   cfg_;
  api::ApiHelper            api_helper_;        // api helper will be for modules
  std::exception_ptr        fatal_err_;         // fatal error
  int                       fw_ver_[3];         // system version

  const char            *   mkert_dir_;         // path to mkert directory
  const DeviceInfo      &   dev_info_;          // UPnP server
  
  std::unique_ptr<mke::util::LedProbe<int> > 
                            probe_;
  
public:
  /**
   * @brief Constructor of BaseWorker that should serve clients requests
   * 
   * @param config          general configuration object
   * @param api_pool        API pool with requests
   * @param api_resolver    API resolver
   */
  WorkerBase(const MkertConfig & cfg, api::MemoryPool &api_pool, 
           api::Resolver &api_resolver, const char * mkert_dir, const DeviceInfo & dev_info);

  /**
   * @brief Destructor
   */
  virtual ~WorkerBase();
  
  /**
   * @brief Initialization of worker
   */
  virtual void init();

  /**
   * @brief Finalization of worker
   */
  virtual void finalize();
  
  /**
   * @brief Starts processing of requests
   */
  virtual void start() = 0;
  
protected:
  /**
   * @brief Register API callbacks
   */
  virtual void registerApiCallbacks();

  /**
   * @brief Unregister API callbacks
   */
  virtual void unregisterApiCallbacks();
  
  /**
   * @brief find system version from filesystem
   */
  void findSystemInfo();
  
  virtual void getStats(StatsType & stats) const {};
  
  virtual void resetStats() {};

  virtual double getTemperature(int sensorId) const;
  
  // callbacks -----------------------------------------------------------------
  
  /**
   * @brief Implements a callback for a test MkE API function Reserved (`mke::api::MKE_REQUEST_INTERNAL`)
   * 
   * Note that every API callback should return as soon as possible
   * 
   * @param request p_request:...
   */
  void internalCallback(api::Request *request_ptr);
    
  /**
   * @brief Implements a callback for get firmware info MkE API functions
   * (`mke::api::MKE_REQUEST_GET_FIRMWARE_INFO`)
   * 
   * Note that every API callback should return as soon as possible
   * 
   * @param request p_request:...
   */
  void getFwInfoCallback(api::Request *request_ptr);    
  
  /**
   * @brief Implements a callback for get device info MkE API functions
   * (`mke::api::MKE_REQUEST_GET_DEVICE_INFO`)
   * 
   * Note that every API callback should return as soon as possible
   * 
   * @param request p_request:...
   */
  void getDeviceInfoCallback(api::Request *request_ptr);    
  
  /**
   * @brief Implements a callback for get device xml MkE API functions
   * (`mke::api::MKE_REQUEST_GET_DEVICE_XML`)
   * 
   * Note that every API callback should return as soon as possible
   * 
   * @param request p_request:...
   */
  void getDeviceXmlCallback(api::Request *request_ptr);    
  
  /**
   * @brief Implements a callback for get device info MkE API functions
   * (`mke::api::MKE_REQUEST_UPLOAD_PACKAGE`)
   * 
   * Note that every API callback should return as soon as possible
   * 
   * @param request p_request:...
   */
  void uploadPackageCallback(api::Request *request_ptr); 
  
  /**
   * @brief Implements a callback for get device info MkE API functions
   * (`mke::api::MKE_REQUEST_UPLOAD_PACKAGE`)
   * 
   * Note that every API callback should return as soon as possible
   * 
   * @param request p_request:...
   */
  void downloadFileCallback(api::Request *request_ptr);  
};

/* -------------------------------------------------------------------------- */

} // namespace mke::rt
} // namespace mke

/* -------------------------------------------------------------------------- */

#endif // _WORKERBASE_H_
