/*
 * ApiError - error should be propagated to client
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar, Jan Heller
 *
 */

/* -------------------------------------------------------------------------- */

// includes

#include "apierror.h"

/* -------------------------------------------------------------------------- */

using namespace mke;
using namespace mke::rt;
using namespace mke::rt::api;

/* -------------------------------------------------------------------------- */

// ApiError class

/* -------------------------------------------------------------------------- */

const char * ApiError::statusToStr(mke::api::MkEReplyStatus status)

{
  switch(status)
    {
    case mke::api::MKE_REPLY_UNDEF:
      return "Undefined status error";
    case mke::api::MKE_REPLY_DATA_WILL_START:
      return "Data will start";
    case mke::api::MKE_REPLY_DATA_WILL_CONTINUE:
      return "Data will continue";
    case mke::api::MKE_REPLY_DATA_STOPPED:
      return "Data has been stopped";
    case mke::api::MKE_REPLY_OK:
      return "OK";
    case mke::api::MKE_REPLY_CLIENT_ERROR:
      return "Client error";
    case mke::api::MKE_REPLY_CLIENT_MALFORMED_REQUEST:
      return "Malformed request";
    case mke::api::MKE_REPLY_CLIENT_ILLEGAL_REQUEST_TYPE:
      return "Illegal request type";
    case mke::api::MKE_REPLY_CLIENT_REQUEST_DOES_NOT_APPLY:
      return "Request does not apply";
    case mke::api::MKE_REPLY_SERVER_ERROR:
      return "Server error";
    case mke::api::MKE_REPLY_SERVER_BUSY:
      return "Server is busy";
    case mke::api::MKE_REPLY_SERVER_INSUFFICIENT_RESOURCES:
      return "Server has insufficient resources";
    case mke::api::MKE_REPLY_SERVER_REQUEST_INTERRUPTED:
      return "Request has been interrupted";
    default:
      return "Unknown status error";
    }
}

/* -------------------------------------------------------------------------- */

const char * ApiError::what() const throw()

{
  oss_.str("");
  oss_ << "MkE API error no.";
  oss_ << mke_status_ << "(" << statusToStr(mke_status_) << "): "
      << runtime_error::what();

  std::cout << oss_.str().c_str() << std::endl;

  return oss_.str().c_str();
}

/* -------------------------------------------------------------------------- */

std::ostringstream      ApiError::oss_;    // formater for what method

/* -------------------------------------------------------------------------- */

// FatalError class

/* -------------------------------------------------------------------------- */

const char * FatalError::statusToStr(mke::api::MkEFatalErrorType error_code)

{
  switch(error_code)
    {
    case mke::api::MKE_FATAL_UNDEF:
      return "Undefined fatal error";
    case mke::api::MKE_FATAL_BADCONFIG:
      return "Invalid configuration";
    case mke::api::MKE_FATAL_DETECTORINIT:
      return "Unable to initialize detector";
    case mke::api::MKE_FATAL_BADCAMERA:
      return "Communication problem with the camera";
    case mke::api::MKE_FATAL_RUNTIME:
      return "Runtime fatal error occured";
    default:
      return "Unknown fatal e=rror";
    }
}

/* -------------------------------------------------------------------------- */

const char * FatalError::what() const throw()

{
  oss_.str("");
  oss_ << "MkE FATAL error no.";
  oss_ << error_code_ << "(" << statusToStr(error_code_) << "): "
      << runtime_error::what();

  std::cout << oss_.str().c_str() << std::endl;

  return oss_.str().c_str();
}

/* -------------------------------------------------------------------------- */

std::ostringstream      FatalError::oss_;    // formater for what method

/* -------------------------------------------------------------------------- */
