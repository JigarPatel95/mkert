/*
 * RequestSink
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#ifndef _API_REQUEST_SINK_H_
#define _API_REQUEST_SINK_H_


namespace mke {
namespace rt {
namespace api {

class Request;

class RequestSink
 {
public:
  virtual ~RequestSink() {}

  virtual void request(Request *request) = 0;   
};

} // namespace api
} // namespace rt
} // namespace mke

#endif // _API_REQUEST_SINK_H_
