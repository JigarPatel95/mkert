/*
 * ApiError - error should be propagated to client
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar, Jan Heller
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef _APIERROR_H_
#define _APIERROR_H_

/* -------------------------------------------------------------------------- */

#include <cassert>
#include <exception>
#include <stdexcept>
#include <iostream>
#include <sstream>

#include "mkeapi.h"

/* -------------------------------------------------------------------------- */

namespace mke {
namespace rt {
namespace api {

/* -------------------------------------------------------------------------- */

/**
 * @brief Error corresponding to API Reply error
 */

class ApiError : public std::runtime_error

{
  mke::api::MkEReplyStatus      mke_status_;   // MMAL status raised the exception
  static std::ostringstream     oss_;           // formater for what method
  
public:
  /**
   * @brief Constructor
   * 
   * @param mke_reply_status    error code defined by API Reply Status
   * @param desc                detailed reason if available
   */
  ApiError(mke::api::MkEReplyStatus mke_reply_status, const char* desc = "")
  : std::runtime_error(desc), mke_status_(mke_reply_status)
  {
    // only use for errors
    
    assert(mke_reply_status >= 400);
  };
  
  /**
   * @brief Destructor
   */
  virtual ~ApiError()
  {};
  
  /**
   * @brief Get API Reply status
   */
  inline mke::api::MkEReplyStatus getStatusCode() const { return mke_status_; };
  
  /**
   * @brief Description of error
   */
  virtual const char* what() const throw();

  /**
   * @brief Convert MMAL_STATUS to const char*
   */
  static const char * statusToStr(mke::api::MkEReplyStatus status);
};

/* -------------------------------------------------------------------------- */

/**
 * @brief Error corresponding to FatalError
 */

class FatalError : public std::runtime_error

{
  mke::api::MkEFatalErrorType   error_code_;   // MMAL status raised the exception
  static std::ostringstream     oss_;           // formater for what method
  
public:
  /**
   * @brief Constructor
   * 
   * @param mke_reply_status    error code defined by API Reply Status
   * @param desc                detailed reason if available
   */
  FatalError(mke::api::MkEFatalErrorType error_code, const char* desc = "")
  : std::runtime_error(desc), error_code_(error_code)
  {
  };
  
  /**
   * @brief Destructor
   */
  virtual ~FatalError()
  {};
  
  /**
   * @brief Get API Reply status
   */
  inline mke::api::MkEFatalErrorType getErrorCode() const { return error_code_; };
  
  /**
   * @brief Description of error
   */
  virtual const char* what() const throw();

  /**
   * @brief Convert MMAL_STATUS to const char*
   */
  static const char * statusToStr(mke::api::MkEFatalErrorType fatal_error);
};

/* -------------------------------------------------------------------------- */

}   // end of namespace mke::rt::api
}   // end of namespace mke::rt
}   // end of namespace mke

/* -------------------------------------------------------------------------- */

#endif // _APIERROR_H_

/* -------------------------------------------------------------------------- */
