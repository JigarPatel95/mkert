#ifndef _API_REQUEST_PARSER_H_
#define _API_REQUEST_PARSER_H_

#include <cstddef>
#include <tuple>

#include "mkeapi.h"
#include "rt/api/request.h"

#define REQUEST_MAX_PAYLOAD_SIZE 64*1024*1024

namespace mke {
namespace rt {
namespace api {
  
class RequestParser {
private:

  enum State {
    STATE_NEED_M,
    STATE_NEED_K,
    STATE_NEED_E,
    STATE_NEED_R,
    STATE_NEED_Q,
    STATE_NEED_0_1,
    STATE_NEED_0_2,
    STATE_NEED_1,
    STATE_NEED_HEADER_DATA,
    STATE_NEED_PAYLOAD_DATA,
    STATE_DONE,
  };
  
public:
  
  enum Result {
    PARSER_OK,
    PARSER_ERROR,
    PARSER_NEEDMOREDATA,
  };
  
  void reset() {
    state_ = STATE_NEED_M;
    request_header_size_ = Request::getRestHeaderSize();
  }
  
  template <typename InputIterator>
  std::tuple<Result, InputIterator> parse(Request &request,
    InputIterator begin, InputIterator end) 
  {
    while (begin != end)
      {
        Result result = consume(request, *begin++);
        
        if (result == PARSER_OK || result == PARSER_ERROR)
          return std::make_tuple(result, begin);
      }
      
    if (state_ != STATE_DONE)
      return std::make_tuple(PARSER_NEEDMOREDATA, begin);
    else
      return std::make_tuple(PARSER_OK, begin);
  }
  
private:
    
  inline Result change_state(Request &request, const uint8_t input, 
                             const uint8_t expected, const State next, const Result ok, const Result fail) {
    if (input == expected)
      {
        state_ = next;
        request.pushToBuffer(expected);
        return ok;
      }
    else
      {
        return fail;
      }
  }
  
  
  Result consume(Request &request, uint8_t input) {
    switch (state_) 
      {
      case STATE_NEED_M:
        return change_state(request, input, 'M', STATE_NEED_K, PARSER_NEEDMOREDATA, PARSER_NEEDMOREDATA);
      case STATE_NEED_K:
        return change_state(request, input, 'K', STATE_NEED_E, PARSER_NEEDMOREDATA, PARSER_ERROR);
      case STATE_NEED_E:
        return change_state(request, input, 'E', STATE_NEED_R, PARSER_NEEDMOREDATA, PARSER_ERROR);
      case STATE_NEED_R:
        return change_state(request, input, 'R', STATE_NEED_Q, PARSER_NEEDMOREDATA, PARSER_ERROR);
      case STATE_NEED_Q:
        return change_state(request, input, 'Q', STATE_NEED_1, PARSER_NEEDMOREDATA, PARSER_ERROR);
      case STATE_NEED_1:
        return change_state(request, input, '1', STATE_NEED_0_1, PARSER_NEEDMOREDATA, PARSER_ERROR);
      case STATE_NEED_0_1:
        return change_state(request, input, '0', STATE_NEED_0_2, PARSER_NEEDMOREDATA, PARSER_ERROR);
      case STATE_NEED_0_2:
        return change_state(request, input, '0', STATE_NEED_HEADER_DATA, PARSER_NEEDMOREDATA, PARSER_ERROR);
      case STATE_NEED_HEADER_DATA:
        request_header_size_--;
        request.pushToBuffer(input);
        
        if (request_header_size_ == 0)
          {
            request_payload_size_ = request.getPayloadSize();
            
            if(request_payload_size_ <= 0)
              {
                state_ = STATE_DONE;
                return PARSER_OK;
              }
            else if(request_payload_size_ > REQUEST_MAX_PAYLOAD_SIZE)
              {
                return PARSER_ERROR;
              }
            else 
              {
                state_ = STATE_NEED_PAYLOAD_DATA;
                request.preparePayloadBuffer();
                return PARSER_NEEDMOREDATA;
              }
          }
        return PARSER_NEEDMOREDATA;      
      case STATE_NEED_PAYLOAD_DATA:        
        request_payload_size_--;
        request.pushToPayload(input);
        if (request_payload_size_ == 0)
          {
            state_ = STATE_DONE;
            return PARSER_OK;
          }
        return PARSER_NEEDMOREDATA;              
      default:
        return PARSER_ERROR;
      }
  }
  
  State state_;
  std::size_t request_header_size_;
  std::size_t request_payload_size_;
};

} // namespace api
} // namespace rt
} // namespace mke

#endif // _API_REQUEST_PARSER_H_
