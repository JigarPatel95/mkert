/*
 * MemoryPool - handes memory allocation and deallocation of API requests and replies.
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#ifndef _API_MEMORY_POOL_H_
#define _API_MEMORY_POOL_H_

#include <vector>
#include <stdlib.h>

#include <boost/lockfree/spsc_queue.hpp>
#ifdef __ANDROID__
#include <boost/align/aligned_alloc.hpp>
#endif

#include "mkert_config.h"
#include "rt/api/request.h"
#include "rt/api/reply.h"


namespace mke {
namespace rt {
namespace api {

typedef unsigned int uint;

// We need to set this at compile time so we can use boost::lockfree::spsc_queue
#ifndef API_MAX_REQUESTS
#define API_MAX_REQUESTS 128
#endif

  /**
   * @brief Implements API memory management
   * 
   * MemoryPool handes memory allocation and deallocation of API requests 
   * and replies. The implementation requires that the memory is requested 
   * *always* in one thread and released *always* in another thread. 
   */
class MemoryPool
 {
  public:
  MemoryPool(const MkertConfig &config, std::size_t max_frame_size) : 
    config_(config),
    max_frame_size_(max_frame_size),
    basic_reply_memory_slab_(nullptr),
    frame_reply_memory_slab_(nullptr),
    stock_reply_memory_slab_(nullptr)
    {}
  
  ~MemoryPool() {
    LOG_INFO << "MemoryPool destruction & dealloc";
    const int & basic_size = config_.getAPIConfig().max_basic_replies;
    const int & frame_size = config_.getAPIConfig().max_frame_replies;
    const int & dynamic_size = config_.getAPIConfig().max_dynamic_replies;
        
    if(request_pool_queue_.read_available() != static_cast<uint>(basic_size + frame_size + dynamic_size))
      LOG_WARN << "Size of MemoryPool Queue[Request]: " << request_pool_queue_.read_available();
    if(basic_reply_pool_queue_.read_available() != static_cast<uint>(basic_size))
      LOG_WARN << "Size of MemoryPool Queue[ReplyBasic]:   " << basic_reply_pool_queue_.read_available();
    if(dynamic_reply_pool_queue_.read_available() != static_cast<uint>(dynamic_size))
      LOG_WARN << "Size of MemoryPool Queue[ReplyDynamic]: " << dynamic_reply_pool_queue_.read_available();
    if(frame_reply_pool_queue_.read_available() != static_cast<uint>(frame_size))
      LOG_WARN << "Size of MemoryPool Queue[ReplyFrame]:   " << frame_reply_pool_queue_.read_available();
    
    if (basic_reply_memory_slab_)
      {        
        deallocReplyBuffer(basic_reply_memory_slab_);
        basic_reply_memory_slab_ = nullptr;
      }
      
    if (frame_reply_memory_slab_)
      {
        deallocReplyBuffer(frame_reply_memory_slab_);
        frame_reply_memory_slab_ = nullptr;
      }      

    if (stock_reply_memory_slab_)
      {
        deallocReplyBuffer(stock_reply_memory_slab_);
        stock_reply_memory_slab_ = nullptr;
      }
  }
      
  /**
   * @brief API Memory pool initialization. Must be called *before* 
   * API dispatcher/worker threads.
   * 
   */
  void init(void) {
    const MkertConfig::APIConfig & api_config = config_.getAPIConfig();
    
    if (api_config.max_basic_replies < 1)
      LOG_FATAL << "Maximum number of basic replies cannot be less than 1";
    
    if (api_config.max_frame_replies < 1)
      LOG_FATAL << "Maximum number of frame replies cannot be less than 1";    

    if (api_config.max_dynamic_replies < 1)
      LOG_FATAL << "Maximum number of dynamic replies cannot be less than 1";    

    int max_requests = api_config.max_basic_replies + 
                       api_config.max_frame_replies +
                       api_config.max_dynamic_replies;
    
    LOG_INFO << "API memory pool initialization" <<
      ", max_requests = " << max_requests <<
      ", max_frame_size = " << max_frame_size_;
    
    if (max_requests > API_MAX_REQUESTS)
      LOG_FATAL << "Maximum number of API requests cannot be more than " << API_MAX_REQUESTS;
    
    initRequestPool(max_requests);
    // TODO max frame request, max dyn requests
    initReplyPool(api_config.max_basic_replies, api_config.max_dynamic_replies,
                  api_config.max_frame_replies, max_frame_size_);
  }
  
  // Dynamic memory for Replies ===============================================
  
  /**
   * @brief Convenience method to allocate memory buffer for `mke::api::MkEReply`
   * The method guaranteed appropriately aligned memory pointer. 
   * 
   * @param payload_size p_size: Size of the reply data payload. The method 
   * will allocate at least `sizeof(mke::api::MkEReply) + size` bytes.
   * @param num_replies p_num_replies: Number of reply buffers to allocate, e.g.
   * if `size` == 0, the method will allocate `sizeof(mke::api::MkEReply) * num_replies`
   * bytes.
   * @return mke::api::MkEReply*
   */
  static mke::api::MkEReply *allocReplyBuffer(const std::size_t payload_size = 0, const std::size_t num_replies = 1) {
    std::size_t mem_size = num_replies * (sizeof(mke::api::MkEReply) + payload_size);
    
    if (mem_size == 0)
      return nullptr;
       
#ifndef __ANDROID__
    void *mem_buffer = aligned_alloc(4, mem_size);
#else
    void *mem_buffer = boost::alignment::aligned_alloc(4, mem_size);
#endif
    if (mem_buffer)
      std::memset(mem_buffer, 0, mem_size);
    
    return reinterpret_cast<mke::api::MkEReply *>(mem_buffer);
  }
  
  /**
   * @brief Convenience method to deallocate memory allocated by `allocReplyBuffer()`
   * 
   * @param reply_ptr p_reply_ptr: Reply buffer to deallocate 
   */
  static void deallocReplyBuffer(mke::api::MkEReply *reply_ptr) {
    free(reply_ptr);
  }
  
  // Requests =================================================================
  
  /**
   * @brief Allocate `Request` object from the API memory pool. 
   * The call will not lock and will always return.
   * 
   * @param r p_r: Reference to a pointer to `Request`. Set to `nullptr` 
   * if memory is not available, points to a valid `Request` object otherwise
   * @return `true` if memory successfully allocated, `false` otherwise.
   */
  bool request(Request* &r) {
    bool rflag = request_pool_queue_.pop(r);
    
    if (!rflag)
      r = nullptr;
    
    return rflag;
  }
  
  /**
   * @brief Release `Request` object to the API memory pool. 
   * The call will not lock and will always return.
   * 
   * @param r p_r: Reference to a pointer to `Request`. 
   * Set to `nullptr` if memory successfully 
   * deallocated. Note that `Request` object is invalidate regardless of 
   * whether it was sucessfully deallocated or not. 
   * @return `true` if memory successfully deallocated, `false` otherwise.
   */
  bool release(Request* &request) {
    request->release();
    
    bool rflag = request_pool_queue_.push(request);
    
    if (rflag)
      request = nullptr;

    return rflag;
  }
  
  // Replies ==================================================================
  
  void requestStock(Reply* &reply, mke::api::MkEReplyStatus status) {
    if (status == mke::api::MKE_REPLY_SERVER_BUSY)
      reply = &reply_server_busy_;
    else if (status == mke::api::MKE_REPLY_SERVER_INSUFFICIENT_RESOURCES)
      reply = &reply_server_ires_;
    else if (status == mke::api::MKE_REPLY_CLIENT_MALFORMED_REQUEST)
      reply = &reply_client_malreq_;
    else if (status == mke::api::MKE_REPLY_CLIENT_ILLEGAL_REQUEST_TYPE)
      reply = &reply_client_illreq_;
  }
  
  Reply* requestStock(mke::api::MkEReplyStatus status) {
    Reply *reply;
    requestStock(reply, status);
    return reply;
  }
  
  bool request(Reply* &reply, Reply::ReplyBufferType buffer_type) {
    bool rflag = false;
    
    if (buffer_type == Reply::REPLY_BUFFER_BASIC)
      rflag = basic_reply_pool_queue_.pop(reply);
    else if (buffer_type == Reply::REPLY_BUFFER_FRAME)
      rflag = frame_reply_pool_queue_.pop(reply);
    else if (buffer_type == Reply::REPLY_BUFFER_DYNAMIC)
      rflag = dynamic_reply_pool_queue_.pop(reply);
    
    if (!rflag)
      reply = nullptr;
    
    return rflag;    
  }
    
  bool release(Reply * &reply) {
    bool rflag = false;
        
    if (!reply->isManaged())
      return true;
   
    if (reply->getBufferType() == Reply::REPLY_BUFFER_BASIC)
      rflag = basic_reply_pool_queue_.push(reply);
    else if (reply->getBufferType() == Reply::REPLY_BUFFER_FRAME)
      rflag = frame_reply_pool_queue_.push(reply);
    else if (reply->getBufferType() == Reply::REPLY_BUFFER_DYNAMIC)
      {
        deallocReplyBuffer(reply->getBuffer());
        rflag = dynamic_reply_pool_queue_.push(reply);
      }

    if (rflag)
      reply = nullptr;
      
    return rflag;      
  }
  
// Initialization =============================================================
  
private:
  void initRequestPool(const uint max_requests) {
    // Allocate memory for API requests and push into request_pool_queue_
    request_pool_.reserve(max_requests);
    
    Request *request;
    
    // Empty request_pool_queue
    while(request_pool_queue_.pop(request))
      ;
    
    // Push available API requests into queue
    for (std::size_t i = 0; i < max_requests; i++) 
      {
        request_pool_.push_back(Request());
        if (!request_pool_queue_.push(&request_pool_[i]))
          LOG_FATAL << "API memory initialization error";
      }    
  }
  
  void initReplyPool(const std::size_t max_basic_replies, const std::size_t max_dynamic_replies, 
                     const std::size_t max_frame_replies, const std::size_t max_frame_size) {
    
    // Basic replies
    basic_reply_pool_.resize(max_basic_replies, Reply(Reply::REPLY_BUFFER_BASIC, true));
    basic_reply_memory_slab_ = allocReplyBuffer(0, max_basic_replies);
    
    Reply *reply;
    while(basic_reply_pool_queue_.pop(reply))
      ;
    
    for (std::size_t i = 0; i < max_basic_replies; i++) 
      {
        basic_reply_pool_[i].setBuffer(basic_reply_memory_slab_ + i);
        if (!basic_reply_pool_queue_.push(&basic_reply_pool_[i]))
          LOG_FATAL << "API memory initialization error";
      }      
      
    // Frame replies
    frame_reply_pool_.resize(max_frame_replies, Reply(Reply::REPLY_BUFFER_FRAME, true));
    frame_reply_memory_slab_ = allocReplyBuffer(max_frame_size, max_frame_replies);
    
    while(frame_reply_pool_queue_.pop(reply))
      ;    

    for (std::size_t i = 0; i < max_frame_replies; i++) 
      {
        uint8_t *buffer_ptr = reinterpret_cast<uint8_t *>(frame_reply_memory_slab_)
          + (i * (sizeof(mke::api::MkEReply) + max_frame_size));
        frame_reply_pool_[i].setBuffer(reinterpret_cast<mke::api::MkEReply *>(buffer_ptr));
        if (!frame_reply_pool_queue_.push(&frame_reply_pool_[i]))
          LOG_FATAL << "API memory initialization error";
      }      
      
    // Dynamic replies
    dynamic_reply_pool_.resize(max_dynamic_replies, Reply(Reply::REPLY_BUFFER_DYNAMIC, true));
    
    while(dynamic_reply_pool_queue_.pop(reply))
      ;    

    for (std::size_t i = 0; i < max_dynamic_replies; i++) 
      {
        if (!dynamic_reply_pool_queue_.push(&dynamic_reply_pool_[i]))
          LOG_FATAL << "API memory initialization error";
      }      
    
    // Stock replies
    std::size_t num_stock_replies = 5;
    stock_reply_memory_slab_ = allocReplyBuffer(0, num_stock_replies);
    
    reply_server_error_.setup(stock_reply_memory_slab_ + 0, 0, false,
                              mke::api::MKE_REQUEST_UNDEF,
                              mke::api::MKE_REPLY_SERVER_ERROR);    
    reply_server_busy_.setup(stock_reply_memory_slab_ + 1, 0, false,
                              mke::api::MKE_REQUEST_UNDEF,
                              mke::api::MKE_REPLY_SERVER_BUSY);
    reply_server_ires_.setup(stock_reply_memory_slab_ + 2, 0, false,
                             mke::api::MKE_REQUEST_UNDEF,
                             mke::api::MKE_REPLY_SERVER_INSUFFICIENT_RESOURCES);
    reply_client_malreq_.setup(stock_reply_memory_slab_ + 3, 0, false,
                             mke::api::MKE_REQUEST_UNDEF,
                             mke::api::MKE_REPLY_CLIENT_MALFORMED_REQUEST);
    reply_client_illreq_.setup(stock_reply_memory_slab_ + 4, 0, false,
                             mke::api::MKE_REQUEST_UNDEF,
                             mke::api::MKE_REPLY_CLIENT_ILLEGAL_REQUEST_TYPE);    
  }
  
  const MkertConfig & config_;
  std::size_t max_frame_size_;
  
  std::vector<Request> request_pool_;
  boost::lockfree::spsc_queue<Request *, 
    boost::lockfree::capacity<API_MAX_REQUESTS>> request_pool_queue_;
    
  mke::api::MkEReply *basic_reply_memory_slab_;  
  std::vector<Reply> basic_reply_pool_;
  boost::lockfree::spsc_queue<Reply *, 
    boost::lockfree::capacity<API_MAX_REQUESTS>> basic_reply_pool_queue_;
    
  mke::api::MkEReply *frame_reply_memory_slab_;  
  std::vector<Reply> frame_reply_pool_;
  boost::lockfree::spsc_queue<Reply *, 
    boost::lockfree::capacity<API_MAX_REQUESTS>> frame_reply_pool_queue_;    
    
  std::vector<Reply> dynamic_reply_pool_;
  boost::lockfree::spsc_queue<Reply *, 
    boost::lockfree::capacity<API_MAX_REQUESTS>> dynamic_reply_pool_queue_;        
    
  mke::api::MkEReply *stock_reply_memory_slab_;  
  Reply reply_server_error_;
  Reply reply_server_busy_;
  Reply reply_server_ires_;
  Reply reply_client_malreq_;
  Reply reply_client_illreq_;
};

} // namespace api
} // namespace rt
} // namespace mke

#endif // _API_MEMORY_POOL_H_
