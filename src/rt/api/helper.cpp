/*
 * ApiHelper - to make easy work with pool and resolver
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#include "helper.h"
#include "reply.h"

/* -------------------------------------------------------------------------- */

using namespace mke;
using namespace mke::rt;
using namespace mke::rt::api;

/* -------------------------------------------------------------------------- */

Reply * ApiHelper::prepareBasicReply(Request * request_ptr,
                                             mke::api::MkEReplyStatus status)

{
  Reply * reply_ptr;

  if (!api_pool.request(reply_ptr, Reply::REPLY_BUFFER_BASIC))
    throw ApiError(mke::api::MKE_REPLY_SERVER_INSUFFICIENT_RESOURCES);

  reply_ptr->setup(request_ptr, status);
  
  return reply_ptr;
}

/* -------------------------------------------------------------------------- */

Reply * ApiHelper::prepareDynamicReply(Request * request_ptr,  size_t buff_size, 
                                       mke::api::MkEReplyStatus status)
{
  Reply *reply_ptr;

  mke::api::MkEReply * reply_buff = MemoryPool::allocReplyBuffer(buff_size, 1);
  if (!reply_buff || !api_pool.request(reply_ptr, Reply::REPLY_BUFFER_DYNAMIC))
    {
      if (reply_buff)
        MemoryPool::deallocReplyBuffer(reply_buff);
      
      throw ApiError(mke::api::MKE_REPLY_SERVER_INSUFFICIENT_RESOURCES);
    }

  reply_ptr->setBuffer(reply_buff, buff_size);
  reply_ptr->setup(request_ptr, status, buff_size);
  
  return reply_ptr;
}

/* -------------------------------------------------------------------------- */
