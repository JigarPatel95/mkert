/*
 * ApiHelper - to make easy work with pool and resolver
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef _APIHELPER_H_
#define _APIHELPER_H_

/* -------------------------------------------------------------------------- */

#include "resolver.h"
#include "memory_pool.h"

/* -------------------------------------------------------------------------- */

namespace mke {
namespace rt {
namespace api {

/* -------------------------------------------------------------------------- */

struct ApiHelper
{
  api::MemoryPool       &   api_pool;          // api pool given by worker
  api::Resolver         &   api_resolver;      // api resolver given by worker
  
  
  ApiHelper(api::MemoryPool & pool, api::Resolver & resolver) :
    api_pool(pool),
    api_resolver(resolver)
  {};
  
  /**
   * @brief prepare dynamic reply with allocated buffer
   */
  api::Reply * prepareDynamicReply(api::Request * request_ptr, size_t buff_size, 
                      mke::api::MkEReplyStatus status = mke::api::MKE_REPLY_OK);
  
  /**
   * @brief prepare basic reply with allocated buffer
   */
  api::Reply * prepareBasicReply(api::Request * request_ptr,
                      mke::api::MkEReplyStatus status = mke::api::MKE_REPLY_OK);
};

/* -------------------------------------------------------------------------- */

}   // end of namespace mke::rt::api
}   // end of namespace mke::rt
}   // end of namespace mke

/* -------------------------------------------------------------------------- */

#endif

/* -------------------------------------------------------------------------- */
