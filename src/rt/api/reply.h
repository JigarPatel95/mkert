/*
 * Reply - Represents `mke::api::MkEReply` within the MkE API runtime.
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#ifndef _API_REPLY_H_
#define _API_REPLY_H_

#include <cstring>
#include <algorithm>

#include "util/logging.h"
#include "mkeapi.h"

#include "request.h"

namespace mke {
namespace rt {
namespace api {

  
/**
 * @brief Represents `mke::api::MkEReply` within the runtime
 * 
 * `Reply` represents `mke::api::MkEReply` within the MkE API runtime.
 *  During the execution of the runtime, no new `Reply` objects are created,
 *  only borrowed (requested) from the `MemoryPool`. Further, unlike `Request`,
 *  `Reply` does not hold the underlying memory buffer, merely a pointer to it.
 *  It is the responsibility of the user to provide memory buffer that is guaranteed
 *  to exist for the appropriate time. When requested through `MemoryPool`, 
 *  the  memory buffer is handled automatically with the exception of 
 *  `Reply::REPLY_DYNAMIC_BUFFER`.
 *  
 */
class Reply {
public:
  /** Buffer type distinction used by `MemoryPool` */
  enum ReplyBufferType {
    REPLY_BUFFER_BASIC,
    REPLY_BUFFER_FRAME,
    REPLY_BUFFER_DYNAMIC
  };

private:
    /** Pointer to the underlying memory buffer. Must be set via `setBuffer()` */
    mke::api::MkEReply *reply_ptr_;
    /** Size of the underlying memory buffer. Must be set via `setBuffer()`.
     * Used by  `ReplySink` to defermine the size of the outgoing data */    
    std::size_t buffer_size_;
    
    bool is_managed_;
    ReplyBufferType buffer_type_; 
    
  static void uint2code4(const unsigned int val, char *code) {
    unsigned int s = std::min((unsigned int) val, 9999u); 
    
    char ss[5];
    std::sprintf(ss, "%04d", s);
    std::memcpy(code, ss, 4);
  }
  
public:  
 // ==========================================================================  
/** @name Construction
 */ 
///@{    
  
 // Reply() : reply_ptr_(nullptr), is_managed_(false), buffer_type_(REPLY_BUFFER_BASIC) 
 //    {}
   
   Reply(ReplyBufferType buffer_type = REPLY_BUFFER_BASIC, bool is_managed = false) : 
     reply_ptr_(nullptr), is_managed_(is_managed), buffer_type_(buffer_type) 
     {}
   
///@}   
  
 // ==========================================================================  
/** @name Buffer
 *  MkE API request memory buffer setup
 */ 
///@{    

/**
 * @brief Sets the pointer to the underlying memory buffer. 
 * *BEWARE*, the buffer must be _at least_ `sizeof(mke::api::MkEReply)` 
 * in size and _4-bytes aligned_.
 * 
 * @param reply_ptr :
 * @param payload_size :
 */
  inline void setBuffer(mke::api::MkEReply *reply_ptr, const std::size_t payload_size = 0) {
    static char magik[] = "MKERP100"; 
    
    reply_ptr_ = reply_ptr;
    std::memcpy(reply_ptr_->magik, magik, 8);
    
    buffer_size_ = sizeof(mke::api::MkEReply) + payload_size;
  }
  
  inline mke::api::MkEReply* getBuffer(void) const {
    return reply_ptr_;
  }
  
  inline void * getPayloadBuffer(void) const {
    return reinterpret_cast<void *>(reply_ptr_ + 1);
  }
  
  template<typename T> T* getParamsBuffer(void) {
    return reinterpret_cast<T *>(&(reply_ptr_->params));
  }
  
  /**
   * @brief Returns the number of bytes occupied by the current reply buffer
   * 
   * This value is constant for replies with buffers of type `REPLY_BUFFER_BASIC`
   * and variable others. The value of `mke::api::MkEReply::num_bytes` must be
   * set to a valid value (e.g. via `setNumBytes`, `setup`, etc.) 
   *  prior the call of this method. 
   * 
   * @return std::size_t
   */
  inline std::size_t getSize(void) const {
    return buffer_size_;
  }
  
///@}    
  
// ==========================================================================  
/** @name Memory Pool
 *  Tracking information used by `MemoryPool`
 */
///@{  


  inline bool isManaged(void) const {
    return is_managed_;
  }

  inline void setManaged(bool is_managed) {
    is_managed_ = is_managed;
  }
  
  inline ReplyBufferType getBufferType(void) const {
    return buffer_type_;
  }
  
  inline void setBufferType(ReplyBufferType buffer_type) {
    buffer_type_ = buffer_type;
  }
  
///@}  

// ==========================================================================  
/** @name Reply contents setup
 *  These methods set various fields of the MkE API request.
 *  Note that the reply memory buffer must be set via `setBuffer()` before using
 *  these methods, otherwise the behavior is undefined
 */
///@{  
  
  /**
   * @brief Sets `mke::api::MkEReply` field `mke::api::MkEReply::status`.
   * 
   * @param status p_status:...
   */
  inline void setStatus(mke::api::MkEReplyStatus status) {
    uint2code4((unsigned int) status, reply_ptr_->status);
  }
  
  inline void setReqId(uint32_t reqid) {
    reply_ptr_->reqid = reqid;
  }
  
  inline void setNumBytes(uint32_t num_bytes) {
    reply_ptr_->num_bytes = num_bytes;
    buffer_size_ = sizeof(mke::api::MkEReply) + num_bytes;
  }
  
  inline void setType(mke::api::MkERequestType type) {
    uint2code4((unsigned int) type, reply_ptr_->type);
  }    
  
  inline void setParams(void *params_ptr = NULL) {
    if (params_ptr)
      std::memcpy(&(reply_ptr_->params), params_ptr, sizeof(mke::api::MkEReply_params));
    else
      std::memset(&(reply_ptr_->params), 0, sizeof(mke::api::MkEReply_params));      
  }
    
  /**
   * @brief Convenience method for bulk `mke::api::MkEReply` fields setup
   * 
   * @param type p_type:...
   * @param status p_status:...
   * @param reqid p_reqid:...
   * @param num_bytes p_num_bytes:...
   */
  void setup(const mke::api::MkERequestType type,
             const mke::api::MkEReplyStatus status,
             const uint32_t reqid = 0,
             const uint32_t num_bytes = 0) 
  {
    setType(type);
    setStatus(status);
    setReqId(reqid);
    setNumBytes(num_bytes);
    setParams();  
  }
  
  /**
   * @brief Convenience method for bulk `mke::api::MkEReply` fields setup
   * 
   * @param reply_ptr p_reply_ptr:...
   * @param buffer_size p_buffer_size:...
   * @param is_managed p_is_managed:...
   * @param type p_type:...
   * @param status p_status:...
   * @param reqid p_reqid:...
   * @param num_bytes p_num_bytes:...
   */
  void setup(mke::api::MkEReply *reply_ptr,
             const std::size_t buffer_size,
             const bool is_managed,
             const mke::api::MkERequestType type,
             const mke::api::MkEReplyStatus status,
             const uint32_t reqid = 0,
             const uint32_t num_bytes = 0) 
  {
    setBuffer(reply_ptr, buffer_size);
    setManaged(is_managed);
    setType(type);
    setStatus(status);
    setReqId(reqid);
    setNumBytes(num_bytes);
    setParams();  
  }  
  
  /**
   * @brief Convenience method for bulk `mke::api::MkEReply` fields 
   * setup as a reply to `request`
   * 
   * @param request p_request:...
   * @param status p_status:...
   * @param num_bytes p_num_bytes:...
   */
  void setup(const Request* request, 
             const mke::api::MkEReplyStatus status = mke::api::MKE_REPLY_OK,
             const uint32_t num_bytes = 0) 
  {
    request->getType(reply_ptr_->type);
    setStatus(status);
    setReqId(request->getId());
    setNumBytes(num_bytes);
    setParams();
  }
};

///@}  

} // namespace api
} // namespace rt
} // namespace mke

#endif
