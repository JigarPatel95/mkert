/*
 * ReplySink
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#ifndef _API_REPLY_SINK_H_
#define _API_REPLY_SINK_H_


namespace mke {
namespace rt {
namespace api {

class Reply;

class ReplySink
 {
public:
  virtual ~ReplySink() {}

  virtual void reply(Reply *reply) = 0;
  virtual uint64_t getID(void) = 0;
};

} // namespace api
} // namespace rt
} // namespace mke

#endif // _API_REPLY_SINK_H_
