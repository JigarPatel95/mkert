/*
 * Queue - Implements a thread safe queue of API requests represented by `Request`. 
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#ifndef _API_QUEUE_H_
#define _API_QUEUE_H_

#include <vector>

#include <boost/lockfree/spsc_queue.hpp>

#include "mkert_config.h"
#include "rt/api/request.h"

namespace mke {
namespace rt {
namespace api {
  
// We need to set this at compile time so we can use boost::lockfree::spsc_queue
#ifndef API_MAX_REQUESTS
#define API_MAX_REQUESTS 128
#endif

  /**
   * @brief Implements a thread safe queue of API requests represented by `Request`.
   * 
   * Implements a thread safe queue of API requests represented by `Request`. It is expected that 
   * it will be used in the single producer / single consumer scenario.
   */
class Queue
 {
  public:
  Queue(const MkertConfig &config) : 
    config_(config)
    {}
  
  /**
   * @brief API queue initialization. Must be called *before* the API dispatcher/worker threads
   * 
   */
  void init(void) {
    const MkertConfig::APIConfig & api_config = config_.getAPIConfig();
    
    int max_requests = api_config.max_basic_replies + 
                       api_config.max_frame_replies +
                       api_config.max_dynamic_replies;
    
    if (max_requests < 1)
      LOG_FATAL << "Maximum number of API requests cannot be less than 1";

    if (max_requests > API_MAX_REQUESTS)
      LOG_FATAL << "Maximum number of API requests cannot be more than " << API_MAX_REQUESTS;
          
    // Empty api_call_queue_
    Request *request;
    while (api_calls_queue_.pop(request))
      ;
  }

  /**
   * @brief Pushes an API request into the API queue. 
   * Must be *always* called from a single producer thread.
   * 
   * @param r Reference to an API request to be pushed. Set to `nullptr` if pushed successfully.
   * @return `true` if sucessfully pushed into the queue, `false` otherwise.
   */
  bool push(Request* &r) {
    bool rflag = api_calls_queue_.push(r);
    
    if (rflag)
      r = nullptr;

    return rflag;    
  }
  
  /**
   * @brief Pops an API request from the API queue. 
   * Must be *always* called from a single consumer thread.
   * 
   * @param r If poped successfully, `r` points to an API request `Request`. Set to `nullptr` otherwise
   * @return `true` if sucessfully poped from the queue, `false` otherwise.
   */
  bool pop(Request* &r) {    
    bool rflag = api_calls_queue_.pop(r);
    
    if (!rflag)
      r = nullptr;
    
    return rflag;
  }
  
private:
  const MkertConfig & config_;
  
  boost::lockfree::spsc_queue<Request *, boost::lockfree::capacity<API_MAX_REQUESTS>> api_calls_queue_;      
};

} // namespace api
} // namespace rt
} // namespace mke

#endif // _API_QUEUE_H_
