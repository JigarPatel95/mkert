/*
 * Request - Represents `mke::api::MkERequest` within the MkE API runtime.
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#ifndef _API_REQUEST_H_
#define _API_REQUEST_H_

#include <string.h>
#include <stdlib.h>
#include <malloc.h>

#include "util/logging.h"
#include "mkeapi.h"
#ifdef __ANDROID__
#include <boost/align/aligned_alloc.hpp>
#endif

#ifdef __MINGW32__
//#define aligned_alloc _aligned_malloc
//#define aligned_free  _aligned_free
#define aligned_alloc(align, size) malloc(size)
#define aligned_free free
#endif //MINGW

namespace mke {
namespace rt {
namespace api {
 
class ReplySink;

/**
 * @brief Represents `mke::api::MkERequest` within the runtime
 * 
 * `Request` represents `mke::api::MkERequest` within the MkE API runtime.
 *  During the execution of the runtime, no new `Request` objects are created,
 *  only borrowed (requested) from the `MemoryPool`. After an `Request` object
 *  is successfully requested from `MemoryPool`, `ReplySink` *must* be set via
 *  `init()`. This allows the responder to this API request to respond
 *  via the appropriate data channel. To allow for correct management and 
 *  destruction of the data channel (implemented using `boost::asio`), 
 *  `release()` must be called prior returning the `Request` object into 
 *  the API `MemoryPool`.
 */
class Request {
public:

  enum InternalRequestType {
    INT_REQUEST_UNDEF,
    INT_REQUEST_REPLY_SINK_INVALID,
    INT_REQUEST_TERMINATE,
  };
    
private:

  union MkERequestData {
    mke::api::MkERequest request;
    uint8_t bytes[sizeof(mke::api::MkERequest)];
  } __attribute__((packed,aligned(8)));
    
  int data_size_;
  bool is_internal_;
  InternalRequestType internal_request_type_;
  
  MkERequestData        request_;
  uint8_t             * payload_buff_;                  // pointer to payload
  uint8_t             * payload_ptr_;                   // pointer to current written position
  std::shared_ptr<ReplySink> reply_sink_;
  
  // forbidden to assign const Request
  Request& operator=(const Request& rhs) = delete;
  
public:
  
// ==========================================================================  
/** @name Construction
 */
///@{    
  
  Request() {
    resetBuffer();
    reply_sink_ = nullptr;
    is_internal_ = false;
    internal_request_type_ = INT_REQUEST_UNDEF;
    payload_buff_ = nullptr;
    payload_ptr_ = nullptr;
  }
  
  // assignment operator

  Request& operator=(Request& rhs) {
    assert(payload_buff_ == nullptr);
    
    this->data_size_            = rhs.data_size_;
    this->is_internal_          = rhs.is_internal_;
    this->internal_request_type_= rhs.internal_request_type_;
    this->request_              = rhs.request_;
    this->setReplySink(rhs.reply_sink_);

    // if there is a payload move responsibility to new object
    
    this->payload_buff_         = rhs.payload_buff_;
    this->payload_ptr_          = rhs.payload_ptr_;
    rhs.payload_buff_ = nullptr;
    rhs.payload_ptr_ = nullptr;
    
    return *this;
  }
  
  ~Request() {
    release();
  }
  
  inline void init(std::shared_ptr<ReplySink> reply_sink) {
    resetBuffer();
    setReplySink(reply_sink);
    is_internal_ = false;
    internal_request_type_ = INT_REQUEST_UNDEF;
    payload_buff_ = nullptr;
    payload_ptr_ = nullptr;
  }
  
  /**
   * @brief Releases resources. *Must* be called before releasing the object
   * itself into the API memory pool
   * 
   */
  inline void release() {
    reply_sink_ = nullptr;
    
    // release memory if allocated
    
    if(payload_buff_)
      {
        free(payload_buff_);
        payload_buff_ = nullptr;
        
        LOG_INFO << "Releasing PAYLOAD";
      }
  }
  
///@}  

// ==========================================================================
/** @name ReplySink
 *  Used by the responder to this request (`Resolver`) to route the reply 
 *  to the correct client.
 */
///@{   
  
  /**
   * @brief Sets `ReplySink` to be used by the responder to this request
   * 
   * @param reply_sink p_reply_sink
   */
  inline void setReplySink(std::shared_ptr<ReplySink> &reply_sink) {
    reply_sink_ = reply_sink;
  }
    
  /**
   * @brief Returns `ReplySink` to be used by the responder to this request
   * 
   * @return std::shared_ptr< mke::rt::api::ReplySink >
   */
  inline std::shared_ptr<ReplySink> getReplySink(void) {
    return reply_sink_;
  }
  
///@}  

// ==========================================================================
/** @name Buffer
 *  Used by `RequestParser` to push bytes from the underlying
 *  network buffer.
 */
///@{  

  /**
   * @brief Resets the internal buffer pointer 
   * 
   */
  inline void resetBuffer(void) {
    data_size_ = 0;
    payload_ptr_ = nullptr;
  }
  
  /**
   * @brief Pushed one byte at the end the request buffer and increments the internal
   * buffer pointer
   * 
   * @param datum p_datum: byte to be pushed at the end of the request buffer
   */
  inline void pushToBuffer(uint8_t datum) {
    request_.bytes[data_size_++] = datum;
  }
    
  /**
   * @brief Pushed one byte at the end the request payload buffer and increments the internal
   * buffer pointer
   * 
   * @param datum p_datum: byte to be pushed at the end of the request buffer
   */
  inline void pushToPayload(uint8_t datum) {
    *payload_ptr_++ = datum;
  }
  
  /**
   * 
   */
  inline size_t preparePayloadBuffer() {
    assert(payload_buff_ == nullptr);
    
    size_t payload_size = getPayloadSize();
    
    // should I prepare memmory
    
    if(payload_size > 0)
      {
#ifndef __ANDROID__
        payload_buff_ = reinterpret_cast<uint8_t *>(aligned_alloc(4, payload_size));
#else
        payload_buff_ = reinterpret_cast<uint8_t *>(boost::alignment::aligned_alloc(4, payload_size));
#endif
        if (payload_buff_)
          std::memset(reinterpret_cast<void *>(payload_buff_), 0, payload_size);

        LOG_INFO << "Preparing PAYLOAD of size " << payload_size; 
        payload_ptr_ = payload_buff_;
        
        return payload_size;
      }
    
    return 0;
  }
  
///@}

// ==========================================================================
/** @name Internal Request
 *  Used by API to signalize request that origated within the runtime itself
 */
///@{

  inline void setInternalType(InternalRequestType itype = INT_REQUEST_UNDEF) 
  {
    setType(mke::api::MKE_REQUEST_INTERNAL);
    is_internal_ = true;
    internal_request_type_ = itype;        
  }
  
  inline bool isInternal(void) const {
    return is_internal_;
  }
  
  InternalRequestType getInternalType(void) const {
    return internal_request_type_;
  }

///@}

// ==========================================================================
/** @name Info
 *  These methods return various information about the MkE API Request.
 *  Note that the return values may not be well defined if the underlying buffer
 *  data is invalid (e.g., the request was only partially read from the data stream).
 */
///@{  
  
  
  
  /**
   * @brief Returns the value of `mke::api::MkERequest::type` as 
   * `mke::api::MkERequestType`
   * 
   * @return mke::api::MkERequestType
   */
  inline mke::api::MkERequestType getType(void) const {
     const char *t = request_.request.type;
     int  type = int(t[3] - '0') * 1   + int(t[2] - '0') * 10 +
                 int(t[1] - '0') * 100 + int(t[0] - '0') * 1000;
     return mke::api::MkERequestType(type);
  }
  
  /**
   * @brief Copies the  value of `MkERequest::type` into `type`.
   * 
   * @param type p_type: pointer to a memory of at least 4 bytes 
   */
  inline void getType(char* type) const {
    std::memcpy(type, request_.request.type, 4); 
  }

  /**
   * @brief Copies the  value of `type` into `MkERequest::type`.
   * 
   * @param type p_type: pointer to a memory of at least 4 bytes 
   */  
  inline void setType(mke::api::MkERequestType type) {
    
    assert(!isInternal());
    
    unsigned int s = std::min((unsigned int) type, 9999u); 
    
    char ss[5];
    std::sprintf(ss, "%04d", s);
    std::memcpy(request_.request.type, ss, 4);
  }  

  /**
   * @brief Returns the value of `mke::api::MkERequest::reqid` field. 
   *  Note that the return value may not be well defined if the underlying buffer
   *  data is invalid (e.g., the request was only partially read from the data stream).
   * 
   * @return uint32_t
   */
  inline uint32_t getId(void) const {
    return request_.request.reqid;
  }
  
  /**
   * @brief Return a constant pointer to the part of the buffer holding the 
   * `mke::api::MkERequest::MkERequest_Params` union
   * 
   * @return const mke::api::MkERequest_Params*
   */
  template<typename T> T* getParamsBuffer(void) {
    return reinterpret_cast<T *>(&request_.request.params);
  }
  
  /**
   * @brief Returns the size of the MkE API request in bytes 
   * minus the length of the magik string
   * 
   * @return std::size_t
   */
  inline static std::size_t getRestHeaderSize(void) {
    return sizeof(mke::api::MkERequest) - sizeof(mke::api::MkERequest::magik);
  }
  
  /**
   * @brief Returns the size of the payload of the MkE API request in bytes 
   * 
   * @return std::size_t
   */
  inline std::size_t getPayloadSize(void) {
    mke::api::MkERequestType req_type = getType();

    if(req_type < mke::api::MKE_REQUEST_DYNAMIC_OFFSET || 
                                  req_type > mke::api::MKE_REQUEST_DYNAMIC_LAST)
      return 0;         // no payload
     
    return getParamsBuffer<mke::api::MkERequest_Dynamic>()->payload_size;
  }
  
  /**
   * @brief Returns a pointer to the payload buffer
   * 
   * @return payload buffer pointer (nullptr = no payload)
   */
  inline const uint8_t * getPayloadBuffer() { return payload_buff_; };
  
  /**
   * @brief Returns the size of the MkE API request structure `mke::api::MkERequest`
   * 
   * @return std::size_t
   */
  inline static std::size_t getSize(void) {
    return sizeof(mke::api::MkERequest);
  }  
  
///@}
};    
  

} // namespace api
} // namespace rt
} // namespace mke

#endif
