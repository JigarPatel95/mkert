/*
 * Resolver - Convenience class for simplying API `Reply`/`Request` handling in the worker thread
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#ifndef _APIRESOLVER_H_
#define _APIRESOLVER_H_

#include <stddef.h>
#include <functional>
#include <unordered_map>

#include "util/logging.h"

#include "mkeapi.h"

#include "mkert_config.h"
#include "rt/api/memory_pool.h"
#include "rt/api/queue.h"
#include "rt/api/reply_sink.h"
#include "rt/api/request.h"
#include "rt/api/reply.h"
#include "apierror.h"

  
namespace mke {
namespace rt {
namespace api {

typedef std::function<void(Request *)> APICallbackFunction;  
  
/**
 * @brief Convenience class for simplying API `Reply`/`Request` handling in the worker thread
 * 
 */
class Resolver
{
private:
    
  void callbackNotImplemented(Request *request_ptr) {
    reply(mke::api::MKE_REPLY_SERVER_ERROR, request_ptr);
 }
  
public:
  void registerCallback(const mke::api::MkERequestType type, const APICallbackFunction &fnc) {
    
    assert(api_callbacks_.find(type) == api_callbacks_.end());
    
    api_callbacks_[type] = fnc;
  }
  
  void unregisterCallback(const mke::api::MkERequestType type) {
    
    assert(api_callbacks_.find(type) != api_callbacks_.end());
    
    api_callbacks_.erase(type);
  }
  
  void setFatal(mke::api::MkEFatalErrorType err_code) {
    
    assert(!fatal_mode_);
    
    fatal_mode_ = true;
    fatal_err_ = err_code;
  }
  
  void resetFatal() {
    
    fatal_mode_ = false;
  }
  
  Resolver(const MkertConfig & config, api::MemoryPool & api_pool, api::Queue &api_queue) :
    config_(config),
    api_pool_(api_pool),
    api_queue_(api_queue),
    curr_state_(mke::api::MKE_STATE_IDLE),
    fatal_mode_(false)
  {
    api_callbacks_.clear();
  }
  
  // ==========================================================================
/** @name Runtime
 *  Resolver runtime methods
 */
///@{ 

  void init(void) {
    // Register default callbacks
//    APICallbackFunction not_impl = std::bind(&Resolver::callbackNotImplemented, this, std::placeholders::_1);
//    registerCallback(mke::api::MKE_REQUEST_PING, not_impl);
  }
  
  inline void release(Request* &request_ptr) {
    api_pool_.release(request_ptr);
  }
  
  inline void reply(Reply* &reply_ptr, Request* &request_ptr, bool do_release = true) {
    if (!reply_ptr)
      {
        //This should never happen
        api_pool_.requestStock(reply_ptr, mke::api::MKE_REPLY_SERVER_ERROR);
      }
        
     if (request_ptr->getReplySink())
       request_ptr->getReplySink()->reply(reply_ptr);
        
     if (do_release)
       release(request_ptr);
  }

  inline void reply(mke::api::MkEReplyStatus status, Request* &request_ptr, bool do_release = true) {
    api::Reply *reply_ptr;
   
    if (!api_pool_.request(reply_ptr, api::Reply::REPLY_BUFFER_BASIC))
      api_pool_.requestStock(reply_ptr, mke::api::MKE_REPLY_SERVER_INSUFFICIENT_RESOURCES);
    else
      reply_ptr->setup(request_ptr, status);

    reply(reply_ptr, request_ptr, do_release);
  }  
  
  void replyGetState(Request * request_ptr)
  
  {
    api::Reply *reply_ptr;

    if (!api_pool_.request(reply_ptr, api::Reply::REPLY_BUFFER_BASIC))
      {
        api_pool_.requestStock(reply_ptr, mke::api::MKE_REPLY_SERVER_INSUFFICIENT_RESOURCES);
        reply(reply_ptr, request_ptr);
      }
    else
      {
        reply_ptr->setup(request_ptr, mke::api::MKE_REPLY_OK);

        // We have `Reply` object, let's fill it up
        mke::api::MkEReply_State * state_params = 
                        reply_ptr->getParamsBuffer<mke::api::MkEReply_State>();
        state_params->state = (uint32_t) getState();
        
        reply(reply_ptr, request_ptr);
      }
  }
  
  void replyFatalError(Request * request_ptr)
  
  {
    assert(fatal_mode_);
    
    api::Reply *reply_ptr;

    if (!api_pool_.request(reply_ptr, api::Reply::REPLY_BUFFER_BASIC))
      {
        api_pool_.requestStock(reply_ptr, mke::api::MKE_REPLY_SERVER_FATAL_ERROR);
        reply(reply_ptr, request_ptr);
      }
    else
      {
        reply_ptr->setup(request_ptr, mke::api::MKE_REPLY_SERVER_FATAL_ERROR);

        // We have `Reply` object, let's fill it up
        mke::api::MkEReply_ServerFatal * fatal_params = 
                        reply_ptr->getParamsBuffer<mke::api::MkEReply_ServerFatal>();
        fatal_params->err_code = (uint32_t) fatal_err_;
        
        reply(reply_ptr, request_ptr);
      }    
  }
  
  /**
   * @brief This method will dequeue any and all outstanding requests in the MkE request queue.
   * 
   * For every request, a registered callback will be called. The callback is responsible for 
   * replying to the request and releasing the request via `reply()` resp. `release()`.
   * 
   * @return `true` if every outstanding request has been dequeued and responded to via callback,
   * `false` if an exit request has been found in the API queue. This signalizes the caller of 
   * `process()` to release any outstanding resources and to return from the worker thread.
   */
  bool process(void) {
    Request *request_ptr;
    
    while (api_queue_.pop(request_ptr))
      {
        mke::api::MkERequestType rtype = request_ptr->getType();

        LOG_INFO << "Resolver: resolving request " << rtype;
        
        // process inner request
        
        if(rtype == mke::api::MKE_REQUEST_TERMINATE ||      // TERMINATE request  
            (request_ptr->isInternal() 
              && request_ptr->getInternalType() == api::Request::INT_REQUEST_TERMINATE))
          {
                
            // check input parameters
            
            uint32_t method = request_ptr->getParamsBuffer<mke::api::MkERequest_Terminate>()->method;
#ifdef MKE_IS_TERMINATE_METHOD_VALID            
            bool can_exit = config_.getAPIConfig().reserved_mode || request_ptr->isInternal();
            if(MKE_IS_TERMINATE_METHOD_VALID(method, can_exit))
#else
            if(method == mke::api::MKE_TERMINATE_BY_SHUTDOWN || method == mke::api::MKE_TERMINATE_BY_REBOOT
                || method == mke::api::MKE_TERMINATE_BY_EXIT)
#endif                
              {
                // should I reply ?
                
                if(!request_ptr->isInternal())
                  reply(mke::api::MKE_REPLY_OK, request_ptr);
                else
                  release(request_ptr);

                // Silently discard any other outstanding requests
                while (api_queue_.pop(request_ptr))
                  release(request_ptr);

                return false;
              }
            else
              {
                // reply bad request
                
                reply(mke::api::MKE_REPLY_CLIENT_MALFORMED_REQUEST, request_ptr);
                
                continue;
              }
          }
        else if(!fatal_mode_ && rtype == mke::api::MKE_REQUEST_GET_STATE)   // GET_STATE request
          {
            replyGetState(request_ptr);

            continue;
          }

        // propagate request by callbacks
          
        APICallbackFunction callback = api_callbacks_[request_ptr->getType()];
        
        try
          {
            if(callback == nullptr)
              {
                if(!fatal_mode_)
                  reply(mke::api::MKE_REPLY_CLIENT_ILLEGAL_REQUEST_TYPE, request_ptr);
                else
                  replyFatalError(request_ptr);
              }
            else
              callback(request_ptr);
          } catch(ApiError & e) {
            // catched ApiError
            
            reply(e.getStatusCode(), request_ptr);
          } catch(std::exception & e) {
            // fallback exception
            
            LOG_WARN << "std::exception thrown in callback: " << e.what();
            reply(mke::api::MKE_REPLY_SERVER_ERROR, request_ptr);
          }
      }
      
    return true;  
  }
  
  /**
   * @brief Set current state of application.
   * 
   * @param new_state new state according to MkEStateType < See MkEStateType
   */
  inline void setState(mke::api::MkEStateType new_state) { curr_state_ = new_state; }
  
  /**
   * @brief Get current state of application.
   * 
   * @result current state of application according < See MkEStateType
   */
  inline mke::api::MkEStateType getState(void) { return curr_state_; }

///@} 

private:
  const MkertConfig & config_;
  api::MemoryPool & api_pool_;
  api::Queue & api_queue_;  
  std::unordered_map<uint32_t, APICallbackFunction> api_callbacks_;
  mke::api::MkEStateType curr_state_;
  bool fatal_mode_;
  mke::api::MkEFatalErrorType fatal_err_;
  
};

} // namespace api
} // namespace rt
} // namespace mke

#endif // _APIRESOLVER_H_
