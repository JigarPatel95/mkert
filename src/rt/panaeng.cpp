/*
 * PanaEngine - interface for engine that is connection to devices
 * 
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * Copyright (c) 2019, Magik-Eye ltd.
 * author: Ondra Fisar, Jyunji Kondo
 *
 */

/* -------------------------------------------------------------------------- */

#include "panaeng.h"

/* -------------------------------------------------------------------------- */

using namespace mke::rt;
using namespace mke;

/* -------------------------------------------------------------------------- */

PanaEngine::~PanaEngine()

{
    fireLaserPattern(0, 0, 0);
    stopCapture();
}

/* -------------------------------------------------------------------------- */

void PanaEngine::initialize()
{
    // change the exposure settings
    
    PanaCam::setFps(cam_cfg_.fps);
    PanaEngine::setShutterSpeed(cam_cfg_.shutter_speed);
    PanaEngine::setISO(cam_cfg_.iso);
}

/* -------------------------------------------------------------------------- */
  
void PanaEngine::stateChanged(mke::api::MkEStateType state)
{
  if(state == mke::api::MKE_STATE_IDLE)
    {
      if(last_state_ != mke::api::MKE_STATE_DEPTH_SENSOR)
        {
            // change the exposure settings
            
            PanaEngine::setShutterSpeed(cam_cfg_.shutter_speed);
            PanaEngine::setISO(cam_cfg_.iso);
        }

      // stop the lasers
      fireLaserPattern(0, 0, 0);
      stopCapture();
    }
  else
    {

      if(state == mke::api::MKE_STATE_DEPTH_SENSOR)
        {
            // start flickering with lasers
            startSync();
        }
      
      startCapture();
    }
    
  last_state_ = state;
}

/* -------------------------------------------------------------------------- */

void PanaEngine::startSync()
{
    uint16_t seq = 0x0000;
    unsigned int det_num = detector_.getNoDetectors();
    switch(det_num)
    {
      case 1:
          seq = 1;
      break;
      default:
        throw std::runtime_error("Unsupported number of detectors");
    }
    LOG_INFO << "sync " << seq << " at " << det_num;

    fireLaserPattern(seq, 0, 0);
}

/* -------------------------------------------------------------------------- */

void PanaEngine::videoFrameReceived(const dev::Pix8 * data, int w, int h, int memw, uint16_t phase)

{
  uint64_t now;
  mke::dev::CamFrame<dev::Pix8>::setTimerToNow(now);
  
  // lock the queue
  
  std::unique_lock<std::mutex> lock(mtx_frames_);
  
  unsigned int num_det = detector_.getNoDetectors();
  if(num_det == 0)
      num_det = 1;
  frames_received_++;
  
  fpser_.tick();

  if(frames_.size() < max_frames_)
    {
        frames_.push(dev::CamFrame<dev::Pix8>(const_cast<dev::Pix8 *>(data),
                                            w, h, memw, now, frames_received_-1, phase%num_det));
        
        lock.unlock();
        cv_frames_.notify_one();
        return;
    }
  else
    {
        frames_droped_++;
        if(dropped_stamp_ > now || (now-dropped_stamp_) > MKE_ENGINE_DROPLOG_TIMEOUT)
        {
          std::cout << "! Too much frames in Q. Already dropped " 
              << frames_droped_ << " frames." << std::endl;
          dropped_stamp_ = now;
        }
    }

  // return back buffer if not used
  
  returnBackBuffer(data);
}

/* -------------------------------------------------------------------------- */

const dev::CamFrame<dev::Pix8> * PanaEngine::getFrame(uint32_t timeout)

{
  // LOG_INFO << "PanaEngine::getFrame";
  // check for the exception in thread proc
    
  if (err_)
      std::rethrow_exception(err_);
    
  // lock the queue
  
  std::unique_lock<std::mutex> lock(mtx_frames_);
  
  // no new frame available
  
  while(!frames_.size())
    if(cv_frames_.wait_for(lock, std::chrono::milliseconds(timeout)) 
                                                    == std::cv_status::timeout)
      return NULL;

  // get next frame and lock it
  
  dev::CamFrame<dev::Pix8> & frame = frames_.front();
//  lockBuffer(frame.header_);

  // return it
  
  return &frame;
}

/* -------------------------------------------------------------------------- */

void PanaEngine::releaseFrame(const dev::CamFrame<dev::Pix8> * frame)

{
  // lock the queue
  
  std::lock_guard<std::mutex> lock(mtx_frames_);
  
  // check if releasing last getted frame
  
  assert(frame == &frames_.front());

  dev::CamFrame<dev::Pix8> & curr = frames_.front();
  
//  unlockBuffer(curr.header_);
  returnBackBuffer(frame->mem);
  
  // release frame from queue
  frames_.pop();
}

/* -------------------------------------------------------------------------- */

void PanaEngine::fireLaserPattern(int pattern, float strobe_len, float strobe_off) {
  PanaCam::setLaserPattern(pattern);
}

/* -------------------------------------------------------------------------- */

void PanaEngine::setShutterSpeed(uint32_t exposure) {
    if (exposure > 0)
        PanaCam::setShutterSpeed(exposure);
    else
        LOG_WARN << "Unsupported autoexposure for onsemi for now - will be ignored";
}

/* -------------------------------------------------------------------------- */

uint32_t PanaEngine::getShutterSpeed() { 
    return round(PanaCam::getShutterSpeed());
}

/* -------------------------------------------------------------------------- */

uint32_t PanaEngine::getAnalogGain() { 
    return PanaCam::getAnalogGain()*1000; 
}

/* -------------------------------------------------------------------------- */

uint32_t PanaEngine::getDigitalGain() { 
    return PanaCam::getDigitalGain()*1000; 
}

/* -------------------------------------------------------------------------- */

bool PanaEngine::setISO(uint32_t value) { 
    if(value > 0)
        PanaCam::setGain(value/100.0);
    else
        LOG_WARN << "Unsupported auto gain, will be ignored";
    return true;
}

/* -------------------------------------------------------------------------- */

void PanaEngine::getStats(StatsType & stats) const
{
  stats["im_width"] = this->getImageWidth();
  stats["im_height"] = this->getImageHeight();
  stats["cam_temp"] = this->getCamTemp();
}

/* -------------------------------------------------------------------------- */

float PanaEngine::getCamTemp() const
{
  return PanaCam::getCamTemp();
}

/* -------------------------------------------------------------------------- */
