/* 
 * Detector
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#ifndef _DETECTOR_H_
#define _DETECTOR_H_

#include <cstddef>
#include <vector>

#include "mkert_config.h"
#include "mkeapi.h"
#include "mkedd.h"
#include "rt/profiler.h"
#include "rt/statsmng.h"
#include "util/timers.h"

namespace mke {
namespace rt {

  
  /**
   * @brief Implements MkE detector
   * 
   * This class implements the MkE detector. It uses `mke:dd::DotDetector` as its 
   * underlying detector engine and `mke:dd::DataLut` for lookup datalut
   * data management. Further, it can be used to assemble the results of the partial
   * detectors into the data body of MkE Frame `Reply`. This class *IS NOT* thread-safe, 
   * it is the responsibility of the caller to use in a thread-safe fashion.
   */
class Detector : public Profiled, public StatsGenerating {  
  const MkertConfig         &   config_;
  
  // stats timers
  mke::util::StatsThreadTimer frame_asm_timer_;
  
  // mkedd detector
  mke::dd::DepthSensor detector_;

  void addToFrameType1(uint8_t* &frame_buffer, uint32_t &num_bytes, 
                       uint16_t &num_data, uint32_t &crc32, const int det_idx);
  void addToFrameType2(uint8_t* &frame_buffer, uint32_t &num_bytes, 
                       uint16_t &num_data, uint32_t &crc32, const int det_idx);
  void addToFrameTypeReserved1(uint8_t* &frame_buffer, uint32_t &num_bytes, 
                       uint16_t &num_data, uint32_t &crc32, const int det_idx);
  void addToFrameTypeReserved2(uint8_t* &frame_buffer, uint32_t &num_bytes, 
                       uint16_t &num_data, uint32_t &crc32, const int det_idx);
  
public:
  Detector(const MkertConfig &config) : config_(config)
    {}
    
  virtual ~Detector() {};
  
  void init(void); 
  
 // ==========================================================================
/** @name Detector
 *  
 */
///@{

  /** 
   * @brief Returns the number of partial detectors
   * 
   * @return unsigned int
   */
  inline unsigned int getNoDetectors(void) const {
    return detector_.getNoDetectors();
  }
  
  /**
   * @brief Clear the detections from the partial detectors
   * 
   * @param det_idx Partial detector index, if set to -1, 
   * detections from all detectors will be cleared
   */
  void clearDetections(const int det_idx = -1);
  
  /**
   * @brief Returns the laser pattern of the partial detector determined 
   * by the index `det_idx`
   * 
   * @param det_idx Index of the partial detector
   * @return int
   */
  inline int getLaserPattern(const unsigned int det_idx) const {
    return detector_.getLaserPattern(det_idx);
  }

 /**
   * @brief Return current data3d data type
   * 
   * @param det_idx Index of the partial detector
   * @return Data3dType
   */
  inline mke::dd::Data3dType getData3dType(const unsigned int det_idx = 0) { 
    return detector_.getNoDetectors() ? detector_.getData3dType(det_idx) : mke::dd::DD_DATA3D_MM; 
  }
  
  /**
   * @brief Returns the number of detections for given detector 
   * by the index `det_idx`
   * 
   * @param det_idx Index of the partial detector
   * @return int
   */
  inline uint32_t getNoDetections(const unsigned int det_idx) const {
    return detector_.getNoDetections(det_idx);
  }
  
  virtual void setProfile(const ProfileType & profile);

  virtual void getProfile(ProfileType & profile) const;
  
  virtual void validateProfile(const ProfileType & profile) const;
 
  virtual void getStats(StatsType & stats) const;
  
  virtual void resetStats();    

  /**
   * @brief Processes a memory buffer containing the camera frame using a partial detector 
   * 
   * Note that the method does not locks the memory buffer in any way and expect it to contain
   * valid data during the whole execution time.
   * 
   * @param buffer Pointer to a memory buffer containing the camera frame to be processed
   * @param det_idx Index of the partial detector to process the buffer with
   */
  inline unsigned int runDetector(const uint8_t *buffer, const unsigned int det_idx) {
    return detector_.process(det_idx, buffer);
  }

  /**
   * @brief is detector running in dry run mode - no detection
   */
  inline bool isDryRun() const {
    return config_.getDetectorConfig().dry_run;
  }
  
///@}  
  
 // ==========================================================================
/** @name MkE data frame processing
 *  
 */
///@{
  
  /**
   * @brief Returns the maximum size in bytes of a MkE data frame if assembled
   * from detections from all partial detectors.
   * 
   */
  std::size_t getMaxFrameSize(void);
  
 /**
  * @brief Assembles MkE data frame as described by MkE API 1.0
  * 
  * @param buffer Pointer to a memory buffer large enough to hold the
   * maximal size of a MkE data frame. The memory *must be* aligned to 4-bytes.
   * It is assumed that the pointer points to a memory block right after a memory
   * interpreted as `mke::api::MkEReply`.
  * @param num_bytes Will contain the number of bytes of assembled frame
  * @param num_data Will contain the number of detections, i.e., 
  * the number of `MkEFrameType*` structures in the assembled frame
  * @param frame_type `mke::api::MkEFrameType` of the frame to be assembled
  * @param det_idx Partial detector index, if set to -1, the frame will
  * be assembled using the detections from all of partial detectors
  */
  void assembleFrame(void *buffer, uint32_t &num_bytes, uint16_t &num_data, 
                    const mke::api::MkEFrameType frame_type, const int det_idx = -1);

///@}  

};

}   // end of rt
}   // end of mke


#endif // _DETECTOR_H_
