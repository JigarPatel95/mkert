
/*
 * PanaEngine - interface for engine that is connection to devices
 * 
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * Copyright (c) 2019, Magik-Eye ltd.
 * author: Ondra Fisar, Jyunji Kondo
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef _PANAENG_H_
#define _PANAENG_H_

/* -------------------------------------------------------------------------- */

#include <queue>
#include <condition_variable>
#include <boost/circular_buffer.hpp>

#include "engine.h"
#include "detector.h"
#include <dev/panacam.h>
#include <util/fpscnt.h>

/* -------------------------------------------------------------------------- */

namespace mke {
namespace rt {

/* -------------------------------------------------------------------------- */

// camera with queue

class PanaEngine : public Engine, protected dev::PanaCam
{
protected:
  MkertConfig::CameraConfig
                        cam_cfg_;               // shortcut to camera cfg
  MkertConfig::EngineConfig
                        eng_cfg_;               // shortcut to laser cfg

  Detector           &  detector_;              // used detector
                     
  uint64_t              frames_received_;       // number of already received frames
  uint32_t              frames_droped_;         // number of droped frames
  uint64_t              dropped_stamp_;         // last dropped log timestamp  

  uint16_t              max_frames_;
  std::queue<dev::CamFrame<dev::Pix8>, boost::circular_buffer<dev::CamFrame<dev::Pix8> > >
                        frames_;                // queue of unprocessed images
  mutable std::mutex    mtx_frames_;
  std::condition_variable
                        cv_frames_;             // conditional variable for new frame
  mke::api::MkEStateType 
                        last_state_;            // last setted state
                        
  mke::util::FpsCounter fpser_;                 // fps counter

  virtual void videoFrameReceived(const dev::Pix8 * buff, int w, int h, int memw, uint16_t phase);

  void startSync();
  
  float getCamTemp() const;
  
  uint32_t getCamTempRaw() const;
  
public:
  // constructor
  PanaEngine(const MkertConfig & cfg, Detector & detector)
  : Engine(cfg), dev::PanaCam(cfg.getCameraConfig().firmware_image_file,
                                cfg.getCameraConfig().range_mode,
                                cfg.getCameraConfig().use_raw,
                                cfg.getCameraConfig().use_int,
                                cfg.getCameraConfig().num_buffers+1), 
    cam_cfg_(cfg.getCameraConfig()), eng_cfg_(cfg.getEngineConfig()),
    detector_(detector), frames_received_(0), frames_droped_(0), dropped_stamp_(0),
    max_frames_(cfg.getCameraConfig().num_buffers > 2 ? 
                    cfg.getCameraConfig().num_buffers-2 : 0),
    frames_(boost::circular_buffer<dev::CamFrame<dev::Pix8> >(max_frames_)),
    last_state_(mke::api::MKE_STATE_IDLE), fpser_(10,true)
  {};
    
  virtual ~PanaEngine();

  // initialization of devices
  virtual void initialize();

  // get next frame from camera
  virtual const dev::CamFrame<dev::Pix8> * getFrame(uint32_t timout = 0xffffffff);
  
  // release frame
  virtual void releaseFrame(const dev::CamFrame<dev::Pix8> * frame);
  
  // run lasers
  virtual void stateChanged(mke::api::MkEStateType state);
  
  // fire some pattern
  virtual void fireLaserPattern(int pattern, float strobe_len, float strobe_off);
  
  // set shutter speed
  virtual void setShutterSpeed(uint32_t exposure_);
  
  // get shutter speed
  virtual uint32_t getShutterSpeed();
  
  // get analog gain
  virtual uint32_t getAnalogGain();
  
  // get digital gain
  virtual uint32_t getDigitalGain();
  
  // set ISO - combination of analog and digital values
  virtual bool setISO(uint32_t value);
    
  virtual void getStats(StatsType & stats) const;

  virtual void resetStats() {};

  virtual void setProfile(const ProfileType & profile) {};

  virtual void getProfile(ProfileType & profile) const {};

  virtual void validateProfile(const ProfileType&) const {}
  
};

/* -------------------------------------------------------------------------- */

} // end of mke::rt namespace
} // end of mke namespace

/* -------------------------------------------------------------------------- */

#endif // _PANAENG_H_

