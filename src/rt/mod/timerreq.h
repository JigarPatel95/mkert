/*
 * TimerRequest - sending tick packets until predefined number
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar, Jan Heller
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef _TIMERREQ_H_
#define _TIMERREQ_H_

/* -------------------------------------------------------------------------- */

#include "longreq.h"
#include "util/imavg.h"

/* -------------------------------------------------------------------------- */

namespace mke {
namespace rt {
namespace mod {

/* -------------------------------------------------------------------------- */

/**
 * @brief TimerRequest conting down and repling request to client on each count
 */
class TimerRequest : public LongLivedRequest
{
protected:
  int               timer_ticks_;
  
public:  
  /**
   * @brief Contructor
   * 
   * @param request     request that is parent of LongLivedRequest
   */
  TimerRequest(api::ApiHelper * helper, api::Request * request_ptr);
  
  /**
   * @brief Process current frame
   * 
   * @param frame   processed frame
   * @result        keep request alive
   */
  virtual bool processCameraFrame(const dev::CamFrame<dev::Pix8> * frame);
};

/* -------------------------------------------------------------------------- */

} // end of namespace mke::rt::mod
} // end of namespace mke::rt
} // end of namespace mke

/* -------------------------------------------------------------------------- */

#endif  // _TIMERREQ_H_

/* -------------------------------------------------------------------------- */
