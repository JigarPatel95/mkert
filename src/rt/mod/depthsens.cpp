/*
 * DepthSensModule - serves API request in STATE_DEPTH_SENSOR
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

// includes

#include "depthsens.h"
#include "rt/detector.h"
#include "pushfrmreq.h"
#ifdef MKE_USE_RESERVED_API
    #include "dbgimreq.h"
#endif

/* -------------------------------------------------------------------------- */

using namespace mke;
using namespace mke::rt;
using namespace mke::rt::mod;

/* -------------------------------------------------------------------------- */

#ifndef MKE_IS_FRAME_TYPE_VALID
#define MKE_IS_FRAME_TYPE_VALID(val, reserved_mode)  \
  (val == mke::api::MKE_FRAME_TYPE_1 || val == mke::api::MKE_FRAME_TYPE_2) 
#endif
/* -------------------------------------------------------------------------- */

void DepthSensModule::finalize()

{
  mke::rt::mod::WorkerModule::finalize();
  
  killLongLivedRequests();
}

/* -------------------------------------------------------------------------- */

void DepthSensModule::registerApiCallbacks()

{
  api_helper_->api_resolver.registerCallback(mke::api::MKE_REQUEST_GET_FRAME,
      std::bind(&DepthSensModule::frameCallback, this, std::placeholders::_1));

  api_helper_->api_resolver.registerCallback(mke::api::MKE_REQUEST_START_FRAME_PUSH,
      std::bind(&DepthSensModule::pushFramesCallback, this, std::placeholders::_1));
  api_helper_->api_resolver.registerCallback(mke::api::MKE_REQUEST_STOP_FRAME_PUSH,
      std::bind(&DepthSensModule::pushFramesCallback, this, std::placeholders::_1));

#ifdef MKE_USE_RESERVED_API    
  
  if(is_reserved_mode_)
    {
      api_helper_->api_resolver.registerCallback(mke::api::MKE_REQUEST_GET_DEBUG_IMAGES,
          std::bind(&DepthSensModule::debugImagesCallback, this, std::placeholders::_1));
    }

#endif    
}

/* -------------------------------------------------------------------------- */

void DepthSensModule::unregisterApiCallbacks()

{
  api_helper_->api_resolver.unregisterCallback(mke::api::MKE_REQUEST_GET_FRAME);

  api_helper_->api_resolver.unregisterCallback(mke::api::MKE_REQUEST_START_FRAME_PUSH);
  api_helper_->api_resolver.unregisterCallback(mke::api::MKE_REQUEST_STOP_FRAME_PUSH);

#ifdef MKE_USE_RESERVED_API    
  
  if(is_reserved_mode_)
    {
      api_helper_->api_resolver.unregisterCallback(mke::api::MKE_REQUEST_GET_DEBUG_IMAGES);
    }
    
#endif
}

/* -------------------------------------------------------------------------- */

mke::rt::api::Reply * DepthSensModule::makeReplyFrame(api::Request* request, 
                                     const dev::CamFrame< dev::Pix8 >* last_frame,
                                     mke::api::MkEReplyStatus status)
{
  assert(last_frame);
  
  uint16_t frame_type = 
    request->getParamsBuffer<mke::api::MkERequest_GetFrame>()->frame_type;
  
  if(!MKE_IS_FRAME_TYPE_VALID(frame_type, is_reserved_mode_))
    throw api::ApiError(mke::api::MKE_REPLY_CLIENT_MALFORMED_REQUEST);

  api::Reply * reply = nullptr;
  if (!api_helper_->api_pool.request(reply, api::Reply::REPLY_BUFFER_FRAME))
    throw api::ApiError(mke::api::MKE_REPLY_SERVER_INSUFFICIENT_RESOURCES);

  uint32_t buff_len;
  uint16_t num_data;
  detector_.assembleFrame(reply->getPayloadBuffer(), buff_len, num_data,
    (mke::api::MkEFrameType) frame_type);
  reply->setup(request, status, buff_len);
  mke::api::MkEReply_Frame * params = reply->getParamsBuffer<mke::api::MkEReply_Frame>();

  params->frame_type = frame_type;
  params->data3d_type = (uint16_t) detector_.getData3dType();
  params->seqn = detector_seqn_;
  params->timer = last_frame->timer;
  params->num_data = num_data;
  
  return reply;
}

/* -------------------------------------------------------------------------- */

void DepthSensModule::processCameraFrame(const dev::CamFrame<dev::Pix8> * frame)

{
  // no depth sensor no processing
  
  if(api_helper_->api_resolver.getState() != mke::api::MKE_STATE_DEPTH_SENSOR 
      || !frame)
    return;

  // process long lives
  
  WorkerModule::processCameraFrame(frame);
  
  // and my processes

  curr_frame_ = frame;
  uint32_t curr_final_frame_id = frame->frame_id - (frame->phase) + detector_.getNoDetectors() - 1;

  if(curr_final_frame_id != next_final_frame_id)
    {
      detector_.clearDetections(-1);
      
      next_final_frame_id = curr_final_frame_id;
      if(next_final_frame_id > detector_.getNoDetectors())
        detector_seqn_++;
    }  
  
  if(detector_.getNoDetectors() && !detector_.isDryRun())
    detector_.runDetector(frame->mem, frame->phase);
  
  if(get_frame_request_)
    {
      try
      {
        api::Reply * reply_ptr = makeReplyFrame(get_frame_request_, frame);
        api_helper_->api_resolver.reply(reply_ptr, get_frame_request_);
      } catch(api::ApiError & e) {
        // catched ApiError
        
        api_helper_->api_resolver.reply(e.getStatusCode(), get_frame_request_);
      }      
      get_frame_request_ = nullptr;
    }

  // process all long requests
                                     
  std::unique_ptr<LongLivedRequest> * a = actions_;
  for(int i = 0; i < NUM_ACTIONS; ++i, ++a)
    if(*a && !(*a)->processCameraFrame(frame))
      *a = nullptr;
}

/* -------------------------------------------------------------------------- */

void DepthSensModule::killLongLivedRequests(api::Request * sender_ptr)

{
  // remove all long live requests
  
  std::unique_ptr<LongLivedRequest> * a = actions_;
  for(int i = 0; i < NUM_ACTIONS; ++i, ++a)
    if(*a && (!sender_ptr || ((*a)->getParentRequest()->getReplySink()->getID()
                  == sender_ptr->getReplySink()->getID())))
      {
        (*a)->cancel();
        *a = nullptr;
      }
}

/* -------------------------------------------------------------------------- */

void DepthSensModule::internalSignal(api::Request::InternalRequestType int_type, 
                                      api::Request * request_ptr)
{
  mke::rt::mod::WorkerModule::internalSignal(int_type, request_ptr);

  if (int_type == api::Request::INT_REQUEST_REPLY_SINK_INVALID)
    {
      // We are told that the sink referred to in this request is invalid
      // We need to check whether we have any outstanding request that area
      // connected with this sink and stop them.
      
      killLongLivedRequests(request_ptr);
    }
}

/* -------------------------------------------------------------------------- */

void DepthSensModule::stateChanged(mke::api::MkEStateType state)

{
  std::unique_ptr<LongLivedRequest> * a = actions_;
  for(int i = 0; i < NUM_ACTIONS; ++i, ++a)
    if(*a)
      {
        (*a)->cancel();
        a->reset();
      }
}

/* -------------------------------------------------------------------------- */

#ifdef MKE_USE_RESERVED_API    

void DepthSensModule::debugImagesCallback(api::Request * request_ptr)

{
  checkState();
  
  std::unique_ptr<LongLivedRequest> & act = actions_[ACTION_DEBUGFRAMES];
  if(act)
    throw api::ApiError(mke::api::MKE_REPLY_SERVER_BUSY);
  
  // check input arguments

  mke::api::MkERequest_DebugImages * params = 
              request_ptr->getParamsBuffer<mke::api::MkERequest_DebugImages>();
  
  bool valid_frametype = MKE_IS_FRAME_TYPE_VALID(params->frame_type, is_reserved_mode_);
  if(!valid_frametype
      || params->num_images <= 0 || params->num_images > gen_cfg_.max_debug_images)
    throw api::ApiError(mke::api::MKE_REPLY_CLIENT_MALFORMED_REQUEST);
  
  act.reset(new DebugImRequest(api_helper_, request_ptr, detector_));
}

#endif

/* -------------------------------------------------------------------------- */

void DepthSensModule::frameCallback(api::Request * request_ptr)

{
  checkState();
    
  if(curr_frame_ && curr_frame_->phase == (detector_.getNoDetectors()-1))
    {
      api::Reply * reply_ptr = makeReplyFrame(request_ptr, curr_frame_);
      api_helper_->api_resolver.reply(reply_ptr, request_ptr);
    }
  else
    {
      if(get_frame_request_ != nullptr)
        throw api::ApiError(mke::api::MKE_REPLY_SERVER_BUSY);
      
      get_frame_request_ = request_ptr;
    }
}

/* -------------------------------------------------------------------------- */

void DepthSensModule::pushFramesCallback(api::Request * request_ptr)

{
  checkState();
  
  std::unique_ptr<LongLivedRequest> & act = actions_[ACTION_PUSHFRAMES];
  if(request_ptr->getType() == mke::api::MKE_REQUEST_START_FRAME_PUSH)
    {
      if(act)
        throw api::ApiError(mke::api::MKE_REPLY_SERVER_BUSY);
      
      uint16_t frame_type = 
        request_ptr->getParamsBuffer<mke::api::MkERequest_GetFrame>()->frame_type;
      
      if(!MKE_IS_FRAME_TYPE_VALID(frame_type, is_reserved_mode_))
        throw api::ApiError(mke::api::MKE_REPLY_CLIENT_MALFORMED_REQUEST);
  
      act.reset(new PushFramesRequest(api_helper_, request_ptr, *this, detector_));
    }
  else if (request_ptr->getType() == mke::api::MKE_REQUEST_STOP_FRAME_PUSH)
    {
      if(!act)
        throw api::ApiError(mke::api::MKE_REPLY_CLIENT_REQUEST_DOES_NOT_APPLY);
      
      api_helper_->api_resolver.reply(mke::api::MKE_REPLY_OK, request_ptr);
      
      act.reset();
    }
}

/* -------------------------------------------------------------------------- */
