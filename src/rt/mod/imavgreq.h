/*
 * ImageAvgRequest - compute average image from x frames
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar, Jan Heller
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef _IMAVGREQ_H_
#define _IMAVGREQ_H_

/* -------------------------------------------------------------------------- */

#include "rt/mod/longreq.h"
#include "util/imavg.h"
#include "rt/engine.h"

/* -------------------------------------------------------------------------- */

namespace mke {
namespace rt {
namespace mod {

/* -------------------------------------------------------------------------- */

/**
 * @brief ImageAvgRequest makes average image from n images
 */
class ImageAvgRequest : public LongLivedRequest
{
protected:
  Engine                                   *engine_;
  util::ImageAverager<dev::Pix8, uint16_t>  averager_;
  int                                       countdown_;
  uint8_t                                   flags_;
  mke::api::MkEImageFormat                  im_format_;
  uint32_t                                  frame_id_1st_;
  uint64_t                                  timer_1st_;
  
public:  
  /**
   * @brief Contructor
   * 
   * @param request     request that is parent of LongLivedRequest
   */
  ImageAvgRequest(api::ApiHelper * helper, api::Request * request_ptr, Engine * engine);
  
  /**
   * @brief Process current frame
   * 
   * @param frame   processed frame
   * @result        keep request alive
   */
  virtual bool processCameraFrame(const dev::CamFrame<dev::Pix8> * frame);
};

/* -------------------------------------------------------------------------- */

} // end of namespace mke::rt::mod
} // end of namespace mke::rt
} // end of namespace mke

/* -------------------------------------------------------------------------- */

#endif  // _IMAVGREQ_H_

/* -------------------------------------------------------------------------- */
