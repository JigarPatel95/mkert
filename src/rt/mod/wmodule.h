/*
 * WorkerModule - module that is possible to serve API calls and process images
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef _WMODULE_H_
#define _WMODULE_H_

/* -------------------------------------------------------------------------- */

#include "rt/api/helper.h"
#include "dev/img.h"

/* -------------------------------------------------------------------------- */

namespace mke {
namespace rt {
namespace mod {

/* -------------------------------------------------------------------------- */

class WorkerModule
{
protected:
  api::ApiHelper    *   api_helper_;            // pointer to API helper
  const bool            is_reserved_mode_;       // is application in reserved mode
  
  /**
   * @brief Register API callbacks
   */
  virtual void registerApiCallbacks() = 0;
  
  /**
   * @brief Unregister API callbacks
   */
  virtual void unregisterApiCallbacks() = 0;
   
public:
  /**
   * @brief Contructor
   */
  WorkerModule(api::ApiHelper * helper_ptr, bool is_reserved_mode) :
    api_helper_(helper_ptr),
    is_reserved_mode_(is_reserved_mode)
  {};
  
  /**
   * @brief Module initialization
   */
  virtual void init()
  {
    registerApiCallbacks();
  };
  
  /**
   * @brief Module finalization
   */
  virtual void finalize()
  {
    unregisterApiCallbacks();
  }
  
  /**
   * 
   */
  virtual void stateChanged(mke::api::MkEStateType state) {};
  
  /**
   * @brief Destructor
   */
  virtual ~WorkerModule() 
  {};
  
  /**
   * @brief Process one camera frame
   */
  virtual void processCameraFrame(const dev::CamFrame<dev::Pix8> * frame) {};
  
  /**
   * @brief signal internal message
   */
  virtual void internalSignal(api::Request::InternalRequestType int_type,
                              api::Request * request_ptr) {};
};

/* -------------------------------------------------------------------------- */

} // end of namespace mke::rt::mod
} // end of namespace mke::rt
} // end of namespace mke

/* -------------------------------------------------------------------------- */

#endif // _WMODULE_H_

/* -------------------------------------------------------------------------- */