/*
 * ServiceModule - serves API request in STATE_SERVICE
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

// includes

#include "service.h"

#include "util/pgm.h"
#include "rt/worker.h"
#include "imavgreq.h"
#include "shutterreq.h"

/* -------------------------------------------------------------------------- */

using namespace mke;
using namespace mke::rt;
using namespace mke::rt::mod;

/* -------------------------------------------------------------------------- */

void ServiceModule::finalize()
{
  mke::rt::mod::WorkerModule::finalize();
  
  killLongLivedRequests();
}

/* -------------------------------------------------------------------------- */

void ServiceModule::registerApiCallbacks()

{
  if(is_reserved_mode_)
    {
      // ping
      
      api_helper_->api_resolver.registerCallback(mke::api::MKE_REQUEST_PING,
          std::bind(&ServiceModule::pingCallback, this, std::placeholders::_1));

      // timer
      
      api_helper_->api_resolver.registerCallback(mke::api::MKE_REQUEST_START_TIMER, 
          std::bind(&ServiceModule::timerCallback, this, std::placeholders::_1));
      api_helper_->api_resolver.registerCallback(mke::api::MKE_REQUEST_STOP_TIMER, 
          std::bind(&ServiceModule::timerCallback, this, std::placeholders::_1));

      // images
      
      api_helper_->api_resolver.registerCallback(mke::api::MKE_REQUEST_GET_IMAGE,
          std::bind(&ServiceModule::imageCallback, this, std::placeholders::_1));

      // stream 
      
      api_helper_->api_resolver.registerCallback(mke::api::MKE_REQUEST_STREAM_ON,
          std::bind(&ServiceModule::streamCallback, this, std::placeholders::_1));
      api_helper_->api_resolver.registerCallback(mke::api::MKE_REQUEST_STREAM_OFF,
          std::bind(&ServiceModule::streamCallback, this, std::placeholders::_1));

      // shutter
      
      api_helper_->api_resolver.registerCallback(mke::api::MKE_REQUEST_GET_SHUTTER,
          std::bind(&ServiceModule::shutterCallback, this, std::placeholders::_1));
      api_helper_->api_resolver.registerCallback(mke::api::MKE_REQUEST_SET_SHUTTER,
          std::bind(&ServiceModule::shutterCallback, this, std::placeholders::_1));

      // gain
      
      api_helper_->api_resolver.registerCallback(mke::api::MKE_REQUEST_GET_GAIN,
          std::bind(&ServiceModule::gainCallback, this, std::placeholders::_1));
      api_helper_->api_resolver.registerCallback(mke::api::MKE_REQUEST_SET_GAIN,
          std::bind(&ServiceModule::gainCallback, this, std::placeholders::_1));

      // laser
      
      api_helper_->api_resolver.registerCallback(mke::api::MKE_REQUEST_GET_LASER,
          std::bind(&ServiceModule::laserCallback, this, std::placeholders::_1));
      api_helper_->api_resolver.registerCallback(mke::api::MKE_REQUEST_SET_LASER,
          std::bind(&ServiceModule::laserCallback, this, std::placeholders::_1));
    }
}

/* -------------------------------------------------------------------------- */

void ServiceModule::unregisterApiCallbacks()

{
  if(is_reserved_mode_)
    {
      // ping
      
      api_helper_->api_resolver.unregisterCallback(mke::api::MKE_REQUEST_PING);

      // timer
      
      api_helper_->api_resolver.unregisterCallback(mke::api::MKE_REQUEST_START_TIMER);
      api_helper_->api_resolver.unregisterCallback(mke::api::MKE_REQUEST_STOP_TIMER);

      // images
      
      api_helper_->api_resolver.unregisterCallback(mke::api::MKE_REQUEST_GET_IMAGE);

      // stream
      
      api_helper_->api_resolver.unregisterCallback(mke::api::MKE_REQUEST_STREAM_ON);
      api_helper_->api_resolver.unregisterCallback(mke::api::MKE_REQUEST_STREAM_OFF);

      // shutter
      
      api_helper_->api_resolver.unregisterCallback(mke::api::MKE_REQUEST_GET_SHUTTER);
      api_helper_->api_resolver.unregisterCallback(mke::api::MKE_REQUEST_SET_SHUTTER);

      // gain
      
      api_helper_->api_resolver.unregisterCallback(mke::api::MKE_REQUEST_GET_GAIN);
      api_helper_->api_resolver.unregisterCallback(mke::api::MKE_REQUEST_SET_GAIN);

      // laser
      
      api_helper_->api_resolver.unregisterCallback(mke::api::MKE_REQUEST_GET_LASER);
      api_helper_->api_resolver.unregisterCallback(mke::api::MKE_REQUEST_SET_LASER);
    }
}

/* -------------------------------------------------------------------------- */

void ServiceModule::processCameraFrame(const dev::CamFrame<dev::Pix8> * frame)

{
  WorkerModule::processCameraFrame(frame);

  // process all long requests
                                     
  std::unique_ptr<LongLivedRequest> * a = actions_;
  for(int i = 0; i < NUM_ACTIONS; ++i, ++a)
    if(*a && !(*a)->processCameraFrame(frame))
      *a = nullptr;
  
  // store frame for later use
  
  curr_frame_ = frame;
}

/* -------------------------------------------------------------------------- */

void ServiceModule::killLongLivedRequests(api::Request * sender_ptr)

{
  // remove all long live requests
  
  std::unique_ptr<LongLivedRequest> * a = actions_;
  for(int i = 0; i < NUM_ACTIONS; ++i, ++a)
    if(*a && (!sender_ptr || ((*a)->getParentRequest()->getReplySink()->getID()
          == sender_ptr->getReplySink()->getID())))
      {
        (*a)->cancel();
        *a = nullptr;
      }
}

/* -------------------------------------------------------------------------- */

void ServiceModule::internalSignal(api::Request::InternalRequestType int_type, 
                                      api::Request * request_ptr)
{
  mke::rt::mod::WorkerModule::internalSignal(int_type, request_ptr);

  if (int_type == api::Request::INT_REQUEST_REPLY_SINK_INVALID)
    {
      // We are told that the sink referred to in this request is invalid
      // We need to check whether we have any outstanding request that area
      // connected with this sink and stop them.
      
      killLongLivedRequests(request_ptr);
    }
}

/* -------------------------------------------------------------------------- */

void ServiceModule::stateChanged(mke::api::MkEStateType state)

{
  std::unique_ptr<LongLivedRequest> * a = actions_;
  for(int i = 0; i < NUM_ACTIONS; ++i, ++a)
    if(*a)
      (*a)->cancel();
}

/* -------------------------------------------------------------------------- */

void ServiceModule::pingCallback(api::Request *request_ptr) 

{
  checkState();
  
  LOG_INFO << "Replying to MKE_REQUEST_TIMER, seqid = " << request_ptr->getId();
  
  api_helper_->api_resolver.reply(mke::api::MKE_REPLY_OK, request_ptr);
}  

/* -------------------------------------------------------------------------- */

void ServiceModule::timerCallback(api::Request *request_ptr) 

{
  checkState();
  
  std::unique_ptr<LongLivedRequest> & timer_act = actions_[ACTION_TIMER];

  // what action
  
  if (request_ptr->getType() == mke::api::MKE_REQUEST_START_TIMER)  // start timer request
    {  
      
      // timer already running
      
      if(timer_act)
        throw api::ApiError(mke::api::MKE_REPLY_SERVER_BUSY);

      // go on - start it

      actions_[ACTION_TIMER] = std::move(
        std::unique_ptr<LongLivedRequest>(new TimerRequest(api_helper_, request_ptr)));

      LOG_INFO << "Replying to MKE_REQUEST_START_TIMER, seqid = " << request_ptr->getId() <<
                  ", ticks = " << request_ptr->getParamsBuffer<mke::api::MkERequest_Timer>()->timer_ticks;
    }
  else if (request_ptr->getType() == mke::api::MKE_REQUEST_STOP_TIMER)  // stop timer
    { 
      // no timer to be stopped
      
      if (!timer_act)
        throw api::ApiError(mke::api::MKE_REPLY_CLIENT_REQUEST_DOES_NOT_APPLY);
      
      LOG_INFO << "Replying to MKE_REQUEST_STOP_TIMER, seqid = " << request_ptr->getId();
     
      api_helper_->api_resolver.reply(mke::api::MKE_REPLY_OK, request_ptr, false);

      timer_act.reset();
    }
}

/* -------------------------------------------------------------------------- */

void ServiceModule::imageCallback(api::Request * request_ptr)

{
  checkState();

  // is already computing
  
  if(actions_[ACTION_GETIMAGE])
    throw api::ApiError(mke::api::MKE_REPLY_SERVER_BUSY);
  
  actions_[ACTION_GETIMAGE] = std::move(
    std::unique_ptr<LongLivedRequest>(new ImageAvgRequest(api_helper_, request_ptr, engine_)));
}

/* -------------------------------------------------------------------------- */

void ServiceModule::streamCallback(api::Request * request_ptr)

{
  checkState();
  
  throw api::ApiError(mke::api::MKE_REPLY_SERVER_ERROR);
}

/* -------------------------------------------------------------------------- */

void ServiceModule::shutterCallback(api::Request* request_ptr)

{
  checkState();

  if(request_ptr->getType() == mke::api::MKE_REQUEST_SET_SHUTTER)
    {
      // is already computing
      
      if(actions_[ACTION_SETSHUTTER])
        throw api::ApiError(mke::api::MKE_REPLY_SERVER_BUSY);
      
      actions_[ACTION_SETSHUTTER] = std::move(
          std::unique_ptr<LongLivedRequest>(new SetShutterRequest(api_helper_, 
            request_ptr, engine_, request_ptr->getParamsBuffer<mke::api::MkERequest_SetShutterSpeed>()->shutter_speed)));
    }
  else
    {
      api::Reply * reply_ptr = api_helper_->prepareBasicReply(request_ptr);
      mke::api::MkEReply_GetShutterSpeed * params = 
              reply_ptr->getParamsBuffer<mke::api::MkEReply_GetShutterSpeed>();
      params->shutter_speed = engine_->getShutterSpeed();
      
      api_helper_->api_resolver.reply(reply_ptr, request_ptr);
    }
}

/* -------------------------------------------------------------------------- */

void ServiceModule::gainCallback(api::Request* request_ptr)

{
  checkState();

  if(request_ptr->getType() == mke::api::MKE_REQUEST_SET_GAIN)
    {
      if(engine_->setISO(
        request_ptr->getParamsBuffer<mke::api::MkERequest_SetGain>()->iso_value))
        {
          api_helper_->api_resolver.reply(mke::api::MKE_REPLY_OK, request_ptr);
        }
      else
        {
          api_helper_->api_resolver.reply(mke::api::MKE_REPLY_CLIENT_MALFORMED_REQUEST, 
                                          request_ptr);
        }
    }
  else
    {
      api::Reply * reply_ptr = api_helper_->prepareBasicReply(request_ptr);
      mke::api::MkEReply_GetGain * params = 
                      reply_ptr->getParamsBuffer<mke::api::MkEReply_GetGain>();
      params->analog_gain  = engine_->getAnalogGain();
      params->digital_gain = engine_->getDigitalGain();
      
      api_helper_->api_resolver.reply(reply_ptr, request_ptr);
    }
}

/* -------------------------------------------------------------------------- */

inline double convSciNum(const int8_t mantisa, const int8_t exp)

{
  return mantisa * std::pow(2, exp);
}

/* -------------------------------------------------------------------------- */

void ServiceModule::laserCallback(api::Request* request_ptr)

{
  checkState();
  
  if(request_ptr->getType() == mke::api::MKE_REQUEST_SET_LASER)
    {
      mke::api::MkERequest_SetLaser * params = 
                  request_ptr->getParamsBuffer<mke::api::MkERequest_SetLaser>();
      
      laser_params_ = *params;
      
      double laser_duration_ = convSciNum(params->dur_mantisa, params->dur_exp);
      double laser_offset_ = convSciNum(params->off_mantisa, params->off_exp);
      
      engine_->fireLaserPattern(params->pattern, laser_duration_, laser_offset_);
      api_helper_->api_resolver.reply(mke::api::MKE_REPLY_OK, request_ptr);
    }
  else
    {
      api::Reply * reply_ptr = api_helper_->prepareBasicReply(request_ptr);
      mke::api::MkEReply_GetLaser * params = 
                      reply_ptr->getParamsBuffer<mke::api::MkEReply_GetLaser>();
      
      params->pattern = laser_params_.pattern;
      params->dur_mantisa = laser_params_.dur_mantisa;
      params->dur_exp = laser_params_.dur_exp;
      params->off_mantisa = laser_params_.off_mantisa;
      params->off_exp = laser_params_.off_exp;
      
      api_helper_->api_resolver.reply(reply_ptr, request_ptr);
    }
}

/* -------------------------------------------------------------------------- */
