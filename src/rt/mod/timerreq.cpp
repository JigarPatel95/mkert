/*
 * TimerRequest - sending tick packets until predefined number
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar, Jan Heller
 *
 */

/* -------------------------------------------------------------------------- */

#include "dev/img.h"
#include "rt/api/reply.h"

#include "timerreq.h"

/* -------------------------------------------------------------------------- */

using namespace mke;
using namespace mke::rt;
using namespace mke::rt::mod;

/* -------------------------------------------------------------------------- */

TimerRequest::TimerRequest(api::ApiHelper * helper, api::Request * request_ptr): 
  LongLivedRequest(helper, request_ptr)
{
  timer_ticks_ = 
    request_ptr->getParamsBuffer<mke::api::MkERequest_Timer>()->timer_ticks;

  // prepare final reply
    
  setFinalReply(api_helper_->prepareBasicReply(request_ptr, 
                                               mke::api::MKE_REPLY_DATA_STOPPED));
  // send that data will continue
    
  api_helper_->api_resolver.reply(mke::api::MKE_REPLY_DATA_WILL_START, request_ptr, false);              
}

/* -------------------------------------------------------------------------- */

bool TimerRequest::processCameraFrame(const dev::CamFrame<dev::Pix8> * frame)

{
  // There is a timer to be processed, do the `work`
  timer_ticks_--;
  
  uint64_t timer;
  dev::CamFrame<dev::Pix8>::setTimerToNow(timer);
  
  // OK, we are done, let's construct the reply        
  
  api::Reply * reply_ptr;
  try
  {
    reply_ptr = api_helper_->prepareBasicReply(request_ptr_, mke::api::MKE_REPLY_DATA_WILL_CONTINUE);

  } catch(api::ApiError & e) {
    // do not remove memmory
    
    api_helper_->api_pool.requestStock(reply_ptr, mke::api::MKE_REPLY_SERVER_INSUFFICIENT_RESOURCES);
    api_helper_->api_resolver.reply(reply_ptr, request_ptr_, false);
    
    return false;
  }

  // We have `Reply` object, let's fill it up
  
  mke::api::MkEReply_Timer * timer_params = reply_ptr->getParamsBuffer<mke::api::MkEReply_Timer>();
  timer_params->timer_ticks = timer_ticks_;
  timer_params->timer = timer;
  
  if (timer_ticks_)
      LOG_INFO << "Continuing to MKE_REQUEST_START_TIMER, seqid = " << request_ptr_->getId() <<
        ", timer_ticks = " << timer_ticks_ << ", timer = " << timer;
  else
      LOG_INFO << "Finishing to MKE_REQUEST_START_TIMER, seqid = " << request_ptr_->getId() << 
        ", timer_ticks = " << timer_ticks_ << ", timer = " << timer;                

  api_helper_->api_resolver.reply(reply_ptr, request_ptr_, false);
  
  // should we continue
  
  return timer_ticks_;
}

/* -------------------------------------------------------------------------- */
