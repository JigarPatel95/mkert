/*
 * ImageAvgRequest - compute average image from x frames
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar, Jan Heller
 *
 */

/* -------------------------------------------------------------------------- */

#include "imavgreq.h"
#include "dev/img.h"
#include "util/pgm.h"
#include "rt/api/reply.h"

#include "wmodule.h"

#ifdef USE_PNG
  #include "util/pngenc.h"
#endif

/* -------------------------------------------------------------------------- */

using namespace mke;
using namespace mke::rt;
using namespace mke::rt::mod;

/* -------------------------------------------------------------------------- */

ImageAvgRequest::ImageAvgRequest(api::ApiHelper * helper, api::Request * request_ptr, Engine * engine): 
  LongLivedRequest(helper, request_ptr), engine_(engine)
{
  countdown_ = std::max<uint32_t>(1,
    request_ptr->getParamsBuffer<mke::api::MkERequest_GetImage>()->num_average);
  flags_ = request_ptr->getParamsBuffer<mke::api::MkERequest_GetImage>()->flags;
  im_format_ = mke::api::MkEImageFormat(
    request_ptr->getParamsBuffer<mke::api::MkERequest_GetImage>()->image_format);
  
  // check availability of image formats
  
  switch(im_format_)
    {
#ifdef USE_PNG
      case mke::api::MKE_IMAGE_FORMAT_PNG:
#endif
      case mke::api::MKE_IMAGE_FORMAT_PGM:
        break;
      default:
        throw mke::rt::api::ApiError(mke::api::MKE_REPLY_CLIENT_MALFORMED_REQUEST);        
    }
}

/* -------------------------------------------------------------------------- */

bool ImageAvgRequest::processCameraFrame(const dev::CamFrame<dev::Pix8> * frame)

{
  if(!frame)
    return true;
  
  if(!averager_.getCnt())
    {
      frame_id_1st_ = frame->frame_id;
      timer_1st_ = frame->timer;
    }
  
  averager_.add(*frame);
  countdown_--;
  
  LOG_INFO << "CDOWN: " << countdown_;
  
  if(countdown_ <= 0)
    {
      if(flags_ & mke::api::MKE_FLAG_TURNOFF_LASERS_AFTER)
        engine_->fireLaserPattern(0, 0, 0);
      
      dev::MemImage<dev::Pix8> avgs(frame->w, frame->h);
      averager_.avg<dev::Pix8>(avgs);
      size_t size_im = mke::util::PGM::getBufferSize(frame->w, frame->h);
      api::Reply * reply_ptr = api_helper_->prepareDynamicReply(request_ptr_, size_im);
      setFinalReply(reply_ptr);
      mke::api::MkEReply_GetImage * params = reply_ptr->getParamsBuffer<mke::api::MkEReply_GetImage>();
      params->image_id = frame_id_1st_;
      params->timer = timer_1st_;
      if(im_format_ == mke::api::MKE_IMAGE_FORMAT_PNG)
        {
#ifdef USE_PNG
          size_im = mke::util::PNG::encode<dev::Pix8>(avgs.w, avgs.h, avgs.stride, 
              avgs.mem, reinterpret_cast<char *>(reply_ptr->getPayloadBuffer()), size_im, 3);
#else
          throw mke::rt::api::ApiError(mke::api::MKE_REPLY_CLIENT_MALFORMED_REQUEST);
#endif
          // resize the reply because of encodings
          
          reply_ptr->setup(request_ptr_, mke::api::MKE_REPLY_OK, size_im);
        }
      else 
        {
          mke::util::PGM::encode(avgs, reply_ptr->getPayloadBuffer());
        }
      
      return false;
    }    
  
  return true;
}

/* -------------------------------------------------------------------------- */
