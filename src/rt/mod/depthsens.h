/*
 * DepthSensModule - serves API request in STATE_DEPTH_SENSOR
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef _DEPTHSENS_H_
#define _DEPTHSENS_H_

/* -------------------------------------------------------------------------- */

#include "wmodule.h"
#include "longreq.h"
#include "../detector.h"
#include "../autoexp.h"
#include "mkert_config.h"

/* -------------------------------------------------------------------------- */

namespace mke {
namespace rt {
namespace mod {

/* -------------------------------------------------------------------------- */

class DepthSensModule : public WorkerModule
{
protected:
  enum Actions { ACTION_PUSHFRAMES,
                 ACTION_DEBUGFRAMES,
                 NUM_ACTIONS };

                 
  Detector                  &   detector_;          // detector
  uint64_t                      detector_seqn_;     // detector sequesnce number
  uint64_t                      next_final_frame_id;    // estimated frame_id of final frame
  
  AutoExposure              *   autoexp_;
  
  const dev::CamFrame<dev::Pix8>  
                            *   curr_frame_;        // currently processed frame
  api::Request              *   get_frame_request_; // pointer to get frame request

  const MkertConfig::GeneralConfig
                            &   gen_cfg_;           // engine config
  std::unique_ptr<LongLivedRequest>
                                actions_[NUM_ACTIONS];  // array of all actions

  
public:
  /**
   * @brief Constructor
   */
  DepthSensModule(api::ApiHelper * helper, bool is_reserved_mode, Detector & detector, AutoExposure * autoexp, const MkertConfig::GeneralConfig & cfg):
    WorkerModule(helper, is_reserved_mode),
    detector_(detector),
    detector_seqn_(0),
    next_final_frame_id(detector.getNoDetectors()-1),
    autoexp_(autoexp),
    curr_frame_(nullptr),
    get_frame_request_(nullptr),
    gen_cfg_(cfg)
  {};
  
  /**
   * @brief Process one camera frame
   */
  virtual void processCameraFrame(const dev::CamFrame<dev::Pix8> * frame);
  
  /**
   * @brief Kill all active long live requests
   */
  virtual void killLongLivedRequests(api::Request * sender_ptr = nullptr);
  
  /**
   * @brief signal internal message
   */
  virtual void internalSignal(api::Request::InternalRequestType int_type,
                              api::Request * request_ptr);
  
  /**
   * 
   */
  virtual void stateChanged(mke::api::MkEStateType state);  
  
  /**
   * @brief Module finalization
   */
  virtual void finalize();
  
protected:
  /**
   * 
   */
  inline void checkState() 
  { 
    if(api_helper_->api_resolver.getState() != mke::api::MKE_STATE_DEPTH_SENSOR) 
      throw api::ApiError(mke::api::MKE_REPLY_CLIENT_REQUEST_DOES_NOT_APPLY);
  };
  
  friend class PushFramesRequest;
  
  /**
   * 
   */
  mke::rt::api::Reply * makeReplyFrame(api::Request * request, 
                              const dev::CamFrame<dev::Pix8> * last_frame,
                              mke::api::MkEReplyStatus status = mke::api::MKE_REPLY_OK);

  /**
   * @brief Register API callbacks
   */
  virtual void registerApiCallbacks();
  
  /**
   * @brief Unregister API callbacks
   */
  virtual void unregisterApiCallbacks();
  
  // callbacks

#ifdef MKE_USE_RESERVED_API        
  /**
   * @brief callback for batch of debug images with frames
   *
   * @param request p_request:...
   */
  void debugImagesCallback(api::Request * request_ptr);
#endif
  
  /**
   * @brief callback for get current frame
   *
   * @param request p_request:...
   */
  void frameCallback(api::Request * request_ptr);
    
  /**
   * @brief callback for pushing frame
   *
   * @param request p_request:...
   */
  void pushFramesCallback(api::Request * request_ptr);
};

/* -------------------------------------------------------------------------- */

} // end of namespace mke::rt::mod
} // end of namespace mke::rt
} // end of namespace mke

/* -------------------------------------------------------------------------- */

#endif // _DEPTHSENS_H_

/* -------------------------------------------------------------------------- */
