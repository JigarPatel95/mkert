/*
 * DebugImRequest - compute average image from x frames
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar, Jan Heller
 *
 */

/* -------------------------------------------------------------------------- */

#include "dbgimreq.h"
#include "rt/api/reply.h"
#include "wmodule.h"
#include "util/pgm.h"

#ifdef USE_PNG
  #include "util/pngenc.h"
#endif

/* -------------------------------------------------------------------------- */

using namespace mke;
using namespace mke::rt;
using namespace mke::rt::mod;

/* -------------------------------------------------------------------------- */

DebugImRequest::DebugImRequest(api::ApiHelper * helper, api::Request * request_ptr, Detector & detector): 
  LongLivedRequest(helper, request_ptr),
  detector_(detector),
  params_(request_ptr->getParamsBuffer<mke::api::MkERequest_DebugImages>()),
  countdown_(params_->num_images),
  reply_ptr_(nullptr)
{
#ifndef USE_PNG  
  if(params_->image_format == mke::api::MKE_IMAGE_FORMAT_PNG)
    throw mke::rt::api::ApiError(mke::api::MKE_REPLY_CLIENT_MALFORMED_REQUEST);
#endif
}

/* -------------------------------------------------------------------------- */

bool DebugImRequest::processCameraFrame(const dev::CamFrame<dev::Pix8> * frame)

{
  if(!frame)
    return true;
  
  size_t im_size = util::PGM::getBufferSize(frame->w, frame->h);
  
  if(!reply_ptr_)
    {
      size_t dbg_size = detector_.getMaxFrameSize() 
                          + im_size 
                          + sizeof(mke::api::MkEDebugImagesSubHeader);
      try
        {
          reply_ptr_ = api_helper_->prepareDynamicReply(request_ptr_, dbg_size * countdown_, 
                                          mke::api::MKE_REPLY_OK);
          setFinalReply(reply_ptr_);
        } 
      catch(api::ApiError & e) 
        {
          // do not remove memmory
          
          api_helper_->api_pool.requestStock(reply_ptr_, mke::api::MKE_REPLY_SERVER_INSUFFICIENT_RESOURCES);
          api_helper_->api_resolver.reply(reply_ptr_, request_ptr_, false);
          
          return false;        
        }
      data_ptr_ = reinterpret_cast<char *>(reply_ptr_->getPayloadBuffer());
    }

  // prepare header
  
  mke::api::MkEDebugImagesSubHeader * header = reinterpret_cast<mke::api::MkEDebugImagesSubHeader *>(data_ptr_);
  header->timer = frame->timer;
  header->image_frame_id = frame->frame_id;
  header->image_num_bytes = util::PGM::getBufferSize(frame->w, frame->h);
  header->frame_type = params_->frame_type;
  header->laser_pattern = detector_.getLaserPattern(frame->phase);
  header->data3d_type = detector_.getData3dType(frame->phase);
  
  // write data into buffer
  
  data_ptr_ += sizeof(mke::api::MkEDebugImagesSubHeader);
  if(params_->image_format == mke::api::MKE_IMAGE_FORMAT_PNG)
    {
#ifdef USE_PNG
      header->image_num_bytes = mke::util::PNG::encode<dev::Pix8>(frame->w, frame->h, frame->stride, 
          frame->mem, data_ptr_, im_size);
      
      data_ptr_ += header->image_num_bytes;
#else
      throw mke::rt::api::ApiError(mke::api::MKE_REPLY_CLIENT_MALFORMED_REQUEST);
#endif      
    }
  else
    {
      data_ptr_ += util::PGM::encode(*frame, data_ptr_);      
    }

  // write frame into buffer
  detector_.assembleFrame(data_ptr_, header->frame_num_bytes, header->frame_num_data,
    (mke::api::MkEFrameType) params_->frame_type, frame->phase);
  
  data_ptr_ += header->frame_num_bytes;

  countdown_--;
  
  LOG_INFO << "CDOWN: " << countdown_;
  
  if(countdown_ <= 0)
    {
      reply_ptr_->setup(request_ptr_, mke::api::MKE_REPLY_OK, 
            data_ptr_-reinterpret_cast<char *>(reply_ptr_->getPayloadBuffer()));
      reply_ptr_->getParamsBuffer<mke::api::MkEReply_DebugImages>()->num_images =
        params_->num_images;

      return false;
    }
  
  return true;
}

/* -------------------------------------------------------------------------- */
