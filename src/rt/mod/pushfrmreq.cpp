/*
 * PushFramesRequest - sending tick packets until predefined number
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar, Jan Heller
 *
 */

/* -------------------------------------------------------------------------- */

#include "rt/api/reply.h"
#include "pushfrmreq.h"

/* -------------------------------------------------------------------------- */

using namespace mke;
using namespace mke::rt;
using namespace mke::rt::mod;

/* -------------------------------------------------------------------------- */

PushFramesRequest::PushFramesRequest(api::ApiHelper * helper, api::Request * request_ptr, 
                                     DepthSensModule & parent, Detector & detector): 
  LongLivedRequest(helper, request_ptr),
  parent_(parent),
  detector_(detector),
  push_idx_(detector_.getNoDetectors()-1)
{

  // prepare final reply
    
  setFinalReply(api_helper_->prepareBasicReply(request_ptr, 
                                               mke::api::MKE_REPLY_DATA_STOPPED));

  // send that data will continue
    
  api_helper_->api_resolver.reply(mke::api::MKE_REPLY_DATA_WILL_START, request_ptr, false);              
}

/* -------------------------------------------------------------------------- */

bool PushFramesRequest::processCameraFrame(const dev::CamFrame<dev::Pix8> * frame)

{
  if(!frame || frame->phase != push_idx_)
    return true;

  api::Reply * reply_ptr;
  try
  {
    reply_ptr = parent_.makeReplyFrame(request_ptr_, frame, 
                                       mke::api::MKE_REPLY_DATA_WILL_CONTINUE);
    api_helper_->api_resolver.reply(reply_ptr, request_ptr_, false);
    //LOG_INFO << "data pushed " << frame->frame_id;
  }
  catch(api::ApiError & e)
  {
    if(e.getStatusCode() == mke::api::MKE_REPLY_SERVER_INSUFFICIENT_RESOURCES)
      {
        LOG_INFO << "full ReplyFrame queue, ignored " << frame->frame_id;
        
        return true;
      }
    
    // do not remove memory

    final_reply_ptr_->setup(request_ptr_, e.getStatusCode());
    
    return false;
  }
  
  return true;
}

/* -------------------------------------------------------------------------- */
