/*
 * LongLivedRequest - request that reply after few frames
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar, Jan Heller
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef _LONGREQ_H_
#define _LONGREQ_H_

/* -------------------------------------------------------------------------- */

#include "mkeapi.h"
#include "rt/api/request.h"
#include "rt/api/resolver.h"
#include "rt/api/helper.h"
#include "dev/img.h"

/* -------------------------------------------------------------------------- */

namespace mke {
namespace rt {
namespace mod {

/* -------------------------------------------------------------------------- */

/**
 * @brief LongLivedRequest is request that reply later
 */
class LongLivedRequest
{
protected:
  api::ApiHelper    *   api_helper_;        // pointer to API helper
  api::Request      *   request_ptr_;       // request that start this one
  
  api::Reply        *   final_reply_ptr_;   // final reply
  
public:  
  /**
   * @brief Contructor
   * 
   * @param request     request that is parent of LongLivedRequest
   */
  LongLivedRequest(api::ApiHelper * helper_ptr, api::Request * request_ptr) :
    api_helper_(helper_ptr),
    request_ptr_(request_ptr),
    final_reply_ptr_(nullptr)
  {};
  
  /**
   * @brief Destructor
   */
  virtual ~LongLivedRequest() 
  {
    // when I die my parent request too
    
    if(final_reply_ptr_)
      api_helper_->api_resolver.reply(final_reply_ptr_, request_ptr_);
    else
      api_helper_->api_resolver.release(request_ptr_);
  };
  
  /**
   * @brief returns parent request
   */
  inline api::Request * getParentRequest() { return request_ptr_; };
  
  /**
   * @brief set final reply - that will be sent
   */
  inline void setFinalReply(api::Reply * reply_ptr) 
  { 
    assert(final_reply_ptr_ == nullptr);
    
    final_reply_ptr_ = reply_ptr;
  };
  
  /**
   * @brief cancel request - means will be deleted without ca
   */
  virtual void cancel() 
  {
    // if allready allocated some request - use it, otherwise allocate my own
    
    if(final_reply_ptr_)
      final_reply_ptr_->setup(request_ptr_, 
                              mke::api::MKE_REPLY_SERVER_REQUEST_INTERRUPTED, 0);
    else if(!api_helper_->api_pool.request(final_reply_ptr_, api::Reply::REPLY_BUFFER_BASIC))
      final_reply_ptr_ =  
        api_helper_->api_pool.requestStock(mke::api::MKE_REPLY_SERVER_INSUFFICIENT_RESOURCES);
  }
  
  /**
   * @brief Process current frame
   * 
   * @param frame   processed frame
   * @result        keep request alive
   */
  virtual bool processCameraFrame(const dev::CamFrame<dev::Pix8> * frame) = 0;
};

/* -------------------------------------------------------------------------- */

} // end of namespace mke::rt::mod
} // end of namespace mke::rt
} // end of namespace mke

/* -------------------------------------------------------------------------- */

#endif  // _LONGREQ_H_

/* -------------------------------------------------------------------------- */
