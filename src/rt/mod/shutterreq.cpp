/*
 * SetShutterRequest - send shutter after is stable and images are ok
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#include "shutterreq.h"

/* -------------------------------------------------------------------------- */

#define SETSHUTTER_LIMIT_FRAMES 60

/* -------------------------------------------------------------------------- */

using namespace mke;
using namespace rt;
using namespace mod;

/* -------------------------------------------------------------------------- */

SetShutterRequest::SetShutterRequest(api::ApiHelper* helper, 
                                     api::Request* request_ptr,
                                     Engine * engine,
                                     uint32_t exposure_time) :
LongLivedRequest(helper, request_ptr),
engine_(engine),
exposure_time_(exposure_time),
countdown_(SETSHUTTER_LIMIT_FRAMES)
                                                   
{
  engine_->setShutterSpeed(exposure_time);
}

/* -------------------------------------------------------------------------- */

bool SetShutterRequest::processCameraFrame(const mke::dev::CamFrame<mke::dev::Pix8 >* frame)

{
  // no frame, no shutter info
  
  if(!frame)
    return true;

#ifdef USE_RSPI
  // is shutter value already setted ?
  
  bool shutter_ok = exposure_time_ - engine_->getShutterSpeed() < 256;
  if(!shutter_ok)
    return true;

  if(dynamic_cast<RasPiEngine *>(engine_) != nullptr)
  {
    // check output image - is it valid ?
    
    dev::Pix8 mx = 0;
    dev::Pix8 mn = 255;
    for(int r = 0; r < frame->h; ++r)
        {
        const dev::Pix8 * p  = frame->row(r);
        const dev::Pix8 * pE = p + frame->w;
        
        for(; p != pE; ++p)
            {
            mx = std::max(mx, *p);
            mn = std::min(mn, *p);
            }
        }
    
    bool invalid_image = mn >= 15 && mx <= 16;

    // finite number of loops
    
    countdown_--;
    
    if(countdown_ > 0 && invalid_image)
        return true;

    if(countdown_ <= 0)
        LOG_WARN << "SetShutter ends too early";
    else
        LOG_INFO << "Image ok: " << int(mn) << " .. " << int(mx) << " in " << 
                                (SETSHUTTER_LIMIT_FRAMES-countdown_) << " frames.";
    
    // everything is ok or number of loops exceeds
    
    setFinalReply(api_helper_->prepareBasicReply(request_ptr_));
    return false;
  }
#endif

  setFinalReply(api_helper_->prepareBasicReply(request_ptr_));
  return false;
}

/* -------------------------------------------------------------------------- */
