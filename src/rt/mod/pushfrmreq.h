/*
 * PushFramesRequest - pushing requests to client 
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar, Jan Heller
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef _PUSHFRMREQ_H_
#define _PUSHFRMREQ_H_

/* -------------------------------------------------------------------------- */

#include "longreq.h"
#include "depthsens.h"
#include "../detector.h"

/* -------------------------------------------------------------------------- */

namespace mke {
namespace rt {
namespace mod {

/* -------------------------------------------------------------------------- */

/**
 * @brief PushFramesRequest reply request on each frame finished
 */
class PushFramesRequest : public LongLivedRequest
{
  DepthSensModule       &   parent_;
  Detector              &   detector_;
  const int                 push_idx_;
  
public:  
  /**
   * @brief Contructor
   * 
   * @param request     request that is parent of LongLivedRequest
   */
  PushFramesRequest(api::ApiHelper * helper, api::Request * request_ptr, 
                    DepthSensModule & parent, Detector & detector);
  
  virtual ~PushFramesRequest()
  {};
  
  /**
   * @brief Process current frame
   * 
   * @param frame   processed frame
   * @result        keep request alive
   */
  virtual bool processCameraFrame(const dev::CamFrame<dev::Pix8> * frame);
};

/* -------------------------------------------------------------------------- */

} // end of namespace mke::rt::mod
} // end of namespace mke::rt
} // end of namespace mke

/* -------------------------------------------------------------------------- */

#endif  // _PUSHFRMREQ_H_

/* -------------------------------------------------------------------------- */
