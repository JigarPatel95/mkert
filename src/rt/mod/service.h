/*
 * WorkerModule - module that is possible to serve API calls and process images
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef _SERVICE_H_
#define _SERVICE_H_

/* -------------------------------------------------------------------------- */

#include "wmodule.h"
#include "imavgreq.h"
#include "timerreq.h"
#include "rt/engine.h"
#include "rt/api/helper.h"

/* -------------------------------------------------------------------------- */

namespace mke {
namespace rt {
namespace mod {

/* -------------------------------------------------------------------------- */

class ServiceModule : public WorkerModule
{

protected:
  enum Actions { ACTION_TIMER, 
                 ACTION_GETIMAGE,
                 ACTION_SETSHUTTER,
                 NUM_ACTIONS };

  Engine                    *   engine_;            // used engine
  const dev::CamFrame<dev::Pix8>  
                            *   curr_frame_;        // currently processed frame
  mke::api::MkERequest_SetLaser laser_params_;            // last params

  std::unique_ptr<LongLivedRequest>
                                actions_[NUM_ACTIONS];  // array of all actions
  
public:
  /**
   * @brief Constructor
   */
  ServiceModule(api::ApiHelper * helper, bool is_reserved_mode):
    WorkerModule(helper, is_reserved_mode),
    engine_(nullptr)
  {
    std::memset(&laser_params_, 0, sizeof(mke::api::MkERequest_SetLaser));
  };
  
  /**
   * @brief Process one camera frame
   */
  virtual void processCameraFrame(const dev::CamFrame<dev::Pix8> * frame);
  
  /**
   * @brief Kill all active long live requests
   */
  virtual void killLongLivedRequests(api::Request * sender_ptr = nullptr);
  
  /**
   * @brief signal internal message
   */
  virtual void internalSignal(api::Request::InternalRequestType int_type,
                              api::Request * request_ptr);
  
  virtual void stateChanged(mke::api::MkEStateType state);

  /**
   * post setter for engine
   */
  void setEngine(Engine * engine) { engine_ = engine; }
  
  /**
   * @brief Module finalization
   */
  virtual void finalize();
  
protected:
  /**
   * 
   */
  inline void checkState() 
  { 
    if(api_helper_->api_resolver.getState() != mke::api::MKE_STATE_SERVICE) 
      throw api::ApiError(mke::api::MKE_REPLY_CLIENT_REQUEST_DOES_NOT_APPLY);
  };
  
  /**
   * @brief Register API callbacks
   */
  virtual void registerApiCallbacks();
  
  /**
   * @brief Unregister API callbacks
   */
  virtual void unregisterApiCallbacks();

  // callbacks -----------------------------------------------------------------
  
  /**
   * @brief Implements a callback for a test MkE API function Ping (`mke::api::MKE_REQUEST_PING`)
   * 
   * Note that every API callback should return as soon as possible
   * 
   * @param request p_request:...
   */
  void pingCallback(api::Request *request_ptr);
  
  /**
   * @brief Implements a joint callback for a test MkE API function Timer 
   * (`mke::api::MKE_REQUEST_START_TIMER`, `mke::api::MKE_REQUEST_STOP_TIMER`)
   * 
   * Note that every API callback should return as soon as possible
   * 
   * @param request p_request:...
   */
  void timerCallback(api::Request * request_ptr);
  
  /**
   * @brief return current image from camera
   *
   * @param request p_request:...
   */
  void imageCallback(api::Request * request_ptr);
  
  /**
   * @brief return current image from camera
   *
   * @param request p_request:...
   */
  void streamCallback(api::Request * request_ptr);
  
  /**
   * @brief Set/Get shutter callback
   * 
   * @param request p_request:...
   */
  void shutterCallback(api::Request * request_ptr);
  
  /**
   * @brief Set/Get shutter callback
   * 
   * @param request p_request:...
   */
  void gainCallback(api::Request * request_ptr);
  
  /**
   * @brief Set/Get laser callback
   * 
   * @param request p_request:...
   */
  void laserCallback(api::Request * request_ptr);  
};

/* -------------------------------------------------------------------------- */

} // end of namespace mke::rt::mod
} // end of namespace mke::rt
} // end of namespace mke

/* -------------------------------------------------------------------------- */

#endif // _SERVICE_H_

/* -------------------------------------------------------------------------- */