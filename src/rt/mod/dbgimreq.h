/*
 * DebugImRequest - return n frames with images back
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar, Jan Heller
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef _DBGIMREQ_H_
#define _DBGIMREQ_H_

/* -------------------------------------------------------------------------- */

#include "longreq.h"
#include "util/imavg.h"
#include "../detector.h"

/* -------------------------------------------------------------------------- */

namespace mke {
namespace rt {
namespace mod {

/* -------------------------------------------------------------------------- */

/**
 * @brief Debug Images Request makes frames with images amd reply them
 */
class DebugImRequest : public LongLivedRequest
{
  Detector      &   detector_;      // reference to active detector
  const mke::api::MkERequest_DebugImages
                *   params_;        // pointer to request parameters
  int               countdown_;     // how many images should be saved
  char *            data_ptr_;      // pointer to empty space in payload
  api::Reply    *   reply_ptr_;     // prepared reply with buffer
  
public:  
  /**
   * @brief Contructor
   * 
   * @param request     request that is parent of LongLivedRequest
   */
  DebugImRequest(api::ApiHelper * helper, api::Request * request_ptr, 
                  Detector & detector);
  
  /**
   * @brief Process current frame
   * 
   * @param frame   processed frame
   * @result        keep request alive
   */
  virtual bool processCameraFrame(const dev::CamFrame<dev::Pix8> * frame);
};

/* -------------------------------------------------------------------------- */

} // end of namespace mke::rt::mod
} // end of namespace mke::rt
} // end of namespace mke

/* -------------------------------------------------------------------------- */

#endif  // _DBGIMREQ_H_

/* -------------------------------------------------------------------------- */
