/*
 * SetShutterRequest - send shutter after is stable and images are ok
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef _SHUTTERREQ_H_
#define _SHUTTERREQ_H_

/* -------------------------------------------------------------------------- */

#include "longreq.h"
#include "dev/img.h"
#include "rt/engine.h"


/* -------------------------------------------------------------------------- */

namespace mke {
namespace rt {
namespace mod {

/* -------------------------------------------------------------------------- */

/**
 * @brief SetShutterRequest set shutter and wait until it will be setted corretly
 */
class SetShutterRequest : public LongLivedRequest
{
protected:
  Engine            *   engine_;
  uint32_t              exposure_time_;
  int                   countdown_;
  
public:  
  /**
   * @brief Contructor
   * 
   * @param request     request that is parent of LongLivedRequest
   */
  SetShutterRequest(api::ApiHelper * helper, api::Request * request_ptr,
                    Engine * engine, uint32_t exposure_time);
  
  /**
   * @brief Process current frame
   * 
   * @param frame   processed frame
   * @result        keep request alive
   */
  virtual bool processCameraFrame(const dev::CamFrame<dev::Pix8> * frame);
};

/* -------------------------------------------------------------------------- */

} // end of namespace mke::rt::mod
} // end of namespace mke::rt
} // end of namespace mke

/* -------------------------------------------------------------------------- */

#endif  // _SHUTTERREQ_H_

/* -------------------------------------------------------------------------- */
