/*
 * Worker - serves most of API requests
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar, Jan Heller
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef _WORKER_H_
#define _WORKER_H_

/* -------------------------------------------------------------------------- */

#include "util/logging.h"
#include "mkert_config.h"

/* -------------------------------------------------------------------------- */

#include "workerbase.h"

#include "mkeapi.h"
#include "api/queue.h"
#include "api/resolver.h"
#include "detector.h"

#include "engine.h"
#include "loadeng.h"

#include "mod/wmodule.h"
#include "mod/depthsens.h"
#ifdef MKE_USE_RESERVED_API
    #include "mod/service.h"
#endif

#include "devinfo.h"
#include "profiler.h"

#include "util/governon.h"
#include "autoexp.h"

/* -------------------------------------------------------------------------- */

namespace mke {
namespace rt {

/* -------------------------------------------------------------------------- */

/**
 * @brief Worker = API request processor
 * 
 * This class serve clients' API request
 */

class Worker : public WorkerBase, protected mod::WorkerModule
{
protected:
  enum ProbeProfile { PROBE_IDLE, PROBE_MEASURE, PROBE_SERVICE };
  Detector              &   detector_;          // currently used detector

  std::unique_ptr<Engine>   engine_;            // used engine
  
  const dev::CamFrame<dev::Pix8> 
                        *   curr_frame;         // currently processed image
  
  AutoExposure              autoexp_;

  // used modules
  
  mod::DepthSensModule      mod_depth_;         // depth sens module
#ifdef MKE_USE_RESERVED_API
  mod::ServiceModule        mod_service_;       // reserved service module
#endif
  std::vector<mod::WorkerModule *>
                            modules_;           // list of used modules
  using WorkerBase::api_helper_;

  mke::util::Governon       gov_;               // governon
  Profiler                  profiler_;          // profile manager
  StatsManager              stats_;             // stats manager
  
public:
  /*
   * @brief Constructor of Worker that should serve clients requests
   * 
   * @param config          general configuration object
   * @param api_pool        API pool with requests
   * @param api_resolver    API resolver
   */
  Worker(const MkertConfig &config, Detector &detector, api::MemoryPool &api_pool, 
         api::Resolver &api_resolver, const char * mkert_dir, const DeviceInfo & dev_info) : 
    WorkerBase(config, api_pool, api_resolver, mkert_dir, dev_info),
    mod::WorkerModule(nullptr, config.getAPIConfig().reserved_mode),
    detector_(detector),
    engine_(nullptr),
    autoexp_(config.getAutoExpConfig(), config.getCameraConfig()),
    mod_depth_(&(WorkerBase::api_helper_), is_reserved_mode_, detector, &autoexp_, config.getGeneralConfig()),
#ifdef MKE_USE_RESERVED_API    
    mod_service_(&(WorkerBase::api_helper_), is_reserved_mode_),
#endif
    gov_(false, config.getGeneralConfig().gov_max_on, 
         config.getGeneralConfig().gov_min_off,
         config.getGeneralConfig().gov_off_factor),
    profiler_(config.getGeneralConfig())
  {};

  /**
   * @brief Destructor
   */
  virtual ~Worker() {};
  
  /**
   * @brief Initialization of worker
   */
  virtual void init();

  /**
   * @brief Finalization of worker
   */
  virtual void finalize();
  
  /**
   * @brief Starts processing of requests
   */
  virtual void start();

  /**
   * @brief Is application in reserved mode
   */
  inline bool isReservedMode() 
  {
    return is_reserved_mode_;
  }
  
protected:
  /**
   * @brief Register API callbacks
   */
  virtual void registerApiCallbacks();

  /**
   * @brief Unregister API callbacks
   */
  virtual void unregisterApiCallbacks();
    
  /**
   * @brief find system version from filesystem
   */
  void findSystemInfo();
  
  /**
   * @brief change state of application
   */
  void applyState(mke::api::MkEStateType new_state);
  
  /**
   * @brief Process of one frame
   * 
   * @result bool should worker continue to next frame/action
   */
  virtual bool process();
  
  /**
   * @brief Process one camera frame
   */
  virtual void processCameraFrame(const dev::CamFrame<dev::Pix8> * frame) {};
  
  virtual double getTemperature(int sensorId) const;
  
  void checkTemperatures();
  
#ifdef MKE_USE_RESERVED_API    
  
  virtual void getStats(StatsType & stats) const;

  virtual void resetStats() {};
#endif  
  // callbacks -----------------------------------------------------------------
  
  /**
   * @brief Implements a callback for a test MkE API function Reserved (`mke::api::MKE_REQUEST_INTERNAL`)
   * 
   * Note that every API callback should return as soon as possible
   * 
   * @param request p_request:...
   */
   virtual void internalCallback(api::Request *request_ptr);
  
  /**
   * @brief Implements a callback for set state MkE API functions
   * (`mke::api::MKE_REQUEST_SET_STATE_???`)
   * 
   * Note that every API callback should return as soon as possible
   * 
   * @param request p_request:...
   */
  void setStateCallback(api::Request *request_ptr);
  
  /**
   * @brief Implements a callback for get reserved info MkE API functions
   * (`mke::api::MKE_REQUEST_GET_RESERVED_INFO`)
   * 
   * Note that every API callback should return as soon as possible
   * 
   * @param request p_request:...
   */
  void reservedCallback(api::Request * request_ptr);
    
  /**
   * @brief get/set detector policy 
   *
   * @param request p_request:...
   */
  void policyCallback(api::Request * request_ptr);      
  
  /**
   * @brief get/reset stats
   *
   * @param request p_request:...
   */
  void statsCallback(api::Request * request_ptr);  
};

/* -------------------------------------------------------------------------- */

} // namespace mke::rt
} // namespace mke

/* -------------------------------------------------------------------------- */

#endif // _WORKER_H_
