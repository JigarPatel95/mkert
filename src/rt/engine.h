/*
 * Engine - interface for engine that is connection to devices
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef _ENGINE_H_
#define _ENGINE_H_

/* -------------------------------------------------------------------------- */

#include "dev/img.h"
#include "mkert_config.h"
#include "mkeapi.h"

#include "rt/profiler.h"
#include "rt/statsmng.h"

/* -------------------------------------------------------------------------- */

namespace mke {
namespace rt {
  
/* -------------------------------------------------------------------------- */

#define MKE_ENGINE_DROPLOG_TIMEOUT 10000

/* -------------------------------------------------------------------------- */

// camera with queue
    
class Engine : public Profiled, public StatsGenerating
{
protected:
  const MkertConfig  &  cfg_;                   // global cfg
  
public:
  // constructor
  Engine(const MkertConfig & cfg)
  : cfg_(cfg)
  {};
  
  // destructor
  virtual ~Engine() {};
  
  // initialization of devices
  virtual void initialize() {};

  // get next frame from camera
  virtual const dev::CamFrame<dev::Pix8> * getFrame(uint32_t timout = 0xffffffff) = 0;
  
  // release frame
  virtual void releaseFrame(const dev::CamFrame<dev::Pix8> * frame) = 0;
  
  // run lasers
  virtual void stateChanged(mke::api::MkEStateType state) = 0;
  
  // fire some pattern
  virtual void fireLaserPattern(int pattern, float strobe_len, float strobe_off) = 0;
  
  // set shutter speed
  virtual void setShutterSpeed(uint32_t exposure_) = 0;
  
  // get shutter speed
  virtual uint32_t getShutterSpeed() = 0;
  
  // get analog gain
  virtual uint32_t getAnalogGain() = 0;
  
  // get digital gain
  virtual uint32_t getDigitalGain() = 0;  
  
  // set ISO - combination of analog and digital values
  virtual bool setISO(uint32_t value) = 0;
  
};

/* -------------------------------------------------------------------------- */

} // end of mke::rt namespace
} // end of mke namespace

/* -------------------------------------------------------------------------- */

#endif // _ENGINE_H_
