/* 
 * Dispatcher - Handles all incoming (network/serial/...) API requests
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#ifndef _DISPATCHER_H_
#define _DISPATCHER_H_

#include <boost/asio.hpp>

#include "util/logging.h"
#include "mkert_config.h"

#include "mkeapi.h"
#include "rt/api/memory_pool.h"
#include "rt/api/queue.h"
#include "rt/api/request_sink.h"
#include "rt/tcp_listener.h"
#include "rt/serial_listener.h"

namespace mke {
namespace rt {

  /**
   * @brief Handles all incoming (network/serial/...) API requests, parses them
   * and serializes them into the API queue. 
   * 
   */  
class Dispatcher : public api::RequestSink {
private:
  const MkertConfig & config_;  
  const MkertConfig::DispatcherConfig & listener_config_;
  api::MemoryPool & api_pool_;
  api::Queue & api_queue_;
  mke::api::MkETerminateMethodType exit_method_;
  
  boost::asio::io_service io_service_; 
  boost::asio::io_service strand_;
  std::vector<std::shared_ptr<Listener> > listeners_;
  boost::asio::signal_set signals_;
  
public:
  Dispatcher(const MkertConfig &config, api::MemoryPool & api_pool, api::Queue & api_queue) : 
  config_(config), 
  listener_config_ (config.getListenerConfig()),
  api_pool_(api_pool),
  api_queue_(api_queue),
  exit_method_(mke::api::MKE_TERMINATE_UNDEF),
  io_service_(),
  signals_(io_service_)
   {
    signals_.add(SIGINT);
    signals_.add(SIGTERM);
  }
  
  
  mke::api::MkETerminateMethodType getExitReason(void) {
    return exit_method_;
  }
   
  
// ==========================================================================
/** @name Initialization
 *  Dispatcher initialization
 */
///@{   

  void init(void) {
    LOG_INFO << "Dispatcher initialization";
    
     // Initialize TCP servers
     for (std::size_t i = 0; i != listener_config_.tcp_ports.size(); ++i) 
       listeners_.push_back(std::shared_ptr<Listener>(
         new TCPListener(io_service_, api_pool_, *this,  
                         listener_config_.tcp_ports[i])));
     
     // Initialize Serial servers
     boost::asio::serial_port::baud_rate    baudrate(listener_config_.serial_baudrate);
     boost::asio::serial_port::parity::type       parity;
     boost::asio::serial_port::stop_bits::type    stopbits;
     boost::asio::serial_port::flow_control::type flowcontrol;
     
     // parse serial parity
     
     if(listener_config_.serial_parity == "NONE")
       parity = boost::asio::serial_port::parity::none;
     else if(listener_config_.serial_parity == "EVEN")
       parity = boost::asio::serial_port::parity::even;
     else if(listener_config_.serial_parity == "ODD")
       parity = boost::asio::serial_port::parity::odd;
     else
       throw std::runtime_error("Unknown value for serial_parity");
     
     // parse serial stopbits
     
     if(listener_config_.serial_stopbits == "1")
       stopbits = boost::asio::serial_port::stop_bits::one;
     else if(listener_config_.serial_stopbits == "1.5")
       stopbits = boost::asio::serial_port::stop_bits::onepointfive;
     else if(listener_config_.serial_stopbits == "2")
       stopbits = boost::asio::serial_port::stop_bits::two;
     else 
       throw std::runtime_error("Unknown value for serial_stopbits");
     
     // parse serial flowcontrol
     
     if(listener_config_.serial_flowcontrol == "NONE")
       flowcontrol = boost::asio::serial_port::flow_control::none;
     else if(listener_config_.serial_flowcontrol == "HW")
       flowcontrol = boost::asio::serial_port::flow_control::hardware;
     else if(listener_config_.serial_flowcontrol == "SW")
       flowcontrol = boost::asio::serial_port::flow_control::software;
     else
       throw std::runtime_error("Unknown value for serial_flowcontrol");
     
     for (std::size_t i = 0; i != listener_config_.serial_ports.size(); ++i) 
       listeners_.push_back(std::shared_ptr<Listener>(
         new SerialListener(io_service_, api_pool_, *this,
                            listener_config_.serial_ports[i], baudrate, 
                            boost::asio::serial_port::parity(parity), 
                            boost::asio::serial_port::stop_bits(stopbits), 
                            boost::asio::serial_port::flow_control(flowcontrol))));
     
     // Signal handlers
    initInterruptHandler();
  }
  
  void close(void) {
    for (std::size_t i = 0; i != listeners_.size(); ++i)
      listeners_[i]->stop();    
  }
    
  void initInterruptHandler() {
     signals_.async_wait([this](boost::system::error_code ec, int  signal_number)
        {
          if (!ec)
            {
              api::Request *request_ptr;
              
              if (api_pool_.request(request_ptr))
              {
                // Let's try to shutdown gracefully
                LOG_INFO << "Received signal " << signal_number << ", emitting MKE_REQUEST_TERMINATE/MKE_TERMINATE_BY_EXIT";                
                request_ptr->setInternalType(mke::rt::api::Request::INT_REQUEST_TERMINATE);
                request_ptr->getParamsBuffer<mke::api::MkERequest_Terminate>()->method = mke::api::MKE_TERMINATE_BY_EXIT;
                request(request_ptr);
              }
              else
              {
                // 'Force' shutdown - have no memmory
                
                LOG_INFO << "Received signal " << signal_number << ", calling stop()";
                stop();
              }
            }
          else
           {
            LOG_INFO << ec.message();
           }           
        });
  }
  
///@}  

// ==========================================================================
/** @name Runtime
 *  Dispatcher runtime
 */
///@{ 

void start() {
  bool do_run = false;
    
  for (std::size_t i = 0; i != listeners_.size(); ++i)
    do_run = do_run | listeners_[i]->start();   
    
  if (!do_run)
    {
      LOG_WARN << "No listener could be started, emitting MKE_REQUEST_TERMINATE/MKE_TERMINATE_BY_EXIT"; 
      
      api::Request *request_ptr;
      if (api_pool_.request(request_ptr))
        {
          exit_method_ = mke::api::MKE_TERMINATE_BY_EXIT;          
          request_ptr->setInternalType(mke::rt::api::Request::INT_REQUEST_TERMINATE);
          request_ptr->getParamsBuffer<mke::api::MkERequest_Terminate>()->method = exit_method_;
          api_queue_.push(request_ptr);
        }
    }
   else
   {
    io_service_.run();
   }
}
  
  void request(api::Request *request_ptr) {
    if (request_ptr->getType() == mke::api::MKE_REQUEST_TERMINATE ||
         (request_ptr->isInternal() && 
            request_ptr->getInternalType() == api::Request::INT_REQUEST_TERMINATE))
     {
       // check input parameters
       
       uint32_t method = request_ptr->getParamsBuffer<mke::api::MkERequest_Terminate>()->method;
#ifdef MKE_IS_TERMINATE_METHOD_VALID            
       bool can_exit = config_.getAPIConfig().reserved_mode || request_ptr->isInternal();
       if(MKE_IS_TERMINATE_METHOD_VALID(method, can_exit))
#else
       if(method == mke::api::MKE_TERMINATE_BY_SHUTDOWN || method == mke::api::MKE_TERMINATE_BY_REBOOT
            || method == mke::api::MKE_TERMINATE_BY_EXIT)
#endif                
        {
          // store exit method and propagate to the worker
          
          exit_method_ = mke::api::MkETerminateMethodType(method);
          api_queue_.push(request_ptr);
          
          // stop() - will be called by main thread/loop after worker thread stop
        }
     }
      
   if (!exit_method_)
     {
       if (!api_queue_.push(request_ptr))
         {
           // This should never happen!!! 
           // The API queue should ALWAYS be longer that the maximum number of API requests
           if (request_ptr->getReplySink())
             request_ptr->getReplySink()->reply(api_pool_.requestStock(mke::api::MKE_REPLY_SERVER_BUSY));
           request_ptr->release();
         }
     }
  }
  
  void stop(void) {
    io_service_.stop();
  }
};

///@} 

} // namespace rt
} // namespace mke

#endif // _DISPATCHER_H_
