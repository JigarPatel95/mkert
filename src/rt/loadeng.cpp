/*
 * RasPiEngine - specific engine for Raspberry PI
 *    controls all devices (camera, lasers) and sync them
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

// includes

#include "loadeng.h"
#include "util/pgm.h"
#ifdef USE_PNG
#include "util/pngenc.h"
#endif // USE_PNG
#include <boost/filesystem.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <thread>
#include <iostream>

/* -------------------------------------------------------------------------- */

using namespace mke;
using namespace mke::rt;
using namespace mke::dev;

using namespace boost::filesystem;

/* -------------------------------------------------------------------------- */

// LoadEngine implementation

/* -------------------------------------------------------------------------- */

void LoadEngine::initialize()

{
  // check validity of configuration
  
  if(eng_cfg_.load_dir.length() <= 0)
    throw std::runtime_error("For LoadEngine is neccessary define config item Engine.load_dir");
  
  // wanna fake images ?
  
  fake = eng_cfg_.load_dir == "NOISE";
  
  if(fake)
    return;
  
  // load all files from directory
  
  path p(eng_cfg_.load_dir);
  
  // go through all nodes in directory
  
  for (directory_entry& it: directory_iterator(p))
    {
      if (is_regular_file(it.path())) 
        files_.push_back(it.path().string());
    }
    
  // sort images by name
    
  std::sort(files_.begin(), files_.end());
}

/* -------------------------------------------------------------------------- */

const CamFrame<Pix8> * LoadEngine::getFrame(uint32_t timeout)

{
  // Load keep just one file, so must wait
  
  if(!ready)
    return nullptr;
  
  // sleep for some time
  
  if(timeout > 0)
    std::this_thread::sleep_for(std::chrono::milliseconds(timeout));

  if(!running)
    return nullptr;

  // fake or read ?
  
  if(fake)
  {
    generateRandom(curr_image_);
    fps_cnt_.tick();

  }
  else
  {
    unsigned int i = 0;
    for(; i < files_.size(); ++i)
      {
        try
          {
            const char * suffix = files_[im_index_].c_str() + files_[im_index_].length()-3;
            if((suffix[0] == 'p' || suffix[0] == 'P') 
              && (suffix[1] == 'g' || suffix[1] == 'G') 
              && (suffix[2] == 'm' || suffix[2] == 'M'))              
                mke::util::PGM::read(files_[im_index_], curr_image_, eng_cfg_.load_stride);
#ifdef USE_PNG                
            else if((suffix[0] == 'p' || suffix[0] == 'P') 
              && (suffix[1] == 'n' || suffix[1] == 'N') 
              && (suffix[2] == 'g' || suffix[2] == 'G'))              
                mke::util::PNG::decode<Pix8>(files_[im_index_], curr_image_, eng_cfg_.load_stride);
#endif // USE_PNG            
            im_index_ = (im_index_+1) % files_.size();
            fps_cnt_.tick();
            break;
          } 
        catch(std::exception & e)
          { 
            LOG_WARN << e.what() << ", " << files_[im_index_];
          }
        im_index_ = (im_index_+1) % files_.size();
      }
    
    if(i >= files_.size())
      throw std::runtime_error("No data available");
  }
  
  // update flags and im_index_
  
  ready = false;
  
  // be sure that curr_image_ has not been reallocated - so reassign image
  
  curr_frame_ = CamFrame<mke::dev::Pix8>(curr_image_, 0, frame_index_, 
                                         num_detectors_ > 0 ? frame_index_ % num_detectors_ : 0);
  
  frame_index_++;
  CamFrame<mke::dev::Pix8>::setTimerToNow(curr_frame_.timer);
  
  return &curr_frame_;
}

/* -------------------------------------------------------------------------- */

void LoadEngine::releaseFrame(const CamFrame<Pix8> * frame)

{
  assert(frame == &curr_frame_);
  
  ready = true;
}


/* -------------------------------------------------------------------------- */

void LoadEngine::generateRandom(MemImage<Pix8> & out)

{
  const int s = 11;

  for(int r = 0; r < out.h; ++r)
    {
      Pix8 * p = out.row(r);
      Pix8 val = 0;

      if(r % s == 0)            // delimiter between rows
        {
          for(int c = 0; c < out.w; ++c, ++p)
            *p = 0;
        }
      else if(r % s == 1)       // 1st row after delimiter
        {
          for(int c = 0; c < out.w; ++c, ++p)
            if(c % s == 0)
              {
                *p = 0;
                val = rand() % 256;
              }
            else
              *p = val;
        }
      else                       // general row
        {
          memcpy(p, out.row(r-1), out.w);
        }
    }
}

/* -------------------------------------------------------------------------- */

void LoadEngine::getStats(StatsType& stats) const
{
  stats["im_index"] = im_index_;
  stats["im_width"] = curr_frame_.w;
  stats["im_height"] = curr_frame_.h;
}

/* -------------------------------------------------------------------------- */
