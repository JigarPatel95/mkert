/* 
 * Detector
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *
 */

#include <boost/filesystem.hpp>

#include "util/logging.h"
#include "util/crc32.h"
#include "util/timers.h"

#include "detector.h"

using nlohmann::json;

void mke::rt::Detector::init(void) 
{
  auto dconfig = config_.getDetectorConfig();

  if (dconfig.mkedet_path.size() > 0)
    {
      for(auto  det : dconfig.mkedet_path)
        {
            LOG_INFO << "Loading MkE detector file: " << det;
            detector_.addCalibFile(det.c_str());
        }    
        detector_.setDefaultProfile();      
    }
  else
    {
      LOG_WARN << "No calibration file provided, detector WILL NOT WORK";
    }
}


void mke::rt::Detector::setProfile(const ProfileType & tree)
{
  try 
    {      
      detector_.setProfile(tree.dump().c_str());
    }
    catch (std::exception &e)
    {
      LOG_WARN << e.what();
      throw;
    }
}

void mke::rt::Detector::getProfile(ProfileType & tree) const
{
  std::stringstream ss(detector_.getProfile());
  tree = json::parse(ss.str());  
}

void mke::rt::Detector::validateProfile(const ProfileType & profile) const
{  
  try
    {
      detector_.validateProfile(profile.dump().c_str());
    }
  catch (std::exception &e)
    {
      LOG_WARN << e.what();
      throw;
    }
}


/* -------------------------------------------------------------------------- */

void mke::rt::Detector::getStats(StatsType & stats) const 
{
  stats["frame_asm_timer_ms"] = frame_asm_timer_.elapsed<double, std::milli>();
  
  StatsType mkedd_tree;
  stats["mkedd"] = json::parse(detector_.getStats());
}

/* -------------------------------------------------------------------------- */

void mke::rt::Detector::resetStats()
{
  frame_asm_timer_.reset();
  detector_.resetStats();
}

// ==========================================================================
// MkE data frame processing

std::size_t mke::rt::Detector::getMaxFrameSize(void) 
{
  // We use sizeof(mke::api::MkEFrameDataType4) since mke::api::MkEFrameDataType4
  // is the largest of MkE data frame types
  return detector_.getMaxNoDetections() * sizeof(MKE_LARGEST_FRAME_TYPE) + sizeof(mke::api::MkEFrameFooter);
}
  

void mke::rt::Detector::addToFrameType1(uint8_t* &frame_buffer, uint32_t &num_bytes, 
                                       uint16_t &num_data, uint32_t &crc32, const int det_idx) 
{
  if (!detector_.getNoDetections(det_idx))
    return;
  
  const mke::dd::PointData *detections;
  uint32_t detections_size = 0;
  uint32_t no_detections = 0;
  
  detector_.getDetections(det_idx, detections, detections_size);
  mke::api::MkEFrameItem1 *frame_data = reinterpret_cast<mke::api::MkEFrameItem1 *>(frame_buffer);
  
  for (unsigned int i = 0; i < detections_size; i++)
    {
      if (!detections[i].confidence)
        continue;
      
      // Fill out the frame datum
      frame_data->uid = detections[i].uid;
      frame_data->x = detections[i].x;
      frame_data->y = detections[i].y;
      frame_data->z = detections[i].z;
      
      // Compute the running CRC32 value
      crc32 = mke::util::Crc32::crcBuffer(frame_data, sizeof(mke::api::MkEFrameItem1), crc32);        
      frame_data++;
      no_detections++;
    }
      
  uint32_t frame_bytes = no_detections * sizeof(mke::api::MkEFrameItem1);
  num_data += no_detections;
  num_bytes += frame_bytes;
  frame_buffer += frame_bytes;
}
  
  
void mke::rt::Detector::addToFrameType2(uint8_t* &frame_buffer, uint32_t &num_bytes, 
                                        uint16_t &num_data, uint32_t &crc32, const int det_idx) 
{
  if (!detector_.getNoDetections(det_idx))
    return;
  
  const mke::dd::PointData *detections;
  uint32_t detections_size = 0;
  uint32_t no_detections = 0;
  
  detector_.getDetections(det_idx, detections, detections_size);  
  mke::api::MkEFrameItem2 *frame_data = reinterpret_cast<mke::api::MkEFrameItem2 *>(frame_buffer);

  for (unsigned int i = 0; i < detections_size; i++)
    {
      if (!detections[i].confidence)
        continue;
      
      // Fill out the frame datum
      frame_data->uid = detections[i].uid;
      frame_data->x = detections[i].x;
      frame_data->y = detections[i].y;
      frame_data->z = detections[i].z;
      frame_data->lid = detections[i].id1;
      frame_data->did = detections[i].id2;
      
      // Compute the running CRC32 value
      crc32 = mke::util::Crc32::crcBuffer(frame_data, sizeof(mke::api::MkEFrameItem2), crc32);        
      frame_data++;
      no_detections++;
    }
      
  std::size_t frame_bytes = no_detections * sizeof(mke::api::MkEFrameItem2);
  num_data += no_detections;
  num_bytes += frame_bytes;
  frame_buffer += frame_bytes;
}
  
#ifdef MKE_USE_RESERVED_API  
void mke::rt::Detector::addToFrameTypeReserved1(uint8_t* &frame_buffer, uint32_t &num_bytes, 
                                        uint16_t &num_data, uint32_t &crc32, const int det_idx) 
{
  if (!detector_.getNoDetections(det_idx))
    return;
  
  const mke::dd::PointData *detections;
  uint32_t detections_size = 0;
  uint32_t no_detections = 0;
  
  detector_.getDetections(det_idx, detections, detections_size);  
  mke::api::MkEFrameItemReserved1 *frame_data = 
      reinterpret_cast<mke::api::MkEFrameItemReserved1 *>(frame_buffer);

  for (unsigned int i = 0; i < detections_size; i++)
    {
      if (!detections[i].confidence)
        continue;
      
      // Fill out the frame datum
      frame_data->uid = detections[i].uid;
      frame_data->x = detections[i].x;
      frame_data->y = detections[i].y;
      frame_data->z = detections[i].z;
      frame_data->lid = detections[i].id1;
      frame_data->did = detections[i].id2;
      frame_data->u = detections[i].u;
      frame_data->v = detections[i].v;
      
      // Compute running CRC32 value
      crc32 = mke::util::Crc32::crcBuffer(frame_data, sizeof(mke::api::MkEFrameItemReserved1), crc32);        
      frame_data++;
      no_detections++;
    }
      
  std::size_t frame_bytes = no_detections * sizeof(mke::api::MkEFrameItemReserved1);
  num_data += no_detections;
  num_bytes += frame_bytes;
  frame_buffer += frame_bytes;
}
  
  
void mke::rt::Detector::addToFrameTypeReserved2(uint8_t* &frame_buffer, uint32_t &num_bytes, 
                                        uint16_t &num_data, uint32_t &crc32, const int det_idx) 
{
  if (!detector_.getNoDetections(det_idx))
    return;
  
  const mke::dd::PointData *detections;
  uint32_t detections_size = 0;
  uint32_t no_detections = 0;
  
  detector_.getDetections(det_idx, detections, detections_size);   
  mke::api::MkEFrameItemReserved2 *frame_data = 
      reinterpret_cast<mke::api::MkEFrameItemReserved2 *>(frame_buffer);

  for (unsigned int i = 0; i < detections_size; i++)
    {
      if (!detections[i].confidence)
        continue;
      
      // Fill out the frame datum
      frame_data->uid = detections[i].uid;
      frame_data->x = detections[i].x;
      frame_data->y = detections[i].y;
      frame_data->z = detections[i].z;
      frame_data->lid = detections[i].id1;
      frame_data->did = detections[i].id2;
      frame_data->u = detections[i].u;
      frame_data->v = detections[i].v;
      frame_data->det = 0;
      frame_data->bwidth = 0; 
      frame_data->sigma = detections[i].sigma2D;
      frame_data->resp = detections[i].confidence;
        
      // Compute the running CRC32 value
      crc32 = mke::util::Crc32::crcBuffer(frame_data, sizeof(mke::api::MkEFrameItemReserved2), crc32);        
      frame_data++;
      no_detections++;      
    }
      
  std::size_t frame_bytes = no_detections * sizeof(mke::api::MkEFrameItemReserved2);
  num_data += no_detections;
  num_bytes += frame_bytes;
  frame_buffer += frame_bytes;
}
#endif

void mke::rt::Detector::clearDetections(const int det_idx) {
  int det_min = (det_idx == -1) ? 0 : det_idx;
  int det_max = (det_idx == -1) ? detector_.getNoDetectors() : det_idx + 1;
  
  for (int i = det_min; i < det_max; i++)
    detector_.clearDetections(i);
}

void mke::rt::Detector::assembleFrame(void *buffer, uint32_t &num_bytes, uint16_t &num_data, 
                                      const mke::api::MkEFrameType frame_type, const int det_idx)
{

  frame_asm_timer_.start();
  
  uint8_t *frame_buffer = (uint8_t *) buffer;
  int det_min = (det_idx == -1) ? 0 : det_idx;
  int det_max = (det_idx == -1) ? detector_.getNoDetectors() : det_idx + 1;
  
  uint32_t crc32 = 0xFFFFFFFF;
  num_data = 0;
  num_bytes = 0;
    
  for (int i = det_min; i < det_max; i++)
    {        
      if (frame_type == mke::api::MKE_FRAME_TYPE_1)
        addToFrameType1(frame_buffer, num_bytes, num_data, crc32, i);
      else if (frame_type == mke::api::MKE_FRAME_TYPE_2)
        addToFrameType2(frame_buffer, num_bytes, num_data, crc32, i);
#ifdef MKE_USE_RESERVED_API
      else if (frame_type == mke::api::MKE_FRAME_TYPE_RESERVED_1)
        addToFrameTypeReserved1(frame_buffer, num_bytes, num_data, crc32, i);           
      else if (frame_type == mke::api::MKE_FRAME_TYPE_RESERVED_2)
        addToFrameTypeReserved2(frame_buffer, num_bytes, num_data, crc32, i);           
#endif
    }

  // Add mke::api::MkEFrameFooter
  mke::api::MkEFrameFooter *footer = reinterpret_cast<mke::api::MkEFrameFooter *>(frame_buffer);
  footer->crc32 = ~crc32;
  num_bytes += sizeof(mke::api::MkEFrameFooter);
  
  frame_asm_timer_.stop();
}
