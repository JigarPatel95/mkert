/*
 * AutoExposure module
 * 
 * Copyright (c) 2019, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#ifndef _AUTOEXP_H_
#define _AUTOEXP_H_

/* -------------------------------------------------------------------------- */

#include <stdint.h>

#include <mkert_config.h>
#include "dev/img.h"

#include "engine.h"

/* -------------------------------------------------------------------------- */

namespace mke {
namespace rt {

// AutoExposure class
// triggers: - Q50 < 30
//           - Q99 > 150
//             
class AutoExposure : public Profiled {

  MkertConfig::AutoExpConfig        cfg_;
  const MkertConfig::CameraConfig & cam_cfg_;
  Engine                          * engine_;
  int                               curr_exp_;
  
  uint32_t                          hist_[256];
  uint32_t                          cnt_;
  
  int                               rep_up_, rep_down_;
  
  void computeHist(const dev::CamFrame<dev::Pix8> * frame);
  
public: 
  // constructor
  AutoExposure(const MkertConfig::AutoExpConfig & ae_cfg, 
               const MkertConfig::CameraConfig & cam_cfg, 
               Engine * engine = nullptr);
  
  // destructor
  virtual ~AutoExposure() {}
  
  void reset();
  
  bool process(const dev::CamFrame<dev::Pix8> * frame);

  void setEngine(Engine * engine);
  
  virtual void setProfile(const ProfileType & profile);

  virtual void getProfile(ProfileType & profile) const;

  virtual void validateProfile(const ProfileType & profile) const;
};

}
}

/* -------------------------------------------------------------------------- */

#endif

/* -------------------------------------------------------------------------- */
