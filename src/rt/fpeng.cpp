/*
 * FpEngine - interface for engine that is connection to devices
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 */

/* -------------------------------------------------------------------------- */

#include "fpeng.h"

/* -------------------------------------------------------------------------- */

using namespace mke::rt;
using namespace mke;

/* -------------------------------------------------------------------------- */

void FpEngine::initialize()
{
#ifdef __ANDROID__
    FpCam::registerUsbFd(cam_cfg_.usb_vender_id, cam_cfg_.usb_product_id, cam_cfg_.usb_fd);
#endif

    FpCam::initialize();

    // change the exposure settings
    
    FpEngine::setShutterSpeed(cam_cfg_.shutter_speed);
    FpEngine::setISO(cam_cfg_.iso);
    FpCam::setFps(cam_cfg_.fps);
}

/* -------------------------------------------------------------------------- */
  
void FpEngine::stateChanged(mke::api::MkEStateType state)
{
  if(state == mke::api::MKE_STATE_IDLE)
    {
      if(last_state_ != mke::api::MKE_STATE_DEPTH_SENSOR)
        {
            // change the exposure settings
            
            FpEngine::setShutterSpeed(cam_cfg_.shutter_speed);
            FpEngine::setISO(cam_cfg_.iso);
        }

      // stop the lasers
      FpCam::setLaserSequence(0, 0);
      stopCapture();
    }
  else
    {

      if(state == mke::api::MKE_STATE_DEPTH_SENSOR)
        {
            // start flickering with lasers
            startSync();
        }
      
      startCapture();
    }
    
  last_state_ = state;
}

/* -------------------------------------------------------------------------- */

void FpEngine::startSync()
{
    uint16_t seq = 0x0000;
    unsigned int det_num = detector_.getNoDetectors();
    switch(det_num)
    {
      case 4:
          seq |= (0x0f & detector_.getLaserPattern(3)) << 12;
      case 3:
          seq |= (0x0f & detector_.getLaserPattern(2)) << 8;
      case 2:
          seq |= (0x0f & detector_.getLaserPattern(1)) << 4;
      case 1:
          seq |= 0x0f & detector_.getLaserPattern(0);
      break;
      default:
        throw std::runtime_error("Unsupported number of detectors");
    }
    LOG_INFO << "sync " << seq << " at " << det_num;

    if(det_num == 1)
    {
        det_num = 4;
        seq = seq | (seq << 4) | (seq << 8) | (seq << 12);
    }
    else if(det_num == 2)
    {
        det_num = 4;
        seq = seq | (seq << 8);
    }
    FpCam::setLaserSequence(seq, det_num);
}

/* -------------------------------------------------------------------------- */

void FpEngine::videoFrameReceived(const dev::Pix8 * data, int w, int h, int memw, uint16_t phase)

{

  uint64_t now;
  mke::dev::CamFrame<dev::Pix8>::setTimerToNow(now);
  
  // lock the queue
  
  std::unique_lock<std::mutex> lock(mtx_frames_);
  
  unsigned int num_det = detector_.getNoDetectors();
  if(num_det == 0)
      num_det = 1;
  frames_received_++;

  if(frames_.size() < max_frames_)
    {
        frames_.push(dev::CamFrame<dev::Pix8>(const_cast<dev::Pix8 *>(data),
                                            w, h, memw, now, frames_received_-1, phase%num_det));
        
        lock.unlock();
        cv_frames_.notify_one();
        return;
    }
  else
    {
        frames_droped_++;
        if(dropped_stamp_ > now || (now-dropped_stamp_) > MKE_ENGINE_DROPLOG_TIMEOUT)
        {
          std::cout << "! Too much frames in Q. Already dropped " 
              << frames_droped_ << " frames." << std::endl;
          dropped_stamp_ = now;
        }
    }

  // return back buffer if not used
  
  returnBackBuffer(data);
}

/* -------------------------------------------------------------------------- */

const dev::CamFrame<dev::Pix8> * FpEngine::getFrame(uint32_t timeout)

{
  // check for the exception in thread proc
    
  if (err_)
      std::rethrow_exception(err_);
    
  // lock the queue
  
  std::unique_lock<std::mutex> lock(mtx_frames_);
  
  // no new frame available
  
  while(!frames_.size())
    if(cv_frames_.wait_for(lock, std::chrono::milliseconds(timeout)) 
                                                    == std::cv_status::timeout)
      return NULL;

  // get next frame and lock it
  
  dev::CamFrame<dev::Pix8> & frame = frames_.front();
//  lockBuffer(frame.header_);

  // return it
  
  return &frame;
}

/* -------------------------------------------------------------------------- */

void FpEngine::releaseFrame(const dev::CamFrame<dev::Pix8> * frame)

{
  // lock the queue
  
  std::lock_guard<std::mutex> lock(mtx_frames_);
  
  // check if releasing last getted frame
  
  assert(frame == &frames_.front());

  dev::CamFrame<dev::Pix8> & curr = frames_.front();
  
//  unlockBuffer(curr.header_);
  returnBackBuffer(frame->mem);
  
  // release frame from queue
  frames_.pop();
}

/* -------------------------------------------------------------------------- */

void FpEngine::fireLaserPattern(int pattern, float strobe_len, float strobe_off) {
    uint16_t seq = 0x0f & pattern;
    seq = seq | (seq << 4) | (seq << 8) | (seq <<12);
    FpCam::setLaserSequence(seq, 4);

    std::this_thread::sleep_for(std::chrono::milliseconds(200));
}

/* -------------------------------------------------------------------------- */

void FpEngine::setShutterSpeed(uint32_t exposure) {
    if (exposure > 0)
        FpCam::setShutterSpeed(exposure/1000.0);
    else
        FpCam::setShutterSpeed(exposure, true);
}

/* -------------------------------------------------------------------------- */

uint32_t FpEngine::getShutterSpeed() { 
    return round(FpCam::getShutterSpeed()*1000);
}

/* -------------------------------------------------------------------------- */

uint32_t FpEngine::getAnalogGain() { return 1000; }

/* -------------------------------------------------------------------------- */

uint32_t FpEngine::getDigitalGain() { 
    return round(FpCam::getGain()*1000); 
}

/* -------------------------------------------------------------------------- */

bool FpEngine::setISO(uint32_t value) { 
    if(value > 0)
        FpCam::setGain(value/100); 
    else
        FpCam::setGain(value, true);
    return true;
}

/* -------------------------------------------------------------------------- */

void FpEngine::getStats(StatsType & stats) const
{
  stats["im_width"] = this->getImageWidth();
  stats["im_height"] = this->getImageHeight();
}

/* -------------------------------------------------------------------------- */
