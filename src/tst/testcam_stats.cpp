/*
 * testcam_stats - save stats for evaluating exposure times
 *
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 * Ideas based on code from RaspiVidYUV cloned from
 *                                  https://github.com/raspberrypi/userland.git
 */

/* -------------------------------------------------------------------------- */

#include <time.h>
#include <vector>
#include <iostream>
#include <fstream>

#include "../dev/raspicam.h"


/* -------------------------------------------------------------------------- */

namespace mke {
namespace tst {

/* -------------------------------------------------------------------------- */

using namespace mke::dev;

/* -------------------------------------------------------------------------- */
    
class MyCamera : public RasPiCam
{
protected:  
  struct timespec           curr_time;
  std::vector<timespec>     tms;

public:
  // constructor
  MyCamera(int camid, int sensormode)
  : RasPiCam(camid, sensormode)
  {
    tms.reserve(1000);
  };
  
  virtual ~MyCamera()
  {};

protected:  

  virtual void videoFrameReceived(MMAL_PORT_T * port, MMAL_BUFFER_HEADER_T * buff) 
  {
    lockBuffer(buff);
    // Get time.
    clock_gettime(CLOCK_MONOTONIC, &curr_time);
    
    int len;
    void * data;
    getInfo(buff, data, len);
    std::cout << tms.size() << ": Received " << len << " bytes" << std::endl;
    tms.push_back(curr_time);
    if(tms.capacity() <= tms.size())
    {
      exportStats();
      std::cout << "Exported" << std::endl;
      exit(0);
    }

    unlockBuffer(buff);
    returnBackBuffer(port, buff);
  };
  
  virtual void controlFrameReceived(MMAL_PORT_T * port, uint32_t shutter, 
                                    uint32_t analog, uint32_t digital) {};
  
  void exportStats()
  {
    std::ofstream ofs("export.txt");
    
    for(auto it = tms.begin(); it != tms.end(); ++it)
      ofs << it->tv_sec << "\t" << it->tv_nsec << std::endl;

  }
};

/* -------------------------------------------------------------------------- */

}   // namespace mke::tst
}   // namespace mke

/* -------------------------------------------------------------------------- */

/**
 * main
 */

int main(int argc, const char **argv)

{
  using namespace mke::tst;
  
  MyCamera cam(0,4);
  cam.setVideoFormat(1296,972,15);
  cam.startCapture();
  
  getchar();
  
  cam.stopCapture();
}

/* -------------------------------------------------------------------------- */
