/*
 * testcam_save - save 10 images
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 * Ideas based on code from RaspiVidYUV cloned from
 *                                  https://github.com/raspberrypi/userland.git
 */

/* -------------------------------------------------------------------------- */

#include <time.h>
#include <vector>
#include <set>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cstring>
#include <memory>
#include <unistd.h>
#include <thread>
#include <chrono>

#include "../dev/raspicam.h"
#include "../dev/rpsynclib/rpsynclib.h"

/* -------------------------------------------------------------------------- */

namespace mke {
namespace tst {

/* -------------------------------------------------------------------------- */

using namespace mke::dev;

/* -------------------------------------------------------------------------- */

#define NUM_PATTERNS 2

/* -------------------------------------------------------------------------- */
  
class MyCamera : public RasPiCam
{
  int           idx;
  double        strobe;
  int sleep;
  
  std::vector<int>                      idxs;
  std::vector<std::unique_ptr<char[]> > buffer;
  std::set<int>                         to_store;
  std::vector<int>                      patterns;
  struct timespec                       epoch;
  
public:
  // constructor
  MyCamera(int camid, int sensormode, float strobe, int sleep)
  : RasPiCam(camid, sensormode), idx(0), strobe(strobe), sleep(sleep)
  {
    rpsync_open();
    clock_gettime(CLOCK_MONOTONIC, &epoch);
  };
  
  void storeId(int id) 
  {
    to_store.insert(id);
  };
  
  void addPattern(int p)
  {
    patterns.push_back(p);
  }

protected:  
  
  void saveBinary(int idx, const char * data, int num)
  {
    std::ostringstream oss("image");
    oss << std::setw(3) << std::setfill('0') << idx;
    oss << ".bin";
    std::ofstream output(oss.str(), std::ios::binary);
      
    output.write(data, num);
    output.close();
  }
  
  void savePGM(int idx, const char * data, int w, int h, int stride)
  {
    std::ostringstream oss("image");
    oss << std::setw(3) << std::setfill('0') << idx;
    oss << ".pgm";
    std::ofstream output(oss.str());
    
    output << "P2" << std::endl;
    output << "# " << oss.str().c_str() << std::endl;
    output << w << " " << h << std::endl;
    output << 255 << std::endl;
    
    const char * d = data;
    for(int r = 0; r < h; ++r)
      {
        output << int(*d++);
        int c = 1;
        for(; c < w; ++c)
          output << " " << int(*d++);
        
        output << std::endl;
        
        for(; c < stride; ++c, ++d)
          {}
      }
    
    output.close();
  }

  void saveImages()
  {
    auto idx = idxs.begin();
    for(auto b = buffer.begin(); b != buffer.end(); ++b, ++idx)
      {
//        saveBinary(*idx, (const char *) b->get(), stride_*height_);
        savePGM(*idx, (const char *) b->get(), width_, height_, stride_);
      }
  }
  
  struct timespec sync;
  
  void blinker()
  {
    
  }
  
  
  virtual void controlFrameReceived(MMAL_PORT_T * port, uint32_t shutter, 
                                    uint32_t analog, uint32_t digital) {};
  
  
  virtual void videoFrameReceived(MMAL_PORT_T * port, MMAL_BUFFER_HEADER_T * buff) 
  {
    lockBuffer(buff);
    
    struct timespec start;
    clock_gettime(CLOCK_MONOTONIC, &start);
    float diff = (start.tv_sec - epoch.tv_sec) + (start.tv_nsec - epoch.tv_nsec)/1000000000.0f;
    std::cout << std::setw(3) << std::setfill('0') << idx;
    std::cout << ": " << diff << std::endl;
    
    std::this_thread::sleep_for(std::chrono::milliseconds(sleep));
    
    if(strobe > 0)
      rpsync_set_sec(patterns[idx%patterns.size()], strobe);
    else
      rpsync_set(patterns[idx%patterns.size()]);
    
    if(to_store.find(idx) != to_store.end())
      {
        int len;
        void * dataptr;
        getInfo(buff, dataptr, len);
        
        std::unique_ptr<char[]> data(new char[stride_*height_]);
        std::memcpy(data.get(), dataptr, stride_*height_);
        buffer.push_back(std::move(data));
        idxs.push_back(idx);
        
        struct timespec stop;
        clock_gettime(CLOCK_MONOTONIC, &stop);
        
        float diff = (stop.tv_sec - start.tv_sec) + (stop.tv_nsec - start.tv_nsec)/1000000000.0f;
        
        std::cout << "Image [" << len << "] no. " << idx << " stored to memory in " << diff << "s." << std::endl;
      }
    
    idx++;
    
    unlockBuffer(buff);
    returnBackBuffer(port, buff);

    if(to_store.size() <= buffer.size())
    {
      rpsync_enable_laser(0);
      rpsync_close();
      
      saveImages();
      exit(0);
    }
    epoch = start;
  };
};

/* -------------------------------------------------------------------------- */

} // namespace mke::tst
} // namespace mke

/* -------------------------------------------------------------------------- */

/**
 * main
 */
int main(int argc, const char **argv)

{
  using namespace mke::tst;
  
  std::cout << "Usage: " << argv[0] << " FPS shutter sleep strobe pat1 pat2 idx1 idx2 .. idxn" << std::endl;
  
  if(argc < 5)
    exit(-1);
  
  int i = 1;
  int fps = atoi(argv[i++]);
  int shutter = atoi(argv[i++]);
  int sleep = atoi(argv[i++]);
  MyCamera cam(0,4,atof(argv[i++]), sleep);
  cam.addPattern(atoi(argv[i++]));
  cam.addPattern(atoi(argv[i++]));
  for(; i < argc; ++i)
    cam.storeId(atoi(argv[i]));
  
  cam.setVideoFormat(1296,972,fps);
  cam.setShutterSpeed(shutter);
  cam.setISO(800);
  cam.setWB(false,65500,65500);
  cam.startCapture();
  
  getchar();
  
  cam.stopCapture();
}

/* -------------------------------------------------------------------------- */
