#include <unistd.h>
#include "../dev/rpsynclib/rpsynclib.h"

int main(int argc, const char ** argv)
{
  rpsync_open();

  for(int j = 0; j < 10; ++j)
    {
      for(int i = 0; i != 16; ++i)
        {
          rpsync_set_sec(i, 0.1);
          sleep(1);
        }	
    }
    
  rpsync_enable_laser(0);
  rpsync_close();
}