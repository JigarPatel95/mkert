/*
 * testcam_stats - save stats for evaluating exposure times
 *
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 * Ideas based on code from RaspiVidYUV cloned from
 *                                  https://github.com/raspberrypi/userland.git
 */

/* -------------------------------------------------------------------------- */

#include <time.h>
#include <vector>
#include <iostream>
#include <fstream>

#include <chrono>

#include "../dev/v4l2cam.h"


/* -------------------------------------------------------------------------- */

namespace mke {
namespace tst {

/* -------------------------------------------------------------------------- */

using namespace mke::dev;

/* -------------------------------------------------------------------------- */
    
class MyCamera : public V4L2Cam
{
public:
  // constructor
  MyCamera(int camid)
  : V4L2Cam(camid, 10)
  {};
  
  virtual ~MyCamera()
  {};

protected:  

  virtual void videoFrameReceived(const Pix8 * data, uint32_t w, uint32_t h, uint32_t memw)
  {
    std::cout << "new frame: " << w << "x" << h << " stride: " << memw << std::endl;  
    returnBackBuffer(data);
  };
};

/* -------------------------------------------------------------------------- */

}   // namespace mke::tst
}   // namespace mke

/* -------------------------------------------------------------------------- */

/**
 * main
 */

int main(int argc, const char **argv)

{
  using namespace mke::tst;
 
  try {
    MyCamera cam(0);
    cam.startCapture();
    

    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    
    cam.stopCapture();
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    cam.startCapture();
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    cam.stopCapture();
    
  } catch (std::exception & ex) {
    std::cerr << ex.what() << std::endl;
  }
}

/* -------------------------------------------------------------------------- */


