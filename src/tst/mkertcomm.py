#!/bin/python 
#
# mkertcomm - mkert communication testing application
# 
# Copyright (c) 2017, Magik-Eye s.r.o., Prague
# author: Ondra Fisar
#
# ------------------------------------------------------------------------------

# imports

import sys
import socket
import serial
import time
import struct
import math

import colorama

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from numpy import reshape

# ------------------------------------------------------------------------------

class TcpClient:
  
  def __init__(self, host, port):
    self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    self.sock.connect((host, int(port)))
    
  def send(self, data):
    return self.sock.send(data)
    
  def recv(self, length):
    return self.sock.recv(length)
    
# ------------------------------------------------------------------------------

class SerialClient:
  
  def __init__(self, dev_path, baudrate):
    self.ser = serial.serial_for_url(dev_path, do_not_open=True)
    self.ser.baudrate = baudrate
    self.ser.parity = serial.PARITY_NONE
    self.ser.stopbits = serial.STOPBITS_ONE
    self.ser.xonxoff = False
    self.ser.rtscts = False
    self.ser.dsrdtr = False
    self.ser.open()
    
  def send(self, data):
    return self.ser.write(data)
    
  def recv(self, length):
    return self.ser.read(length)

# ------------------------------------------------------------------------------

# global vars

conn = None         # connection
packet_len = 48
keep_figures = False
keep_delay = 0

# ------------------------------------------------------------------------------

TERMINATE   = 10
GET_FWINFO  = 11
GET_DEVINFO = 12
GET_DEVXML  = 13
GET_STATE   = 20
SET_STATE   = 21
GET_POLICY  = 22
SET_POLICY  = 23
START_PUSH  = 24
STOP_PUSH   = 25
GET_FRAME   = 26
LIST_POLICIES = 27
TIMER       = 1002
GET_SHUTTER = 1006
SET_SHUTTER = 1007
GET_GAIN    = 1008
SET_GAIN    = 1009
GET_LASER   = 1010
SET_LASER   = 1011
GET_IMAGE   = 1012
GET_DEBUG   = 1013
GET_STATS   = 1014
GET_RESERVED_INFO = 1015
RESET_STATS   = 1017
GET_PROFILE = 1016

UPLOAD_PACKAGE = 2001
DOWNLOAD_FILE = 2999
SET_PROFILE = 2002

# ------------------------------------------------------------------------------

def cprint(color, text):
  print(color + text + colorama.Style.RESET_ALL)

# ------------------------------------------------------------------------------

def print_api():
  
  print("Current API:--------")
  
  with open('../../mkeapi/include/mkeapi.h') as f:
    out = False
    
    for line in f:
      out = out or line.find('enum') >= 0
        
      if out:
        print(line, end='')

      out = out and line.find('}') < 0
      
  print("--------------------")

# ------------------------------------------------------------------------------

# send mke request

def send_mke(cmd, seq_id, params = b'', payload = b''):
  
  msg = "MKERQ100%04d%04d" % (cmd, seq_id)
  header = bytes(msg, 'ascii') + params 
  msg = header + payload
  
  if conn.send(msg) == 0:
    raise RuntimeError("Connection broken")
  
  print("<<< %s [%d]" % (header, len(payload)));

# ------------------------------------------------------------------------------

# parse replied parameter

def parse_reply_param(data, is_signed = False):

  return int.from_bytes(data, byteorder='little', signed=is_signed)

# ------------------------------------------------------------------------------

# read PGM

def read_pgm(data):
  
  pos = data.find(b'\n');
  parts = data[0:pos].decode().split(' ');

  assert(parts[0] == 'P5')
  w = int(parts[1])
  h = int(parts[2])
  
  return reshape(bytearray(data[pos+1:]), (h, w)) #.transpose()

# ------------------------------------------------------------------------------

def fromSci(scival):
  return scival[0] * math.pow(2, scival[1])

# ------------------------------------------------------------------------------

def toSci(value):
  exp = math.floor(math.log2(abs(value))-6) if value != 0 else 0;
  mantisa = round(value/math.pow(2,exp))
  return (mantisa, exp)

# ------------------------------------------------------------------------------

fig_pts = None
fig_im = None
ax_pts = None
ax_im = None

# process frame data

def process_frame(frame_type, num_data, data, keep_fig = False, label_prefix='', im = None):
  
  global fig_pts
  global fig_im
  global ax_pts
  global ax_im
    
  if frame_type == 0:
    print(">>> UNDEFINED FORMAT")
  else:
    ids = []
    us = []
    vs = []
    xs = []
    ys = []
    zs = []
    
    for i in range(num_data):
      sz = { 1: 8, 2: 12, 1001: 20, 1002: 36}.get(frame_type)
      loc = data[i*sz:(i+1)*sz]
      
      ids.append(parse_reply_param(loc[0:2]))
      xs.append(parse_reply_param(loc[2:4], True))
      ys.append(parse_reply_param(loc[4:6], True))
      zs.append(parse_reply_param(loc[6:8], True))
      
      if frame_type > 1000:
        us.append(float(parse_reply_param(loc[12:16]))/256/256)
        vs.append(float(parse_reply_param(loc[16:20]))/256/256)

    if len(ids) == 0 and im is None:
      print(">>> NO DATA");
      return

    if not keep_fig or fig_pts is None:
      fig_pts = plt.figure()
      ax_pts = fig_pts.add_subplot(111, projection='3d')
    else:
      ax_pts.cla()
    ax_pts.plot(xs, ys, zs, 'b.');
    plt.title(label_prefix + '3D')
    plt.show(block=False);
    
    if frame_type > 1000 or not (im is None):
      if not keep_fig or fig_im is None:
        fig_im, ax_im = plt.subplots();
      ax_im.cla()
      
      if not (im is None): 
        ax_im.imshow(im, cmap=plt.get_cmap('gray'), vmin=0, vmax=255)
      
      if frame_type > 1000:
        ax_im.plot(us, vs, 'r.', mfc='none')
        
      plt.title(label_prefix + '2D')
      plt.show(block=False);
    
    if keep_fig:
      global keep_delay
      plt.pause(keep_delay)
      
# ------------------------------------------------------------------------------

# process payload

def process_payload(cmd, params, data):
  
  global keep_figures
  
  if cmd == GET_FRAME or cmd == START_PUSH:
    timer       = parse_reply_param(params[0:8])
    seqn        = parse_reply_param(params[8:16])
    data_type   = parse_reply_param(params[16:20])
    frame_type  = parse_reply_param(params[20:22])
    num_data    = parse_reply_param(params[22:24])
    
    print(">>> FRAME (timer=%d, seqn=%d, data_type=%d, frame_type=%d, num_data=%d)" 
            % (timer, seqn, data_type, frame_type, num_data))
    
    process_frame(frame_type, num_data, data, cmd == START_PUSH or keep_figures, "no. %d " % seqn)
    
#  elif cmd == GET_IMAGE:
#    plt.imshow(read_pgm(data),  cmap=plt.get_cmap('gray'), vmin=0, vmax=255)
#    plt.show(block=False)
  elif cmd == GET_DEBUG:
    num = parse_reply_param(params[0:4])
    off = 0
    
    for i in range(num):
      subhead     = data[off:(off+32)]
      frame_id    = parse_reply_param(subhead[8:12])
      image_num   = parse_reply_param(subhead[12:16])
      frame_num   = parse_reply_param(subhead[16:20])
      frame_type  = parse_reply_param(subhead[20:22])
      frame_data  = parse_reply_param(subhead[22:24])
      pattern     = parse_reply_param(subhead[24:26])
      
      im_off = off + 32
      frame_off = im_off + image_num
      frame_end = frame_off + frame_num

      im = read_pgm(data[im_off:frame_off])
      frame = data[frame_off:frame_end]
      
      process_frame(frame_type, frame_data, frame, keep_figures, 'DGB%d#%d ' % (i, pattern), im)
      
      off = frame_end
  elif cmd == GET_SHUTTER:
    exp_time = parse_reply_param(params[0:4])
    print(">>> GET_SHUTTER (exp_time=%d)" % exp_time)
  elif cmd == GET_GAIN:
    analog = parse_reply_param(params[0:4])
    digital = parse_reply_param(params[4:8])
    print(">>> GET_GAIN (analog=%d, digital=%d)" % (analog, digital))
  elif cmd == GET_LASER:
    v = struct.unpack('<Hbbbb', params[0:6])
    print(">>> GET_LASER (pattern=%d, dur=%d*2^%d (%f), off=%d*2^%d (%f))" % (v[0], v[1], v[2], fromSci((v[1], v[2])), v[3], v[4], fromSci((v[3], v[4]))))
  elif cmd == GET_FWINFO:
    posixtime = parse_reply_param(params[0:8])
    commit = parse_reply_param(params[8:12])
    ver_major = int(params[12])
    ver_minor = int(params[13])
    ver_patch = int(params[14])
    sys_major = int(params[15])
    sys_minor = int(params[16])
    sys_patch = int(params[17])
    
    print(">>> FWINFO (time=%d, commit=%08x, fw_ver=%d.%d.%d, sys_ver=%d.%d.%d)" % 
          (posixtime, commit, ver_major, ver_minor, ver_patch, 
                              sys_major, sys_minor, sys_patch))
  elif cmd == GET_DEVINFO:
    dev_id = parse_reply_param(params[0:2])
    unit_id = str(params[2:10])

    print(">>> DEVINFO (device_id=%d, unit_id=%s)" % (dev_id, unit_id))
  elif cmd == GET_DEVXML:

    print(">>> DEVXML:")
    print(data.decode('ascii'))
  elif cmd == GET_POLICY:
    print(">>> POLICY: %s " % str(params[0:8].decode('ascii')))
  elif cmd == LIST_POLICIES:
    policies = []
    pos = data.find(b'\0')
    while pos >= 0:
      policies.append(str(data[0:pos]));
      data = data[pos+1:]
      pos = data.find(b'\0')
    print(">>> POLICIES [%d]: %s " % (parse_reply_param(params[0:4]), policies))
  elif cmd == GET_PROFILE:
    print(">>> PROFILE: %s " % data.decode('ascii'))
  elif cmd == GET_STATS:
    print(">>> STATS: %s " % data.decode('ascii'))
  else:
    payload_size = len(data)
    if payload_size <= 0:
      pass
    elif payload_size <= 40:
      print(">>> PAYLOAD: %s" % data[48:])
    else:
      print(">>> PAYLOAD: %s ... continue" % data[48:68])
      
      outname = str(input("III Payload is too heavy. Choose file where to store it (empty = no store)?> "))
      if len(outname) > 0:
        with open(outname, 'wb') as fp:
          fp.write(data[48:])
        
        print("III Payload stored");
    

# ------------------------------------------------------------------------------

# receive mke reply

def recv_mke():
  
  data = bytes()
  while len(data) < packet_len:
    tmp = conn.recv(packet_len)
    if len(tmp) == 0:
      raise RuntimeError("Connection closed");
    data += tmp

  payload_size = parse_reply_param(data[20:24]);

  ret_cmd   = int(data[8:12])
  ret_code  = int(data[12:16])
  clr       = colorama.Style.RESET_ALL

  if ret_code >= 500:
    clr = colorama.Fore.RED + colorama.Style.BRIGHT
  elif ret_code >= 400:
    clr = colorama.Fore.YELLOW + colorama.Style.BRIGHT
  elif ret_code >= 200:
    clr = colorama.Fore.GREEN + colorama.Style.BRIGHT
  elif ret_code >= 100:
    clr = colorama.Fore.CYAN + colorama.Style.BRIGHT
    
  cprint(clr, ">>> %s %s %s [%d+%d](CMD: %d, RET: %d)" % 
        (data[:20], data[20:24], data[24:], len(data), payload_size, ret_cmd, ret_code));

  while len(data) < (packet_len + payload_size):
    tmp = conn.recv(payload_size)
    if len(tmp) == 0:
      raise RuntimeError("Connection closed");
    data += tmp

  if ret_code == 200 or ret_code == 101:
    process_payload(ret_cmd, data[24:48], data[48:])

# ------------------------------------------------------------------------------

def get_input_param(label, num_bytes, offset, params):
  val = int(input(label + "? > "));
  
  return set_param(val, num_bytes, offset, params)

# ------------------------------------------------------------------------------

def set_param(val, num_bytes, offset, params):

  for i in range(offset, offset + num_bytes):
    params[i] = val % 256
    val = int(val/256)
    
  return params;

# ------------------------------------------------------------------------------

def check_params(cmd):
  params = bytearray(8)
  payload = bytearray(0)
  
  if cmd == SET_STATE:    # get frame with data
    params = get_input_param('NEW_STATE', 4, 0, params)
  if cmd == GET_FRAME or cmd == START_PUSH:    # get frame with data
    params = get_input_param('FRAME_TYPE', 2, 0, params)
  elif cmd == GET_IMAGE:     # get image
    params = get_input_param('NUM_AVARAGES', 4, 0, params)
    params = get_input_param('TURNOFF', 1, 4, params)
  elif cmd == GET_DEBUG:     # get debug frames
    params = get_input_param('NUM_IMAGES', 4, 0, params)
    params = get_input_param('FRAME_TYPE', 2, 4, params)
  elif cmd == TIMER:     # start timer
    params = get_input_param('NUM_TICKS', 4, 0, params)
  elif cmd == SET_LASER: # laser pattern and duration
    pattern = int(input('PATTERN? > '))
    dur = toSci(float(input('DURATION? > ')))
    off = toSci(float(input('OFFSET? > ')))
    params = struct.pack('<HbbbbH', pattern, dur[0], dur[1], off[0], off[1], 0)
  elif cmd == SET_SHUTTER: # laser pattern and duration
    params = get_input_param('EXP TIME', 4, 0, params)
  elif cmd == SET_GAIN:
    params = get_input_param('ISO', 4, 0, params)
  elif cmd == SET_POLICY:
    profile = input('POLICY? > ').encode('ascii')
    params = profile[0:min(len(profile), 8)]
    params = params + bytearray(8-len(params))
  elif cmd == SET_PROFILE:
    payload = input('PROFILE? > ').encode('ascii')
    params = set_param(len(payload), 4, 0, params)
  elif cmd == UPLOAD_PACKAGE:
    path = input('PATH_TO_UPLOAD? > ')
    with open(path, 'rb') as f:
      payload = f.read()
    params = set_param(len(payload), 4, 0, params)
  elif cmd == DOWNLOAD_FILE:
    path = input('PATH_TO_FILE? > ')
    payload = path.encode('ascii')
    params = set_param(len(payload), 4, 0, params)    
  elif cmd == TERMINATE:
    params = get_input_param('METHOD', 4, 0, params)
  elif cmd == GET_RESERVED_INFO:
    print('GET_RESERVED_INFO?')
  return (bytes(params), bytes(payload))

# ------------------------------------------------------------------------------

# check arguments

if(len(sys.argv) != 4):
  cprint(colorama.Fore.RED, "Usage %s <TCP/SERIAL> <IP/TTY> <port/baudrate>" % sys.argv[0])
  raise RuntimeError("Bad number arguments");

# init colorama

colorama.init()

# open connection

if(sys.argv[1] == "TCP"):
  conn = TcpClient(sys.argv[2], int(sys.argv[3]))
elif(sys.argv[1] == "SERIAL"):
  conn = SerialClient(sys.argv[2], int(sys.argv[3]));
else:
  raise RuntimeError("Unknown client type. Supported TCP or SERIAL")

# main loop

seq_id = 0;
while True:
  
  line = input("CMD? > ").strip()
  if len(line) == 0:
    continue
  elif line == 'p' or line == 'P':
    print_api()
    continue
  elif line == 'q' or line == 'Q':
    break
  elif line == 'c' or line == 'C':
    plt.close('all')
    continue
  elif line == 'k':
    keep_figures = True
    keep_delay = float(input('DELAY? > '))
    print("Keeping figures activated!")
    continue
  elif line == 'K':
    keep_figures = False
    print("Keeping figures DEactivated!")
    continue
  elif line == 'r' or line == 'R':
    recv_mke();
    continue
  
  cmd = int(line);
  
  if cmd < 0:
    break;
  
  (params, payload) = check_params(cmd);
  
  send_mke(cmd, seq_id, params, payload)
  recv_mke();
  seq_id += 1

cprint(colorama.Fore.GREEN, "Correct termination");

# ------------------------------------------------------------------------------
