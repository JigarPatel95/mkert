/*
 * testcam_save - save 10 images
 * 
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 * Ideas based on code from RaspiVidYUV cloned from
 *                                  https://github.com/raspberrypi/userland.git
 */

/* -------------------------------------------------------------------------- */

#include <time.h>
#include <vector>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

#include "../dev/raspicam.h"

/* -------------------------------------------------------------------------- */

namespace mke {
namespace tst {

/* -------------------------------------------------------------------------- */

using namespace mke::dev;

/* -------------------------------------------------------------------------- */
    
class MyCamera : public RasPiCam
{
  int idx;
public:
  // constructor
  MyCamera(int camid, int sensormode)
  : RasPiCam(camid, sensormode), idx(0)
  {};
  
  virtual ~MyCamera()
  {};

protected:  

  virtual void videoFrameReceived(MMAL_PORT_T * port, MMAL_BUFFER_HEADER_T * buff) 
  {
    lockBuffer(buff);
    std::ostringstream oss("image");
    oss << std::setw(2) << std::setfill('0') << idx;
    oss << ".bin";
    std::ofstream output(oss.str(), std::ios::binary);
    
    int len;
    void * data;
    getInfo(buff, data, len);
    output.write((const char *) data, stride_*height_);
    output.close();
    
    std::cout << "Image [" << len << "] no. " << idx << " saved" << std::endl;
    
    idx++;

    unlockBuffer(buff);
    returnBackBuffer(port, buff);
    
    if(idx >= 10)
      exit(0);
  };
  
  
  virtual void controlFrameReceived(MMAL_PORT_T * port, uint32_t shutter, 
                                    uint32_t analog, uint32_t digital) {};
  
};

/* -------------------------------------------------------------------------- */

} // namespace mke::tst
} // namespace mke


/* -------------------------------------------------------------------------- */

/**
 * main
 */
int main(int argc, const char **argv)

{
  using namespace mke::tst;
  
  MyCamera cam(0,4);
  cam.setVideoFormat(1296,972,30);
  cam.startCapture();
  
  getchar();
  
  cam.stopCapture();
}

/* -------------------------------------------------------------------------- */
