/*
 * testcam_stats - save stats for evaluating exposure times
 *
 * Copyright (c) 2017, Magik-Eye s.r.o., Prague
 * author: Ondra Fisar
 *
 * Ideas based on code from RaspiVidYUV cloned from
 *                                  https://github.com/raspberrypi/userland.git
 */

/* -------------------------------------------------------------------------- */

#include <time.h>
#include <vector>
#include <iostream>
#include <fstream>

#include "../dev/fpcam.h"


/* -------------------------------------------------------------------------- */

namespace mke {
namespace tst {

/* -------------------------------------------------------------------------- */

using namespace mke::dev;

/* -------------------------------------------------------------------------- */
    
class MyCamera : public FpCam
{
public:
  // constructor
  MyCamera(int camid)
  : FpCam(camid, 10)
  {};
  
  virtual ~MyCamera()
  {};

protected:  

  virtual void videoFrameReceived(const Pix8 * data, int w, int h, int memw, uint16_t phase) 
  {
    std::cout << "new frame" << std::endl;  
    returnBackBuffer(data);
  };
};

/* -------------------------------------------------------------------------- */

}   // namespace mke::tst
}   // namespace mke

/* -------------------------------------------------------------------------- */

/**
 * main
 */

int main(int argc, const char **argv)

{
  using namespace mke::tst;
 
  try {
    MyCamera cam(0);
    cam.startCapture();
    
    getchar();
    
    cam.stopCapture();
  } catch (std::exception & ex) {
    std::cerr << ex.what() << std::endl;
  }
}

/* -------------------------------------------------------------------------- */

