/*
 * mkert - The main entry point to MkE Runtime
 * 
 * Copyright (c) 2017-2018, Magik-Eye s.r.o., Prague
 * author: Jan Heller, jan@magik-eye.com
 *         Ondra Fisar, fisar@magik-eye.com
 */

#include <iostream>
#include <stdexcept>
#include <locale>
#include <codecvt>

#include <unistd.h>
#ifndef __MINGW32__
#include <linux/reboot.h>
#include <sys/reboot.h>
#endif

#include <boost/thread.hpp>
#include <boost/filesystem.hpp>


#include "util/mac_addr.h"
#include "util/logging.h"
#include "mkert_config.h"

#include "mkeapi.h"
#include "mkedd.h"

#include "rt/api/memory_pool.h"
#include "rt/api/queue.h"
#include "rt/detector.h"
#include "rt/dispatcher.h"
#include "rt/worker.h"
#include "rt/failsafe.h"
#include "rt/upnpserver.h"
#include "rt/devinfo.h"

#include "mkert_version.h"
#include "mkert_buildinfo.h"

#ifndef MKERT_VERSION
#define MKERT_VERSION "X.Y.Z"
#endif

#ifndef MKEAPI_VERSION
#define MKEAPI_VERSION "V.W"
#endif

#ifndef MKERT_GIT_COMMIT
#define MKERT_GIT_COMMIT "!UNKNOWN"
#endif

using namespace mke;
using namespace mke::rt;

namespace fs = boost::filesystem;

/**
 * @brief Apply configuration given by command line
 * 
 * Suppose there will be just info messages. If some error appears throw it.
 * 
 * @result should application continue
 */
bool applyConfiguration(mke::MkertConfig & config)

{
  // print print help
  
  if (config.doHelp())
    {
      config.getCommandLineHelp(std::cout);
      return false;
    }

  // print version
  
  if (config.doVersion())
    {
      std::cout << MKERT_VERSION << std::endl;
      return false;
    }

  // Set logging verbosity level
  
  if (config.doVerbose())    
    util::SimpleLogger::setLevel(util::SimpleLogger::INFO);
  
  return true;
}

void printAppInfo()

{
  std::cout << "This is MkE RT version " << MKERT_VERSION << ", ";
  std::cout << "(c) 2016-2018 Magik Eye Inc." << std::endl;
  std::cout << "Supported MkE API version " << MKEAPI_VERSION << std::endl;
  std::cout << "Built on " << MKERT_TIMESTAMP_STR;
  
  if(strcmp(MKERT_GIT_COMMIT, "00000000") != 0)
    {
      std::cout << ", based on commit " << MKERT_GIT_COMMIT << std::endl;
      std::cout << "!! This is UNOFFICIAL build. Please do not redistribute. !!";
    }
  
  std::cout << std::endl;
}

/**
 * @brief Translate MkE terminate method to string
 * 
 * @result method string
 */
static const char * getExitMethod(mke::api::MkETerminateMethodType term_type)

{
  switch(term_type)
    {        
    case mke::api::MKE_TERMINATE_BY_EXIT:
      return "mke::api::MKE_TERMINATE_BY_EXIT";
    case mke::api::MKE_TERMINATE_BY_REBOOT:
      return "mke::api::MKE_TERMINATE_BY_REBOOT";
#ifdef MKE_USE_RESERVED_API      
    case mke::api::MKE_TERMINATE_BY_RESTART:
      return "mke::api::MKE_TERMINATE_BY_RESTART";
#endif
    case mke::api::MKE_TERMINATE_BY_SHUTDOWN:
      return "mke::api::MKE_TERMINATE_BY_SHUTDOWN";
    default:
      return "mke::api::MKE_TERMINATE_UNDEF";
    }
}

/**
 * @brief Perform exit action
 */
void performExitAction(mke::api::MkETerminateMethodType term_type, char * argv[], bool ignore_reboot)

{
  // print log
  
  LOG_INFO << "Exit method: " << getExitMethod(term_type);
  
  // make an action

  int cmd = 0;
  std::string msg;
  switch(term_type)
    {
#ifdef MKE_USE_RESERVED_API        
    case mke::api::MKE_TERMINATE_BY_RESTART:      
      execv(argv[0], argv);
      return;
#ifndef __MINGW32__
    case mke::api::MKE_TERMINATE_BY_REBOOT:
      cmd = LINUX_REBOOT_CMD_RESTART;
      msg = "REBOOT";
      break;
    case mke::api::MKE_TERMINATE_BY_SHUTDOWN:
      cmd = LINUX_REBOOT_CMD_POWER_OFF;
      msg = "SHUTDOWN";
      break;
#endif
#endif
    case mke::api::MKE_TERMINATE_BY_EXIT:
    default:
      // nothing to do
      return;
    }
    
    if(ignore_reboot)
      LOG_WARN << "Command " << msg << " has been ignored.";
    else 
      {
#ifdef __MINGW32__
        LOG_WARN << "Reboot and shutdown the sensor is not available on Windows";
#else
        // sync filesystem
        
        sync();
        
        // do an action
        switch(reboot(cmd))
          {
          case EPERM:
            throw std::runtime_error("You must be root to reboot device.");
          case 0:
            break;
          default:
            throw std::runtime_error("Unable reboot device.");
          }
#endif
      }
}

int runFailSafe(const char * full_path, const DeviceInfo & dev_info, const MkertConfig * cfg, 
                mke::api::MkEFatalErrorType err_code,
                mke::api::MkETerminateMethodType & term_type)

{
  try
    {
      
      MkertConfig config(cfg);
      
      // Initialize camera and lasers
      // TODO After this step, we should be able to tell api_pool the maximum sizes of the respective responses

      // Initialize API classes
      mke::rt::api::MemoryPool api_pool(config, 0);
      mke::rt::api::Queue api_queue(config);
      mke::rt::api::Resolver api_resolver(config, api_pool, api_queue);
      api_resolver.setFatal(err_code);

      api_pool.init();
      api_queue.init();
      api_resolver.init();
            
      // Initialize worker and dispatcher
      // ...
      FailSafeMode worker(config, api_pool, api_resolver, full_path, dev_info);
      Dispatcher dispatcher(config, api_pool, api_queue);
      
      worker.init();
      dispatcher.init();
          
      // Start worker
      boost::thread worker_thread([& worker]{worker.start();});

      // Start dispatcher
      boost::thread dispatcher_thread([& dispatcher]{dispatcher.start();});
          
      // Wait for threads to exit
      
      worker_thread.join();
      dispatcher.stop();
      
      dispatcher_thread.join();

      // TODO cleanup, stop lasers stop camera ...
      dispatcher.close();
      worker.finalize();
      
      term_type = dispatcher.getExitReason();  
    }
  catch(std::exception & e) 
    {
      LOG_INFO << "FATAL ERROR: " << e.what() << std::endl;
      mke::util::SimpleLogger::closeLogger();
      return 1;          
    }
  
  return 0;
}

/**
 * @brief Main method of application
 */
int main(int argc, char* argv[])
{
  mke::util::SimpleLogger::openLogger();
  printAppInfo();
  
  mke::MkertConfig config;
  mke::api::MkETerminateMethodType term_type = mke::api::MKE_TERMINATE_UNDEF;
  
  fs::path full_path( fs::initial_path<fs::path>() );
  full_path = fs::system_complete( fs::path( argv[0] ) ).parent_path();

#ifdef __MINGW32__
  std::wstring_convert<std::codecvt_utf8<wchar_t>> conv1;
  std::string full_path_utf8 = conv1.to_bytes(full_path.c_str());
#else
  std::string full_path_utf8 = full_path.c_str();
#endif
  
  std::unique_ptr<UpnpServer> upnp;

  // prepare device info
  DeviceInfo dev_info(1, "MagikEyeOne");

  try
    {
      std::map<std::string, std::string> devs;
      mke::util::MacAddresses::get(devs);
      
      if (devs.begin() != devs.end())
        dev_info.serial_no = devs.begin()->second;
      
      dev_info.generateXml();
      
      upnp.reset(new UpnpServer(dev_info));
    }
  catch(std::exception & e)
    {
      LOG_WARN << e.what();
      LOG_INFO << "Error has been ignored. Application continues.";
    }
  
#ifdef USE_RPICAM
  cmldc_open();
#endif
  
  int ret_code = 0;
  try
    {
      try
        {
          // Parse command line / config file options
          
          config.parseFromCmdLine(argc, argv);
      
          if (config.getConfigFile() != "")
            config.parseFromCfgFile(config.getConfigFile());      

          if(!applyConfiguration(config))
            return 0;
        }
      catch(std::exception & e)
        {
          throw mke::rt::api::FatalError(mke::api::MKE_FATAL_BADCONFIG, e.what());
        }

      
      // Initialize detector
      // TODO
      mke::rt::Detector detector(config);

      try
        {
          detector.init();
        }
      catch(std::exception & e)
        {
          throw mke::rt::api::FatalError(mke::api::MKE_FATAL_DETECTORINIT, e.what());
        }

      // Initialize camera and lasers
      // TODO After this step, we should be able to tell api_pool the maximum sizes of the respective responses

      // Initialize API classes
      mke::rt::api::MemoryPool api_pool(config, detector.getMaxFrameSize());
      mke::rt::api::Queue api_queue(config);
      mke::rt::api::Resolver api_resolver(config, api_pool, api_queue);
  
      api_pool.init();
      api_queue.init();
      api_resolver.init();
            
      // Initialize worker and dispatcher
      // ...
      Worker worker(config, detector, api_pool, api_resolver, (const char *) full_path_utf8.c_str(), dev_info);
      Dispatcher dispatcher(config, api_pool, api_queue);
      
      worker.init();
      dispatcher.init();
          
      // Start worker
      boost::thread worker_thread([& worker]{worker.start();});

      // Start dispatcher
      boost::thread dispatcher_thread([& dispatcher]{dispatcher.start();});
          
      // Wait for threads to exit
      
      worker_thread.join();
      dispatcher.stop();
      
      dispatcher_thread.join();

      // TODO cleanup, stop lasers stop camera ...
      dispatcher.close();
      worker.finalize();
      
      term_type = dispatcher.getExitReason();
    }
  catch(mke::rt::api::FatalError & e)
    { 
      LOG_WARN << "FATAL ERROR (" << mke::rt::api::FatalError::statusToStr(e.getErrorCode()) << "): " << e.what();
      LOG_WARN << "Switching to FAIL-SAFE mode";
      
      if(config.getGeneralConfig().failsafe)
        ret_code = runFailSafe((const char *) full_path_utf8.c_str(), dev_info, &config, e.getErrorCode(),
                                term_type);
    }
  catch(std::exception& e)
    { 
      LOG_WARN << "FATAL ERROR (unknown): " << e.what();
      LOG_WARN << "Switching to FAIL-SAFE mode";
      
      ret_code = runFailSafe(full_path_utf8.c_str(), dev_info, &config, mke::api::MKE_FATAL_UNDEF,
                             term_type);
    }
  
    
  upnp.reset();

#ifdef USE_RPICAM
  cmldc_close();
#endif
  
  if(ret_code == 0)
    performExitAction(term_type, argv, config.isRebootIgnored());
  
  mke::util::SimpleLogger::closeLogger();
  return ret_code;
}
