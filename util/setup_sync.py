#!/usr/bin/python3

# Copyright 2017 MagikEye Inc., Ondrej Fisar (fisar@magik-eye.com)
#
# Usage ./setup_sync.py MODE FPS SHUTTER_SPEED [STROBE_LENGTH]
#

import sys
import re
import json

# print usage

def usage(appname):
  sys.stderr.write("Usage:\n")
  sys.stderr.write("\t%s [--profile|--config CONFIG] [--comment \"...\"] MODE FPS [SHUTTER_SPEED=MAX_POSSIBLE] [PRE_STROBE=0] [POST_STROBE=0]\n" % appname)

# constants - given by measurements

ROLLING_DELAY = {}
ROLLING_DELAY[2] = 46550
ROLLING_DELAY[3] = ROLLING_DELAY[2]
ROLLING_DELAY[4] = 23300
ROLLING_DELAY[6] = 6990

RESOLUTION = {}
RESOLUTION[2] = [3280, 2464, 3296]
RESOLUTION[3] = [3280, 2464, 3296]
RESOLUTION[4] = [1640, 1232, 1664]
RESOLUTION[6] = [1280, 720, 1280]

STROBE_OFFSET_LIMIT = 1000

# body

sys.stderr.write("%s - application for setup synchronization parameters of mkert configuration\n" % sys.argv[0])
sys.stderr.write("Copyright 2017 MagikEye Inc.\n\n")

# check arguments

try:
  args = sys.argv
  appname = args[0]
  args.pop(0)
    
  # output type
  
  out_type = None
  if "--config" in args:
    idx = args.index("--config")
    out_type = args.pop(idx)
    if len(args) < idx:
      raise RuntimeError("Bad number of arguments")
    config = args.pop(idx)
    
  elif "--profile" in args:
    idx = args.index("--profile")
    out_type = args.pop(idx)
  else:
    raise RuntimeError("Bad TYPE of conversion, possible: --config/--profile");

  # parse comments

  comm = None
  if "--comment" in args:
    if out_type == "--config":
      raise RuntimeError("No support for comments in configs")
    idx = args.index("--comment")
    args.pop(idx)
    if len(args) < idx:
      raise RuntimeError("Bad number of arguments")
    comm = args[idx]
    args.pop(idx)

  # parse rest of order dependent arguments

  if len(args) == 0:
    raise RuntimeError("Bad number of arguments")
  mode = int(args.pop(0))
  if not(mode in ROLLING_DELAY):
    raise RuntimeError("Bad MODE arguments. Supported modes: %s" % ROLLING_DELAY.keys())
  
  if len(args) == 0:
    raise RuntimeError("Bad number of arguments")
  fps = int(args.pop(0))
  period = round(1000000/fps)

  if len(args) > 0:
    shutter = int(args.pop(0))
  else:
    shutter = period - ROLLING_DELAY[mode]

  strobe = shutter
  pre_strobe = 0
  post_strobe = 0

  if len(args) > 0:
    pre_strobe = int(args.pop(0))
  if len(args) > 0:
    post_strobe = int(args.pop(0))
    
except RuntimeError as err:
  sys.stderr.write("Bad arguments! Error: %s\n" % err);
  usage(appname);
  exit();

# the body

try:

  roll = ROLLING_DELAY[mode]
  strobe += roll + pre_strobe + post_strobe

  pos_offset = period - shutter - roll - pre_strobe
  neg_offset = period - shutter - pre_strobe
  
  if pos_offset < STROBE_OFFSET_LIMIT:
    sys.stderr.write("WARN: Too small positive strobe_offset, will be ignored.\n")
    pos_offset = -1

  if neg_offset < STROBE_OFFSET_LIMIT:
    sys.stderr.write("WARN: Too small negative strobe_offset, will be ignored.\n")
    neg_offset = -1
  
  if pos_offset < 0 and neg_offset < 0:
    raise RuntimeError("Too long shutter or strobe for this MODE+FPS combination!");
  
  if strobe > period:
    raise RuntimeError("Too long shutter!");    
  
  offset = pos_offset if pos_offset < neg_offset and pos_offset > 0 else -neg_offset
  
  # config output
  
  if out_type == '--config':
    output = []
    with open(config) as f:
      for line in f:
        if re.search('^mode\s*=', line):
          output.append('mode = %d\n' % mode)
        elif re.search('^fps\s*=', line):
          output.append('fps = %d\n' % fps)
        elif re.search('^shutter_speed\s*=', line):
          output.append('shutter_speed = %d\n' % shutter)
        elif re.search('^strobe_offset\s*=', line):
          output.append('strobe_offset = %d\n' % offset)
        elif re.search('^strobe_length\s*=', line):
          output.append('strobe_length = %d\n' % strobe)
        elif re.search('^width\s*=', line):
          output.append('width = %d\n' % RESOLUTION[mode][0])
        elif re.search('^height\s*=', line):
          output.append('height = %d\n' % RESOLUTION[mode][1])
        elif re.search('^load_stride\s*=', line):
          output.append('load_stride = %d\n' % RESOLUTION[mode][2])
        else:
          output.append(line);

    for line in output:
      print(line, end='')
  
  # profile output
  
  else:
    output = {'mode'           : mode,
             'fps'           : fps,
             'shutter_speed' : shutter,
             'strobe_offset' : offset,
             'strobe_length' : strobe,
             'width'         : RESOLUTION[mode][0],
             'height'        : RESOLUTION[mode][1]}
    
    output = { 'engine' : output }
    if comm:
      output['comment'] = comm
      
    print(json.dumps(output, indent=4, sort_keys=True))
  
except RuntimeError as err:
  sys.stderr.write('An error occured: %s\n' % err)
  
