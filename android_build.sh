#!/bin/sh

CMAKE=~/Library/Android/sdk/cmake/3.6.4111459/bin/cmake
CMAKE_TOOLCHAIN_FILE=~/Library/Android/sdk/ndk-bundle/build/cmake/android.toolchain.cmake
ABIS="arm64-v8a armeabi-v7a x86_64 x86"
#ABIS="x86"
#ABIS="arm64-v8a"
API_LEVEL=24
STL=c++_static
CMAKE_INSTALL_PREFIX_BASE=/Users/jyunjikondo/git/bitbucket/mkert/install
BOOST_BASE_DIR=/Users/jyunjikondo/boost
FTDI_BASE_DIR=/Users/jyunjikondo/Downloads/libftdi1/install

for abi in $ABIS
do
  rm -rf build
  mkdir build
  cd build
  $CMAKE \
    -DBoost_DEBUG=ON \
    -DCMAKE_TOOLCHAIN_FILE=$CMAKE_TOOLCHAIN_FILE -DANDROID_ABI=$abi -DANDROID_NATIVE_API_LEVEL=$API_LEVEL -DANDROID_STL=$STL -Bbuild \
    -DCMAKE_INSTALL_PREFIX=$CMAKE_INSTALL_PREFIX_BASE/$abi \
    -DBOOST_INCLUDEDIR=$BOOST_BASE_DIR/$abi/include/ -DBOOST_LIBRARYDIR=$BOOST_BASE_DIR/$abi/lib/ \
    -DBoost_NAMESPACE=libboost \
    -DCMAKE_FIND_ROOT_PATH_MODE_INCLUDE=BOTH -DCMAKE_FIND_ROOT_PATH_MODE_LIBRARY=BOTH \
    -DUSE_FPCAM=ON -DUSE_UPNP=OFF -DUSE_RSPI=OFF -DUSE_SYSLOG=OFF -DFTD2XX_DIR="/" -DMKEDD_TESTS=OFF \
    -DFTDI_DIR=$FTDI_BASE_DIR/$abi \
    -DUSE_AS_LIBRARY=ON \
    -DCMAKE_BUILD_TYPE=Release \
    -DMKEDD_MULTITHREADING=OFF \
    -DUSE_RESERVED_API=ON \
    -DMKEDD_DEMO=OFF \
    ..
  make install
  cd ..
done
