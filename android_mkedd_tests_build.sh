#!/bin/sh

CMAKE=~/Library/Android/sdk/cmake/3.6.4111459/bin/cmake
CMAKE_TOOLCHAIN_FILE=~/Library/Android/sdk/ndk-bundle/build/cmake/android.toolchain.cmake
ABIS="arm64-v8a armeabi-v7a x86_64 x86"
#ABIS="x86_64"
ABIS="arm64-v8a"
API_LEVEL=24
STL=c++_static
CMAKE_INSTALL_PREFIX_BASE=/Users/jyunjikondo/git/bitbucket/mkert/install
BOOST_BASE_DIR=/Users/jyunjikondo/boost
FTDI_BASE_DIR=/Users/jyunjikondo/Downloads/libftdi1/install
ANDROID_BASE=/Users/jyunjikondo/AndroidStudioProjects/MkeCameraDemo/mkeddtests
HALIDE_ROOT_DIR=/Users/jyunjikondo/Downloads/halide
USE_HALIDE_HEXAGON=OFF

for abi in $ABIS
do
  rm -rf build
  mkdir build
  cd build
  $CMAKE \
    -DBoost_DEBUG=ON \
    -DCMAKE_TOOLCHAIN_FILE=$CMAKE_TOOLCHAIN_FILE -DANDROID_ABI=$abi -DANDROID_NATIVE_API_LEVEL=$API_LEVEL -DANDROID_STL=$STL -Bbuild \
    -DCMAKE_INSTALL_PREFIX=$CMAKE_INSTALL_PREFIX_BASE/$abi \
    -DBOOST_INCLUDEDIR=$BOOST_BASE_DIR/$abi/include/ -DBOOST_LIBRARYDIR=$BOOST_BASE_DIR/$abi/lib/ \
    -DBoost_NAMESPACE=libboost \
    -DCMAKE_FIND_ROOT_PATH_MODE_INCLUDE=BOTH -DCMAKE_FIND_ROOT_PATH_MODE_LIBRARY=BOTH \
    -DUSE_FPCAM=ON -DUSE_UPNP=OFF -DUSE_RSPI=OFF -DUSE_SYSLOG=OFF -DFTD2XX_DIR="/" -DMKEDD_TESTS=OFF \
    -DFTDI_DIR=$FTDI_BASE_DIR/$abi \
    -DUSE_AS_LIBRARY=ON \
    -DCMAKE_BUILD_TYPE=Release \
    -DMKEDD_MULTITHREADING=ON \
    -DUSE_RESERVED_API=ON \
    -DMKEDD_DEMO=OFF \
    -DMKEDD_USE_NEON=ON \
    -DMKEDD_USE_HALIDE=ON \
    -DMKEDD_USE_HALIDE_HEXAGON=$USE_HALIDE_HEXAGON \
    -DHALIDE_ROOT_DIR=$HALIDE_ROOT_DIR \
    -DUSE_RUNTIME_STATS=OFF \
    ..
  VERBOSE=1 make clean mkedd_static

  if [ ! -d $ANDROID_BASE/libs/mkedd/$abi/lib ] ; then
    mkdir -p $ANDROID_BASE/libs/mkedd/$abi/lib
  fi
  cp mkedd/src/libmkedd.a $ANDROID_BASE/libs/mkedd/$abi/lib
  cp mkedd/src/halide-prefix/src/halide-build/genfiles/halide_convolve/halide_convolve.a $ANDROID_BASE/libs/halide/$abi
  cp mkedd/src/halide-prefix/src/halide-build/genfiles/halide_convolve/halide_convolve.h $ANDROID_BASE/src/main/cpp
  RUNTIME=halide_rt_arm_64_android
  if [ ${USE_HALIDE_HEXAGON:-OFF} = ON ] ; then
    RUNTIME=${RUNTIME}_hvx_128
  fi
  cp mkedd/src/halide-prefix/src/halide-build/genfiles/$RUNTIME/$RUNTIME.a $ANDROID_BASE/libs/halide/$abi/halide_runtime.a

  cd ..
done
